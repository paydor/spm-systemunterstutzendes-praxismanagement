<?php

require_once 'calsy/calendar/CalsyCalendar.class.php';

try
{
      
    {
                
        $interval = 300;

        $notification = new CalsyCalendarNotification(floor($executionStartTime), $interval);
        $notification->sendNotifications();
        
    }
            
    $result = Array(

        "notifications" => $notification->getNotificationCount(),
        "message" => PerisianLanguageVariable::getVariable(10678),
        "success" => true

    );
   
}
catch(PerisianException $e)
{
    
    $result = Array(
        
        "message" => $e->getMessage(),
        "success" => false
        
    );
            
}
