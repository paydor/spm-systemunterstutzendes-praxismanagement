<?php

try
{
      
    {
        
        $command = "sudo date -s \"\$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z\"";
        
        PerisianCLI::executeCommand($command);
        
    }
            
    $result = Array(

        "message" => PerisianLanguageVariable::getVariable('p58a88ab698508'),
        "success" => true

    );
   
}
catch(PerisianException $e)
{
    
    $result = Array(
        
        "message" => $e->getMessage(),
        "success" => false
        
    );
            
}
