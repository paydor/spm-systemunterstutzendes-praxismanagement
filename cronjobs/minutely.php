<?php

// Will be executed every single minute

require_once 'includes/header_cronjob.inc.php';

$overallResult = array();

{
    
    require 'process_engine/start_processes.php';
    
    $newEntry = array(
        
        'title' => PerisianLanguageVariable::getVariable('p58a73e57ccf1e'), 
        'result' => $result,
        'time' => time(),
        'time_formatted' => date('Y-m-d H-i-s', time())
            
    );
    
    array_push($overallResult, $newEntry);
    
}

//file_put_contents(($currentPath . '/../files/log/cronjob_minutely.log'), json_encode($overallResult));

print_r($overallResult);