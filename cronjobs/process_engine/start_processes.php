<?php

require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'modules/CalsyProcessEngineModule.class.php';

try
{
      
    {
        
        $processCount = 0;
    
        if(CalsyProcessEngineModule::isEnabled())
        {

            $interval = 60;

            $timeEnd = time();
            $timeBegin = $timeEnd - $interval;

            $latestEntries = CalsyProcessEngineModule::getProcessEngineCalendarEntriesBetween($timeBegin, $timeEnd);
            $processCount = count($latestEntries);

            foreach($latestEntries as $latestEntry)
            {

                $processId = CalsyCalendarEntryFlag::getFlagValueForEntry($latestEntry->{$latestEntry->field_pk}, CalsyProcessEngineModule::FLAG_PROCESS_TO_START);

                if(strlen($processId) > 0)
                {

                    CalsyProcessEngineModule::startProcess($processId);

                }
                else
                {

                    --$processCount;

                }

            }

        }
        
    }
            
    $result = Array(

        "processes" => $processCount,
        "message" => PerisianLanguageVariable::getVariable(10678),
        "success" => true

    );
   
}
catch(PerisianException $e)
{
    
    $result = Array(
        
        "message" => $e->getMessage(),
        "success" => false
        
    );
            
}

