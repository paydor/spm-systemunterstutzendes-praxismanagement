<?php

// Will be executed every 5 minutes

require_once 'includes/header_cronjob.inc.php';

$overallResult = array();

{
    
    require 'calendar/notification.php';
    
    $newEntry = array(
        
        'title' => PerisianLanguageVariable::getVariable(11187), 
        'result' => $result,
        'time' => time(),
        'time_formatted' => date('Y-m-d H-i-s', time())
            
    );
    
    array_push($overallResult, $newEntry);
    
}

//file_put_contents(($currentPath . '/../files/log/cronjob_five_minutes.log'), json_encode($overallResult));

print_r($overallResult);