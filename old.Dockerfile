# Main file to compose the docker image

# General Apache image
#FROM bitnami/apache

# OpenShift specific Apache image: https://hub.docker.com/r/centos/httpd-24-centos7/
#FROM centos/httpd-24-centos7 

#FROM php:7.3.4

# Another attempt from https://github.com/docker-library/php/issues/771 
#FROM piegsaj/nano
FROM php:7.2-apache-stretch

ARG ARG_APACHE_LISTEN_PORT=8000
ENV APACHE_LISTEN_PORT=${ARG_APACHE_LISTEN_PORT}
RUN sed -s -i -e "s/80/${APACHE_LISTEN_PORT}/" /etc/apache2/ports.conf /etc/apache2/sites-available/*.conf

#USER www-data
EXPOSE ${APACHE_LISTEN_PORT}
# /Another attempt

MAINTAINER ConsSys IT AG

COPY . /var/www/docker-calsy
COPY ./.docker/vhost.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/docker-calsy

RUN docker-php-ext-install mysqli mbstring pdo pdo_mysql \
    && chown -R www-data:www-data /var/www/docker-calsy \
    && chmod 0777 /var/www/docker-calsy/configuration \
    && chmod 0777 /var/www/docker-calsy/database
    #&& a2enmod rewrite

