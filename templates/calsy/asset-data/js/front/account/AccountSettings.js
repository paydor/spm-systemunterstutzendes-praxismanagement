PerisianBaseSettings.controller = baseUrl + 'account/settings/';

var SystemSettings = jQuery.extend(true, {}, PerisianBaseAjax);
SystemSettings.controller = PerisianBaseSettings.controller;

var AccountSettings = {
    
    controllerPrivacy: baseUrl + 'account/privacy/',
        
    changeLanguage: function() {

        var sendData = {

            'do': 'changeLanguage',
            'lg': jQuery('#user_frontend_language').val()

        };
        
        jQuery.post(PerisianBaseSettings.controller, sendData, function(data) {
            
            var callback = JSON.parse(data);
            
            if(callback.success)
            {
                
                location.reload();
                
            }
            else
            {
                
                toastr.warning(Language.getLanguageVariable(10484));
                
            }
            
        });
        
    },
    
    privacyDisclosure: function() {
        
        location.href = this.controllerPrivacy;
        
    },
    
    privacyDeleteAccount: function() 
    {
        
        var sendData = {
            
            'do': 'privacyDeleteAccountConfirmation'
            
        };
             
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');

        elementContainer.load(PerisianBaseSettings.controller, sendData, function (data) {

            Perisian.ajaxBox('show');
            
        });
        
    },
    
    privacyDeleteAccountConfirm: function()
    {
        
        if(confirm(Language.getLanguageVariable('p5aee1f3aef98e')))
        {

            location.href = this.controllerPrivacy + '?do=delete';

        }
        
    }
    
};