var Login = {
    
    step: 'email',
    
    addressLoginUtils: baseUrl + '/ajax/login_utils/',
    adressRegistration: baseUrl + 'registration/'
    
};

Login.initialize = function()
{
    
    jQuery('#language-selector').on('change', function(e) {
        
        var newLanguageCode = jQuery(this).find('option:selected').attr('code');
        var documentLanguageCode = jQuery('html').attr('lang');

        if(newLanguageCode != documentLanguageCode)
        {

            var newLanguageId = jQuery(this).val();

            document.location.search = Perisian.addUrlParam(document.location.search, "lg", newLanguageId);

        }

    });
    
    jQuery('#login_email, #login_password, #login_set_password_one, #login_set_password_two').keypress(function(e) {
        
        if(e.keyCode == 13)
        {
            
            Login.nextPage();
            
        }
        
    });
    
    jQuery('#login_email').focus();
    
};

Login.nextPage = function()
{
    
    if(Login.step == 'email')
    {
        
        var sendData = {

            'u': 'm',
            'm': jQuery('#login_email').val()

        };

        jQuery.ajax({

            type: 'GET',
            async: true,
            timeout: 5000,
            url: Login.addressLoginUtils,
            data: sendData, 

            success: function(result) {

                var resultData = JSON.parse(result);
                
                if(resultData['is_valid'])
                {
                    
                    if(resultData['is_active'] && resultData['is_password_set'])
                    {
                        
                        Login.showStep('password');
                        
                        jQuery('#login_password').focus();
                        
                    }
                    else if(!resultData['is_password_set'])
                    {
                                                
                        Login.showStep('set-password');
                        
                    }
                    else if(!resultData['is_active'] && resultData['is_password_set'])
                    {
                        
                        // User is currently inactive
                        Login.showError(Language.getLanguageVariable(10639));
                        
                    }
                    
                }
                else
                {
                    
                    // Unknown email address
                    Login.showError(Language.getLanguageVariable('p576973285e8af'));
                    
                }

            }

        });
        
    }
    else if(Login.step == 'password')
    {
        
        jQuery('#login-form').submit();
    
    }
    else if(Login.step == 'set-password')
    {
        
        if(Validation.checkPassword(jQuery('#login_set_password_one').val(), jQuery('#login_set_password_two').val(), '#login-set-password-error'))
        {
            
            var sendData = {

                'u': 'sp',
                'm': jQuery('#login_email').val(),
                'p': jQuery('#login_set_password_one').val()

            };

            jQuery.ajax({

                type: 'GET',
                async: true,
                timeout: 5000,
                url: Login.addressLoginUtils,
                data: sendData, 

                success: function(result) {

                    var resultData = JSON.parse(result);

                    if(resultData['success'])
                    {
                        
                        // Successfully set the password
                        Login.showSuccess(Language.getLanguageVariable('p57698af06e73c'));

                    }
                    else
                    {

                        // Could not save the password
                        Login.showError(Language.getLanguageVariable('p57698a2fc8df7'));

                    }

                    console.log(resultData);

                }

            });
            
        }
        
    }
    
};

Login.showError = function(errorString) 
{
    
    errorString = '<b>' + Language.getLanguageVariable(10623) + '</b>: ' + errorString;
    
    jQuery('#login-error-box').html(nl2br(errorString));
    
    Login.showStep('error');
    
};

Login.showSuccess = function(successString) 
{
    
    successString = '<b>' + Language.getLanguageVariable('p576989fe6143f') + '</b>: ' + successString;
    
    jQuery('#login-success-box').html(nl2br(successString));
    
    Login.showStep('success');
    
};

Login.tryAgain = function()
{
    
    Login.showStep('email');
    
};

Login.showStep = function(step)
{
    
    var showHeader = false;
    var showRegistrationBox = false;
    
    if(step == 'email')
    {
        
        showHeader = true;
        showRegistrationBox = true;
        
    }
        
    jQuery('#login-box-header').css('display', showHeader ? 'block' : 'none');
    jQuery('#login-registration-box').css('display', showRegistrationBox ? 'block' : 'none');
    
    jQuery('#login-set-password-error').css('display', 'none');
    
    jQuery('.login-page').css('display', 'none');
    
    jQuery('#login-page-' + step).css('display', 'block');
    
    Login.step = step;
    
};

Login.restorePassword = function()
{
        
    location.href = Login.adressRegistration + '?do=rp&m=' + jQuery('#login_email').val();
    
};

Login.register = function()
{
    
    var mailValue = jQuery('#login_email').val();
        
    location.href = Login.adressRegistration + (mailValue.length > 0 ? '?m=' + mailValue : '');
    
};