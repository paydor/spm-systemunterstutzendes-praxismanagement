var AppointmentAssistant = {
    
    allowBookingsToday: false,

    containerIdentifier: 'appointment-assistant',
    warningBoxIdentifier: 'warning-box',
    successBoxIdentifier: 'success-box',
    
    addressRegistration: baseUrl + '/registration/',
    addressAjaxUsersForArea: baseUrl + '/ajax/users_for_area/',
    addressAjaxAppointmentProposals: baseUrl + '/ajax/appointment_proposal/',
    addressAjaxUserFrontendFullname: baseUrl + '/ajax/user_frontend_fullname/',
    addressAjaxAreaPriceInfo: baseUrl + '/ajax/price_for_area/',
    addressAjaxCreate: baseUrl + '/ajax/appointment_create/',
    
    currentQueryProposal: null,
    datePreselected: -1,
    
    // The field identifiers for the registration
    fieldsRegistration: {

        emailOne: '#aa_registration_email_one',
        emailTwo: '#aa_registration_email_two',

        passwordOne: '#aa_registration_password_one',
        passwordTwo: '#aa_registration_password_two',

        nameFirst: '#aa_registration_name_first',
        nameSecond: '#aa_registration_name_second',

        gender: '#aa_registration_sex',
        phone: '#aa_registration_phone'

    },
    
    // The field identifiers for the login
    fieldsLogin: {
                
        email: '#aa_login_email',
        password: '#aa_login_password'

    },
    
    validationFieldTypes: 'select, textarea, input',
    
    /**
     * Initializes the appointment assistant
     * 
     * @author Peter Hamm
     * @param String containerIdentifier The ID of the containing element.
     * @param String containerIdentifier The ID of the success box element.
     * @returns void 
     */
    initialize: function(containerIdentifier, succesBoxIdentifier, targetTabIndex)
    {
        
        if(containerIdentifier != null && containerIdentifier.length > 0)
        {
            
            AppointmentAssistant.containerIdentifier = containerIdentifier.replace('#', '');
            
        }
        
        if(succesBoxIdentifier != null && succesBoxIdentifier.length > 0)
        {
        
            AppointmentAssistant.succesBoxIdentifier = succesBoxIdentifier.replace('#', '');
        
        }
        
        PerisianFormPager.initFormPager(AppointmentAssistant.containerIdentifier, true);
        PerisianFormPager.onBeforeNavigate = AppointmentAssistant.onBeforeNavigate;
        
        AppointmentAssistant.initIconSelectMenuWidget();
                
        AppointmentAssistant.initLoginRegistrationSwitch();
        AppointmentAssistant.initAreaSwitch();
        AppointmentAssistant.updateUserSelector();
        AppointmentAssistant.initDatePicker();
        
        AppointmentAssistant.initNavigationButtons();
        AppointmentAssistant.initTabButtons();
        AppointmentAssistant.initFieldChanges();
        
        setTimeout(function() {
            
            AppointmentAssistant.toggleLoadingIndicator(false);
            
            if(targetTabIndex && targetTabIndex >= 0)
            {
                
                AppointmentAssistant.navigateTo(targetTabIndex);
                
            }
            
        }, 2000);
        
        PerisianFormPager.executeConditions(0);
        
    },
    
    /**
     * Initializes the widget to display an icon select menu.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initIconSelectMenuWidget: function() {
        
        jQuery.widget("custom.iconselectmenu", jQuery.ui.selectmenu, {

            _renderItem: function(ul, item) {

                var li = jQuery("<li>");

                var wrapper = jQuery("<div>", {

                    text: item.label 

                });

                if(item.disabled) 
                {

                    li.addClass("ui-state-disabled");

                }

                var span = jQuery("<span>", {

                    style: item.element.attr("data-style"),
                    "class": "user-color"

                });

                if(item.element.attr("value") == 0 || item.element.attr("value") == '__none')
                {

                    span.css('display', 'none');

                }

                span.css('background-color', item.element.css('background-color'));
                span.css('background-image', item.element.css('background-image'));
                span.css('float', 'left');
                span.css('margin-left', '2px');
                span.css('margin-right', '8px');

                span.appendTo(wrapper);

                return li.append(wrapper).appendTo(ul);

            }

        });
        
    },
    
    /**
     * Initializes what happens once a field is changed, e.g. the conditions are being checked.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initFieldChanges: function(){
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        containerElement.find('input, select, textarea').change(function() {
            
            var currentIndex = PerisianFormPager.getCurrentIndex(containerElement.attr('id'));
            
            PerisianFormPager.executeConditions(currentIndex);
            
        });
        
    },
    
    /**
     * Toggles the display of the loading indicator.
     * 
     * @author Peter Hamm
     * @param bool show
     * @returns void
     */
    toggleLoadingIndicator: function(show)
    {
                
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var indicatorBoxElement = containerElement.find('#loading-box');
        var contentBoxElement = containerElement.find('#content-box');
        
        indicatorBoxElement.css('display', show ? 'block' : 'none');
        contentBoxElement.css('display', !show ? 'block' : 'none');
        
    },
    
    /**
     * Callback, fired just before a navigation to another tab occurs.
     * 
     * @author Peter Hamm
     * @param String containerElement
     * @param int index
     * @returns bool Returning false will cancel the navigation.
     */
    onBeforeNavigate: function(containerElement, index)
    {
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var currentIndex = PerisianFormPager.getCurrentIndex(containerElement.attr('id'));
        var tabs = PerisianFormPager.getTabs(containerElement.attr('id'));
                
        if(currentIndex == 0)
        {
            
            // Make sure that users are loaded for the user selection (if it is enabled)

            var selectElement = jQuery('#' + AppointmentAssistant.containerIdentifier).find('#user_backend');
            var selectedElementOptions = selectElement.find('option');

            if(selectElement.length > 0 && selectedElementOptions.length == 0)
            {

                var selectedArea = jQuery('input[type=radio][name="area"]:checked').val();
                
                AppointmentAssistant.loadUsersForArea(selectedArea);

            }
            
        }
        
        if(index == 2 && containerElement.find('#aa_appointment_proposal:checked').length == 0)
        {
            
            // Navigating to the appointment tab: 
            // Make sure that appointment proposals are loaded for the area and user selection (if enabled)

            AppointmentAssistant.loadAppointmentProposals();
            
        }
        else if(index == tabs.length - 1)
        {
            
            AppointmentAssistant.prepareApproval();
            
        }
        
        return true;
        
    },
    
    /**
     * Prepares the displayed data on the 'approval' tab.
     * 
     * @author Peter Hamm
     * @returns void
     */
    prepareApproval: function()
    {
                
        var checkData = AppointmentAssistant.getSendData();
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        containerElement.find('#approval_user_frontend_login').css('display', checkData.isRegistered && checkData.user_frontend ? 'block' : 'none');
        containerElement.find('#approval_user_frontend_registration').css('display', checkData.isRegistered || !checkData.user_frontend ? 'none' : 'block');
                
        if(checkData.isRegistered)
        {
         
            containerElement.find('#approval_user_frontend_login').find('.col-sm-8').html(checkData.user_frontend.fullname);
            
        }
        else
        {
                        
            if(checkData.user_frontend)
            {

                jQuery.each(checkData.user_frontend, function(key, value) {

                    var fieldIdentifier = AppointmentAssistant.fieldsRegistration[key] + '_approval';

                    if(key == 'gender')
                    {

                        var languageVariableId = 10833;

                        if(value == 'male')
                        {

                            languageVariableId = 10831;

                        }
                        else if(value == 'female')
                        {

                            languageVariableId = 10832;

                        }

                        value = Language.getLanguageVariable(languageVariableId);

                    }

                    containerElement.find(fieldIdentifier).html(value);

                });
                
            }
            
        }
        
        // The appointment
        {
            
            // The time
            
            var timeLabel = containerElement.find('#aa_appointment_proposal:checked').parent().find('span').html();
            
            containerElement.find('#approval_appointment_time').html(timeLabel);
            
            if(checkData.appointment['userBackend'] !== undefined && checkData.appointment['userBackend'].length > 0 && checkData.appointment['userBackend'] != "0")
            {
                
                // Selected backend user
                containerElement.find('#approval_appointment_user_backend').html(containerElement.find('#user_backend option:selected').html());
                
                containerElement.find('#approval_appointment_user_backend_container').css('display', 'block');
                
            }
            else
            {
                
                containerElement.find('#approval_appointment_user_backend_container').css('display', 'none');
                
            }
            
            containerElement.find('#approval_appointment_area').html(containerElement.find('input[type=radio][name="area"]:checked').attr('label'));
            
            containerElement.find('#approval_appointment_annotations_container').css('display', checkData.appointment.annotations.length > 0 ? 'block' : 'none');
            containerElement.find('#approval_appointment_annotations').html(checkData.appointment.annotations);
                        
            if(checkData.image)
            {
                
                var imageTag = '<img src="' + baseUrl + '/u/image/' + checkData.image.file + '" class="aa_uploaded_image"/>';
                
                containerElement.find('#approval_appointment_annotations_image').html(imageTag);
                
                containerElement.find('#approval_appointment_annotations_image_container').css('display', 'block');
                
            }
            
            if(checkData['multiple-booking'])
            {
                
                jQuery('#approval_multiple_booking').css('display', 'block');
                
                var dummy = jQuery('#approval_multiple_booking_dummy').html();
                
                jQuery('#approval_multiple_booking').html('');
                
                for(var i = 0; i < checkData['multiple-booking'].amount; ++i)
                {
                    
                    var multipleBookingElement = dummy.replace('%bookingNumber', (i + 1));
                    multipleBookingElement = multipleBookingElement.replace('%approval_multiple_booking_title', checkData['multiple-booking']['data'][i]['name']);
                    
                    jQuery('#approval_multiple_booking').append(multipleBookingElement);
                    
                }
                
            }
            
            // Additional fields
            {
                
                var additionalFieldContainer = containerElement.find('#additional_fields_approval');
                
                additionalFieldContainer.css('display', Object.keys(checkData.appointment.additionalFieldData).length > 0 ? 'block' : 'none');
                additionalFieldContainer.empty();
                
                jQuery.each(checkData.appointment.additionalFieldData, function(key, value) {
                    
                    if(key == 'area')
                    {
                        
                        // Already got this covered.
                        return true;
                        
                    }
                    
                    var newElement = containerElement.find('#field-dummy').clone();
                    
                    newElement.attr('id', '');
                    newElement.css('display', 'block');

                    var additionalField = jQuery('#' + key);

                    var additionalFieldLabel = additionalField.parent().parent().find('.col-sm-4').html();
                    var additionalFieldValue = value;
                    
                    if(additionalField.is('input') && additionalField.prop('type') == 'radio')
                    {

                        additionalFieldLabel = additionalField.parent().parent().parent().parent().find('.col-sm-4').html();
                        additionalFieldValue =  additionalField.parent().parent().parent().find('input:checked').parent().find('span').html()

                    }
                    else if(additionalField.is('select'))
                    {

                        additionalFieldLabel = additionalField.parent().parent().parent().find('.col-sm-4').html();
                        additionalFieldValue = additionalField.find('option:selected').text();

                    }
                    
                    additionalFieldLabel = additionalFieldLabel.replace('<sup>*</sup>', '');
                    
                    newElement.find('#container-key').html(additionalFieldLabel);
                    newElement.find('#container-value').html(additionalFieldValue);
                    
                    additionalFieldContainer.append(newElement);

                });
                
            }
            
        }
        
        // Area price & payment preparations
        {
                        
            var sendData = {
                
                'a': containerElement.find('input[type=radio][name="area"]:checked').val()
                
            };
            
            jQuery.ajax({

                type: 'GET',
                async: false,
                timeout: 5000,
                url: AppointmentAssistant.addressAjaxAreaPriceInfo,
                data: sendData, 

                success: function(result) {

                    var resultObject = JSON.parse(result);
                    
                    var priceFieldContainer = containerElement.find('#area_price_approval');
                    
                    priceFieldContainer.css('display', parseFloat(resultObject['price_net']) > 0 ? 'block' : 'none');
                    priceFieldContainer.empty();
                    
                    var needsPayment = parseFloat(resultObject['price_net']) > 0;
                    
                    jQuery('#approval-hint').html(nl2br(Language.getLanguageVariable(needsPayment ? 'p5a318f2304aff' : '11043')));
                                        
                    jQuery('#btn-confirm').html(Language.getLanguageVariable(needsPayment ? 'p59f4e5b4891c6' : '11044'));
                    
                    jQuery('#btn-success-login').css('display', (needsPayment ? 'none' : 'block'));
                    jQuery('#btn-success-payment').css('display', (needsPayment ? 'block' : 'none'));
                                        
                    if(needsPayment)
                    {
                        
                        // Net price
                        {

                            var newElement = containerElement.find('#field-dummy').clone();

                            newElement.attr('id', '');
                            newElement.css('display', 'block');

                            newElement.find('#container-key').html(Language.getLanguageVariable('p59f34cbcc1a4f'));
                            newElement.find('#container-value').html(resultObject['price_net'] + ' ' + resultObject['currency_symbol']);

                            priceFieldContainer.append(newElement);

                        }

                        // Tax price
                        {

                            var newElement = containerElement.find('#field-dummy').clone();

                            newElement.attr('id', '');
                            newElement.css('display', 'block');

                            newElement.find('#container-key').html(Language.getLanguageVariable('p59f3975c65957'));
                            newElement.find('#container-value').html(resultObject['price_tax'] + ' ' + resultObject['currency_symbol'] + ' (' + resultObject['percentage_tax'] + '%)');

                            priceFieldContainer.append(newElement);

                        }

                        // Gross price
                        {

                            var newElement = containerElement.find('#field-dummy').clone();

                            newElement.attr('id', '');
                            newElement.css('display', 'block');

                            newElement.find('#container-key').html(Language.getLanguageVariable('p59f4e25ed8372'));
                            newElement.find('#container-value').html('<b>' + resultObject['price_gross'] + ' ' + resultObject['currency_symbol'] + '</b>');

                            priceFieldContainer.append(newElement);

                        }
                      
                    }

                }

            });
            
        }
        
    },
    
    /**
     * Retrieves the data from the whole form that will be sent in the end.
     * 
     * @author Peter Hamm
     * @returns Object
     */
    getSendData: function()
    {
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var isRegistered = containerElement.find('#registration_login_selector input[type=radio]:checked').val() == 1;
        
        var data = {};
        
        if(containerElement.find('#registration_login_selector').length > 0)
        {
            
            data.isRegistered = isRegistered;

            if(isRegistered)
            {

                data['user_frontend'] = AppointmentAssistant.getValuesForFields(AppointmentAssistant.fieldsLogin);
                data['stay_logged_in'] = jQuery('#aa_stay_logged_in').is(':checked') ? 1 : 0;

                // Retrieve the frontend user's full name
                {

                    var sendData = {

                        'm': data['user_frontend']['email'],
                        'p': data['user_frontend']['password']

                    };

                    jQuery.ajax({

                        type: 'GET',
                        async: false,
                        timeout: 5000,
                        url: AppointmentAssistant.addressAjaxUserFrontendFullname,
                        data: sendData, 

                        success: function(result) {

                            var resultObject = JSON.parse(result);

                            data['user_frontend']['fullname'] = resultObject.fullname;

                        }

                    });

                }

            }
            else
            {

                data['user_frontend'] = AppointmentAssistant.getValuesForFields(AppointmentAssistant.fieldsRegistration);

            }
            
        }
        
        // Additional field data, from the enhanced fields
        {
            
            var additionalFieldData = {};
            
            containerElement.find('.additional-field').each(function() {
                        
                var additionalField = jQuery(this);

                if(additionalField.css('display') == 'block')
                {

                    additionalField.find('input, select').each(function() {

                        var field = jQuery(this);

                        additionalFieldData[field.attr('id')] = AppointmentAssistant.getFieldValue(field.attr('id'));

                    });

                }

            });
            
            data['appointment'] = {

                time: containerElement.find('#aa_appointment_proposal:checked').val(),
                timeEnd: containerElement.find('#aa_appointment_proposal:checked').attr('end'),
                user_backend: containerElement.find('#user_backend option:selected').val(),
                annotations: containerElement.find('#annotations').val(),
                area: containerElement.find('input[type=radio][name="area"]:checked').val(),
                
                additionalFieldData: additionalFieldData

            };
            
        }
        
        if(containerElement.find('#appointment_image_filename').length > 0)
        {
            
            data['image'] = {
                
                'file': containerElement.find('#appointment_image_filename').val(),
                'filename': containerElement.find('.dz-filename span').html()
                
            }
            
        }
        
        if(jQuery('#multiple-booking-amount').length > 0)
        {
            
            var amountMultipleBooking = AppointmentAssistant.getAmountMultipleBooking();
            
            if(amountMultipleBooking > 1)
            {

                data['multiple-booking'] = {

                    'amount': amountMultipleBooking,
                    'data': []

                };

                jQuery('#multiple-booking-title-container .multiple-booking-title-element').each(function() {

                    var element = jQuery(this);

                    var multipleBookingElement = {

                        'name': element.find('input').val()

                    };

                    data['multiple-booking']['data'].push(multipleBookingElement);

                });
                
            }
            
        }
        
        return data;
        
    },
    
    /**
     * Retrieves the amount of multiple bookings.
     * 
     * @author Peter Hamm
     * @returns int
     */
    getAmountMultipleBooking: function()
    {
        
        var amount = jQuery('#multiple-booking-amount').length == 0 ? 1 : parseInt(jQuery('#multiple-booking-amount :selected').val());
        
        return amount;
        
    },
    
    /**
     * Retrieves the value for the specified field identifier.
     * 
     * @author Peter Hamm
     * @param String fieldIdentifier
     * @returns String
     */
    getFieldValue: function(fieldIdentifier)
    {
        
        fieldIdentifier = fieldIdentifier.replace('#', '');
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var field = containerElement.find('#' + fieldIdentifier);

        var fieldValue = "";
        
        if(field.is('input'))
        {

            if(field.prop('type') == 'text' || field.prop('type') == 'password')
            {

                fieldValue = field.val();

            }
            else if(field.prop('type') == 'radio')
            {
                                
                fieldValue = containerElement.find('input[type=radio][name="' + field.attr('name') + '"]:checked').val();
                
                if(!fieldValue)
                {
                    
                    fieldValue = '';
                    
                }
                
            }

        }
        else if(field.is('select'))
        {

            fieldValue = field.find('option:selected').val();

        }
      
        return fieldValue;
        
    },
    
    /**
     * Initializes the basic navigation buttons (previous / next / approve)
     * 
     * @author Peter Hamm
     * @returns void
     */
    initNavigationButtons: function()
    {
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
          
        var tabButtonsNext = containerElement.find('.btn-next');
        
        tabButtonsNext.each(function(index) {
            
            var button = jQuery(this);

            if(index == tabButtonsNext.length - 1)
            {

                button.click(AppointmentAssistant.confirmAndSend);

            }
            else
            {

                button.click(AppointmentAssistant.navigateNext);

            }
            
        });
        
        var tabButtonsPrevious = containerElement.find('.btn-previous');
        
        tabButtonsPrevious.each(function(index) {
            
            var button = jQuery(this);
            
            button.click(AppointmentAssistant.navigatePrevious);
            
        });
        
    },
    
    /**
     * Initializes the tab buttons on the top of the form.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initTabButtons: function() 
    {
        
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
          
        var tabButtons = element.find('.nav li');

        tabButtons.each(function(index) {

            var button = jQuery(this);

            button.click(function() {
                
                AppointmentAssistant.navigateTo(index);

            });

        });
        
    },
    
    /**
     * Updates the image of the selected backend user.
     * 
     * @author Peter Hamm
     * @returns void
     */
    updateSelectedUserBackendImage: function() {
        
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var imageBox = element.find('#user-backend-image-container');
        var selectedUserOption = element.find('#user_backend option:selected');
                
        if(selectedUserOption.css('background-image').length > 0 && selectedUserOption.css('background-image') != 'none')
        {
            
            imageBox.css('background-image', selectedUserOption.css('background-image'));
            imageBox.css('display', 'block');
            
        }
        else
        {
            
            imageBox.css('display', 'none');
            
        }
                
    },
        
    /**
     * Updates the user selector.
     * 
     * @author Peter Hamm
     * @returns void
     */
    updateUserSelector: function()
    {
        
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
                
        try
        {
            
            element.find("#user_backend").iconselectmenu("destroy");

            element.find('#user-backend-image-container').css('display', 'none');
            
        }
        catch(error)
        {
            
        }
                
        element.find('#user_backend').iconselectmenu({
            
            'change': function() {
            
                AppointmentAssistant.updateSelectedUserBackendImage();

                AppointmentAssistant.loadAppointmentProposals();

            }
            
        }).iconselectmenu("menuWidget").addClass("ui-menu-icons customicons");
        
    },
    
    /**
     * Initializes the switch where the user can choose to area.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initAreaSwitch: function()
    {
        
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        element.find('input[type=radio][name="area"]').each(function(i) {
            
            var radio = jQuery(this);
            
            radio.change(function() {

                var element = jQuery(this);

                if(element.is(":checked")) 
                {
                    
                    AppointmentAssistant.loadUsersForArea(element.val());
                    AppointmentAssistant.loadAppointmentProposals();

                }

            });
        
        });
        
    },
    
    /**
     * Initializes the date picker for the appointment selection
     * 
     * @author Peter Hamm
     * @returns void
     */
    initDatePicker: function()
    {
        
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        element.find('#aa_date_selection_earliest_possible_date').datepicker({
            
            language: Language.getLanguageCode(),
            startDate: AppointmentAssistant.allowBookingsToday ? '+0d' : '+1d',
            todayHighlight: true,
            autoclose: true,
            
            format: 'dd.mm.yyyy'
            
        });
        
        element.find('#aa_date_selection_earliest_possible_date').on('changeDate', function() {
            
            AppointmentAssistant.loadAppointmentProposals();
            
        });
        
        element.find('#aa_date_selection_earliest_possible_time').blur(function() {
            
            AppointmentAssistant.loadAppointmentProposals();
            
        });
        
        element.find('#aa_date_selection_earliest_possible_time').keypress(function(e) {
            
            if(e.which == 13)
            {
                
                AppointmentAssistant.loadAppointmentProposals();
                
            }
            
        });
        
        element.find("#aa_date_selection_earliest_possible_time").inputmask('h:s', {placeholder: 'hh:mm'});
        
    },
    
    closeModal: function()
    {
        
        PerisianBaseAjax.cancel();
        
    },
    
    /**
     * Shows details for the bookable time with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int bookableTimeId
     * @return void
     */
    showBookableTimeDetails: function(bookableTimeId)
    {
        
        var sendData = {
            
            'do': 'bookableTimeInfo',
            'i': bookableTimeId
            
        };
        
        PerisianBaseAjax.loadInModal(AppointmentAssistant.addressAjaxAppointmentProposals, sendData);
        
    },
    
    /**
     * Displays a warning that the selected amount of bookings is higher than the amount
     * of bookings available.
     * 
     * @author Peter Hamm
     * @param int amountBookable
     * @param int amountOfBookingsSelected
     * @returns void
     */
    showBookableAmountWarning: function(amountBookable, amountOfBookingsSelected)
    {
        
        var sendData = {
            
            'do': 'bookableAmountWarning',
            'amountBookable': amountBookable,
            'amountOfBookingsSelected': amountOfBookingsSelected
            
        };
        
        PerisianBaseAjax.loadInModal(AppointmentAssistant.addressAjaxAppointmentProposals, sendData);
        
    },
    
    /**
     * Loads proposals for appointments for the currently selected user (if enabled), 
     * earliest date and area.
     * 
     * @author Peter Hamm
     * @param startDate Timestamp, optional. If left empty, the picked timestamp from the fields is taken.
     * @returns void
     */
    loadAppointmentProposals: function(startDate, forceReload) 
    {
                
        var element = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var selectedUser = element.find('#user_backend option:selected').val();
        var selectedArea = element.find('input[type=radio][name="area"]:checked').val();
        var timestampFromFields = Calendar.getTimestampFromFields("#aa_date_selection_earliest_possible_date", "#aa_date_selection_earliest_possible_time");
        var selectedStartDate = startDate ? startDate : timestampFromFields;
        var amountOfBookings = AppointmentAssistant.getAmountMultipleBooking();
        
        var proposalSelector = element.find('#appointment_proposal');
        
        var loadingIndicator = '<div class="loading-indicator" style="width: 50%; height: 160px"></div>';
        
        if(forceReload || proposalSelector.attr('user_backend') != selectedUser || proposalSelector.attr('area') != selectedArea || proposalSelector.attr('begin') != selectedStartDate)
        {
            
            // Something changed, load new proposals
            
            proposalSelector.attr('user_backend', selectedUser);
            proposalSelector.attr('area', selectedArea);
            proposalSelector.attr('begin', selectedStartDate);
            
            var sendData = {
                
                'u': selectedUser,
                'a': selectedArea,
                't': isNaN(selectedStartDate) ? 0 : selectedStartDate,
                'amountOfBookings': amountOfBookings
                
            };
            
            AppointmentAssistant.currentQueryProposal = jQuery.ajax({

                type: 'GET',
                async: true,
                timeout: 30000,
                url: AppointmentAssistant.addressAjaxAppointmentProposals,
                data: sendData, 
                
                beforeSend: function() 
                {       
                    
                    if(AppointmentAssistant.currentQueryProposal != null) 
                    {
                        
                        AppointmentAssistant.currentQueryProposal.abort();
                        AppointmentAssistant.currentQueryProposal = null;
                        
                    }
                    
                    proposalSelector.empty();
                    
                    proposalSelector.append(loadingIndicator);
                    
                },
                
                error: function (xhr, textStatus, errorThrown) 
                {
                                        
                    if(textStatus == 'timeout')
                    {
                        
                        AppointmentAssistant.loadAppointmentProposals(startDate, true);
                        
                    }
                    
                },

                success: function(result) 
                {
            
                    var resultData = JSON.parse(result);
                    
                    proposalSelector.empty();
                    
                    if(resultData.proposals && Array.isArray(resultData.proposals))
                    {
                        
                        for(var i = 0; i < resultData.proposals.length; ++i)
                        {
                            
                            var isTimeDisabled = false;
                            
                            var infoButton = '';
                            
                            if(resultData.proposals[i] && resultData.proposals[i]['flags'] && resultData.proposals[i]['flags']['bookable_time_public_annotations_enabled'] == "1")
                            {
                                
                                infoButton = ' <a class="btn btn-default btn-primary" style="padding: 0; margin-left: 6px; height: 15px; padding-left: 4px; padding-right: 4px;" href="javascript:AppointmentAssistant.showBookableTimeDetails(' + resultData.proposals[i]['flags']['bookable_time_id'] + ');"><i class="fa fa-info-circle" style="position: relative; top: -6px;"></i><span style="font-size: 12px; position: relative; top: -7px; padding-left: 4px;">info</span></a>';
                                
                            }
                            
                            var tooManyBookings = '';
                            
                            if(resultData.proposals[i] && resultData.proposals[i]['flags'] && resultData.proposals[i]['flags']['bookable_time_number_bookable'] < sendData.amountOfBookings)
                            {
                                
                                tooManyBookings = ' <a class="btn btn-default btn-primary" style="padding: 0; margin-left: 6px; height: 15px; padding-left: 4px; padding-right: 4px;" href="javascript:AppointmentAssistant.showBookableAmountWarning(' + resultData.proposals[i]['flags']['bookable_time_number_bookable'] + ', ' + sendData.amountOfBookings + ');"><i class="fa fa-ban" style="position: relative; top: -6px;"></i><span style="font-size: 12px; position: relative; top: -7px; padding-left: 4px;">' + Language.getLanguageVariable('p5e49b227d18a1') + '</span></a>';
                                
                                isTimeDisabled = true;
                                
                            }
                            
                            var isSelected = false;
                            
                            if(AppointmentAssistant.datePreselected != -1)
                            {
                                
                                if(parseInt(resultData.proposals[i]['begin']) == parseInt(AppointmentAssistant.datePreselected))
                                {
                                    
                                    isSelected = true;
                                    
                                }
                                
                            }

                            var option = '<label class="radio-styled radio-primary">';
                            option += '<input' + (isTimeDisabled ? ' disabled' : '') + ' type="radio" name="aa_appointment_proposal" id="aa_appointment_proposal"' + (isSelected ? ' checked' : '') + ' value="' + resultData.proposals[i]['begin'] + '" end="' + resultData.proposals[i]['end'] + '"/>';
                            option += '<span>' + resultData.proposals[i]['label'] + infoButton + tooManyBookings + '</span>';
                            option += "</label>";
                            option += '<br/>';

                            proposalSelector.append(option);

                        }
                             
                    }
                    else
                    {
                                                
                        if(!resultData.success && resultData['display_message'])
                        {

                            proposalSelector.append('<span class="alert alert-warning">' + resultData.message + '</span>');

                        }
                        
                    }
                    
                    if(selectedStartDate != timestampFromFields)
                    {

                        var buttonBegin = jQuery('<button/>').addClass('btn btn-raised aa_button_date_navigation_begin').text(Language.getLanguageVariable(11058));

                        buttonBegin.click(function() {

                            AppointmentAssistant.loadAppointmentProposals(timestampFromFields);

                        });

                        proposalSelector.append(buttonBegin);

                    }
                    
                    if(!resultData['display_message'])
                    {
                        
                        // Display pagination buttons
                        
                        var buttonLater = jQuery('<button/>').addClass('btn btn-raised aa_button_date_navigation_next').text(Language.getLanguageVariable(11057));

                        buttonLater.click(function() {
                            
                            /*
                            var lastEntry = resultData.proposals[resultData.proposals.length - 1];
                            
                            var timezoneOffsetInSeconds = AppointmentAssistant.getTimezoneOffsetInSeconds();
                            var nextPageTimestamp = parseInt(lastEntry['begin']) + parseInt(lastEntry['distance']) - timezoneOffsetInSeconds;
                            */
                           
                            var nextPageTimestamp = resultData.time_next;

                            AppointmentAssistant.loadAppointmentProposals(nextPageTimestamp);

                        });

                        proposalSelector.append(buttonLater);
                        
                    }

                }

            });
            
        }
        
    },
    
    /**
     * Retrieves the timezone offset of the browser, in seconds.
     * 
     * @author Peter Hamm
     * @returns int
     */
    getTimezoneOffsetInSeconds: function() 
    {
        
        var offset = new Date().getTimezoneOffset() * 60;
        
        return offset;
        
    },
    
    /**
     * Loads all possible users for the specified area,
     * if the user selection is enabled.
     * 
     * @param int areaId
     * @returns void
     */
    loadUsersForArea: function(areaId) 
    {
        
        var selectElement = jQuery('#' + AppointmentAssistant.containerIdentifier).find('#user_backend');
        
        if(selectElement.length == 0)
        {
            
            return;
            
        }
        
        // Remove all previously available options and add a default one.
        {
            
            selectElement.empty();
            
            var option = '<option value="0" selected>' + Language.getLanguageVariable(11039) + '</option>';

            selectElement.append(option);
            
        }
        
        var sendData = {

            'a': areaId

        };

        jQuery.ajax({

            type: 'GET',
            async: true,
            timeout: 5000,
            url: AppointmentAssistant.addressAjaxUsersForArea,
            data: sendData, 

            success: function(result) {

                var resultArray = JSON.parse(result);
                
                for(var i = 0; i < resultArray.length; ++i)
                {
                    
                    var style = "";
                    
                    if(resultArray[i]['user_image_profile'].length > 0)
                    {
                        
                        style += 'background-image: url(\'' + resultArray[i]['user_image_profile'] + '\');';
                    
                    }
                    
                    {
                        
                        style += 'background-color: ' + resultArray[i]['user_color'] + ';';
                    
                    }
                                    
                    var option = '<option value="' + resultArray[i]['user_id'] + '" style="' + style + '" class="user-backend-option">' + resultArray[i]['user_fullname'] + '</option>';

                    selectElement.append(option);
                    
                }
                
                AppointmentAssistant.updateUserSelector();

            }

        });
        
    },
    
    /**
     * Initializes the switch where the user can choose to log in or register.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initLoginRegistrationSwitch: function()
    {
        
        jQuery('#' + AppointmentAssistant.containerIdentifier).find('#registration_login_selector input[type=radio]').each(function() {
            
            var radio = jQuery(this);
            
            radio.change(function() {

                var element = jQuery(this);

                if(element.is(":checked")) 
                {

                    jQuery('#aa_login').css('display', element.val() == 1 ? 'block' : 'none');
                    jQuery('#aa_registration').css('display', element.val() == 1 ? 'none' : 'block');

                }
                
                AppointmentAssistant.toggleError();
                AppointmentAssistant.unHighlightAll(PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier));

            });
        
        });
        
    },
    
    /**
     * Sets the content of the success box to the value specified.
     * 
     * @author Peter Hamm
     * @param String contentString
     * @returns void
     */
    setSuccessBoxContent: function(contentString)
    {
        
        jQuery('#' + AppointmentAssistant.successBoxIdentifier).find('#success-box-content').html(contentString);
        
    },
    
    /**
     * Sets the content of the warning box to the valuespecified
     * 
     * @author Peter Hamm
     * @param String contentString
     * @returns void
     */
    setWarningBoxContent: function(contentString)
    {
        
        jQuery('#' + AppointmentAssistant.containerIdentifier).find('#' + AppointmentAssistant.warningBoxIdentifier).find('#warning-box-content').html(contentString);
        
    },
    
    /**
     * Navigates to the specified tab.
     * Validates everything on the way there - if the validation fails on the way, the affected tab is displayed.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @returns void
     */
    navigateTo: function(tabIndex)
    {
        
        jQuery('#aa-page-link-container').css('display', 'none');
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        var indicatorBoxElement = containerElement.find('#loading-box');
        
        if(indicatorBoxElement.css('display') == 'block')
        {
            
            return;
            
        }
        
        for(var i = 0; i < tabIndex; ++i)
        {
            
            if(!AppointmentAssistant.validateTab(i))
            {
                
                AppointmentAssistant.toggleError(true);

                PerisianFormPager.navigateTo(containerElement, i);
                
                return;

            }

        }  
        
        AppointmentAssistant.toggleError();
                
        PerisianFormPager.navigateTo(containerElement, tabIndex);
        
        AppointmentAssistant.onTabSwitched(tabIndex);
        
    },
    
    'onTabSwitched': function(tabIndex)
    {
        
        var activeTab = jQuery('.form-page.active');
        
        var dateField = activeTab.find('#aa_date_selection_earliest_possible_date');
        
        if(dateField.length > 0)
        {
            
            if(dateField.attr('data-timestamp-earliest') != "-1")
            {
                
                AppointmentAssistant.datePreselected = dateField.attr('data-timestamp-earliest');
                
            }
            
        }
        
    },
    
    /**
     * Toggles the success box
     * 
     * @author Peter Hamm
     * @param bool show Set this to true to show the box. Default: false
     * @returns void
     */
    toggleSuccess: function(show)
    {
        
        jQuery('#' + AppointmentAssistant.successBoxIdentifier).css('display', show ? 'block' : 'none');  
        jQuery('#' + AppointmentAssistant.containerIdentifier).css('display', !show ? 'block' : 'none');  
        
    },
    
    /**
     * Toggles the error box
     * 
     * @author Peter Hamm
     * @param bool show Set this to true to show the box. Default: false
     * @returns void
     */
    toggleError: function(show)
    {
        
        jQuery('#' + AppointmentAssistant.containerIdentifier).find('#' + AppointmentAssistant.warningBoxIdentifier).css('display', show ? 'block' : 'none');  
        
    },
    
    /**
     * Does a validation check on the current tab and goes to the next page.
     * 
     * @author Peter Hamm
     * @returns void
     */
    navigateNext: function() 
    {
        
        var currentTabIndex = PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier);
        
        var validationOkay = AppointmentAssistant.validateTab(currentTabIndex);
        
        AppointmentAssistant.toggleError(!validationOkay);
        
        if(validationOkay)
        {
            
            PerisianFormPager.navigateNext(AppointmentAssistant.containerIdentifier);
            
            jQuery('#aa-page-link-container').css('display', 'none');
        
        }
        
    },
    
    /**
     * Navigates back to the previous tab.
     * 
     * @author Peter Hamm
     * @returns void
     */
    navigatePrevious: function() 
    {
        
        AppointmentAssistant.toggleError(false);
        
        var currentTabIndex = PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier);
        
        AppointmentAssistant.unHighlightAll(currentTabIndex);
        
        PerisianFormPager.navigatePrevious(AppointmentAssistant.containerIdentifier);
        
        jQuery('#aa-page-link-container').css('display', 'none');
        
    },
    
    /**
     * Sends the data of the appointment assistant and checks back for any callbacks.
     * 
     * @author Peter Hamm
     * @returns void
     */
    confirmAndSend: function()
    {
        
        AppointmentAssistant.toggleError(false);
        AppointmentAssistant.toggleLoadingIndicator(true);
        
        var sendData = AppointmentAssistant.getSendData();
        
        jQuery.post(AppointmentAssistant.addressAjaxCreate, sendData, function(result){
            
            var resultObject = {
                
                'success': false
                
            };
            
            try
            {
                
                var resultObject = JSON.parse(result);
                
            }
            catch(e)
            {
                
                // The response of the server was invalid.
                
                resultObject.message = Language.getLanguageVariable('p59e75ad19929b');
                                
            }
            
            if(resultObject.success)
            {
                
                AppointmentAssistant.setSuccessBoxContent(resultObject.message);
                AppointmentAssistant.toggleSuccess(true);
                
                if(resultObject['payment_url'] && resultObject['payment_url'].length > 0)
                {
                    
                    jQuery('#btn-success-payment').attr('href', resultObject['payment_url']);
                    
                }
                
            }
            else
            {
                
                if(resultObject.e)
                {
                    
                    console.error(resultObject.e);
                    
                }
                
                AppointmentAssistant.setWarningBoxContent(resultObject.message);
                
                setTimeout(function() {
                   
                    AppointmentAssistant.toggleError(true);
                    AppointmentAssistant.toggleLoadingIndicator(false);
                    
                }, 1000);
                
            }
            
        });
        
    },
    
    /**
     * Validates a single field.
     * 
     * @author Peter Hamm
     * @param Object element
     * @return bool
     */
    validateField: function(element, recursive)
    {
                
        var containerBox = jQuery('[id="field_' + element.attr('id') + '"]');
        
        if(!recursive && containerBox.length && containerBox.length > 1)
        {
            
            // A field with this identifier exists multiple times, we need to handle it specially
                        
            for(var i = 0; i < containerBox.length; ++i)
            {
             
                element = jQuery(containerBox[i]).find(AppointmentAssistant.validationFieldTypes)[0];
                element = jQuery(element);
                
                if(this.validateField(element, true))
                {
                    
                    // One of the multiple fields meets the conditions, it must be true then.
                    
                    return true;
                    
                }
                                
            }
            
            // None of the multiple fields meet the conditions.
            
            return false;
            
            
        }
        else if(containerBox.length && containerBox.length == 1)
        {
            
            element = jQuery(element[0]);
            
        }
        else
        {
            
            element = jQuery(element);
            
        }
                
        var fieldValidationOkay = true;

        if(containerBox.css('display') == 'none')
        {

            // This field isn't displayed, don't validate it
            return true;

        }
        
        if(element.prop('required') && containerBox.css('display') != 'none')
        {

            if(element.attr('type') == 'radio')
            {

                // Find out if any radio of this identifier is checked.

                var checked = jQuery('#' + element.attr('id') + ':checked').length > 0;

                if(!checked)
                {

                    fieldValidationOkay = false;

                }

            }
            else if(element.is('select'))
            {

                // Find out if any option is selected

                var selected = jQuery('#' + element.attr('id') + ' option:selected').val() != '__none';

                if(!selected)
                {

                    fieldValidationOkay = false;

                }

            }
            else if(element.attr('type') == 'checkbox')
            {

                // Find out if any radio of this identifier is checked.

                var checked = jQuery('#' + element.attr('id') + ':checked').length > 0;

                if(!checked)
                {

                    fieldValidationOkay = false;

                }

            }
            else
            {

                if(!element.val())
                {

                    fieldValidationOkay = false;

                }

            }

        }
        
        return fieldValidationOkay;
        
    },
    
    /**
     * Validates the tab with the specified index.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @returns bool
     */
    validateTab: function(tabIndex)
    {
                
        AppointmentAssistant.unHighlightAll(tabIndex);
        
        var validationOkay = true;
        
        var tab = PerisianFormPager.getTab(AppointmentAssistant.containerIdentifier, tabIndex);
        
        var requiredFields = tab.find(AppointmentAssistant.validationFieldTypes);

        requiredFields.each(function() {
            
            var element = jQuery(this);
            
            var fieldValidationOkay = AppointmentAssistant.validateField(element);
            
            if(!fieldValidationOkay)
            {
                
                AppointmentAssistant.highlightField(tabIndex, element.attr('id'));

                validationOkay = false;

            }

        });
        
        if(!validationOkay)
        {
            
            // Error: Please fill out the required fields
            AppointmentAssistant.setWarningBoxContent(Language.getLanguageVariable(11029));
            
        }
        
        if(validationOkay && tabIndex == 3)
        {
            
            validationOkay = AppointmentAssistant.checkLoginRegistration();
            
        }
        
        if(validationOkay && tabIndex == 2)
        {
                        
            validationOkay = AppointmentAssistant.checkAppointmentSelection();
            
        }
        
        return validationOkay;
        
    },
    
    /**
     * Retrieves the values for the specified list of fields.
     * 
     * @author Peter Hamm
     * @param Object fieldList
     * @returns Object
     */
    getValuesForFields: function(fieldList)
    {
        
        var valueList = {};
        
        jQuery.each(fieldList, function(key, value) {

            var fieldValue = AppointmentAssistant.getFieldValue(value);
            
            valueList[key] = fieldValue;

        });
        
        return valueList;
        
    },
    
    /**
     * Checks if a valid appointment proposal selection has been made.
     * 
     * @author Peter Hamm
     * @returns bool
     */
    checkAppointmentSelection: function()
    {
        
        var isValid = true;
        
        var errors = [];
        
        var containerElement = jQuery('#' + AppointmentAssistant.containerIdentifier);
        
        if(containerElement.find('#aa_appointment_proposal:checked').length == 0)
        {
            
            isValid = false;

            errors.push(Language.getLanguageVariable(11042));

            AppointmentAssistant.highlightField(PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier), 'appointment_proposal');
            
        }
        
        if(!isValid)
        {
            
            AppointmentAssistant.setWarningBoxContent(errors.join(" "));
            
        }
        
        return isValid;
        
    },
    
    /**
     * Callback function to highlight errors.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @param String field
     * @returns void
     */
    hightlightError: function(tabIndex, field)
    {
        
        AppointmentAssistant.highlightField(tabIndex, field);
        
    },
    
    /**
     * Checks (depending on what is selected) if either the registration or the login are valid.
     * 
     * @author Peter Hamm
     * @returns bool
     */
    checkLoginRegistration: function(highlightErrorFunction) 
    { 
        
        if(!highlightErrorFunction || highlightErrorFunction == null)
        {
            
            highlightErrorFunction = AppointmentAssistant.hightlightError;
            
        }
     
        var isValid = true;
        var isLogin = jQuery('#registration_login_selector input[type=radio]:checked').val() == 1;
        
        var currentTabIndex = PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier);
        
        var errors = [];
        
        if(isLogin)
        {
            
            // Login
            
            var loginFields = AppointmentAssistant.fieldsLogin;
            var loginValues = AppointmentAssistant.getValuesForFields(loginFields);
            
            // Email validation
            {
                                
                if(!Validation.checkEmail(loginValues['email']))
                {
                    
                    isValid = false;
                    
                    errors.push(Language.getLanguageVariable(10094));
                    
                    highlightErrorFunction(currentTabIndex, loginFields['email']);
                    
                }
                
            }
            
            // Password validation
            {
                
                if(!Validation.checkRequired(loginValues['password']))
                {
                    
                    isValid = false;
                    
                    errors.push(Language.getLanguageVariable(11038));
                    
                    highlightErrorFunction(currentTabIndex, loginFields['password']);
                    
                }
                
            }
            
            if(isValid)
            {
                
                var sendData = {
                    
                    'do': 'cl',
                    'm': loginValues['email'],
                    'p': loginValues['password']
                    
                };
                
                jQuery.ajax({

                    type: 'GET',
                    async: false,
                    timeout: 5000,
                    url: AppointmentAssistant.addressRegistration,
                    data: sendData, 

                    success: function(result) {

                        var resultObject = JSON.parse(result);

                        isValid = resultObject.result == 1;

                        if(!isValid)
                        {

                            errors.push(resultObject.message);

                            highlightErrorFunction(currentTabIndex, loginFields['email']);
                            highlightErrorFunction(currentTabIndex, loginFields['password']);

                        }

                    }

                });
                
            }
            
        }
        else
        {
            
            // Registration
            
            var registrationFields = AppointmentAssistant.fieldsRegistration;
            var registrationValues = AppointmentAssistant.getValuesForFields(registrationFields);
            
            // Email validation
            {
                
                if(!Validation.checkEmail(registrationValues['emailOne']) || !Validation.checkEmail(registrationValues['emailTwo']) || registrationValues['emailOne'] != registrationValues['emailTwo'])
                {
                    
                    isValid = false;
                    
                    errors.push(Language.getLanguageVariable(11035));
                    
                    highlightErrorFunction(currentTabIndex, registrationFields['emailOne']);
                    highlightErrorFunction(currentTabIndex, registrationFields['emailTwo']);
                    
                }
                
            }
            
            // Password validation
            {
                
                var passwordResult = {};
                
                if(!Validation.checkPassword(registrationValues['passwordOne'], registrationValues['passwordTwo'], null, passwordResult))
                {
                    
                    isValid = false;
                    
                    errors.push(passwordResult.message);
                    
                    highlightErrorFunction(currentTabIndex, registrationFields['passwordOne']);
                    highlightErrorFunction(currentTabIndex, registrationFields['passwordTwo']);
                    
                }
                
            }
            
            // The rest of the fields
            {
                
                var isRestValid = true;
                
                if(!Validation.checkRequired(registrationValues['nameFirst']))
                {
                    
                    isRestValid = false;
                    
                    highlightErrorFunction(currentTabIndex, registrationFields['nameFirst']);
                    
                }
                
                if(!Validation.checkRequired(registrationValues['nameSecond']))
                {
                    
                    isRestValid = false;
                    
                    highlightErrorFunction(currentTabIndex, registrationFields['nameSecond']);
                    
                }
                
                if(!Validation.checkRequired(registrationValues['phone']))
                {
                    
                    isRestValid = false;
                    
                    highlightErrorFunction(currentTabIndex, registrationFields['phone']);
                    
                }
                
                if(!isRestValid)
                {
                    
                    errors.push(Language.getLanguageVariable(11029));
                    
                    isValid = false;
                    
                }
                
                if(jQuery('#registration_gtc:checked').length == 0)
                {
                    
                    errors.push(Language.getLanguageVariable(11065));
                    
                    highlightErrorFunction(currentTabIndex, 'registration_gtc');
                    
                    isValid = false;
                    
                }
                
            }
            
            // Check if the e-mail address is already registered
            if(isValid)
            {
                
                var sendData = {
                    
                    'do': 'cr',
                    'm': registrationValues['emailOne']
                    
                };
                
                jQuery.ajax({
                    
                    type: 'GET',
                    async: false,
                    timeout: 5000,
                    url: AppointmentAssistant.addressRegistration,
                    data: sendData, 
                    
                    success: function(result) {
                    
                        var resultObject = JSON.parse(result);

                        isValid = resultObject.result == 1;
                        
                        if(!isValid)
                        {
                            
                            errors.push(resultObject.message);

                            highlightErrorFunction(currentTabIndex, registrationFields['emailOne']);
                            highlightErrorFunction(currentTabIndex, registrationFields['emailTwo']);
                            
                        }

                    }
                    
                });
                
            }
            
        }
        
        if(!isValid)
        {
            
            AppointmentAssistant.setWarningBoxContent(errors.join(" "));
            
        }
        
        return isValid;
        
    },
    
    /**
     * Highlights the field with the specified identifier on the tab with the specified tab index.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @param String fieldIdentifier
     * @returns void
     */
    highlightField: function(tabIndex, fieldIdentifier)
    {
        
        if(!fieldIdentifier)
        {
            
            console.error('Invalid identifier ' + tabIndex + ' / ' + fieldIdentifier);
            
            return;
            
        }
        
        fieldIdentifier = fieldIdentifier.replace('#', '');
        
        var tab = PerisianFormPager.getTab(AppointmentAssistant.containerIdentifier, tabIndex);
        
        var field = tab.find('[id="' + fieldIdentifier + '"]');
        
        var fieldContainer = field.closest('.form-group');
        
        fieldContainer.addClass('form-pager-highlight');
        
    },
    
    /**
     * Unhighlights all fields for the specified tab index.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @returns void
     */
    unHighlightAll: function(tabIndex)
    {
        
        var tab = PerisianFormPager.getTab(AppointmentAssistant.containerIdentifier, tabIndex);
        
        var fields = tab.find('.form-pager-highlight');
        
        fields.removeClass('form-pager-highlight');
        
    },
    
    /**
     * Displays the terms and conditions in a modal window.
     * 
     * @author Peter Hamm
     * @returns void
     */
    showGtc: function() 
    {

        jQuery('#modal-gtc').modal('show');

    },
    
    /**
     * Retrieves the link to the current page within the appointment assistant.
     * 
     * @author Peter Hamm
     * @returns void
     */
    getTabLink: function()
    {
        
        jQuery('#aa-page-link-container').css('display', 'block');
        
        var currentTabIndex = PerisianFormPager.getCurrentIndex(AppointmentAssistant.containerIdentifier);
        var formData = AppointmentAssistant.getSendData();
        var relevantData = formData.appointment;
        
        delete(relevantData.time);
        //delete(relevantData.area);
        
        var parameters = {
            
            'do': 'aa',
            'action': 'jump',
            'tabIndex': currentTabIndex,
            'data': relevantData
            
        };
        
        var pageLink = baseUrl + '?' + AppointmentAssistant.serializeParameters(parameters);
        
        jQuery('#aa-page-link').val(pageLink);
        
    },
    
    /**
     * Serializes the parameters from the object to be able to add it to an URL.
     * 
     * @author Peter Hamm
     * @param Object object
     * @param String prefix
     * @returns String
     */
    serializeParameters: function(object, prefix) 
    {
        
        var str = [];
        
        for(var p in object) 
        {
            
            if(object.hasOwnProperty(p)) 
            {
                
                var k = prefix ? prefix + "[" + p + "]" : p;
                var v = object[p];
                
                if(typeof v === 'undefined')
                {
                    
                    continue;
                    
                }

                str.push(typeof v == "object" ? AppointmentAssistant.serializeParameters(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));

            }
          
        }
        
        return str.join("&");
        
    },
    
    /**
     * Initializes the functionality for multiple bookings.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initMultipleBookings: function()
    {
        
        jQuery('#multiple-booking-amount').change(function() {
            
            var selectedAmount = parseInt(jQuery('#multiple-booking-amount :selected').val());
                        
            var dummy = jQuery('#multiple-booking-dummy').html();
            
            jQuery('#multiple-booking-title-container').css('display', selectedAmount > 1 ? 'block' : 'none');
                       
            while(jQuery('#multiple-booking-title-container .multiple-booking-title-element').length > selectedAmount)
            {
                
                var elementLength = jQuery('#multiple-booking-title-container .multiple-booking-title-element').length;
                
                var element = jQuery(jQuery('#multiple-booking-title-container .multiple-booking-title-element')[elementLength - 1]);
                
                element.remove();
                
            }
            
            for(var i = jQuery('#multiple-booking-title-container .multiple-booking-title-element').length; i < selectedAmount; ++i)
            {
                
                var newDummy = dummy;
                newDummy = newDummy.replace('%bookingNumber', (i + 1));
                
                jQuery('#multiple-booking-title-container').append(newDummy);
                
            }
            
            AppointmentAssistant.loadAppointmentProposals(null, true);
            
        });
        
    }
    
};
