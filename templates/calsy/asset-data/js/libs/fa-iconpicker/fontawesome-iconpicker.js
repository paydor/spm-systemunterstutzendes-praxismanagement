jQuery(document).ready(function() {
    
    (function(a, b) {
        a.ui = a.ui || {};
        var c, d = Math.max, e = Math.abs, f = Math.round, g = /left|center|right/, h = /top|center|bottom/, i = /[\+\-]\d+(\.[\d]+)?%?/, j = /^\w+/, k = /%$/, l = a.fn.pos;
        function m(a, b, c) {
            return [ parseFloat(a[0]) * (k.test(a[0]) ? b / 100 : 1), parseFloat(a[1]) * (k.test(a[1]) ? c / 100 : 1) ];
        }
        function n(b, c) {
            return parseInt(a.css(b, c), 10) || 0;
        }
        function o(b) {
            var c = b[0];
            if (c.nodeType === 9) {
                return {
                    width: b.width(),
                    height: b.height(),
                    offset: {
                        top: 0,
                        left: 0
                    }
                };
            }
            if (a.isWindow(c)) {
                return {
                    width: b.width(),
                    height: b.height(),
                    offset: {
                        top: b.scrollTop(),
                        left: b.scrollLeft()
                    }
                };
            }
            if (c.preventDefault) {
                return {
                    width: 0,
                    height: 0,
                    offset: {
                        top: c.pageY,
                        left: c.pageX
                    }
                };
            }
            return {
                width: b.outerWidth(),
                height: b.outerHeight(),
                offset: b.offset()
            };
        }
        a.pos = {
            scrollbarWidth: function() {
                if (c !== b) {
                    return c;
                }
                var d, e, f = a("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), g = f.children()[0];
                a("body").append(f);
                d = g.offsetWidth;
                f.css("overflow", "scroll");
                e = g.offsetWidth;
                if (d === e) {
                    e = f[0].clientWidth;
                }
                f.remove();
                return c = d - e;
            },
            getScrollInfo: function(b) {
                var c = b.isWindow || b.isDocument ? "" : b.element.css("overflow-x"), d = b.isWindow || b.isDocument ? "" : b.element.css("overflow-y"), e = c === "scroll" || c === "auto" && b.width < b.element[0].scrollWidth, f = d === "scroll" || d === "auto" && b.height < b.element[0].scrollHeight;
                return {
                    width: f ? a.pos.scrollbarWidth() : 0,
                    height: e ? a.pos.scrollbarWidth() : 0
                };
            },
            getWithinInfo: function(b) {
                var c = a(b || window), d = a.isWindow(c[0]), e = !!c[0] && c[0].nodeType === 9;
                return {
                    element: c,
                    isWindow: d,
                    isDocument: e,
                    offset: c.offset() || {
                        left: 0,
                        top: 0
                    },
                    scrollLeft: c.scrollLeft(),
                    scrollTop: c.scrollTop(),
                    width: d ? c.width() : c.outerWidth(),
                    height: d ? c.height() : c.outerHeight()
                };
            }
        };
        a.fn.pos = function(b) {
            if (!b || !b.of) {
                return l.apply(this, arguments);
            }
            b = a.extend({}, b);
            var c, k, p, q, r, s, t = a(b.of), u = a.pos.getWithinInfo(b.within), v = a.pos.getScrollInfo(u), w = (b.collision || "flip").split(" "), x = {};
            s = o(t);
            if (t[0].preventDefault) {
                b.at = "left top";
            }
            k = s.width;
            p = s.height;
            q = s.offset;
            r = a.extend({}, q);
            a.each([ "my", "at" ], function() {
                var a = (b[this] || "").split(" "), c, d;
                if (a.length === 1) {
                    a = g.test(a[0]) ? a.concat([ "center" ]) : h.test(a[0]) ? [ "center" ].concat(a) : [ "center", "center" ];
                }
                a[0] = g.test(a[0]) ? a[0] : "center";
                a[1] = h.test(a[1]) ? a[1] : "center";
                c = i.exec(a[0]);
                d = i.exec(a[1]);
                x[this] = [ c ? c[0] : 0, d ? d[0] : 0 ];
                b[this] = [ j.exec(a[0])[0], j.exec(a[1])[0] ];
            });
            if (w.length === 1) {
                w[1] = w[0];
            }
            if (b.at[0] === "right") {
                r.left += k;
            } else if (b.at[0] === "center") {
                r.left += k / 2;
            }
            if (b.at[1] === "bottom") {
                r.top += p;
            } else if (b.at[1] === "center") {
                r.top += p / 2;
            }
            c = m(x.at, k, p);
            r.left += c[0];
            r.top += c[1];
            return this.each(function() {
                var g, h, i = a(this), j = i.outerWidth(), l = i.outerHeight(), o = n(this, "marginLeft"), s = n(this, "marginTop"), y = j + o + n(this, "marginRight") + v.width, z = l + s + n(this, "marginBottom") + v.height, A = a.extend({}, r), B = m(x.my, i.outerWidth(), i.outerHeight());
                if (b.my[0] === "right") {
                    A.left -= j;
                } else if (b.my[0] === "center") {
                    A.left -= j / 2;
                }
                if (b.my[1] === "bottom") {
                    A.top -= l;
                } else if (b.my[1] === "center") {
                    A.top -= l / 2;
                }
                A.left += B[0];
                A.top += B[1];
                if (!a.support.offsetFractions) {
                    A.left = f(A.left);
                    A.top = f(A.top);
                }
                g = {
                    marginLeft: o,
                    marginTop: s
                };
                a.each([ "left", "top" ], function(d, e) {
                    if (a.ui.pos[w[d]]) {
                        a.ui.pos[w[d]][e](A, {
                            targetWidth: k,
                            targetHeight: p,
                            elemWidth: j,
                            elemHeight: l,
                            collisionPosition: g,
                            collisionWidth: y,
                            collisionHeight: z,
                            offset: [ c[0] + B[0], c[1] + B[1] ],
                            my: b.my,
                            at: b.at,
                            within: u,
                            elem: i
                        });
                    }
                });
                if (b.using) {
                    h = function(a) {
                        var c = q.left - A.left, f = c + k - j, g = q.top - A.top, h = g + p - l, m = {
                            target: {
                                element: t,
                                left: q.left,
                                top: q.top,
                                width: k,
                                height: p
                            },
                            element: {
                                element: i,
                                left: A.left,
                                top: A.top,
                                width: j,
                                height: l
                            },
                            horizontal: f < 0 ? "left" : c > 0 ? "right" : "center",
                            vertical: h < 0 ? "top" : g > 0 ? "bottom" : "middle"
                        };
                        if (k < j && e(c + f) < k) {
                            m.horizontal = "center";
                        }
                        if (p < l && e(g + h) < p) {
                            m.vertical = "middle";
                        }
                        if (d(e(c), e(f)) > d(e(g), e(h))) {
                            m.important = "horizontal";
                        } else {
                            m.important = "vertical";
                        }
                        b.using.call(this, a, m);
                    };
                }
                i.offset(a.extend(A, {
                    using: h
                }));
            });
        };
        a.ui.pos = {
            _trigger: function(a, b, c, d) {
                if (b.elem) {
                    b.elem.trigger({
                        type: c,
                        position: a,
                        positionData: b,
                        triggered: d
                    });
                }
            },
            fit: {
                left: function(b, c) {
                    a.ui.pos._trigger(b, c, "posCollide", "fitLeft");
                    var e = c.within, f = e.isWindow ? e.scrollLeft : e.offset.left, g = e.width, h = b.left - c.collisionPosition.marginLeft, i = f - h, j = h + c.collisionWidth - g - f, k;
                    if (c.collisionWidth > g) {
                        if (i > 0 && j <= 0) {
                            k = b.left + i + c.collisionWidth - g - f;
                            b.left += i - k;
                        } else if (j > 0 && i <= 0) {
                            b.left = f;
                        } else {
                            if (i > j) {
                                b.left = f + g - c.collisionWidth;
                            } else {
                                b.left = f;
                            }
                        }
                    } else if (i > 0) {
                        b.left += i;
                    } else if (j > 0) {
                        b.left -= j;
                    } else {
                        b.left = d(b.left - h, b.left);
                    }
                    a.ui.pos._trigger(b, c, "posCollided", "fitLeft");
                },
                top: function(b, c) {
                    a.ui.pos._trigger(b, c, "posCollide", "fitTop");
                    var e = c.within, f = e.isWindow ? e.scrollTop : e.offset.top, g = c.within.height, h = b.top - c.collisionPosition.marginTop, i = f - h, j = h + c.collisionHeight - g - f, k;
                    if (c.collisionHeight > g) {
                        if (i > 0 && j <= 0) {
                            k = b.top + i + c.collisionHeight - g - f;
                            b.top += i - k;
                        } else if (j > 0 && i <= 0) {
                            b.top = f;
                        } else {
                            if (i > j) {
                                b.top = f + g - c.collisionHeight;
                            } else {
                                b.top = f;
                            }
                        }
                    } else if (i > 0) {
                        b.top += i;
                    } else if (j > 0) {
                        b.top -= j;
                    } else {
                        b.top = d(b.top - h, b.top);
                    }
                    a.ui.pos._trigger(b, c, "posCollided", "fitTop");
                }
            },
            flip: {
                left: function(b, c) {
                    a.ui.pos._trigger(b, c, "posCollide", "flipLeft");
                    var d = c.within, f = d.offset.left + d.scrollLeft, g = d.width, h = d.isWindow ? d.scrollLeft : d.offset.left, i = b.left - c.collisionPosition.marginLeft, j = i - h, k = i + c.collisionWidth - g - h, l = c.my[0] === "left" ? -c.elemWidth : c.my[0] === "right" ? c.elemWidth : 0, m = c.at[0] === "left" ? c.targetWidth : c.at[0] === "right" ? -c.targetWidth : 0, n = -2 * c.offset[0], o, p;
                    if (j < 0) {
                        o = b.left + l + m + n + c.collisionWidth - g - f;
                        if (o < 0 || o < e(j)) {
                            b.left += l + m + n;
                        }
                    } else if (k > 0) {
                        p = b.left - c.collisionPosition.marginLeft + l + m + n - h;
                        if (p > 0 || e(p) < k) {
                            b.left += l + m + n;
                        }
                    }
                    a.ui.pos._trigger(b, c, "posCollided", "flipLeft");
                },
                top: function(b, c) {
                    a.ui.pos._trigger(b, c, "posCollide", "flipTop");
                    var d = c.within, f = d.offset.top + d.scrollTop, g = d.height, h = d.isWindow ? d.scrollTop : d.offset.top, i = b.top - c.collisionPosition.marginTop, j = i - h, k = i + c.collisionHeight - g - h, l = c.my[1] === "top", m = l ? -c.elemHeight : c.my[1] === "bottom" ? c.elemHeight : 0, n = c.at[1] === "top" ? c.targetHeight : c.at[1] === "bottom" ? -c.targetHeight : 0, o = -2 * c.offset[1], p, q;
                    if (j < 0) {
                        q = b.top + m + n + o + c.collisionHeight - g - f;
                        if (b.top + m + n + o > j && (q < 0 || q < e(j))) {
                            b.top += m + n + o;
                        }
                    } else if (k > 0) {
                        p = b.top - c.collisionPosition.marginTop + m + n + o - h;
                        if (b.top + m + n + o > k && (p > 0 || e(p) < k)) {
                            b.top += m + n + o;
                        }
                    }
                    a.ui.pos._trigger(b, c, "posCollided", "flipTop");
                }
            },
            flipfit: {
                left: function() {
                    a.ui.pos.flip.left.apply(this, arguments);
                    a.ui.pos.fit.left.apply(this, arguments);
                },
                top: function() {
                    a.ui.pos.flip.top.apply(this, arguments);
                    a.ui.pos.fit.top.apply(this, arguments);
                }
            }
        };
        (function() {
            var b, c, d, e, f, g = document.getElementsByTagName("body")[0], h = document.createElement("div");
            b = document.createElement(g ? "div" : "body");
            d = {
                visibility: "hidden",
                width: 0,
                height: 0,
                border: 0,
                margin: 0,
                background: "none"
            };
            if (g) {
                a.extend(d, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
            }
            for (f in d) {
                b.style[f] = d[f];
            }
            b.appendChild(h);
            c = g || document.documentElement;
            c.insertBefore(b, c.firstChild);
            h.style.cssText = "position: absolute; left: 10.7432222px;";
            e = a(h).offset().left;
            a.support.offsetFractions = e > 10 && e < 11;
            b.innerHTML = "";
            c.removeChild(b);
        })();
    })(jQuery);

    (function(a) {
        "use strict";
        if (typeof define === "function" && define.amd) {
            define([ "jquery" ], a);
        } else if (window.jQuery && !window.jQuery.fn.iconpicker) {
            a(window.jQuery);
        }
    })(function(a) {
        "use strict";
        var b = {
            isEmpty: function(a) {
                return a === false || a === "" || a === null || a === undefined;
            },
            isEmptyObject: function(a) {
                return this.isEmpty(a) === true || a.length === 0;
            },
            isElement: function(b) {
                return a(b).length > 0;
            },
            isString: function(a) {
                return typeof a === "string" || a instanceof String;
            },
            isArray: function(b) {
                return a.isArray(b);
            },
            inArray: function(b, c) {
                return a.inArray(b, c) !== -1;
            },
            throwError: function(a) {
                throw "Font Awesome Icon Picker Exception: " + a;
            }
        };
        var c = function(d, e) {
            this._id = c._idCounter++;
            this.element = a(d).addClass("iconpicker-element");
            this._trigger("iconpickerCreate");
            this.options = a.extend({}, c.defaultOptions, this.element.data(), e);
            this.options.templates = a.extend({}, c.defaultOptions.templates, this.options.templates);
            this.options.originalPlacement = this.options.placement;
            this.container = b.isElement(this.options.container) ? a(this.options.container) : false;
            if (this.container === false) {
                if (this.element.is(".dropdown-toggle")) {
                    this.container = a("~ .dropdown-menu:first", this.element);
                } else {
                    this.container = this.element.is("input,textarea,button,.btn") ? this.element.parent() : this.element;
                }
            }
            this.container.addClass("iconpicker-container");
            if (this.isDropdownMenu()) {
                this.options.templates.search = false;
                this.options.templates.buttons = false;
                this.options.placement = "inline";
            }
            this.input = this.element.is("input,textarea") ? this.element.addClass("iconpicker-input") : false;
            if (this.input === false) {
                this.input = this.container.find(this.options.input);
                if (!this.input.is("input,textarea")) {
                    this.input = false;
                }
            }
            this.component = this.isDropdownMenu() ? this.container.parent().find(this.options.component) : this.container.find(this.options.component);
            if (this.component.length === 0) {
                this.component = false;
            } else {
                this.component.find("i").addClass("iconpicker-component");
            }
            this._createPopover();
            this._createIconpicker();
            if (this.getAcceptButton().length === 0) {
                this.options.mustAccept = false;
            }
            if (this.isInputGroup()) {
                this.container.parent().append(this.popover);
            } else {
                this.container.append(this.popover);
            }
            this._bindElementEvents();
            this._bindWindowEvents();
            this.update(this.options.selected);
            if (this.isInline()) {
                this.show();
            }
            this._trigger("iconpickerCreated");
        };
        c._idCounter = 0;
        c.defaultOptions = {
            title: false,
            selected: false,
            defaultValue: false,
            placement: "bottom",
            collision: "none",
            animation: true,
            hideOnSelect: false,
            showFooter: false,
            searchInFooter: false,
            mustAccept: false,
            selectedCustomClass: "bg-primary",
            icons: [],
            fullClassFormatter: function(a) {
                return "fa " + a;
            },
            input: "input,.iconpicker-input",
            inputSearch: false,
            container: false,
            component: ".input-group-addon,.iconpicker-component",
            templates: {
                popover: '<div class="iconpicker-popover popover"><div class="arrow"></div>' + '<div class="popover-title"></div><div class="popover-content"></div></div>',
                footer: '<div class="popover-footer"></div>',
                buttons: '<button class="iconpicker-btn iconpicker-btn-cancel btn btn-default btn-sm">' + Language.getLanguageVariable(10056) + '</button>' + ' <button class="iconpicker-btn iconpicker-btn-accept btn btn-primary btn-sm">' + Language.getLanguageVariable(10069) + '</button>',
                search: '<input type="search" class="form-control iconpicker-search" placeholder="' + Language.getLanguageVariable(10494) + '" />',
                iconpicker: '<div class="iconpicker"><div class="iconpicker-items"></div></div>',
                iconpickerItem: '<a role="button" href="#" class="iconpicker-item"><i></i></a>'
            }
        };
        c.batch = function(b, c) {
            var d = Array.prototype.slice.call(arguments, 2);
            return a(b).each(function() {
                var b = a(this).data("iconpicker");
                if (!!b) {
                    b[c].apply(b, d);
                }
            });
        };
        c.prototype = {
            constructor: c,
            options: {},
            _id: 0,
            _trigger: function(b, c) {
                c = c || {};
                this.element.trigger(a.extend({
                    type: b,
                    iconpickerInstance: this
                }, c));
            },
            _createPopover: function() {
                this.popover = a(this.options.templates.popover);
                var c = this.popover.find(".popover-title");
                if (!!this.options.title) {
                    c.append(a('<div class="popover-title-text">' + this.options.title + "</div>"));
                }
                if (this.hasSeparatedSearchInput() && !this.options.searchInFooter) {
                    c.append(this.options.templates.search);
                } else if (!this.options.title) {
                    c.remove();
                }
                if (this.options.showFooter && !b.isEmpty(this.options.templates.footer)) {
                    var d = a(this.options.templates.footer);
                    if (this.hasSeparatedSearchInput() && this.options.searchInFooter) {
                        d.append(a(this.options.templates.search));
                    }
                    if (!b.isEmpty(this.options.templates.buttons)) {
                        d.append(a(this.options.templates.buttons));
                    }
                    this.popover.append(d);
                }
                if (this.options.animation === true) {
                    this.popover.addClass("fade");
                }
                return this.popover;
            },
            _createIconpicker: function() {
                var b = this;
                this.iconpicker = a(this.options.templates.iconpicker);
                var c = function(c) {
                    var d = a(this);
                    if (d.is("i")) {
                        d = d.parent();
                    }
                    b._trigger("iconpickerSelect", {
                        iconpickerItem: d,
                        iconpickerValue: b.iconpickerValue
                    });
                    if (b.options.mustAccept === false) {
                        b.update(d.data("iconpickerValue"));
                        b._trigger("iconpickerSelected", {
                            iconpickerItem: this,
                            iconpickerValue: b.iconpickerValue
                        });
                    } else {
                        b.update(d.data("iconpickerValue"), true);
                    }
                    if (b.options.hideOnSelect && b.options.mustAccept === false) {
                        b.hide();
                    }
                    c.preventDefault();
                    return false;
                };
                for (var d in this.options.icons) {
                    var e = a(this.options.templates.iconpickerItem);
                    e.find("i").addClass(this.options.fullClassFormatter(this.options.icons[d]));
                    e.data("iconpickerValue", this.options.icons[d]).on("click.iconpicker", c);
                    this.iconpicker.find(".iconpicker-items").append(e.attr("title", "." + this.options.icons[d]));
                }
                this.popover.find(".popover-content").append(this.iconpicker);
                return this.iconpicker;
            },
            _isEventInsideIconpicker: function(b) {
                var c = a(b.target);
                if ((!c.hasClass("iconpicker-element") || c.hasClass("iconpicker-element") && !c.is(this.element)) && c.parents(".iconpicker-popover").length === 0) {
                    return false;
                }
                return true;
            },
            _bindElementEvents: function() {
                var c = this;
                this.getSearchInput().on("keyup.iconpicker", function() {
                    c.filter(a(this).val().toLowerCase());
                });
                this.getAcceptButton().on("click.iconpicker", function() {
                    var a = c.iconpicker.find(".iconpicker-selected").get(0);
                    c.update(c.iconpickerValue);
                    c._trigger("iconpickerSelected", {
                        iconpickerItem: a,
                        iconpickerValue: c.iconpickerValue
                    });
                    if (!c.isInline()) {
                        c.hide();
                    }
                });
                this.getCancelButton().on("click.iconpicker", function() {
                    if (!c.isInline()) {
                        c.hide();
                    }
                });
                this.element.on("focus.iconpicker", function(a) {
                    c.show();
                    a.stopPropagation();
                });
                if (this.hasComponent()) {
                    this.component.on("click.iconpicker", function() {
                        c.toggle();
                    });
                }
                if (this.hasInput()) {
                    this.input.on("keyup.iconpicker", function(d) {
                        if (!b.inArray(d.keyCode, [ 38, 40, 37, 39, 16, 17, 18, 9, 8, 91, 93, 20, 46, 186, 190, 46, 78, 188, 44, 86 ])) {
                            c.update();
                        } else {
                            c._updateFormGroupStatus(c.getValid(this.value) !== false);
                        }
                        if (c.options.inputSearch === true) {
                            c.filter(a(this).val().toLowerCase());
                        }
                    });
                }
            },
            _bindWindowEvents: function() {
                var b = a(window.document);
                var c = this;
                var d = ".iconpicker.inst" + this._id;
                a(window).on("resize.iconpicker" + d + " orientationchange.iconpicker" + d, function(a) {
                    if (c.popover.hasClass("in")) {
                        c.updatePlacement();
                    }
                });
                if (!c.isInline()) {
                    b.on("mouseup" + d, function(a) {
                        if (!c._isEventInsideIconpicker(a) && !c.isInline()) {
                            c.hide();
                        }
                        a.stopPropagation();
                        a.preventDefault();
                        return false;
                    });
                }
                return false;
            },
            _unbindElementEvents: function() {
                this.popover.off(".iconpicker");
                this.element.off(".iconpicker");
                if (this.hasInput()) {
                    this.input.off(".iconpicker");
                }
                if (this.hasComponent()) {
                    this.component.off(".iconpicker");
                }
                if (this.hasContainer()) {
                    this.container.off(".iconpicker");
                }
            },
            _unbindWindowEvents: function() {
                a(window).off(".iconpicker.inst" + this._id);
                a(window.document).off(".iconpicker.inst" + this._id);
            },
            updatePlacement: function(b, c) {
                b = b || this.options.placement;
                this.options.placement = b;
                c = c || this.options.collision;
                c = c === true ? "flip" : c;
                var d = {
                    at: "right bottom",
                    my: "right top",
                    of: this.hasInput() && !this.isInputGroup() ? this.input : this.container,
                    collision: c === true ? "flip" : c,
                    within: window
                };
                this.popover.removeClass("inline topLeftCorner topLeft top topRight topRightCorner " + "rightTop right rightBottom bottomRight bottomRightCorner " + "bottom bottomLeft bottomLeftCorner leftBottom left leftTop");
                if (typeof b === "object") {
                    return this.popover.pos(a.extend({}, d, b));
                }
                switch (b) {
                  case "inline":
                    {
                        d = false;
                    }
                    break;

                  case "topLeftCorner":
                    {
                        d.my = "right bottom";
                        d.at = "left top";
                    }
                    break;

                  case "topLeft":
                    {
                        d.my = "left bottom";
                        d.at = "left top";
                    }
                    break;

                  case "top":
                    {
                        d.my = "center bottom";
                        d.at = "center top";
                    }
                    break;

                  case "topRight":
                    {
                        d.my = "right bottom";
                        d.at = "right top";
                    }
                    break;

                  case "topRightCorner":
                    {
                        d.my = "left bottom";
                        d.at = "right top";
                    }
                    break;

                  case "rightTop":
                    {
                        d.my = "left bottom";
                        d.at = "right center";
                    }
                    break;

                  case "right":
                    {
                        d.my = "left center";
                        d.at = "right center";
                    }
                    break;

                  case "rightBottom":
                    {
                        d.my = "left top";
                        d.at = "right center";
                    }
                    break;

                  case "bottomRightCorner":
                    {
                        d.my = "left top";
                        d.at = "right bottom";
                    }
                    break;

                  case "bottomRight":
                    {
                        d.my = "right top";
                        d.at = "right bottom";
                    }
                    break;

                  case "bottom":
                    {
                        d.my = "center top";
                        d.at = "center bottom";
                    }
                    break;

                  case "bottomLeft":
                    {
                        d.my = "left top";
                        d.at = "left bottom";
                    }
                    break;

                  case "bottomLeftCorner":
                    {
                        d.my = "right top";
                        d.at = "left bottom";
                    }
                    break;

                  case "leftBottom":
                    {
                        d.my = "right top";
                        d.at = "left center";
                    }
                    break;

                  case "left":
                    {
                        d.my = "right center";
                        d.at = "left center";
                    }
                    break;

                  case "leftTop":
                    {
                        d.my = "right bottom";
                        d.at = "left center";
                    }
                    break;

                  default:
                    {
                        return false;
                    }
                    break;
                }
                this.popover.css({
                    display: this.options.placement === "inline" ? "" : "block"
                });
                if (d !== false) {
                    this.popover.pos(d).css("maxWidth", a(window).width() - this.container.offset().left - 5);
                } else {
                    this.popover.css({
                        top: "auto",
                        right: "auto",
                        bottom: "auto",
                        left: "auto",
                        maxWidth: "none"
                    });
                }
                this.popover.addClass(this.options.placement);
                return true;
            },
            _updateComponents: function() {
                this.iconpicker.find(".iconpicker-item.iconpicker-selected").removeClass("iconpicker-selected " + this.options.selectedCustomClass);
                if (this.iconpickerValue) {
                    this.iconpicker.find("." + this.options.fullClassFormatter(this.iconpickerValue).replace(/ /g, ".")).parent().addClass("iconpicker-selected " + this.options.selectedCustomClass);
                }
                if (this.hasComponent()) {
                    var a = this.component.find("i");
                    if (a.length > 0) {
                        a.attr("class", this.options.fullClassFormatter(this.iconpickerValue));
                    } else {
                        this.component.html(this.getHtml());
                    }
                }
            },
            _updateFormGroupStatus: function(a) {
                if (this.hasInput()) {
                    if (a !== false) {
                        this.input.parents(".form-group:first").removeClass("has-error");
                    } else {
                        this.input.parents(".form-group:first").addClass("has-error");
                    }
                    return true;
                }
                return false;
            },
            getValid: function(c) {
                if (!b.isString(c)) {
                    c = "";
                }
                var d = c === "";
                c = a.trim(c);
                if (b.inArray(c, this.options.icons) || d) {
                    return c;
                }
                return false;
            },
            setValue: function(a) {
                var b = this.getValid(a);
                if (b !== false) {
                    this.iconpickerValue = b;
                    this._trigger("iconpickerSetValue", {
                        iconpickerValue: b
                    });
                    return this.iconpickerValue;
                } else {
                    this._trigger("iconpickerInvalid", {
                        iconpickerValue: a
                    });
                    return false;
                }
            },
            getHtml: function() {
                return '<i class="' + this.options.fullClassFormatter(this.iconpickerValue) + '"></i>';
            },
            setSourceValue: function(a) {
                a = this.setValue(a);
                if (a !== false && a !== "") {
                    if (this.hasInput()) {
                        this.input.val(this.iconpickerValue);
                    } else {
                        this.element.data("iconpickerValue", this.iconpickerValue);
                    }
                    this._trigger("iconpickerSetSourceValue", {
                        iconpickerValue: a
                    });
                }
                return a;
            },
            getSourceValue: function(a) {
                a = a || this.options.defaultValue;
                var b = a;
                if (this.hasInput()) {
                    b = this.input.val();
                } else {
                    b = this.element.data("iconpickerValue");
                }
                if (b === undefined || b === "" || b === null || b === false) {
                    b = a;
                }
                return b;
            },
            hasInput: function() {
                return this.input !== false;
            },
            isInputSearch: function() {
                return this.hasInput() && this.options.inputSearch === true;
            },
            isInputGroup: function() {
                return this.container.is(".input-group");
            },
            isDropdownMenu: function() {
                return this.container.is(".dropdown-menu");
            },
            hasSeparatedSearchInput: function() {
                return this.options.templates.search !== false && !this.isInputSearch();
            },
            hasComponent: function() {
                return this.component !== false;
            },
            hasContainer: function() {
                return this.container !== false;
            },
            getAcceptButton: function() {
                return this.popover.find(".iconpicker-btn-accept");
            },
            getCancelButton: function() {
                return this.popover.find(".iconpicker-btn-cancel");
            },
            getSearchInput: function() {
                return this.popover.find(".iconpicker-search");
            },
            filter: function(c) {
                if (b.isEmpty(c)) {
                    this.iconpicker.find(".iconpicker-item").show();
                    return a(false);
                } else {
                    var d = [];
                    this.iconpicker.find(".iconpicker-item").each(function() {
                        var b = a(this);
                        var e = b.attr("title").toLowerCase();
                        var f = false;
                        try {
                            f = new RegExp(c, "g");
                        } catch (g) {
                            f = false;
                        }
                        if (f !== false && e.match(f)) {
                            d.push(b);
                            b.show();
                        } else {
                            b.hide();
                        }
                    });
                    return d;
                }
            },
            show: function() {
                if (this.popover.hasClass("in")) {
                    return false;
                }
                a.iconpicker.batch(a(".iconpicker-popover.in:not(.inline)").not(this.popover), "hide");
                this._trigger("iconpickerShow");
                this.updatePlacement();
                this.popover.addClass("in");
                setTimeout(a.proxy(function() {
                    this.popover.css("display", this.isInline() ? "" : "block");
                    this._trigger("iconpickerShown");
                }, this), this.options.animation ? 300 : 1);
            },
            hide: function() {
                if (!this.popover.hasClass("in")) {
                    return false;
                }
                this._trigger("iconpickerHide");
                this.popover.removeClass("in");
                setTimeout(a.proxy(function() {
                    this.popover.css("display", "none");
                    this.getSearchInput().val("");
                    this.filter("");
                    this._trigger("iconpickerHidden");
                }, this), this.options.animation ? 300 : 1);
            },
            toggle: function() {
                if (this.popover.is(":visible")) {
                    this.hide();
                } else {
                    this.show(true);
                }
            },
            update: function(a, b) {
                a = a ? a : this.getSourceValue(this.iconpickerValue);
                this._trigger("iconpickerUpdate");
                if (b === true) {
                    a = this.setValue(a);
                } else {
                    a = this.setSourceValue(a);
                    this._updateFormGroupStatus(a !== false);
                }
                if (a !== false) {
                    this._updateComponents();
                }
                this._trigger("iconpickerUpdated");
                return a;
            },
            destroy: function() {
                this._trigger("iconpickerDestroy");
                this.element.removeData("iconpicker").removeData("iconpickerValue").removeClass("iconpicker-element");
                this._unbindElementEvents();
                this._unbindWindowEvents();
                a(this.popover).remove();
                this._trigger("iconpickerDestroyed");
            },
            disable: function() {
                if (this.hasInput()) {
                    this.input.prop("disabled", true);
                    return true;
                }
                return false;
            },
            enable: function() {
                if (this.hasInput()) {
                    this.input.prop("disabled", false);
                    return true;
                }
                return false;
            },
            isDisabled: function() {
                if (this.hasInput()) {
                    return this.input.prop("disabled") === true;
                }
                return false;
            },
            isInline: function() {
                return this.options.placement === "inline" || this.popover.hasClass("inline");
            }
        };
        a.iconpicker = c;
        a.fn.iconpicker = function(b) {
            return this.each(function() {
                var d = a(this);
                if (!d.data("iconpicker")) {
                    d.data("iconpicker", new c(this, typeof b === "object" ? b : {}));
                }
            });
        };
        c.defaultOptions.icons = ["fa-500px","fa-adjust","fa-adn","fa-align-center","fa-align-justify","fa-align-left","fa-align-right","fa-amazon","fa-ambulance","fa-anchor","fa-android","fa-angellist","fa-angle-double-down","fa-angle-double-left","fa-angle-double-right","fa-angle-double-up","fa-angle-down","fa-angle-left","fa-angle-right","fa-angle-up","fa-apple","fa-archive","fa-area-chart","fa-arrow-circle-down","fa-arrow-circle-left","fa-arrow-circle-o-down","fa-arrow-circle-o-left","fa-arrow-circle-o-right","fa-arrow-circle-o-up","fa-arrow-circle-right","fa-arrow-circle-up","fa-arrow-down","fa-arrow-left","fa-arrow-right","fa-arrow-up","fa-arrows","fa-arrows-alt","fa-arrows-h","fa-arrows-v","fa-asterisk","fa-at","fa-automobile","fa-backward","fa-balance-scale","fa-ban","fa-bank","fa-bar-chart","fa-bar-chart-o","fa-barcode","fa-bars","fa-battery-0","fa-battery-1","fa-battery-2","fa-battery-3","fa-battery-4","fa-battery-empty","fa-battery-full","fa-battery-half","fa-battery-quarter","fa-battery-three-quarters","fa-bed","fa-beer","fa-behance","fa-behance-square","fa-bell","fa-bell-o","fa-bell-slash","fa-bell-slash-o","fa-bicycle","fa-binoculars","fa-birthday-cake","fa-bitbucket","fa-bitbucket-square","fa-bitcoin","fa-black-tie","fa-bold","fa-bolt","fa-bomb","fa-book","fa-bookmark","fa-bookmark-o","fa-briefcase","fa-btc","fa-bug","fa-building","fa-building-o","fa-bullhorn","fa-bullseye","fa-bus","fa-buysellads","fa-cab","fa-calculator","fa-calendar","fa-calendar-check-o","fa-calendar-minus-o","fa-calendar-o","fa-calendar-plus-o","fa-calendar-times-o","fa-camera","fa-camera-retro","fa-car","fa-caret-down","fa-caret-left","fa-caret-right","fa-caret-square-o-down","fa-caret-square-o-left","fa-caret-square-o-right","fa-caret-square-o-up","fa-caret-up","fa-cart-arrow-down","fa-cart-plus","fa-cc","fa-cc-amex","fa-cc-diners-club","fa-cc-discover","fa-cc-jcb","fa-cc-mastercard","fa-cc-paypal","fa-cc-stripe","fa-cc-visa","fa-certificate","fa-chain","fa-chain-broken","fa-check","fa-check-circle","fa-check-circle-o","fa-check-square","fa-check-square-o","fa-chevron-circle-down","fa-chevron-circle-left","fa-chevron-circle-right","fa-chevron-circle-up","fa-chevron-down","fa-chevron-left","fa-chevron-right","fa-chevron-up","fa-child","fa-chrome","fa-circle","fa-circle-o","fa-circle-o-notch","fa-circle-thin","fa-clipboard","fa-clock-o","fa-clone","fa-close","fa-cloud","fa-cloud-download","fa-cloud-upload","fa-cny","fa-code","fa-code-fork","fa-codepen","fa-coffee","fa-cog","fa-cogs","fa-columns","fa-comment","fa-comment-o","fa-commenting","fa-commenting-o","fa-comments","fa-comments-o","fa-compass","fa-compress","fa-connectdevelop","fa-contao","fa-copy","fa-copyright","fa-creative-commons","fa-credit-card","fa-crop","fa-crosshairs","fa-css3","fa-cube","fa-cubes","fa-cut","fa-cutlery","fa-dashboard","fa-dashcube","fa-database","fa-dedent","fa-delicious","fa-desktop","fa-deviantart","fa-diamond","fa-digg","fa-dollar","fa-dot-circle-o","fa-download","fa-dribbble","fa-dropbox","fa-drupal","fa-edit","fa-eject","fa-ellipsis-h","fa-ellipsis-v","fa-empire","fa-envelope","fa-envelope-o","fa-envelope-square","fa-eraser","fa-eur","fa-euro","fa-exchange","fa-exclamation","fa-exclamation-circle","fa-exclamation-triangle","fa-expand","fa-expeditedssl","fa-external-link","fa-external-link-square","fa-eye","fa-eye-slash","fa-eyedropper","fa-facebook","fa-facebook-f","fa-facebook-official","fa-facebook-square","fa-fast-backward","fa-fast-forward","fa-fax","fa-feed","fa-female","fa-fighter-jet","fa-file","fa-file-archive-o","fa-file-audio-o","fa-file-code-o","fa-file-excel-o","fa-file-image-o","fa-file-movie-o","fa-file-o","fa-file-pdf-o","fa-file-photo-o","fa-file-picture-o","fa-file-powerpoint-o","fa-file-sound-o","fa-file-text","fa-file-text-o","fa-file-video-o","fa-file-word-o","fa-file-zip-o","fa-files-o","fa-film","fa-filter","fa-fire","fa-fire-extinguisher","fa-firefox","fa-flag","fa-flag-checkered","fa-flag-o","fa-flash","fa-flask","fa-flickr","fa-floppy-o","fa-folder","fa-folder-o","fa-folder-open","fa-folder-open-o","fa-font","fa-fonticons","fa-forumbee","fa-forward","fa-foursquare","fa-frown-o","fa-futbol-o","fa-gamepad","fa-gavel","fa-gbp","fa-ge","fa-gear","fa-gears","fa-genderless","fa-get-pocket","fa-gg","fa-gg-circle","fa-gift","fa-git","fa-git-square","fa-github","fa-github-alt","fa-github-square","fa-gittip","fa-glass","fa-globe","fa-google","fa-google-plus","fa-google-plus-square","fa-google-wallet","fa-graduation-cap","fa-gratipay","fa-group","fa-h-square","fa-hacker-news","fa-hand-grab-o","fa-hand-lizard-o","fa-hand-o-down","fa-hand-o-left","fa-hand-o-right","fa-hand-o-up","fa-hand-paper-o","fa-hand-peace-o","fa-hand-pointer-o","fa-hand-scissors-o","fa-hand-spock-o","fa-hand-stop-o","fa-hdd-o","fa-header","fa-headphones","fa-heart","fa-heart-o","fa-heartbeat","fa-history","fa-home","fa-hospital-o","fa-hotel","fa-hourglass","fa-hourglass-1","fa-hourglass-2","fa-hourglass-3","fa-hourglass-end","fa-hourglass-half","fa-hourglass-o","fa-hourglass-start","fa-houzz","fa-html5","fa-i-cursor","fa-ils","fa-image","fa-inbox","fa-indent","fa-industry","fa-info","fa-info-circle","fa-inr","fa-instagram","fa-institution","fa-internet-explorer","fa-intersex","fa-ioxhost","fa-italic","fa-joomla","fa-jpy","fa-jsfiddle","fa-key","fa-keyboard-o","fa-krw","fa-language","fa-laptop","fa-lastfm","fa-lastfm-square","fa-leaf","fa-leanpub","fa-legal","fa-lemon-o","fa-level-down","fa-level-up","fa-life-bouy","fa-life-buoy","fa-life-ring","fa-life-saver","fa-lightbulb-o","fa-line-chart","fa-link","fa-linkedin","fa-linkedin-square","fa-linux","fa-list","fa-list-alt","fa-list-ol","fa-list-ul","fa-location-arrow","fa-lock","fa-long-arrow-down","fa-long-arrow-left","fa-long-arrow-right","fa-long-arrow-up","fa-magic","fa-magnet","fa-mail-forward","fa-mail-reply","fa-mail-reply-all","fa-male","fa-map","fa-map-marker","fa-map-o","fa-map-pin","fa-map-signs","fa-mars-double","fa-mars-stroke","fa-mars-stroke-h","fa-mars-stroke-v","fa-maxcdn","fa-meanpath","fa-medium","fa-medkit","fa-mercury","fa-microphone","fa-microphone-slash","fa-minus","fa-minus-circle","fa-minus-square","fa-minus-square-o","fa-mobile","fa-mobile-phone","fa-money","fa-moon-o","fa-mortar-board","fa-motorcycle","fa-mouse-pointer","fa-music","fa-navicon","fa-neuter","fa-newspaper-o","fa-object-group","fa-object-ungroup","fa-odnoklassniki","fa-odnoklassniki-square","fa-opencart","fa-openid","fa-opera","fa-optin-monster","fa-outdent","fa-pagelines","fa-paint-brush","fa-paper-plane","fa-paper-plane-o","fa-paperclip","fa-paragraph","fa-paste","fa-pause","fa-paw","fa-paypal","fa-pencil","fa-pencil-square","fa-pencil-square-o","fa-phone","fa-phone-square","fa-photo","fa-picture-o","fa-pie-chart","fa-pied-piper","fa-pied-piper-alt","fa-pinterest","fa-pinterest-p","fa-pinterest-square","fa-plane","fa-play","fa-play-circle","fa-play-circle-o","fa-plug","fa-plus","fa-plus-circle","fa-plus-square","fa-plus-square-o","fa-power-off","fa-print","fa-puzzle-piece","fa-qq","fa-qrcode","fa-question","fa-question-circle","fa-quote-left","fa-quote-right","fa-ra","fa-random","fa-rebel","fa-recycle","fa-reddit","fa-reddit-square","fa-refresh","fa-registered","fa-remove","fa-renren","fa-reorder","fa-repeat","fa-reply","fa-reply-all","fa-retweet","fa-rmb","fa-road","fa-rocket","fa-rotate-left","fa-rotate-right","fa-rouble","fa-rss","fa-rss-square","fa-rub","fa-ruble","fa-rupee","fa-safari","fa-save","fa-scissors","fa-search","fa-search-minus","fa-search-plus","fa-sellsy","fa-send","fa-send-o","fa-server","fa-share","fa-share-alt","fa-share-alt-square","fa-share-square","fa-share-square-o","fa-shekel","fa-sheqel","fa-shield","fa-ship","fa-shirtsinbulk","fa-shopping-cart","fa-sign-in","fa-sign-out","fa-signal","fa-simplybuilt","fa-sitemap","fa-skyatlas","fa-skype","fa-slack","fa-sliders","fa-slideshare","fa-smile-o","fa-soccer-ball-o","fa-sort","fa-sort-alpha-asc","fa-sort-alpha-desc","fa-sort-amount-asc","fa-sort-amount-desc","fa-sort-asc","fa-sort-desc","fa-sort-down","fa-sort-numeric-asc","fa-sort-numeric-desc","fa-sort-up","fa-soundcloud","fa-space-shuttle","fa-spinner","fa-spoon","fa-spotify","fa-square","fa-square-o","fa-stack-exchange","fa-stack-overflow","fa-steam","fa-steam-square","fa-star","fa-star-half","fa-star-half-empty","fa-star-half-full","fa-star-half-o","fa-star-o","fa-step-backward","fa-step-forward","fa-stethoscope","fa-sticky-note","fa-sticky-note-o","fa-stop","fa-street-view","fa-strikethrough","fa-stumbleupon","fa-stumbleupon-circle","fa-subscript","fa-subway","fa-suitcase","fa-sun-o","fa-superscript","fa-support","fa-table","fa-tablet","fa-tachometer","fa-tag","fa-tags","fa-tasks","fa-taxi","fa-television","fa-tencent-weibo","fa-terminal","fa-text-height","fa-text-width","fa-th","fa-th-large","fa-th-list","fa-thumb-tack","fa-thumbs-down","fa-thumbs-o-down","fa-thumbs-o-up","fa-thumbs-up","fa-ticket","fa-times","fa-times-circle","fa-times-circle-o","fa-tint","fa-toggle-down","fa-toggle-left","fa-toggle-off","fa-toggle-on","fa-toggle-right","fa-toggle-up","fa-trademark","fa-train","fa-transgender","fa-transgender-alt","fa-trash","fa-trash-o","fa-tree","fa-trello","fa-tripadvisor","fa-trophy","fa-truck","fa-try","fa-tty","fa-tumblr","fa-tumblr-square","fa-turkish-lira","fa-tv","fa-twitch","fa-twitter","fa-twitter-square","fa-umbrella","fa-underline","fa-university","fa-unlink","fa-unlock","fa-unlock-alt","fa-unsorted","fa-upload","fa-usd","fa-user","fa-user-md","fa-user-plus","fa-user-secret","fa-user-times","fa-users","fa-venus","fa-venus-double","fa-venus-mars","fa-viacoin","fa-video-camera","fa-vimeo","fa-vimeo-square","fa-vine","fa-vk","fa-volume-down","fa-volume-off","fa-volume-up","fa-warning","fa-wechat","fa-weibo","fa-weixin","fa-whatsapp","fa-wheelchair","fa-wifi","fa-wikipedia-w","fa-windows","fa-won","fa-wordpress","fa-wrench","fa-xing","fa-xing-square","fa-y-combinator","fa-y-combinator-square","fa-yahoo","fa-yc","fa-yc-square","fa-yelp","fa-yen","fa-youtube","fa-youtube-play","fa-youtube-square"];
        c.defaultOptions.icons.push('md-3d-rotation','md-accessibility','md-account-balance','md-account-balance-wallet','md-account-box','md-account-child','md-account-circle','md-add-shopping-cart','md-alarm','md-alarm-add','md-alarm-off','md-alarm-on','md-android','md-announcement','md-aspect-ratio','md-assessment','md-assignment','md-assignment-ind','md-assignment-late','md-assignment-return','md-assignment-returned','md-assignment-turned-in','md-autorenew','md-backup','md-book','md-bookmark','md-bookmark-outline','md-bug-report','md-cached','md-class','md-credit-card','md-dashboard','md-delete','md-description','md-dns','md-done','md-done-all','md-event','md-exit-to-app','md-explore','md-extension','md-face-unlock','md-favorite','md-favorite-outline','md-find-in-page','md-find-replace','md-flip-to-back','md-flip-to-front','md-get-app','md-grade','md-group-work','md-help','md-highlight-remove','md-history','md-home','md-https','md-info','md-info-outline','md-input','md-invert-colors','md-label','md-label-outline','md-language','md-launch','md-list','md-lock','md-lock-open','md-lock-outline','md-loyalty','md-markunread-mailbox','md-note-add','md-open-in-browser','md-open-in-new','md-open-with','md-pageview','md-payment','md-perm-camera-mic','md-perm-contact-cal','md-perm-data-setting','md-perm-device-info','md-perm-identity','md-perm-media','md-perm-phone-msg','md-perm-scan-wifi','md-picture-in-picture','md-polymer','md-print','md-query-builder','md-question-answer','md-receipt','md-redeem','md-report-problem','md-restore','md-room','md-schedule','md-search','md-settings','md-settings-applications','md-settings-backup-restore','md-settings-bluetooth','md-settings-cell','md-settings-display','md-settings-ethernet','md-settings-input-antenna','md-settings-input-component','md-settings-input-composite','md-settings-input-hdmi','md-settings-input-svideo','md-settings-overscan','md-settings-phone','md-settings-power','md-settings-remote','md-settings-voice','md-shop','md-shopping-basket','md-shopping-cart','md-shop-two','md-speaker-notes','md-spellcheck','md-star-rate','md-stars','md-store','md-subject','md-swap-horiz','md-swap-vert','md-swap-vert-circle','md-system-update-tv','md-tab','md-tab-unselected','md-theaters','md-thumb-down','md-thumbs-up-down','md-thumb-up','md-toc','md-today','md-track-changes','md-translate','md-trending-down','md-trending-neutral','md-trending-up','md-turned-in','md-turned-in-not','md-verified-user','md-view-agenda','md-view-array','md-view-carousel','md-view-column','md-view-day','md-view-headline','md-view-list','md-view-module','md-view-quilt','md-view-stream','md-view-week','md-visibility','md-visibility-off','md-wallet-giftcard','md-wallet-membership','md-wallet-travel','md-work','md-error','md-warning','md-album','md-av-timer','md-closed-caption','md-equalizer','md-explicit','md-fast-forward','md-fast-rewind','md-games','md-hearing','md-high-quality','md-loop','md-mic','md-mic-none','md-mic-off','md-movie','md-my-library-add','md-my-library-books','md-my-library-music','md-new-releases','md-not-interested','md-pause','md-pause-circle-fill','md-pause-circle-outline','md-play-arrow','md-play-circle-fill','md-play-circle-outline','md-playlist-add','md-play-shopping-bag','md-queue','md-queue-music','md-radio','md-recent-actors','md-repeat','md-repeat-one','md-replay','md-shuffle','md-skip-next','md-skip-previous','md-snooze','md-stop','md-subtitles','md-surround-sound','md-videocam','md-videocam-off','md-video-collection','md-volume-down','md-volume-mute','md-volume-off','md-volume-up','md-web','md-business','md-call','md-call-end','md-call-made','md-call-merge','md-call-missed','md-call-received','md-call-split','md-chat','md-clear-all','md-comment','md-contacts','md-dialer-sip','md-dialpad','md-dnd-on','md-email','md-forum','md-import-export','md-invert-colors-off','md-invert-colors-on','md-live-help','md-location-off','md-location-on','md-message','md-messenger','md-no-sim','md-phone','md-portable-wifi-off','md-quick-contacts-dialer','md-quick-contacts-mail','md-ring-volume','md-stay-current-landscape','md-stay-current-portrait','md-stay-primary-landscape','md-stay-primary-portrait','md-swap-calls','md-textsms','md-voicemail','md-vpn-key','md-add','md-add-box','md-add-circle','md-add-circle-outline','md-archive','md-backspace','md-block','md-clear','md-content-copy','md-content-cut','md-content-paste','md-create','md-drafts','md-filter-list','md-flag','md-forward','md-gesture','md-inbox','md-link','md-mail','md-markunread','md-redo','md-remove','md-remove-circle','md-remove-circle-outline','md-reply','md-reply-all','md-report','md-save','md-select-all','md-send','md-sort','md-text-format','md-undo','md-access-alarm','md-access-alarms','md-access-time','md-add-alarm','md-airplanemode-off','md-airplanemode-on','md-battery-20','md-battery-30','md-battery-50','md-battery-60','md-battery-80','md-battery-90','md-battery-alert','md-battery-charging-20','md-battery-charging-30','md-battery-charging-50','md-battery-charging-60','md-battery-charging-80','md-battery-charging-90','md-battery-charging-full','md-battery-full','md-battery-std','md-battery-unknown','md-bluetooth','md-bluetooth-connected','md-bluetooth-disabled','md-bluetooth-searching','md-brightness-auto','md-brightness-high','md-brightness-low','md-brightness-medium','md-data-usage','md-developer-mode','md-devices','md-dvr','md-gps-fixed','md-gps-not-fixed','md-gps-off','md-location-disabled','md-location-searching','md-multitrack-audio','md-network-cell','md-network-wifi','md-nfc','md-now-wallpaper','md-now-widgets','md-screen-lock-landscape','md-screen-lock-portrait','md-screen-lock-rotation','md-screen-rotation','md-sd-storage','md-settings-system-daydream','md-signal-cellular-0-bar','md-signal-cellular-1-bar','md-signal-cellular-2-bar','md-signal-cellular-3-bar','md-signal-cellular-4-bar','md-signal-cellular-connected-no-internet-0-bar','md-signal-cellular-connected-no-internet-1-bar','md-signal-cellular-connected-no-internet-2-bar','md-signal-cellular-connected-no-internet-3-bar','md-signal-cellular-connected-no-internet-4-bar','md-signal-cellular-no-sim','md-signal-cellular-null','md-signal-cellular-off','md-signal-wifi-0-bar','md-signal-wifi-1-bar','md-signal-wifi-2-bar','md-signal-wifi-3-bar','md-signal-wifi-4-bar','md-signal-wifi-off','md-storage','md-usb','md-wifi-lock','md-wifi-tethering','md-attach-file','md-attach-money','md-border-all','md-border-bottom','md-border-clear','md-border-color','md-border-horizontal','md-border-inner','md-border-left','md-border-outer','md-border-right','md-border-style','md-border-top','md-border-vertical','md-format-align-center','md-format-align-justify','md-format-align-left','md-format-align-right','md-format-bold','md-format-clear','md-format-color-fill','md-format-color-reset','md-format-color-text','md-format-indent-decrease','md-format-indent-increase','md-format-italic','md-format-line-spacing','md-format-list-bulleted','md-format-list-numbered','md-format-paint','md-format-quote','md-format-size','md-format-strikethrough','md-format-textdirection-l-to-r','md-format-textdirection-r-to-l','md-format-underline','md-functions','md-insert-chart','md-insert-comment','md-insert-drive-file','md-insert-emoticon','md-insert-invitation','md-insert-link','md-insert-photo','md-merge-type','md-mode-comment','md-mode-edit','md-publish','md-vertical-align-bottom','md-vertical-align-center','md-vertical-align-top','md-wrap-text','md-attachment','md-cloud','md-cloud-circle','md-cloud-done','md-cloud-download','md-cloud-off','md-cloud-queue','md-cloud-upload','md-file-download','md-file-upload','md-folder','md-folder-open','md-folder-shared','md-cast','md-cast-connected','md-computer','md-desktop-mac','md-desktop-windows','md-dock','md-gamepad','md-headset','md-headset-mic','md-keyboard','md-keyboard-alt','md-keyboard-arrow-down','md-keyboard-arrow-left','md-keyboard-arrow-right','md-keyboard-arrow-up','md-keyboard-backspace','md-keyboard-capslock','md-keyboard-control','md-keyboard-hide','md-keyboard-return','md-keyboard-tab','md-keyboard-voice','md-laptop','md-laptop-chromebook','md-laptop-mac','md-laptop-windows','md-memory','md-mouse','md-phone-android','md-phone-iphone','md-phonelink','md-phonelink-off','md-security','md-sim-card','md-smartphone','md-speaker','md-tablet','md-tablet-android','md-tablet-mac','md-tv','md-watch','md-add-to-photos','md-adjust','md-assistant-photo','md-audiotrack','md-blur-circular','md-blur-linear','md-blur-off','md-blur-on','md-brightness-1','md-brightness-2','md-brightness-3','md-brightness-4','md-brightness-5','md-brightness-6','md-brightness-7','md-brush','md-camera','md-camera-alt','md-camera-front','md-camera-rear','md-camera-roll','md-center-focus-strong','md-center-focus-weak','md-collections','md-colorize','md-color-lens','md-compare','md-control-point','md-control-point-duplicate','md-crop','md-crop-3-2','md-crop-5-4','md-crop-7-5','md-crop-16-9','md-crop-din','md-crop-free','md-crop-landscape','md-crop-original','md-crop-portrait','md-crop-square','md-dehaze','md-details','md-edit','md-exposure','md-exposure-minus-1','md-exposure-minus-2','md-exposure-zero','md-exposure-plus-1','md-exposure-plus-2','md-filter','md-filter-1','md-filter-2','md-filter-3','md-filter-4','md-filter-5','md-filter-6','md-filter-7','md-filter-8','md-filter-9','md-filter-9-plus','md-filter-b-and-w','md-filter-center-focus','md-filter-drama','md-filter-frames','md-filter-hdr','md-filter-none','md-filter-tilt-shift','md-filter-vintage','md-flare','md-flash-auto','md-flash-off','md-flash-on','md-flip','md-gradient','md-grain','md-grid-off','md-grid-on','md-hdr-off','md-hdr-on','md-hdr-strong','md-hdr-weak','md-healing','md-image','md-image-aspect-ratio','md-iso','md-landscape','md-leak-add','md-leak-remove','md-lens','md-looks','md-looks-1','md-looks-2','md-looks-3','md-looks-4','md-looks-5','md-looks-6','md-loupe','md-movie-creation','md-nature','md-nature-people','md-navigate-before','md-navigate-next','md-palette','md-panorama','md-panorama-fisheye','md-panorama-horizontal','md-panorama-vertical','md-panorama-wide-angle','md-photo','md-photo-album','md-photo-camera','md-photo-library','md-portrait','md-remove-red-eye','md-rotate-left','md-rotate-right','md-slideshow','md-straighten','md-style','md-switch-camera','md-switch-video','md-tag-faces','md-texture','md-timelapse','md-timer','md-timer-3','md-timer-10','md-timer-auto','md-timer-off','md-tonality','md-transform','md-tune','md-wb-auto','md-wb-cloudy','md-wb-incandescent','md-wb-irradescent','md-wb-sunny','md-beenhere','md-directions','md-directions-bike','md-directions-bus','md-directions-car','md-directions-ferry','md-directions-subway','md-directions-train','md-directions-transit','md-directions-walk','md-flight','md-hotel','md-layers','md-layers-clear','md-local-airport','md-local-atm','md-local-attraction','md-local-bar','md-local-cafe','md-local-car-wash','md-local-convenience-store','md-local-drink','md-local-florist','md-local-gas-station','md-local-grocery-store','md-local-hospital','md-local-hotel','md-local-laundry-service','md-local-library','md-local-mall','md-local-movies','md-local-offer','md-local-parking','md-local-pharmacy','md-local-phone','md-local-pizza','md-local-play','md-local-post-office','md-local-print-shop','md-local-restaurant','md-local-see','md-local-shipping','md-local-taxi','md-location-history','md-map','md-my-location','md-navigation','md-pin-drop','md-place','md-rate-review','md-restaurant-menu','md-satellite','md-store-mall-directory','md-terrain','md-traffic','md-apps','md-cancel','md-arrow-drop-down-circle','md-arrow-drop-down','md-arrow-drop-up','md-arrow-back','md-arrow-forward','md-check','md-close','md-chevron-left','md-chevron-right','md-expand-less','md-expand-more','md-fullscreen','md-fullscreen-exit','md-menu','md-more-horiz','md-more-vert','md-refresh','md-unfold-less','md-unfold-more','md-adb','md-bluetooth-audio','md-disc-full','md-dnd-forwardslash','md-do-not-disturb','md-drive-eta','md-event-available','md-event-busy','md-event-note','md-folder-special','md-mms','md-more','md-network-locked','md-phone-bluetooth-speaker','md-phone-forwarded','md-phone-in-talk','md-phone-locked','md-phone-missed','md-phone-paused','md-play-download','md-play-install','md-sd-card','md-sim-card-alert','md-sms','md-sms-failed','md-sync','md-sync-disabled','md-sync-problem','md-system-update','md-tap-and-play','md-time-to-leave','md-vibration','md-voice-chat','md-vpn-lock','md-cake','md-domain','md-location-city','md-mood','md-notifications-none','md-notifications','md-notifications-off','md-notifications-on','md-notifications-paused','md-pages','md-party-mode','md-group','md-group-add','md-people','md-people-outline','md-person','md-person-add','md-person-outline','md-plus-one','md-poll','md-public','md-school','md-share','md-whatshot','md-check-box','md-check-box-outline-blank','md-radio-button-off','md-radio-button-on','md-star','md-star-half','md-star-outline');
        
    });
    
});