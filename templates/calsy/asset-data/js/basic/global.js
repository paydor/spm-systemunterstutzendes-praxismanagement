Date.prototype.stdTimezoneOffset = function() {
    
    var january = new Date(this.getFullYear(), 0, 1);
    var july = new Date(this.getFullYear(), 6, 1);
    
    return Math.max(january.getTimezoneOffset(), july.getTimezoneOffset());
    
};

/**
 * Retrieves whether the date is in daylight saving time or not. 
 * This means, in summer: true, winter: false
 * 
 * @author Peter Hamm
 * @returns bool
 */
Date.prototype.isInDST = function() {
    
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
    
};

function nl2br (str, is_xhtml) 
{
    
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    
};