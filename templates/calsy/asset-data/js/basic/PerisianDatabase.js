var PerisianDatabase = jQuery.extend(true, {}, PerisianBaseAjax);
PerisianDatabase.controller = baseUrl + '/system/database/';

PerisianDatabase.exportDump = function()
{
    
    window.open(PerisianDatabase.controller + '?do=dump', '_blank');
    
};

/**
 * Toggles the form to upload the database sql file
 * 
 * @author Peter Hamm
 * @returns void
 */
PerisianDatabase.diffDatabase = function()
{
    
    var elementContainer = PerisianDatabase.getModalContainer();
    
    var sendData = {

        'do': 'upload_database',
        'step': 1
        
    };

    elementContainer.load(this.controller, sendData, function () {

        Perisian.ajaxBox('show');

    });
    
};

/**
 * Loads the diff data
 * 
 * @author Peter Hamm
 * @returns void
 */
PerisianDatabase.loadDiff = function()
{
    
    PerisianDatabase.cancel();
    
    var elementContainer = PerisianDatabase.getModalContainer();
    
    var uploadedFile = elementContainer.find('#uploaded_file').val();
    
    var sendData = {
        
        "do": "diff",
        "f": uploadedFile
        
    };
        
    jQuery.post(this.controller, sendData, function(data) {
                
        jQuery('#database-diff-result').css('display', 'block');
        jQuery('#database-diff-result code').empty().html(data);
        
    });
    
};

PerisianDatabase.displayImportError = function(errorMessage)
{
    
    var elementContainer = PerisianDatabase.getModalContainer();
    
    elementContainer.find('.import-selection').css('display', 'none');
    
    elementContainer.find('.import-result:first').html('<i class="fa fa-exclamation-circle text-danger"></i> ' + errorMessage);
    
    elementContainer.find('.import-result').css('display', 'block');
    
};

PerisianDatabase.onUploadSuccess = function(data, response) {
        
    var callback = JSON.parse(response);

    if(!callback.success)
    {

        PerisianDatabase.displayImportError(callback.message);

    }
    else
    {
        
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');
        
        elementContainer.find('#uploaded_file').val(callback.file);
        
        elementContainer.find('#startButton').removeClass("disabled");
        
    }

};

PerisianDatabase.getModalContainer = function()
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    return elementContainer;
    
};