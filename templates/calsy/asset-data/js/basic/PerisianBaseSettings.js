/**
 * Basic class to handle settings
 *
 * @author Peter Hamm
 * @date 2010-12-01
 */
var PerisianBaseSettings = {

    // These need to be initialised on page ready
    // or in the concrete class

    dataSent: false,
    controller: '',
    
    actionSave: 'save',

    getSettingShortcut: function(longSetting)
    {
        
        return longSetting.split('_')[longSetting.split('_').length - 1];
        
    },

    /**
     * Saves the settings of a form
     *
     * @author Peter Hamm
     * @parameter integer settingListId The div ID of a setting list
     * @parameter String validation Optional: The name of a validation function
     * @parameter String callback Optional: The name of a callback function
     * @returns void
     */
    save: function(settingListId, validation, callback)
    {
                
        var settingId, settingValue, subjectId;

        if(jQuery('#subjectId'))
        {
            
            subjectId = jQuery('#subjectId').val();
            
        }

        if(this.dataSent)
        {

            if(typeof console != 'undefined')
            {
                
                console.log(Language.getLanguageVariable(10089));
                
            }

            return;

        }

        if(validation)
        {

            if(!validation(sendData))
            {

                if(typeof console != 'undefined')
                {
                    
                    console.log(Language.getLanguageVariable(10093));
                    
                }

                return;

            }

        }

        // Get the form with the specified ID
        var settingList = jQuery('#' + settingListId + ' input, #' + settingListId + ' select, #' + settingListId + ' textarea, #' + settingListId + ' div.icon-selector, #' + settingListId + ' div.color-picker-box');

        var sendData = {

            'do': PerisianBaseSettings.actionSave,
            'data': Array(),
            'subjectId': subjectId
            
        };
        
        var listData = {};

        settingList.each(function(i, item) {

            settingId = jQuery(item).attr('id') ? PerisianBaseSettings.getSettingShortcut(jQuery(item).attr('id')) : '';
            settingValue = '';

            if(jQuery(item).is('input'))
            {

                if(jQuery(item).attr('type') == 'checkbox')
                {
                    
                    settingValue = jQuery(item).attr('checked') ? 1 : 0;
                    
                    if(settingId.length == 0)
                    {
                        
                        // This is part of a checkbox list.
                        
                        var listItemKey = jQuery(item).parent().parent().parent().parent().attr('data-id');
                        var listItemSettingId = PerisianBaseSettings.getSettingShortcut(jQuery(item).parent().parent().parent().parent().parent().parent().attr('id'));
                        
                        if(!listData[listItemSettingId])
                        {
                            
                            listData[listItemSettingId] = [];
                            
                        }
                        
                        listData[listItemSettingId].push({'value': listItemKey, 'checked': settingValue});
                        
                        return;
                        
                    }
                    
                }
                else if(jQuery(item).attr('type') == 'text' || jQuery(item).attr('type') == 'password')
                {
                    
                    if(jQuery(item).parent().parent().hasClass('input-timespan'))
                    {
                        
                        // It's a timespan element
                        
                        var originalIdentifier = jQuery(item).attr('original-id');
                                                
                        settingValue = {
                            
                            'begin': jQuery('#' + originalIdentifier + '_begin').val(),
                            'end': jQuery('#' + originalIdentifier + '_end').val()
                            
                        };
                        
                        settingId = PerisianBaseSettings.getSettingShortcut(originalIdentifier);
                        
                    }
                    else
                    {
                        
                        settingValue = jQuery(item).val();
                        
                        if(jQuery(item).attr('type') == 'text')
                        {
                            
                            settingValue = settingValue.length == 0 ? '_=_empty_=_' : settingValue;
                            
                        }
                        
                    }
                    
                }

            }
            else if(jQuery(item).is('select'))
            {
                
                settingValue = jQuery(item).val();
                
            }
            else if(jQuery(item).is('textarea'))
            {
                
                settingValue = jQuery(item).val();
                settingValue = settingValue.length == 0 ? '_=_empty_=_' : settingValue;
                
            }
            else if(jQuery(item).is('div'))
            {
                
                if(jQuery(item).hasClass('color-picker-box'))
                {
                    
                    settingValue = Perisian.rgbToHex(jQuery(item).css('background-color'));
                    
                }
                else
                {
                    
                    settingValue = jQuery(item).attr('data-selected');
                
                }
                
            }

            if(settingId && settingId.length > 0)
            {
                
                sendData['s_' + settingId] = settingValue;
                
            }
            
        });
        
        jQuery(listData).each(function(key, item) {
            
            var actualKeys = Object.keys(listData);
            var actualKey = actualKeys[key];
                        
            sendData['s_' + actualKey] = listData[actualKey];
            
        });

        // Disable all buttons
        var buttonList = jQuery('#' + settingListId + ' button');

        buttonList.each(function(j, item) {

            jQuery(item).attr('disabled', 'disabled');
            
        });

        jQuery("#submitButton").attr("disabled", true);
        
        this.dataSent = true;

        var controller = this.controller;

        jQuery.post(controller, sendData, function(data) {
            
            buttonList.each(function(i, item) {

                jQuery(item).removeAttr('disabled');

            });
            
            var newId = data;
            
            try
            {
                
                var callbackData = JSON.parse(data);

                if(callbackData && callbackData.id)
                {

                    newId = callbackData.id;

                }
                
                if(callbackData && (callbackData.success === false || callbackData.error === true))
                {

                    newId = 'error';

                }
                
            }
            catch(e)
            {
                
            }

            if(newId != 'error')
            {

                if(callback)
                {
                    
                    callback(newId);
                    
                }
                
                toastr.success(Language.getLanguageVariable(10483));

            }
            else
            {
                
                var message = Language.getLanguageVariable(10484);
                
                if(callbackData.message)
                {
                    
                    message = callbackData.message;
                    
                }
                
                toastr.error(message);
                
            }

        });

    },

    /**
     * Use this for example to highlight fields with validation errors
     *
     * @author Peter Hamm
     * @returns void
     */
    highlightInputTitleError: function(fieldName, action)
    {

        if(!action)
        {
            jQuery("#" + fieldName).prev('p').addClass('validationProblem');
        }
        else if(action == 'remove')
        {
            jQuery("#" + fieldName).prev('p').removeClass('validationProblem');
        }

    },

    /**
     * Overwrite this if desired
     */
    validate: function()
    {
        return true;
    },

    checkNumberField: function(inputField)
    {

        var field = jQuery(inputField);
        var fieldVal = field.val().replace(',', '.');

        if(!Validation.checkNumber(fieldVal))
        {

            fieldVal = parseFloat(fieldVal).toString().replace('.', ',');
            field.val(fieldVal);

        }

    }

};