/**
 * Provides funtionality for a form pager.
 * 
 * @author Peter Hamm
 * @date 2016-03-28
 */
var PerisianFormPager = {
    
    conditions: {},
    
    currentConditionRunId: 0,
    lastConditionRunIdForAreaFilter: 0,
    
    /**
     * Adds a condition that gets executed when a tab change occurs.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @param function conditionFunction
     * @param String identifier
     * @returns void
     */
    addCondition: function(pageIndex, conditionFunction, identifier)
    {
        
        if(!PerisianFormPager.conditions[pageIndex])
        {
            
            PerisianFormPager.conditions[pageIndex] = [];
            
        }
        
        PerisianFormPager.conditions[pageIndex].push({
            
            'identifier': identifier,
            'conditionFunction': conditionFunction
            
        });
        
    },
    
    /**
     * Called from the conditions: Filters/displays the specified area.
     * 
     * @author Peter Hamm
     * @param int areaId
     * @returns void
     */
    filterArea: function(areaId)
    {
        
        var areaSelectorElement = null;
        
        jQuery("input[name='area']").each(function() {

            var element = jQuery(this);
            var containerElement = element.parent().parent();
            
            if(areaSelectorElement == null)
            {
                
                areaSelectorElement = containerElement.parent();
                
            }
            
            if(PerisianFormPager.lastConditionRunIdForAreaFilter != PerisianFormPager.currentConditionRunId)
            {
                
                containerElement.css('display', 'none');
                
            }
                        
            // -1: Show all, -2: Show none
            if((element.val() == areaId || areaId == '-1') && areaId != '-2')
            {
                
                containerElement.css('display', 'block');
                
            }

        });
                
        if(areaSelectorElement)
        {
                        
            areaSelectorElement.find('#no-area-selector-hint').remove();
        
            if(areaId == '-2')
            {

                var noSelectorHint = '<span id="no-area-selector-hint">' + Language.getLanguageVariable('p575f14993cbf4') + '</span>';

                areaSelectorElement.append(noSelectorHint);

            }

            PerisianFormPager.lastConditionRunIdForAreaFilter = PerisianFormPager.currentConditionRunId;
            
        }
        else
        {
            
            console.log("Warning: No area selector defined.")
            
        }
        
    },
    
    /**
     * Called from the conditions: Filters/displays the areas by the specified area group.
     * 
     * @author Peter Hamm
     * @param int areaGroupId
     * @returns void
     */
    filterAreaGroup: function(areaGroupId)
    {
        
        var areaSelectorElement = null;
        
        var containedAreasInGroup = PerisianFormPager.areaGroups[areaGroupId];
        
        jQuery("input[name='area']").each(function() {

            var areaIsInGroup = false;
            var element = jQuery(this);
            var containerElement = element.parent().parent();
            
            if(areaSelectorElement == null)
            {
                
                areaSelectorElement = containerElement.parent();
                
            }
            
            for(var i = 0; i < containedAreasInGroup.length; ++i)
            {
                
                if(element.val() == parseInt(containedAreasInGroup[i]))
                {
                    
                    areaIsInGroup = true;
                    
                    break;
                    
                }
                
            }
                                                
            // -1: Show all
            if(areaIsInGroup || areaGroupId == '-1')
            {
                                
                containerElement.css('display', 'block');
                
            }
            else
            {
                
                containerElement.css('display', 'none');
                
            }

        });
                
        if(areaSelectorElement)
        {
                        
            areaSelectorElement.find('#no-area-selector-hint').remove();
            
        }
        else
        {
            
            console.log("Warning: No area selector defined.")
            
        }
        
    },
    
    /**
     * Callback, fired just before a navigation to another tab occurs.
     * 
     * @author Peter Hamm
     * @param String containerElement
     * @param int index
     * @returns bool Returning false will cancel the navigation.
     */
    onBeforeNavigate: function(containerElement, index) 
    {
        
        return true;
        
    },
    
    /**
     * Initializes the form pager for the specified container identifier.
     * 
     * @author Peter Hamm
     * @param String containerId
     * @param bool skipInitTabButtons If set to true, the navigation buttons on the top are not initialized. Default: false
     * @returns void
     */
    initFormPager: function(containerId, skipInitTabButtons)
    {
        
        var containerIdentifier = containerId.replace("#", "");
        
        var element = jQuery('#' + containerIdentifier);
        
        if(!skipInitTabButtons)
        {
            
            var tabButtons = element.find('.nav li');

            tabButtons.each(function(index) {

                var button = jQuery(this);

                button.click(function() {

                    PerisianFormPager.navigateTo(element, index);

                });

            });
            
        }
        
    },
    
    /**
     * Sets the progress to the specified index
     * 
     * @author Peter Hamm
     * @param Object containerElement
     * @param int index
     * @returns void
     */
    setProgress: function(containerElement, index)
    {
        
        var tabButtons = containerElement.find('.nav li');
        var progressBar = containerElement.find('.progress-bar-primary');
        
        var percentage = (index / (tabButtons.length - 1)) * 100;
        
        TweenMax.to(progressBar, 0.5, {"width": percentage + "%", ease: Power2.easeInOut});
        
        tabButtons.each(function(buttonIndex) {
            
            var button = jQuery(this);
            
            if(buttonIndex < index)
            {
                
                button.removeClass('active').removeClass('done').addClass('done');
                
            }
            else if(buttonIndex == index)
            {
                
                button.removeClass('done').removeClass('active').addClass('active');
                
            }
            
            else if(buttonIndex > index)
            {
                
                button.removeClass('done').removeClass('active');
                
            }
            
        });
        
    },
    
    /**
     * Shows the tab with the specified index.
     * 
     * @author Peter Hamm
     * @param Object containerElement
     * @param int index
     * @returns void
     */
    showTab: function(containerElement, index)
    {
        
        var tabs = containerElement.find('.form-page');
        
        tabs.removeClass('active');
        
        jQuery(tabs[index]).addClass('active');
        
    },
    
    /**
     * Retrieves the current index for the specified container identifier
     * 
     * @author Peter Hamm
     * @param String containerId
     * @returns int
     */
    getCurrentIndex: function(containerId)
    {
        
        var containerIdentifier = containerId.replace("#", "");
        var containerElement = jQuery('#' + containerIdentifier);
        
        var currentIndex = -1;
        
        containerElement.find('.nav li').each(function(index) {
            
            var currentElement = jQuery(this);
            
            if(currentElement.hasClass('active'))
            {
                
                currentIndex = index;
                
                return;
                
            }
            
        });
        
        return currentIndex;
        
    },
    
    /**
     * Retrieves a list of the tabs associated with the specified container identifier.
     * 
     * @author Peter Hamm
     * @param String containerId
     * @returns Array
     */
    getTabs: function(containerId)
    {
        
        var containerIdentifier = containerId.replace("#", "");
        var containerElement = jQuery('#' + containerIdentifier);
        
        return containerElement.find('.form-page');
        
    },
    
    /**
     * Retrieves the tab element with the specified index, within the specified container.
     * 
     * @author PeterHamm
     * @param String containerId
     * @param int tabIndex
     * @returns Object
     */
    getTab: function(containerId, tabIndex)
    {
        
        var tabs = PerisianFormPager.getTabs(containerId);
        
        return jQuery(tabs[tabIndex]);
        
    },
    
    /**
     * Retrieves the current tab element, within the specified container.
     * 
     * @author PeterHamm
     * @param String containerId
     * @param int tabIndex
     * @returns Object
     */
    getCurrentTab: function(containerId)
    {
        
        var tabIndex = PerisianFormPager.getCurrentIndex(containerId);
        var tab = PerisianFormPager.getTab(containerId, tabIndex);
        
        return tab;
        
    },
    
    /**
     * Naivgates to the next page in the specified container.
     * 
     * @author Peter Hamm
     * @param String containerId
     * @returns void
     */
    navigateNext: function(containerId)
    {
        
        var containerIdentifier = containerId.replace("#", "");
        var containerElement = jQuery('#' + containerIdentifier);
        
        var currentIndex = PerisianFormPager.getCurrentIndex(containerId);
        
        PerisianFormPager.navigateTo(containerElement, currentIndex + 1);
        
    },
    
    /**
     * Naivgates to the previous page in the specified container.
     * 
     * @author Peter Hamm
     * @param String containerId
     * @returns void
     */
    navigatePrevious: function(containerId)
    {
        
        var containerIdentifier = containerId.replace("#", "");
        var containerElement = jQuery('#' + containerIdentifier);
        
        var currentIndex = PerisianFormPager.getCurrentIndex(containerId);
        
        PerisianFormPager.navigateTo(containerElement, currentIndex - 1);
        
    },
    
    /**
     * Executes the conditions for the specified tab index.
     * 
     * @author Peter Hamm
     * @param int tabIndex
     * @returns void
     */
    executeConditions: function(index)
    {
        
        PerisianFormPager.currentConditionRunId = new Date().getTime();
        
        var identifiersDisplayed = [];
        
        if(PerisianFormPager.conditions[index] && PerisianFormPager.conditions[index].length > 0)
        {
            
            for(var i = 0; i < PerisianFormPager.conditions[index].length; ++i)
            {
                
                var identifier = PerisianFormPager.conditions[index][i].identifier;
                                
                if(identifiersDisplayed.indexOf(identifier) > -1)
                {
                    
                    // A condition for this identifier has been met previously already.
                    // Future conditions for this identifier are being ignored.
                    
                    continue;
                    
                }
                
                var result = PerisianFormPager.conditions[index][i].conditionFunction();
                
                if(result)
                {
                    
                    // This condition has been met. 
                    // Possible future conditions with the same identifier will be ignored.
                    
                    identifiersDisplayed.push(identifier);
                    
                }
                
            }
            
        }
        
        PerisianFormPager.onConditionsExecuted();
        
    },
    
    /**
     * Fired as soon as the conditions got executed.
     * 
     * @author Peter Hamm
     * @returns void
     */
    onConditionsExecuted: function()
    {
        
        var selectedArea = jQuery("input[name='area']:checked");
        
        if(selectedArea.length > 0)
        {
            
            var containerElement = selectedArea.parent().parent();
            
            if(containerElement.css('display') == 'none')
            {
                
                selectedArea.prop('checked', false);
                
            }
            
        }
        
    },
    
    /**
     * Navigates to the specified index within the specified container element.
     * 
     * @author Peter Hamm
     * @param String containerElement
     * @param int index
     * @returns void
     */
    navigateTo: function(containerElement, index)
    {
        
        if(!PerisianFormPager.onBeforeNavigate(containerElement, index))
        {
            
            return;
            
        }

        jQuery('#button-pw-forgotten').css('display', index == 0 ? 'inline-block' : 'none');
        
        // Call the field conditions
        PerisianFormPager.executeConditions(index);
        
        PerisianFormPager.setProgress(containerElement, index);
        PerisianFormPager.showTab(containerElement, index);
        
    }
    
};