
/**
 * Basic system function
 *
 * @author Peter Hamm
 * @date 2010-11-17
 */
var Perisian = {

    containerIdentifier: 'modalContainer',

    /**
     * Global function to show / hide or toggle the AJAX message box and it's background
     *
     * @author Peter Hamm
     * @parameter String action May be 'show', 'hide' or 'toggle'
     * @returns void
     */
    ajaxBox: function(action)
    {

        if(!action)
        {
            action = 'toggle';
        }
        
        var element = jQuery('#' + this.containerIdentifier);

        if(action == 'show')
        {

            element.modal('show');

        }
        else if(action == 'hide')
        {

            element.modal('hide');

        }
        else if(action == 'toggle')
        {

            element.modal('toggle');

        }

    },

    /**
    * This function was inspired by the print_r function of PHP.
    * This will accept some data as the argument and return a
    * text that will be a more readable version of the
    * array / hash / object that is given.
    * 
    * @parameter mixed arr May be an array, hash or object
    * @parameter int level You do not have to specify this.
    * @returns String
    */
    dump: function (arr,level) {

        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";

        
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object')
        {
            for(var item in arr)
            {
                
                var value = arr[item];

                if(typeof(value) == 'object')
                {
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                }
                else
                {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }

            }

        }
        else
        { //Stings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }

        return dumped_text;

    },
    
    /**
     * Generates a unique identifier.
     * 
     * @author Peter Hamm
     * @return String
     */
    generateUniqueIdentifier: function(a = "",b = false)
    {

        var c = Date.now() / 1000;
        var d = c.toString(16).split(".").join("");

        while(d.length < 14)
        {

            d += "0";

        }

        var e = "";

        if(b)
        {

            e = ".";

            var f = Math.round(Math.random() * 100000000);

            e += f;

        }

        return a + d + e;

    },
    
    /**
     * Transforms a specified rgb color value to a hex color value
     * 
     * @author Peter Hamm
     * @param String rgb
     * @returns String
     */
    rgbToHex: function(rgb) 
    {

        var rgbVals = /rgb\((.+),(.+),(.+)\)/i.exec(rgb);

        if(rgbVals === null)
        {
            
            return "#ff8000";
            
        }

        var red = parseInt(rgbVals[1]);
        var green = parseInt(rgbVals[2]);
        var blue = parseInt(rgbVals[3]);

        var redHex = red.toString(16);
        var greenHex = green.toString(16);
        var blueHex = blue.toString(16);

        redHex = (redHex.length < 2 ? "0" : "") + redHex;
        greenHex = (greenHex.length < 2 ? "0" : "") + greenHex;
        blueHex = (blueHex.length < 2 ? "0" : "") + blueHex;

        return '#' + (redHex + greenHex + blueHex).toUpperCase();

    },
    
    /**
    * Adds an URL parameter (or changes it if it already exists)
    * 
    * @param string search The parameter string, e.g. document.location.search
    * @param string key 
    * @param string value 
    */
    addUrlParam: function(search, key, value) 
    {
       
        key = encodeURIComponent(key); 
        value = encodeURIComponent(value);

        var kvp = key + "=" + value;
        var regex = new RegExp("(&|\\?)" + key + "=[^\&]*");

        search = search.replace(regex, "$1" + kvp);

        if(!RegExp.$1) 
        {
            
            search += (search.length > 0 ? '&' : '?') + kvp;
        
        };
       
        return search;
      
    },
    
    /**
     * Initializes the "clear field" buttons.
     * 
     * @author Peter Hamm
     * @param String containerId Optional, default: document
     * @returns void
     */
    initFieldClearButtons: function(containerId)
    {
        
        if(!containerId)
        {
            
            containerId = document;
            
        }
        
        var container = jQuery(containerId);
        
        container.find(".btn-clear").click(function() {
            
            var button = jQuery(this);
            
            var previousInput = button.prev("input");
            
            if(previousInput.attr("perisian-default-value"))
            {
                
                previousInput.val(previousInput.attr("perisian-default-value"));
                
            }
            else
            {
                
                previousInput.val("");
                
            }
                            
            if(previousInput.attr("perisian-data-id"))
            {
                
                // It's an autocomplete field, clear additional data
                previousInput.attr("perisian-data-id", "");
                
            }
            
            if(button.attr("callback"))
            {
                
                eval(button.attr("callback"));
                
            }
            
        });
        
    },
    
    /**
     * Initializes the righthand menu, if available.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initMenuRight: function() 
    {
        
        var menuRight = jQuery('#menubar-right');
        
        if(menuRight.length == 0)
        {
            
            return;
            
        }
        
        menuRight.find('#menubar-right-handle').click(function() {
            
            var toggleButton = jQuery(this);
            
            if(toggleButton.hasClass('menubar-right-handle-closed'))
            {
                
                Perisian.menuRightOpen();
                
            }
            else
            {
                
                Perisian.menuRightClose();
                
            }
            
        });
        
    },
    
    /**
     * Opens the righthand menu, if available.
     * 
     * @author Peter Hamm
     * @returns void
     */
    menuRightOpen: function()
    {
        
        var menuRight = jQuery('#menubar-right');
        var pageBase = jQuery('#base');
        var toggleButton = menuRight.find('#menubar-right-handle');
        var toggleButtonIcon = toggleButton.find('.fa');

        pageBase.removeClass('base-menu-right-closed');
        menuRight.removeClass('menubar-right-closed');
        toggleButton.removeClass('menubar-right-handle-closed');
        
        toggleButtonIcon.removeClass('fa-caret-left');
        toggleButtonIcon.addClass('fa-caret-right');
        
        Perisian.menuRightSaveState(false);
        
    },
    
    /**
     * Closes the righthand menu, if available.
     * 
     * @author Peter Hamm
     * @returns void
     */
    menuRightClose: function()
    {
        
        var menuRight = jQuery('#menubar-right');
        var pageBase = jQuery('#base');
        var toggleButton = menuRight.find('#menubar-right-handle');
        var toggleButtonIcon = toggleButton.find('.fa');
        
        pageBase.addClass('base-menu-right-closed');
        menuRight.addClass('menubar-right-closed');
        toggleButton.addClass('menubar-right-handle-closed');
        
        toggleButtonIcon.removeClass('fa-caret-right');
        toggleButtonIcon.addClass('fa-caret-left');
        
        Perisian.menuRightSaveState(true);
        
    },
    
    /**
     * Saves the state of the righthand menu for the current backend user.
     * 
     * @author Peter Hamm
     * @param bool isOpen
     * @return void
     */
    menuRightSaveState: function(isOpen)
    {
        
        var controller = baseUrl + '/user_backend/settings/';

        var params = {

            'do': 'saveSettings',
            's_184': isOpen ? 1 : 0

        };

        jQuery.post(controller, params, function(data) {

            var decodedData = jQuery.parseJSON(data);

        });
        
    },
    
    /**
     * Reloads the current page with all parameters.
     * 
     * @author Peter Hamm
     * @returns void
     */
    reloadPage: function()
    {
        
        location.href = location.href;
        
    },
    
    /**
     * Retrieves if the user's browser is a mobile one.
     * 
     * @author Peter Hamm
     * @returns bool
     */
    isMobileDevice: function()
    {
        
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
        {

            return true;

        }

        return false;

    },
    
    /**
     * Reloads the style sheets of the current page.
     * 
     * @author Peter Hamm
     * @returns void
     */
    reloadStylesheets: function() 
    {
        
        var queryString = '?reload=' + new Date().getTime();
        
        jQuery('link[rel="stylesheet"]').each(function () {
            
            this.href = this.href.replace(/\?.*|$/, queryString);
            
        });
        
    },
    
    /**
     * Copies the gives string to the clipboard of the browser and displays a short 
     * success message.
     * 
     * @author Peter Hamm
     * @param string string
     * @returns void
     */
    copyToClipboard: function(string)
    {

        clipboard.copy(string);

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-bottom-right",
          "preventDuplicates": true,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "1000",
          "extendedTimeOut": "1000"
        };

        toastr["success"](Language.getLanguageVariable('p5a15f7acf0f4b'));

    }
    
};

jQuery(document).ready(function() {
    
    Perisian.initMenuRight();
    
    jQuery(window).resize(function() 
    {
        
        var windowObject = jQuery(this);
        
        if(windowObject.width() <= 750)
        {
            
            Perisian.menuRightClose();
            
        }
                
    });
    
});