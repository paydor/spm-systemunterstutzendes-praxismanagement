jQuery(document).ready(function() {

    jQuery('#registration_name').focus();
    
    jQuery('#registration').submit(function(e) {
        
        if(!Validation.checkRequired(jQuery('#registration_name_first').val()))
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_name_first');
             e.preventDefault();
            
        }
        else
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_name_first', 'remove');
            
        }
        
        if(!Validation.checkRequired(jQuery('#registration_name_last').val()))
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_name_last');
             e.preventDefault();
            
        }
        else
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_name_last', 'remove');
            
        }
        
        var emailAddress = jQuery('#registration_email').val();
        
        if(!Validation.checkEmail(emailAddress))
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_email');
             e.preventDefault();
            
        }
        else
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_email', 'remove');
            
        }
        
        if(!Validation.checkPassword(jQuery('#registration_pw_one').val(), jQuery('#registration_pw_two').val(), '#password_check_container'))
        {
            
            PerisianBaseAjax.highlightInputTitleError('registration_pw_one');
            PerisianBaseAjax.highlightInputTitleError('registration_pw_two');
        
            e.preventDefault();
            
        }
        else
        {
            
            PerisianBaseAjax.highlightInputTitleError('registration_pw_one', 'remove');
            PerisianBaseAjax.highlightInputTitleError('registration_pw_two', 'remove');
            
        }
                
        if(jQuery('#registration_gtc:checked').length == 0)
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_gtc');
             e.preventDefault();
            
        }
        else
        {
            
             PerisianBaseAjax.highlightInputTitleError('registration_gtc', 'remove');
            
        }
        
    });

});

var showGtc = function() {
    
    jQuery('#modal-gtc').modal('show');
    
}