/**
 * Class to handle basic functions
 *
 * @author Peter Hamm
 * @date 2010-11-24
 */
var PerisianBaseAjax = {
    
    // These need to be initialised on page ready
    // or in the concrete class

    currentOffset: 0,
    currentLimit: 0,
    currentSortOrder: 'DESC',
    currentSorting: 'pk',
    searchTerm: '',
    editId: 0,
    dataSent: false,
    controller: '',
    
    initializedAutocompleteResize: false,
    autocompleteFields: {},
    autocompleteDefaultMaxDisplayedEntries: 5,
        
    /**
     * Shows the form to add a new entry
     *
     * @author Peter Hamm
     * @returns void
     */
    createNewEntry: function (callback)
    {
        
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');

        elementContainer.load(this.controller, {
            
            'do': 'new'
            
        },
        function ()
        {

            Perisian.ajaxBox('show');

            if(callback)
            {
                callback();
            }

        });

    },
    
    /**
     * Retrieves the modal element container 
     * 
     * @author Peter Hamm
     * @returns Object
     */
    getElementContainer: function() 
    {
        
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');
        
        return elementContainer;
        
    },

    /**
     * Resets an upload to a default value.
     * 
     * @author Peter Hamm
     * @param String settingName
     * @returns void
     */
    resetUpload: function(settingName) {

        var sendData = {

            "do" : "resetUpload",
            "setting": settingName

        };

        jQuery.post(this.settingController, sendData, function(data) {

            var callback = JSON.parse(data);

            if(callback.success)
            {

                toastr.success(callback.message);

            }
            else
            {

                toastr.error(callback.message);

            }

        });

    },
    
    /**
     * Shows the form to add a new user or edit an existing one
     *
     * @author Peter Hamm
     * @returns void
     */
    editEntry: function(sendData, callback)
    {
                
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');

        elementContainer.load(this.controller, sendData, function (data) {

            Perisian.ajaxBox('show');

            if(callback)
            {
                
                callback(data);
                
            }
            
            elementContainer.find('[data-toggle="tooltip"]').tooltip();

        });
        
    },

    /**
     * Loads the specified controller with the specified sendData params into the modal.
     * 
     * @author Peter Hamm
     * @param Object sendData
     * @param Function callback
     * @returns void
     */
    loadInModal: function(controller, sendData, callback)
    {

        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');

        elementContainer.load(controller, sendData, function () {

            Perisian.ajaxBox('show');

            if(callback)
            {

                callback();

            }

        });  

    },
    
    /**
     * Saves the entry you are currently editing
     *
     * @author Peter Hamm
     * @parameter Object sendData An object with various data to send
     * @parameter Function validation A function used for validation
     * @parameter Function callback Optional: a callback function
     * @parameter String individualController Optional: The name path to an
     * individual controller for saving
     * @paramater boolean stepTwo Optional: If set to true, this method calls
     * the controller with parameter sendData['do'] and parameter 'step=2'. default: false
     * @parameter boolean surpressErrors Optional: If set to true, this method
     * will not alert any errors that may occur. Default: false
     * @returns void
     */
    saveEntry: function(sendData, validation, callback, individualController, stepTwo, surpressErrors)
    {

        var objectReference = this;

        if(!sendData)
        {
            
            if(typeof console != 'undefined')
            {
                
                console.log(Language.getLanguageVariable(10088));
                
            }
            
            return;
            
        }

        if(objectReference.dataSent)
        {

            if(typeof console != 'undefined')
            {
                
                console.log(Language.getLanguageVariable(10089));
                
            }

            return;

        }

        if(validation)
        {

            if(!validation(sendData))
            {

                if(typeof console != 'undefined')
                {
                    
                    console.log(Language.getLanguageVariable(10093));
                    
                }

                return;

            }

        }

        jQuery("#submitButton").attr("disabled", true);
        this.dataSent = true;

        var controller = this.controller;

        if(individualController)
        {
            
            controller = individualController;
            
        }

        jQuery.post(controller, sendData, function (data) {
            
            var newId = data;
            
            var errorMessage = '';
            
            try
            {
                
                var callbackData = JSON.parse(data);
                
                if(callbackData && typeof callbackData.success != "undefined" && callbackData.success === false)
                {
                    
                    newId = null;
                    
                    if(callbackData.message)
                    {
                        
                        errorMessage = callbackData.message;
                        
                    }
                    
                }
                else if(callbackData && callbackData.id)
                {

                    newId = callbackData.id;

                }
                
            }
            catch(e)
            {
                
            }
            
            if((newId == null || newId.substr(0, 5) == 'error' || newId.length == 0) && !surpressErrors)
            {
                
                if(errorMessage.length == 0)
                {
                    
                    errorMessage = Language.getLanguageVariable(10222);
                    
                }

                toastr.error(errorMessage);
                
                jQuery("#submitButton").attr("disabled", false);
                objectReference.dataSent = false;
                
            }
            else
            {

                if(stepTwo)
                {

                    var sendDataNew = {

                        'editId': newId,
                        'do': sendData['do'],
                        'step': 2

                    };

                    var element = jQuery("#" + Perisian.containerIdentifier);
                    var elementContainer = element.find('.modal-content');

                    elementContainer.load(controller, sendDataNew, function() {
                        
                        jQuery("#submitButton").attr("disabled", false);
                        objectReference.dataSent = false;
                        
                    });

                }
                
            }

            if(callback)
            {
                
                callback(newId);
                
            }

        });

    },
    
    /**
     * Deletes the entry with the specified ID
     *
     * @author Peter Hamm
     * @returns void
     */
    deleteEntry: function(deleteId, questionLanguageId, callback)
    {

        if(!questionLanguageId)
        {
            
            questionLanguageId = 10100;
            
        }

        if(confirm(Language.getLanguageVariable(questionLanguageId)))
        {

            jQuery.post(this.controller, {
                
                'deleteId': deleteId,
                'do': 'delete'
                
            }, function(data) {
                
                jQuery('.tooltip').remove();
                
                if(callback)
                {
                    
                    callback(data);
                    
                }

            });

        }

    },
    /**
     * Loads the list
     *
     * @author Peter Hamm
     * @parameter int offset An individual offset of entries >= 0
     * @parameter int limit An individual limit > 0
     * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
     * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
     * @parameter boolean search Optional: Set this to true if you want to perform a new search
     * @returns void
     */
    showEntries: function (offset, limit, sortBy, sortOrder, initSearch)
    {

        if(!offset && !limit)
        {
            offset = this.currentOffset;
            limit = this.currentLimit;
        }

        if(!sortBy)
        {
            sortBy = this.currentSorting;
        }

        if(!sortOrder)
        {
            sortOrder = this.currentSortOrder;
        }

        if(offset < 0 || limit <= 0)
        {
            return;
        }

        if(initSearch)
        {
            this.searchTerm = jQuery('#search').val();
        }

        jQuery("#list").load(this.controller, {
            
            'do': 'list',
            'sorting': sortBy,
            'order': sortOrder,
            'offset': offset,
            'limit': limit,
            'search': this.searchTerm
            
        },
        function ()
        {
            
            this.currentOffset = offset;
            this.currentLimit = limit;
            this.currentSorting = sortBy;
            this.currentSortOrder = sortOrder;
            
        });

    },
    /**
     * Hides the form to edit
     *
     * @author Peter Hamm
     * @returns void
     */
    cancel: function ()
    {
        
        this.dataSent = false;
        Perisian.ajaxBox('hide');
        
    },
    
    /**
     * Use this for example to highlight fields with validation errors
     *
     * @author Peter Hamm
     * @returns void
     */
    highlightInputTitleError: function(fieldName, action)
    {
        
        var element = jQuery("#" + fieldName).closest('.form-group');
        
        if(!action)
        {
            element.addClass('has-error');
        }
        else if(action == 'remove')
        {
           element.removeClass('has-error');
        }

    },
    
    /**
     * Gets all form data from input fields from visible
     * objects matching the selection string
     *
     * @author Peter Hamm
     * @parameter String selection A jQuery selection string
     * @parameter String excludePattern Optional: A string to match the beginning
     * of input field IDs that shall not be included in the output!
     * @returns Object A JSON object
     */
    getInputData: function (selection, excludePattern)
    {

        var contents = jQuery(selection);

        var dataJson = {};
        var i = 0;

        jQuery.each(contents, function (key, content)
        {

            if(jQuery(content).css('display') != 'none')
            {

                var subJson = {};
                var currentId = '';
                var oldValue = '';
                var counters = new Array();

                jQuery.each(jQuery(content).find('input, textarea, select'), function (inputKey, inputField)
                {

                    currentId = jQuery(inputField).attr('id');

                    if(!excludePattern || currentId.substr(0, excludePattern.length) != excludePattern)
                    {

                        if(typeof (subJson[currentId]) == 'undefined')
                        {
                            subJson[currentId] = jQuery(inputField).val();
                        }
                        else if(typeof (subJson[currentId]) == 'string')
                        {

                            oldValue = subJson[currentId];

                            subJson[currentId] = {
                                '0': oldValue,
                                '1': jQuery(inputField).val()
                            };

                            counters[currentId] = 2;

                        }
                        else if(typeof (subJson[currentId]) == 'object')
                        {

                            subJson[currentId][counters[currentId]] = jQuery(inputField).val();
                            ++counters[currentId];

                        }

                    }

                });

                dataJson[i] = subJson;
                ++i;

            }

        });

        return dataJson;

    },
    /**
     * Toggles the sorting of the current list
     *
     * @author Peter Hamm
     * @parameter String sortBy DESC or ASC
     * @returns void
     */
    toggleSorting: function (sortBy)
    {

        if(this.currentSorting == sortBy)
        {

            if(this.currentSortOrder == 'DESC')
            {
                this.currentSortOrder = 'ASC';
            }
            else
            {
                this.currentSortOrder = 'DESC';
            }

        }
        else
        {

            this.currentSorting = sortBy;

        }

        this.showEntries();

    },
    
    /**
     * Displays an autocomplete box for the specified result.
     * 
     * @author Peter Hamm
     * @param Object result {element: Object, matching: Array, fieldToIdentify: String, fieldToDisplay: String, customRenderer: function, onSelect: function, maxDisplayedEntries: int, keyDownCallback: function(currentFieldValue, possibleElements)}
     * @returns void
     */
    showAutocompleteResult: function(result)
    {
        
        var that = this;
        
        this.initializeAutocompleteResize();
           
        if(!result || result.length == 0)
        {
            
            return;
            
        }
        
        var box = this.getAutocompleteBox(result.element);
        
        // Hide the autocomplete box on blur
        result.element.blur(function() {
            
            var hitElement = jQuery(this);
            
            hitElement.unbind('keyup');
            
            PerisianBaseAjax.hideAutocompleteBox(this);
            
        });
        
        if(result.keyDownCallback)
        {
            
            result.element.keyup(function() {
                
                if(!result['__originalMatch'])
                {
                    
                    result['__originalMatch'] = result['matching'];
                    
                }
                
                var box = that.getAutocompleteBox(result.element);
                
                var element = jQuery(result.element);
                
                var newResults = result.keyDownCallback(element.val(), result['__originalMatch']);   
                
                result['matching'] = newResults;
                                
                that.fillAutocompleteBox(box, result);
                that.autocompleteFields[result.element.attr('id')] = result;
                
            });
            
        }
        
        this.fillAutocompleteBox(box, result);
        
        this.autocompleteFields[result.element.attr('id')] = result;

    },
    
    /**
     * Retrieves the autocomplete-filled object data from a field.
     * 
     * @author Peter Hamm
     * @param String fieldId The identifier of the field you want the object values from
     * @returns Object
     */
    getAutocompleteObjectValue: function(fieldId)
    {
        
        var element = jQuery(fieldId);
        
        var autocompleteList = this.autocompleteFields[fieldId.replace("#", "")];
        
        if(!autocompleteList)
        {
            
            return null;
            
        }
        
        var keyField = autocompleteList['fieldToIdentify'];
        var displayField = autocompleteList['fieldToDisplay'];
        
        var result = null;
        
        var escapedValue = jQuery('<div/>').text(element.val()).html();
                
        for(var i = 0; i < autocompleteList['matching'].length; ++i)
        {
            
            if(autocompleteList['matching'][i][displayField] == escapedValue)
            {
                
                result = autocompleteList['matching'][i];
                
                break;
                
            }
            
        }
        
        if(result != null)
        {
            
            result['__keyField'] = keyField;
            result['__displayField'] = displayField;
            
        }
            
        return result;
        
    },
    
    /**
     * Clears all the object values currently stored for the specified autocomplete field.
     * 
     * @author Peter Hamm
     * @param String fieldId
     * @returns void
     */
    clearAutocompleteFieldValue: function(fieldId)
    {
        
        var field = jQuery(fieldId);
        
        field.val('');
        field.attr('perisian-data-id', '');
        
    },
    
    /**
     * Retrieves the autocomplete-filled identifier value from a field.
     * 
     * @author Peter Hamm
     * @param String fieldId The identifier of the field you want the value from, e.g. #fieldId
     * @returns Object
     */
    getAutocompleteFieldIdentifier: function(fieldId)
    {
               
        var entry = this.getAutocompleteObjectValue(fieldId);
        var result = null;
        
        if(entry != null)
        {
            
            result = entry[entry['__keyField']];
            
        }
        else
        {
            
            // Try to find it directly in the field, maybe just the autocomplete list didn't load yet.
            
            var element = jQuery(fieldId);
            result = element.attr('perisian-data-id');
            
        }
        
        return result;
        
    },
    
    /**
     * Retrieves the autocomplete-filled display value from a field.
     * 
     * @author Peter Hamm
     * @param String fieldId The identifier of the field you want the value from, e.g. #fieldId
     * @returns Object
     */
    getAutocompleteFieldValue: function(fieldId)
    {
               
        var entry = this.getAutocompleteObjectValue(fieldId);
        var result = null;
        
        if(entry != null)
        {
            
            result = entry[entry['__displayField']];
            
        }
        else
        {
            
            // Try to find it directly in the field, maybe just the autocomplete list didn't load yet.
            
            var element = jQuery(fieldId);
            result = element.val();
            
        }
        
        return result;
        
    },
    
    selectNextAutoCompleteElement: function(element, direction)
    {
        
        var indexToSelect = 0;
        var indexCurrentlySelected = 0;
                
        var elementIdentifier = ".perisian-autocomplete-container [data-id='" + element.attr('id') + "']";
        var itemContainer = jQuery(elementIdentifier);
                
        itemContainer.css('border-color', 'red');
        
    },
    
    /**
     * Creates an autocomplete input field attachment
     * 
     * @author Peter Hamm
     * @param Object data {element: Object, url: String, fieldToIdentify: String, fieldToDisplay: String, data: Object, forceEntryFromList: boolean, renderer: function(Object matchingObject, String fieldToIdentify, String fieldToDisplay), onSelect: function(selectedElement), int maxDisplayedEntries}
     * @returns void
     */
    createAutocomplete: function(data)
    {
                
        var element = data.element;
        var url = data.url;
        var fieldToIdentify = data.fieldToIdentify;
        var fieldToDisplay = data.fieldToDisplay;
        var forceEntryFromList = data.forceEntryFromList;
        var additionalData = data.data;
        var customRenderer = data.renderer;
        var onSelect = data.onSelect;
        var maxDisplayedEntries = data.maxDisplayedEntries;
        
        if(!element || element == null || !url || url == null || !fieldToIdentify || fieldToIdentify == null || !fieldToDisplay || fieldToDisplay == null)
        {
            
            console.log(Language.getLanguageVariable(10783));
            
            return;
            
        }
        
        jQuery(data.element).off('blur').on('blur', function() {
            
            var element = jQuery(this);
            
            if(element.val() == '')
            {
                
                element.attr('perisian-data-id', '');
                
            }
            
        });
        
        var sendData = {

            'do': 'getJson',
            'source': element.data('source')

        };
        if(additionalData != null && Object.keys(additionalData).length > 0)
        {
            
            for(var attribute in additionalData) 
            { 
                                
                sendData[attribute] = additionalData[attribute]; 
            
            }
            
        }
        
        sendData.search = jQuery(element).val();

        jQuery.ajax({

            url: url,
            data: sendData,
            dataType: "json",

            success: function(data) {
                
                var entries = data.data ? data.data : data;
                
                var perisianResponse = function(element, matching) { 
                                        
                    PerisianBaseAjax.showAutocompleteResult({

                        'element': element,
                        'matching': matching,
                        'fieldToIdentify': fieldToIdentify,
                        'fieldToDisplay': fieldToDisplay,
                        'forceEntryFromList': forceEntryFromList,
                        'customRenderer': customRenderer,
                        'onSelect': onSelect,
                        'maxDisplayedEntries': maxDisplayedEntries

                    });
                    
                };
                
                // Call it directly, to display all possible entries.
                perisianResponse(element, entries);
                                
                element.autocomplete({
                    
                    minLength: 0,

                    source: function(request, response) {
                        
                        var matcher = new RegExp(jQuery.ui.autocomplete.escapeRegex(request.term), "i");

                        var matching = jQuery.grep(entries, function (value) {
                            
                            var name = value[fieldToDisplay];
                            var id = value[fieldToIdentify];

                            return matcher.test(name) || matcher.test(id);
                            
                        });
                        
                        response(matching);
                        
                        perisianResponse(element, matching);
                        
                    },
                    
                    focus: function (e, ui) {
                        
                        e.preventDefault();
                        
                        var direction = '';
                    
                        if(e.keyCode == 38)
                        {
                            
                            direction = 'up';
                        
                        }
                        else if(e.keyCode == 40)
                        {
                            
                            direction = 'down';
                        
                        }
                        
                        if(direction == 'up' || direction == 'down')
                        {
                            
                            PerisianBaseAjax.selectNextAutoCompleteElement(jQuery(this), direction);
                            
                        }
                        
                    },

                });
               
            }

        });
        
    },
    
    /**
     * Fills the autocomplete box with the provided results.
     * 
     * @author Peter Hamm
     * @param Object box
     * @param Object resuult
     * @returns void
     */
    fillAutocompleteBox: function(box, result)
    {
        
        var results = result.matching;
        var fieldToIdentify = result.fieldToIdentify;
        var fieldToDisplay = result.fieldToDisplay;
        var customRenderer = result.customRenderer;
        var onSelect = result.onSelect;
        var maxDisplayedEntries = result.maxDisplayedEntries;
                
        box.empty();
        
        if(!results || results.length == 0)
        {

            newItem = '<div class="perisian-autocomplete-container-element-empty">' + Language.getLanguageVariable(10787) + "</div>";

            box.append(newItem);
            
            return;
            
        }
        else
        {
            
            var displayLimit = maxDisplayedEntries < 0 ? results.length : (maxDisplayedEntries > 0 ? maxDisplayedEntries : this.autocompleteDefaultMaxDisplayedEntries);
            displayLimit = displayLimit > results.length ? results.length : displayLimit;
            
            for(var i = 0; i < displayLimit; ++i)
            {

                var newItem = '';

                if(customRenderer && customRenderer != null)
                {

                    newItem = customRenderer(results[i], fieldToIdentify, fieldToDisplay);

                }
                else
                {

                    newItem = '<div class="perisian-autocomplete-container-element"';

                    newItem += ' data-id="' + fieldToIdentify + '"';
                    newItem += ' data-value="' + results[i][fieldToDisplay] + '"';

                    newItem += ' data-displayfield="' + fieldToDisplay + '">' + results[i][fieldToDisplay] + "</div>";

                }

                box.append(newItem);

            }
            
        }
        
        box.find('.perisian-autocomplete-container-element').mousedown(function(e) {
            
            e.preventDefault();
            e.stopImmediatePropagation();
            
        });
        
        box.find('.perisian-autocomplete-container-element').bind('click', {'onSelect': onSelect}, this.onAutocompleteResultClick);
        
    },
    
    /**
     * Handles clicks on the autocomplete result fields
     * 
     * @param Object e Event
     * @returns void
     */
    onAutocompleteResultClick: function(e) 
    {
        
        var element = jQuery(this);
        var hostFieldId = '#' + element.parent().attr('data-id');
        
        var fieldToDisplay = element.attr('data-displayfield');
        var hostField = element.parent().parent().find(hostFieldId);
        
        var attributes = [];
        
        hostField.val(element.attr("data-value"));
        
        if(e.data && e.data.onSelect)
        {
            
            var selectedObject = PerisianBaseAjax.getAutocompleteObjectValue(hostFieldId);
                        
            e.data.onSelect(selectedObject);
            
        }
        
        hostField.change();
        
        hostField.blur();
        
    },
    
    /**
     * Initializes an event that automatically resizes the autocomplete boxes when the window gets resized.
     * 
     * @author Peter Hamm
     * @returns void
     */
    initializeAutocompleteResize: function()
    {
        
        if(this.initializedAutocompleteResize)
        {
            
            return;
            
        }
        
        jQuery(window).resize(function() {
            
            jQuery('.perisian-autocomplete-container').each(function() {
                
                var box = jQuery(this);
                var hostElement = jQuery("#" + box.attr("data-id"));
                
                box.css('width', hostElement.css('width'));
                
            });
            
        });
        
        this.initializedAutocompleteResize = true;
        
    },
    
    /**
     * Removes the autocomplete box
     * 
     * @author Peter Hamm
     * @param Object element
     * @return void
     */
    hideAutocompleteBox: function(element)
    {
        
        var elementId = jQuery(element).attr('id');
        
        var box = PerisianBaseAjax.getAutocompleteBox(jQuery(element));
        var autocompleteList = PerisianBaseAjax.autocompleteFields[elementId];
        
        // Enforce an entry from the list? If yes and the currently selected value is not contained, it is emptied.
        if(autocompleteList && autocompleteList.forceEntryFromList)
        {
                        
            if(!PerisianBaseAjax.isAutocompleteSelectedValueInList(elementId))
            {
                
                PerisianBaseAjax.clearAutocompleteFieldValue('#' + elementId);
                
                if(autocompleteList.onSelect)
                {
                    
                    autocompleteList.onSelect({});
                    
                }
                
            }
            
        }
        
        box.remove();
        
    },
    /**
     * Checks if the currently set value of an autocomplete field is in the list or not.
     * 
     * @author Peter Hamm
     * @param String elementId elementId
     * @returns boolean
     */
    isAutocompleteSelectedValueInList: function(elementId)
    {
        
        elementId = elementId.replace('#', "");
                
        var autocompleteList = PerisianBaseAjax.autocompleteFields[elementId];
        
        var currentFieldValue = PerisianBaseAjax.getAutocompleteFieldValue('#' + elementId);
        var currentFieldIndex = PerisianBaseAjax.getAutocompleteFieldIdentifier('#' + elementId);
        
        var fieldToDisplay = autocompleteList.fieldToDisplay;
        var fieldToIdentify = autocompleteList.fieldToIdentify;
        
        for(var i = 0; i < autocompleteList['matching'].length; ++i)
        {
            
            var currentIndex = autocompleteList['matching'][i][fieldToIdentify];
            var currentValue = autocompleteList['matching'][i][fieldToDisplay];
            
            if(currentFieldValue == currentValue && currentFieldIndex == currentIndex)
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    },
    
    /**
     * Removes all autocomplete boxes
     * 
     * @author Peter Hamm
     * @param Object element
     * @return void
     */
    hideAutocompleteBoxes: function()
    {
        
        var boxes = jQuery('.perisian-autocomplete-container');
        boxes.remove();
        
    },
    
    /**
     * Retrieves an autocomplete box next to the specified element.
     * The box is created if it doesn't exist yet.
     * 
     * @author Peter Hamm
     * @param Object element
     * @returns Object
     */
    getAutocompleteBox: function(element)
    {
        
        var elementIteration = element.attr("data-iteration");
                
        var searchString = ".perisian-autocomplete-container[data-id='" + element.attr("id") + "']";
        
        if(elementIteration && elementIteration.length > 0)
        {
            
            searchString += '[data-iteration="' + elementIteration + '"]';
            
        }
        
        var box = element.parent().find(searchString);
                
        if(box.length == 0)
        {
            
            // A new box needs to be created.
            
            var newBox = '<div class="perisian-autocomplete-container" data-id="' + element.attr("id") + '"';
                    
            if(elementIteration && elementIteration.length > 0)
            {
                
                newBox += 'data-iteration="' + elementIteration + '"';
                
            }
                    
            newBox += '></div>';
            
            element.after(newBox);
            
            return this.getAutocompleteBox(element);
            
        }
        
        box.css('width', element.css('width'));
        
        return box;
        
    },
    
    /**
     * Goes to the specified URL
     * 
     * @param String url
     * @returns void
     */
    goToUrl: function(url)
    {
        
        var prefix = '';
        
        if(url.substr(0, 7) == "http://")
        {
            
            url = url.substr(7);
            
            prefix = "http://";
            
        }
        else if(url.substr(0, 8) == "https://")
        {
            
            url = url.substr(8);
            
            prefix = "https://";
            
        }
        
        if(url.substr(0, 1) == "/")
        {
            
            url = url.substr(1);
            
        }
        
        url = url.replace("//", "/");
        
        url = prefix + url;
        
        location.href = url;
        
    },
    
    /**
     * Enrichens the specified sendDataObject with data gathered from input/select fields
     * found withing the container with the specified containerId and with the specified String prefix in the id.
     * 
     * @param Object sendData
     * @param String containerId For example #modalContent
     * @param String idPrefix For example "user_"
     * @returns Object
     */
    enhanceSaveDataWithFields: function(sendData, containerId, idPrefix)
    {
        
        jQuery(containerId).find('[id^="' + idPrefix + '"]').each(function() {

            var element = jQuery(this);
            
            if(element.is(":checkbox"))
            {
                
                sendData[element.attr('id')] = element.is(':checked') ? 1 : 0;
                
            }
            else
            {
                
                sendData[element.attr('id')] = element.val();
            
            }

        });
        
        return sendData;
        
    }

}