jQuery(document).ready(function() {

    jQuery('#rp_pw_one').focus();
    
    jQuery('#password_set').submit(function(e) {
        
        if(!Validation.checkPassword(jQuery('#rp_pw_one').val(), jQuery('#rp_pw_two').val(), '#password_check_container'))
        {
            
            PerisianBaseAjax.highlightInputTitleError('rp_pw_one');
            PerisianBaseAjax.highlightInputTitleError('rp_pw_two');
        
            e.preventDefault();
            
        }
        else
        {
            
            PerisianBaseAjax.highlightInputTitleError('rp_pw_one', 'remove');
            PerisianBaseAjax.highlightInputTitleError('rp_pw_two', 'remove');
            
        }
        
    });

});