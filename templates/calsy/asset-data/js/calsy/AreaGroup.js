/**
 * Class to handle area groups
 *
 * @author Peter Hamm
 * @date 2017-09-11
 */
var AreaGroup = jQuery.extend(true, {}, PerisianBaseAjax);

AreaGroup.controller = baseUrl + "/area/group/";

AreaGroup.currentSortOrder = 'ASC';
AreaGroup.currentSorting = 'calsy_area_group_title';
AreaGroup.areaGroupList = null;

AreaGroup.createNewAreaGroup = function()
{
   
    AreaGroup.editAreaGroup();
    
};

/**
 * Retrieves a list of all area groups
 * 
 * @author Peter Hamm
 * @returns array
 */
AreaGroup.getAreaGroupList = function()
{
    
    if(AreaGroup.areaGroupList != null)
    {
        
        return AreaGroup.areaGroupList;
        
    }
    
    var sendData = {

        'do': 'getAreaGroupList'
        
    };

    jQuery.ajax({
        
        url: AreaGroup.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            AreaGroup.areaGroupList = decodedData.list;
                        
        }
        
    });
    
    return AreaGroup.areaGroupList;
    
};

AreaGroup.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

AreaGroup.editAreaGroup = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

/**
 * Should be fired when the edit form for an area group is being opened.
 * 
 * @author Peter Hamm
 * @returns void
 */
AreaGroup.initEditAreaGroup = function()
{
    
    jQuery('#calsy_area_title').focus();
    
};

/**
 * Gets the selected areas for the currently edited entry
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
AreaGroup.getSelectedAreas = function(asCsv)
{
    
    return AreaGroup.getSelectedAreasFromSelector('#area-group-area-selector', asCsv);
    
};

/**
 * Gets the selected areas for the currently edited entry
 * 
 * @author Peter Hamm
 * @param String selectorId The identifier of the user selector DOM element.
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
AreaGroup.getSelectedAreasFromSelector = function(selectorId, asCsv)
{
    
    var returnValue = [];
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var selectorElement = elementContainer.find(selectorId);
    
    selectorElement.find("li.user-selector").each(function() {
        
        var element = jQuery(this);
        var userId = element.attr('data-id');
        
        var isChecked = element.find(":checkbox").prop("checked");
        
        if(isChecked)
        {
            
            returnValue.push(userId);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

AreaGroup.saveAreaGroup = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        "areas": AreaGroup.getSelectedAreas()
        
    }, "#" + Perisian.containerIdentifier, "calsy_area_group_");
            
    if(!AreaGroup.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            AreaGroup.cancel(); 
            AreaGroup.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

AreaGroup.deleteAreaGroup = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        AreaGroup.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

AreaGroup.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        AreaGroup.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_area_group_title))
    {

        AreaGroup.highlightInputTitleError('calsy_area_group_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
AreaGroup.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
AreaGroup.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
AreaGroup.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};