/**
 * Class to handle checkins
 *
 * @author Peter Hamm
 * @date 2020-06-15
 */
var CheckIn = jQuery.extend(true, {}, PerisianBaseAjax);

CheckIn.controller = baseUrl + "/checkin/overview/";

CheckIn.currentSortOrder = 'DESC';
CheckIn.currentSorting = 'calsy_checkin_time_in';
CheckIn.checkInList = null;
CheckIn.filterUserFrontendId = null;
    
CheckIn.createNewCheckIn = function()
{
    
    CheckIn.editCheckIn();
    
};

/**
 * Retrieves a list of all checkins
 * 
 * @author Peter Hamm
 * @returns array
 */
CheckIn.getCheckInList = function()
{
    
    if(CheckIn.checkInList != null)
    {
        
        return CheckIn.checkInList;
        
    }
    
    var sendData = {

        'do': 'getCheckInList'
        
    };

    jQuery.ajax({
        
        url: CheckIn.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            CheckIn.checkInList = decodedData.list;
                        
        }
        
    });
    
    return CheckIn.checkInList;
    
};

CheckIn.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

CheckIn.editCheckIn = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

CheckIn.selectUserFrontend = function(object) 
{

    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', object['calsy_user_frontend_id']);

};

CheckIn.resetUserFrontend = function() 
{
    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', '');

};

/**
 * Should be fired when the edit form for a checkin is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
CheckIn.initEditCheckIn = function()
{
        
    jQuery('#calsy_checkin_user_frontend').focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_user_frontend_id',
            'fieldToDisplay': 'calsy_user_frontend_fullname',
            'forceEntryFromList': true,

            'url': CheckIn.controller,

            'renderer': CalsyAutocompleteRenderer.renderUserFrontend,

            'onSelect': CheckIn.selectUserFrontend

        });

    });
    
    CheckIn.initDatePicker();
    
};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
CheckIn.initDatePicker = function()
{

    var element = jQuery('#modalContainer');

    element.find('#calsy_checkin_time_in_field_date, #calsy_checkin_time_out_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_checkin_time_in_field_time, #calsy_checkin_time_out_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};

CheckIn.saveCheckIn = function()
{
    
    var editId = jQuery("#editId").val();
    
    var dstOffset = Calendar.isBrowserInDaylightSavingTime() ? Calendar.getDstOffsetForTimestamp() : 0;
     
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        
        "calsy_checkin_user_frontend_id": jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id'),
        "calsy_checkin_time_in": parseInt(Calendar.getTimestampFromFields("#calsy_checkin_time_in_field_date", "#calsy_checkin_time_in_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds(),
        "calsy_checkin_time_out": parseInt(Calendar.getTimestampFromFields("#calsy_checkin_time_out_field_date", "#calsy_checkin_time_out_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds()
        
    }, "#" + Perisian.containerIdentifier, "calsy_checkin_");
            
    if(!CheckIn.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            CheckIn.cancel(); 
            CheckIn.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

CheckIn.deleteCheckIn = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        CheckIn.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

CheckIn.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        CheckIn.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    /*
    if(!Validation.checkRequired(sendData.calsy_area_title))
    {

        Area.highlightInputTitleError('calsy_area_title', false);
        
        ++errors;

    }
    */
   
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
CheckIn.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
CheckIn.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'u': CheckIn.filterUserFrontendId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
CheckIn.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};