/**
 * Class to handle calendar entries (/events)
 *
 * @author Peter Hamm
 * @date 2016-02-06
 */
var Event = jQuery.extend(true, {}, PerisianBaseAjax);

Event.controller = baseUrl + "/event/overview/";
Event.editController = baseUrl + "/calendar/overview/";

Event.currentSortOrder = 'ASC';
Event.currentSorting = 'pk';

Event.filterUserFrontendId = 0;
Event.filterOrderId = 0;
Event.filterUserId = 0;
Event.filterResourceId = 0;
Event.filterAreaId = 0;
Event.filterShowUnconfirmed = 0;
Event.filterShowCanceled = 0;

Event.timestampBegin = -1;
Event.timestampEnd = -1;
    
Event.createNewEvent = function()
{
    
    Event.editEvent();
    
};

Event.exportIcs = function(exportId)
{
    
    window.open(Event.controller + "?do=exportICS&c=" + exportId, "_blank");
    
}

Event.editEvent = function(editId)
{
        
    Calendar.editEvent(editId);
    
};

Event.infoEvent = function(editId)
{
        
    Calendar.infoEvent(editId);
    
};

Event.deleteEvent = function(editId)
{
        
    Calendar.deleteEntryById(editId);
    
    setTimeout(function() {
        
        jQuery('.tooltip').remove();
        
    }, 250);
    
};

Event.updateParameters = function()
{
        
    Event.filterUserFrontendId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_user_frontend_id");
    Event.filterOrderId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_order_id");
    Event.filterUserId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_user_id");
    Event.filterResourceId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_resource_id");
    Event.filterAreaId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_area_id");
    Event.filterShowUnconfirmed = jQuery('#filter_show_unconfirmed').css('display') == 'block' ? 0 : 1;
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
Event.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    Event.updateParameters();

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }
    
    jQuery('#list').css('display', 'none');
    jQuery('#list-loading-indicator').css('display', 'block');

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'p': Event.filterUserFrontendId,
        'r': Event.filterOrderId,
        'u': Event.filterUserId,
        're': Event.filterResourceId,
        'a': Event.filterAreaId,
        'su': Event.filterShowUnconfirmed,
        'c': Event.filterShowCanceled,
        'tb': Event.timestampBegin,
        'te': Event.timestampEnd,
        'search': this.searchTerm

    }, function() {
        
        jQuery('#list-loading-indicator').css('display', 'none');
        jQuery('#list').css('display', 'block');

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
Event.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};

Event.showUnconfirmed = function()
{
    
    jQuery("#filter_show_unconfirmed").css('display', 'none');
    jQuery("#filter_show_confirmed").css('display', 'block');
    
    Event.showEntries();
    
};

Event.showConfirmed = function()
{
    
    jQuery("#filter_show_unconfirmed").css('display', 'block');
    jQuery("#filter_show_confirmed").css('display', 'none');
    
    Event.showEntries();
    
};

Event.showPast = function()
{
    
    var timestamp = parseInt(new Date().getTime() / 1000);
    
    Event.timestampBegin = -1;
    Event.timestampEnd = timestamp;
    Event.currentSortOrder = "DESC";
    
    jQuery("#event-button-show-past").css('display', 'none');
    jQuery("#event-button-show-future").css('display', 'block');
    
    Event.showEntries(0, Event.currentLimit);
    
};

Event.showFuture = function()
{
    
    var timestamp = parseInt(new Date().getTime() / 1000);
        
    Event.timestampBegin = timestamp;
    Event.timestampEnd = -1;
    Event.currentSortOrder = "ASC";
    
    Event.showEntries(0, Event.currentLimit);
    
    jQuery("#event-button-show-past").css('display', 'block');
    jQuery("#event-button-show-future").css('display', 'none');
    
};