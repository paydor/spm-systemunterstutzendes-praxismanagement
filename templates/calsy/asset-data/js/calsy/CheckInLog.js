/**
 * Class to handle checkin log entries
 *
 * @author Peter Hamm
 * @date 2020-06-29
 */
var CheckInLog = jQuery.extend(true, {}, PerisianBaseAjax);

CheckInLog.controller = baseUrl + "/checkin/log/";

CheckInLog.currentSortOrder = 'DESC';
CheckInLog.currentSorting = 'calsy_checkin_log_time_log';
CheckInLog.checkInList = null;
CheckInLog.filterUserFrontendId = null;

CheckInLog.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

CheckInLog.selectUserFrontend = function(object) 
{

    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', object['calsy_user_frontend_id']);

};

CheckInLog.resetUserFrontend = function() 
{
    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', '');

};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
CheckInLog.initDatePicker = function()
{

    var element = jQuery('#modalContainer');

    element.find('#calsy_checkin_time_in_field_date, #calsy_checkin_time_out_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_checkin_time_in_field_time, #calsy_checkin_time_out_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
CheckInLog.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'u': CheckInLog.filterUserFrontendId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
CheckInLog.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};