
jQuery(document).ready(function() {
    
    Calendar.initAutoCompleteForUserBackendWithResource();
    Calendar.initAutocompleteForUserFrontendWithOrders();
    
    Calendar.initTimeMask();
    Calendar.initFormBackend();
    
});