
var CalendarContextMenu = {
    
    copyPasteIdentifier: null,
    
    eventContainer: null,
    
    contextMenuId: '#calendarContextMenu',
    contextMenuElement: null,
    contextElement: null,
    
    disableCopy: false,
    disablePaste: false,
    
    init: function(eventContainer) 
    {
        
        this.eventContainer = eventContainer == null ? '.fc-event' : eventContainer;
        
        // Insert the actual context element
        {
            
            var menuMarkup = '<ul id="' + this.contextMenuId.substr(1) + '" class="calendar-context-menu"></ul>';
            jQuery('body').append(menuMarkup);
            
            this.contextMenuElement = jQuery(this.contextMenuId);
            
        }
        
        jQuery(this.eventContainer).bind("contextmenu", function (event) {
            
            var element = CalendarContextMenu.getClickedElement(event);
            
            CalendarContextMenu.showContextMenu(element, event);
            
        });

        jQuery(document).bind("mousedown", function (e) {
            
            // A click happened somewhere on the document, not the context menu... hide it!

            if(!jQuery(e.target).parents(CalendarContextMenu.contextMenuId).length > 0) 
            {

                CalendarContextMenu.hideContextMenu();
                
            }
            
        });
        
        this.cacheLanguageVariables();
        
    },
    
    getClickedElement: function(event) 
    {
                
        var element = jQuery(event.target);
        
        var eventClass = 'fc-event'

        if(element.hasClass('fc-day') || element.hasClass('fc-event-container') || element.hasClass('fc-widget-content'))
        {
            
            // Show it here only if we can paste something into it,
            // e.g. when an entry was copied before.
            
            if(this.copyPasteIdentifier == null)
            {
                
                return null;
                
            }
            
            this.disableCopy = true;
            
        }
        else
        {

            var goOn = false;

            // Check if one of the parents has the proper class...

            while(element != jQuery(document))
            {

                if(element.hasClass(eventClass))
                {

                    goOn = true;

                    break;

                }

                element = jQuery(element.parent());

                if(element.length == 0)
                {

                    break;

                }

            }

            if(!goOn)
            {

                return null;

            }
            
            this.disablePaste = true;
            
            // This is a holiday, ignore it
            if(element.hasClass('fc-holiday'))
            {

                return null;

            }
            
            // Check if the user may edit this appointment
            {
                
                var ownerId = element.attr('data-owner');
                
                if(ownerId != Calendar.userId && !Calendar.userMayEditForOtherUsers)
                {
                                        
                    return null;
                    
                }
                
            }

        }
        
        return element;
        
    },
    
    doDelete: function(eventId) 
    {
        
        Calendar.deleteEntryById(eventId);
        
    },
    
    doCopy: function(eventId) 
    {
        
        this.copyPasteIdentifier = eventId;
        
    },
    
    doPaste: function(timestamp, userId) 
    {
        
        if(!timestamp || timestamp.length == 0)
        {
                        
            return;
            
        }
        
        var sendData = {
            
            'do': 'copyEntry',
            'id': this.copyPasteIdentifier,
            't': timestamp,
            'dst': Calendar.isBrowserInDaylightSavingTime() ? 1 : 0,
            'keepHour': (Calendar.currentDisplayType == "month" ? 1 : 0)
            
        };
        
        if(userId != null)
        {
            
            sendData['u'] = userId;
            
        }
        
        jQuery.post(Calendar.controller, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);
            
            if(decodedData.success)
            {
                
                Calendar.refreshContainer();
                
            }
            else
            {
                
                toastr.error(decodedData.message);
                
            }
            
        });
        
    },
    
    showContextMenu: function(element, event) 
    {
        
        if(element == null)
        {
            
            return;
            
        }
        
        event.preventDefault();
        
        this.contextElement = element;
                
        CalendarContextMenu.updateContextMenu(event);
        
        jQuery(CalendarContextMenu.contextMenuId).finish().css({

            'display': 'block',
            'top': event.pageY + "px",
            'left': event.pageX + "px"

        });
        
    },
    
    hideContextMenu: function()
    {
        
        jQuery(CalendarContextMenu.contextMenuId).css('display', 'none');
        
        this.contextElement = null;
        this.disableCopy = false;
        this.disablePaste = false;
        
    },
    
    updateContextMenu: function(event) 
    {
        
        this.contextMenuElement.empty();
        
        if(!this.disableCopy)
        {
            
            // "Copy" 
            
            jQuery(this.contextMenuElement).append('<li data-action="copy">' + Language.getLanguageVariable('p588f82cf17be8') + '</li>');
        
        }
        
        if(jQuery(this.contextElement).hasClass('fc-event'))
        {
            
            // The click was on an element, show the option to delete it
            jQuery(this.contextMenuElement).append('<li class="option-delete" data-action="delete">' + Language.getLanguageVariable('10297') + '</li>');
            
        }
        
        if(this.copyPasteIdentifier != null && !this.disablePaste)
        {
            
            jQuery(this.contextMenuElement).append('<li data-action="paste">' + Language.getLanguageVariable('p588f830702a7d') + '</li>');
            
        }
        
        this.updateContextMenuActions(event);
        
    },
    
    updateContextMenuActions: function(event) 
    {
        
        jQuery(CalendarContextMenu.contextMenuId + " li").click(function(){
            
            switch(jQuery(this).attr("data-action")) 
            {

                case "delete": 
                    
                    CalendarContextMenu.doDelete(CalendarContextMenu.contextElement.attr('data-id'));
                    
                    break;
                    
                case "copy": 
                    
                    CalendarContextMenu.doCopy(CalendarContextMenu.contextElement.attr('data-id'));
                    
                    break;
                    
                case "paste": 
                    
                    var timestamp = null;
                    var userId = null;
                    
                    if(CalendarContextMenu.contextElement.hasClass('fc-day') || CalendarContextMenu.contextElement.hasClass('fc-event-container'))
                    {
                        
                        // A day was clicked
                        
                        timestamp = CalendarContextMenu.getAttributeFromContextElement('data-time');
                        
                    }
                    else if(CalendarContextMenu.contextElement.hasClass('fc-widget-content'))
                    {
                                                
                        // Find out which specific time / hour and user was clicked
                        
                        var timeAndUser = Calendar.getClickedHourlyTimeAndUserFromEvent(event, true);
                        
                        timestamp = timeAndUser.time;
                        userId = timeAndUser.user;
                        
                    }
                    
                    CalendarContextMenu.doPaste(timestamp, userId);
                    
                    break;
                    
            }
            
            CalendarContextMenu.hideContextMenu();
            
        });  
        
    },
    
    getAttributeFromContextElement: function(attribute)
    {
        
        var returnValue = '';
                
        if(this.contextElement.hasClass('fc-day') || this.contextElement.hasClass('fc-event-container'))
        {
            
            returnValue = CalendarContextMenu.contextElement.attr(attribute);
            
        }
        
        return returnValue;
        
    },
    
    cacheLanguageVariables: function()
    {
        
        Language.getLanguageVariable('p588f82cf17be8');
        Language.getLanguageVariable('p588f830702a7d');
        
    }
    
};