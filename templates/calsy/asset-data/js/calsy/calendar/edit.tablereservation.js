
var CalendarTableReservation = {
    
    'locationTableStructure': null,
    
    'setLocationTableStructure': function(structure)
    {
        
        CalendarTableReservation.locationTableStructure = structure;
        
    },
    
    'onLocationChange': function(e)
    {
        
        var element = jQuery(this);
        
        var selectedLocationId = element.find('option:selected').val();
        
        CalendarTableReservation.fillTablesFromLocation(selectedLocationId);
        
        jQuery('#calsy_calendar-entry_table_reservation_table_selector').css('display', 'block');
        
    },
    
    'fillTablesFromLocation': function(locationId)
    {
        
        var locationData = CalendarTableReservation.locationTableStructure[locationId];
        
        jQuery('#calsy_calendar_entry_table_reservation_table option').each(function(index) {
            
            if(index == 0)
            {
                
                return;
                
            }
            
            jQuery(this).remove();
            
        });
        
        if(locationData != null)
        {
            
            jQuery.each(locationData.tables, function(tableId, tableData) {

                var newElement = '<option value="' + tableId + '">' + tableData['title'] + '</option>';

                jQuery('#calsy_calendar_entry_table_reservation_table option:last-child').after(newElement);
            
            });

        }
        
    }
    
};