/**
 * Email log list navigation handling
 *
 * @author Peter Hamm
 * @date 2016-05-07
 */
var MailLog = jQuery.extend(true, {}, PerisianBaseAjax);

MailLog.controller = baseUrl + "/mail/log/";

MailLog.currentSortOrder = 'DESC';
MailLog.currentSorting = 'mail_log_date_created';

/**
 * Asks whether to clear the log data or not.
 * 
 * @author Peter Hamm
 * @returns void
 */
MailLog.clearLog = function() 
{
    
    if(confirm(Language.getLanguageVariable(11173)))
    {
        
        var sendData = {
            
            do: 'emptyLog'
            
        };
        
        jQuery.post(this.controller, sendData, function(data) {

            var callback = JSON.parse(data);

            if(!callback.success)
            {

                toastr.warning(Language.getLanguageVariable(11174));

            }
            else
            {

                toastr.success(Language.getLanguageVariable(11175));
                
                MailLog.showEntries();

            }

        });
        
    }
    
};

/**
 * Opens the form to write a new email.
 * 
 * @author Peter Hamm
 * @returns void
 */
MailLog.newMail = function()
{
    
    var sendData = {
        
        'do': 'newMail'
        
    };
    
    PerisianBaseAjax.editEntry(sendData);
    
};

/**
 * Sends the mail with the data provided by the mail form
 * 
 * @author Peter Hamm
 * @returns void
 */
MailLog.sendMail = function()
{
    
    var sendData = {
        
        'do': 'sendMail',
        
        'recipient': jQuery('#mail_recipient').val(),
        'subject': jQuery('#mail_subject').val(),
        'content': jQuery('#mail_content').val()
        
    };
    
    jQuery('#submitButton').prop('disabled', true);
    
    jQuery.ajax({
        
        url: MailLog.controller,
        data: sendData,
        
        error: function(data)
        {
            
            console.error(data);
            
            jQuery('#submitButton').prop('disabled', false);
            
        },
        
        success: function(data) 
        {
            
            jQuery('#submitButton').prop('disabled', false);
            
            var decodedData = jQuery.parseJSON(data);
            
            if(decodedData.success)
            {
                
                toastr.success(Language.getLanguageVariable('10476'));
                
                MailLog.cancel();
                
                MailLog.showEntries();
                
            }
            else
            {
                
                toastr.warning(decodedData.message);
                
                MailLog.showEntries(null, null, null, null, true);
                
            }
                        
        }
        
    });
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
MailLog.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};