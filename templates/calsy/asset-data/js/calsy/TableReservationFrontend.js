/**
 * Class to handle table reservations in the frontend
 *
 * @author Peter Hamm
 * @date 2020-09-25
 */
var TableReservationFrontend = jQuery.extend(true, {}, PerisianBaseAjax);

TableReservationFrontend.controller = baseUrl + "/table_reservation/overview/";
TableReservationFrontend.controllerRegistration = baseUrl + "/registration/";
    
TableReservationFrontend.selectedTables = null;
    
TableReservationFrontend.initLoginRegistration = function()
{
    
    AppointmentAssistant.containerIdentifier = 'table-reservation-content-box';
    
    jQuery('input[name="is_registered"]').on('change', function() {
        
        var element = jQuery(this);
        
        jQuery('#aa_login, #btn-table-reservation-login').css('display', element.val() == "1" ? 'block' : 'none');
        jQuery('#aa_registration, #btn-table-reservation-registration').css('display', element.val() == "0" ? 'block' : 'none');
        
    });
    
};

TableReservationFrontend.goToFrontend = function()
{
    
    location.href = baseUrl;
    
};
    
TableReservationFrontend.initializeLandingPage = function(loadStatus)
{
    
    TableReservationFrontend.initialize();
    
    if(loadStatus)
    {
        
        setTimeout(function() {
            
            //TableReservationFrontend.loadCheckInStatus();
            
        }, 1000);
    
    }
    
};

TableReservationFrontend.initialize = function()
{
    
    TableReservationFrontend.initDatePicker();
    
    jQuery('#btn-table-reservation-find-table').click(function() {
        
        var element = jQuery(this);
        
        element.addClass('btn-table-reservation-find-table-clicked');
        
        TableReservationFrontend.findTable();
        
    });
    
};

TableReservationFrontend.findTable = function()
{
    
    jQuery('#btn-table-reservation-find-table, #calsy_reservation_time_field_date, #calsy_reservation_time_field_time, #calsy_reservation_number_persons').prop('disabled', true);
    
    jQuery('#table-reservation-selection-tiles, #table-reservation-selection-message').css('display', 'none');
    jQuery('#table-reservation-selection-loading-indicator').css('display', 'block');
    
    var sendData = {
        
        'do': 'findTable',
        'time': Calendar.getTimestampFromFields('#calsy_reservation_time_field_date', '#calsy_reservation_time_field_time'),
        'persons': jQuery('#calsy_reservation_number_persons').val()
        
    };
    
    jQuery.post(TableReservationFrontend.controller, sendData, function(data) {
        
        var result = JSON.parse(data);
                
        jQuery('#table-reservation-selection-loading-indicator').css('display', 'none');
            
        if(result.success == false)
        {
            
            jQuery('#table-reservation-selection-message').html('<span class="alert alert-block alert-warning" style="text-align: left">' + result.message + '</span>').css('display', 'block');
            
        }
        else
        {
            
            var tileContent = TableReservationFrontend.buildTileContentFromResult(result);
            
            jQuery('#table-reservation-selection-tiles').html(tileContent).css('display', 'block');
            
            TableReservationFrontend.addFunctionalityToTiles();
            
        }
        
        jQuery('#btn-table-reservation-find-table, #calsy_reservation_time_field_date, #calsy_reservation_time_field_time, #calsy_reservation_number_persons').prop('disabled', false);
        
    });
    
};

TableReservationFrontend.addFunctionalityToTiles = function()
{
    
    jQuery('.tile-proposal-table-reservation').click(function()
    {
        
        var element = jQuery(this);
        var elementCapacity = parseInt(element.attr('data-capacity'));
                
        if(TableReservationFrontend.isTableElementSelected(element))
        {
            
            TableReservationFrontend.unmarkElementSelected(element);
            
        }
        else
        {
        
            if(TableReservationFrontend.getSelectedCapacityFromTables() < TableReservationFrontend.getRequiredCapacity())
            {
                
                TableReservationFrontend.markElementSelected(element);
            
            }
            
        }
        
        if(TableReservationFrontend.getAmountSelectedTables() == 1 && elementCapacity >= TableReservationFrontend.getRequiredCapacity())
        {
            
            // Only this table is required, proceed to the booking.
            
            TableReservationFrontend.proceedWithSelectedTables();
             
        }
        else
        {
            
            // More tables need to be selected to meet the capacity requirements.
            
            if(TableReservationFrontend.getSelectedCapacityFromTables() < TableReservationFrontend.getRequiredCapacity())
            {

                TableReservationFrontend.displayAlertCapacityInsufficient();

            }
            else
            {

                TableReservationFrontend.displayAlertCapacitySufficient();

            }

        }
        
    });
    
    jQuery('#btn-book-tables-confirm').click(function() {
        
        TableReservationFrontend.proceedWithSelectedTables();
        
    });
    
};

TableReservationFrontend.getAmountSelectedTables = function()
{
    
    return jQuery('.table-selector-selected').length;
    
};

TableReservationFrontend.displayAlertCapacityInsufficient = function()
{
    
    jQuery('#table-reservation-selection-info-box').css('display', 'block');
    jQuery('#btn-book-tables-confirm').css('display', 'none');
    
    var variableText = Language.getLanguageVariable(TableReservationFrontend.getSelectedCapacityFromTables() == 0 ? 'p5fe344ffdf756' : 'p5fe344099c2ef');
    variableText = variableText.replace('%1', TableReservationFrontend.getSelectedCapacityFromTables());
    
    jQuery('#table-reservation-selection-info-box .alert-success').html(variableText);
        
};

TableReservationFrontend.displayAlertCapacitySufficient = function()
{
    
    jQuery('#table-reservation-selection-info-box').css('display', 'block');
    jQuery('#btn-book-tables-confirm').css('display', 'block');
    
    var variableText = Language.getLanguageVariable('p5fe345f89b270');
    
    jQuery('#table-reservation-selection-info-box .alert-success').html(variableText);
            
};

TableReservationFrontend.isTableElementSelected = function(element)
{
    
    return jQuery(element).hasClass('table-selector-selected');
    
};

TableReservationFrontend.unmarkElementSelected = function(element)
{
        
    jQuery(element).removeClass('table-selector-selected');
    
};

TableReservationFrontend.markElementSelected = function(element)
{
    
    jQuery(element).addClass('table-selector-selected');
    
};

/**
 * Retrieves the required capacity from the input field.
 * 
 * @author Peter hamm
 * @return int
 */
TableReservationFrontend.getRequiredCapacity = function()
{
    
    return parseInt(jQuery('#calsy_reservation_number_persons').val());
    
};

/**
 * Retrieves the selected capacity based on the selected tables.
 * 
 * @author Peter Hamm
 * @return int
 */
TableReservationFrontend.getSelectedCapacityFromTables = function()
{
    
    var capacityTotal = 0;
    
    jQuery('.table-selector-selected').each(function() {
        
        var element = jQuery(this);
        
        var capacity = parseInt(element.attr('data-capacity'));
        
        capacityTotal += capacity;
        
    });
    
    return capacityTotal;
    
};

TableReservationFrontend.buildConfirmation = function()
{
    
    var text = '<b>' + Language.getLanguageVariable('p5f6d760edd4b4') + '</b><br/><br/>'; 
    
    text += '<div class="row">';
    
    for(var i = 0; i < TableReservationFrontend.selectedTables.length; ++i)
    {
        
        text += '<div class="col-sm-4">'
        text += '<b>' + TableReservationFrontend.selectedTables[i]['content-title'] + '</b><br/>' + TableReservationFrontend.selectedTables[i]['content-time'];
        text += '</div>';
        
    }
    
    text += '</div>';
        
    jQuery('#info-landing-header-details').html(text);
    
    TableReservationFrontend.checkDisplayHeader();
    
};

TableReservationFrontend.proceedWithSelectedTables = function()
{
    
    var selectedTables = [];
    
    jQuery('.table-selector-selected').each(function() {
        
        var element = jQuery(this);
        
        var data = {
            
            'time': element.attr('data-time'),
            'id': element.attr('data-table-id'),
            'capacity': element.attr('data-capacity'),
            
            'content-title': element.find('.tile-proposal-content-title').html(),
            'content-time': element.find('.tile-proposal-content-time').html()
            
        };
        
        selectedTables.push(data);
    
    });
    
    TableReservationFrontend.selectedTables = selectedTables;
    
    jQuery('#table-reservation-selection').css('display', 'none');
    
    TableReservationFrontend.buildConfirmation();
    
    if(jQuery('#table-reservation-login-registration').length > 0)
    {
        
        jQuery('#table-reservation-login-registration').css('display', 'block');
        
        return;
        
    }
    
    TableReservationFrontend.loadApprovalAfterLogin();
        
};

TableReservationFrontend.buildTileContentFromResult = function(result)
{
    
    var content = '<span class="alert alert-info alert-block table-reservation-success-info" style="text-align: left">' + result.message + "</span>";
    
    for(var i = 0; i < result.proposals.length; ++i)
    {
        
        var tileContent = TableReservationFrontend.buildTile(result.proposals[i]);
        
        content += tileContent;
        
    }
    
    return content;
    
};

TableReservationFrontend.buildTile = function(tileData)
{

    var tileContent = '<div class="col-lg-3 tile-proposal tile-proposal-table-reservation" data-capacity="' + tileData['table_capacity'] + '" data-time="' + tileData['time'] + '" data-table-id="' + tileData['table_id'] + '">';

    tileContent += '<div class="tile-proposal-content" style="background-image:url(' + tileData['location_image_url'] + ')">';
    tileContent += '<div class="tile-proposal-content-overlay"></div>';
    tileContent += '</div>';

    tileContent += '<div class="tile-proposal-content-title">' + tileData['location_title'] + '</div>';
    tileContent += '<div class="tile-proposal-content-time">' + tileData['table_title'] + '<br/>' + tileData['table_capacity_text'] + '<br/>' + tileData['time_formatted'] + '</div>';
    
    tileContent += '</div>';

    return tileContent;

};

TableReservationFrontend.doAsyncLogin = function(email, password, onSuccess)
{
    
    var sendData = {
        
        'do': 'loginAsync',
        'login_email': email,
        'login_password': password
        
    };
    
    jQuery.post(TableReservationFrontend.controller, sendData, function(data) {
        
        if(onSuccess)
        {
            
            onSuccess(data);
            
        }
        
    });
    
};

TableReservationFrontend.doAsyncRegistrationAndLogin = function(registrationData, onSuccess)
{
        
    registrationData['tr'] = 1;
    registrationData['do'] = 'registrationAsync';
    
    jQuery.post(TableReservationFrontend.controllerRegistration, registrationData, function(data) {
        
        if(onSuccess)
        {
            
            onSuccess(data);
            
        }
        
    });
    
};

TableReservationFrontend.checkLogin = function() 
{
    
    if(AppointmentAssistant.checkLoginRegistration(TableReservationFrontend.hightlightError))
    {
                
        TableReservationFrontend.doAsyncLogin(jQuery('#aa_login_email').val(), jQuery('#aa_login_password').val(), TableReservationFrontend.callbackLogin);
        
    }
    else
    {
        
        alert(Language.getLanguageVariable(11037));
        
    }
    
};

TableReservationFrontend.callbackLogin = function(data) 
{
            
    var result = JSON.parse(data);

    if(result.success)
    {
        
        TableReservationFrontend.loadApprovalAfterLogin();

    }
    else
    {
        
        TableReservationFrontend.loadApprovalAfterLogin();
        
        return;
        
        if(result.error == 'user_inactive')
        {
            
            TableReservationFrontend.loadCheckInUserInactive();
        
        }

        if(result.message && result.message.length > 0)
        {
            
            alert(result.message);
            
        }
        
    }

};

TableReservationFrontend.approveReservation = function()
{
    
    var sendData = {
        
        'do': 'approveReservation',
        'tables': TableReservationFrontend.selectedTables,
        'persons': jQuery('#calsy_reservation_number_persons').val()
        
    };
    
    jQuery('#table-reservation-approval').css('display', 'none');
    jQuery('#table-reservation-confirmation').html('<div class="loading-indicator"></div>').css('display', 'block');
    
    jQuery('#table-reservation-confirmation').load(this.controller, sendData);
    
};

TableReservationFrontend.changeSelection = function()
{
        
    TableReservationFrontend.selectedTables = null;
    
    TableReservationFrontend.checkDisplayHeader();
    
    jQuery('#table-reservation-approval, #table-reservation-confirmation, #table-reservation-login-registration, #table-reservation-loading-indicator').css('display', 'none');
    jQuery('#table-reservation-selection').css('display', 'block');
    
};

TableReservationFrontend.checkDisplayHeader = function()
{
    
    var displayStandardHeader = TableReservationFrontend.selectedTables == null;
    
    jQuery('#info-landing-header').css('display', displayStandardHeader ? 'block' : 'none');
    jQuery('#info-landing-header-details').css('display', displayStandardHeader ? 'none' : 'block');
    
};

/**
 * Callback function to highlight errors.
 * 
 * @author Peter Hamm
 * @param int tabIndex
 * @param String fieldIdentifier
 * @returns void
 */
TableReservationFrontend.hightlightError = function(tabIndex, fieldIdentifier)
{
    
    if(!fieldIdentifier)
    {

        console.error('Invalid identifier ' + tabIndex + ' / ' + fieldIdentifier);

        return;

    }

    fieldIdentifier = fieldIdentifier.replace('#', '');

    var field = jQuery('[id="' + fieldIdentifier + '"]');

    var fieldContainer = field.closest('.form-group');

    fieldContainer.addClass('form-pager-highlight');
    
};

TableReservationFrontend.checkRegistration = function() 
{
    
    jQuery('.form-pager-highlight').removeClass('form-pager-highlight');
    
    if(AppointmentAssistant.checkLoginRegistration(TableReservationFrontend.hightlightError))
    {
        
        var callback = function(data) {
            
            var result = JSON.parse(data);
            
            if(result.success)
            {
                
                TableReservationFrontend.doAsyncLogin(result['registration_email'], jQuery('#aa_registration_password_one').val(), TableReservationFrontend.callbackLogin);
                
            }
            else
            {
                
                alert(result.message);
                
            }
            
        };
        
        var registrationDataRaw = AppointmentAssistant.getSendData()['user_frontend'];
        
        var registrationData = {
            
            'registration_name_first': registrationDataRaw['nameFirst'],
            'registration_name_last': registrationDataRaw['nameSecond'],
            'registration_gender': registrationDataRaw['gender'],
            'registration_email': registrationDataRaw['emailOne'],
            'registration_phone': registrationDataRaw['emailTwo'],
            'registration_pw_one': registrationDataRaw['passwordOne'],
            'registration_pw_two': registrationDataRaw['passwordTwo']
            
        };
                        
        TableReservationFrontend.doAsyncRegistrationAndLogin(registrationData, callback);
        
    }
    
};

TableReservationFrontend.doLogout = function()
{
    
    if(confirm(Language.getLanguageVariable('p5eee04fd3375d')))
    {
        
        location.href = Perisian.addUrlParam(location.href + "?", 'do', 'logout');
        
    }
    
};

TableReservationFrontend.goToOverview = function()
{
    
    location.href = TableReservationFrontend.controller;
    
};

TableReservationFrontend.loadApprovalAfterLogin = function()
{
    
    jQuery('#table-reservation-login-registration').css('display', 'none');
    
    jQuery('#table-reservation-approval').html('<div class="loading-indicator"></div>');
    
    var sendData = {
        
        'do': 'reservationApproval',
        'tables': TableReservationFrontend.selectedTables,
        'persons': jQuery('#calsy_reservation_number_persons').val()
        
    };
    
    jQuery('#table-reservation-approval').css('display', 'block');
    
    jQuery('#table-reservation-approval').load(TableReservationFrontend.controller, sendData);
    
};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
TableReservationFrontend.initDatePicker = function()
{

    var element = jQuery('#table-reservation-selection');

    element.find('#calsy_reservation_time_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_reservation_time_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
TableReservationFrontend.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};
