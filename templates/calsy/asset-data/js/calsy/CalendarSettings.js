/**
 * Class to handle the calsy calendar settings
 *
 * @author Peter Hamm
 * @date 2016-02-17
 */
var CalendarSettings = {};

CalendarSettings.controller = baseUrl + "/calendar/settings/";
/**
 * Converts a given amount of minutes to a displayable time for a time mask input field
 * 
 * @author Peter Hamm
 * @param int minutes
 * @returns String
 */
CalendarSettings.minutesToTimeMaskString = function(minutes)
{
    
    var hours = Math.floor(minutes / 60);
    var minutes = minutes % 60;
    
    hours = (hours < 10 ? "0" : "") + hours;
    minutes = (minutes < 10 ? "0" : "") + minutes;
    
    var timeString = hours + ":" + minutes;
        
    return timeString;
    
};

/**
 * Converts the given string from a time mask input field into minutes.
 * 
 * @author Peter Hamm
 * @param String maskedString
 * @returns int
 */
CalendarSettings.timeMaskStringToMask = function(maskedString)
{
   
   var splitString = maskedString.split(":");
   
   var minutes = parseInt(splitString[0] * 60) + parseInt(splitString[1]);
   
   return minutes;
    
};

CalendarSettings.enableAllDisplayableDays = function()
{
    
    jQuery('#displayed-days-list input:checkbox').each(function() {
        
        jQuery(this).prop("checked", true);
        
    });
    
};

CalendarSettings.saveDisplayableDays = function()
{
    
    var sendData = {
        
        'do': 'saveDisplayableDays',
        'days': []
        
    };
        
    jQuery('.displayed-days-list-day').each(function(i) {
        
        var dayElement = jQuery(this);        
        
        var day = {
            
            'year': dayElement.find('#day_' + i + '_yearly').is(':checked') ? 1 : 0,
            'month': dayElement.find('#day_' + i + '_monthly').is(':checked') ? 1 : 0,
            'week': dayElement.find('#day_' + i + '_weekly').is(':checked') ? 1 : 0,
            'day': dayElement.find('#day_' + i + '_daily').is(':checked') ? 1 : 0
            
        };
        
        sendData.days.push(day);
        
    });
    
    jQuery.post(CalendarSettings.controller, sendData, function(data) {

        var decodedData = jQuery.parseJSON(data);
        
        if(decodedData.success)
        {
            
            toastr.success(Language.getLanguageVariable('10669'));

        }
        else
        {
            
            toastr.error(Language.getLanguageVariable('10057'));
            
        }

    });
    
};