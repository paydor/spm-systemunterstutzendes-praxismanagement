
Calendar.isWorkPlan = false;

/**
 * When editing an entry, will return the begin time.
 * 
 * @author Peter Hamm
 * @param int index Optional
 * @returns int
 */
Calendar.getTimeBegin = function(index) {
        
    var time = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date" + (index > 0 ? '_' + index : ''), "#calsy_calendar_date_range_begin_time" + (index > 0 ? '_' + index : ''));
    
    return time;
    
};

/**
 * When editing an entry, will return the end time.
 * 
 * @author Peter Hamm
 * @param int index Optional
 * @returns int
 */
Calendar.getTimeEnd = function(index) {
    
    var time = Calendar.getTimestampFromFields("#calsy_calendar_date_range_end_date" + (index > 0 ? '_' + index : ''), "#calsy_calendar_date_range_end_time" + (index > 0 ? '_' + index : ''));
    
    return time;
    
};

/**
 * Initializes the calendar entry form for the backend.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initFormBackend = function() {
    
    jQuery('#calsy_calendar_entry_is_in_series').change(function() {

        var checkbox = jQuery(this);

        jQuery('#calsy_calendar_entry_repetition_box').css('display', checkbox.attr('checked') == 'checked' ? 'block' : 'none');

    });
    
    jQuery('.user-frontend-order').each(function(index) {
        
        var userFrontendOrderElement = jQuery(this);
        
        userFrontendOrderElement.find('#button-add-new-user-frontend').click(function() {
            
            // Button to add a new user within the form
            
            var calendarEntryId = jQuery('#editId').val();
            
            var timestampBegin = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time") - 2 * 3600;
            var timestampEnd = Calendar.getTimestampFromFields("#calsy_calendar_date_range_end_date", "#calsy_calendar_date_range_end_time") - 3600;
            
            Calendar.newUserFrontendData = {
                
                'calendarEntryId': calendarEntryId,
                
                'timeBegin': timestampBegin,
                'timeEnd': timestampEnd,
                
                'index': index
                
            };
            
            UserFrontend.editUserFrontend();
            
        });
        
        userFrontendOrderElement.find('#button-show-history-user-frontend').click(function() {
            
            // Button to show the appointment history of a frontend user
            
            var userFrontendId = PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_frontend_id_' + index);
            var calendarEntryId = jQuery('#editId').val();

            var timestampBegin = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time") - 2 * 3600;
            var timestampEnd = Calendar.getTimestampFromFields("#calsy_calendar_date_range_end_date", "#calsy_calendar_date_range_end_time") - 3600;

            Calendar.historyData = {
                
                'userFrontendId': userFrontendId,
                'calendarEntryId': calendarEntryId,
                
                'timeBegin': timestampBegin,
                'timeEnd': timestampEnd,
                
                'index': index
                
            };
            
            UserFrontend.showCalendarHistory(userFrontendId);
            
        });
        
        var boxes = [

            '#calsy_calendar_entry_confirmed_' + index,
            '#calsy_calendar_entry_canceled_' + index

        ];

        userFrontendOrderElement.find(boxes.join(',')).change(function() {
            
            var currentCheckBox = jQuery(this);
            
            if(currentCheckBox.prop('checked'))
            {
                
                // Deactivate all other checkboxes in the list
                
                for(var i = 0; i < boxes.length; ++i)
                {
                    
                    if(('#' + currentCheckBox.attr('id')) != boxes[i])
                    {
                        
                        jQuery(boxes[i]).prop('checked', false);
                        
                    }
                    
                }
            
            }
        
            jQuery('#calsy_calendar_entry_info_status_confirmation_message_container_' + index).css('display', jQuery(boxes[1]).is(':checked') ? 'block' : 'none');
            
        });
        
    });
    
    // Consider filling in the data of newly created frontend users
    if(Calendar.newUserFrontendData)
    {
                
        var element = jQuery('#calsy_calendar_entry_user_frontend_id_' + Calendar.newUserFrontendData.index);
        
        if(Calendar.newUserFrontendData.id)
        {
            
            element.val(Calendar.newUserFrontendData.fullname);
            element.attr('perisian-data-id', Calendar.newUserFrontendData.id);
        
        }
        
    }
        
};

/**
 * Asks the user if he really wants to cancel this entry
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.cancelEntry = function()
{
    
    Calendar.cancelEntryById(jQuery('#editId').val(), jQuery('#calsy_calendar_entry_cancellation_reason').val());
    
};

/**
 * Asks the user if he really wants to delete this entry
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.deleteEntry = function()
{
    
    Calendar.deleteEntryById(jQuery('#editId').val());
    
};

/**
 * Deletes all the appointments with the specified group identifier.
 * 
 * @author Peter Hamm
 * @param int groupId
 * @returns void
 */
Calendar.deleteGroup = function(groupId)
{
    
    if(confirm(Language.getLanguageVariable(11147)))
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#deleteButtonGroup').attr('disabled', 'disabled');
        
        var sendData = {

            'do': 'deleteGroup',
            'deleteId': groupId

        };
        
        jQuery.post(this.controller, sendData, function(data) {
            
            Calendar.cancel();
            Calendar.refreshContainer();
            
            var decodedData = jQuery.parseJSON(data);
            
            toastr.success(decodedData.message);

        });
        
    }
    
};

/**
 * Asks the user if he really wants to cancel the entry with the specified ID
 * 
 * @author Peter Hamm
 * @param int entryId The identifier of the entry
 * @param String reason The reason for the cancellation.
 * @returns void
 */
Calendar.cancelEntryById = function(entryId, reason)
{
    
    if(reason.length < 3)
    {
        
        alert(Language.getLanguageVariable('p58bbc2250c344'));
        
        return;
        
    }
    
    if(confirm(Language.getLanguageVariable('p58bbc20f68e08')))
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#cancelEntryButton').attr('disabled', 'disabled');
        
        var sendData = {

            'do': 'cancel',
            'id': entryId,
            'reason': reason

        };
        
        jQuery.post(this.controller, sendData, function(data) {
            
            Calendar.cancel();
            Calendar.refreshContainer();
            
            var decodedData = jQuery.parseJSON(data);
            
            toastr.success(decodedData.message);

        });
        
    }
    
};

/**
 * Asks the user if he really wants to delete tthe entry with the specified ID
 * 
 * @author Peter Hamm
 * @param int deleteId The identifier of the entry to delete
 * @returns void
 */
Calendar.deleteEntryById = function(deleteId)
{
    
    if(confirm(Language.getLanguageVariable(10790)))
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').attr('disabled', 'disabled');
        
        var sendData = {

            'do': 'delete',
            'deleteId': deleteId

        };
        
        jQuery.post(this.controller, sendData, function(data) {
            
            Calendar.cancel();
            Calendar.refreshContainer();
            
            var decodedData = jQuery.parseJSON(data);
            
            toastr.success(decodedData.message);

        });
        
    }
    
};

/**
 * Opens the form to add a new event
 * 
 * @author Peter Hamm
 * @param int timestamp Optional
 * @param int userId Optional
 * @param int userFrontendId Optional
 * @param int orderId Optional
 * @param int copyEntryId Optional
 * @param int areaId Optional
 * @param String viewType Optional, may be overridden. Default: The currently used view type, e.g. "year", "month", "week" or "day".
 * @returns void
 */
Calendar.createNewEvent = function(timestamp, userId, userFrontendId, orderId, copyEntryId, viewType, areaId)
{
    
    if(!viewType)
    {
        
        viewType = Calendar.currentDisplayType;
        
    }
        
    var sendData = {

        't': timestamp,
        'u': userId,
        'p': userFrontendId,
        'r': orderId,
        'c': copyEntryId,
        'a': areaId,
        'dst': Calendar.isBrowserInDaylightSavingTime() ? 1 : 0,
        'calendarType': Calendar.calendarType,
        'viewType': viewType,
        'do': 'create',
        'step': 1
        
    };
    
    if(Calendar.historyData)
    {
        
        sendData['t'] = Calendar.historyData.timeBegin;
        sendData['te'] = Calendar.historyData.timeEnd;
        
    }
    
    this.editEntry(sendData, Calendar.onEditEvent);
    
};

/**
 * Opens a new form with information about bookable times within the specified timestamps.
 * 
 * @author Peter Hamm
 * @param int dateBegin
 * @param int dateEnd
 * @returns void
 */
Calendar.infoBookableTime = function(timestampBegin, timestampEnd)
{
            
    var sendData = {

        't': timestampBegin,
        'te': timestampEnd,
        'dst': Calendar.isBrowserInDaylightSavingTime() ? 1 : 0,
        'calendarType': Calendar.calendarType,
        'do': 'infoBookableTime'
        
    };
    
    this.editEntry(sendData);    
    
};

/**
 * Opens the form to edit a holiday
 * 
 * @author Peter Hamm
 * @param int timestamp
 * @returns void
 */
Calendar.editHoliday = function(eventId)
{
    
    if(Calendar.calendarType == 'user_frontend')
    {
        
        return false;
        
    }
    
    var sendData = {

        'e': eventId,
        'calendarType': Calendar.calendarType,
        'do': 'editHoliday',
        'step': 1
        
    };

    this.editEntry(sendData, function() {
        
        jQuery("#calsy_calendar_entry_title").focus();
        
    });
    
};

/**
 * Opens the info for an event
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.infoEvent = function(eventId)
{
    
    Calendar.newUserFrontendData = null;
    Calendar.historyData = null;
        
    Calendar.handleInfoEvent(eventId);
    
};

/**
 * Opens the form to edit an event
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.editEvent = function(eventId)
{
    
    Calendar.newUserFrontendData = null;
    Calendar.historyData = null;
        
    Calendar.handleEditEvent(eventId);
    
};

/**
 * Clears all of the elements in the time element container.
 * 
 * @author Peter Hamm
 * @returns Objetc
 */
Calendar.clearTimeElementContainer = function() {
    
    return jQuery('#calendar-form-time-element-container').html('');
    
};

/**
 * Retrieves the element that contains the time elements.
 * 
 * @author Peter Hamm
 * @returns Objetc
 */
Calendar.getTimeElementContainer = function() {
    
    return jQuery('#calendar-form-time-element-container');
    
};

/**
 * Adds a time duration element to the list.
 * 
 * @author Peter Hamm
 * @param Object data
 * @param int areaId
 * @returns void
 */
Calendar.addTimeDurationElement = function(data, areaId) {
    
    var timeElementContainer = Calendar.getTimeElementContainer();
    
    var timeElementClone = jQuery('#calendar-form-time-element-dummy').clone();
    timeElementClone.removeAttr('id');
    
    var countExistingElements = timeElementContainer.find('.calendar-form-time-element').length + 1;
    
    var timeDurationElementsActual = Calendar.getActualTimeDurationElementsForArea(areaId);
    
    {
        
        var timeTitle = Calendar.getFormattedTimeDurationElementTitleHtml(jQuery('.calendar-form-time-element').length + 1, timeDurationElementsActual.length, data.title);
        timeElementClone.find('.time-duration-title').html(timeTitle);
                
    }
    
    // Update the identifiers
    {
        
        timeElementClone.find('#calsy_calendar_entry_user_id').attr('id', 'calsy_calendar_entry_user_id_' + countExistingElements);
        timeElementClone.find('#calsy_calendar_entry_resource_id').attr('id', 'calsy_calendar_entry_resource_id_' + countExistingElements);
        
        timeElementClone.find('#calsy_calendar_date_range_begin_date').attr('id', 'calsy_calendar_date_range_begin_date_' + countExistingElements);
        timeElementClone.find('#calsy_calendar_date_range_begin_time').attr('id', 'calsy_calendar_date_range_begin_time_' + countExistingElements);
        timeElementClone.find('#calsy_calendar_date_range_end_date').attr('id', 'calsy_calendar_date_range_end_date_' + countExistingElements);
        timeElementClone.find('#calsy_calendar_date_range_end_time').attr('id', 'calsy_calendar_date_range_end_time_' + countExistingElements);
        
        timeElementClone.find('#calsy_calendar_entry_title').attr('id', 'calsy_calendar_entry_title_' + countExistingElements);
        timeElementClone.find('#calsy_calendar_entry_description').attr('id', 'calsy_calendar_entry_description_' + countExistingElements);
        
    }
    
    // Take over some of the field values
    {
        
        timeElementClone.find('#calsy_calendar_entry_user_id_' + countExistingElements).attr('perisian-data-id', jQuery('#calsy_calendar_entry_user_id').attr('perisian-data-id')).val(jQuery('#calsy_calendar_entry_user_id').val());
        timeElementClone.find('#calsy_calendar_entry_resource_id_' + countExistingElements).attr('perisian-data-id', jQuery('#calsy_calendar_entry_resource_id').attr('perisian-data-id')).val(jQuery('#calsy_calendar_entry_resource_id').val());
        
        timeElementClone.find('#calsy_calendar_entry_title_' + countExistingElements).val(jQuery('#calsy_calendar_entry_title').val());
                
    }
    
    timeElementContainer.append(timeElementClone);
    
    Calendar.initTimeElement(timeElementClone.find('#calsy_calendar_date_range_' + countExistingElements));
    
    Calendar.initAutoCompleteForUserBackendWithResource(countExistingElements);
    
    setTimeout(function() {
                
        Calendar.updateTimeElementTimes(countExistingElements);
        
    }, 250);
    
};

/**
 * Updates all of the time elements' begin and end times.
 * 
 * @author Peter Hamm
 * @param int exceptionIndex If specified, the elements with this index is not updated.
 * @returns void
 */
Calendar.updateAllTimeElementTimes = function(exceptionIndex) {
        
    var timeElementContainer = Calendar.getTimeElementContainer();
    
    if(!exceptionIndex)
    {
        
        exceptionIndex = Calendar.currentTimeDurationElementStep;
        
    }
        
    timeElementContainer.find('.calendar-form-time-element').each(function(index) {
        
        if(exceptionIndex >= (index + 1))
        {
                        
            return true;
            
        }
        
        Calendar.updateTimeElementTimes(index + 1);
        
    });
    
}; 

/**
 * Retrieves the time offset in seconds between the previous and the specified actual time duration entry index.
 * 
 * @author Peter Hamm
 * @param int areaId
 * @param int actualEntryIndex
 * @returns int
 */
Calendar.getTimeDurationOffsetFromPrevious = function(areaId, actualEntryIndex) {
    
    var timeDurationElements = Calendar.areaTimes[areaId];
    var actualCount = 0;
    var offset = 0;
        
    if(!timeDurationElements)
    {
        
        return;
        
    }
        
    for(var i = 0; i < timeDurationElements.length; ++i)
    {
                        
        if(actualCount == actualEntryIndex)
        {
                        
            if(timeDurationElements[i].type == 'free')
            {
                                
                offset += parseInt(timeDurationElements[i].duration);
                
            }
            
        }
        else if(actualCount > actualEntryIndex)
        {
            
            break;
            
        }
        
        if(timeDurationElements[i].type == 'block')
        {
                                    
            ++actualCount;
            
        }
        
    }
    
    return offset;
    
};

/**
 * Retrieves the currently selected area id, if available.
 * 
 * @author Peter Hamm
 * @returns int
 */
Calendar.getSelectedAreaId = function() {
    
    var areaId = jQuery('#calsy_calendar_entry_area_0').val();
    
    return areaId;
    
};

/**
 * Updates the specified time element's begin and end times.
 * 
 * @author Peter Hamm
 * @param int index
 * @returns void
 */
Calendar.updateTimeElementTimes = function(index) {
    
    var areaId = Calendar.getSelectedAreaId();
    var timeDurationElementsActual = Calendar.getActualTimeDurationElementsForArea(areaId);
    
    var currentBlock = timeDurationElementsActual[index];
    var offset = Calendar.getTimeDurationOffsetFromPrevious(areaId, index);
        
    // Calculate the times
    if(currentBlock)
    {

        var previousElementIdSuffix = index > 1 ? ('_' + (index - 1)) : '';

        var timestampEndPrevious = Calendar.getTimestampFromFields('#calsy_calendar_date_range_end_date' + previousElementIdSuffix, '#calsy_calendar_date_range_end_time' + previousElementIdSuffix);

        var elementTimeBegin = timestampEndPrevious + offset;
        var elementTimeEnd = elementTimeBegin + parseInt(currentBlock.duration);

        Calendar.setTimestampForFields('#calsy_calendar_date_range_begin_date_' + index, '#calsy_calendar_date_range_begin_time_' + index, elementTimeBegin);
        Calendar.setTimestampForFields('#calsy_calendar_date_range_end_date_' + index, '#calsy_calendar_date_range_end_time_' + index, elementTimeEnd);

    }
    
};

/**
 * Navigates to the next time duration element of the selected area.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.nextTimeDurationElement = function() {
    
    if(Calendar.currentTimeDurationElementStep == 0)
    {
        
        // Take over the title of the first element to the children, if those are still empty
        
        Calendar.getTimeElementContainer().find('.calendar-form-time-element').each(function(index) {
            
            var element = jQuery(this);
                        
            var mainTitle = jQuery('#calsy_calendar_entry_title').val();
            
            var elementTitleInput = element.find('#calsy_calendar_entry_title_' + (index + 1));
            
            if(elementTitleInput.val() == '')
            {
                
                elementTitleInput.val(mainTitle);
                
            }
            
        });
        
    }
    
    Calendar.showTimeDurationElement(Calendar.currentTimeDurationElementStep + 1);
    
};

/**
 * Navigates to the previous time duration element of the selected area.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.previousTimeDurationElement = function() {
    
    Calendar.showTimeDurationElement(Calendar.currentTimeDurationElementStep - 1);
    
};

/**
 * Shows the time duration element for the specified index.
 * 
 * @author Peter Hamm
 * @param int elementIndex
 * @returns void
 */
Calendar.showTimeDurationElement = function(elementIndex) {
    
    var areaId = jQuery('#calsy_calendar_entry_area_0').val();
    var timeDurationElementsActual = Calendar.getActualTimeDurationElementsForArea(areaId);
        
    if(elementIndex <= 0)
    {
        
        elementIndex = 0;
        
        // Show the main detail page
        
        jQuery('#calendar-form-main').css('display', 'block');
        jQuery('#calendar-form-time-element-container').css('display', 'none');
        
    }
    else
    {
        
        jQuery('#calendar-form-main').css('display', 'none');
        jQuery('#calendar-form-time-element-container').css('display', 'block');

        if(elementIndex >= Calendar.countTimeDurationElementsActual - 1)
        {

            // This is the last page

            elementIndex = timeDurationElementsActual.length - 1;

        }
        
        jQuery('.calendar-form-time-element').not('#calendar-form-time-element-dummy').each(function(index) {
                        
            var element = jQuery(this);
            
            element.css('display', index == (elementIndex - 1) ? 'block' : 'block');
            
        });
        
    }
  
    Calendar.currentTimeDurationElementStep = elementIndex;
    Calendar.updateTimeDurationElementsByArea(areaId);
    
};

/**
 * Opens the page for the payment options for a calendar entry.
 * 
 * @author Peter Hamm
 * @param int eventId
 * @returns void
 */
Calendar.showPaymentOptions = function(eventId)
{
    
    var sendData = {

        'e': eventId,
        'do': 'paymentOptions'
        
    };
    
    this.editEntry(sendData);
    
};

/**
 * Saves an entry that is part of a work plan.
 * 
 * @author Peter Hamm
 * @return void
 */
Calendar.saveEntryWorkPlan = function()
{
    
    Calendar.isWorkPlan = true;
    
    Calendar.saveEntry();
    
    Calendar.isWorkPlan = false;
    
};

/**
 * Saves the entry that is currently being edited.
 * 
 * @author Peter Hamm
 * @param bool isHoliday Set this to true to save it as a holiday
 * @param bool isSurvey Set this to true to save it as a survey
 * @returns void
 */
Calendar.saveEntry = function(isHoliday, isSurvey)
{
        
    var sendData = Calendar.getSendData(isHoliday, isSurvey);
        
    if(Calendar.validate(sendData))
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('.submitButton').attr('disabled', 'disabled');
        jQuery('#' + Perisian.containerIdentifier).find('.deleteButton').attr('disabled', 'disabled');
        
        jQuery.post(this.controller, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);

            if(decodedData.success)
            {
                
                Calendar.cancel();
                
                if(Calendar.jumpToTodayAfterSaving)
                {
                    
                    Calendar.onClickToday();
                    
                }
                else
                {
                    
                    Calendar.refreshContainer();
                    
                }
                
                if(typeof CalendarService !== 'undefined')
                {
                    
                    CalendarService.updateCountEntries();
                    
                }
                
                if(decodedData.needsPayment == true)
                {
                    
                    Calendar.showPaymentOptions(decodedData.id);
                    
                }
                
                toastr.success(decodedData.message);
                
            }
            else
            {
                
                toastr.warning(decodedData.message);
                
                jQuery('#' + Perisian.containerIdentifier).find('.submitButton').removeAttr('disabled');
                jQuery('#' + Perisian.containerIdentifier).find('.deleteButton').removeAttr('disabled');
                
            }

        });
        
    };
    
};

/**
 * Retrieves all the entered data for an entry to be saved.
 * 
 * @author Peter Hamm
 * @param bool isHoliday Set this to true to save it as a holiday
 * @param bool isSurvey Set this to true to save it as a survey
 * @returns Object
 */
Calendar.getSendData = function(isHoliday, isSurvey) 
{
    
    var timestampBegin = Calendar.getTimeBegin();
    var timestampEnd = Calendar.getTimeEnd();
    
    var isSeries = !isHoliday && jQuery('#calsy_calendar_entry_is_in_series').attr('checked') == 'checked';
    
    var userFrontendOrderList = [];
    
    jQuery(".user-frontend-order").each(function(index) {
        
        var element = {
            
            'p': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_frontend_id_' + index),
            'r': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_order_id_' + index),
            'a': jQuery('#calsy_calendar_entry_area_' + index).val(),
            'c': jQuery('#calsy_calendar_entry_confirmed_' + index).prop('checked'),
            'ca': jQuery('#calsy_calendar_entry_canceled_' + index).prop('checked'),
            'info_additional': jQuery('#calsy_calendar_entry_info_additional_' + index).val(),
            'message_to_client': jQuery('#calsy_calendar_entry_message_to_client_' + index).val(),
            'confirmation_status_message': jQuery('#calsy_calendar_entry_info_status_confirmation_message_' + index).val()
            
        };
                
        if(jQuery('#calsy_calendar_entry_confirmed_' + index).length == 0)
        {
            
            element['c'] = true;
        
        }
        
        userFrontendOrderList.push(element);
        
    }); 
    
    if(Calendar.isWorkPlan)
    {
        
        var element = {
            
            'c': true,
            'a': jQuery('#work_plan_area_id option:selected').val()
            
        };
        
        userFrontendOrderList.push(element);
        
    }
    
    var sendData = {
        
        'do': 'save',
        'isHoliday': isHoliday,
        'isSurvey': isSurvey,
        
        'seriesData': {
            
            'isSeries': isSeries,
            'interval': isSeries ? jQuery('#calsy_calendar_entry_repetition_interval').val() : 0,
            'intervalEnd': isSeries ? Calendar.getTimestampFromFields("#calsy_calendar_entry_repetition_end") : 0
            
        },
        
        'notificationData': {
            
            'time': jQuery('#calsy_calendar_entry_notification_time').val()
            
        },
        
        'children': [],
        'editId': jQuery('#editId').val(),
        'copyId': jQuery('#copyId').val(),
        'workPlanId': jQuery('#workPlanId').val(),
        'userId': isHoliday ? jQuery('#calsy_calendar_entry_holiday_user').val() : PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_id'),
        'resourceId': isHoliday ? -1 : PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_resource_id'),
        'userFrontendWithOrders': isHoliday ? null : userFrontendOrderList,
        'title': jQuery('#calsy_calendar_entry_title').val(),
        'description': jQuery('#calsy_calendar_entry_description').val(),
        
        'begin': timestampBegin,
        'end': timestampEnd,
        'dst': Calendar.isBrowserInDaylightSavingTime() ? 1 : 0
        
    };
    
    if(jQuery('#calsy_calendar_entry_table_reservation_table').length > 0)
    {
        
        sendData['location_id'] = jQuery('#calsy_calendar_entry_table_reservation_location option:selected').val();
        sendData['table_id'] = jQuery('#calsy_calendar_entry_table_reservation_table option:selected').val();
        sendData['number_persons'] = jQuery('#calsy_calendar_entry_table_reservation_number_persons').val();
        
    }   
    
    if(isSurvey)
    {
        
        sendData['surveyAreaId'] = jQuery('#calsy_calendar_entry_area_0 :selected').val();
        sendData['surveyUserData'] = [];
        
        jQuery('.calsy-user-selector-multi-element').each(function() {
            
            var element = jQuery(this);
            var checkbox = element.find('input[type=checkbox]');
            var isChecked = checkbox.is(':checked');
            
            if(isChecked)
            {
                
                var surveyUserEntry = {

                    'id': checkbox.val(),
                    'checked': true

                };
                
                sendData['surveyUserData'].push(surveyUserEntry);
                
            }
            
        });
        
    }
    
    if(jQuery('#form_calsy_calender_entry_image').length > 0)
    {

        var images = [];

        var element = jQuery('#form_calsy_calender_entry_image')

        var imageObject = {

            'file': element.find('#calsy_calender_entry_image_filename').val(),
            'filename': element.find('.dz-details').find('.dz-filename span').html(),
            'image_id': element.find('#image_id').val()

        };

        if(imageObject['filename'] && imageObject['filename'].length > 0)
        {
            
            images.push(imageObject);
            
        }

        sendData['images'] = images;

    }
    
    if(jQuery('#calsy_calendar_entry_area_user_frontend').length > 0)
    {
                
        sendData['areaId'] = jQuery('#calsy_calendar_entry_area_user_frontend').val();
        
    }
    
    if(jQuery('.calendar-form-time-element').not('#calendar-form-time-element-dummy').length > 0)
    {
                
        // This entry has children, e.g. from split times.
        
        jQuery('.calendar-form-time-element').not('#calendar-form-time-element-dummy').each(function(index) {
            
            var elementIndex = index + 1;
            var element = jQuery(this);
            
            var timestampBegin = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date_" + elementIndex, "#calsy_calendar_date_range_begin_time_" + elementIndex);
            var timestampEnd = Calendar.getTimestampFromFields("#calsy_calendar_date_range_end_date_" + elementIndex, "#calsy_calendar_date_range_end_time_" + elementIndex);
            
            var childData = {
                
                'editId': element.find('.data-container').attr('perisian-data-entry-id'),
                
                'title': jQuery('#calsy_calendar_entry_title_' + elementIndex).val(),
                'description': jQuery('#calsy_calendar_entry_description_' + elementIndex).val(),
                
                'userId': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_id_' + elementIndex),
                'resourceId': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_resource_id_' + elementIndex),
                
                'begin': timestampBegin,
                'end': timestampEnd
                
            };
            
            sendData.children.push(childData);
            
        });
        
    }
    
    return sendData;
    
};

/**
 * Validates the fields of the data to save before sending them.
 * 
 * @author Peter Hamm
 * @param Objetc sendData
 * @returns bool Indicator whether successfully validated or not.
 */
Calendar.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    
    var errors = 0;
    
    jQuery(element).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        Calendar.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(jQuery('#calsy_calendar_entry_title').length > 0 && !Validation.checkRequired(sendData.title))
    {

        this.highlightInputTitleError('calsy_calendar_entry_title', false);
        
        ++errors;

    }
    
    if(Calendar.calendarType != 'user_frontend' && Calendar.isWorkPlan)
    {
        
        if(!Validation.checkRequired(sendData.userId))
        {

            this.highlightInputTitleError('calsy_calendar_entry_user_id', false);

            ++errors;

        }
        
    }
    
    if(sendData.userFrontendWithOrders)
    {

        for(var i = 0; i < sendData.userFrontendWithOrders.length; ++i)
        {

            var element = sendData.userFrontendWithOrders[i];

            if(element['ca'] && element['confirmation_status_message'].length == 0)
            {

                this.highlightInputTitleError('calsy_calendar_entry_info_status_confirmation_message_' + i);

                ++errors;

            }

        }
        
    }
    
    if(!Validation.checkRequired(sendData.begin))
    {

        this.highlightInputTitleError('calsy_calendar_date_range_begin_date', false);
        this.highlightInputTitleError('calsy_calendar_date_range_begin_time', false);
        
        ++errors;

    }
    
    if(!Validation.checkRequired(sendData.end))
    {

        this.highlightInputTitleError('calsy_calendar_date_range_end_date', false);
        this.highlightInputTitleError('calsy_calendar_date_range_end_time', false);
        
        ++errors;

    }
        
    if(parseInt(sendData.end) <= parseInt(sendData.begin))
    {
        
        this.highlightInputTitleError('calsy_calendar_date_range_begin_date', false);
        this.highlightInputTitleError('calsy_calendar_date_range_begin_time', false);
        this.highlightInputTitleError('calsy_calendar_date_range_end_date', false);
        this.highlightInputTitleError('calsy_calendar_date_range_end_time', false);
        
        ++errors;
        
    }
    
    return errors == 0;

};

/**
 * Opens the form to book a work plan. 
 * Takes the ID of a work plan entry from the calendar to load the data.
 * 
 * @author Peter Hamm
 * @param int entryId
 * @returns void
 */
Calendar.bookWorkPlan = function(entryId) 
{
    
    Calendar.isWorkPlan = true;
    
    Calendar.editEvent(entryId);
    
    Calendar.isWorkPlan = false;
    
};

/**
 * Retrieves all of the actual time duration elements for the area with the specified identifier.
 * 
 * @author Peter Hamm
 * @param int areaId
 * @returns Array
 */
Calendar.getActualTimeDurationElementsForArea = function(areaId) {
    
    
    var timeElements = Calendar.areaTimes[areaId];
    var actualElements = [];
    
    if(!timeElements)
    {
        
        return actualElements;
    
    }
    
    for(var i = 0; i < timeElements.length; ++i)
    {

        if(timeElements[i].type == 'block')
        {
            
            actualElements.push(timeElements[i]);

        }

    }
    
    return actualElements;
    
};

/**
 * Updates the displayed blocks by the specified area identifier.
 * 
 * @author Peter Hamm
 * @param int areaId
 * @returns void
 */
Calendar.updateTimeDurationElementsByArea = function(areaId) 
{
        
    var showButtonNext = false;
    var showButtonPrevious = false;
    var showButtonSave = true;
    var showButtonDelete = true;
    var showButtonGroupExtra = true;
    
    var timeDurationElementsActual = Calendar.getActualTimeDurationElementsForArea(areaId);
    
    if(timeDurationElementsActual.length > 1)
    {

        showButtonSave = false;

        if(Calendar.currentTimeDurationElementStep >= 1)
        {

            showButtonPrevious = true;
            showButtonDelete = false;
            showButtonGroupExtra = false;

        }

        if(Calendar.currentTimeDurationElementStep < timeDurationElementsActual.length - 1)
        {

            showButtonNext = true;

        }
        else
        {

            showButtonSave = true;

        }
            
    }
        
    if(Calendar.flagUpdateTimeDurationElements)
    {

        var timeElementContainer = jQuery('#calendar-form-time-element-container');
        timeElementContainer.empty();

        if(timeDurationElementsActual.length > 1)
        {
            
            var timeTitle = Calendar.getFormattedTimeDurationElementTitleHtml(Calendar.currentTimeDurationElementStep + 1, timeDurationElementsActual.length, timeDurationElementsActual[0].title);
            
            jQuery('#time-duration-title').html(timeTitle);

            for(var i = 1; i < timeDurationElementsActual.length; ++i)
            {

                Calendar.addTimeDurationElement(timeDurationElementsActual[i], areaId);

            }
            
            Calendar.initTimeMask();

        }
        
        Calendar.flagUpdateTimeDurationElements = false;
        
    }
    
    jQuery('#time-duration-title').css('display', (showButtonNext || showButtonPrevious)  ? 'inline-block' : 'none')
    jQuery('#time-title').css('display', !(showButtonNext || showButtonPrevious)  ? 'inline-block' : 'none')
    jQuery('#button-group-extra').css('display', showButtonGroupExtra  ? 'inline' : 'none')
    jQuery('#button-block-next').css('display', showButtonNext ? 'inline-block' : 'none');
    jQuery('#button-block-previous').css('display', showButtonPrevious ? 'inline-block' : 'none');
    jQuery('#button-save-main').css('display', showButtonSave ? 'inline-block' : 'none');
    jQuery('#button-delete-main').css('display', showButtonDelete ? 'inline-block' : 'none');
    
};

/**
 * Retrieves a formatted title for a time duration element.
 * 
 * @author Peter Hamm
 * @param int currentStepIndex
 * @param int maxIndex
 * @param String stepTitleUnformatted
 * @returns String
 */
Calendar.getFormattedTimeDurationElementTitleHtml = function(currentStepIndex, maxIndex, stepTitleUnformatted) {
    
    var timeTitle = Language.getLanguageVariable('p58f714011cfb8');
                        
    timeTitle = timeTitle.replace(/%1/g, currentStepIndex);
    timeTitle = timeTitle.replace(/%2/g, maxIndex);
    timeTitle = timeTitle.replace(/%3/g, stepTitleUnformatted);
    
    timeTitle += '<i title="' + Language.getLanguageVariable('p58ffc956964be') + '" class="md md-call-split split-time-icon"></i>';
    
    return timeTitle;
    
};

Calendar.initTimeMask = function()
{
    
    jQuery(".form-control.time-mask").inputmask('h:s', {placeholder: 'hh:mm'});
    
};

/**
 * Retrieves the area selector for the currently edited entry
 * 
 * @author Peter Hamm
 * @returns Object
 */
Calendar.getAreaSelector = function()
{
    
    return jQuery('#calsy_calendar_entry_area_user_frontend').length > 0 ? jQuery('#calsy_calendar_entry_area_user_frontend') : jQuery('#calsy_calendar_entry_area_0');  
    
};

/**
 * Opens the form to add a new event within the current time scope.
 * 
 * @author peter Hamm
 * @returns void
 */
Calendar.createNewEventInScope = function()
{
    
    Calendar.handleClickDay(Calendar.currentStartDate);
    
};

/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @param String fieldId
 * @param String action "add"/empty or "remove"
 * @returns void
 */
Calendar.highlightInputTitleError = function(fieldId, action)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier).find('#' + fieldId).parent();

    if(!action)
    {
                
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Cancels editing an entry
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.cancel = function()
{
    
    Calendar.newUserFrontendData = null;
    Calendar.historyData = null;
    
    Calendar.clearTimeElementContainer();
    
    PerisianBaseAjax.cancel();
    
};

/**
 * Toggles the holiday form for the entry editing form
 * 
 * @author Peter Hamm
 * @param bool hide Optional. Set this to true to go back to the default editing. Default: false
 * @returns void
 */
Calendar.toggleFormHoliday = function(hide)
{
 
    if(hide)
    {
        
        Calendar.createNewEventInScope();
        
    }
    else
    {
            
        var sendData = {

            't': Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time"),
            'calendarType': Calendar.calendarType,
            'do': 'createHoliday',
            'step': 1

        };

        this.editEntry(sendData, function() {

            jQuery("#calsy_calendar_entry_title").focus();

        });
        
    }
    
};

/**
 * Toggles the survey form for the entry editing form
 * 
 * @author Peter Hamm
 * @param bool hide Optional. Set this to true to go back to the default editing. Default: false
 * @returns void
 */
Calendar.toggleFormSurvey = function(hide)
{
 
    if(hide)
    {
        
        Calendar.createNewEventInScope();
        
    }
    else
    {
            
        var sendData = {

            't': Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time"),
            'calendarType': Calendar.calendarType,
            'do': 'createSurvey',
            'step': 1

        };

        this.editEntry(sendData, function() {

            jQuery("#calsy_calendar_entry_title").focus();
            
            Calendar.initFormSurvey();

        });
        
    }
    
};

/**
 * Initializes the form to edit a survey entry.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initFormSurvey = function()
{
    
    jQuery('.calsy-user-selector-multi-element').click(function() {
        
        var element = jQuery(this);
        var closestCheckbox = element.find('input[type=checkbox]');
                
        closestCheckbox.prop("checked", !closestCheckbox.prop("checked"));
        
    });
    
};

/**
 * Saves the current data as a holiday.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.saveHoliday = function()
{
  
    Calendar.saveEntry(true);
    
};
/**
 * Saves the current data as a survey.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.saveSurvey = function()
{
  
    Calendar.saveEntry(false, true);
    
};

/**
 * Creates a modal window with a copy of the entry with the specified identifier.
 * 
 * @author Peter Hamm
 * @param int entryId
 * @returns void
 */
Calendar.copyEntry = function(entryId)
{
    
    if(!entryId)
    {
        
        entryId = jQuery('#editId').val();
        
    }
    
    Calendar.createNewEvent(null, null, null, null, entryId);
    
};

/**
 * Initializes automatically updating an appointment's title based 
 * on the default title in the backend's calendar settings.
 * 
 * @author Peter Hamm
 * @param String languageVariableIdentifier
 * @return void
 */
Calendar.initEditAutoTitle = function(languageVariableIdentifier) 
{
            
    var possibleVariables = Calendar.getAutoTitleVariables();
    
    for(var i = 0; i < possibleVariables.length; ++i)
    {
        
        if(possibleVariables[i].watch && possibleVariables[i].watch.length > 0)
        {
            
            for(var j = 0; j < possibleVariables[i].watch.length; ++j)
            {
                
                // On change of this element, update the title
                
                var watchIdentifier = possibleVariables[i].watch[j];
                                
                jQuery('#' + watchIdentifier).on('change', function() {
                                        
                    Calendar.updateAutoTitle(languageVariableIdentifier)
                    
                });
                
            }
            
        }
        
    }
    
};

/**
 * Retrieves the variables (and their values) that are used in the auto-generated title for events.
 * 
 * @author Peter Hamm
 * @return Array
 */
Calendar.getAutoTitleVariables = function()
{
    
    var variables = [
        
        {
            
            'variable': '%userBackendName',
            
            'watch': [
                
                'calsy_calendar_entry_user_id'
                
            ],
            
            'value': jQuery('#calsy_calendar_entry_user_id').val()

        },
        
        {
            
            'variable': '%userFrontendName',
            
            'watch': [
                
                'calsy_calendar_entry_user_frontend_id_0'
                
            ],
            
            'value': Calendar.calendarType == 'user_frontend' ? jQuery('#user_frontend_name').text() : jQuery('#calsy_calendar_entry_user_frontend_id_0').val()

        },
        
        {
            
            'variable': '%areaName',
            
            'watch': [
                
                'calsy_calendar_entry_area_0',
                'calsy_calendar_entry_area_user_frontend'
                
            ],
            
            'illegalValues': [
                
                '0'
                
            ],
            
            'value': jQuery(Calendar.calendarType == 'user_frontend' ? '#calsy_calendar_entry_area_user_frontend' : '#calsy_calendar_entry_area_0').val(),
            'displayedValue': jQuery(Calendar.calendarType == 'user_frontend' ? '#calsy_calendar_entry_area_user_frontend :selected' : '#calsy_calendar_entry_area_0 :selected').text()

        }
        
    ];
    
    return variables;
    
};

/**
 * Updates an appointment's title based 
 * on the default title in the backend's calendar settings.
 * 
 * If not all of the contained variables are set, the title will be empty!
 * 
 * @author Peter Hamm
 * @param String languageVariableIdentifier
 * @return void
 */
Calendar.updateAutoTitle = function(languageVariableIdentifier)
{
    
    var title = Calendar.getNewAutoTitle(languageVariableIdentifier);
        
    jQuery('#calsy_calendar_entry_title').val(title);
    
};

/**
 * Retrieves an appointment's title based 
 * on the default title in the backend's calendar settings.
 * 
 * If not all of the contained variables are set, the title will be empty!
 * 
 * @author Peter Hamm
 * @param String languageVariableIdentifier
 * @return String
 */
Calendar.getNewAutoTitle = function(languageVariableIdentifier)
{
    
    var title = Language.getLanguageVariable(languageVariableIdentifier);
    
    var possibleVariables = Calendar.getAutoTitleVariables();
    
    var variablesFound = 0;
    var error = false;
    
    // Check how many of the variables occure in the title string.
    for(var i = 0; i < possibleVariables.length; ++i)
    {
        
        if(title.indexOf(possibleVariables[i].variable) !== -1)
        {
            
            // Check if the found variable has any value yet
            // or alternatively, if the value is not in the collection of illegal values.
            // If the check fails, the title is being set empty.
            
            if(possibleVariables[i] && (!possibleVariables[i].value || possibleVariables[i].value.length == 0))
            {
                
                // This is an empty value
                                
                error = true;
                
            }
            else
            {
                
                if(possibleVariables[i].illegalValues && possibleVariables[i].illegalValues.length > 0)
                {
                    
                    // Is this an illegal value?
                    
                    for(var j = 0; j < possibleVariables[i].illegalValues.length; ++j)
                    {
                        
                        if(possibleVariables[i].illegalValues[j] == possibleVariables[i].value)
                        {
                            
                            // This is an illegal value
                                                          
                            error = true;

                            break;
                            
                        }
                        
                    }
                    
                }
                
            }
            
            if(error)
            {
                
                title = '';
                
                break;
                
            }
            
            ++variablesFound;
            
        }
        
    }
    
    if(!error && variablesFound > 0)
    {
        
        // Replace the variables in the string
        
        for(var i = 0; i < possibleVariables.length; ++i)
        {
            
            var newValue = possibleVariables[i].displayedValue ? possibleVariables[i].displayedValue : possibleVariables[i].value;
            
            title = title.replace(possibleVariables[i].variable, newValue);
        
        }
        
    }
    
    return title;
    
};

/**
 * Initializes the autocomplete field for the backend user and the resource, optionally
 * at the specified index, e.g. used by the time duration elements.
 * 
 * @author Peter Hamm
 * @param int index Optional, leave empty for the default fields.
 * @returns void
 */
Calendar.initAutoCompleteForUserBackendWithResource = function(index) {
    
    var indexString = '';
    
    if(index)
    {
        
        indexString = '_' + index;
        
    }
    
    jQuery('#calsy_calendar_entry_user_id' + indexString).focus(function() {
        
        var timestampBegin = Calendar.getTimeBegin(index);
        var timestampEnd = Calendar.getTimeEnd(index);

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'user_id',
            'fieldToDisplay': 'user_fullname',
            'forceEntryFromList': false,
            'maxDisplayedEntries': -1,

            'url': Calendar.controller,

            'data': {

                'area': Calendar.getAreaSelector().find('option:selected').val(),
                'begin': timestampBegin,
                'end': timestampEnd,
                'editId': jQuery('#editId').val()

            },

            'renderer': CalsyAutocompleteRenderer.renderUser

        });

    }); 

    jQuery('#calsy_calendar_entry_resource_id' + indexString).focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_calendar_resource_id',
            'fieldToDisplay': 'calsy_calendar_resource_title',
            'forceEntryFromList': false,
            'maxDisplayedEntries': -1,

            'url': Calendar.controller

        });

    });
    
};

/**
 * Initializes the time element within the specified container.
 * 
 * @author Peter Hamm
 * @param Object container
 * @returns void
 */
Calendar.initTimeElement = function(container)
{
        
    Calendar.initDatePicker(container);
    Calendar.initEditAutoTitle('p58ab456403156');
    
};

/**
 * Initializes the datepicker for the specified container object.
 * If the container object is null, the default container is taken.
 * 
 * @author Peter Hamm
 * @param Object container
 * @returns Object The container
 */
Calendar.initDatePicker = function(container) {
    
    if(!container)
    {
        
        container = jQuery('#calsy_calendar_date_range');
        
    }
        
    jQuery.fn.datepicker.defaults.language = Calendar.languageCode;
    
    container.find('.datepicker-item').datepicker({

        language: Calendar.languageCode, 
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy',

        beforeShowDay: function(date) {

            var calendarDate = parseInt(date.getDay());

            calendarDate -= 1;

            if(calendarDate < 0)
            {

                calendarDate = 6;

            }

            return Calendar.displayableDaysMonthly.indexOf(calendarDate) != -1;

        }

    });

    container.find('.datepicker-item, .time-mask').blur(function() {
        
        setTimeout(function() {

            Calendar.updateAllTimeElementTimes();

        }, 250);

    });
    
    return container;
    
};

/**
 * Handles the selection of a frontend user
 * 
 * @author Peter Hamm
 * @param int index
 * @param Object entry
 * @returns void
 */
Calendar.onSelectUserFrontend = function(index, entry) {
            
    if(!Calendar.filterAreaByGender || index < 0 || !entry)
    {
                
        return;
        
    }
    
    if(Calendar.listAreas == null)
    {

        Calendar.listAreas = {};

    }
    
    if(!Calendar.listAreas[index])
    {
        
        Calendar.listAreas[index] = jQuery('#calsy_calendar_entry_area_' + index + ' option');
        
    }
    
    // Get the currently selected entry:
    var oldSelection = jQuery('#calsy_calendar_entry_area_' + index).val();
    var wasReselected = false;
    
    // Empty the list
    jQuery('#calsy_calendar_entry_area_' + index).empty();
    
    for(var i = 0; i < Calendar.listAreas[index].length; ++i)
    {

        var option = jQuery(Calendar.listAreas[index][i]);
        option.prop('selected', false);
        
        if(!option.attr('gender') || option.attr('gender') == 'none' || option.attr('gender') == entry['calsy_user_frontend_sex'] || entry['calsy_user_frontend_sex'] == 'none')
        {

            if(option.val() == oldSelection)
            {

                wasReselected = true;

                option.prop('selected', true);
            }

            jQuery('#calsy_calendar_entry_area_' + index).append(option);

        }

    }
    
    if(!wasReselected)
    {
        
        // Select the default
        
        jQuery(jQuery('#calsy_calendar_entry_area_' + index + ' option')[0]).prop('selected', true);
        
    }
    
    Calendar.listAreas = null;
    
};

 /**
 * Initializes autocomplete for each frontend user with order.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initAutocompleteForUserFrontendWithOrders = function() {
    
    jQuery(".user-frontend-order").each(function(index) {
        
        Calendar.initAutocompleteForUserFrontendWithOrdersAtIndex(index);
        
    });
    
};

 /**
 * Initializes autocomplete for the frontend user with order at the specified index.
 * 
 * @author Peter Hamm
 * @param int index
 * @returns void
 */
Calendar.initAutocompleteForUserFrontendWithOrdersAtIndex = function(index) {
    
    jQuery('#calsy_calendar_entry_order_id_' + index).focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_order_id',
            'fieldToDisplay': 'calsy_order_title',
            'forceEntryFromList': true,
            'maxDisplayedEntries': 5,

            'url': Calendar.controller,

            'data': {

                'p': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_frontend_id_' + index)

            }

        });

    });

    jQuery('#calsy_calendar_entry_user_frontend_id_' + index).focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_user_frontend_id',
            'fieldToDisplay': 'calsy_user_frontend_fullname',
            'forceEntryFromList': true,
            'maxDisplayedEntries': 10,
                
            'data': {

                'showDefaultUser': false

            },

            'url': Calendar.controller,
            
            'onSelect': function(selectedEntry) {
                
                PerisianBaseAjax.clearAutocompleteFieldValue('#calsy_calendar_entry_order_id_' + index);
                
                var identifier = PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_frontend_id_' + index);
                
                jQuery('#button-show-history-user-frontend').prop('disabled', (identifier.length == 0));
                                
                Calendar.onSelectUserFrontend(index, selectedEntry);
                
            },

            'renderer': CalsyAutocompleteRenderer.renderUserFrontend

        });

    });
    
};

/**
 * Opens the info for an event
 * 
 * @author Peter Hamm
 * @param int eventId
 * @returns void
 */
Calendar.handleInfoEvent = function(eventId)
{
    
    var sendData = {

        'e': eventId,
        'calendarType': Calendar.calendarType,
        'do': 'info',
        
    };
    
    this.editEntry(sendData, Calendar.onEditEvent);
    
};

/**
 * Opens the form to edit an event
 * 
 * @author Peter Hamm
 * @param int eventId
 * @returns void
 */
Calendar.handleEditEvent = function(eventId)
{
    
    var sendData = {

        'e': eventId,
        'calendarType': Calendar.calendarType,
        'do': 'edit',
        'step': 1,
        
        't': Calendar.newUserFrontendData ? Calendar.newUserFrontendData.timeBegin : null,
        'te': Calendar.newUserFrontendData ? Calendar.newUserFrontendData.timeEnd : null,
        
        'isWorkPlan': Calendar.isWorkPlan
        
    };
    
    this.editEntry(sendData, Calendar.onEditEvent);
    
};

/**
 * Triggered when an event is being edited.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.onEditEvent = function() 
{

    jQuery("#calsy_calendar_entry_title").focus();

};