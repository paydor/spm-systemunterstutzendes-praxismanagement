/**
 * Class to handle checkins
 *
 * @author Peter Hamm
 * @date 2020-06-15
 */
var CheckInFrontend = jQuery.extend(true, {}, PerisianBaseAjax);

CheckInFrontend.controller = baseUrl + "/checkin/overview/";
CheckInFrontend.controllerRegistration = baseUrl + "/registration/";
CheckInFrontend.controllerQuickCheckIn = baseUrl + "/checkin/";

CheckInFrontend.currentSortOrder = 'DESC';
CheckInFrontend.currentSorting = 'calsy_checkin_time_in';
CheckInFrontend.checkInList = null;
CheckInFrontend.filterUserFrontendId = null;

CheckInFrontend.goToQuickCheckIn = function()
{
    
    location.href = CheckInFrontend.controllerQuickCheckIn;
    
};
    
CheckInFrontend.initLoginRegistration = function()
{
    
    AppointmentAssistant.containerIdentifier = 'checkin-content-box';
    
    jQuery('input[name="is_registered"]').on('change', function() {
        
        var element = jQuery(this);
        
        jQuery('#aa_login, #btn-checkin-login').css('display', element.val() == "1" ? 'block' : 'none');
        jQuery('#aa_registration, #btn-checkin-registration').css('display', element.val() == "0" ? 'block' : 'none');
        
    });
    
};
    
CheckInFrontend.initializeLandingPage = function(loadStatus)
{
    
    if(loadStatus)
    {
        
        setTimeout(function() {
            
            CheckInFrontend.loadCheckInStatus();
            
        }, 1000);
    
    }
    
};

CheckInFrontend.doAsyncLogin = function(email, password, onSuccess)
{
    
    var sendData = {
        
        'do': 'loginAsync',
        'login_email': email,
        'login_password': password
        
    };
    
    jQuery.post(CheckInFrontend.controller, sendData, function(data) {
        
        if(onSuccess)
        {
            
            onSuccess(data);
            
        }
        
    });
    
};

CheckInFrontend.doAsyncRegistrationAndLogin = function(registrationData, onSuccess)
{
        
    registrationData['do'] = 'registrationAsync';
    
    jQuery.post(CheckInFrontend.controllerRegistration, registrationData, function(data) {
        
        if(onSuccess)
        {
            
            onSuccess(data);
            
        }
        
    });
    
};

CheckInFrontend.checkLogin = function() 
{
    
    if(AppointmentAssistant.checkLoginRegistration())
    {
                
        CheckInFrontend.doAsyncLogin(jQuery('#aa_login_email').val(), jQuery('#aa_login_password').val(), CheckInFrontend.callbackLogin);
        
    }
    else
    {
        
        alert(Language.getLanguageVariable(11037));
        
    }
    
};

CheckInFrontend.callbackLogin = function(data) 
{
            
    var result = JSON.parse(data);

    if(result.success)
    {

        CheckInFrontend.loadCheckInStatus();

    }
    else
    {
        
        if(result.error == 'user_inactive')
        {
            
            CheckInFrontend.loadCheckInUserInactive();
        
        }

        if(result.message && result.message.length > 0)
        {
            
            alert(result.message);
            
        }
        
    }

};

CheckInFrontend.checkRegistration = function() 
{
    
    if(AppointmentAssistant.checkLoginRegistration())
    {
        
        var callback = function(data) {
            
            var result = JSON.parse(data);
            
            if(result.success)
            {
                
                CheckInFrontend.doAsyncLogin(result['registration_email'], jQuery('#aa_registration_password_one').val(), CheckInFrontend.callbackLogin);
                
            }
            else
            {
                
                alert(result.message);
                
            }
            
        };
        
        var registrationDataRaw = AppointmentAssistant.getSendData()['user_frontend'];
        
        var registrationData = {
            
            'registration_name_first': registrationDataRaw['nameFirst'],
            'registration_name_last': registrationDataRaw['nameSecond'],
            'registration_gender': registrationDataRaw['gender'],
            'registration_email': registrationDataRaw['emailOne'],
            'registration_phone': registrationDataRaw['emailTwo'],
            'registration_pw_one': registrationDataRaw['passwordOne'],
            'registration_pw_two': registrationDataRaw['passwordTwo']
            
        };
        
        console.log(registrationData);
                
        CheckInFrontend.doAsyncRegistrationAndLogin(registrationData, callback);
        
    }
    
};

CheckInFrontend.doLogout = function()
{
    
    if(confirm(Language.getLanguageVariable('p5eee04fd3375d')))
    {
        
        location.href = Perisian.addUrlParam(location.href + "?", 'do', 'logout');
        
    }
    
};

CheckInFrontend.goToOverview = function()
{
    
    location.href = CheckInFrontend.controller;
    
};

CheckInFrontend.doCheckOut = function()
{
    
    var sendData = {
        
        'do': 'quickCheckOut'
        
    };
    
    jQuery.post(CheckInFrontend.controller, sendData, function(){
        
        CheckInFrontend.loadCheckInStatus();
        
    });
    
};

CheckInFrontend.doCheckIn = function()
{
    
    var sendData = {
        
        'do': 'quickCheckIn'
        
    };
    
    jQuery.post(CheckInFrontend.controller, sendData, function(){
        
        CheckInFrontend.loadCheckInStatus();
        
    });
    
};

CheckInFrontend.loadCheckInUserInactive = function()
{
    
    var sendData = {
        
        'do': 'infoUserInactive'
        
    };
    
    jQuery('#checkin-content-box').load(CheckInFrontend.controller, sendData);
        
};

CheckInFrontend.loadCheckInStatus = function()
{
    
    jQuery('#checkin-content-box').html('<div class="loading-indicator"></div>');
    
    var sendData = {
        
        'do': 'statusCheckIn'
        
    };
    
    jQuery('#checkin-content-box').load(CheckInFrontend.controller, sendData);
    
};

CheckInFrontend.reloadPage = function()
{
    
    location.reload();
    
};
    
CheckInFrontend.createNewCheckIn = function()
{
    
    CheckInFrontend.editCheckIn();
    
};

/**
 * Retrieves a list of all checkins
 * 
 * @author Peter Hamm
 * @returns array
 */
CheckInFrontend.getCheckInList = function()
{
    
    if(CheckInFrontend.checkInList != null)
    {
        
        return CheckInFrontend.checkInList;
        
    }
    
    var sendData = {

        'do': 'getCheckInList'
        
    };

    jQuery.ajax({
        
        url: CheckInFrontend.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            CheckInFrontend.checkInList = decodedData.list;
                        
        }
        
    });
    
    return CheckInFrontend.checkInList;
    
};

CheckInFrontend.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

CheckInFrontend.editCheckIn = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

CheckInFrontend.selectUserFrontend = function(object) 
{

    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', object['calsy_user_frontend_id']);

};

CheckInFrontend.resetUserFrontend = function() 
{
    jQuery('#calsy_checkin_user_frontend').attr('perisian-data-id', '');

};

/**
 * Should be fired when the edit form for a checkin is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
CheckInFrontend.initEditCheckIn = function()
{
        
    jQuery('#calsy_checkin_user_frontend').focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_user_frontend_id',
            'fieldToDisplay': 'calsy_user_frontend_fullname',
            'forceEntryFromList': true,

            'url': CheckInFrontend.controller,

            'renderer': CalsyAutocompleteRenderer.renderUserFrontend,

            'onSelect': CheckInFrontend.selectUserFrontend

        });

    });
    
    CheckInFrontend.initDatePicker();
    
};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
CheckInFrontend.initDatePicker = function()
{

    var element = jQuery('#modalContainer');

    element.find('#calsy_checkin_time_in_field_date, #calsy_checkin_time_out_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_checkin_time_in_field_time, #calsy_checkin_time_out_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};

CheckInFrontend.saveCheckIn = function()
{
    
    var editId = jQuery("#editId").val();
    
    var dstOffset = Calendar.isBrowserInDaylightSavingTime() ? Calendar.getDstOffsetForTimestamp() : 0;
     
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        
        "calsy_checkin_time_in": parseInt(Calendar.getTimestampFromFields("#calsy_checkin_time_in_field_date", "#calsy_checkin_time_in_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds(),
        "calsy_checkin_time_out": parseInt(Calendar.getTimestampFromFields("#calsy_checkin_time_out_field_date", "#calsy_checkin_time_out_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds()
        
    }, "#" + Perisian.containerIdentifier, "calsy_checkin_");
            
    if(!CheckInFrontend.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            CheckInFrontend.cancel(); 
            CheckInFrontend.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

CheckInFrontend.deleteCheckIn = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        CheckInFrontend.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

CheckInFrontend.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        CheckInFrontend.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    /*
    if(!Validation.checkRequired(sendData.calsy_area_title))
    {

        Area.highlightInputTitleError('calsy_area_title', false);
        
        ++errors;

    }
    */
   
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
CheckInFrontend.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
CheckInFrontend.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'u': CheckInFrontend.filterUserFrontendId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
CheckInFrontend.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};