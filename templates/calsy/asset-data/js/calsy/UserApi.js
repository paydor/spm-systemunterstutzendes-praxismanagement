/**
 * Class to handle API users
 *
 * @author Peter Hamm
 * @date 2016-11-24
 */
var UserApi = jQuery.extend(true, {}, PerisianBaseAjax);

UserApi.controller = baseUrl + "/user_api/overview/";UserApi

UserApi.currentSortOrder = 'ASC';
UserApi.currentSorting = 'calsy_user_api_name';
    
UserApi.createNewUserApi = function()
{
    
    UserApi.editUserApi();
    
};

UserApi.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

/**
 * Opens the form to edit/create an API user as a modal.
 * 
 * @author Peter Hamm
 * @param int editId Optional. Leave empty to create a new entry.
 * @returns void
 */
UserApi.editUserApi = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (editId && editId > 0) ? 'edit' : 'new',
        'step': 1
        
    };

    this.editEntry(sendData, function() {jQuery('#calsy_user_api_name').focus();});
    
};

/**
 * Opens the form to edit API access for API users
 * 
 * @author Peter Hamm
 * @param int editId
 * @returns void
 */
UserApi.editAccess= function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': 'editAccess',
        'step': 1
        
    };

    this.editEntry(sendData, function() {jQuery('#calsy_user_api_name').focus();});
    
};

UserApi.saveAccess = function()
{

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var data = {};
        
    elementContainer.find('.api-user-rights').find('input:checkbox').each(function() {
        
        var item = jQuery(this);
        
        var identifier = item.attr('id');
        
        var resourceName = identifier.substr(0, identifier.lastIndexOf("_"));
        var type = identifier.substr(identifier.lastIndexOf("_") + 1);
                
        if(!data[resourceName])
        {
            
            data[resourceName] = {};
            
        }
        
        data[resourceName][type] = item.is(':checked') ? 1 : 0;
        
    });
    
    var sendData = {

        "do": "saveAccess",
        "editId": jQuery("#editId").val(),
        "data": data

    };
    
    jQuery.post(this.controllerImport, sendData, function(data) {
               
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
            
            toastr.warning(callback.message);
            
        }
        else
        {
            
            toastr.success(callback.message)
            
        }
                
    });
    
};

UserApi.newPassword = function(editId)
{

    var sendData = {

        'editId' : editId,
        'do': 'editPassword',
        'step': 1

    };

    this.editEntry(sendData, function() { jQuery('#user_api_password_1').focus();} );

};

UserApi.savePassword = function()
{

    var sendData = {

        "do" : "savePassword",
        "editId" : jQuery("#editId").val(),
        "step" : 1,
        "user_api_password_1" : jQuery("#user_api_password_1").val(),
        "user_api_password_2" : jQuery("#user_api_password_2").val()

    };
    
    this.saveEntry(sendData, function(data) { 
        
        return UserApi.validatePassword(data);
    
    }, null, null, true, false);
    
};

UserApi.validatePassword = function(sendData)
{

    this.highlightInputTitleError('user_api_password_1', 'remove');
    this.highlightInputTitleError('user_api_password_2', 'remove');

    if(!Validation.checkPassword(sendData.user_api_password_1, sendData.user_api_password_2, '#password_error_box'))
    {

        this.highlightInputTitleError('user_api_password_1');
        this.highlightInputTitleError('user_api_password_2');
        
        return false;

    }

    return true;

};

/**
 * Saves the currently edited entry.
 * 
 * @author Peter Hamm
 * @returns void
 */
UserApi.saveUserApi = function()
{
    
    var editId = jQuery("#editId").val();
    
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId
        
    }, "#" + Perisian.containerIdentifier, "calsy_user_api_");
        
    if(!UserApi.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
            
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable('p583721001abca'));
            
            this.dataSent = false; 
            
            if(typeof(Calendar) != "undefined" && Calendar.newUserApiData != null)
            {
                
                var calendarEntryId = Calendar.newUserApiData.calendarEntryId;
                
                Calendar.newUserApiData = {
                    
                    'index': Calendar.newUserApiData.index,
                    'id': callback.newId,
                    'name': callback.name
                    
                };
                
                Calendar.handleEditEvent(calendarEntryId);
                
            }
            else
            {
                
                UserApi.cancel(); 
                UserApi.showEntries(); 
                
            }
                    
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

UserApi.deleteUserApi = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10099, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        UserApi.showEntries();
        
        toastr.success(Language.getLanguageVariable(10479));
    
    });
    
};

UserApi.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        UserApi.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_user_api_name))
    {

        UserApi.highlightInputTitleError('calsy_user_api_name', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
UserApi.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
                
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
UserApi.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
UserApi.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};
