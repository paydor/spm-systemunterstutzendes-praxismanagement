/**
 * Class to handle tables for reservations
 *
 * @author Peter Hamm
 * @date 2020-09-23
 */
var TableReservationTable = jQuery.extend(true, {}, PerisianBaseAjax);

TableReservationTable.controller = baseUrl + "table_reservation/overview_table/";
TableReservationTable.controllerLocation = baseUrl + "table_reservation/overview/";

TableReservationTable.currentSortOrder = 'ASC';
TableReservationTable.currentSorting = 'calsy_table_reservation_table_title';
TableReservationTable.tableList = null;
    
TableReservationTable.createNewTable = function()
{
    
    TableReservationTable.editTable();
    
};

/**
 * Retrieves a list of all entries
 * 
 * @author Peter Hamm
 * @returns array
 */
TableReservationTable.getTableList = function()
{
    
    if(TableReservationTable.tableList != null)
    {
        
        return TableReservationTable.tableList;
        
    }
    
    var sendData = {

        'do': 'getTableList'
        
    };

    jQuery.ajax({
        
        url: TableReservationTable.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            TableReservationTable.tableList = decodedData.list;
                        
        }
        
    });
    
    return TableReservationTable.tableList;
    
};

TableReservationTable.goToLocationOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controllerLocation);
    
};

TableReservationTable.editTable = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

/**
 * Should be fired when the edit form for an entry is being opened.
 * 
 * @author Peter Hamm
 * @returns void
 */
TableReservationTable.initEditTable = function()
{
    
    jQuery('#calsy_table_reservation_table_title').focus();
    
};

TableReservationTable.saveTable = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        "calsy_table_reservation_table_location_id": TableReservationTable.locationId
        
    }, "#" + Perisian.containerIdentifier, "calsy_table_reservation_");
            
    if(!TableReservationTable.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery('#submitButton').prop('disabled', true);
    
    jQuery.post(this.controller, sendData, function(data) {
               
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            TableReservationTable.cancel(); 
            TableReservationTable.showEntries(); 
                        
        }
        
        jQuery('#submitButton').prop('disabled', false);
        
    });

};

TableReservationTable.deleteTable = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        TableReservationTable.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

TableReservationTable.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        TableReservationTable.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData['calsy_table_reservation_table_title']))
    {

        TableReservationTable.highlightInputTitleError('calsy_table_reservation_table_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
TableReservationTable.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
TableReservationTable.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'l': TableReservationTable.locationId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
TableReservationTable.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            
            this.currentSortOrder = 'ASC';
            
        }
        else
        {
            
            this.currentSortOrder = 'DESC';
            
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};