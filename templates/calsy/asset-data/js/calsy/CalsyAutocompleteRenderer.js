var CalsyAutocompleteRenderer = {
    
    renderUserFrontend: function(matchingObject, fieldToIdentify, fieldToDisplay) {

        var item = '<div class="perisian-autocomplete-container-element"';

        item += ' data-id="' + fieldToIdentify + '"';
        item += ' data-value="' + matchingObject[fieldToDisplay] + '"';

        item += ' data-displayfield="' + fieldToDisplay + '">';
        
        if(matchingObject['calsy_user_frontend_image_profile_address'] && matchingObject['calsy_user_frontend_image_profile_address'].length > 0)
        {
            
            item += ' <div class="user-color user-color-head" style="background-repeat: no-repeat; background-size: cover; margin-right: 5px; background-image: url(\'' + matchingObject['calsy_user_frontend_image_profile_address'] + '\');"></div>&nbsp;';
            
        }
        else
        {
            
            item += ' <div class="fa fa-' + matchingObject['calsy_user_frontend_sex'] + '"></div>&nbsp;';
            
        }

        item += matchingObject[fieldToDisplay] + "</div>";
        
        return item;

    },
    
    renderUser: function(matchingObject, fieldToIdentify, fieldToDisplay) {

        var item = '<div class="perisian-autocomplete-container-element"';

        item += ' data-id="' + fieldToIdentify + '"';
        item += ' data-value="' + matchingObject[fieldToDisplay] + '"';

        item += ' data-displayfield="' + fieldToDisplay + '">';

        item += ' <div class="user-color" style="background-color:' + matchingObject['user_color'] + '; background-image: url(\'' + matchingObject['user_image_profile'] + '\');"></div>';

        item += matchingObject[fieldToDisplay] + "</div>";

        return item;

    },
    
    renderSupplier: function(matchingObject, fieldToIdentify, fieldToDisplay) {

        var item = '<div class="perisian-autocomplete-container-element"';

        item += ' data-id="' + fieldToIdentify + '"';
        item += ' data-value="' + matchingObject[fieldToDisplay] + '"';

        item += ' data-displayfield="' + fieldToDisplay + '">';

        item += ' <div class="md md-add-circle-outline"></div>&nbsp;';

        item += matchingObject[fieldToDisplay] + "</div>";

        return item;

    }
    
};
