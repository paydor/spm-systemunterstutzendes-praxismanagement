var AppointmentAssistantTileProposals = {
    
    'controllerLoggedIn': baseUrl + 'calendar/assistant/',
    'controller': baseUrl + "ajax/appointment_tiles/",
    'areaGroup': 0,
    'currentPage': 0,
    'hasNextPage': false,
    
    'init': function(areaGroup) 
    {
                
        areaGroup = parseInt(areaGroup);
        
        if(areaGroup > 0)
        {
            
            AppointmentAssistantTileProposals.areaGroup = areaGroup;
            
        }
                
        AppointmentAssistantTileProposals.loadPage(0);
        
        AppointmentAssistantTileProposals.initButtons();
        
    },
    
    'initButtons': function() 
    {
        
        jQuery('#appointment-assistant-tile-proposals-btn-previous').click(AppointmentAssistantTileProposals.navigateToPreviousPage);
        jQuery('#appointment-assistant-tile-proposals-btn-next').click(AppointmentAssistantTileProposals.navigateToNextPage);
        jQuery('#appointment-assistant-tile-proposals-btn-find').click(AppointmentAssistantTileProposals.hideTiles);
        jQuery('.appointment-assistant-tile-proposals-btn-show-tiles').click(AppointmentAssistantTileProposals.showTiles);
        
    },
    
    'showTiles': function()
    {
                
        jQuery('#appointment-assistant').css('display', 'none');
        jQuery('#appointment-assistant-tile-proposals-box').css('display', 'block');
        
    },
    
    'hideTiles': function()
    {
        
        jQuery('#appointment-assistant-tile-proposals-box').css('display', 'none');
        jQuery('#appointment-assistant').css('display', 'block');
        
    },
    
    'navigateToPreviousPage': function()
    {
        
        var previousPageIndex = AppointmentAssistantTileProposals.currentPage - 1;
        previousPageIndex = previousPageIndex < 0 ? 0 : previousPageIndex;
        
        AppointmentAssistantTileProposals.loadPage(previousPageIndex);
        
    },
    
    'navigateToNextPage': function()
    {
        
        var nextPageIndex = AppointmentAssistantTileProposals.hasNextPage ? AppointmentAssistantTileProposals.currentPage + 1 : AppointmentAssistantTileProposals.currentPage;
        
        AppointmentAssistantTileProposals.loadPage(nextPageIndex);
        
    },
    
    'loadPage': function(index)
    {
        
        var sendData = {
            
            'do': 'getTileInfo',
            'page': index,
            'ag': AppointmentAssistantTileProposals.areaGroup
            
        };
        
        AppointmentAssistantTileProposals.displayLoadingIndicator();
        
        jQuery.post(AppointmentAssistantTileProposals.controller, sendData, function(data) {
            
            var resultData = JSON.parse(data);
                                    
            if(resultData['success'])
            {
                
                AppointmentAssistantTileProposals.currentPage = resultData['page'];
                AppointmentAssistantTileProposals.hasNextPage = resultData['hasNextPage'];

                AppointmentAssistantTileProposals.displayResult(resultData['tiles']);

            }
            else
            {
                
                AppointmentAssistantTileProposals.getContainer().html(resultData.message);
                
                AppointmentAssistantTileProposals.currentPage = 0;
                AppointmentAssistantTileProposals.hasNextPage = false;
                
            }
            
            AppointmentAssistantTileProposals.onPageLoaded();
            
        });
        
    },
    
    'onPageLoaded': function()
    {
        
        jQuery('#appointment-assistant-tile-proposals-btn-previous').css('display', AppointmentAssistantTileProposals.currentPage > 0 ? 'inline-block' : 'none');
        jQuery('#appointment-assistant-tile-proposals-btn-next').css('display', AppointmentAssistantTileProposals.hasNextPage ? 'inline-block' : 'none');
        
        AppointmentAssistantTileProposals.initInfoButtons();
        AppointmentAssistantTileProposals.initTileClicks();
        
    },
    
    'initTileClicks': function()
    {
      
        jQuery('.tile-proposal').click(function() {
            
            var element = jQuery(this);
            var elementId = element.attr('data-bookable-time-id');
                        
            AppointmentAssistant.getTabLink();
            
            var parameters = {

                'action': 'jump',
                'bt': elementId,

            };
            
            var assistantUrl = baseUrl;
            
            if(jQuery('#isUserLoggedIn') && jQuery('#isUserLoggedIn').val() == "1")
            {
                
                assistantUrl = AppointmentAssistantTileProposals.controllerLoggedIn; 
               
            }
            else
            {
                
                parameters['do'] = 'aa';
                
            }

            var pageLink = assistantUrl + '?' + AppointmentAssistant.serializeParameters(parameters);
            
            location.href = pageLink;
            
        });
        
    },
    
    'initInfoButtons': function()
    {
        
        jQuery('.tile-proposal-content-btn-info').click(function(e) {
            
            e.preventDefault();
            e.stopImmediatePropagation();
            
            var element = jQuery(this);
            
            AppointmentAssistant.showBookableTimeDetails(element.attr('data-bookable-time-id'));
            
        });
                
    },
    
    'displayResult': function(tileData)
    {
        
        var tileContent = '';
                
        for(var i = 0; i < tileData.length; ++i)
        {
            
            tileContent += AppointmentAssistantTileProposals.buildTile(tileData[i]);
            
        }
        
        AppointmentAssistantTileProposals.getContainer().html(tileContent);
        
    },
    
    'buildTile': function(tileData)
    {
        
        var tileContent = '<div class="col-lg-3 tile-proposal" data-bookable-time-id="' + tileData.id + '">';
        
        tileContent += '<div class="tile-proposal-content" style="background-image:url(' + tileData.image + ')">';
        tileContent += '<div class="tile-proposal-content-overlay"></div>';
        tileContent += '</div>';
        
        tileContent += '<div class="tile-proposal-content-title">' + tileData['title'] + '</div>';
        tileContent += '<div class="tile-proposal-content-time">' + tileData['time_formatted'] + '</div>';
        tileContent += '<button type="button" class="tile-proposal-content-btn-info" data-bookable-time-id="' + tileData.id + '"><i class="fa fa-info"></i></button>';
        
        if(tileData['bookings_max'] && parseInt(tileData['bookings_max']) > 0)
        {
            
            tileContent += '<div class="booking-count-indicator' + (tileData['bookings_current'] == tileData['bookings_max'] ? ' booking-count-indicator-full' : '') + '">' + tileData['bookings_current'] + '/' + tileData['bookings_max'] + '</div>';
            
        }
        
        tileContent += '</div>';
        
        return tileContent;
        
    },
    
    'getContainer': function()
    {
        
        return jQuery('#appointment-assistant-tile-proposal-container');
        
    },
    
    'displayLoadingIndicator': function() 
    {
        
        var loadingIndicator = '<div class="loading-indicator"></div>';
        
        AppointmentAssistantTileProposals.getContainer().html(loadingIndicator);
        
    }
    
};
