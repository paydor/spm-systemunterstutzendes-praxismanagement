
var AppointmentField = {
    
    controller: baseUrl + "/module/calsy_user_frontend_registration_and_calendar/",
    
    containerId: '',
    containerElement: null,
    fieldData: {},
    
    conditionIteration: 0
    
};

/**
 * Initializes the appointment field editor.
 * 
 * @author Peter Hamm
 * @param String containerId
 * @returns void
 */
AppointmentField.initialize = function(containerId, fieldData)
{
    
    AppointmentField.fieldData = fieldData;
    AppointmentField.containerId = containerId.replace('#', '');
    AppointmentField.containerElement = jQuery('#' + AppointmentField.containerId);
    
    AppointmentField.initFieldData();
    
    Area.getAreaList();
    
    AppointmentField.onLoaded();
    
};

/**
 * Fired as soon as all data is loaded and displayed
 * 
 * @author Peter Hamm
 * @returns void
 */
AppointmentField.onLoaded = function()
{
    
    AppointmentField.initButtons();
    
    jQuery('#loading-box').css('display', 'none');
    jQuery('#additional-field-controls').css('display', 'block');
    
    AppointmentField.switchTab(AppointmentField.getCurrentTabIndex());
        
};

/**
 * Initializes the main buttons 
 * 
 * @author Peter Hamm
 * @returns void
 */
AppointmentField.initButtons = function()
{
    
    AppointmentField.containerElement.find('.appointment-field-tab').click(function() {
        
        var clickedIndex = jQuery(this).attr('data-index');
        
        AppointmentField.switchTab(clickedIndex);
        
    });
    
    AppointmentField.containerElement.find('#button-add-new').click(function() {
        
        AppointmentField.addNewField(AppointmentField.getCurrentTabIndex(), AppointmentField.containerElement.find('#type_selector').val());
        
    });
    
    AppointmentField.containerElement.find('#button-save-all').click(function() {
        
        AppointmentField.saveFields();
        
    });
    
};

/**
 * Validates the field data and highlights eventual errors.
 * 
 * @author Peter Hamm
 * @param Object fieldData
 * @returns bool
 */
AppointmentField.validateFields = function(fieldData)
{
    
    return true;
    
};

/**
 * Saves all the data of the form.
 * 
 * @author Peter Hamm
 * @returns void
 */
AppointmentField.saveFields = function()
{
    
    var fieldData = AppointmentField.getFieldData();
    
    if(AppointmentField.validateFields(fieldData))
    {
        
        var sendData = {
            
            'do': 'saveFields',
            'fields': fieldData
            
        };
                
        jQuery.post(AppointmentField.controller, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);

            if(decodedData.success)
            {
                                
                toastr.success(decodedData.message);
                
            }
            else
            {
                
                toastr.warning(decodedData.message);
                
            }

        });
        
    }
    
};

/**
 * Compiles all the field data from the form.
 * 
 * @author Peter Hamm
 * @returns Object
 */
AppointmentField.getFieldData = function()
{
    
    var fieldData = {
        
        pages: {}
        
    };
    
    AppointmentField.containerElement.find('.appointment-field-container').each(function() {
       
        var currentContainer = jQuery(this);
        var currentPageIndex = currentContainer.attr('data-index');
        
        currentContainer.find('.additional-field-main-container').each(function() {
            
            // The single field
            var currentField = jQuery(this);
            var currentFieldType = currentField.find('#type').attr('data-type');
            
            var currentFieldData = {
                
                page: currentPageIndex,
                identifier: currentField.find('#identifier').val(),
                label: currentField.find('#label_language_id').val(),
                type: currentFieldType
                
            };
            
            if(currentFieldType == 'radio' || currentFieldType == 'select')
            {
                
                var options = {
                    
                    values: {}
                    
                };
                
                // The radio options
                currentField.find('.option-container').find('.radio-option').each(function() {
                    
                    var currentOption = jQuery(this);
                    
                    var optionKey = currentOption.find('#option-value').val();
                    var optionValue = currentOption.find('#option-label_language_id').val();
                    
                    if(optionKey.length > 0 && optionValue.length > 0)
                    {
                        
                        options.values[optionKey] = optionValue;
                        
                    }
                    
                });
                
                currentFieldData.data = options;
                
            }
            else if(currentFieldType == 'area-filter')
            {
                
                currentFieldData.data = {
                    
                    value: currentField.find('#area_selector').val()
                    
                };
                
            }
            else if(currentFieldType == 'area-group-filter')
            {
                
                currentFieldData.data = {
                    
                    value: currentField.find('#area_group_selector').val()
                    
                };
                
            }
            
            // Add the conditions
            {
                
                if(!currentFieldData.data || currentFieldData.data == null)
                {

                    currentFieldData.data = {};

                }
                
                currentFieldData.data['required'] = currentField.find('#isRequired :selected').val() == 1;
                
                currentFieldData.data['conditions'] = [];
                
                currentField.find('.condition-container').find('.condition-field').each(function() {
                    
                    var currentOption = jQuery(this);
                    
                    var conditionData = {
                        
                        'identifier': currentOption.find('#condition-identifier').val(),
                        'operator': currentOption.find('#condition-operator').val(),
                        'value': currentOption.find('#condition-value').val()
                        
                    };
                    
                    if(conditionData.identifier.length > 0 && conditionData.operator.length > 0)
                    {
                        
                        currentFieldData.data['conditions'].push(conditionData);
                        
                    }
                    
                });
            
            }
            
            // Add it to the list.
            if(!fieldData.pages[currentPageIndex])
            {
                
                fieldData.pages[currentPageIndex] = [];
                
            }
            
            fieldData.pages[currentPageIndex].push(currentFieldData);
            
        });
        
    });
    
    return fieldData;
    
};

/**
 * Adds a new field of the specified type to the tab with the specified tab index.
 * 
 * @author Peter Hamm
 * @param int tabIndex
 * @param String fieldType
 * @returns void
 */
AppointmentField.addNewField = function(tabIndex, fieldType)
{
    
    // Check if this is a unique field and if it may already be added.
    {
        
        var isUniqueAndExists = false;
    
        if(fieldType == 'area-selector')
        {
            
            var existingElementCount = AppointmentField.containerElement.find("[data-type='" + fieldType + "']").length;
            
            if(existingElementCount >= 1)
            {
                
                isUniqueAndExists = true;
                
            }
            
        }
        
        if(isUniqueAndExists)
        {
            
            toastr.warning(Language.getLanguageVariable('p577e5cee3253d'));
            
            return;
            
        }
    
    }
    
    var fieldData = {
        
        'label': null,
        'type': fieldType,
        'required': false,
        'data': null,
        'identifier': ''
        
    };
    
    AppointmentField.addField(tabIndex, fieldData);
    
};

/**
 * Returns the index of the currently active tab.
 * 
 * @author Peter Hamm
 * @returns int
 */
AppointmentField.getCurrentTabIndex = function()
{
    
    var tabIndex = AppointmentField.containerElement.find(".appointment-field-tab.active").attr('data-index');
    
    return tabIndex;
    
};

/**
 * Initializes the fields displayed from the field data provided.
 * 
 * @author Peter Hamm
 * @returns void
 */
AppointmentField.initFieldData = function()
{
        
    for(var pageIndex in AppointmentField.fieldData.pages)
    {
        
        var pageContent = AppointmentField.fieldData.pages[pageIndex];
        
        var container = AppointmentField.getContainerByIndex(pageIndex);
                
        if(container.length > 0)
        {
                        
            // The container exists, we may now add the fields
            
            for(var i = 0; i < pageContent.length; ++i)
            {
                
                AppointmentField.addField(pageIndex, pageContent[i]);
                
            }
            
        }
        
    }
    
};

/**
 * Tries to retrieve the container with the specified page index.
 * 
 * @author Peter Hamm
 * @param int index
 * @returns Object
 */
AppointmentField.getContainerByIndex = function(index)
{
    
    return AppointmentField.containerElement.find(".appointment-field-container[data-index='" + index + "']");
    
};

/**
 * Generates a gield from the specified field data and adds it to the container with the specified tab index.
 * 
 * @param int tabIndex
 * @param Object fieldData
 * @returns void
 */
AppointmentField.addField = function(tabIndex, fieldData)
{
    
    var container = AppointmentField.getContainerByIndex(tabIndex);
    
    var generatedField = AppointmentField.generateField(fieldData, tabIndex);
    
    if(generatedField != null)
    {
        
        AppointmentField.addEventsToField(generatedField);
        
        container.append(generatedField);
        
    }
    
};

/**
 * Sets the language variable editing callback to the specified label and language variable input fields.
 * 
 * @author Peter Hamm
 * @param Object label
 * @param Object labelLanguageVariable
 * @returns void
 */
AppointmentField.setLanguageVariableCallbackFields = function(label, labelLanguageVariable)
{
  
    LanguageVariable.onSaveSuccessful = function(savedLanguageId)
    {
                
        labelLanguageVariable.val(savedLanguageId);
        label.val(Language.getLanguageVariable(savedLanguageId, true));
        
    }
    
};

/**
 * Moves the specified to the specified direction ('up' / 'down')
 * 
 * @author Peter Hamm
 * @param Object field
 * @param Sring direction A direction, 'up' or 'down.
 * @returns void
 */
AppointmentField.moveField = function(field, direction)
{
    
    var sibling = jQuery(direction == 'up' ? field.prev() : field.next());
        
    if(sibling.length > 0)
    {
               
        if(direction == 'up')
        {
            
            jQuery(sibling).insertAfter(field);
            
        }
        else
        {
            
            jQuery(sibling).insertBefore(field);
            
        }
        
    }
    
};

/**
 * Adds events to the elements of the specified field.
 * 
 * @author Peter Hamm
 * @param Object field
 * @returns void
 */
AppointmentField.addEventsToField = function(field) 
{
    
    field.find('.button-remove').click(function() {
        
        field.remove();
        
    });
    
    field.find('.button-move-up').click(function() {
        
        AppointmentField.moveField(jQuery(this).parent().parent(), 'up');
        
    });
    
    field.find('.button-move-down').click(function() {
        
        AppointmentField.moveField(jQuery(this).parent().parent(), 'down');
        
    });
    
    field.find('#label_language_id').change(function() {
       
        field.find('#label').val(Language.getLanguageVariable(jQuery(this).val()));
        
    });
    
    var label = field.find('#label');
    var labelLanguageVariable = field.find('#label_language_id');
    
    field.find('#button-edit').click(function() {
        
        label.trigger('click');
    
    });
    
    field.find('#button-search').click(function() {
        
        AppointmentField.searchLanguageVariable(label, labelLanguageVariable);
    
    });
        
    field.find('#button-clear').click(function() {
        
        label.val('');
        labelLanguageVariable.val('');
    
    });
    
    label.click(function() {
                
        AppointmentField.setLanguageVariableCallbackFields(label, labelLanguageVariable);
        
        // Language variable editing
        if(labelLanguageVariable.val() && labelLanguageVariable.val().length > 0)
        {
            
            LanguageVariable.editVariable(labelLanguageVariable.val());
            
        }
        else
        {
            
            LanguageVariable.createNewVariable();
            
        }
        
    });
    
    // Option actions
    {
        
        AppointmentField.addFieldOptionActions(field);

        field.find('#button-add-new-option').click(function() {

            var contentBox = field.find('.option-container');

            var element = AppointmentField.generateRadioOption(null, null);

            AppointmentField.addFieldOptionActions(element);

            contentBox.append(element);

        });
        
    }
    
    // Condition actions
    {

        AppointmentField.addFieldConditionActions(field);

        field.find('#button-add-new-condition').click(function() {

            var contentBox = field.find('.condition-container');

            var element = AppointmentField.generateConditionField(null);

            AppointmentField.addFieldConditionActions(element);

            contentBox.append(element);

        });
        
    }
    
};

/**
 * Opens the dialog to search for an exisiting language variable.
 * 
 * @author Peter Hamm
 * @param Object label
 * @param Object labelLanguageVariable
 * @returns void
 */
AppointmentField.searchLanguageVariable = function(label, labelLanguageVariable)
{
    
    LanguageVariable.onVariableSelected = function(pickedLanguageId) {
        
        labelLanguageVariable.val(pickedLanguageId);
        label.val(Language.getLanguageVariable(pickedLanguageId, true));
        
        Perisian.ajaxBox('hide');
        
    };
    
    LanguageVariable.showSearchPopup();
    
};

/**
 * Retrieves a list of possible field identifiers from the DOM.
 * 
 * @author Peter Hamm
 * @param Object exceptionElement Optional, an element (additional field) that should not be in the list
 * @returns array
 */
AppointmentField.getFieldIdentifierList = function(exceptionElement)
{
    
    var list = [];
    var formattedList = [];
    
    var exceptionValue = exceptionElement.find('#identifier').val();
        
    jQuery('.additional-field').each(function() {
        
        var element = jQuery(this);
        
        var value = element.find("#identifier").val();
        
        if((!exceptionValue || exceptionValue && value != exceptionValue) && typeof value !== 'undefined' && value.length > 0)
        {
            
            list.push(value);
            
        }
        
    });
    
    list.sort();
        
    for(var i = 0; i < list.length; ++i)
    {
        
        formattedList.push({
                
            'value': list[i]
                
        });
        
    }
        
    return formattedList;
    
};

/**
 * Adds actions to conditions.
 * 
 * @autor Peter Hamm
 * @param Object field
 * @returns void
 */
AppointmentField.addFieldConditionActions = function(field)
{
            
    field.find('#condition-identifier').on('focus', function() {
        
        var focusedInput = jQuery(this);
        var field = focusedInput.parent().parent().parent().parent().parent().parent();
        
        var results = AppointmentField.getFieldIdentifierList(field);
                
        PerisianBaseAjax.showAutocompleteResult({

            'element': focusedInput,
            'matching': results,
            'fieldToDisplay': 'value',
            'maxDisplayedEntries': -1,
            
            'keyDownCallback': function(fieldValue, elements) {
                                
                var newList = [];
                
                for(var i = 0; i < elements.length; ++i)
                {
                    
                    if(elements[i]['value'].substr(0, fieldValue.length) == fieldValue)
                    {
                        
                        newList.push(elements[i]);
                        
                    }
                    
                }
                
                return newList;
                
            }

        });
        
    });
   
    field.find('.button-remove-condition').click(function() {
        
        var button = jQuery(this);
        
        button.parent().parent().parent().remove();
        
    });
    
};

/**
 * Adds actions to options.
 * 
 * @autor Peter Hamm
 * @param Object field
 * @returns void
 */
AppointmentField.addFieldOptionActions = function(field)
{
    
    field.find('.button-remove-option').click(function() {
        
        var button = jQuery(this);
        
        button.parent().parent().parent().remove();
        
    });
        
    field.find('#button-option-edit').click(function() {
        
        var label = jQuery(this).parent().parent().parent().find('#option-label');
        
        label.trigger('click');
    
    });
    
    field.find('#button-option-search').click(function() {
        
        var label = jQuery(this).parent().parent().parent().find('#option-label');
        var labelLanguageVariable = label.parent().parent().parent().find('#option-label_language_id');
        
        AppointmentField.searchLanguageVariable(label, labelLanguageVariable);
    
    });
    
    field.find('#button-option-clear').click(function() {
        
        var label = jQuery(this).parent().parent().parent().find('#option-label');
        var labelLanguageVariable = label.parent().parent().parent().find('#option-label_language_id');
        
        label.val('');
        labelLanguageVariable.val('');
    
    });
    
    field.find('#option-label_language_id').change(function() {
        
        var variableField = jQuery(this);
       
        variableField.parent().parent().parent().find('#option-label').val(Language.getLanguageVariable(variableField.val()));
        
    });
    
    field.find('#option-label').click(function() {
        
        var label = jQuery(this);
        var labelLanguageVariable = label.parent().parent().parent().find('#option-label_language_id');
                        
        AppointmentField.setLanguageVariableCallbackFields(label, labelLanguageVariable);
        
        // Language variable editing
        if(labelLanguageVariable.val() && labelLanguageVariable.val().length > 0)
        {
            
            LanguageVariable.editVariable(labelLanguageVariable.val());
            
        }
        else
        {
            
            LanguageVariable.createNewVariable();
            
        }
        
    });
    
};

/**
 * Retrieves a decent name for a specified input type key, as for example "radio" will return "Radio button".
 * 
 * @author Peter Hamm
 * @param String typeKey
 * @return String
 */
AppointmentField.getTypeName = function(typeKey)
{
    
    // "Unknown"
    var variableId = 11087;
    
    if(typeKey == "radio")
    {
        
        variableId = 11088;
        
    }
    else if(typeKey == "text")
    {
        
        variableId = 11089;
        
    }
    else if(typeKey == "select")
    {
        
        variableId = 11215;
        
    }
    else if(typeKey == 'area-filter')
    {
        
        variableId = 'p575ecd585ad92';
        
    }
    else if(typeKey == 'area-group-filter')
    {
        
        variableId = 'p59ba7a0d156f8';
        
    }
    else if(typeKey == 'area-elector')
    {
        
        variableId = 'p577e5b01378dd';
        
    }
    
    return Language.getLanguageVariable(variableId);
    
};

/**
 * Generates a field from the data specified.
 * 
 * @author Peter Hamm
 * @param Object fieldData
 * @returns Object
 */
AppointmentField.generateField = function(fieldData, tabIndex)
{
    
    if(fieldData == null)
    {
        
        return null;
        
    }
    
    try
    {
        
        var fieldIdentifier = fieldData.identifier;
        
        var mainContainer = jQuery('<div/>').addClass('additional-field-main-container');
        
        mainContainer.append('<div class="additional-field-header"><button class="btn button-move-down"><i class="fa fa-arrow-down"></i></button>&nbsp;<button class="btn button-move-up"><i class="fa fa-arrow-up"></i></button>&nbsp;<button class="btn btn-danger button-remove">&times;</button></div>');
        
        var fieldBox = jQuery('<div/>').addClass('additional-field');
        
        if(fieldData.type == 'area-filter')
        {
            
            var titleBox = jQuery('<div/>').addClass('col-sm-12 no-sidepadding-left no-sidepadding-right');
            
            var areaList = Area.getAreaList();
                        
            var areaSelector = '<div class="form-group"><label id="type" data-type="area-filter" for="area_selector">' + Language.getLanguageVariable('p575ecd585ad92') + ' - ' + Language.getLanguageVariable('p575eea3b50829') + '</label>';
            
            areaSelector += '<select name="area_selector" id="area_selector" class="form-control static dirty">';
            
            areaSelector += '<option value="-1" ' + ((!fieldData.data || !fieldData.data.value || fieldData.data.value == '-1') ? ' selected' : '') + '>- ' + Language.getLanguageVariable('p575edf80ea145') + '</option>';
            areaSelector += '<option value="-2" ' + ((fieldData.data && fieldData.data.value && fieldData.data.value == '-2') ? ' selected' : '') + '>- ' + Language.getLanguageVariable('p575f12a3e925c') + '</option>';

            for(var i = 0; i < areaList.length; ++i)
            {
                
                areaSelector += '<option value="' + areaList[i]['calsy_area_id'] + '"' + ((fieldData.data && fieldData.data.value && fieldData.data.value == areaList[i]['calsy_area_id']) ? ' selected' : '') + '>' + areaList[i]['calsy_area_title'] + '</option>';
                
            }
            
            areaSelector += '</select></div>';
            
            titleBox.append(areaSelector);
            
            fieldBox.append(titleBox);
            
        }
        else if(fieldData.type == 'area-group-filter')
        {
                        
            var titleBox = jQuery('<div/>').addClass('col-sm-12 no-sidepadding-left no-sidepadding-right');
            
            var areaGroupList = AreaGroup.getAreaGroupList();
                        
            var areaGroupSelector = '<div class="form-group"><label id="type" data-type="area-group-filter" for="area_group_selector">' + Language.getLanguageVariable('p59ba7a0d156f8') + ' - ' + Language.getLanguageVariable(' p59ba88bc6b253') + '</label>';
            
            areaGroupSelector += '<select name="area_group_selector" id="area_group_selector" class="form-control static dirty">';
            
            areaGroupSelector += '<option value="-1" ' + ((!fieldData.data || !fieldData.data.value || fieldData.data.value == '-1') ? ' selected' : '') + '>- ' + Language.getLanguageVariable('p575edf80ea145') + '</option>';

            for(var i = 0; i < areaGroupList.length; ++i)
            {
                
                areaGroupSelector += '<option value="' + areaGroupList[i]['calsy_area_group_id'] + '"' + ((fieldData.data && fieldData.data.value && fieldData.data.value == areaGroupList[i]['calsy_area_group_id']) ? ' selected' : '') + '>' + areaGroupList[i]['calsy_area_group_title'] + '</option>';
                
            }
            
            areaGroupSelector += '</select></div>';
            
            titleBox.append(areaGroupSelector);
            
            fieldBox.append(titleBox);
            
        }
        else if(fieldData.type == 'area-selector')
        {
            
            var titleBox = jQuery('<div/>').addClass('col-sm-12 no-sidepadding-left no-sidepadding-right');
                                    
            var areaSelector = '<div class="form-group" id="type" data-type="area-selector"><span class="alert alert-info alert-block" style="text-align: left;">' + Language.getLanguageVariable('p577e5b01378dd') + '</span></div>';
                        
            titleBox.append(areaSelector);
            
            fieldBox.append(titleBox);
            
        }
        else
        {
            
            var titleBox = jQuery('<div/>').addClass('col-sm-12 no-sidepadding-left no-sidepadding-right');
            titleBox.append('<div class="form-group"><label for="label">' + Language.getLanguageVariable(10796) + ':</label>' + '<input readonly id="label" name="label" class="form-control additional-field-label readonly-edit" value="' + (fieldData.label != null ? Language.getLanguageVariable(fieldData.label) : '') + '"/></div>');

            fieldBox.append(titleBox);
            
            var buttons = '<button class="btn" id="button-clear">&times;</button>&nbsp;<button class="btn btn-primary-bright" id="button-edit"><i class="fa fa-edit"></i></button>&nbsp;<button class="btn btn-primary-bright" id="button-search"><i class="fa fa-search"></i></button>';

            var variableBox = jQuery('<div/>').addClass('col-sm-12 no-sidepadding-left no-sidepadding-right');
            variableBox.append('<div class="form-group" style="margin-bottom: 0px; text-align: right; white-space: nowrap">' + buttons + '<input style="display: none" id="label_language_id" name="label_language_id" class="form-control additional-field-label" value="' + (fieldData.label != null ? fieldData.label : '') + '"/></div>');
            
            fieldBox.append('<br style="clear:both"/>');
            
            fieldBox.append(variableBox);
            
            fieldBox.append('<br style="clear:both"/>');
        
            // Unique identifier
            fieldBox.append('<div class="form-group"><label for="identifier">' + Language.getLanguageVariable(11084) + ':</label>' + '<input id="identifier" name="identifier" class="form-control" value="' + fieldIdentifier + '"/></div>');

            // "Required" selector
            fieldBox.append('<div class="col-sm-6 no-sidepadding-left"><div class="form-group"><label for="isRequired">' + Language.getLanguageVariable(11080) + ':</label>' + '<select class="form-control static dirty" id="isRequired" name="isRequired"><option value="0"' + (!fieldData.data || fieldData.data.required == 'false' ? ' selected' : '') + '>' + Language.getLanguageVariable(11082) + '</option><option value="1"' + (fieldData.data && fieldData.data.required == 'true' ? ' selected' : '') + '>' + Language.getLanguageVariable(11081) + '</option></select></div></div>');

            // "Type" label
            fieldBox.append('<div class="col-sm-6 no-sidepadding-right"><div class="form-group"><label for="type">' + Language.getLanguageVariable(11086) + ':</label>' + '<input id="type" name="type" class="form-control" data-type="' + fieldData.type + '" readonly value="' + AppointmentField.getTypeName(fieldData.type) + '"/></div></div>');

        }
        
        // The content / options, depending on the type of the input.
        {
            
            var contentBox = jQuery('<div/>');
            
            var addContentBox = false;

            if(fieldData.type == 'radio' || fieldData.type == 'select')
            {
                
                addContentBox = true;
                
                contentBox.addClass('col-sm-12 field-content option-container');
                
                contentBox.append('<div class="options-header">' + Language.getLanguageVariable(11083) + '<button id="button-add-new-option" class="btn btn-primary-bright ink-reaction">+</button></div>');

                // Add the radios themselves
                if(fieldData.data && fieldData.data != null)
                {
                                        
                    var keys = Array.isArray(fieldData.data.values) ? fieldData.data.values.keys() : Object.keys(fieldData.data.values);
                    
                    for(var i = 0; i < keys.length; ++i)
                    {
                        
                        var key = keys[i];
                        var value = fieldData.data.values[key];
                                                
                        var element = AppointmentField.generateRadioOption(key, value);
                        contentBox.append(element);

                    };
                    
                }

            }
            
            if(addContentBox)
            {
                
                fieldBox.append(contentBox);
                
            }
            
        }
        
        // The conditions for this input
        if(fieldData.type != 'area-selector')
        {
            
            var conditionBox = jQuery('<div/>');

            {
                
                conditionBox.addClass('col-sm-12 field-content condition-container');
                
                conditionBox.append('<div class="condition-header">' + Language.getLanguageVariable(11105) + '<button id="button-add-new-condition" class="btn btn-primary-bright ink-reaction">+</button></div>');

                // Add each single condition
                if(fieldData.data && fieldData.data != null)
                {
                    
                    jQuery(fieldData.data.conditions).each(function(key, conditionData) {
                        
                        var element = AppointmentField.generateConditionField(key, conditionData);
                        conditionBox.append(element);

                    });
                    
                }
                else
                {
                    
                    if(fieldData.type == 'area-filter')
                    {
                        
                        var element = AppointmentField.generateConditionField(0, null);
                        conditionBox.append(element);
                        
                    }
                    else if(fieldData.type == 'area-group-filter')
                    {
                        
                        var element = AppointmentField.generateConditionField(0, null);
                        conditionBox.append(element);
                        
                    }
                    
                }

            }
            
            fieldBox.append(conditionBox);
            
        }
        
        mainContainer.append(fieldBox);
        mainContainer.append('<br style="clear:both"/>');
        
        return mainContainer;
        
    }
    catch(e)
    {
        
        console.log('Could not initialize a field.');
        console.log(e);
        
        return null;
        
    }
    
};

/**
 * Generates a field to define a single condition
 * 
 * @author Peter Hamm
 * @param String key
 * @param Object conditionData
 * @returns Object
 */
AppointmentField.generateConditionField = function(key, conditionData)
{
        
    var element = jQuery('<div class="condition-field" style="display: block"/>');
    
    var conditionIdentifierBox = jQuery('<div/>').addClass('col-sm-4 no-sidepadding-left no-sidepadding-right');
    conditionIdentifierBox.append('<div class="form-group"><label for="option-label">' + Language.getLanguageVariable(11084) + ':</label><input id="condition-identifier" name="condition-identifier" class="form-control additional-field-label" value="' + (conditionData && conditionData.identifier != null ? conditionData.identifier : '') + '" data-iteration="' + AppointmentField.conditionIteration + '"/></div>');
    element.append(conditionIdentifierBox);
    
    var conditionOperatorBox = jQuery('<div/>').addClass('col-sm-4');
    
    var operatorSelection = '<div class="form-group"><label for="condition-operator">' + Language.getLanguageVariable(11108) + ':</label><select id="condition-operator" name="condition-operator" class="form-control static dirty">';
    operatorSelection += '<option value="is"' + (conditionData && conditionData.operator == 'is' ? ' selected' : '') + '>' + Language.getLanguageVariable(11106) + '</option>';
    operatorSelection += '<option value="is_not"' + (conditionData && conditionData.operator == 'is_not' ? ' selected' : '') + '>' + Language.getLanguageVariable(11107) + '</option>';
    operatorSelection += '</select></div>'>
    
    conditionOperatorBox.append(operatorSelection);
    
    element.append(conditionOperatorBox);
    
    var conditionValueBox = jQuery('<div/>').addClass('col-sm-4 no-sidepadding-left no-sidepadding-right');
    conditionValueBox.append('<div class="form-group"><label for="option-label">' + Language.getLanguageVariable(11093) + ':</label><input id="condition-value" name="condition-value" class="form-control additional-field-label" value="' + (conditionData && conditionData.value != null ? conditionData.value : '') + '"/></div>');
    element.append(conditionValueBox);
    
    {
        
        var buttons = '<button class="btn btn-danger button-remove-condition">&times;</button>';

        var optionVariableBox = jQuery('<div/>').addClass('col-sm-12 condition-options no-sidepadding-left no-sidepadding-right');
        optionVariableBox.append('<div class="form-group" style="margin-bottom: 0px; text-align: right; white-space: nowrap; background-color: transparent;">' + buttons + '</div>');

        element.append('<br style="clear:both"/>');
        element.append(optionVariableBox);
        element.append('<br style="clear:both"/>');
        
    }
    
    ++AppointmentField.conditionIteration;
    
    return element;
    
};

/**
 * Generates a radio option for a field.
 * 
 * @author Peter Hamm
 * @param String key
 * @param String value
 * @returns Object
 */
AppointmentField.generateRadioOption = function(key, value) 
{
    
    var element = jQuery('<div class="radio-option" style="display: block"/>');

    var optionValueBox = jQuery('<div/>').addClass('col-sm-4 no-sidepadding-left');
    optionValueBox.append('<div class="form-group"><label for="option-value">' + Language.getLanguageVariable(11093) + ':</label>' + '<input id="option-value" name="option-value" class="form-control additional-field-label" value="' + (key != null ? key : '') + '"/></div>');

    element.append(optionValueBox);

    var optionTitleBox = jQuery('<div/>').addClass('col-sm-8 no-sidepadding-left no-sidepadding-right');
    optionTitleBox.append('<div class="form-group"><label for="option-label">' + Language.getLanguageVariable(10796) + ':</label><input readonly id="option-label" name="option-label" class="form-control additional-field-label readonly-edit" value="' + (value != null ? Language.getLanguageVariable(value) : '') + '"/></div>');

    element.append(optionTitleBox);
    
    {
        
        var buttons = '<button class="btn" id="button-option-clear">&times;</button>&nbsp;<button class="btn btn-primary-bright" id="button-option-edit"><i class="fa fa-edit"></i></button>&nbsp;<button class="btn btn-primary-bright" id="button-option-search"><i class="fa fa-search"></i></button>&nbsp;<button class="btn btn-danger button-remove-option">&times;</button>';

        var optionVariableBox = jQuery('<div/>').addClass('col-sm-12 option-options no-sidepadding-left no-sidepadding-right');
        optionVariableBox.append('<div class="form-group" style="margin-bottom: 0px; text-align: right; white-space: nowrap; background-color: transparent;">' + buttons + '<input style="display: none" id="option-label_language_id" name="option-label_language_id" class="form-control additional-field-label" value="' + (value != null ? value : '') + '"/></div>');

        element.append('<br style="clear:both"/>');
        element.append(optionVariableBox);
        element.append('<br style="clear:both"/>');
        
    }
    
    return element;
    
};

/**
 * Switches to the specified tab.
 * 
 * @author Peter Hamm
 * @param int targetIndex
 * @returns void
 */
AppointmentField.switchTab = function(targetIndex) 
{
    
    AppointmentField.containerElement.find('.appointment-field-tab').each(function() {
        
        var currentTab = jQuery(this);
        var currentIndex = currentTab.attr('data-index');
        
        if(currentIndex == targetIndex)
        {
            
            currentTab.addClass('active');
            
        }
        else
        {
            
            currentTab.removeClass('active');
            
        }
        
    });
    
    AppointmentField.containerElement.find('.appointment-field-container').each(function() {
        
        var currentContainer = jQuery(this);
        var currentIndex = currentContainer.attr('data-index');
        
        if(currentIndex == targetIndex)
        {
            
            currentContainer.addClass('active');
            
        }
        else
        {
            
            currentContainer.removeClass('active');
            
        }
                
    });
        
};
