
var CalsySurveyInvitation = {
    
    'controller': baseUrl + 'invitation/',
    
    'init': function()
    {
        
        jQuery('#btn-accept-appointment').click(CalsySurveyInvitation.confirmAppointment);
        jQuery('#btn-reject-appointment').click(CalsySurveyInvitation.rejectAppointment);
        
    },
    
    'confirmAppointment': function() 
    {
     
        CalsySurveyInvitation.updateAppointmentStatus(true);
        
    },
    
    'rejectAppointment': function() 
    {
        
        CalsySurveyInvitation.updateAppointmentStatus(false);
        
    },
    
    'updateAppointmentStatus': function(doConfirm) 
    {
        
        var confirmationMessage = Language.getLanguageVariable(doConfirm ? 'p5e4c6b8b33f99' : 'p5e4c6ba2a914a');
        
        if(!confirm(confirmationMessage))
        {
            
            return;
            
        }
        
        jQuery('.btn-invitation').attr('disabled', true);
        
        var sendData = {
            
            'do': doConfirm ? 'confirm' : 'reject',
            'id': jQuery('#entryId').val(),
            'code': jQuery('#code').val()
            
        };
        
        jQuery.post(CalsySurveyInvitation.controller, sendData, function(result) {
            
            var data = JSON.parse(result);
            
            if(data.success)
            {
                
                location.href = CalsySurveyInvitation.controller + '?do=' + (data.confirmed ? 'confirmReply' : 'rejectReply');
                
            }
            else
            {
                
                jQuery('.btn-invitation').attr('disabled', false);
        
                toastr.error(data.message);
                
            }
            
        });
        
    }
    
};
