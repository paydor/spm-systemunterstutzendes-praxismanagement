/**
 * Class to handle the calsy calendar
 *
 * @author Peter Hamm
 * @date 2016-01-14
 */
var Calendar = jQuery.extend(true, {}, PerisianBaseAjax);

Calendar.controller = baseUrl + "/calendar/overview/";

Calendar.calendarType = null;
Calendar.isGlobal = false;

// Behaviour
Calendar.jumpToTodayAfterSaving = false;
Calendar.editEntryDirectly = false;
Calendar.filterAreaByGender = false;

// Calendar entry display
Calendar.containerId = null;
Calendar.filterUsers = null;
Calendar.filterUserFrontend = null;
Calendar.filterFlags = null;
Calendar.filterResource = null;
Calendar.currentDisplayType = null;
Calendar.currentStartDate = null;
Calendar.listAreas = null;
Calendar.lastScrollTop = -1;
Calendar.lastScrollLeft = -1;
Calendar.displayableDaysDaily = null;
Calendar.displayableDaysWeekly = null;
Calendar.displayableDaysMonthly = null;

// Calendar detail display
Calendar.containerDetailId = null;
Calendar.currentDetailPage = null;

// Default start time hours and minutes for events.
Calendar.defaultEventStartTimeHours = 0;
Calendar.defaultEventStartTimeMinutes = 0;

// Default end time of events after the begin (in seconds)
Calendar.defaultEventEndTime = 3600;

Calendar.userId = null;
Calendar.userMayEditForOtherUsers = false;

Calendar.popoverData = [];
Calendar.newUserFrontendData = null;
Calendar.historyData = null;

// If specified, this is fired instead of Calendar.refreshContainer();
Calendar.onRefreshContainer = null;

// Entry browser params
Calendar.entryBrowserShowPast = false;
Calendar.entryBrowserOffset = 0;

Calendar.areaTimes = null;
Calendar.currentTimeDurationElementStep = 0;
Calendar.flagUpdateTimeDurationElements = false;

// Layout
Calendar.layoutMinimumHeight = 720;

/**
 * Initializes the display of the calsy calendar.
 * 
 * @author Peter Hamm
 * @param string The type of the displayed calendar, e.g. "overview", "user" or "user_frontend"
 * @param int calendarStartDate A php timestamp.
 * @param String filterUsers Optional, a CSV string with user ids to filter by. Default: -1 (no filter) - default: none.
 * @param String filterUserFrontend Optional, a CSV string with frontend user ids to filter by. Default: -1 (no filter) - default: none.
 * @param String filterFlags Optional, an object of filter flags, e.g. {'flag_key': {'operator': '=', 'value': 'something'}} - default: none.
 * @param String container The container element to load the calendar into.
 * @param String containerDetail The container element to load the calendar detail info.
 * @returns void
 */
Calendar.initialize = function(calendarType, calendarStartDate, filterUsers, filterUserFrontend, filterFlags, container, containerDetail) 
{
    
    Calendar.calendarType = calendarType == null ? 'user' : calendarType;
    Calendar.containerId = container == null ? '#calendarContainer' : container;
    Calendar.containerDetailId = containerDetail == null ? '#calendarDetail' : containerDetail;
    Calendar.filterUsers = filterUsers == null ? "-1" : filterUsers;
    Calendar.filterUserFrontend = filterUserFrontend == null ? "-1" : filterUserFrontend;
    Calendar.filterFlags = filterFlags;
    
    if(typeof CalendarContextMenu !== 'undefined')
    {
        
        CalendarContextMenu.init('#calendarContainer');
        
    }
    
    Calendar.showMonth(calendarStartDate);
    
    jQuery(window).resize(Calendar.onWindowResize);
    
};

/**
 * Sums up all of the time duration elements of the specified area and returns the total time.
 * 
 * @author Peter Hamm
 * @param int areaId
 * @returns The total duration in seconds
 */
Calendar.getDurationSumForArea = function(areaId) {
    
    var sum = 0;
    
    if(Calendar.areaTimes && Calendar.areaTimes[areaId])
    {
        
        for(var i = 0; i < Calendar.areaTimes[areaId].length; ++i)
        {
            
            sum += parseInt(Calendar.areaTimes[areaId][i].duration);
            
        }
    
    }
    
    return sum;
    
};

/**
 * Retrieves a unix timestamp from the specified datepicker field and time value field
 * 
 * @author Peter Hamm
 * @date 2016-02-03
 * @param String fieldIdentifierDatepicker The identifier of the field containing the date picker
 * @param String fieldIdentifierTime The identifier of the field containing the time value
 * @returns int
 */
Calendar.getTimestampFromFields = function(fieldIdentifierDatepicker, fieldIdentifierTime)
{
    
    moment.locale(Calendar.languageCode !== "undefined" ? Calendar.languageCode : languageCode);
    
    var format = Calendar.getDateFormatForMomentFromDatePicker();
    var momentObject = moment.utc(jQuery(fieldIdentifierDatepicker).val() + ' ' + jQuery(fieldIdentifierTime).val(), format);
    
    var value = momentObject.format('X');
    
    return value;
    
};

/**
 * Retrieves the date format for the moment library as it is set through the locale 
 * for the date picker library.
 * 
 * @author Peter Hamm
 * @date 2018-04-17
 * @returns String
 */
Calendar.getDateFormatForMomentFromDatePicker = function()
{
    
    var datePickerFormat = languageCode.toLowerCase() == 'en' ? jQuery().datepicker.defaults.format : jQuery().datepicker.dates[languageCode].format;
    
    if(datePickerFormat)
    {
        
        datePickerFormat = datePickerFormat.replace('dd', 'DD');
        datePickerFormat = datePickerFormat.replace('d', 'D');
        datePickerFormat = datePickerFormat.replace('mm', 'MM');
        datePickerFormat = datePickerFormat.replace('m', 'M');
        datePickerFormat = datePickerFormat.replace('yyyy', 'YYYY');
        datePickerFormat = datePickerFormat.replace('yy', 'YY');
        
    }
    
    var format = datePickerFormat + ' HH:mm';
  
    return format;
    
};

/**
 * Sets a unix timestamp for the specified datepicker field and time value field
 * 
 * @author Peter Hamm
 * @date 2016-02-03
 * @param String fieldIdentifierDatepicker The identifier of the field containing the date picker
 * @param String fieldIdentifierTime The identifier of the field containing the time value
 * @param int newTimestamp The timestamp to set the fields to.
 * @returns int
 */
Calendar.setTimestampForFields = function(fieldIdentifierDatepicker, fieldIdentifierTime, newTimestamp)
{
    
    var newDateFromTimestamp = Calendar.getNewDateObjectWithoutTimezone(newTimestamp * 1000);
    
    var year = newDateFromTimestamp.getFullYear();
    var month = newDateFromTimestamp.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    
    var day = newDateFromTimestamp.getDate();
    day = (day < 10 ? "0" : "") + day;
    
    var dateString = day + '-' + month + '-' + year;
    
    jQuery(fieldIdentifierDatepicker).datepicker('update', dateString);
    
    var hours = newDateFromTimestamp.getHours();
    hours = (hours < 10 ? "0" : "") + hours;
    
    var minutes = newDateFromTimestamp.getMinutes();
    minutes = (minutes < 10 ? "0" : "") + minutes;
    
    jQuery(fieldIdentifierTime).val(hours + ':' + minutes);
    
};

/**
 * Listens to changes of the browser window size.
 * 
 * @author Peter Hamm
 * @return void
 */
Calendar.onWindowResize = function()
{
    
    if(Calendar.currentDisplayType == 'week' || Calendar.currentDisplayType == 'day')
    {
        
        Calendar.resizeCalendarHourly();
        
    }
    else if(Calendar.currentDisplayType == 'month')
    {
        
        Calendar.resizeCalendarWeekly();
        
    }
    
};

/**
 * Retrieves the height of the calendar.
 * 
 * @author Peter Hamm
 * @param Object containerElement
 * @return int
 */
Calendar.getCalendarLayoutSize = function(containerElement)
{
    
    var newHeight = 0;
    
    try
    {
        
        var scrollerOffsetTop = parseInt(containerElement.offset().top);
        var footerHeight = parseInt(jQuery('.custom-footer').css('height'));
        var marginBottom = parseInt(jQuery(Calendar.containerId).find('.card').css('margin-bottom'));
        
        footerHeight = isNaN(footerHeight) ? 0 : footerHeight;
        marginBottom = isNaN(marginBottom) ? 24 : marginBottom;

        var newHeight = jQuery(window).height() - scrollerOffsetTop - footerHeight - marginBottom - 18;
        
        newHeight = newHeight < Calendar.layoutMinimumHeight ? Calendar.layoutMinimumHeight : newHeight;

    }
    catch(error)
    {
        
        console.log(error);
        
    }
    
    return newHeight;    
    
};

/**
 * Resizes the weekly calendar view to fit the window size.
 * 
 * @author Peter Hamm
 * @return void
 */
Calendar.resizeCalendarWeekly = function()
{
    
    var weekCount = jQuery(Calendar.containerId).find('.fc-week').length;
    
    var containerElement = jQuery(Calendar.containerId).find('.fc-day-grid-container');
    
    var newHeight = Calendar.getCalendarLayoutSize(containerElement) / weekCount;
        
    jQuery(Calendar.containerId).find('.fc-week').css('min-height', newHeight);
        
};

/**
 * Resizes the hourly calendar views to fit the window size.
 * 
 * @author Peter Hamm
 * @return void
 */
Calendar.resizeCalendarHourly = function()
{
        
    var subContainerId = '#calendar';
    
    var containerElement = jQuery(Calendar.containerId).find(subContainerId);

    var newHeight = Calendar.getCalendarLayoutSize(containerElement);
    
    containerElement.css('height', newHeight)
    
}

/**
 * Selects all users and refreshs the display.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.toggleSelectAllUsers = function()
{
    
    var selectorElements = jQuery('.user-selector input:checkbox');
    var splitUsers = Calendar.filterUsers.split(',');
        
    if(splitUsers.length == selectorElements.length)
    {
        
        // Deselect all except the user
        
        selectorElements.each(function() {

            var element = jQuery(this);
            
            var newValue = element.parent().parent().parent().parent().attr('data-id') == Calendar.userId;

            element.prop('checked', newValue);

        });
        
    }
    else
    {
        
        // Select all
        
        selectorElements.each(function() {

            var element = jQuery(this);

            element.attr('checked', 'checked');

        });
        
    }
    
    Calendar.filterByUsers(Calendar.getSelectedUsers(true));
    
};

/**
 * Filters the currently displayed calendar by the provided list of users
 * 
 * @author Peter Hamm
 * @param string userList A CSV list of user IDs
 * @returns void
 */
Calendar.filterByUsers = function(filterUsers)
{
    
    if(!filterUsers || filterUsers.length == 0)
    {
        
        filterUsers = -1;
        
    }
    
    Calendar.filterUsers = filterUsers;
    
    Calendar.refreshContainer();
    
};

/**
 * Displays the work plan with the specified identifier in the calendar.
 * 
 * @author Peter Hamm
 * @param int workPlanId 
 * @returns void
 */
Calendar.displayWorkPlan = function(workPlanId)
{
    
    if(workPlanId > 0)
    {
        
        if(!Calendar.filterFlags)
        {
            
            Calendar.filterFlags = {};
            
        }
        
        Calendar.filterFlags['work_plan_id'] = {
            
            'operator': '=',
            'value': workPlanId,
            'logic': 'or'
            
        };
        
    }
    else
    {
        
        if(Calendar.filterFlags['work_plan_id'])
        {
            
            delete Calendar.filterFlags['work_plan_id'];
            
        }
        
    }
    
    Calendar.refreshContainer();
    
};

/**
 * Filters the currently displayed calendar by the provided list of users and the specified resource identifier.
 * 
 * @author Peter Hamm
 * @param string userList A CSV list of user IDs
 * @param int resourceId Optional, an identifier of a resource. Default: None.
 * @returns void
 */
Calendar.filterByUsersAndResource = function(filterUsers, resourceId)
{
    
    Calendar.filterResource = resourceId;
    
    Calendar.filterByUsers(filterUsers);
    
};

/**
 * Refresh the main container view
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.refreshContainer = function()
{
    
    if(Calendar.onRefreshContainer)
    {
        
        return Calendar.onRefreshContainer();
        
    }
    
    if(Calendar.currentDisplayType == "day")
    {
    
        Calendar.showDay(Calendar.currentStartDate);
        
    }
    else if(Calendar.currentDisplayType == "week")
    {
    
        Calendar.showWeek(Calendar.currentStartDate);
        
    }
    else if(Calendar.currentDisplayType == "year")
    {
    
        Calendar.showYear(Calendar.currentStartDate);
        
    }
    else
    {
        
        Calendar.showMonth(Calendar.currentStartDate);
        
    }
    
    if(Calendar.currentDetailPage == "entryBrowser")
    {
        
        Calendar.showEntryBrowser();
        
    }
   
};

/**
 * If the currently displayed detail page is the user selector,
 * this will return the list of currently selected user's IDs either as array or as a CSV string.
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns {Array}
 */
Calendar.getSelectedUsers = function(asCsv)
{
    
    var returnValue = [];
    var detailContainer = jQuery(Calendar.containerDetailId);

    detailContainer.find(".user-selector").each(function() {
        
        var element = jQuery(this);
        var userId = element.attr('data-id');
        
        var isChecked = element.find(":checkbox").prop("checked");
                
        if(isChecked && userId && userId.length >= 1)
        {
            
            returnValue.push(userId);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

/**
 * Displays a manual on how to book entries.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.showManual = function() 
{
    
    Calendar.currentDetailPage = "manual";
    
    var params = {
        
        do: "manual"
        
    };
    
    jQuery(Calendar.containerDetailId).load(Calendar.controller, params, Calendar.initDetailOnLoad);
    
};

/**
 * Displays the user selector to filter the displayed entries.
 * 
 * @author Peter Hamm
 * @param bool withResourceFilter Optional, set to true to display a resource filter with the user selector. default: false
 * @returns void
 */
Calendar.showUserSelector = function(withResourceFilter) 
{
    
    Calendar.currentDetailPage = withResourceFilter ? "userSelectorWithResource" : "userSelector";
    
    var params = {
        
        do: Calendar.currentDetailPage,
        u: Calendar.filterUsers,
        r: Calendar.filterResource
        
    };
    
    jQuery(Calendar.containerDetailId).load(Calendar.controller, params, Calendar.initDetailOnLoad);
    
};

/**
 * Displays the entry browser in the detail container
 * 
 * @author Peter Hamm
 * @param bool showPast Optional, default: false. Set this to true to show past entries.
 * @param int offset Optional, an offset index. Default: 0
 * @returns void
 */
Calendar.showEntryBrowser = function(showPast, offset) 
{
    
    Calendar.currentDetailPage = "entryBrowser";
    
    if(showPast != null && showPast !== undefined) 
    {
        
        Calendar.entryBrowserShowPast = showPast;
        
    }
    
    if(offset != null && offset !== undefined) 
    {
        
        Calendar.entryBrowserOffset = offset;
        
    }
    
    var params = {
        
        do: "entryBrowser",
        sp: Calendar.entryBrowserShowPast,
        o: Calendar.entryBrowserOffset
        
    };
    
    jQuery(Calendar.containerDetailId).load(Calendar.controller, params, Calendar.initDetailOnLoad);
    
};

/**
 * Fired when content in the detail area was loaded.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initDetailOnLoad = function() 
{
        
    
    if(Calendar.currentDetailPage == 'userSelector' || Calendar.currentDetailPage == 'userSelectorWithResource')
    {
        
        Calendar.initUserSelector();
        
    }
    else if(Calendar.currentDetailPage == 'entryBrowser' || Calendar.currentDetailPage == 'manual')
    {
        
        var detailContainer = jQuery(Calendar.containerDetailId);
        
        detailContainer.find(".upcoming-event").click(function() {
            
            var element = jQuery(this);
            
            Calendar.editEvent(element.attr('data-id'));
            
        });
        
    }
    
};

/**
 * Initializes the user selector in the specified container box.
 * 
 * @author Peter Hamm
 * @param String containerIdentifier Optional, will user Calendar.containerDetailId if not specified.
 * @returns void
 */
Calendar.initUserSelector = function(containerIdentifier) 
{
    
    var detailContainer = jQuery((containerIdentifier && containerIdentifier.length > 0) ? containerIdentifier : Calendar.containerDetailId);
    
    detailContainer.find(".user-selector").click(function(event) {

        event.stopPropagation();

        var checkbox = jQuery(this).find(":checkbox");

        if(checkbox.length == 1)
        {

            checkbox.prop("checked", !checkbox.prop("checked"));

        }

    });

    // Handle clicks on the users
    
    detailContainer.find('.user-selector').click(function(event) {

        event.stopPropagation();
        
        Calendar.filterByUsers(Calendar.getSelectedUsers(true));

    });

    if(Calendar.currentDetailPage == 'userSelectorWithResource')
    {

        Perisian.initFieldClearButtons(Calendar.containerDetailId);

        // Initialize the autocomplete.

        detailContainer.find('#resource_selector').focus(function() {

            var element = jQuery(this);

            if(element.val() == Language.getLanguageVariable(10925))
            {

                element.val('');

            }

            PerisianBaseAjax.createAutocomplete({

                'element': element,

                'fieldToIdentify': 'calsy_calendar_resource_id',
                'fieldToDisplay': 'calsy_calendar_resource_title',
                'forceEntryFromList': false,
                'maxDisplayedEntries': -1,

                'url': Calendar.controller,

                'onSelect': function() {

                    Calendar.filterByUsersAndResource(Calendar.getSelectedUsers(true), PerisianBaseAjax.getAutocompleteFieldIdentifier('#resource_selector'));

                }

            });

        });

        jQuery('#resource_selector').blur(function() {

            var element = jQuery(this);

            if(element.val() == '')
            {

                element.val(Language.getLanguageVariable(10925));

            }

        });

    }
    
    
};

/**
 * Called just before a new page is shown.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.beforeShow = function()
{
        
    jQuery('.popover').finish().remove();
        
    if(Calendar.currentDisplayType == "day" || Calendar.currentDisplayType == "week")
    {
                        
        var containerElement = jQuery(Calendar.containerId);
        var scrollerElement = containerElement.find('.calendar-hourly');
        
        Calendar.lastScrollTop = scrollerElement.scrollTop();
        Calendar.lastScrollLeft = scrollerElement.scrollLeft();
        
    }
    
};

/**
 * Called just before a new page is initialized
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.beforeInit = function()
{
    
    Calendar.popoverData = [];
    
    toastr.options = {
        
        "debug": false,
        "positionClass": "toast-top-left",
        
    };
   
};

/**
 * Retrieves a list of parameters used for navigating the calendar.
 * 
 * @author Peter Hamm
 * @returns Object
 */
Calendar.getNavigationParameters = function()
{
    
    var params = {
        
        do: "getCalendar",
        d: Calendar.currentDisplayType,
        t: Calendar.currentStartDate,
        u: Calendar.filterUsers,
        p: Calendar.filterUserFrontend,
        r: Calendar.filterResource,
        f: Calendar.filterFlags,
        g: Calendar.isGlobal
        
    };
    
    return params;
    
};

/**
 * Triggers the printing of the currently displayed page.
 * 
 * @author peter Hamm
 * @returns void
 */
Calendar.printCalendar = function()
{
       
    window.print();
      
};

/**
 * Displays the year where the specified timestamp lies in.
 * 
 * @author Peter Hamm
 * @param int timestampInYear PHP timestamp
 * @returns void
 */
Calendar.showYear = function(timestampInYear)
{
    
    Calendar.beforeShow();
    
    Calendar.currentDisplayType = "year";
    Calendar.currentStartDate = timestampInYear;
    
    var params = Calendar.getNavigationParameters();
    
    jQuery(Calendar.containerId).load(Calendar.controller, params, Calendar.initOnLoad);
    
};

/**
 * Displays the month where the specified timestamp lies in.
 * 
 * @author Peter Hamm
 * @param int timestampInMonth PHP timestamp
 * @returns void
 */
Calendar.showMonth = function(timestampInMonth)
{
    
    Calendar.beforeShow();
    
    Calendar.currentDisplayType = "month";
    Calendar.currentStartDate = timestampInMonth;
    
    var params = Calendar.getNavigationParameters();
    
    jQuery(Calendar.containerId).load(Calendar.controller, params, Calendar.initOnLoad);
    
};

/**
 * Displays the week where the specified timestamp lies in.
 * 
 * @author Peter Hamm
 * @param int timestampInWeek PHP timestamp
 * @returns void
 */
Calendar.showWeek = function(timestampInWeek)
{
    
    Calendar.beforeShow();
    
    Calendar.currentDisplayType = "week";
    Calendar.currentStartDate = timestampInWeek;
    
    var params = Calendar.getNavigationParameters();
    
    jQuery(Calendar.containerId).load(Calendar.controller, params, Calendar.initOnLoad);
    
};

/**
 * Displays the day where the specified timestamp lies in.
 * 
 * @author Peter Hamm
 * @param int timestampInDay PHP timestamp
 * @returns void
 */
Calendar.showDay = function(timestampInDay)
{
    
    Calendar.beforeShow();
    
    Calendar.currentDisplayType = "day";
    Calendar.currentStartDate = timestampInDay;
    
    var params = Calendar.getNavigationParameters();
    
    jQuery(Calendar.containerId).load(Calendar.controller, params, Calendar.initOnLoad);
    
};

/**
 * Checks if the specified class has the css class "non-bookable".
 * Also displays a small toast if that's the case.
 * 
 * @author Peter Hamm
 * @param Object element
 * @returns bool
 */
Calendar.checkIfElementIsBookable = function(element)
{
    
    var elementObject = jQuery(element);
    
    if(elementObject.hasClass('non-bookable'))
    {
        
        toastr.warning(Language.getLanguageVariable(10941));
        
        return false;
        
    }
    
    return true;
    
};

/**
 * Triggered on double click or "taphold" in the hourly view.
 * 
 * @author Peter Hamm
 * @param Object e
 * @returns void
 */
Calendar.onClickHourlyElement = function(e)
{
            
    e.preventDefault();
    e.stopImmediatePropagation();
    
    if(!Calendar.checkIfElementIsBookable(e.target))
    {
        
        return;
        
    }
    
    // Creating a new event
    
    var timeAndUser = Calendar.getClickedHourlyTimeAndUserFromEvent(e);
    var targettedTime = timeAndUser.time;
    
    if(targettedTime > 0)
    {
        
        Calendar.handleClickHourlyElement(targettedTime);
        
    }
    
};

Calendar.getClickedHourlyTimeAndUserFromEvent = function(event, considerDst)
{
    
    var clickedElement = jQuery(event.target).parent();
    var clickX = event.type == "taphold" ? mousePageX : event.pageX;

    var targettedDayElement = null;
    
    var targettedTime = 0;
    var targettedUser = Calendar.userId;
    
    // Find the time
    {

        jQuery('.fc-day-header-element').each(function() {

            var element = jQuery(this);

            var left = element.offset().left - parseInt(element.css('padding-left'));
            var right = left + element.width() + parseInt(element.css('padding-right'));

            if(clickX >= left && clickX <= right)
            {

                targettedDayElement = element;

                return;

            }

        });

        if(targettedDayElement != null)
        {

            targettedTime = parseInt(targettedDayElement.attr('data-time')) + parseInt(clickedElement.attr('data-unix-time'));

            if(considerDst)
            {

                var dateDummy = new Date(targettedTime * 1000);

                var offset = (!dateDummy.isInDST() ? 3600 : 0);

                targettedTime += offset;

            }

        }
    
    }
    
    // Find the user
    if(Calendar.userMayEditForOtherUsers && Calendar.filterUsers.length > 1)
    {
                
        jQuery('.fc-user-header').each(function() {

            var element = jQuery(this);

            var left = element.offset().left - parseInt(element.css('padding-left'));
            var right = left + element.width() + parseInt(element.css('padding-right'));

            if(clickX >= left && clickX <= right)
            {

                targettedUser = jQuery(element).attr('data-user-id');

                return;

            }

        });
        
    }
    
    var returnValue = {
        
        'user': targettedUser,
        'time': targettedTime
        
    };
        
    return returnValue;
    
};

/**
 * This is gonna get fired after clicking on an hourly element.
 * 
 * @author Peter Hamm
 * @param int targettedTime
 * @param int userId Optional
 * @param int areaId Optional
 * @returns void
 */
Calendar.handleClickHourlyElement = function(targettedTime, userId, areaId)
{
            
    targettedTime = parseInt(targettedTime) + Calendar.getDstOffsetForTimestamp();
    
    Calendar.createNewEvent(targettedTime, userId, null, null, null, 'day', areaId);
    
};

/**
 * Triggered on double click or "taphold" on a day
 * 
 * @author Peter Hamm
 * @param Object e
 * @returns void
 */
Calendar.onClickDay = function(e) 
{
    
    if(!Calendar.checkIfElementIsBookable(e.target))
    {
        
        return;
        
    }

    // Creating a new event
    var clickedElement = jQuery(e.target);
    
    // At which hour shall the event begin?
    var eventTime = (Calendar.defaultEventStartTimeHours * 60 * 60) + (Calendar.defaultEventStartTimeMinutes * 60);
    
    var elementTime = parseInt(clickedElement.attr('data-time'));
    
    if(elementTime == 0 || isNaN(elementTime))
    {
        
        elementTime = parseInt(clickedElement.closest('td').attr('data-time'));
                
    }
    
    Calendar.handleClickDay(elementTime + eventTime);

};

/**
 * This is gonna get fired after clicking on a day.
 * 
 * @author Peter Hamm
 * @param int targettedTime
 * @returns void
 */
Calendar.handleClickDay = function(targettedTime)
{
       
    Calendar.createNewEvent(targettedTime);
    
};

/**
 * Handles clicks directly on a day's number,
 * jumps to the view for the specified day
 * 
 * @author Peter Hamm
 * @param Object e
 * @returns void
 */
Calendar.onClickDayNumber = function() 
{
    
    var clickedElement = jQuery(this);
    
    var elementTime = parseInt(clickedElement.closest('td').attr('data-time'));
    
    if(elementTime == 0 || isNaN(elementTime))
    {
        
        elementTime = parseInt(clickedElement.closest('th').attr('data-time'));
        
    }
    
    Calendar.showDay(elementTime);
    
};

Calendar.initEventClicks = function() 
{
    
    jQuery('.fc-event').click(function() {

        // Viewing / editing an existing event

        var clickedElement = jQuery(this);

        var elementIdentifier = clickedElement.attr('data-id');

        if(clickedElement.hasClass('fc-process'))
        {

            Calendar.editProcess(elementIdentifier);
            
        }
        else if(clickedElement.hasClass('fc-holiday') || clickedElement.hasClass('fc-blocked'))
        {

            Calendar.editHoliday(elementIdentifier);

        }
        else
        {
            
            if(clickedElement.attr('data-is-bookable-time') == '1')
            {
                
                var dateBegin = clickedElement.attr('data-begin');
                var dateEnd = clickedElement.attr('data-end');
                
                Calendar.infoBookableTime(dateBegin, dateEnd);
                
                return;
                
            }

            if(Calendar.editEntryDirectly)
            {
                
                Calendar.editEvent(elementIdentifier);
                
            }
            else
            {
                
                Calendar.infoEvent(elementIdentifier);
                
            }

        }

    });
    
};

/**
 * Initializes clicks on hourly elements.
 * 
 * @author Peter Hamm 
 * @returns void
 */
Calendar.initHourlyDisplayClicks = function()
{
    
    jQuery('.fc-widget-content-clickable').doubletap(Calendar.onClickHourlyElement);

    if(!Perisian.isMobileDevice())
    {

        jQuery('.fc-widget-content-clickable').longclick(300, Calendar.onClickHourlyElement);

    }

    if(Calendar.currentDisplayType == "week")
    {

        jQuery('.fc-weekly-day-header').click(Calendar.onClickDayNumber);

    }
    
};

/**
 * Gets called automatically after the calendar container's content was loaded.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initOnLoad = function() 
{
    
    Calendar.beforeInit();
    
    var calendarElement = jQuery(this);
    
    calendarElement.find('#calendar-print').click(Calendar.printCalendar);
    calendarElement.find('#calendar-entry-new').click(Calendar.createNewEventInScope);
    calendarElement.find('#calendar-today').click(Calendar.onClickToday);
    calendarElement.find('#calendar-prev').click(Calendar.onClickPrevious);
    calendarElement.find('#calendar-next').click(Calendar.onClickNext);
    
    calendarElement.find('#calendar-display-year').click(function() { Calendar.showYear(Calendar.currentStartDate); });
    calendarElement.find('#calendar-display-month').click(function() { Calendar.showMonth(Calendar.currentStartDate); });
    calendarElement.find('#calendar-display-week').click(function() { Calendar.showWeek(Calendar.currentStartDate); });
    calendarElement.find('#calendar-display-day').click(function() { Calendar.showDay(Calendar.currentStartDate); });
    
    if(Calendar.currentDisplayType == "day" || Calendar.currentDisplayType == "week")
    {
        
        Calendar.initHourlyDisplay();
        Calendar.initHourlyDisplayClicks();
        Calendar.initEventClicks();
        
    }
    else if(Calendar.currentDisplayType == "month")
    {
        
        Calendar.initWeeklyDisplay();
        
        jQuery('.fc-day').doubletap(Calendar.onClickDay);
        jQuery('.button-add-entry-day').click(Calendar.onClickDay);
        jQuery('.button-day-number').click(Calendar.onClickDayNumber);
        
        if(!Perisian.isMobileDevice())
        {
            
            jQuery('.fc-day').longclick(300, Calendar.onClickDay);
        
        }
        
        Calendar.initEventClicks();
        
    }
    else if(Calendar.currentDisplayType == "year")
    {
        
        jQuery('.calendar-year-day').click(Calendar.onClickDayNumber);
        
        jQuery('.calendar-year-month-header').click(function() {
            
            // Jump to the selected month
            
           var clickedElement = jQuery(this);
           
           Calendar.showMonth(clickedElement.attr('data-time'));
            
        });
        
    }
    
    jQuery('.popover').finish().remove();
    
    Calendar.initTooltips();
    
};

Calendar.handleHistoryBack = function()
{
        
    var sendData = {

        'e': Calendar.historyData.calendarEntryId,
        'p': Calendar.historyData.userFrontendId,
        't': Calendar.historyData.timeBegin,
        'te': Calendar.historyData.timeEnd,
        'calendarType': Calendar.calendarType,
        'do': 'edit',
        'step': 1
        
    };
    
    this.editEntry(sendData, Calendar.onEditEvent);
    
};

Calendar.getDstOffsetForTimestamp = function(timestamp)
{
    
    // Is the time in daylight saving time (summer)? Add an offset of one hour!
            
    var dateDummy = new Date(timestamp * 1000);
    var offset = (!dateDummy.isInDST() ? 3600 : 0);
    
    return offset;
    
};

/**
 * Retrieves two Date objects, indicating when the next DST changes will happen.
 * Attention: If the timezone doesn't have DST, the Date objects will be null.
 * 
 * @author Peter Hamm
 * @returns Object
 */
Calendar.getDstSwitchDatesUpcoming = function()
{
    
    var year = new Date().getYear();
    
    if(year < 1000)
    {
        
        year += 1900;
        
    }
    
    var data = {
        
        'dst': null,
        'normal': null
        
    };

    var firstSwitch = 0;
    var secondSwitch = 0;
    var lastOffset = 99;

    // Loop through every month of the current year
    for(i = 0; i < 12; i++)
    {
        
        // Fetch the timezone value for the month
        var newDate = new Date(Date.UTC(year, i, 0, 0, 0, 0, 0));
        var timezoneOffset = -1 * newDate.getTimezoneOffset() / 60;

        // Capture when a timzezone change occurs
        if(timezoneOffset > lastOffset)
        {
            
            firstSwitch = i - 1;
            
        }
        else if (timezoneOffset < lastOffset)
        {
            
            secondSwitch = i - 1;
            
        }

        lastOffset = timezoneOffset;
        
    }

    var secondDstDate = Calendar.getNextDstSwitchDate(year, secondSwitch);
    var firstDstDate = Calendar.getNextDstSwitchDate(year, firstSwitch);

    if(firstDstDate == null && secondDstDate == null)
    {
        
        // No DST in this timezone
        return data;
        
    }
    else
    {
                       
        data.dst = firstDstDate.getMonth() < 7 ? firstDstDate : secondDstDate;
        data.normal = firstDstDate.getMonth() < 7 ? secondDstDate : secondDstDate;
        
    }
    
    return data;
    
};

/**
 * Retrieves the closest future DST switch date for the year and month specified.
 * 
 * @author Peter Hamm
 * @param int year
 * @param int month
 * @returns Date
 */
Calendar.getNextDstSwitchDate = function(year, month)
{
    
    // Set the starting date
    var baseDate = new Date(Date.UTC(year, month, 0, 0, 0, 0, 0));
    var changeMinute = -1;
    var baseOffset = -1 * baseDate.getTimezoneOffset() / 60;

    // Loop to find the exact day a timezone adjust occurs
    for (day = 0; day < 50; ++day)
    {
        
        var tmpDate = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
        var tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

        // Check if the timezone changed from one day to the next
        if(tmpOffset != baseOffset)
        {
            
            var minutes = 0;
            changeDay = day;

            // Back-up one day and grap the offset
            tmpDate = new Date(Date.UTC(year, month, day-1, 0, 0, 0, 0));
            tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

            while(changeMinute == -1)
            {
                
                // Count the minutes until a timezone change occurs
                
                tmpDate = new Date(Date.UTC(year, month, day-1, 0, minutes, 0, 0));
                tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;

                // Determine the exact minute a timezone change occurs
                if(tmpOffset != baseOffset)
                {
                    
                    // Back-up a minute to get the date/time justbefore a timezone change occurs
                    
                    tmpOffset = new Date(Date.UTC(year, month, day - 1, 0, minutes-1, 0, 0));
                    changeMinute = minutes;
                    
                    break;
                    
                }
                else
                {
                    
                    ++minutes;
                    
                }
                
            }

            // Capture the time stamp
            tmpDate = new Date(Date.UTC(year, month, day - 1, 0, minutes - 1, 0, 0));
            
            return tmpDate;
            
        }
        
    }
    
};

/**
 * Initializes the weekly view.
 * 
 * @author Peter Hamm
 * @return void
 * @returns void
 */
Calendar.initWeeklyDisplay = function()
{
    
    Calendar.resizeCalendarWeekly();
    
};

/**
 * Initializes the display after the calendar 
 * has been loaded in e.g. the daily or hourly view.
 * 
 * @author Peter Hamm
 * @return void
 * @returns void
 */
Calendar.initHourlyDisplay = function()
{
    
    Calendar.resizeCalendarHourly();
    
    // Scroll to the last scroll position
    var containerElement = jQuery(Calendar.containerId);
    var scrollerElement = containerElement.find('.calendar-hourly');
    
    scrollerElement.scrollTop(Calendar.getLastScrollTop());
    scrollerElement.scrollLeft(Calendar.getLastScrollLeft());
    
    jQuery(Calendar.containerId).find(".fc-event-container").find(".fc-event").each(function() {
        
        var element = jQuery(this);
        
        Calendar.alignHourlyEventElement(element);
        
    });
    
    // Handle scrolling
    {
        
        // Find out how many entities (e.g. users) are being displayed
        {
            
            var entityList = [];
            var entityListCountable = [];
            
            var entityCount = 0;

            jQuery('#multi-user-header th').each(function() {

                var element = jQuery(this);
                var userId = element.attr('data-user-id');

                entityList.push(userId);

            });
                        
            for(var i = 0; i < entityList.length; ++i)
            {
                
                if(entityList[i] != undefined && entityListCountable.indexOf(entityList[i]) === -1)
                {
                    
                    ++entityCount;
                    
                    entityListCountable.push(entityList[i]);
                    
                }
                
            }
                    
        }
        
        scrollerElement.scroll(function(){

            var scroller = jQuery(this);
            var header = jQuery('#hourly-header');

            var offsetTop = scroller.offset().top;

            var margin = entityCount > 1 ? 154 : 200;

            header.offset({ 

                top: offsetTop - margin

            });

        });

        if(scrollerElement)
        {

            // Make the times on the left side scroll.
            
            scrollerElement.scroll(function(){
                
                var scroller = jQuery(this);
                
                var times = scroller.find('.fc-axis');

                var offsetLeft = scroller.offset().left;
                var scrollLeft = scroller.scrollLeft();
                
                times.offset({ 

                    left: scrollLeft > 0 ? offsetLeft : '0px'

                });
                                
                if(scrollLeft > 0 && !times.hasClass('fc-moving'))
                {
                    
                    times.addClass('fc-moving');
                    
                }
                else if(scrollLeft <= 0)
                {
                    
                    times.removeClass('fc-moving');
                    times.css('left', '0px');
                    
                }

            });

        }
    
    }
    
};

/**
 * Retrieves the last scrollLeft position of the hourly views.
 * If none is set yet, it is set 0.
 * 
 * @author Peter Hamm
 * @returns float
 */
Calendar.getLastScrollLeft = function() 
{
    
    if(Calendar.lastScrollLeft == -1)
    {
        
        Calendar.lastScrollLeft = 0;
        
    }
    
    return Calendar.lastScrollLeft;
    
};

/**
 * Retrieves the last scrollTop position of the hourly views.
 * If none is set yet, it is set to the offset corresponding 0700 hours.
 * 
 * @author Peter Hamm
 * @returns float
 */
Calendar.getLastScrollTop = function() 
{
    
    if(Calendar.lastScrollTop == -1)
    {
        
        Calendar.lastScrollTop = Calendar.getHourPosition('700').offset;
        
    }
    
    return Calendar.lastScrollTop;
    
};

Calendar.getNewDateObjectWithoutTimezone = function(dateParam)
{
    
    var date = new Date(dateParam);
    
    var timezoneOffsetInSeconds = new Date().getTimezoneOffset() * 60;
    
    date.setSeconds(date.getSeconds() + timezoneOffsetInSeconds);
    
    return date;
    
};

/**
 * Aligns an event element in the views for daily and weekly views.
 * 
 * @author Peter Hamm
 * @param Object element
 * @returns void
 */
Calendar.alignHourlyEventElement = function(element)
{
    
    var dateBegin = Calendar.getNewDateObjectWithoutTimezone(element.attr('data-begin') * 1000);
    var dateEnd = Calendar.getNewDateObjectWithoutTimezone(element.attr('data-end') * 1000);
    
    var startsToday = element.attr('data-starts-today') == 1;
    var endsToday = element.attr('data-ends-today') == 1;
    
    var offsetTop = 0;
    var offsetBottom = 0;
    
    var dateDummy = new Date();
    
    var timezoneOffsetInMinutesDST = (dateDummy.isInDST() ? 60 : 0);
    var timezoneOffsetInMinutes = -60 - timezoneOffsetInMinutesDST;
            
    var hourPositionDataBegin = {
      
        'error': true
        
    }; 
    
    var hourPositionDataEnd = {
      
        'error': true
        
    };
            
    // Calculate the begin/top offset
    {
        
        if(startsToday)
        {

            // The event starts on that day

            dateBegin.setMinutes(dateBegin.getMinutes() - timezoneOffsetInMinutes);

            var beginMinutes = dateBegin.getMinutes();

            beginMinutes = beginMinutes < 10 ? "0" + beginMinutes : beginMinutes;

            var startTime = '' + dateBegin.getHours() + (beginMinutes);

            hourPositionDataBegin = Calendar.getHourPosition(startTime);
            offsetTop = hourPositionDataBegin.offset;

        }
        else
        {

            // The event does not start on that day (e.g.: before)

            hourPositionDataBegin = Calendar.getHourPosition("000");
            offsetTop = hourPositionDataBegin.offset;

        }
        
    }
    
    // Calculate the end/bottom offset
    {
        
        if(!endsToday)
        {

            // The event does not end today (e.g.: after)

            hourPositionDataEnd = Calendar.getHourPosition("2359");
            offsetBottom = -1 * (hourPositionDataEnd.offset);

        }
        else
        {

            // The event ends today

            dateEnd.setMinutes(dateEnd.getMinutes() - timezoneOffsetInMinutes);

            var endMinutes = dateEnd.getMinutes();

            endMinutes = endMinutes < 10 ? "0" + endMinutes : endMinutes;

            var endTime = '' + dateEnd.getHours() + (endMinutes);

            hourPositionDataEnd = Calendar.getHourPosition(endTime);
            offsetBottom = -1 * (hourPositionDataEnd.offset);// - 20;

        }
        
    }
    
    // Correct the values
    {
                
        if(hourPositionDataBegin.error && hourPositionDataEnd.error)
        {
            
            element.css({
                
                'display': 'none'
                
            });
            
        }
        else
        {
            
            if(isNaN(offsetBottom))
            {

                // The end time must be outside the defined displayed times.

                offsetBottom = -1 * (Calendar.getHourPosition("2359").offset);

            }

            if(hourPositionDataBegin.error)
            {

                offsetTop = -20;

            }
            
        }
                
    }
        
    element.css({

        top: offsetTop + 'px',
        bottom: offsetBottom + 'px'

    });
    
};

/**
 * Retrieves the position of the specified hour in the displayed calendar,
 * as well as the scroller element for the calendar.
 * 
 * @author Peter Hamm
 * @param int hour An hour in the format of "100", "300", "1300" or "2300"
 * @returns Object
 */
Calendar.getHourPosition = function(hour)
{
    
    var minutePercentage = hour.substr(-2) / 60;
    var closestHour = Math.floor(hour / 100) * 100;
    
    var containerElement = jQuery(Calendar.containerId);
    var scrollerElement = containerElement.find('.fc-scroller');
    var timeElement = scrollerElement.find('[data-time=' + closestHour + ']');
    
    var error = {

        'error': true

    };
        
    if(typeof(timeElement) === 'undefined' || timeElement.length == 0)
    {
        
        return error;
        
    }

    var timePosition = timeElement.offset().top - scrollerElement.offset().top + scrollerElement.scrollTop();
    
    {
                
        var nextHourElement = timeElement;
        
        if(minutePercentage > 0)
        {
            
            while(nextHourElement != null)
            {

                if(nextHourElement[0].nextElementSibling == null)
                {

                    break;

                }

                nextHourElement = jQuery(nextHourElement[0].nextElementSibling);

                if(nextHourElement.hasClass('full-hour'))
                {

                    break;

                }

            }
        
        }
        
        if(nextHourElement.css('display') == 'none')
        {
                        
            return error;
            
        }
        
        if(hour == '000')
        {
            
            timePosition = 0;
            
        }
        else
        {
            
            var hourHeight = parseInt(nextHourElement.offset().top) - parseInt(timeElement.offset().top);

            if(hour == '2359')
            {

                minutePercentage = 1;

            }

            timePosition = Math.round(timePosition + minutePercentage * hourHeight);

        }
        
    }
    
    var margin = parseFloat(jQuery('.fc-time-grid').css('margin-top'));
    
    timePosition -= margin;
    timePosition += (timePosition == 0 && Calendar.currentDisplayType == 'week') ? 2 : 0;
        
    var returnValue = {
        
        'error': false,
        'offset': timePosition, 
        'scrollerElement': scrollerElement
    
    };
    
    return returnValue;
    
};

/**
 * Handles clicks on the "today" button of the calendar - navigates back to "today".
 * 
 * @author Peter Hamm
 * @date 2015-01-18
 * @returns void
 */
Calendar.onClickToday = function()
{
    
    Calendar.currentStartDate = new Date().getTime() / 1000;
    
    Calendar.navigate(0);
    
};

/**
 * Handles clicks on the "previous" button of the calendar.
 * 
 * @author Peter Hamm
 * @date 2015-01-18
 * @returns void
 */
Calendar.onClickPrevious = function()
{
    
    Calendar.navigate(-1);
    
};

/**
 * Handles clicks on the "next" button of the calendar.
 * 
 * @author Peter Hamm
 * @date 2015-01-18
 * @returns void
 */
Calendar.onClickNext = function()
{
    
    Calendar.navigate(1);
    
};

/**
 * Navigates the calendar page int the specified direction.
 * 
 * @author Peter Hamm
 * @param int direction Should be '-1' or '1'
 * @returns void
 */
Calendar.navigate = function(direction)
{
    
    var currentDateObject = new Date(Calendar.currentStartDate * 1000);
    
    if(Calendar.currentDisplayType == 'year')
    {
                
        currentDateObject.setFullYear(currentDateObject.getFullYear() + direction * 1);
        Calendar.showYear(currentDateObject.getTime() / 1000);
        
    }
    else if(Calendar.currentDisplayType == 'month')
    {
        
        currentDateObject.setMonth(currentDateObject.getMonth() + direction * 1);
        Calendar.showMonth(currentDateObject.getTime() / 1000);
        
    }
    else if(Calendar.currentDisplayType == 'week')
    {
        
        currentDateObject.setDate(currentDateObject.getDate() + direction * 7);
        Calendar.showWeek(currentDateObject.getTime() / 1000);
        
    }
    else if(Calendar.currentDisplayType == 'day')
    {
        
        currentDateObject.setDate(currentDateObject.getDate() + direction * 1);
        currentDateObject.setHours(0);
        
        if(Calendar.displayableDaysDaily && Calendar.displayableDaysDaily.length > 0)
        {
                   
            var currentDay = parseInt(currentDateObject.getDay());
            currentDay = currentDay == 0 ? 6 : currentDay - 1;
            
            
            // Check if the targetted day should be displayed or skipped
                        
            while(Calendar.displayableDaysDaily.indexOf(currentDay) == -1)
            {
                                                
                currentDateObject.setDate(currentDateObject.getDate() + direction * 1);               
                
                currentDay = parseInt(currentDateObject.getDay());
                currentDay = currentDay == 0 ? 6 : currentDay - 1;
                                                 
            }
                        
        }
        
        Calendar.showDay(currentDateObject.getTime() / 1000);
        
    }
    
};

/**
 * Retrieves whether the browser is in daylight saving time or not.
 * 
 * @author Peter Hamm
 * @returns bool
 */
Calendar.isBrowserInDaylightSavingTime = function()
{
        
    var dateDummy = new Date();
    
    return dateDummy.isInDST();    
    
};

/**
 * Retrieves the timezone offset of this client in seconds.
 * 
 * @author Peter Hamm
 * @returns int
 */
Calendar.getTimezoneOffsetInSeconds = function()
{
        
    return new Date().getTimezoneOffset() * 60 + (Calendar.isBrowserInDaylightSavingTime() ? 3600 : 0)
    
};

Calendar.getFirstMinuteOfDayAsTimestamp = function(timestamp)
{
    
    var dateTime = new Date(timestamp * 1000);
    
    dateTime.setHours(0);
    dateTime.setMinutes(0);
    dateTime.setSeconds(0);
    
    var returnValue = dateTime.getTime() / 1000
    
    return returnValue;
    
};

/**
 * Retrieves whether the day day of the specified timestamp is the day
 * where the clocks get switched from DST to winter time or net. (e.g. this should be true for the last Sunday of October in Germany).
 * 
 * @author Peter Hamm
 * @param int timestamp
 * @return bool
 */
Calendar.isDaySwitchToWinterTime = function(timestamp)
{
    
    var firstMinuteOfDay = Calendar.getFirstMinuteOfDayAsTimestamp(timestamp);
    var noonOfDay = firstMinuteOfDay + 12 * 3600;
    
    var dateFirstMinuteOfDay = new Date(firstMinuteOfDay * 1000);
    var dateNoonOfDay = new Date(noonOfDay * 1000);

    if(dateFirstMinuteOfDay.isInDST() && !dateNoonOfDay.isInDST())
    {

        return true;

    }

    return false;
    
};

/**
 * Retrieves whether the day day of the specified timestamp is the day
 * where the clocks get switched from winter to dummer (/daylight saving) time or net. (e.g. this should be true for the last Sunday of March in Germany).
 * 
 * @author Peter Hamm
 * @param int timestamp
 * @return bool
 */
Calendar.isDaySwitchToSummerTime = function(timestamp)
{
    
    var firstMinuteOfDay = Calendar.getFirstMinuteOfDayAsTimestamp(timestamp);
    var noonOfDay = firstMinuteOfDay + 12 * 3600;
    
    var dateFirstMinuteOfDay = new Date(firstMinuteOfDay * 1000);
    var dateNoonOfDay = new Date(noonOfDay * 1000);

    if(!dateFirstMinuteOfDay.isInDST() && dateNoonOfDay.isInDST())
    {

        return true;

    }

    return false;
    
};

/**
 * Initializes mouseover tooltips for events
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initTooltips = function()
{
    
    jQuery('.fc-event').not('.fc-holiday').not('.fc-blocked').mouseenter(function() {

        var element = jQuery(this);
        var entryId = element.attr('data-id');
                        
        if(element.attr('data-id-secondary').length > 0 && element.attr('data-id-secondary') != entryId)
        {
            
            entryId = element.attr('data-id-secondary');
            
        }
        
        var isBookableTime = element.attr('data-is-bookable-time') == '1';
        var dataAlreadyLoaded = isBookableTime ? true : typeof Calendar.popoverData[entryId] !== "undefined";
        
        if(!dataAlreadyLoaded)
        {
            
            // Load the details.
            
            var params = {

                'do': 'getJson',
                'source': 'entry',
                'e': entryId

            };

            jQuery.post(Calendar.controller, params, function(data) {

                var decodedData = jQuery.parseJSON(data);
                
                Calendar.popoverData[params.e] = decodedData;
                
                if(element && element.is(":hover"))
                {
                    
                    Calendar.showPopoverForEntry(element, Calendar.popoverData[params.e]);
                    
                }

            });
            
        }
        else
        {
            
            var displayData = {};
            
            if(isBookableTime)
            {
                                
                var timeBegin = element.attr('data-begin-time');
                var timeEnd = element.attr('data-end-time');
                                
                var title = timeBegin + ' ' + Language.getLanguageVariable('10688') + ' ' + timeEnd;
                var content = '<span class="alert alert-info alert-inline">' + Language.getLanguageVariable('p57ab295ddf9ec') + '</span>';
                
                displayData = {
                    
                    'title': title,
                    'content': content
                    
                };
                
                
            }
            else
            {
                
                displayData = Calendar.popoverData[entryId];
                
            }
            
            // Already loaded the details.
            Calendar.showPopoverForEntry(element, displayData);
            
        }

    });
    
};

/**
 * Shows the popover element for the specified calendar element and entry Object
 * 
 * @author Peter Hamm
 * @param Object element
 * @param Object entry
 * @returns void
 */
Calendar.showPopoverForEntry = function(element, entry)
{
    
    element.popover({

        container: "body",
        placement: 'auto',
        html: 'true',
        title: entry.title,
        content: entry.content

    }).popover('show');
    
    element.off("mouseleave");
    
    element.mouseleave(function() {

        jQuery(this).popover('hide');

    });
    
};

/**
 * Toggles the display of the 'plus' button of the calendar.
 * 
 * @author Peter Hamm
 * @param bool showButton
 * @returns void
 */
Calendar.togglePlusButton = function(showButton)
{
    
    jQuery('#calendar-entry-new').css('display', showButton ? 'block' : 'none');
    
};

/**
 * Initializes the form to edit a calendar entry.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.initEditEntry = function()
{
            
    var container = Calendar.initDatePicker();

    container.find('#calsy_calendar_date_range_begin_date, #calsy_calendar_date_range_begin_time').blur(function() {

        setTimeout(function() {

            var timestampBegin = parseInt(Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time"));

            var dstOffset = Calendar.isBrowserInDaylightSavingTime() ? Calendar.getDstOffsetForTimestamp() : 0;

            var areaId = Calendar.getSelectedAreaId();
            var timeDistanceElementTime = (Calendar.areaTimes[areaId] && Calendar.areaTimes[areaId].length > 0) ? Calendar.areaTimes[areaId][0].duration : Calendar.defaultEventEndTime;

            var timestampEnd = timestampBegin + parseInt(timeDistanceElementTime);
            timestampEnd += dstOffset;

            Calendar.setTimestampForFields("#calsy_calendar_date_range_end_date", "#calsy_calendar_date_range_end_time", timestampEnd);

        }, 100);

    });
    
};