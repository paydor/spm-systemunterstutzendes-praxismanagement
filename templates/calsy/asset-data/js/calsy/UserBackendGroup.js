/**
 * Class to handle backend user groups
 *
 * @author Peter Hamm
 * @date 2017-04-10
 */
var UserBackendGroup = jQuery.extend(true, {}, PerisianBaseAjax);

UserBackendGroup.controller = baseUrl + "/user_backend_group/overview/";

UserBackendGroup.currentSortOrder = 'ASC';
UserBackendGroup.currentSorting = 'calsy_user_backend_group_name';
UserBackendGroup.groupList = null;
UserBackendGroup.hourList = null;
    
UserBackendGroup.createNewUserBackendGroup = function()
{
    
    UserBackendGroup.editUserBackendGroup();
    
};

/**
 * Retrieves a list of all groups
 * 
 * @author Peter Hamm
 * @returns array
 */
UserBackendGroup.getUserBackendGroupList = function()
{
    
    if(UserBackendGroup.groupList != null)
    {
        
        return UserBackendGroup.groupList;
        
    }
    
    var sendData = {

        'do': 'getGroupList'
        
    };

    jQuery.ajax({
        
        url: UserBackendGroup.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            UserBackendGroup.groupList = decodedData.list;
                        
        }
        
    });
    
    return UserBackendGroup.groupList;
    
};

UserBackendGroup.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

UserBackendGroup.editUserBackendGroup = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

/**
 * Should be fired when the edit form for a group is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
UserBackendGroup.initEditGroup = function(timeElements)
{
    
    jQuery('#calsy_user_backend_group_name').focus();
    
};

/**
 * Gets the selected view rights for the currently edited entry
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
UserBackendGroup.getSelectedViewRights = function(asCsv)
{
    
    return UserBackendGroup.getSelectedUsersFromSelector('#group-view-rights-selector', asCsv);
    
};

/**
 * Gets the selected users for the currently edited entry
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
UserBackendGroup.getSelectedUsers = function(asCsv)
{
    
    return UserBackendGroup.getSelectedUsersFromSelector('#group-user-selector', asCsv);
    
};

/**
 * Gets the selected users for the currently edited entry
 * 
 * @author Peter Hamm
 * @param String selectorId The identifier of the user selector DOM element.
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
UserBackendGroup.getSelectedUsersFromSelector = function(selectorId, asCsv)
{
    
    var returnValue = [];
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var selectorElement = elementContainer.find(selectorId);
    
    selectorElement.find("li.user-selector").each(function() {
        
        var element = jQuery(this);
        var userId = element.attr('data-id');
        
        var isChecked = element.find(":checkbox").prop("checked");
        
        if(isChecked)
        {
            
            returnValue.push(userId);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

UserBackendGroup.saveUserBackendGroup = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        "users": UserBackendGroup.getSelectedUsers(),
        "viewRights": UserBackendGroup.getSelectedViewRights()
        
    }, "#" + Perisian.containerIdentifier, "calsy_user_backend_group_"); 
    
    if(!UserBackendGroup.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            UserBackendGroup.cancel(); 
            UserBackendGroup.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

UserBackendGroup.deleteUserBackendGroup = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        UserBackendGroup.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

UserBackendGroup.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        UserBackendGroup.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_user_backend_group_name))
    {

        UserBackendGroup.highlightInputTitleError('calsy_user_backend_group_name', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
UserBackendGroup.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
UserBackendGroup.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
UserBackendGroup.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};
