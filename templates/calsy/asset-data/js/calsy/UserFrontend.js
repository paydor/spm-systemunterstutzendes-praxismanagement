/**
 * Class to handle frontend users
 *
 * @author Peter Hamm
 * @date 2016-02-03
 */
var UserFrontend = jQuery.extend(true, {}, PerisianBaseAjax);

UserFrontend.controller = baseUrl + "/user_frontend/overview/";
UserFrontend.controllerImport = baseUrl + "/ajax/data_import/";

UserFrontend.currentSortOrder = 'ASC';
UserFrontend.currentSorting = 'calsy_user_frontend_name_last';
    
UserFrontend.createNewUserFrontend = function()
{
    
    UserFrontend.editUserFrontend();
    
};

UserFrontend.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

UserFrontend.initCustomFields = function()
{
    
    jQuery('.user-custom-input-button-add').off('click').on('click', function() {
        
        UserFrontend.addCustomField();
        
    });
    
    jQuery('.user-custom-input-button-remove').off('click').on('click', function() {
        
        var element = jQuery(this);
        
        UserFrontend.removeCustomField(element.attr('data-id'));
        
    });
    
};

UserFrontend.showQuickLogSettings = function(userFrontendId)
{
    
    var sendData = {

        'do': 'quickLogSettings',
        'u': userFrontendId
        
    };

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');

    elementContainer.load(this.controller, sendData, function () {

        Perisian.ajaxBox('show');

    });
    
};

UserFrontend.saveQuickLogSettings = function()
{
    
    var rights = {};
    
    jQuery('.calsy_user_frontend_quick_log_right').each(function() {
        
        var element = jQuery(this);
        
        var userSettingKey = element.attr('data-quick-log-right-user-setting-key');
        var value = element.find('#calsy_quick_log_right_' + userSettingKey).is(':checked') ? '1' : '0';
        
        rights[userSettingKey] = value;
        
    });
    
    var sendData = {
        
        'do': 'saveQuickLogSettings',
        'u': jQuery('#calsy_user_frontend_quick_log_right_user_frontend_id').val(),
        'rights': rights
        
    };
    
    jQuery.post(this.controllder, sendData, function(data) {
        
        var result = JSON.parse(data);
        
        if(result.success)
        {
            
            toastr.success(result.message);
            
            PerisianBaseAjax.cancel();
            
            
        }
        else
        {
            
            toastr.error(result.message);
            
        }
        
    });
    
};

UserFrontend.addCustomField = function() 
{
    
    var newId = 'c_' + Perisian.generateUniqueIdentifier();
    
    var newElement = jQuery(jQuery('.user-custom-input-field')[0]).clone();
    
    newElement.find('input').val('');
    newElement.attr('id', 'custom_field_' + newId).attr('data-id', newId);
    newElement.find('.user-custom-input-button-remove').attr('data-id', newId);
    
    jQuery('.user-custom-input').append(newElement);
    
    UserFrontend.initCustomFields();
    
};

UserFrontend.removeCustomField = function(fieldId) 
{
    
    if(jQuery('.user-custom-input-field').length > 1)
    {
        
        jQuery('#custom_field_' + fieldId).remove();
        
    }
    else
    {
        
        jQuery('#custom_field_' + fieldId + ' input').val('');
        
    }
    
};

UserFrontend.getCustomFieldValues = function()
{
    
    var data = [];
    
    jQuery('.user-custom-input-field').each(function() {
        
        var element = jQuery(this);
        
        var entry = {
            
            'id': element.attr('data-id'),
            'label': element.find('.user-custom-input-field-label').val(),
            'value': element.find('.user-custom-input-field-value').val()
            
        };
        
        data.push(entry);
        
    });
    
    return data;
    
};

/**
 * Opens the form to edit/create a frontend user as a modal.
 * 
 * @author Peter Hamm
 * @param int editId Optional. Leave empty to create a new entry.
 * @returns void
 */
UserFrontend.editUserFrontend = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'calendarEntryId' : (typeof(Calendar) != "undefined" ? Calendar.newUserFrontendData.calendarEntryId : null),
        'do': (editId && editId > 0) ? 'edit' : 'new',
        'step': 1
        
    };

    this.editEntry(sendData, function() {jQuery('#calsy_user_frontend_name_first').focus();});
    
};

/**
 * Shows a brief history of appointments for the user with the specified identifier.
 * 
 * @author Peter Hamm
 * @param int userFrontendId
 * @returns void
 */
UserFrontend.showCalendarHistory = function(userFrontendId)
{
    
    var sendData = Calendar.historyData;
    
    sendData['do'] = 'getCalendarHistory';
    sendData['step'] = 1;
    
    this.editEntry(sendData);
    
};

UserFrontend.newPassword = function(editId)
{

    var sendData = {

        'editId' : editId,
        'do': 'editPassword',
        'step': 1

    };

    this.editEntry(sendData, function() { jQuery('#user_frontend_password_1').focus();} );

};

UserFrontend.savePassword = function()
{

    var sendData = {

        "do" : "savePassword",
        "editId" : jQuery("#editId").val(),
        "step" : 1,
        "user_frontend_password_1" : jQuery("#user_frontend_password_1").val(),
        "user_frontend_password_2" : jQuery("#user_frontend_password_2").val()

    };
    
    this.saveEntry(sendData, function(data) { 
        
        return UserFrontend.validatePassword(data);
    
    }, null, null, true, false);
    
};

UserFrontend.validatePassword = function(sendData)
{

    this.highlightInputTitleError('user_frontend_password_1', 'remove');
    this.highlightInputTitleError('user_frontend_password_2', 'remove');

    if(!Validation.checkPassword(sendData.user_frontend_password_1, sendData.user_frontend_password_2, '#password_error_box'))
    {

        this.highlightInputTitleError('user_frontend_password_1');
        this.highlightInputTitleError('user_frontend_password_2');
        
        return false;

    }

    return true;

};

UserFrontend.cancel = function()
{
        
    if(typeof(Calendar) != "undefined" && Calendar.newUserFrontendData != null)
    {

        var calendarEntryId = Calendar.newUserFrontendData.calendarEntryId;

        Calendar.handleEditEvent(calendarEntryId);

    }
    else
    {
        
        PerisianBaseAjax.cancel();
        
    }
    
};

/**
 * Saves the currently edited entry.
 * 
 * @author Peter Hamm
 * @returns void
 */
UserFrontend.saveUserFrontend = function()
{
    
    var editId = jQuery("#editId").val();
    
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId
        
    }, "#" + Perisian.containerIdentifier, "calsy_user_frontend_");
        
    if(!UserFrontend.validate(sendData))
    {
        
        return;
        
    }
    
    sendData['fields_custom'] = UserFrontend.getCustomFieldValues();
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10834));
            
            this.dataSent = false; 
            
            if(typeof(Calendar) != "undefined" && Calendar.newUserFrontendData != null)
            {
                
                var calendarEntryId = Calendar.newUserFrontendData.calendarEntryId;
                
                Calendar.newUserFrontendData['id'] = callback.newId;
                Calendar.newUserFrontendData['fullname'] = callback.fullname;
                
                Calendar.handleEditEvent(calendarEntryId);
                
            }
            else
            {
                
                UserFrontend.cancel(); 
                UserFrontend.showEntries(); 
                
            }
                    
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

UserFrontend.deleteUserFrontend = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10835, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        UserFrontend.showEntries();
        
        toastr.success(Language.getLanguageVariable(10836));
    
    });
    
};

UserFrontend.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        UserFrontend.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_user_frontend_name_last))
    {

        UserFrontend.highlightInputTitleError('calsy_user_frontend_name_last', false);
        
        ++errors;

    }
    
    if(!Validation.checkRequired(sendData.calsy_user_frontend_name_first))
    {

        UserFrontend.highlightInputTitleError('calsy_user_frontend_name_first', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
UserFrontend.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
                
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
UserFrontend.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
UserFrontend.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};

/**
 * Toggles the display state of additional information for the specified order in the table.
 * 
 * @param int orderId
 * @param String state Either "visible" or "hidden"
 * @returns void
 */
UserFrontend.toggleMoreDetails = function(orderId, state) 
{
    
    var element = jQuery('#table_info_order_' + orderId);
    var buttonToggle = element.find('#button_toggle_details');
    
    if(buttonToggle.hasClass('md-keyboard-arrow-down') || state == "visible")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-down');
        buttonToggle.addClass('md-keyboard-arrow-up');
        
        element.find('.table-info-additional').css('display', 'block');
        
    }
    else if(buttonToggle.hasClass('md-keyboard-arrow-up') || state == "hidden")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-up');
        buttonToggle.addClass('md-keyboard-arrow-down');
        
        element.find('.table-info-additional').css('display', 'none');
        
    }
    
};

/**
 * Exports the user data
 * 
 * @author Peter Hamm
 * @returns void
 */
UserFrontend.export = function()
{

    window.open(this.controller + '?do=export&step=2&type=default', '_blank');

};

/**
 * Toggles the form to import data
 * 
 * @author Peter Hamm
 * @returns void
 */
UserFrontend.import = function()
{

    var sendData = {

        'do': 'import',
        'step': 1
        
    };

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');

    elementContainer.load(this.controller, sendData, function () {

        Perisian.ajaxBox('show');

    });

};

UserFrontend.startImport = function()
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var uploadedFile = elementContainer.find('#uploaded_file').val();
    
    elementContainer.find('.import-selection').css('display', 'none');
    elementContainer.find('.import-processing').css('display', 'block');
    
    var selectedOption = elementContainer.find('#import_type :selected');
    
    var sendData = {

        'do': 'import',
        'step': 1,
        'f': uploadedFile,
        't': selectedOption.val(),
        'v': selectedOption.attr('data-version')
        
    };
        
    jQuery.post(this.controllerImport, sendData, function(data) {
       
        elementContainer.find('.import-processing').css('display', 'none');
        
        var resultContainer = elementContainer.find('.import-result');
        
        resultContainer.css('display', 'block');
        
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
            
            resultContainer.addClass('text-danger');
            
        }
        else
        {
            
            resultContainer.addClass('text-success');
            
        }
        
        UserFrontend.showEntries();
        
        jQuery(resultContainer[0]).html(callback.message);
                
    });
    
};

UserFrontend.displayImportError = function(errorMessage)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    elementContainer.find('.import-selection').css('display', 'none');
    
    elementContainer.find('.import-result:first').html('<i class="fa fa-exclamation-circle text-danger"></i> ' + errorMessage);
    
    elementContainer.find('.import-result').css('display', 'block');
    
};

UserFrontend.onUploadSuccess = function(data, response) {
        
    var callback = JSON.parse(response);

    if(!callback.success)
    {

        Association.displayImportError(callback.message);

    }
    else
    {
        
        var element = jQuery("#" + Perisian.containerIdentifier);
        var elementContainer = element.find('.modal-content');
        
        elementContainer.find('#uploaded_file').val(callback.file);
        
        elementContainer.find('#startButton').removeClass("disabled");
        
    }

};

UserFrontend.toggleAdditionalData = function(userFrontendId)
{
    
    var sendData = {

        'do': 'additionalData',
        'm': userFrontendId
        
    };

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');

    elementContainer.load(this.controller, sendData, function () {

        Perisian.ajaxBox('show');

    });
    
};

UserFrontend.removeMergeDataSet = function(targetIndex)
{

    var emptyElements = jQuery('.data-set-merge-element-empty');
    var dataElements = jQuery('.data-set-merge-element-data');
    
    var targetData = jQuery(dataElements[targetIndex]);
    var targetEmpty = jQuery(emptyElements[targetIndex]);
    
    targetEmpty.css('display', 'block');
    targetData.css('display', 'none');
    
    targetData.find('.data-set-merge-element-id').html('');
    targetData.find('.data-set-merge-element-name').html('');
    targetData.find('.data-set-merge-element-count-calendar-entries').html('');
    
    UserFrontend.checkDisplayButtonMerge(emptyElements, dataElements);
            
};

UserFrontend.addMergeDataSet = function(userFrontendId)
{
    
    var sendData = {
        
        'do': 'getUserFrontendInfoById',
        'u': userFrontendId
        
    };
    
    jQuery.post(UserFrontend.controller, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10057);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
                           
            var emptyElements = jQuery('.data-set-merge-element-empty');
            var dataElements = jQuery('.data-set-merge-element-data');
            
            var targetIndex = jQuery(dataElements[0]).css('display') == 'block' ? 1 : 0;
            var oppositeIndex = targetIndex == 0 ? 1 : 0;
            
            var targetData = jQuery(dataElements[targetIndex]);
            var targetEmpty = jQuery(emptyElements[targetIndex]);
            
            var oppositeData = jQuery(dataElements[oppositeIndex]);
            
            if(targetData.css('display') == 'block')
            {
                
                // Both data containers are currently displayed and filles already
                
                return;
                
            }
            
            if(oppositeData.find('.data-set-merge-element-id').html() == callback.data['calsy_user_frontend_id'])
            {
                
                // This one is already selected.
                
                return;
                
            }
            
            targetData.css('display', 'block');
            targetEmpty.css('display', 'none');
            
            targetData.find('.data-set-merge-element-id').html(callback.data['calsy_user_frontend_id']);
            targetData.find('.data-set-merge-element-name').html(callback.data['calsy_user_frontend_name_first'] + ' ' + callback.data['calsy_user_frontend_name_last']);
            targetData.find('.data-set-merge-element-count-calendar-entries').html(callback.data['count_calendar_entries']);
            
            UserFrontend.checkDisplayButtonMerge(emptyElements, dataElements);
                   
        }        
        
    });
        
};

UserFrontend.checkDisplayButtonMerge = function(emptyElements, dataElements)
{
    
    var button = jQuery('#button-validate-merge');
    
    var isDisabled = true;
    
    if(jQuery(dataElements[0]).css('display') == 'block' && jQuery(dataElements[1]).css('display') == 'block')
    {
        
        isDisabled = false;

    }
    
    button.prop('disabled', isDisabled);
    
};

UserFrontend.validateMerge = function()
{
    
    var dataElements = jQuery('.data-set-merge-element-data');
    
    var mergeIds = Array(jQuery(dataElements[0]).find('.data-set-merge-element-id').html(), jQuery(dataElements[1]).find('.data-set-merge-element-id').html());
    
    var sendData = {
        
        'do': 'validateMerge',
        'm': mergeIds
        
    };
    
    this.editEntry(sendData, function() {});
    
};

UserFrontend.selectMergeValue = function(fieldName, version)
{
    
    if(jQuery('#' + fieldName + '_merge').is('select'))
    {
        
        var value = jQuery('#' + fieldName + '_' + version).attr('enum');
        
        jQuery('#' + fieldName + '_merge').val(value);
        
    }
    else
    {
        
        var value = jQuery('#' + fieldName + '_' + version).val();

        jQuery('#' + fieldName + '_merge').val(value);
        
    }
    
};

UserFrontend.mergeData = function()
{
    
    var mergeIds = Array(jQuery('#calsy_user_frontend_id_one').val(), jQuery('#calsy_user_frontend_id_two').val());
    
    var sendData = {
        
        'do': 'merge',
        'm': mergeIds,
                
    };
    
    jQuery('#' + Perisian.containerIdentifier).find('input, select').each(function() {
        
        var element = jQuery(this);
        
        var type = element.is('select') ? 'select' : element.attr('type');
        
        var fieldId = '';
        var fieldValue = '';
        
        if(type == 'text')
        {
            
            if(element.attr('id').substr(-6) != '_merge')
            {
                
                // Ignore this one
                
                return;
                
            }
            
            fieldId = element.attr('id').replace('_merge', '');
            fieldValue = element.val();
            
        }
        if(type == 'radio')
        {
            
            fieldId = element.attr('name');
            fieldValue = jQuery("input[name='" + fieldId + "']:checked").val();
            
        }
        else if(type == 'select')
        {
            
            if(element.attr('id').substr(-6) != '_merge')
            {
                
                // Ignore this one
                
                return;
                
            }
            
            fieldId = element.attr('id').replace('_merge', '');
            fieldValue = element.val();
            
        }
        
        if(fieldId.length > 0)
        {
            
            sendData[fieldId] = fieldValue;
            
        }
        
    });
            
    // Disable the save button
    jQuery("#submitButton").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable('p59c524c7eb796');
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable('p59c524a39d6ba'));
            
            this.dataSent = false; 
            
            {
                
                UserFrontend.removeMergeDataSet(0);
                UserFrontend.removeMergeDataSet(1);
                
                UserFrontend.cancel(); 
                UserFrontend.showEntries(); 
                
            }
                    
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });
    
    
};