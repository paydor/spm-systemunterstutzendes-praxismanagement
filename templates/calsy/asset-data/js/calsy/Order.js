/**
 * Class to handle orders
 *
 * @author Peter Hamm
 * @date 2016-01-30
 */
var Order = jQuery.extend(true, {}, PerisianBaseAjax);

Order.controller = baseUrl + "/order/overview/";
Order.editController = baseUrl + "/order/edit/";

Order.currentSortOrder = 'ASC';
Order.currentSorting = 'calsy_order_title';

Order.filterUserFrontendId = 0;
Order.filterSupplierId = 0;
    
Order.createNewOrder = function()
{
    
    Order.editOrder();
    
};

Order.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

Order.editOrder = function(editId)
{
    
    PerisianBaseAjax.goToUrl(this.editController + (editId ? "?e=" + editId : ""));
    
};

Order.saveOrder = function()
{
    
    var editId = jQuery("#editId").val();
    
    var sendData = {
        
        "do": "save",
        "editId": editId, 
        
        "calsy_order_title": jQuery("#calsy_order_title").val(),
        "calsy_order_supplier_id": PerisianBaseAjax.getAutocompleteFieldIdentifier("#calsy_order_supplier_id"),
        "calsy_order_user_frontend_id": PerisianBaseAjax.getAutocompleteFieldIdentifier("#calsy_order_user_frontend_id"),
        "calsy_order_use_count": jQuery("#calsy_order_use_count").val()
        
    };
    
    if(!Order.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10805));
            
            jQuery("#editId").val(callback.newId);
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

Order.deleteOrder = function(deleteId)
{
    
    this.deleteEntry(deleteId, 10839, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        Order.showEntries();
        
        toastr.success(Language.getLanguageVariable(10838));
    
    });
    
};

Order.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        Order.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_order_title))
    {

        Order.highlightInputTitleError('calsy_order_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
Order.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};
        
Order.updateParameters = function()
{
        
    Order.filterUserFrontendId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_user_frontend_id");
    Order.filterSupplierId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_supplier_id");
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
Order.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    Order.updateParameters();

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'p': Order.filterUserFrontendId,
        'd': Order.filterSupplierId,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
Order.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};

/**
 * Toggles the display state of additional information for the specified order in the table.
 * 
 * @param int orderId
 * @param String state Either "visible" or "hidden"
 * @returns void
 */
Order.toggleMoreDetails = function(orderId, state) {
    
    var element = jQuery('#table_info_order_' + orderId);
    var buttonToggle = element.find('#button_toggle_details');
    
    if(buttonToggle.hasClass('md-keyboard-arrow-down') || state == "visible")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-down');
        buttonToggle.addClass('md-keyboard-arrow-up');
        
        element.find('.table-info-additional').css('display', 'block');
        
    }
    else if(buttonToggle.hasClass('md-keyboard-arrow-up') || state == "hidden")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-up');
        buttonToggle.addClass('md-keyboard-arrow-down');
        
        element.find('.table-info-additional').css('display', 'none');
        
    }
    
};