/**
 * Class to handle areas
 *
 * @author Peter Hamm
 * @date 2020-03-04
 */
var MasterData = jQuery.extend(true, {}, PerisianBaseAjax);

MasterData.controller = baseUrl + "/master_data/overview/";

MasterData.currentSortOrder = 'DESC';
MasterData.currentSorting = 'calsy_master_data_id';
    
MasterData.createNewInvitation = function()
{
    
    var sendData = {
        
        'do': 'createNewInvitation'
        
    };
    
    PerisianBaseAjax.loadInModal(MasterData.controller, sendData);
    
};

MasterData.getSelectedUsers = function()
{
    
    var users = [];
    
    jQuery('.calsy-user-selector-multi-element-entry').each(function() {

        var element = jQuery(this);
        var checkbox = element.find('input[type=checkbox]');
        var isChecked = checkbox.is(':checked');

        if(isChecked)
        {

            users.push(checkbox.val());

        }
        
    });
    
    return users;
    
};

MasterData.getSelectedFields = function()
{
    
    var fields = [];
    
    jQuery('.calsy-field-selector').each(function() {

        var element = jQuery(this);
        var checkbox = element.find('input[type=checkbox]');
        var isChecked = checkbox.is(':checked');

        if(isChecked)
        {

            fields.push(checkbox.val());

        }
        
    });
    
    return fields;
    
};

MasterData.getTimeExpiration = function() 
{
        
    var time = Calendar.getTimestampFromFields("#expiration_date", "#expiration_date_time");
    
    return time;
    
};

MasterData.sendInvitations = function()
{
 
    var sendData = {
        
        'do': 'sendInvitations',
        'recipients': MasterData.getSelectedUsers(),
        'fields': MasterData.getSelectedFields(),
        'time_expiration': MasterData.getTimeExpiration(),
        'text': jQuery('#invitation_text').val()
        
    };
    
    jQuery('#submitButton').prop('disabled', true);
    
    jQuery.post(MasterData.controller, sendData, function(data) {
       
        var result = JSON.parse(data);
        
        if(result.success)
        {
            
            toastr.success(result.message);
            
            MasterData.showEntries();
            
            Perisian.ajaxBox('hide');
                        
        }
        else
        {
            
            toastr.error(result.message);
            
            jQuery('#submitButton').prop('disabled', false);
            
        }
        
    });
    
};

MasterData.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

MasterData.deleteMasterData = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        MasterData.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
MasterData.showEntries = function(offset, limit, sortBy, sortOrder)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
MasterData.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};