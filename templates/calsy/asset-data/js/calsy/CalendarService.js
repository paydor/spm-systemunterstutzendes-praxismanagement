/* 
 * Provides services for the calendar, e.g. to update the displayed number of unconfirmed entries in the menu.
 * 
 * @author Peter Hamm
 * @date 2016-03-07
 */
var CalendarService = {
    
    controller: baseUrl + "/calendar/overview/",
    
    idMenuPointUnconfirmedEntries: '42',
    
    timeForIntervalUpdateCountEntries: 25000,
    
    intervalUpdateCountEntries: 0,
    
    startUpdaterCountEntries: function()
    {
        
        CalendarService.intervalUpdateCountEntries = setInterval("CalendarService.updateCountEntries()", CalendarService.timeForIntervalUpdateCountEntries);
        
    },
    
    stopUpdaterCountEntries: function()
    {
        
        clearInterval(CalendarService.intervalUpdateCountEntries);
        
    },
    
    updateCountEntries: function()
    {
                
        jQuery.get(CalendarService.controller, {
            
            'do': 'getJson',
            'source': 'entryCounts'
            
        }, function(data) {
            
            var unconfirmedElementId = "#menu_point_" + CalendarService.idMenuPointUnconfirmedEntries + " .info";
            var unconfirmedElement = jQuery(unconfirmedElementId);
            
            try
            {
                
                var returnData = jQuery.parseJSON(data);

                unconfirmedElement.html(returnData.unconfirmed).css('display', (returnData.unconfirmed > 0 ? 'inline' : 'none'));
                
            }
            catch(e)
            {
                
                console.log("Could not update.");
                
            }
            
        });
        
    }
    
};

jQuery(document).ready(CalendarService.startUpdaterCountEntries);