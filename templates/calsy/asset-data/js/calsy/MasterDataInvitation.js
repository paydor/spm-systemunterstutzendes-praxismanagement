
var MasterDataInvitation = {
    
    'controller': baseUrl + 'master_data/',
    
    'init': function()
    {
        
        jQuery('#btn-update-master-data').click(MasterDataInvitation.updateMasterData);
        
    },
    
    'getSendData': function()
    {
        
        var sendData = {};
        
        jQuery('#form-master-data').find('input, select, textarea').each(function() {
            
            var element = jQuery(this);
            
            sendData[element.attr('id')] = element.val();
            
        });
        
        return sendData;
        
    },
        
    'updateMasterData': function() 
    {
        
        jQuery('.btn-update').attr('disabled', true);
        
        var sendData = MasterDataInvitation.getSendData();
        
        sendData['do'] = 'updateMasterData';
        
        jQuery.post(MasterDataInvitation.controller, sendData, function(result) {
            
            var data = JSON.parse(result);
            
            if(data.success)
            {
                
                toastr.success(data.message);
                
                location.href = MasterDataInvitation.controller + '?do=success&e=' + data.code;
                
            }
            else
            {
                        
                toastr.error(data.message);
            
                jQuery('.btn-invitation').attr('disabled', false);
                
            }
            
        });
        
    }
    
};

