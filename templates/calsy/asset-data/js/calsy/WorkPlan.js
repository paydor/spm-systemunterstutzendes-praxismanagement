/**
 * Class to handle work plans
 *
 * @author Peter Hamm
 * @date 2017-12-15
 */
var WorkPlan = jQuery.extend(true, {}, PerisianBaseAjax);

WorkPlan.controller = baseUrl + "/work_plan/overview/";

WorkPlan.containerMainId = '#work-plan-container';
WorkPlan.containerDetailId = '#work-plan-detail-container';

WorkPlan.currentWorkPlanElements = [];

/**
 * Triggers the printing of the currently displayed page.
 * 
 * @author peter Hamm
 * @returns void
 */
WorkPlan.printWorkPlan = function()
{
        
    window.print();
    
};

/**
 * This is gonna get fired after clicking on an hourly element.
 * 
 * @author Peter Hamm
 * @param int targettedTime
 * @param int targettedDay
 * @returns void
 */
WorkPlan.handleClickHourlyElement = function(targettedTime, targettedDay)
{
            
    targettedTime = parseInt(targettedTime) + Calendar.getDstOffsetForTimestamp();
    
    WorkPlan.addWorkPlanElement(targettedDay, targettedTime);
        
};

/**
 * Triggered on double click or "taphold" in the hourly view.
 * 
 * @author Peter Hamm
 * @param Object e
 * @returns void
 */
WorkPlan.onClickHourlyElement = function(e)
{
    
    e.preventDefault();
    e.stopImmediatePropagation();
        
    var timeAndDay = WorkPlan.getClickedHourlyTimeAndDay(e);
    
    if(timeAndDay.time >= 0)
    {
        
        WorkPlan.handleClickHourlyElement(timeAndDay.time, timeAndDay.day);
        
    }
    
};

WorkPlan.getClickedHourlyTimeAndDay = function(event)
{
    
    var clickedElement = jQuery(event.target).parent();
    var clickX = event.type == "taphold" ? mousePageX : event.pageX;

    var targettedDayElement = null;
    
    var targettedTime = 0;
    var targettedDay = 0;
    
    // Find the time
    {

        jQuery('.fc-day-header-element').each(function() {

            var element = jQuery(this);

            var left = element.offset().left - parseInt(element.css('padding-left'));
            var right = left + element.width() + parseInt(element.css('padding-right'));

            if(clickX >= left && clickX <= right)
            {

                targettedDayElement = element;

                return;

            }

        });

        if(targettedDayElement != null)
        {
            
            targettedDay = parseInt(targettedDayElement.attr('data-day'));
            
        }
        
        {

            targettedTime = parseInt(clickedElement.attr('data-unix-time'));

            var dateDummy = new Date(targettedTime * 1000);
            var offset = (!dateDummy.isInDST() ? 3600 : 0);

            targettedTime -= offset;

        }
    
    }
        
    var returnValue = {
        
        'day': targettedDay,
        'time': targettedTime
        
    };
        
    return returnValue;
    
};

/**
 * Initializes clicks on hourly elements.
 * 
 * @author Peter Hamm 
 * @returns void
 */
WorkPlan.initHourlyDisplayClicks = function()
{
    
    jQuery('.fc-widget-content-clickable').doubletap(WorkPlan.onClickHourlyElement);

    if(!Perisian.isMobileDevice())
    {

        jQuery('.fc-widget-content-clickable').longclick(300, WorkPlan.onClickHourlyElement);

    }

    if(Calendar.currentDisplayType == "week")
    {

        jQuery('.fc-weekly-day-header').click(function() {
            
            WorkPlan.addWorkPlanElement(jQuery(this).attr('data-day'), 3600 * 8);
            
        });

    }
    
};

WorkPlan.updateWorkPlanElementsAndSave = function()
{
    
    // Gather info for the work plan element that is being edited/added currently.
    {
        
        var timeFrom = jQuery('#element_time_from').val().split(':');
        var timeTo = jQuery('#element_time_to').val().split(':');
        
        timeFrom = parseInt(timeFrom[0]) * 3600 + parseInt(timeFrom[1]) * 60;
        timeTo = parseInt(timeTo[0]) * 3600 + parseInt(timeTo[1]) * 60;
     
        var elementToUpdate = {
            
            'id': jQuery('#work_plan_element_id').val(),
            
            'title': jQuery('#element_title').val(),
            'area_id': jQuery('#element_area option:selected').val(),
            
            'time_from': timeFrom,
            'time_to': timeTo,
            
            'day_from': parseInt(jQuery('#element_day_from option:selected').val()),
            'day_to': parseInt(jQuery('#element_day_to option:selected').val()),
            
        };
        
    }
         
    if(!WorkPlan.validateWorkPlanElement(elementToUpdate))
    {
        
        return;
        
    }
        
    if(!elementToUpdate.id || elementToUpdate.id <= 0)
    {
        
        if(!jQuery.isArray(WorkPlan.currentWorkPlanElements))
        {
            
            WorkPlan.currentWorkPlanElements = [];
            
        }
        
        WorkPlan.currentWorkPlanElements.push(elementToUpdate);
        
    }
    else
    {
        
        for(var i = 0; i < WorkPlan.currentWorkPlanElements.length; ++i)
        {
            
            if(WorkPlan.currentWorkPlanElements[i].id == elementToUpdate.id)
            {
                
                WorkPlan.currentWorkPlanElements[i] = elementToUpdate;
                
                break;
                
            }
            
        }
        
    }
    
    WorkPlan.saveWorkPlanElements();
    
};

WorkPlan.saveWorkPlanElements = function()
{
    
    var sendData = {
        
        'do': 'saveWorkPlanElements',
        'p': WorkPlan.getSelectedWorkPlanId(),
        'elements': WorkPlan.currentWorkPlanElements
        
    };
        
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
                        
            this.dataSent = false; 
            
            WorkPlan.initWorkPlanElements(callback['work_plan_elements']);
                        
            WorkPlan.cancel(); 
            
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });
    
};

WorkPlan.initWorkPlan = function(workPlanElements)
{
    
    WorkPlan.initHourlyDisplayClicks();
    WorkPlan.initWorkPlanElements(workPlanElements);
    
    jQuery('#work-plan-element-new').click(function() {
        
        WorkPlan.addWorkPlanElement(0, 3600 * 8);
        
    });
    
};

WorkPlan.initWorkPlanElements = function(workPlanElements)
{
        
    WorkPlan.currentWorkPlanElements = workPlanElements;
    
    jQuery('.fc-event-container').empty();
    
    for(var i = 0; i < workPlanElements.length; ++i)
    {
        
        var dayFrom = workPlanElements[i]['day_from'];
        var dayTo = workPlanElements[i]['day_to'];
        
        for(var day = dayFrom; day <= dayTo; ++day)
        {

            var eventContainer = jQuery(jQuery('.fc-event-container')[day]);
                        
            var element = jQuery('#element_dummy').clone();
            
            var dateDummyFrom = new Date(parseInt(workPlanElements[i]['time_from']) * 1000);
            var offsetFrom = (!dateDummyFrom.isInDST() ? 3600 : 0);
            
            var dateDummyTo = new Date(parseInt(workPlanElements[i]['time_to']) * 1000);
            var offsetTo = (!dateDummyTo.isInDST() ? 3600 : 0);

            // Update the element
            {
                
                var startsToday = dayFrom == day;
                var endsToday = dayTo == day;
                
                var timestampBeginToday = startsToday ? parseInt(workPlanElements[i]['time_from']) - offsetFrom : 0;
                var timestampEndToday = endsToday ? parseInt(workPlanElements[i]['time_to']) - offsetTo : 86400;

                element.attr('id', workPlanElements[i]['id']);
                element.attr('data-begin', timestampBeginToday);
                element.attr('data-end', timestampEndToday);

                element.find('.fc-title').html(workPlanElements[i]['title']);
                element.find('.fc-time').html(workPlanElements[i]['time_formatted']);
                
                element.attr('data-starts-today', startsToday ? '1' : '0');
                element.attr('data-ends-today', endsToday ? '1' : '0');
                
                element.css('display', 'block');
                
                element.addClass('fc-' + (startsToday ? 'not-' : '') + '-start');
                element.addClass('fc-' + (startsToday ? 'not-' : '') + '-end');
                
            }

            eventContainer.append(element);   
            
            Calendar.alignHourlyEventElement(element);
            
        }
        
    }
    
    jQuery('.fc-event').click(function() {
        
        var clickedElement = jQuery(this);
        
        WorkPlan.editWorkPlanElement(clickedElement.attr('id'));
        
    });
    
};

WorkPlan.addWorkPlanElement = function(day, time)
{
        
    var sendData = {

        'do': 'addWorkPlanElement',
        'p': WorkPlan.getSelectedWorkPlanId(),
        'day': day,
        'time': time
        
    };

    this.editEntry(sendData);
    
};

WorkPlan.editWorkPlanElement = function(workPlanElementId)
{
        
    var sendData = {

        'do': 'editWorkPlanElement',
        'p': WorkPlan.getSelectedWorkPlanId(),
        'e': workPlanElementId
        
    };

    this.editEntry(sendData);
    
};

WorkPlan.deleteForWeek = function(timestampInWeek, workPlanId)
{
    
    var sendData = {

        'do': 'deleteWorkPlanForWeek',
        'p': workPlanId,
        't': timestampInWeek
        
    };

    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
       
            WorkPlan.loadYearForApplicationOfWorkPlan(timestampInWeek, workPlanId);
            
        }
        else
        {
            
            toastr.error(callback.message);
            
        }
                
    });
    
};

WorkPlan.applyForWeek = function(timestampInWeek, workPlanId)
{
    
    var sendData = {

        'do': 'applyWorkPlanForWeek',
        'p': workPlanId,
        't': timestampInWeek
        
    };

    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
       
            WorkPlan.loadYearForApplicationOfWorkPlan(timestampInWeek, workPlanId);
            
        }
        else
        {
            
            toastr.error(callback.message);
            
        }
                
    });
    
};

WorkPlan.loadYearForApplicationOfWorkPlan = function(timestampInYear, workPlanId)
{
        
    var sendData = {

        'do': 'getWorkPlanApplicationYear',
        'p': workPlanId,
        't': timestampInYear
        
    };

    jQuery.post(this.controller, sendData, function(data) {
       
        if(data)
        {
        
            jQuery('#work-plan-month-application-container').html(data);
            
        }
                
    });
    
};

WorkPlan.applyWorkPlan = function(workPlanId)
{
        
    var sendData = {

        'do': 'applyWorkPlan',
        'p': workPlanId
        
    };

    this.editEntry(sendData);
    
};

WorkPlan.deleteWorkPlanElement = function(elementId)
{
 
    if(!confirm(Language.getLanguageVariable('10305')))
    {
        
        return;
        
    }
    
    var sendData = {
        
        'do': 'deleteWorkPlanElement',
        'p': WorkPlan.getSelectedWorkPlanId(),
        'e': elementId
        
    };
            
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
                        
            this.dataSent = false; 
            
            WorkPlan.initWorkPlanElements(callback['work_plan_elements']);
                        
            WorkPlan.cancel(); 
            
        }
                
    });
    
};

WorkPlan.editWorkPlan = function(workPlanId) 
{
    
    var sendData = {

        'do': 'edit',
        'p': workPlanId
        
    };

    this.editEntry(sendData);
    
};

WorkPlan.createNewWorkPlan = function() 
{
    
    var sendData = {

        'do': 'new'
        
    };

    this.editEntry(sendData);
    
};

WorkPlan.saveWorkPlan = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "p": editId,
        
    }, "#" + Perisian.containerIdentifier, "calsy_work_plan_");
            
    if(!WorkPlan.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            WorkPlan.cancel(); 
            
            WorkPlan.loadWorkPlanList(function() {
                
                WorkPlan.loadWorkPlan(callback.id); 
                WorkPlan.selectWorkPlan(callback.id);
                
            });
            
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

WorkPlan.getSelectedWorkPlanId = function() 
{
            
    return jQuery('#plan_selector option:selected').val();
    
};

WorkPlan.selectWorkPlan = function(workPlanId) 
{
            
    jQuery('#plan_selector option').each(function() {
        
        var element = jQuery(this);
        
        element.prop("selected", element.val() == workPlanId);
        
    });
    
};

WorkPlan.loadWorkPlan = function(workPlanId)
{
    
    WorkPlan.currentWorkPlanElements = {};
    
    var sendData = {
        
        'do': 'getWorkPlan',
        'p': workPlanId
        
    };

    jQuery.post(this.controller, sendData, function(data) {
        
        jQuery(WorkPlan.containerMainId).html(data);

    });
    
    var sendDataDetails = {
        
        'do': 'getWorkPlanDetails',
        'p': workPlanId
        
    };

    jQuery.post(this.controller, sendDataDetails, function(data) {
                
        jQuery(WorkPlan.containerDetailId).html(data);
                
        jQuery('#work-plan-print').click(WorkPlan.printWorkPlan);

    });
        
};

WorkPlan.deleteWorkPlan = function(workPlanId)
{
    
    if(!confirm(Language.getLanguageVariable('p5a3567b555a6f')))
    {
        
        return;
        
    }
    
    var sendData = {
        
        'do': 'deleteWorkPlan',
        'p': workPlanId
        
    };
    
    jQuery.post(this.controller, sendData, function(data) {
        
        var callbackData = JSON.parse(data);
        
        if(callbackData.success)
        {
            
            toastr.success(Language.getLanguageVariable('p5a356795c7647'));
            
            if(WorkPlan.getSelectedWorkPlanId() == callbackData.id)
            {

                WorkPlan.loadWorkPlanList(function() {

                    WorkPlan.loadWorkPlan(-1);
                    WorkPlan.selectWorkPlan(-1);

                });

            }
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable('p5a3567e91221d'));
            
        }

    });
    
};

WorkPlan.loadWorkPlanList = function(callback)
{
        
    var sendData = {
        
        'do': 'getListWorkPlans'
        
    };
        
    jQuery.post(this.controller, sendData, function(data) {
        
        var callbackData = JSON.parse(data);
        
        var currentlySelectedValue = jQuery('#plan_selector option:selected').val();
    
        jQuery('#plan_selector option').not(':first').remove();
                                
        for(var i = 0; i < callbackData['list'].length; ++i)
        {
            
            var option = '<option value="' + callbackData['list'][i]['calsy_work_plan_id'] + '"' + (callbackData['list'][i]['calsy_work_plan_id'] == currentlySelectedValue ? ' selected' : '') + '>' + callbackData['list'][i]['calsy_work_plan_title'] + '</option>';
            
            jQuery('#plan_selector').append(option);
            
        }
        
        if(callback != null)
        {

            callback();

        }
        
    });
    
};

WorkPlan.validateWorkPlanElement = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    elementContainer.find('.form-group').removeClass('has-error');
        
    if(!Validation.checkRequired(sendData['title']))
    {

        WorkPlan.highlightInputTitleError('element_title', false);
        
        ++errors;

    }
    
    if(parseInt(sendData['day_from']) >= parseInt(sendData['day_to']) && parseInt(sendData['time_from']) >= parseInt(sendData['time_to']))
    {
        
        WorkPlan.highlightInputTitleError('element_time_from', false);
        WorkPlan.highlightInputTitleError('element_time_to', false);
        WorkPlan.highlightInputTitleError('element_day_from', false);
        WorkPlan.highlightInputTitleError('element_day_to', false);
        
        ++errors;
        
    }
    
    return errors == 0;

};

WorkPlan.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    elementContainer.find('.form-group').removeClass('has-error');
        
    if(!Validation.checkRequired(sendData.calsy_work_plan_title))
    {

        WorkPlan.highlightInputTitleError('calsy_work_plan_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};