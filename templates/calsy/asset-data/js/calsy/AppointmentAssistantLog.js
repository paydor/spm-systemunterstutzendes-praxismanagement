/**
 * Appointment assistant log list navigation handling
 *
 * @author Peter Hamm
 * @date 2018-03-25
 */
var AppointmentAssistantLog = jQuery.extend(true, {}, PerisianBaseAjax);

AppointmentAssistantLog.controller = baseUrl + "/appointment_assistant/log/";

AppointmentAssistantLog.currentSortOrder = 'DESC';
AppointmentAssistantLog.AppointmentAssistantLog = 'calsy_appointment_assistant_log_time';

AppointmentAssistantLog.refreshLog = function() 
{
    
    AppointmentAssistantLog.showEntries();
    
};

/**
 * Asks whether to clear the log data or not.
 * 
 * @author Peter Hamm
 * @returns void
 */
AppointmentAssistantLog.clearLog = function() 
{
    
    if(confirm(Language.getLanguageVariable('p5ab7d33496b5c')))
    {
        
        var sendData = {
            
            do: 'emptyLog'
            
        };
        
        jQuery.post(this.controller, sendData, function(data) {

            var callback = JSON.parse(data);

            if(!callback.success)
            {

                toastr.warning(Language.getLanguageVariable(11174));

            }
            else
            {

                toastr.success(Language.getLanguageVariable(11175));
                
                AppointmentAssistantLog.showEntries();

            }

        });
        
    }
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
AppointmentAssistantLog.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};