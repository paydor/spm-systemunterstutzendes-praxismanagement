/**
 * Class to handle calendar resources
 *
 * @author Peter Hamm
 * @date 2016-02-29
 */
var CalendarResource = jQuery.extend(true, {}, PerisianBaseAjax);

CalendarResource.controller = baseUrl + "/resource/overview/";

CalendarResource.currentSortOrder = 'ASC';
CalendarResource.currentSorting = 'calsy_calendar_resource_title';
    
CalendarResource.createNewCalendarResource = function()
{
    
    CalendarResource.editCalendarResource();
    
};

CalendarResource.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

CalendarResource.editCalendarResource = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': 'edit',
        'step': 1
        
    };

    this.editEntry(sendData, function() {jQuery('#calsy_calendar_resource_title').focus();});
    
};

CalendarResource.saveCalendarResource = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId
        
    }, "#" + Perisian.containerIdentifier, "calsy_calendar_resource_");
    
    if(!CalendarResource.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10909));
            
            this.dataSent = false; 
            
            CalendarResource.cancel(); 
            CalendarResource.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

CalendarResource.deleteCalendarResource = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10911, function(data) {
        
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        CalendarResource.showEntries();
        
        toastr.success(Language.getLanguageVariable(10910));
    
    });
    
};

CalendarResource.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        CalendarResource.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_calendar_resource_title))
    {

        CalendarResource.highlightInputTitleError('calsy_calendar_resource_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
CalendarResource.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
CalendarResource.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
CalendarResource.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};