
VmosoChat.controller = baseUrl + "vmoso_backend/chat/";
VmosoChat.controllerLog = VmosoChat.controller;

VmosoChat.chatListPageData = null;
VmosoChat.isRefreshingChatList = false;

/**
 * Updates the data for the pagination of the chat list.
 * 
 * @author Peter Hamm
 * @param Object chatListPageData
 * @returns void
 */
VmosoChat.updateChatListPageData = function(chatListPageData)
{

    VmosoChat.chatListPageData = chatListPageData;
    
    var showButtonPrevious = false;
    var showButtonNext = false;
        
    if(parseInt(chatListPageData.pg.offset) > 0)
    {
                
        showButtonPrevious = true;
        
    }

    if(parseInt(chatListPageData.total) > parseInt(chatListPageData.pg.offset) + parseInt(chatListPageData.pg.limit))
    {
        
        showButtonNext = true;
        
    }
    
    jQuery('#button-chat-log-navigation-backward').css('display', showButtonPrevious ? 'inline-block' : 'none');
    jQuery('#button-chat-log-navigation-forward').css('display', showButtonNext ? 'inline-block' : 'none');
    
    jQuery('#button-chat-log-navigation-backward').parent().css('display', (showButtonPrevious || showButtonNext) ? 'block' : 'none');
        
};

/**
 * Triggered within the process of adding a message, just before pushing it onto the page.
 * You may modify the HTML, for example.
 * 
 * @author Peter Hamm
 * @param Object messageObject
 * @param Object data
 * @returns void
 */
VmosoChat.onAddMessage = function(messageObject, data)
{
    
    if(data['creator_name'])
    {
        
        messageObject.find('.chat-info .username').html(data['creator_name']);
        
    }

};

/**
* Handles the functionality status of the buttons that control the chat,
* e.g. the "Send message" button or "New Chat" button.
* 
* @author Peter Hamm
* @returns int
*/
VmosoChat.handleButtonStatus = function()
{

    var messageCount = VmosoChat.getDisplayedMessageCount();

    var displayCreateNew = VmosoChat.connectionEstablished;
    var displaySend = VmosoChat.connectionEstablished;

    jQuery('#button-chat-new').attr('disabled', !displayCreateNew);
    jQuery('#button-message-send').attr('disabled', !displaySend);

};

/**
 * Displays the form to start a new chat.
 * 
 * @author Peter Hamm
 * @returns void
 */
VmosoChat.newChat = function()
{

    var sendData = {

        'do': 'newChatForm'
        
    };

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');

    elementContainer.load(this.controller, sendData, function (data) {

        Perisian.ajaxBox('show');

    });
    
};

/**
 * Creates a new chat with the data provided by the form.
 * 
 * @author Peter Hamm
 * @returns void
 */
VmosoChat.createNewChat = function()
{
    
    jQuery('#submitButton').attr('disabled', true);
    
    var sendData = {

        'do': 'createNewChat',
        'title': jQuery('#chat-title').val(),
        'participants': VmosoChat.getSelectedUsersFromSelector('#chat-user-selector')

    };

    jQuery.post(this.controller, sendData, function(data) {

        var decodedData = jQuery.parseJSON(data);
        
        jQuery('#submitButton').attr('disabled', false);
                
        if(decodedData.success)
        {
                      
            location.href = VmosoChat.controllerLog + '?l=' + decodedData.data.api.key;

        }
        else
        {
            
            if(decodedData.title && decodedData.message)
            {
                
                toastr.error(decodedData.title + ' - ' + decodedData.message);
                
            }
            else
            {
                
                VmosoChat.throwError(decodedData);
                
            }

            return;

        }

    });
    
};

/**
 * Gets the selected users from the specified list.
 * 
 * @author Peter Hamm
 * @param String selectorId The identifier of the user selector DOM element.
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
VmosoChat.getSelectedUsersFromSelector = function(selectorId, asCsv)
{
    
    var returnValue = [];
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var selectorElement = elementContainer.find(selectorId);
    
    selectorElement.find("li.user-selector").each(function() {
        
        var element = jQuery(this);
        var userKey = element.attr('data-key');
        
        var isChecked = element.find(":checkbox").prop("checked");
        
        if(isChecked)
        {
            
            returnValue.push(userKey);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

/**
 * Hides the modal form box.
 * 
 * @author Peter Hamm
 * @returns void
 */
VmosoChat.cancel = function()
{
    
    Perisian.ajaxBox('hide');
    
};

/**
 * Will be fired just before the messages of the currently 
 * opened chat are updated, loads the list of chats for the current user.
 * 
 * @author Peter Hamm
 * @returns void
 */
VmosoChat.onRefreshLatestMessages = function()
{
        
    if(VmosoChat.isRefreshingChatList)
    {
        
        return;
        
    }
    
    VmosoChat.isRefreshingChatList = true;
        
    var sendData = {

        'do': 'getChatList'

    };

    jQuery.post(this.controller, sendData, function(data) {

        var decodedData = jQuery.parseJSON(data);
        
        if(decodedData.success)
        {
                        
            VmosoChat.buildChatOverview(decodedData.data.list);

            if(VmosoChat.chatListPageData == null)
            {

                VmosoChat.updateChatListPageData(decodedData.data.pager);

            }
            
            VmosoChat.onWindowResize();

        }
        else
        {

            VmosoChat.throwError(decodedData);

            return;

        }
        
        VmosoChat.isRefreshingChatList = false;

    });
    
};

/**
 * Sets the list of participants for this chat.
 * 
 * @author Peter Hamm
 * @param Array listParticipants
 * @returns void
 */
VmosoChat.setParticipants = function(listParticipants)
{
    
    var participantContainer = jQuery('#chat-participants');
    
    participantContainer.empty();
    
    var listFormattedParticipants = Array();
    
    for(var i = 0; i < listParticipants.length; ++i)
    {
        
        var newParticipant = listParticipants[i].name;
        
        if(listParticipants[i]['image'])
        {
            
            newParticipant = '<span class="user-color-header" style="top: 5px; background-image: url(\'' + listParticipants[i]['image'] + '\');"></span> ' + newParticipant;
            
        }
        
        listFormattedParticipants.push(newParticipant);
        
    }
    
    var participantString = Language.getLanguageVariable('p594fd75727a86').replace('%1', listFormattedParticipants.length) + ' ' + listFormattedParticipants.join(', ');
    
    participantContainer.html(participantString);
        
};

VmosoChat.onBeforeSendMessage = function(sendData)
{
    
    sendData.key = VmosoChat.chatKey;
    
};