
var VmosoChat = {
    
    'controller': baseUrl + "vmoso/chat/",
    'controllerLog': baseUrl + "vmoso/chat_log/",
    
    'chatMessageUpdateInterval': 2000,
    'layoutMinimumHeight': 300,
    
    'chatContainerId': '#chat-log',
    'loadMoreMessagesBoxId': '#box-load-older-messages',
    'chatScrollerItem': '.chat-body',
    'chatArchiveListContainer': '#chat-archive-container',
    'chatLogContainer': '#chat-log-container',
    
    'chatKey': '',
    
    'userData': {
        
        'name': 'User',
        'image': ''
        
    },
    
    'dummyMessage': null,
    'blockRefresh': false,
    'blockLoadOlderMessages': false,
    'connectionEstablished': false,
    
    'nextPageData': null,
    
    /**
     * Retrieves an empty dummy message.
     * 
     * @author Peter Hamm
     * @return Object
     */
    'getDummyMessage': function() {
        
        if(this.dummyMessage == null)
        {
            
            var content = jQuery('#chat-dummy').find('.chat-message-row');
            
            this.dummyMessage = jQuery(content);
            
        }
        
        return this.dummyMessage.clone();
        
    },
    
    /**
     * Sets the key of the current chat, e.g. to load messages for this key only.
     * 
     * @author Peter Hamm
     * @param String chatKey
     * @returns void
     */
    'setChatKey': function(chatKey) {
        
        VmosoChat.chatKey = chatKey;
        
    },
    
    /**
     * Retrieves a formatted date/time string, e.g. 'One minute ago'.
     * 
     * @author Peter Hamm
     * @param Date dateObject
     * @return String
     */
    'getFormattedTimeString': function(dateObject)
    {
        
        var returnValue = '';
        
        var seconds = Math.floor((new Date() - dateObject) / 1000);
        
        if(seconds <= 40)
        {
            
            // 'Just now'
            returnValue = Language.getLanguageVariable('p587d1841cbdd3');
            
        }
        else if(seconds <= 90)
        {
            
            // 'One minute ago'
            returnValue = Language.getLanguageVariable('p587d17e3c3237');
            
        }
        else if(seconds <= 150)
        {
            
            // 'Two minutes ago'
            returnValue = Language.getLanguageVariable('p587d1810dd7bf');
            
        }
        else if(seconds <= 210)
        {
            
            // 'Three minutes ago'
            returnValue = Language.getLanguageVariable('p587d185c692d7');
            
        }
        else if(seconds <= 270)
        {
            
            // 'Four minutes ago'
            returnValue = Language.getLanguageVariable('p587d18af9acfc');
            
        }
        else if(seconds <= 360)
        {
            
            // 'Five minutes ago'
            returnValue = Language.getLanguageVariable('p587d18c294ac5');
            
        }
        else
        {
            
            returnValue = this.formatTime(dateObject);
            
        }

        return returnValue;
                
    },
    
    /**
     * Returns a localized formated date/time string.
     * 
     * @author Peter Hamm
     * @param Date dateObject
     * @return String
     */
    'formatTime': function(dateObject)
    {
        
        var now = new Date();
        
        var month = ['10367', '10368', '10369', '10370', '10371', '10372', '10373', '10374', '10375', '10376', '10377', '10378'];
        var date = dateObject.getDate() + ". " + Language.getLanguageVariable(month[dateObject.getMonth()]) + " " +  dateObject.getFullYear();
        var time = dateObject.toLocaleTimeString().toLowerCase();
        
        if(dateObject.getDate() == now.getDate() && dateObject.getMonth() == now.getMonth() && dateObject.getFullYear() == now.getFullYear())
        {
            
            // "Today"
            date = Language.getLanguageVariable('p58d2c48c7f554');
            
        }
        
        var returnValue = date + ", " + time;
            
        return returnValue;
        
    },
    
    /**
     * Triggered within the process of adding a message, just before pushing it onto the page.
     * You may modify the HTML, for example.
     * 
     * @author Peter Hamm
     * @param Object messageObject
     * @param Object data
     * @returns void
     */
    onAddMessage: function(messageObject, data)
    {
        
    },
    
    /**
     * Adds a message to the list.
     * 
     * @param int identifier A unique message identifier
     * @param int timestamp A UNIX timestamp
     * @param String type Can be 'owner' or 'remote', indicates who sent the message.
     * @param String message The message to be displayed
     * @param String status Must be 'sent' or 'pending'
     * @param bool skipScrolling Optional, set to true to skip the scrolling to the bottom. Default: false
     * @param bool prepend Optional, set to true to prepend the message instead of appending it. Default: false
     * @param String imageAddress Optional, an image of the sender.
     * @param object data Optional, message data
     * @return void
     */
    'addMessage': function(identifier, timestamp, type, message, status = 'sent', skipScrolling = false, prepend = false, imageAddress = '', data = null)
    {
        
        var dateObject = new Date(timestamp * 1000);
        var formattedTime = dateObject.toGMTString();
        var timePassed = this.getFormattedTimeString(dateObject);
        
        var userName = type == "remote" ? Language.getLanguageVariable('p586eae0617158') : this.userData.name;
        
        var newMessage = this.getDummyMessage();
        
        if(imageAddress && imageAddress.length > 0)
        {
            
            newMessage.find('.user-color-header').css({
                
                'display': 'inline-block',
                'background-image': "url('" + imageAddress + "')"
                
            });
            
        }
        
        newMessage.attr('data-message-identifier', identifier);
        newMessage.addClass(type == 'remote' ? 'row-right' : 'row-left');
        
        newMessage.find('.chat-content p').html(message);
        newMessage.find('.chat-info .username').html(userName);
        
        newMessage.find('.comment-date .datetime').attr('date-time', timestamp);
        newMessage.find('.comment-date .datetime').attr('title', formattedTime);
        newMessage.find('.comment-date .datetime').html(timePassed);
        
        if(status == 'sent')
        {
            
            newMessage.find('.status-pending').css('display', 'none');
            newMessage.find('.status-sent').css('display', 'inline-block');
            
        }
        
        {
            
            if(data == null)
            {
                
                data = {
                    
                    'creator_name': this.userData['creator_name'],
                    'image': imageAddress
                    
                };
                
            }
            
            VmosoChat.onAddMessage(newMessage, data);
    
        }
        
        if(prepend)
        {
            
            jQuery(this.chatContainerId).prepend(newMessage);
            
        }
        else
        {
            
            jQuery(this.chatContainerId).append(newMessage);
            
        }
        
        this.handleDisplayEmptyLog();
        
        if(!skipScrolling)
        {
                    
            jQuery('#loading-box').css('display', 'none');
            
            this.scrollToBottom();
            
        }
        
        this.onWindowResize();
        
    },
    
    /**
     * Scrolls the chat window to the latest message.
     * 
     * @author Peter Hamm
     * @return void
     */
    'scrollToBottom': function()
    {
     
        var scroller = jQuery(VmosoChat.chatScrollerItem);
        
        var scrollTarget = scroller.prop("scrollHeight");
                
        scroller.animate({
            
            scrollTop: scrollTarget 
        
        }, "slow");
        
    },
    
    /**
     * Updates the data for the retrieval of older chat messages
     * 
     * @author Peter Hamm
     * @param Object nextPageData
     * @returns void
     */
    'updateNextPageData': function(nextPageData)
    {
      
        VmosoChat.nextPageData = nextPageData;
        
        VmosoChat.updateDisplayLoadOlderMessages();
        
    },
    
    'updateDisplayLoadOlderMessages': function()
    {
        
        if(VmosoChat.nextPageData && VmosoChat.nextPageData['c_next'])
        {
            
            jQuery(VmosoChat.loadMoreMessagesBoxId).css('display', 'block');
            
        }
        else
        {
            
            jQuery(VmosoChat.loadMoreMessagesBoxId).css('display', 'none');
            
        }
        
    },
    
    'loadOlderMessages': function()
    {
        
        if(VmosoChat.blockLoadOlderMessages)
        {
            
            return;
            
        }
        
        VmosoChat.blockLoadOlderMessages = true;
        VmosoChat.blockRefresh = true;
        
        jQuery('#button-load-older-messages').css('display', 'none');
        jQuery('#load-older-messages-loading-indicator').css('display', 'inline-block');
        
        var sendData = {
            
            'do': 'getMessages',
            'page': VmosoChat.nextPageData
            
        };
        
        if(VmosoChat.chatKey && VmosoChat.chatKey.length > 0)
        {
            
            sendData['key'] = VmosoChat.chatKey;
            
        }
        
        jQuery.post(this.controller, sendData, function(data) {
                     
            var decodedData = jQuery.parseJSON(data);
            
            if(decodedData.success)
            {
                
                // We're prepending the messages, reverse the order for that.
                decodedData.list.reverse();
                
                VmosoChat.updateMessagesWithData(decodedData.list, true);
                                
                VmosoChat.updateNextPageData(decodedData['page_next']);
                
                toastr.success(Language.getLanguageVariable('p58d2cf02ca425'));
                
            }
            else
            {
                         
                VmosoChat.throwError(decodedData);

                return;
                
            }
            
            VmosoChat.blockRefresh = false;
            VmosoChat.blockLoadOlderMessages = false;

            jQuery('#button-load-older-messages').css('display', 'inline-block');
            jQuery('#load-older-messages-loading-indicator').css('display', 'none');

        });
        
    },
    
    /**
     * Will be fired just before the messages of the currently 
     * opened chat are updated, may be overridden.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'onRefreshLatestMessages': function()
    {
                
    },
    
    /**
     * Makes an AJAX call to load the latest messages.
     * 
     * @author Peter Hamm
     * @return void
     */
    'refreshLatestMessages': function()
    {
        
        if(VmosoChat.blockRefresh)
        {
            
            // Do not refresh now, delay it

            setTimeout(function() {
                
                VmosoChat.refreshLatestMessages();
                
            }, VmosoChat.chatMessageUpdateInterval);
            
            return;

        }
        
        VmosoChat.onRefreshLatestMessages();
        
        var sendData = {
            
            'do': 'getMessages'
            
        };
        
        if(VmosoChat.chatKey && VmosoChat.chatKey.length > 0)
        {
            
            sendData['key'] = VmosoChat.chatKey;
            
        }
        
        jQuery.post(this.controller, sendData, function(data) {
                     
            var decodedData = jQuery.parseJSON(data);
            
            if(decodedData.success)
            {
                
                VmosoChat.updateMessagesWithData(decodedData.list);
                
                if(VmosoChat.nextPageData == null)
                {
                    
                    VmosoChat.updateNextPageData(decodedData['page_next']);
                    
                }
                
            }
            else
            {
                     
                VmosoChat.throwError(decodedData);

                return;
                
            }
            
            setTimeout(function() {
                
                VmosoChat.refreshLatestMessages();
                
            }, VmosoChat.chatMessageUpdateInterval);

        });
                
    },
    
    /**
     * Updates or adds messages.
     * 
     * @author Peter Hamm
     * @param Array messageList
     * @param bool prepend Optional, will prepend the messages instead of appending them. Default: false
     * @return void
     */
    'updateMessagesWithData': function(messageList, prepend)
    {
        
        VmosoChat.connectionEstablished = true;
        
        for(var i = 0; i < messageList.length; ++i)
        {
            
            var existingMessage = jQuery(this.chatContainerId).find('[data-message-identifier="' + messageList[i]['identifier'] + '"]');
            
            if(existingMessage.length > 0)
            {
                
                // The message is in the list already, update the details.

                existingMessage.find('.status-pending').css('display', 'none');
                existingMessage.find('.status-sent').css('display', 'inline-block');
                                
                existingMessage.find('.datetime').html(this.getFormattedTimeString(new Date(messageList[i]['date'] * 1000)));
                
                existingMessage.find('.chat-content p').html(messageList[i]['content']);
                
            }
            else
            {
                
                // The message was not in the list yet, add it to the list
                
                var messageSource = messageList[i]['type'] == 'user' ? 'user' : 'remote';
                var skipScrolling = prepend || (i + 1) < messageList.length;
                                
                VmosoChat.addMessage(messageList[i]['identifier'], messageList[i]['date'], messageSource, messageList[i]['content'], 'sent', skipScrolling, prepend, messageList[i]['image'], messageList[i]);
                
            }
            
        }
        
        VmosoChat.handleDisplayEmptyLog();
        VmosoChat.handleButtonStatus();
        
    },
    
    /**
     * Retrieves the number of displayed messages.
     * 
     * @author Peter Hamm
     * @returns int
     */
    'getDisplayedMessageCount': function()
    {
        
        var count = jQuery(VmosoChat.chatContainerId).find('.chat-message-row').length;
        
        return count;
        
    },
    
    /**
     * Handles displaying the default message if no chat messages are available yet.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'handleDisplayEmptyLog': function()
    {
      
        var messageCount = VmosoChat.getDisplayedMessageCount();
        
        jQuery('#chat-box-empty').css('display', (messageCount == 0 ? 'block' : 'none'));
        jQuery('#chat-log').css('display', (messageCount == 0 ? 'none' : 'block'));
        jQuery('#loading-box').css('display', 'none');
                
    },
    
    /**
     * Handles the functionality status of the buttons that control the chat,
     * e.g. the "Send message" button or "New Chat" button.
     * 
     * @author Peter Hamm
     * @returns int
     */
    'handleButtonStatus': function()
    {
      
        var messageCount = VmosoChat.getDisplayedMessageCount();
        
        var displayCreateNew = messageCount > 0 && VmosoChat.connectionEstablished;
        var displaySend = VmosoChat.connectionEstablished;
                
        jQuery('#button-chat-new').attr('disabled', !displayCreateNew);
        jQuery('#button-message-send').attr('disabled', !displaySend);
                
    },
    
    /**
     * Sets up the user's parameters for this chat.
     * 
     * @author Peter Hamm
     * @param String userName The name to be displayed for the local chat user
     * @param String userImage The image to be displayed for the local chat user
     * @return void
     */
    'setup': function(userName, userImage)
    {
        
        jQuery(window).resize(VmosoChat.onWindowResize);
        
        this.userData.name = userName;
        this.userData.image = userImage;
        
        this.focusMessageBox();
        
        jQuery(VmosoChat.loadMoreMessagesBoxId).click(function() {
            
            VmosoChat.loadOlderMessages();
            
        });
        
        VmosoChat.initToastr();
        
    },
    
    /**
     * Initializes the toastr messages for the chat.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'initToastr': function()
    {
        
        toastr.options = {
            
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "showDuration": 330,
            "hideDuration": 330,
            "timeOut": 2000,
            "extendedTimeOut": 2000,
            "showEasing": "swing",
            "hideEasing": "swing",
            "showMethod": "slideDown",
            "hideMethod": "slideUp"
            
        };
        
    },
    
    /**
     * Handles the resizing of the chat window.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'onWindowResize': function()
    {
        
        var scroller = jQuery(VmosoChat.chatScrollerItem);
        var logListScroller = jQuery(VmosoChat.chatArchiveListContainer);
        
        if(logListScroller.length == 0)
        {
            
            logListScroller = jQuery(VmosoChat.chatLogContainer);
            
        }
        
        var newHeight = 0;
        
        {
            
            var scrollerOffsetTop = parseInt(scroller.offset().top);
            var scrollerListOffsetTop = parseInt(logListScroller.offset().top);;
            var footerHeight = parseInt(jQuery('.custom-footer').css('height'));
            
            var marginBottom = 0;//parseInt(jQuery(Calendar.containerId).find('.card').css('margin-bottom'));
            
            if(jQuery('#form-send-message').length > 0)
            {
                
                marginBottom += parseInt(jQuery('#form-send-message').css('height')) + parseInt(jQuery('#form-send-message').css('margin-bottom'));
                
            }

            var newHeight = jQuery(window).height() - scrollerOffsetTop - footerHeight - marginBottom - 26;
            newHeight = newHeight < VmosoChat.layoutMinimumHeight ? VmosoChat.layoutMinimumHeight : newHeight;
            
            var newHeightScroller = jQuery(window).height() - scrollerListOffsetTop - footerHeight - 26;
            newHeightScroller = newHeightScroller < VmosoChat.layoutMinimumHeight ? VmosoChat.layoutMinimumHeight : newHeightScroller;
            
        }
        
        scroller.css('max-height', newHeight);
        logListScroller.css('max-height', newHeightScroller);
    
    },
    
    /**
     * Puts the cursor focus on the chat message box.
     * 
     * @author Peter Hamm
     * @return void
     */
    'focusMessageBox': function()
    {
        
        var messageBox = jQuery('#chat-message-form');
        
        messageBox.focus();
        
    },
    
    /**
     * Retrieves a new message identifier, e.g. when sending
     * a new message that's gonna be pending for a while.
     * 
     * @auhtor Peter Hamm
     * @return int
     */
    'getNewMessageIdentifier': function()
    {
        
        var newIdentifier = 1;
        
        var latestMessage = jQuery(this.chatContainerId).find('.chat-message-row').last();
        
        if(latestMessage)
        {
                        
            newIdentifier = parseInt(jQuery(latestMessage[0]).attr('data-message-identifier')) + 1;
                        
        }
        
        if(isNaN(newIdentifier))
        {
            
            newIdentifier = 1;
            
        }
                
        return newIdentifier;
        
    },
    
    'throwError': function(data)
    {
        
        VmosoChat.connectionEstablished = false;

        toastr.error(Language.getLanguageVariable('10113'));
        
        VmosoChat.handleButtonStatus();

        return;
        
    },
    
    /**
     * Fired just before a chat message is sent.
     * You may modify the send data or execute other functions.
     * 
     * @author Peter Hamm
     * @param Object sendData
     * @returns void
     */
    'onBeforeSendMessage': function(sendData)
    {
        
    },
    
    /**
     * Sends the message from the form, then clears the form.
     * 
     * @author Peter Hamm
     * @return void
     */
    'sendMessage': function()
    {
        
        var messageBox = jQuery('#chat-message-form');
        var message = nl2br(messageBox.val());
                
        messageBox.val('');
        
        var timestamp = (new Date() / 1000);
        var newIdentifier = this.getNewMessageIdentifier();
                
        this.addMessage(newIdentifier, timestamp, 'user', message, 'pending', false, false, VmosoChat.userData.image);
        
        var sendData = {
            
            'do': 'sendMessage',
            'message': message
            
        };
        
        VmosoChat.onBeforeSendMessage(sendData);
        
        jQuery.post(this.controller, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);
            
            if(!decodedData.success)
            {
                
                VmosoChat.throwError(decodedData);

                return;
                
            }

        });
        
        this.focusMessageBox();
        
    },
    
    /**
     * Deletes all existing chat messages and starts a new instance of the chat.
     * 
     * @author Peter Hamm
     * @return void
     */
    'newChat': function()
    {
        
        if(confirm(Language.getLanguageVariable('p587e50f2a979c')))
        {
            
            VmosoChat.blockRefresh = true;
            
            var sendData = {

                'do': 'archiveChat'

            };

            jQuery.post(this.controller, sendData, function(data) {

                var decodedData = jQuery.parseJSON(data);

                if(decodedData.success)
                {
                    
                    jQuery(VmosoChat.chatContainerId).html('');
                    
                    VmosoChat.nextPageData = null;
                    
                    VmosoChat.handleDisplayEmptyLog();
                    
                    VmosoChat.updateDisplayLoadOlderMessages();

                    VmosoChat.refreshLatestMessages();
                    
                    VmosoChat.buildLogOverview(decodedData['list_logs']);

                }
                else
                {

                    VmosoChat.throwError(decodedData);
                    
                    return;

                }
                
                VmosoChat.blockRefresh = false;

            });
            
        }
        
    },
    
    /**
     * Builds an overview list of active chat entries.
     * 
     * @author Peter Hamm
     * @param Array logList A list of chat entry objects
     * @returns void
     */
    'buildChatOverview': function(chatList)
    {
        
        var listElement = jQuery('#list-chat-log');
                
        if(chatList && chatList.length > 0)
        {
            
            listElement.empty();
            
            for(var i = 0; i < chatList.length; ++i)
            {
                
                var entryClass = "chat-log-entry";
                                
                if(VmosoChat.chatKey.length > 0 && VmosoChat.chatKey == chatList[i]['key'])
                {
                    
                    entryClass += " chat-log-entry-active";
                    
                }
                  
                var element = '<div class="' + entryClass + '" data-id="' + chatList[i]['key'] + '">';
                
                element += '<span class="chat-log-title">' + chatList[i]['name'] + '</span><br/>';
                element += '<span class="chat-log-details">' + Language.getLanguageVariable('p594698c45d99a') + ': ' + chatList[i]['time_updated_formatted'] + '</span><br/>';
                
                element += '</div>';
                
                element = jQuery(element);
                
                element.click(function() {
                    
                    var clickedElement = jQuery(this);
                    
                    var elementId = clickedElement.attr('data-id');
                    
                    location.href = VmosoChat.controllerLog + '?l=' + elementId;
                    
                });
                
                listElement.append(element);
                
            }
           
        }
        
    },
    
    /**
     * Builds an overview list of chat archive entries.
     * 
     * @author Peter Hamm
     * @param Array logList A list of archive entry objects
     * @returns void
     */
    'buildLogOverview': function(logList)
    {
        
        var archiveListElement = jQuery('#list-chat-archive');
        
        if(logList && logList.length > 0)
        {
            
            archiveListElement.empty();
            
            for(var i = 0; i < logList.length; ++i)
            {
           
                var element = '<div class="chat-archive-entry" data-id="' + logList[i]['id'] + '">';
                
                element += '<span class="chat-archive-title">' + logList[i]['title'] + '</span><br/>';
                element += '<span class="chat-archive-details">' + logList[i]['count_messages'] + ' ' + Language.getLanguageVariable('p58d3f06c9369b') + '</span><br/>';
                
                element += '</div>';
                
                element = jQuery(element);
                
                element.click(function() {
                    
                    var clickedElement = jQuery(this);
                    
                    var elementId = clickedElement.attr('data-id');
                    
                    location.href = VmosoChat.controllerLog + '?l=' + elementId;
                    
                });
                
                archiveListElement.append(element);
                
            }
           
        }
        
    },
    
    /**
     * Builds a chat from the stored log data provided.
     * 
     * @author Peter Hamm
     * @param Array logData A list of message objects
     * @returns void
     */
    'buildChatFromLogData': function(logData)
    {
                
        if(logData != null)
        {
            
            VmosoChat.updateMessagesWithData(logData, true);
            
        }    
        
    },
    
    /**
     * Takes the user to the live chat.
     * 
     * @author Peter Hamm
     * @returns void
     */
    'goToLiveChat': function()
    {
        
        location.href = VmosoChat.controller;
        
    }
    
};
