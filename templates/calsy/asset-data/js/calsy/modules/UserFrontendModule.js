
var UserFrontendModule = {
    
    'controller': baseUrl + 'module/calsy_user_frontend/',
    'isInitialized': false
    
};

UserFrontendModule.initCustomFields = function()
{
    
    jQuery('.user-custom-input-button-add').off('click').on('click', function() {
        
        UserFrontendModule.addCustomField();
        
    });
    
    jQuery('.user-custom-input-button-remove').off('click').on('click', function() {
        
        var element = jQuery(this);
        
        UserFrontendModule.removeCustomField(element.attr('data-id'));
        
    });
    
    if(!UserFrontendModule.isInitialized)
    {
        
        jQuery('#user-frontend-default-custom-field-button-save').click(function() {
            
            UserFrontendModule.saveCustomFields();
            
        });
        
        UserFrontendModule.isInitialized = true;
        
    }
    
};

UserFrontendModule.saveCustomFields = function()
{
    
    var sendData = {
        
        'do': 'saveCustomFields',
        'custom_fields': UserFrontendModule.getCustomFields()
        
    };
    
    jQuery('#user-frontend-default-custom-field-button-save').prop('disabled', true);
    
    jQuery.post(UserFrontendModule.controller, sendData, function(data) {
        
        var result = JSON.parse(data);
        
        if(result.success)
        {
            
            toastr.success(result.message);
            
        }
        else
        {
            
            toastr.error(result.message);
            
        }
        
        jQuery('#user-frontend-default-custom-field-button-save').prop('disabled', false);
        
    });
    
    
};

UserFrontendModule.addCustomField = function() 
{
    
    var newId = 'c_' + Perisian.generateUniqueIdentifier();
    
    var newElement = jQuery(jQuery('.user-custom-input-field')[0]).clone();
    
    newElement.find('input').val('');
    newElement.attr('id', 'custom_field_' + newId).attr('data-id', newId);
    newElement.find('.user-custom-input-button-remove').attr('data-id', newId);
    
    jQuery('.user-frontend-default-custom-field-container').append(newElement);
    
    UserFrontendModule.initCustomFields();
    
};

UserFrontendModule.removeCustomField = function(fieldId) 
{
    
    if(jQuery('.user-custom-input-field').length > 1)
    {
        
        jQuery('#custom_field_' + fieldId).remove();
        
    }
    else
    {
        
        jQuery('#custom_field_' + fieldId + ' input').val('');
        
    }
    
};

UserFrontendModule.getCustomFields = function()
{
    
    var data = [];
    
    jQuery('.user-custom-input-field').each(function() {
        
        var element = jQuery(this);
        
        var entry = {
            
            'id': element.attr('data-id'),
            'label': element.find('.user-custom-input-field-label').val()
            
        };
        
        data.push(entry);
        
    });
    
    return data;
    
};
