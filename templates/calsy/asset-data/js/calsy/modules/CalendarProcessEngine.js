
/**
 * Toggles the process engine form for the entry editing form
 * 
 * @author Peter Hamm
 * @param bool hide Optional. Set this to true to go back to the default editing. Default: false
 * @returns void
 */
Calendar.toggleFormProcess = function(hide)
{
    
    if(hide)
    {
        
        Calendar.createNewEventInScope();
        
    }
    else
    {
            
        var sendData = {

            't': Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time"),
            'calendarType': Calendar.calendarType,
            'do': 'createProcess',
            'step': 1

        };

        this.editEntry(sendData, function() {

            jQuery("#calsy_calendar_entry_title").focus();

        });
        
    }
    
};

/**
 * Opens the form to edit a process
 * 
 * @author Peter Hamm
 * @param int timestamp
 * @returns void
 */
Calendar.editProcess = function(eventId)
{
    
    if(Calendar.calendarType == 'user_frontend')
    {
        
        return false;
        
    }
    
    var sendData = {

        'e': eventId,
        'calendarType': Calendar.calendarType,
        'do': 'editProcess',
        'step': 1
        
    };

    this.editEntry(sendData, function() {
        
        jQuery("#calsy_calendar_entry_title").focus();
        
    });
    
};

/**
 * Saves the process entry that is currently being edited.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.saveProcess = function()
{
            
    var timestampBegin = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time");
    var timestampEnd = timestampBegin + 300;
    
    var isSeries = jQuery('#calsy_calendar_entry_is_in_series').attr('checked') == 'checked';
        
    var sendData = {
        
        'do': 'save',
        
        'begin': timestampBegin,
        'end': timestampEnd,
        
        'editId': jQuery('#editId').val(),
        'title': jQuery('#calsy_calendar_entry_title').val(),
        'description': jQuery('#calsy_calendar_entry_description').val(),
        'processIdentifier': jQuery('#calsy_calendar_entry_process_id').val(),
        'isProcess': true,
        
        'seriesData': {
            
            'isSeries': isSeries,
            'interval': isSeries ? jQuery('#calsy_calendar_entry_repetition_interval').val() : 0,
            'intervalEnd': isSeries ? Calendar.getTimestampFromFields("#calsy_calendar_entry_repetition_end") : 0
            
        }
        
    };
        
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#submitButton').attr('disabled', 'disabled');
        jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').attr('disabled', 'disabled');
        
        jQuery.post(this.controller, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);

            if(decodedData.success)
            {
                
                Calendar.cancel();
                Calendar.refreshContainer();
                
                toastr.success(decodedData.message);
                
            }
            else
            {
                
                toastr.warning(decodedData.message);
                
                jQuery('#' + Perisian.containerIdentifier).find('#submitButton').removeAttr('disabled');
                jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').removeAttr('disabled');
                
            }

        });
        
    };
    
};