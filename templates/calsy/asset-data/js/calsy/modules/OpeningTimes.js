var OpeningTimes = {
    
    'controller': baseUrl + "module/calsy_opening_times/"

};

/**
 * Builds a list of opening items.
 * 
 * @author Peter Hamm
 * @param String containerId
 * @param Array timeData
 * @returns void
 */
OpeningTimes.buildOpeningTimesSelector = function(containerId, timeData)
{
            
    if(timeData)
    {
        
        for(var i = 0; i < timeData.length; ++i)
        {
            
            OpeningTimes.addOpeningTime(containerId, timeData[i]);

        }
        
    }
    
};

/**
 * Adds an opening time entry
 * 
 * @author Peter Hamm
 * @param int containerId
 * @param Object timeDataElement
 * @returns void
 */
OpeningTimes.addOpeningTime = function(containerId, timeDataElement)
{
    
    var mainContainer = jQuery(containerId);
    
    var container = mainContainer.find('#calendar-opening-times-container');
    
    var dummyElement = mainContainer.find('#calendar-opening-times-dummy-container').find('.calendar-opening-time-entry');
    var dummyCopy = dummyElement.clone();
    
    dummyCopy.find("#calendar-opening-time-entry-to, #calendar-opening-time-entry-from").inputmask('h:s', {placeholder: 'hh:mm'});
    
    if(timeDataElement)
    {
        
        // Fill it
        
        var from = CalendarSettings.minutesToTimeMaskString(timeDataElement.from);
        var to = CalendarSettings.minutesToTimeMaskString(timeDataElement.to);
    
        dummyCopy.find('#calendar-opening-time-entry-day').val(timeDataElement.day);
        dummyCopy.find('#calendar-opening-time-entry-from').val(from);
        dummyCopy.find('#calendar-opening-time-entry-to').val(to);
        
    }
        
    container.append(dummyCopy);
    
};

/**
 * Saves the opening times items to the database
 * 
 * @author Peter Hamm
 * @returns void
 */
OpeningTimes.saveOpeningTimes = function(containerId)
{
        
    var items = [];
    
    jQuery(containerId).find("#calendar-opening-times-container .calendar-opening-time-entry").each(function() {
        
        var entry = jQuery(this);
        
        var item = {
            
            'day': entry.find('#calendar-opening-time-entry-day').val(),
            'from': CalendarSettings.timeMaskStringToMask(entry.find('#calendar-opening-time-entry-from').val()),
            'to': CalendarSettings.timeMaskStringToMask(entry.find('#calendar-opening-time-entry-to').val())
            
        };
        
        items.push(item);
        
    });
    
    var sendData = {
        
        'do': 'saveOpeningTimes',
        'times': items
        
    };
    
    jQuery.post(OpeningTimes.controller, sendData, function(data) {
        
        var decodedData = jQuery.parseJSON(data);

        if(decodedData.success)
        {

            toastr.success(decodedData.message);

        }
        else
        {

            toastr.warning(decodedData.message);

        }
        
    });
    
};

/**
 * Remoes the opening time from the list
 * 
 * @author Peter Hamm
 * @param bool button
 * @returns void
 */
OpeningTimes.removeOpeningTime = function(button)
{
    
    var element = jQuery(button);
    
    element.closest('.calendar-opening-time-entry').remove();
    
};