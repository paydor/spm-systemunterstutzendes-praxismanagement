
Calendar.controllerBookableTimes = baseUrl + "/bookable_times/overview/";

/**
 * This is gonna get fired after clicking on a day.
 * 
 * @author Peter Hamm
 * @param int targettedTime
 * @returns void
 */
Calendar.handleClickDay = function(targettedTime) 
{
    
    Calendar.newBookableTime(targettedTime);

};

/**
 * This is gonna get fired after clicking on an hourly element.
 * 
 * @author Peter Hamm
 * @param int targettedTime
 * @returns void
 */
Calendar.handleClickHourlyElement = function(targettedTime)
{
    
    Calendar.newBookableTime(targettedTime);
    
};

/**
 * Opens the form to edit a bookable time
 * 
 * @author Peter Hamm
 * @param int eventId
 * @returns void
 */
Calendar.handleEditEvent = function(eventId)
{
    
    var sendData = {

        'e': eventId,
        'do': 'editBookableTime',
        'step': 1
        
    };
    
    Calendar.loadInModal(Calendar.controllerBookableTimes, sendData);
    
};

/**
 * Asks the user if he really wants to delete the entry
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.deleteBookableTime = function()
{    
    
    var deleteId = jQuery('#editId').val();
    
    if(confirm(Language.getLanguageVariable(10790)))
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').attr('disabled', 'disabled');
        
        var sendData = {

            'do': 'deleteBookableTime',
            'deleteId': deleteId

        };
        
        jQuery.post(this.controllerBookableTimes, sendData, function(data) {
            
            Calendar.cancel();
            Calendar.refreshContainer();
            
            var decodedData = jQuery.parseJSON(data);
            
            toastr.success(decodedData.message);

        });
        
    }
    
};

/**
 * Opens the form to create a new bookable time entry.
 * 
 * @author Peter Hamm
 * @param int timestamp
 * @return void
 */
Calendar.newBookableTime = function(timestamp)
{
    
    var sendData = {

        't': timestamp,
        'do': 'createBookableTime',
        'step': 1
        
    };
    
    Calendar.loadInModal(Calendar.controllerBookableTimes, sendData);
    
};

/**
 * Saves the entry that is currently being edited.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.saveBookableTime = function()
{
            
    var timestampBegin = Calendar.getTimestampFromFields("#calsy_calendar_date_range_begin_date", "#calsy_calendar_date_range_begin_time");
    var timestampEnd = Calendar.getTimestampFromFields("#calsy_calendar_date_range_end_date", "#calsy_calendar_date_range_end_time");
    
    var isSeries = jQuery('#calsy_calendar_entry_is_in_series').attr('checked') == 'checked';
        
    var sendData = {
        
        'do': 'saveBookableTime',
        'userId': PerisianBaseAjax.getAutocompleteFieldIdentifier('#calsy_calendar_entry_user_id'),
        'editId': jQuery('#editId').val(),
        'title': jQuery('#calsy_calendar_entry_title').val(),
        'description': jQuery('#calsy_calendar_entry_description').val(),
        'areaId': jQuery('#calsy_calendar_entry_area').val(),
        'allowedNumberOfBookings': jQuery('#calsy_calendar_entry_number_bookable').val(),
        'showAnnotiationsPublicEnabled': jQuery('#bookable_time_show_annotiations_public_enabled').is(':checked') ? '1' : '0',
        'bookableTimeImage': jQuery('#image_bookable_time_filename').val(),
        
        'begin': timestampBegin,
        'end': timestampEnd,
        'dst': Calendar.isBrowserInDaylightSavingTime() ? 1 : 0,
        
        'seriesData': {
            
            'isSeries': isSeries,
            'interval': isSeries ? jQuery('#calsy_calendar_entry_repetition_interval').val() : 0,
            'intervalEnd': isSeries ? Calendar.getTimestampFromFields("#calsy_calendar_entry_repetition_end") : 0
            
        }
        
    };
            
    {
        
        jQuery('#' + Perisian.containerIdentifier).find('#submitButton').attr('disabled', 'disabled');
        jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').attr('disabled', 'disabled');
        
        jQuery.post(this.controllerBookableTimes, sendData, function(data) {
                        
            var decodedData = jQuery.parseJSON(data);

            if(decodedData.success)
            {
                
                Calendar.cancel();
                Calendar.refreshContainer();
                
                toastr.success(decodedData.message);
                
            }
            else
            {
                
                toastr.warning(decodedData.message);
                
                jQuery('#' + Perisian.containerIdentifier).find('#submitButton').removeAttr('disabled');
                jQuery('#' + Perisian.containerIdentifier).find('#deleteButton').removeAttr('disabled');
                
            }

        });
        
    };
    
};

/**
 * Shows the settings for the bookable times on the side.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.showBookableTimesSettings = function()
{
    
    Calendar.currentDetailPage = "bookableTimesSettings";
    
    var params = {
        
        do: Calendar.currentDetailPage
        
    };
    
    jQuery(Calendar.containerDetailId).load(Calendar.controllerBookableTimes, params, Calendar.initDetailOnLoad);
    
};

/**
 * Toggles the bookable times to the specified value.
 * 
 * @author Peter Hamm
 * @param bool value
 * @returns void
 */
Calendar.toggleBookableTimes = function(value)
{
    
    var sendData = {
        
        'do': 'updateBookableTimesSettings',
        'enable': value
        
    };
        
    jQuery.post(this.controllerBookableTimes, sendData, function(data) {

        var decodedData = jQuery.parseJSON(data);

        if(decodedData.success)
        {
            
            jQuery('#buttonActivateBookableTimes').css('display', value ? 'block' : 'none');
            jQuery('#buttonDeactivateBookableTimes').css('display', !value ? 'block' : 'none');

            toastr.success(decodedData.message);

        }
        else
        {

            toastr.warning(decodedData.message);

        }

    });
    
};

/**
 * Enables the bookable times.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.enableBookableTimes = function()
{
    
    Calendar.toggleBookableTimes(true);
    
};

/**
 * Disables the bookable times.
 * 
 * @author Peter Hamm
 * @returns void
 */
Calendar.disableBookableTimes = function()
{
    
    Calendar.toggleBookableTimes(false);
    
};