/**
 * Class to handle areas
 *
 * @author Peter Hamm
 * @date 2016-03-29
 */
var Area = jQuery.extend(true, {}, PerisianBaseAjax);

Area.controller = baseUrl + "/area/overview/";

Area.currentSortOrder = 'ASC';
Area.currentSorting = 'calsy_area_title';
Area.areaList = null;
Area.hourList = null;
    
Area.createNewArea = function()
{
    
    Area.editArea();
    
};

/**
 * Retrieves a list of all areas
 * 
 * @author Peter Hamm
 * @returns array
 */
Area.getAreaList = function()
{
    
    if(Area.areaList != null)
    {
        
        return Area.areaList;
        
    }
    
    var sendData = {

        'do': 'getAreaList'
        
    };

    jQuery.ajax({
        
        url: Area.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            Area.areaList = decodedData.list;
                        
        }
        
    });
    
    return Area.areaList;
    
};

Area.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

Area.editArea = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

/**
 * Should be fired when the edit form for an area is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
Area.initEditArea = function(timeElements)
{
        
    var userNotificationSelector = jQuery('#area-user-notification-selector');

    userNotificationSelector.find("li.user-selector").click(function() {

        var checkbox = jQuery(this).find(":checkbox");
        checkbox.prop("checked", !checkbox.prop("checked"))

    });
    
    jQuery('#button-add-new-duration').click(function() {
        
        Area.addNewTimeElement();
        
    });
    
    Area.initTimeElements(timeElements);
    
    jQuery('#calsy_area_title').focus();
    
};

/**
 * Initializes the time elements to be displayed
 * 
 * @author Peter Hamm
 * @param Array timeElements
 * @returns void
 */
Area.initTimeElements = function(timeElements)
{
    
    if(!timeElements || timeElements.length == 0)
    {
        
        timeElements = [
            
            {
                
                'title': '',
                'type': 'block',
                'duration': 300
                
            }
            
        ];
        
    }
    
    jQuery(Area.getTimeElementContainer()).empty();
        
    for(var i = 0; i < timeElements.length; ++i)
    {
        
        var timeElement = timeElements[i];
        
        Area.addNewTimeElement(timeElement);
        
    }
    
};

/**
 * Adds a new time element to the list, with the optional specified data set.
 * 
 * @author Peter Hamm
 * @param Object timeElementData Optional, default: empty
 * @returns void
 */
Area.addNewTimeElement = function(timeElementData)
{
           
    var newElement = '<div class="form-group time-element">';
    
    // Title
    {
           
        var timeElementTitle = timeElementData && timeElementData.title ? unescape(JSON.parse('"' + timeElementData.title + '"')) : "";
        
        newElement += '<div class="col-sm-3">';
        
        newElement += '<label for="time-element-title">' + Language.getLanguageVariable(10848) + '</label>';
        newElement += '<input type="text" id="time-element-title" class="form-control" name="time-element-title" value="' + timeElementTitle + '"/>';
        
        newElement += '</div>';
        
    }
    
    // Duration
    {
        
        newElement += '<div class="col-sm-3" style="padding-left:0">';
        
        newElement += '<label for="time-element-duration">' + Language.getLanguageVariable('p5773f8539141f') + '</label>';
        
        newElement += '<select id="time-element-duration" class="form-control" name="time-element-duration">';
        
        var hourListObjectKeys = Object.keys(Area.hourList);
        
        for(var i = 0; i < hourListObjectKeys.length; ++i)
        {
            
            var seconds = hourListObjectKeys[i];
            var label = Area.hourList[seconds];
            
            var option = '<option value="' + seconds + '"' + (timeElementData && timeElementData.duration == seconds ? ' selected' : '') + '>' + label + '</option>';
            
            newElement += option;
            
        }
        
        newElement += '</select>';
        
        newElement += '</div>';
        
    }
    
    // Type
    {
        
        newElement += '<div class="col-sm-3" style="padding-left: 0; padding-right: 3px;">';
        
        newElement += '<label for="time-element-type">' + Language.getLanguageVariable(11086) + '</label>';
        
        newElement += '<select id="time-element-type" class="form-control" name="time-element-type">';
        
        // The options
        {
            
            newElement += '<option value="block"' + (!timeElementData || (timeElementData && timeElementData.type == 'block') ? ' selected' : '') + '>' + Language.getLanguageVariable('p57741e7d23429') + '</option>';
            newElement += '<option value="free"' + (timeElementData && timeElementData.type == 'free' ? ' selected' : '') + '>' + Language.getLanguageVariable('p57741eb7c35df') + '</option>';
            
        }
        
        newElement += '</select>';
        
        newElement += '</div>';
        
    }
    
    // Sorting buttons
    {
        
        newElement += '<div class="col-sm-3" style="white-space: nowrap; padding-left: 0; padding-top: 29px">';
        
        newElement += '<button class="btn btn-danger button-remove" style="float:right; margin-left:3px;">x</button>';
        newElement += '<button class="btn button-move-up" style="float:right; margin-left:3px;"><i class="fa fa-arrow-up"></i></button>';
        newElement += '<button class="btn button-move-down" style="float:right; margin-left:3px;"><i class="fa fa-arrow-down"></i></button>';
        
        newElement += '</div>';
        
    }
    
    newElement += '</div>';
    
    var newElementWithFunctions = Area.addFunctionalityToTimeElement(newElement);
    
    jQuery(Area.getTimeElementContainer()).append(newElementWithFunctions);
    
};

/**
 * Adds functionality to the specified newly generated time element
 * 
 * @author Peter Hamm
 * @param mixed newElement Either an HTML string or a jQuery object.
 * @returns Object
 */
Area.addFunctionalityToTimeElement = function(newElement)
{
    
    var newElement = jQuery(newElement);
    
    // Removal of this element
    newElement.find('.button-remove').click(function() {
                
        if(newElement.parent().find('.time-element').length == 1)
        {
            
            Area.addNewTimeElement();
            
        }
        
        newElement.remove();
        
    });
    
    newElement.find('.button-move-up').click(function(){
                
        var previousElement = newElement.prev('.time-element');
        
        console.log('prev:');
        console.log(previousElement);
                
        if(previousElement.length == 1)
        {
            
            previousElement.before(newElement);
                        
        }
        
    });
    
    newElement.find('.button-move-down').click(function(){
                
        var nextElement = newElement.next('.time-element');
                        
        if(nextElement.length == 1)
        {
            
            nextElement.after(newElement);
                        
        }
        
    });
    
    return newElement;
    
};

/**
 * Retrieves the container with all of the time elements
 * 
 * @author Peter Hamm
 * @returns Object
 */
Area.getTimeElementContainer = function()
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    return elementContainer.find('#time-element-container');
    
};

/**
 * Retrieves the data from the time elements
 * @returns {undefined}
 */
Area.getTimeElementData = function()
{
    
    var data = [];
    
    var container = Area.getTimeElementContainer();
    
    container.find('.time-element').each(function(){
        
        var timeElement = jQuery(this);
        
        var dataObject = {
            
            'title': timeElement.find('#time-element-title').val(),
            'type': timeElement.find('#time-element-type').val(),
            'duration': timeElement.find('#time-element-duration').val()
            
        };
        
        data.push(dataObject);
        
    });
    
    return data;
    
};

/**
 * Gets the selected users for the currently edited entry
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
Area.getSelectedUsers = function(asCsv)
{
    
    return Area.getSelectedUsersFromSelector('#area-user-selector', asCsv);
    
};

/**
 * Gets the selected users for the notifications for the currently edited entry
 * 
 * @author Peter Hamm
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
Area.getSelectedNotificationUsers = function(asCsv)
{
    
    return Area.getSelectedUsersFromSelector('#area-user-notification-selector', asCsv);
    
};

/**
 * Retrieves the indices of the enabled weekdays for this area.
 * 
 * @author Peter Hamm
 * @returns Array
 */
Area.getWeekdaysEnabled = function() {
  
    return Area.getSelectedUsersFromSelector('#area-weekday-selector', false);
    
};

Area.getGroupsEnabled = function() {
    
    return Area.getSelectedUsersFromSelector('#group-container', false);
    
};

/**
 * Gets the selected users for the currently edited entry
 * 
 * @author Peter Hamm
 * @param String selectorId The identifier of the user selector DOM element.
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
Area.getSelectedUsersFromSelector = function(selectorId, asCsv)
{
    
    var returnValue = [];
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var selectorElement = elementContainer.find(selectorId);
    
    selectorElement.find("li.user-selector").each(function() {
        
        var element = jQuery(this);
        var userId = element.attr('data-id');
        
        var isChecked = element.find(":checkbox").prop("checked");
        
        if(isChecked)
        {
            
            returnValue.push(userId);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

Area.saveArea = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        "users": Area.getSelectedUsers(),
        "timeElements": Area.getTimeElementData(),
        "notificationUsers": Area.getSelectedNotificationUsers(),
        'weekdaysEnabled': Area.getWeekdaysEnabled(),
        'groupsEnabled': Area.getGroupsEnabled()
        
    }, "#" + Perisian.containerIdentifier, "calsy_area_");
            
    if(!Area.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            Area.cancel(); 
            Area.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

Area.deleteArea = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        Area.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

Area.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        Area.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    if(!Validation.checkRequired(sendData.calsy_area_title))
    {

        Area.highlightInputTitleError('calsy_area_title', false);
        
        ++errors;

    }
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
Area.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
Area.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'g': jQuery('#group_selector').val()

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
Area.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};