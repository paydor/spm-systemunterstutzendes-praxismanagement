/**
 * Class to handle quick logs in the frontend
 *
 * @author Peter Hamm
 * @date 2020-07-06
 */
var QuickLogFrontend = jQuery.extend(true, {}, PerisianBaseAjax);

QuickLogFrontend.controller = baseUrl + "/quick_log/overview/";
QuickLogFrontend.controllerRegistration = baseUrl + "/registration/";

QuickLogFrontend.currentSortOrder = 'DESC';
QuickLogFrontend.currentSorting = 'calsy_quick_log_time';
QuickLogFrontend.quickLogList = null;
QuickLogFrontend.filterUserFrontendId = null;

QuickLogFrontend.goToOverview = function()
{
    
    location.href = QuickLogFrontend.controller;
    
};

QuickLogFrontend.doQuickLog = function()
{
    
    var sendData = {
        
        'do': 'new'
        
    };
    
    PerisianBaseAjax.loadInModal(QuickLogFrontend.controller, sendData);
        
};

QuickLogFrontend.reloadPage = function()
{
    
    location.reload();
    
};
    
QuickLogFrontend.createNewQuickLog = function()
{
    
    QuickLogFrontend.editQuickLog();
    
};

/**
 * Retrieves a list of all quick log entries
 * 
 * @author Peter Hamm
 * @returns array
 */
QuickLogFrontend.getQuickLogList = function()
{
    
    if(QuickLogFrontend.quickLogList != null)
    {
        
        return QuickLogFrontend.quickLogList;
        
    }
    
    var sendData = {

        'do': 'getQuickLogList'
        
    };

    jQuery.ajax({
        
        url: QuickLogFrontend.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            QuickLogFrontend.quickLogList = decodedData.list;
                        
        }
        
    });
    
    return QuickLogFrontend.quickLogList;
    
};

QuickLogFrontend.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

QuickLogFrontend.editQuickLog = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

QuickLogFrontend.selectUserFrontend = function(object) 
{

    jQuery('#calsy_quick_log_user_frontend').attr('perisian-data-id', object['calsy_user_frontend_id']);

};

QuickLogFrontend.resetUserFrontend = function() 
{
    
    jQuery('#calsy_quick_log_user_frontend').attr('perisian-data-id', '');

};

/**
 * Should be fired when the edit form for a quick log is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
QuickLogFrontend.initEditQuickLog = function()
{
        
    jQuery('#calsy_quick_log_user_frontend').focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_user_frontend_id',
            'fieldToDisplay': 'calsy_user_frontend_fullname',
            'forceEntryFromList': true,

            'url': QuickLogFrontend.controller,

            'renderer': CalsyAutocompleteRenderer.renderUserFrontend,

            'onSelect': QuickLogFrontend.selectUserFrontend

        });

    });
    
    QuickLogFrontend.initDatePicker();
    
};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
QuickLogFrontend.initDatePicker = function()
{

    var element = jQuery('#modalContainer');

    element.find('#calsy_quick_log_time_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_quick_log_time_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};

QuickLogFrontend.getTypeFields = function()
{
    
    var fields = [];
    
    jQuery('.calsy_quick_log_type_field').each(function() {
        
        var element = jQuery(this);
        
        var data = {
            
            'key': element.attr('data-quick-log-type-field-key'),
            'value': element.find('#calsy_quick_log_type_field_' + element.attr('data-quick-log-type-field-key')).val()
            
        };
        
        fields.push(data);
        
    });
    
    return fields;
    
};

QuickLogFrontend.saveQuickLog = function()
{
    
    var editId = jQuery("#editId").val();
    
    var dstOffset = Calendar.isBrowserInDaylightSavingTime() ? Calendar.getDstOffsetForTimestamp() : 0;
     
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        
        "calsy_quick_log_time": parseInt(Calendar.getTimestampFromFields("#calsy_quick_log_time_field_date", "#calsy_quick_log_time_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds(),
        
        "type_fields": QuickLogFrontend.getTypeFields()
        
    }, "#" + Perisian.containerIdentifier, "calsy_quick_log_");
            
    if(!QuickLogFrontend.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            QuickLogFrontend.cancel(); 
            QuickLogFrontend.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

QuickLogFrontend.deleteQuickLog = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        QuickLogFrontend.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

QuickLogFrontend.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        QuickLogFrontend.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    /*
    if(!Validation.checkRequired(sendData.calsy_area_title))
    {

        Area.highlightInputTitleError('calsy_area_title', false);
        
        ++errors;

    }
    */
   
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
QuickLogFrontend.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
QuickLogFrontend.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'u': QuickLogFrontend.filterUserFrontendId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
QuickLogFrontend.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            
            this.currentSortOrder = 'ASC';
            
        }
        else
        {
            
            this.currentSortOrder = 'DESC';
            
        }
        
    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};