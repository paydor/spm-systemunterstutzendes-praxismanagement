/**
 * Class to handle checkins
 *
 * @author Peter Hamm
 * @date 2020-06-15
 */
var QuickLog = jQuery.extend(true, {}, PerisianBaseAjax);

QuickLog.controller = baseUrl + "/quick_log/overview/";

QuickLog.currentSortOrder = 'DESC';
QuickLog.currentSorting = 'calsy_quick_log_time';
QuickLog.checkInList = null;
QuickLog.filterUserFrontendId = null;
    
QuickLog.createNewQuickLog = function()
{
    
    QuickLog.editQuickLog();
    
};

/**
 * Retrieves a list of all checkins
 * 
 * @author Peter Hamm
 * @returns array
 */
QuickLog.getQuickLogList = function()
{
    
    if(QuickLog.checkInList != null)
    {
        
        return QuickLog.checkInList;
        
    }
    
    var sendData = {

        'do': 'getQuickLogList'
        
    };

    jQuery.ajax({
        
        url: QuickLog.controller,
        data: sendData,
        async: false,
        
        success: function(data) {
            
            var decodedData = jQuery.parseJSON(data);
                        
            QuickLog.checkInList = decodedData.list;
                        
        }
        
    });
    
    return QuickLog.checkInList;
    
};

QuickLog.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

QuickLog.editQuickLog = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

QuickLog.selectUserFrontend = function(object) 
{

    jQuery('#calsy_quick_log_user_frontend').attr('perisian-data-id', object['calsy_user_frontend_id']);

};

QuickLog.resetUserFrontend = function() 
{
    jQuery('#calsy_quick_log_user_frontend').attr('perisian-data-id', '');

};

/**
 * Should be fired when the edit form for a checkin is being opened.
 * 
 * @author Peter Hamm
 * @returns {undefined}
 */
QuickLog.initEditQuickLog = function()
{
        
    jQuery('#calsy_quick_log_user_frontend').focus(function() {

        PerisianBaseAjax.createAutocomplete({

            'element': jQuery(this),

            'fieldToIdentify': 'calsy_user_frontend_id',
            'fieldToDisplay': 'calsy_user_frontend_fullname',
            'forceEntryFromList': true,

            'url': QuickLog.controller,

            'renderer': CalsyAutocompleteRenderer.renderUserFrontend,

            'onSelect': QuickLog.selectUserFrontend

        });

    });
    
    QuickLog.initDatePicker();
    
};

/**
 * Initializes the date pickers
 * 
 * @author Peter Hamm
 * @returns void
 */
QuickLog.initDatePicker = function()
{

    var element = jQuery('#modalContainer');

    element.find('#calsy_quick_log_time_field_date').datepicker({

        language: Language.getLanguageCode(),
        startDate: '+0d',
        todayHighlight: true,
        autoclose: true,

        format: 'dd.mm.yyyy'

    });

    element.find("#calsy_quick_log_time_field_time").inputmask('h:s', {placeholder: 'hh:mm'});

};

QuickLog.getTypeFields = function()
{
    
    var fields = [];
    
    jQuery('.calsy_quick_log_type_field').each(function() {
        
        var element = jQuery(this);
        
        var data = {
            
            'key': element.attr('data-quick-log-type-field-key'),
            'value': element.find('#calsy_quick_log_type_field_' + element.attr('data-quick-log-type-field-key')).val()
            
        };
        
        fields.push(data);
        
    });
    
    return fields;
    
};

QuickLog.saveQuickLog = function()
{
    
    var editId = jQuery("#editId").val();
    
    var dstOffset = Calendar.isBrowserInDaylightSavingTime() ? Calendar.getDstOffsetForTimestamp() : 0;
     
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId,
        
        "calsy_quick_log_user_frontend_id": jQuery('#calsy_quick_log_user_frontend').attr('perisian-data-id'),
        "calsy_quick_log_time": parseInt(Calendar.getTimestampFromFields("#calsy_quick_log_time_field_date", "#calsy_quick_log_time_field_time")) - dstOffset + Calendar.getTimezoneOffsetInSeconds(),
        
        "type_fields": QuickLog.getTypeFields()
        
    }, "#" + Perisian.containerIdentifier, "calsy_quick_log_");
            
    if(!QuickLog.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            QuickLog.cancel(); 
            QuickLog.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

QuickLog.deleteQuickLog = function(deleteId)
{
        
    this.deleteEntry(deleteId, 10305, function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        QuickLog.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
    
};

QuickLog.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        QuickLog.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    /*
    if(!Validation.checkRequired(sendData.calsy_area_title))
    {

        Area.highlightInputTitleError('calsy_area_title', false);
        
        ++errors;

    }
    */
   
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
QuickLog.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
QuickLog.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'search': this.searchTerm,
        'u': QuickLog.filterUserFrontendId

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
QuickLog.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            
            this.currentSortOrder = 'ASC';
            
        }
        else
        {
            
            this.currentSortOrder = 'DESC';
            
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};