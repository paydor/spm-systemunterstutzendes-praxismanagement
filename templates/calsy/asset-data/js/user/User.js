/**
 * Class to handle user management
 *
 * @author Peter Hamm
 * @date 2010-11-20
 */

var User = jQuery.extend(true, {}, PerisianBaseAjax);

User.controller = baseUrl + "user_backend/management/";
User.settingController = baseUrl + "user_backend/settings/";
User.controllerPrivacy = baseUrl + 'user_backend/privacy/',

User.createNewUser = function()
{
    
    this.createNewEntry(function() {jQuery('#user_name').focus();});
    
};

User.editUser = function(editId)
{

    var sendData = {

        'editId' : editId,
        'do': 'edit',
        'step': 1
        
    };

    this.editEntry(sendData, function() {jQuery('#user_name').focus();});

};

User.editOwnPassword = function()
{

    var sendData = {

        'do': 'editOwnPassword',
        'step': 1

    };

    this.editEntry(sendData, function() { jQuery('#user_password_1').focus();} );

};

User.newPassword = function(editId)
{

    var sendData = {

        'editId' : editId,
        'do': 'editPassword',
        'step': 1

    };

    this.editEntry(sendData, function() { jQuery('#user_password_1').focus();} );

};

/**
 * Retrieves the indices of the enabled weekdays for this user.
 * 
 * @author Peter Hamm
 * @returns Array
 */
User.getWeekdaysEnabled = function() {
  
    return User.getSelectedDataFromSelector('#user-weekday-selector', false);
    
};

/**
 * Gets the selected data for the specified selector and the currently edited entry
 * 
 * @author Peter Hamm
 * @param String selectorId The identifier of the user selector DOM element.
 * @param bool asCsv If set to true, a CSV string is returned instead of an array
 * @returns Array or String
 */
User.getSelectedDataFromSelector = function(selectorId, asCsv)
{
    
    var returnValue = [];
        
    var selectorElement = jQuery(selectorId);
    
    selectorElement.find("li.user-selector").each(function() {
        
        var element = jQuery(this);
        var userId = element.attr('data-id');
        
        var isChecked = element.find(":checkbox").prop("checked");
        
        if(isChecked)
        {
            
            returnValue.push(userId);
            
        }
        
    });
    
    if(asCsv)
    {
        
        returnValue = returnValue.join(',');
        
    }
    
    return returnValue;
  
};

User.saveBookableWeekdays = function()
{
    
    var sendData = {

        "do" : "saveWeekdaysEnabled",
        "subjectId" : jQuery("#subjectId").val(),
        "step" : 1,
        "weekdaysEnabled" : User.getWeekdaysEnabled()

    };

    jQuery.post(this.settingController, sendData, function(data) {
                
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable(10670));
            
        }
        
    });

};

User.savePassword = function()
{

    var sendData = {

        "do" : jQuery('#editAction').val(),
        "editId" : jQuery("#editId").val(),
        "step" : 1,
        "user_password_1" : jQuery("#user_password_1").val(),
        "user_password_2" : jQuery("#user_password_2").val()

    };

    this.saveEntry(sendData, function(data) { 
        
        return User.validatePassword(data) 
    
    }, null, null, true, false);

};

User.saveReports = function()
{

    var sendData = {

        "do" : "save_edit_reports",
        "editId" : jQuery("#subjectId").val()

    };
    
    jQuery('#form_reports input').each(function() {
        
        var currentElement = jQuery(this);
        
        sendData[currentElement.attr('id')] = currentElement.prop('checked') ? 1 : 0;
        
    });
    
    jQuery.post(this.settingController, sendData, function(data) {
                
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable(10670));
            
        }
        
    });

};

User.saveUserEdit = function()
{

    var sendData = {

        "do": "saveUserEdit",
        "editId": jQuery("#subjectId").val(),
        "step": 1,
        "user_email": jQuery("#user_email").val(),
        "user_phone": jQuery("#user_phone").val(),
        "user_sorting_custom": jQuery("#user_sorting_custom").val()

    };

    if(!User.validate(sendData, true))
    {
        
        return;
        
    }
    
    jQuery.post(this.settingController, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable(10670));
            
        }
        
    });

};

User.saveCalendarSettings = function()
{
        
    var settings = {};
    
    jQuery('#calendar_settings input').each(function() {
        
        var element = jQuery(this);
        var elementId = element.attr('id');
                
        if(elementId.substr(0, 8) == "setting_")
        {
            
            var settingId = elementId.substr(8);
            
            settings[settingId] = element.attr('checked') == "checked" ? 1 : 0;
            
        }
        
    });

    var sendData = {

        "do" : "saveCalendarSettings",
        "editId" : jQuery("#editId").val(),
        "step" : 1,
        "user_color": Perisian.rgbToHex(jQuery('#user_color').css('backgroundColor')),
        "settings": settings

    };
    
    jQuery.post(this.settingController, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable(10670));
            
        }
        
    });

};

User.saveUser = function()
{

    var sendData = {

        "do" : "save",
        "editId" : jQuery("#editId").val(),
        "step" : 1,
        "user_name" : jQuery("#user_name").val(),
        "user_fullname" : jQuery("#user_fullname").val(),
        "user_email" : jQuery("#user_email").val(),
        "user_phone" : jQuery("#user_phone").val(),
        "user_status" : jQuery('#user_status').val(),
        "user_sorting_custom" : jQuery('#user_sorting_custom').val()

    };

    this.saveEntry(sendData, function(data) { 
        
        return User.validate(data) 
    
    }, function() { 
        
        this.dataSent = false; 
        User.showEntries(); 
    
    }, null, true, true);

};

User.saveAssociationId = function()
{

    var sendData = {

        "do" : "save_association_id",
        "editId" : jQuery("#subjectId").val(),
        "associationId" : jQuery("#user_association_id").val()

    };

    this.saveEntry(sendData, null, function(data) { this.dataSent = false; }, null, true, true);

};

User.deleteUser = function(deleteId)
{
    
    this.deleteEntry(deleteId, 10099, function() {
        
        User.showEntries();
        
        toastr.success(Language.getLanguageVariable(10479));
    
    });
    
};

User.validate = function(sendData, edit)
{

    this.highlightInputTitleError('user_name', 'remove');
    this.highlightInputTitleError('user_fullname', 'remove');
    this.highlightInputTitleError('user_email', 'remove');
    
    if(!edit && !Validation.checkRequired(sendData.user_name))
    {

        this.highlightInputTitleError('user_name');
        return false;

    }

    if(!edit && !Validation.checkRequired(sendData.user_fullname))
    {

        this.highlightInputTitleError('user_fullname');
        return false;

    }

    if(!Validation.checkEmail(sendData.user_email))
    {

        this.highlightInputTitleError('user_email');
        return false;

    }


    return true;

};

User.validatePassword = function(sendData)
{

    this.highlightInputTitleError('user_password_1', 'remove');
    this.highlightInputTitleError('user_password_2', 'remove');

    if(!Validation.checkPassword(sendData.user_password_1, sendData.user_password_2, '#password_error_box'))
    {

        this.highlightInputTitleError('user_password_1');
        this.highlightInputTitleError('user_password_2');
        
        return false;

    }

    return true;

};

User.editSettings = function(userId)
{

    location.href = User.settingController + '?u=' + userId;

};

User.privacyDisclosure = function() {

    location.href = User.controllerPrivacy;

};

User.privacyDeleteAccount = function() 
{

    var sendData = {

        'do': 'privacyDeleteAccountConfirmation'

    };

    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');

    elementContainer.load(PerisianBaseSettings.controller, sendData, function (data) {

        Perisian.ajaxBox('show');

    });

};

User.privacyDeleteAccountConfirm = function()
{

    if(confirm(Language.getLanguageVariable('p5aee1f3aef98e')))
    {

        location.href = User.controllerPrivacy + '?do=delete';

    }

};