/**
 * Class to handle missing language variables
 *
 * @author Peter Hamm
 * @date 2016-06-23
 */
var LanguageVariableMissing = jQuery.extend(true, {}, PerisianBaseAjax);

LanguageVariableMissing.controller = baseUrl + "language/missing/";

LanguageVariableMissing.selectImport = function()
{
    
    var sendData = {

        'do': 'selectImport'
        
    };

    this.editEntry(sendData);
    
};

LanguageVariableMissing.startImportFromRemote = function()
{
    
    var sendData = {

        'do': 'import',
        'type': 'remote',
        'server': jQuery('#import_server').val()
        
    };
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    jQuery('.import-selector').css('display', 'none');
    jQuery('.import-loading').css('display', 'block');
    
    jQuery.post(this.controller, sendData, function(data) {
               
        var callbackData = JSON.parse(data);
        
        elementContainer.find('.import-loading').css('display', 'none');
        
        if(callbackData.success)
        {
            
            toastr.success(callbackData.message);
            
            elementContainer.find('.import-success').css('display', 'block');
            
            elementContainer.find('#import-result').html(callbackData.messageExtended);
            
        }
        else
        {
            
            toastr.error(callbackData.message + (callbackData.messageExtended && callbackData.messageExtended.length > 0 ? ' ' + callbackData.messageExtended : ''));
            
            elementContainer.find('.import-selector').css('display', 'block');
            
        }
                        
    });
    
};