/**
 * Class to handle database migrations
 *
 * @author Peter Hamm
 * @date 2016-10-20
 */
var DatabaseMigration = {};

DatabaseMigration.controller = baseUrl + "/database_migration/";

DatabaseMigration.startMigration = function()
{
            
    var sendData = {
        
        "do": "migrate",
        "u": jQuery('#login_name').val(),
        "p": jQuery('#login_password').val()
        
    };
        
    jQuery("#button-start-migration").attr("disabled", "disabled");
    
    jQuery('#update-login').css('display', 'none');
    jQuery('#update-processing').css('display', 'block');
    
    jQuery.post(this.controller, sendData, function(data) {
        
        jQuery('#login_name').val('');
        jQuery('#login_password').val('');
        jQuery("#button-start-migration").removeAttr("disabled");
        
        jQuery('#update-processing').css('display', 'none');
       
        var callback = JSON.parse(data);
        
        DatabaseMigration.setResult(callback);
        
    });

};

DatabaseMigration.setResult = function(data)
{
  
    if(data.success)
    {
        
        jQuery('#update-success').css('display', 'block');

        jQuery('#update-success .alert').html(data.message);

    }
    else
    {
        
        jQuery('#update-error').css('display', 'block');
        
        jQuery('#update-error .alert').html(data.message);

    }  
    
    if(data.debugInfo)
    {
        
        console.log("Debug info:");
        console.log(data.debugInfo);
        
    }
    
};

DatabaseMigration.retryMigration = function()
{
  
    jQuery('#update-success').css('display', 'none');
    jQuery('#update-error').css('display', 'none');
    
    jQuery('#update-login').css('display', 'block');
    
};

DatabaseMigration.finishMigration = function()
{
  
    location.href = baseUrl;
    
};