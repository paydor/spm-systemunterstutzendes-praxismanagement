var LanguageVariable = jQuery.extend(true, {}, PerisianBaseAjax);

LanguageVariable.controller = baseUrl + "/system/language_variables/";
LanguageVariable.showConfirmationOnSave = true;

/**
 * Checks if the current language selection is possible or not
 *
 * @author Peter Hamm
 * @returns void
 */
LanguageVariable.checkSelectedLanguages = function()
{

    var max = jQuery('#languageOne option').length - 1;
    var languageOne = jQuery('#languageOne').prop("selectedIndex");
    var languageTwo = jQuery('#languageTwo').prop("selectedIndex");

    if(languageOne == languageTwo)
    {
        
        if(max > languageTwo)
        {
            
            jQuery('#languageTwo').prop("selectedIndex", (languageTwo + 1));
            
        }
        else if(languageTwo - 1 >= 0)
        {
            
            jQuery('#languageTwo').prop("selectedIndex", (languageTwo - 1));
            
        }

    }
    
    LanguageVariable.showEntries(null, null, true);

};

/**
 * Shows the form to create a new language variable.
 * 
 * @author Peter Hamm
 * @returns void
 */
LanguageVariable.createNewVariable = function()
{
    
    this.createNewEntry();
    
};

/**
 * Shows the form to edit a language variable with the specified identifier.
 * 
 * @author Peter Hamm
 * @param String editId
 * @param Function callback Optional, default: null
 * @returns void
 */
LanguageVariable.editVariable = function(editId, callback)
{

    var sendData = {

        'variableSubId' : editId,
        'do': 'edit',
        'step': 1
        
    };

    this.editEntry(sendData, callback);
    
};

/**
 * Shows a popup to search through the available language variables.
 * As soon as a variable is picked, LanguageVariable.onVariableSelected(selectedId)
 * 
 * @author Peter Hamm
 * @returns void
 */
LanguageVariable.showSearchPopup = function()
{
    
    var container = this.getElementContainer();
    
    var sendData = {
        
        'do': 'listModal'
        
    };
    
    jQuery(container).load(LanguageVariable.controller, sendData, function() {
        
        Perisian.ajaxBox('show');
        
    });
    
};

/**
 * Loads the list of entries in the search modal popup.
 * 
 * @author Peter Hamm
 * @param int offset
 * @returns void
 */
LanguageVariable.loadModalListEntries = function(offset)
{
    
    var container = this.getElementContainer();
    var listContainer = container.find('#modal_list_container');
    
    var sendData = {
        
        'do': 'listModalResults',
        'search': container.find('#search').val(),
        'offset': offset,
        'limit': 10
        
    };
    
    listContainer.load(LanguageVariable.controller, sendData, function() {
        
        listContainer.find('.button-take-over').click(function() {
            
            var button = jQuery(this);
            var variableId = button.attr('data-id');
            
            LanguageVariable.onVariableSelected(variableId);
            
            return false;
            
        });
        
        listContainer.css('display', 'block');
        
    });
    
};

/**
 * Overridable callback
 * 
 * @author Peter Hamm
 * @param int selectedId
 * @returns void
 */
LanguageVariable.onVariableSelected = function(selectedId)
{
    
};

/**
 * Overridable callback, fired when a language variable 
 * is saved after creating or editing.
 * 
 * @author Peter Hamm
 * @param int savedId
 * @returns void
 */
LanguageVariable.onSaveSuccessful = function(savedId)
{
    
};

/**
 * Saves the language variable data.
 * 
 * @author Peter Hamm
 * @returns void
 */
LanguageVariable.saveVariable = function()
{

    var variableDivs = jQuery('div[name="variable"]');
    var obj;

    var editId = jQuery('#editId').val();

    var sendData = {
        
        'languages': Array(),
        'step': 1,
        'values': Array(),
        'do': 'save',
        'editId': editId
        
    };

    jQuery(variableDivs).each(function(i, variableDiv)
    {

        obj = jQuery(variableDiv);
        sendData.languages[i] = obj.find('input[name="variableLanguage"]').val();
        sendData.values[i] = obj.find('textarea[name="variableContent"]').val();

    });

    this.saveEntry(sendData, function(data) { 
        
        return LanguageVariable.validate(data) 
    
    }, function(data) { 
        
        LanguageVariable.onSaveSuccessful(data);
        
        LanguageVariable.showEntries(); 
        
        if(!LanguageVariable.showConfirmationOnSave)
        {
            
            LanguageVariable.cancel();
            
            toastr.success(Language.getLanguageVariable('10669'));
            
        }
    
    }, null, LanguageVariable.showConfirmationOnSave, true);
    
};

LanguageVariable.deleteVariable = function(deleteId)
{
    this.deleteEntry(deleteId, 10073, function() {
        
        LanguageVariable.showEntries();
        
        toastr.success(Language.getLanguageVariable(10428));
    
    });
};

/**
 * Shows the form to edit the language variable in the selected
 * language loop ID.
 *
 * @author Peter Hamm
 * @parameter int loopId The ID of the div containing the textarea for a language
 * @returns void
 */
LanguageVariable.showEditLanguage = function(loopId)
{

    var variableDivs = jQuery('div[name="variable"]');

    jQuery(variableDivs).each(function(i, variableDiv)
    {

        if(jQuery(variableDiv).css('display') != 'none')
        {
            jQuery(variableDiv).hide();
        }
        
    });

    if(variableDivs[loopId])
    {
        var obj = jQuery(variableDivs[loopId]);
        obj.show();
        obj.find('textarea[name="variableContent"]').focus();
    }

};


/**
 * Loads the list, overwritten method
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
LanguageVariable.showEntries = function(offset, limit, initSearch)
{
    
    if(jQuery("#list").length == 0)
    {
        
        return;
        
    }

    if(!offset && !limit)
    {
        
        offset = LanguageVariable.currentOffset;
        limit = LanguageVariable.currentLimit;
        
    }
    else if(offset === true)
    {
        
        offset = 0;
        limit = LanguageVariable.currentLimit;
        
    }

    if(offset < 0 || limit <= 0)
    {
        
        return;
        
    }

    if(initSearch)
    {
        
        LanguageVariable.searchTerm = jQuery('#search').val();
        
    }

    jQuery("#list").load(LanguageVariable.controller, {
        
        'do': 'list',
        'languageOne': jQuery('#languageOne').val(),
        'languageTwo': jQuery('#languageTwo').val(),
        'offset': offset,
        'limit': limit,
        'search': LanguageVariable.searchTerm
        
    }, function() {

        LanguageVariable.currentOffset = offset;
        LanguageVariable.currentLimit = limit;
        
    });

};

LanguageVariable.validate = function(sendData)
{

    //@todo Define this
    return true;

};