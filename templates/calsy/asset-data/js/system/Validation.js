
/**
 * This class provides basic validation functions
 *
 * @author Peter Hamm
 * @date 2010-11-24
 */
var Validation = {
    
    securityPasswordPolicy: 'strict',

    /**
     * Checks if the parameter is a valid email address
     *
     * @author Peter Hamm
     * @parameter String address
     * @returns boolean
     */
    checkEmail: function(address)
    {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if(reg.test(address) == false) {

            if(typeof console != 'undefined')
            {
              console.log(Language.getLanguageVariable(10094));
            }

            return false;
            
        }

        return true;

    },

    /**
     * Assuming that the parameter is a required field,
     * this method checks it for validation
     *
     * @author Peter Hamm
     * @parameter String content
     * @returns boolean
     */
    checkRequired: function(content)
    {
        
        if(!content || content == "")
        {
            
            if(typeof console != 'undefined')
            {
                console.log(Language.getLanguageVariable(10098));
            }

            return false;
            
        }

        return true;

    },

    /**
     * Checks the validation of a password.
     * You need to pass this method two password fields to make sure
     * the user did not make any mistake.
     *
     * @parameter String passOne
     * @parameter String passTwo
     * @parameter Object errorBox Optional
     * @parameter Object errorReference Optional
     * @returns boolean
     */
    checkPassword: function(passOne, passTwo, errorBox, errorReference)
    {

        try
        {

            if(!passOne || !passTwo)
            {
                
                throw Language.getLanguageVariable(10095);
                
            }

            if(passOne != passTwo)
            {
                
                throw Language.getLanguageVariable(10096);
                
            }

            if(passOne.length < 3)
            {
                
                throw Language.getLanguageVariable(10097);
                
            }
            
            var atLeastOneNumber = new RegExp('[0-9]', 'g');
            if(!passOne.match(atLeastOneNumber))
            {
                
                throw Language.getLanguageVariable(10728);
                
            }
            
            var atLeastOneLowerChar = new RegExp('[a-z]', 'g');
            if(!passOne.match(atLeastOneLowerChar))
            {
                
                throw Language.getLanguageVariable(10729);
                
            }
            
            var atLeastOneUpperChar = new RegExp('[A-Z]', 'g');
            if(!passOne.match(atLeastOneUpperChar))
            {
                
                throw Language.getLanguageVariable(10730);
                
            }
            
            if(Validation.securityPasswordPolicy == 'strict')
            {
            
                var atLeastOneSpecialChar = new RegExp('[^0-9a-zA-Z *]', 'g');
                if(!passOne.match(atLeastOneSpecialChar))
                {

                    throw Language.getLanguageVariable(10731);

                }
                
            }
            
            return true;

        }
        catch(error)
        {
            
            if(errorBox != null)
            {

                jQuery(errorBox).css('display', 'block').find('span').html(error);

            }

            if(typeof console != 'undefined')
            {
                
                console.log(error);
                
            }
            
            if(errorReference)
            {
                
                errorReference['message'] = error;
                
            }

            return false;
            
        }
        
        if(errorBox != null)
        {

            jQuery(errorBox).css('display', 'none');

        }
        
    },

    /**
     * Checks if the parametr is a valid number or not
     *
     * @parameter mixed number The number to check
     * @returns boolean
     */
    checkNumber: function(number)
    {

        var reg = /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/;

        if(reg.test(number) == false) {

          if(typeof console != 'undefined')
          {
              console.log(Language.getLanguageVariable(10138));
          }

          return false;
          
        }

        return true;
        
    }

}