/**
 * Class to handle languages
 *
 * @author Peter Hamm
 * @date 2010-11-13
 */
var Language = {

    cache: new Array(),
    
    /**
     * Updates the language variable cache with the values specified, 
     * so that they do not need to be loaded later.
     * 
     * @param Array variableData An array of objects like this: {'id': 10000, 'value': 'Custom value'}
     * @returns void
     */
    updateVariableCache: function(variableData)
    {
        
        for(var i = 0; i < variableData.length; ++i)
        {
            
            Language.storeToCache(variableData[i]['id'], variableData[i]['value']);
            
        }
        
    },

    /**
     * Static method used to load language variables in JS.
     *
     * @author Peter Hamm
     * @global int languageId The currently used language ID
     * @parameter int variableId The ID of the language variable to load
     * @parameter bool ignoreCache If set to true, the cache is ignored when loading.
     * @returns String The language variable
     */
    getLanguageVariable: function(variableId, ignoreCache)
    {

        if(!ignoreCache && Language.cache[languageId] && Language.cache[languageId][variableId])
        {
            
            return this.cache[languageId][variableId];
            
        }

        var result = '';

        jQuery.ajax({
            
            type: 'GET',
            async: false,
            timeout: 5000,
            url: baseUrl + '/language-' + languageId + '-' + variableId,
            
            success: function(data)
            {
                
                result = data;
                
            },
            
            error: function()
            {
                
                result = 'Error';
                
            }
            
        });
        
        Language.storeToCache(variableId, result);

        return result;

    },
    
    /**
     * Stores the specified variable value for the specified variable identifier to the cache.
     * 
     * @author Peter Hamm
     * @param mixed variableId
     * @param String variableValue
     * @returns void
     */
    storeToCache: function(variableId, variableValue)
    {
        
        if(!Language.cache[languageId])
        {
            
            Language.cache[languageId] = new Array();
            
        }

        Language.cache[languageId][variableId] = variableValue;
        
    },
    
    /**
     * Retrieves the global language code, e.g. "en" or "de"
     * 
     * @author Peter Hamm
     * @global String languageCode
     * @returns String
     */
    getLanguageCode: function()
    {
        
        return languageCode;
        
    }

};