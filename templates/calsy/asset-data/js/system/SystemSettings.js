PerisianBaseSettings.controller = baseUrl + '/system/settings/';

var SystemSettings = jQuery.extend(true, {}, PerisianBaseAjax);
SystemSettings.controller = PerisianBaseSettings.controller;

/**
 * Handles the change of the system's default mail service
 * and the displayed related settings.
 * @return {undefined}
 */
SystemSettings.onChangeMailService = function()
{

    var selectedService = jQuery('[name-descriptive="system_default_smtp_service"]').val();

    var isEnabledSmtp = selectedService == 'smtp';
    var isEnabledSendInBlue = selectedService == 'sendinblue';

    jQuery('.system_default_smtp_server-box').css('display', isEnabledSmtp ? 'block' : 'none');
    jQuery('.system_default_smtp_server_port-box').css('display', isEnabledSmtp ? 'block' : 'none');
    jQuery('.system_default_smtp_user-box').css('display', isEnabledSmtp ? 'block' : 'none');
    jQuery('.system_default_smtp_password-box').css('display', isEnabledSmtp ? 'block' : 'none');
    jQuery('.system_default_smtp_server_ssl_active-box').css('display', isEnabledSmtp ? 'block' : 'none');

    jQuery('.system_default_smtp_service_sendinblue_api_key-box').css('display', isEnabledSendInBlue ? 'block' : 'none');

};

/**
 * Resets the system's colors to the default value
 * 
 * @author Peter Hamm
 * @returns void
 */
SystemSettings.resetColors = function() {
    
    if(confirm(Language.getLanguageVariable(11135)))
    {
        
        var sendData = {

            "do" : "resetSystemColors"

        };

        jQuery.post(this.settingController, sendData, function(data) {

            var callback = JSON.parse(data);
            
            if(callback.success)
            {

                toastr.success(callback.message);

                Perisian.reloadStylesheets();
                
                var colorKeys = Object.keys(callback.colors);
                
                for(var i = 0; i < colorKeys.length; ++i)
                {
                    
                    jQuery('#color_' + colorKeys[i]).css('backgroundColor', callback.colors[colorKeys[i]]);
                    
                }

            }
            else
            {

                toastr.error(Language.getLanguageVariable(11134));

            }

        });
        
    }
    
};
/**
 * Saves the specified colors.
 * 
 * @author Peter Hamm
 * @returns void
 */
SystemSettings.saveColors = function() {
    
    var sendData = {

        "do" : "saveSystemColors",
        "backend_color_main": Perisian.rgbToHex(jQuery('#color_main').css('backgroundColor')),
        "backend_color_main_alternative": Perisian.rgbToHex(jQuery('#color_main_alternative').css('backgroundColor')),
        "backend_color_main_alternative_two": Perisian.rgbToHex(jQuery('#color_main_alternative_two').css('backgroundColor')),
        "backend_color_secondary": Perisian.rgbToHex(jQuery('#color_secondary').css('backgroundColor'))

    };
    
    jQuery.post(this.settingController, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
                        
            toastr.success(callback.message);
            
            Perisian.reloadStylesheets();
            
        }
        else
        {
            
            toastr.error(Language.getLanguageVariable(10670));
            
        }
        
    });
    
};

/**
 * Resets an upload to a default value.
 * 
 * @author Peter Hamm
 * @param String settingName
 * @returns void
 */
SystemSettings.resetUpload = function(settingName) {
    
    var sendData = {

        "do" : "resetUpload",
        "setting": settingName

    };
    
    jQuery.post(this.settingController, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
                        
            toastr.success(callback.message);
            
        }
        else
        {
            
            toastr.error(callback.message);
            
        }
        
    });
    
};