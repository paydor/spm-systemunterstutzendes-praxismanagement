var Module = {
    
    languageVariableContainerId: '',
    languageVariableContainerElement: ''
    
};

/**
 * Initializes the inline language variable editing for the specified container identifier.
 * 
 * @author Peter Hamm
 * @param String languageVariableContainerId
 * @returns void
 */
Module.initLanguageVariableEditing = function(languageVariableContainerId)
{
    
    var container = Module.getLanguageVariableContainer(languageVariableContainerId);
        
    container.find('.button-option-edit').click(function() {
        
        var label = jQuery(this).parent().find("input[name='language-variable']");
        label.trigger('click');
    
    });
    
    container.find("[name='language-variable']").click(function() {
        
        var label = jQuery(this);
        
        Module.setLanguageVariableCallbackFields(label);
        
        LanguageVariable.editVariable(label.attr('data-language-id'));
    
    });
    
};

/**
 * Retrieves the language variable container for the specified identifier.
 * @param String languageVariableContainerId
 * @returns void
 */
Module.getLanguageVariableContainer = function(languageVariableContainerId)
{
    
    languageVariableContainerId = languageVariableContainerId.replace('#', '');
    
    if(languageVariableContainerId.length > 0 || Module.languageVariableContainerId != languageVariableContainerId)
    {
    
        Module.languageVariableContainerElement = jQuery("#" + languageVariableContainerId);
        Module.languageVariableContainerId = languageVariableContainerId;
        
    }
    
    return Module.languageVariableContainerElement;
    
};

/**
 * Sets the language variable editing callback to the specified label.
 * 
 * @author Peter Hamm
 * @param Object label
 * @returns void
 */
Module.setLanguageVariableCallbackFields = function(label)
{
  
    LanguageVariable.onSaveSuccessful = function(savedLanguageId)
    {
                
        label.attr('data-language-id', savedLanguageId);
        label.val(Language.getLanguageVariable(savedLanguageId, true));
        
    }
    
};