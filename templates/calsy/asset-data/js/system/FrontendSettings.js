PerisianBaseSettings.controller = baseUrl + 'frontend/settings/';

var FrontendSettings = jQuery.extend(true, {}, PerisianBaseAjax);
FrontendSettings.controller = PerisianBaseSettings.controller;

/**
 * Resets an upload to a default value.
 * 
 * @author Peter Hamm
 * @param String settingName
 * @returns void
 */
FrontendSettings.resetUpload = function(settingName) {
    
    var sendData = {

        "do" : "resetUpload",
        "setting": settingName

    };
    
    jQuery.post(this.settingController, sendData, function(data) {
        
        var callback = JSON.parse(data);
        
        if(callback.success)
        {
                        
            toastr.success(callback.message);
            
        }
        else
        {
            
            toastr.error(callback.message);
            
        }
        
    });
    
};