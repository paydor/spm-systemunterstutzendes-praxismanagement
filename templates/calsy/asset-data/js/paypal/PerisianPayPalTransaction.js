
/**
 * Class to handle PayPal transactions
 *
 * @author Peter Hamm
 * @date 2017-10-17
 */
var PerisianPayPalTransaction = jQuery.extend(true, {}, PerisianBaseAjax);

PerisianPayPalTransaction.controller = baseUrl + "/paypal/overview/";
PerisianPayPalTransaction.editController = baseUrl + "/paypal/edit/";

PerisianPayPalTransaction.currentSortOrder = 'DESC';
PerisianPayPalTransaction.currentSorting = 'paypal_tx_id';

PerisianPayPalTransaction.filterUserFrontendId = 0;

PerisianPayPalTransaction.goToOverview = function()
{
    
    PerisianBaseAjax.goToUrl(this.controller);
    
};

PerisianPayPalTransaction.editTransaction = function(editId)
{
    
    var sendData = {

        'editId' : editId,
        'do': (!editId || editId.length == 0) ? 'new' : 'edit',
        'step': 1
        
    };

    this.editEntry(sendData);
    
};

PerisianPayPalTransaction.saveTransaction = function()
{
    
    var editId = jQuery("#editId").val();
        
    var sendData = PerisianBaseAjax.enhanceSaveDataWithFields({
        
        "do": "save",
        "editId": editId
        
    }, "#" + Perisian.containerIdentifier, "calsy_area_");
            
    if(!PerisianPayPalTransaction.validate(sendData))
    {
        
        return;
        
    }
    
    // Disable the save button
    jQuery("#button-save").attr("disabled", "disabled");
    
    jQuery.post(this.controller, sendData, function(data) {
       
        var callback = JSON.parse(data);
        
        if(!callback.success)
        {
                        
            var message = Language.getLanguageVariable(10597);
            
            if(callback.message)
            {
                
                message = callback.message;
                
            }
            
            toastr.warning(message);
                        
        }
        else
        {
            
            toastr.success(Language.getLanguageVariable(10669));
            
            this.dataSent = false; 
            
            PerisianPayPalTransaction.cancel(); 
            PerisianPayPalTransaction.showEntries(); 
                        
        }
        
        jQuery("#button-save").removeAttr("disabled");
        
    });

};

PerisianPayPalTransaction.deleteTransaction = function(deleteId)
{
    
    this.deleteEntry(deleteId, 'p59e6678a51dbe', function(data) {
        
        if(data == "error")
        {
            
            toastr.error(Language.getLanguageVariable(10502));
            
            return;
            
        }
        
        Order.showEntries();
        
        toastr.success(Language.getLanguageVariable('p59e667a7df512'));
    
    });
    
};

PerisianPayPalTransaction.validate = function(sendData)
{
    
    var element = jQuery("#" + Perisian.containerIdentifier);
    var elementContainer = element.find('.modal-content');
    
    var errors = 0;
    
    jQuery(elementContainer).find('input, select').each(function() {
        
        var currentElement = jQuery(this);
        
        PerisianPayPalTransaction.highlightInputTitleError(currentElement.attr('id'), 'remove');
        
    });
    
    /*
    if(!Validation.checkRequired(sendData.calsy_order_title))
    {

        Order.highlightInputTitleError('calsy_order_title', false);
        
        ++errors;

    }
    */
    
    return errors == 0;

};
    
/**
 * Use this for example to highlight fields with validation errors
 *
 * @author Peter Hamm
 * @returns void
 */
PerisianPayPalTransaction.highlightInputTitleError = function (fieldName, action)
{

    var element = jQuery("#" + fieldName).parent();

    if(!action)
    {
        
        console.log("Error in field " + fieldName);
        
        element.addClass('has-error');
        
    }
    else if(action == 'remove')
    {
        
        element.removeClass('has-error');
       
    }

};
        
PerisianPayPalTransaction.updateParameters = function()
{
        
    PerisianPayPalTransaction.filterUserFrontendId = PerisianBaseAjax.getAutocompleteFieldIdentifier("#filter_user_frontend_id");
    
};

/**
 * Loads the list
 *
 * @author Peter Hamm
 * @parameter int offset An individual offset of entries >= 0
 * @parameter int limit An individual limit > 0
 * @parameter String sortBy Optional: Which row to sort the list by, default: primary key
 * @parameter String sortOrder Optional: How to sort the list, ASC or DESC, default: DESC
 * @parameter boolean search Optional: Set this to true if you want to perform a new search
 * @returns void
 */
PerisianPayPalTransaction.showEntries = function(offset, limit, sortBy, sortOrder, initSearch)
{

    PerisianPayPalTransaction.updateParameters();

    if(!offset && !limit)
    {
        offset = this.currentOffset;
        limit = this.currentLimit;
    }

    if(!sortBy)
    {
        sortBy = this.currentSorting;
    }

    if(!sortOrder)
    {
        sortOrder = this.currentSortOrder;
    }

    if(offset < 0 || limit <= 0)
    {
        return;
    }

    if(initSearch)
    {
        this.searchTerm = jQuery('#search').val();
    }

    jQuery("#list").load(this.controller, {

        'do': 'list',
        'sorting': sortBy,
        'order': sortOrder,
        'offset': offset,
        'limit': limit,
        'p': PerisianPayPalTransaction.filterUserFrontendId,
        'd': PerisianPayPalTransaction.filterSupplierId,
        'search': this.searchTerm

    }, function() {

        this.currentOffset = offset;
        this.currentLimit = limit;
        this.currentSorting = sortBy;
        this.currentSortOrder = sortOrder;

    });

};

/**
 * Toggles the sorting of the current list
 *
 * @author Peter Hamm
 * @parameter String sortBy DESC or ASC
 * @returns void
 */
PerisianPayPalTransaction.toggleSorting = function(sortBy) 
{

    if(this.currentSorting == sortBy)
    {

        if(this.currentSortOrder == 'DESC')
        {
            this.currentSortOrder = 'ASC';
        }
        else
        {
            this.currentSortOrder = 'DESC';
        }

    }
    else
    {

        this.currentSorting = sortBy;

    }

    this.showEntries();

};

/**
 * Toggles the display state of additional information for the specified order in the table.
 * 
 * @param int orderId
 * @param String state Either "visible" or "hidden"
 * @returns void
 */
PerisianPayPalTransaction.toggleMoreDetails = function(orderId, state) {
    
    var element = jQuery('#table_info_order_' + orderId);
    var buttonToggle = element.find('#button_toggle_details');
    
    if(buttonToggle.hasClass('md-keyboard-arrow-down') || state == "visible")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-down');
        buttonToggle.addClass('md-keyboard-arrow-up');
        
        element.find('.table-info-additional').css('display', 'block');
        
    }
    else if(buttonToggle.hasClass('md-keyboard-arrow-up') || state == "hidden")
    {
        
        buttonToggle.removeClass('md-keyboard-arrow-up');
        buttonToggle.addClass('md-keyboard-arrow-down');
        
        element.find('.table-info-additional').css('display', 'none');
        
    }
    
};