<?php

try
{
    
    require_once 'includes/header.inc.php';
    require_once 'includes/menu.inc.php';
    
    $menu->setActiveMenuPoint(40);
    $page = 'frontend/contact';
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
