<?php

try
{
    
    $disableLoginScreen = true;

    require_once 'includes/header.inc.php';

    if(@$userFrontend && @$userFrontend->isLoggedIn())
    {
        
        require "index.php";
        exit;
        
    }
    
    require_once 'calsy/user_frontend/controller/CalsyUserFrontendRegistrationController.class.php';
        
    $contentController = new CalsyUserFrontendRegistrationController();    
    
    $do = PerisianFrameworkToolbox::getRequest('do');
        
    if(strlen($do) > 0)
    {
           
        if($do == 'registrationAsync')
        {
            
            $contentController->setAction('registrationAsync');
            
        }
        else if($do == 'r')
        {
                        
            $contentController->setAction('accountConfirmation');
            
        }
        else if($do == 'rp')
        {
            
            $contentController->setAction('resetPassword');
            
        }
        else if($do == 'cr')
        {
            
            $contentController->setAction('checkEmailRegistration');
            
        }
        else if($do == 'cl')
        {
            
            $contentController->setAction('checkLoginCredentials');
            
        }
                
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    $dontShowHeaderHtml = true;
    $dontShowFooterHtml = true;
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
