<?php

$isFrontend = true;

$documentRoot = $_SERVER["DOCUMENT_ROOT"] . $_SERVER["PROJECT_DOCUMENT_ROOT"];
$pathHeader = $documentRoot . 'backend/includes/header.inc.php';

require $pathHeader;
unset($pathHeader);

require_once 'modules/CalsyUserFrontendRegistrationAndCalendarModule.class.php';
require_once 'calsy/checkin/controller/CalsyCheckInFrontendController.class.php';
require_once 'calsy/table_reservation/controller/CalsyTableReservationFrontendController.class.php';

if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled() || !CalsyUserFrontendModule::isEnabled())
{
    
    header(PerisianFrameworkToolbox::getConfig('basic/project/charset')); 
    
    require 'index_empty.php';
    require 'includes/footer.inc.php';

    exit;

}

// Check if the page got requested by a logged in backend user.
{
        
    try
    {

        $backendUser = CalsyUserBackend::handleLogin();
        
    }
    catch(Exception $e)
    {

        $backendUser = new CalsyUserBackend();
        
    }
    
}

// Frontend user login, language and charset handling
{
    
    try
    {
        
        $userFrontend = CalsyUserFrontend::handleLogin(PerisianFrameworkToolbox::getRequest('do'));
        
    }
    catch(Exception $e)
    {
        
        // Login was not successful
        $userFrontend = new CalsyUserFrontend();

    }
    
    // Dynamic, in case the frontend user class got extended.
    $userFrontendClass = get_class($userFrontend);

    header(PerisianFrameworkToolbox::getConfig('basic/project/charset')); 
    
    PerisianSystemConfiguration::updateServerFallbackData();
        
    PerisianSystemConfiguration::handleDatabaseMigration();
    
    if(!$userFrontend->isLoggedIn() && !$disableLoginScreen)
    {
        
        // Force the login
                
        require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/frontend_folder') . '/login.php';
        
        exit;
        
    }

}