<?php

$pageFormData = CalsyUserFrontendRegistrationAndCalendarModule::getAppointmentAssistantFormFields(CalsyAppointmentField::getFormattedList());

// Check if there was some "direct jump" data specified, 
// in that case we should pre-fill the specified fields.
{
    
    $prefillData = array();
    $targetTabIndex = 0;
            
    if(PerisianFrameworkToolbox::getRequest('action') == 'jump')
    {
        
        $bookableTimeId = (int)PerisianFrameworkToolbox::getRequest('bt');
        
        if($bookableTimeId > 0)
        {
            
            // Take the page form data from the specified bookable time identifier.
            
            $targetTabIndex = 2;
            
            $bookableTimeEntry = new CalsyCalendarEntry($bookableTimeId);
            $bookableTimeAreaId = $bookableTimeEntry->getFlagValue(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID);
            $bookableTimeDate = date("d.m.Y", $bookableTimeEntry->{$bookableTimeEntry->timestamp_begin});
            $bookableTimeTime = date("H:i", $bookableTimeEntry->{$bookableTimeEntry->timestamp_begin});
            
            $prefillData = Array(
                
                'area' => $bookableTimeAreaId,
                'trainingsart' => CalsyAppointmentField::getAreaGroupValueForArea('trainingsart', $bookableTimeAreaId),
                'aa_date_selection_earliest_possible_date' => $bookableTimeDate,
                'aa_date_selection_earliest_possible_time' => $bookableTimeTime,
                'aa_date_selection_earliest_possible_timestamp' => (int)($bookableTimeEntry->{$bookableTimeEntry->timestamp_begin}) + 3600
                
            );
            
            $pageFormData = CalsyUserFrontendRegistrationAndCalendarModule::combineFormFieldsWithPrefillData($targetTabIndex, $pageFormData, $prefillData);

        }
        
        else
        {
            
            // Take the page form data from the specified parameters.
            
            $targetTabIndex = abs(PerisianFrameworkToolbox::getRequest('tabIndex'));
            $prefillData = PerisianFrameworkToolbox::getRequest('data');

            $pageFormData = CalsyUserFrontendRegistrationAndCalendarModule::combineFormFieldsWithPrefillData($targetTabIndex, $pageFormData, $prefillData);
            
        }
                
    }
    
}

if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
{
    
}

$areaModule = new CalsyAreaModule();