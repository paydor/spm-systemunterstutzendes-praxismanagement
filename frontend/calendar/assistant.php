<?php

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/calendar/CalsyCalendar.class.php';

    $menu->setActiveMenuPoint(49);
    $do = PerisianFrameworkToolbox::getRequest('do');
    $entriesPerPage = 25;
        
    if(empty($do))
    {
        
        require '../includes/appointment_assistant.inc.php';

        $page = 'frontend/appointment_assistant/page_include';

    }
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
