<?php

try
{

    require_once 'includes/header.inc.php';
    require_once 'framework/PerisianLanguage.class.php';
    
    {
        
        $do = PerisianFrameworkToolbox::getRequest('do');
        $groupFilter = (int)PerisianFrameworkToolbox::getRequest('ag');

        $dontShowHeaderHtml = true;
        $dontShowFooterHtml = true;

        $showAppointmentAssistant = false;
        
    }

    $redirectAfterLogin = PerisianFrameworkToolbox::getServerVar('REQUEST_URI');
    $redirectAfterLoginNoParams = substr($redirectAfterLogin, 0, strpos($redirectAfterLogin, '?') !== false ? strpos($redirectAfterLogin, '?') : strlen($redirectAfterLogin));
        
    if(substr($redirectAfterLoginNoParams, -6) == 'login/' || substr($redirectAfterLoginNoParams, -6) == 'index/' || $redirectAfterLoginNoParams == '/' || $groupFilter > 0)
    {
        
        // Only consider showing the appointment assistant or checkin landing page if there's no intended redirect.
    
        if(CalsyCheckInFrontendController::checkDisplayLandingPageHook())
        {

            header("Location: " . PerisianFrameworkToolbox::getServerAddress(false) . 'checkin/');

        }
        else if($do == "aa" || (CalsyUserFrontendRegistrationAndCalendarModule::getAppointmentAssistantDisplayStatus() == "big" && $do != "l" && $do != "login" && $do != "logout"))
        {
                        
            $page = 'frontend/appointment_assistant/overview';

            $showAppointmentAssistant = true;

        }
        
    }
    
    if(!isset($page))
    {
        
        // Standard login
        
        $page = 'frontend/registration/login';

        if(strpos($redirectAfterLogin, 'error/') === true)
        {

            require('error.php');

            exit;

        }

        $languageList = PerisianLanguage::getFrontendLanguages();

        $isFailedLoginAttempt = @strlen(PerisianFrameworkToolbox::getRequest('login_password')) > 0;
        
    }
    else
    {
        
        if($showAppointmentAssistant)
        {
            
            require 'includes/appointment_assistant.inc.php';
            
        }
        
    }
    
    $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
    $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";

    // Password policy info
    {

        $passwordPolicy = PerisianValidation::getPasswordPolicy();
        $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();

        $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
        $passwordPolicyInfo['type'] = $passwordPolicy;

    }
    
    $result = array(
        
        'url_image_welcome' => $customLogoUrl,
        'text_gtc' => PerisianFrameworkToolbox::securityRevert(PerisianSystemSetting::getSettingValue('frontend_gtc_registration')),
        'security_policy_password' => $passwordPolicyInfo
        
    );
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
