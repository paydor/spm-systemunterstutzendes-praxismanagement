<?php

if($_SERVER['HTTP_HOST'] == 'termine.triathlonkids.de')
{
    
    $_REQUEST['do'] = isset($_REQUEST['do']) ? $_REQUEST['do'] : "aa";
    
}

try
{
        
    require_once 'includes/header.inc.php';
    require_once 'includes/menu.inc.php';
    
    $menu->setActiveMenuPoint(37);
    $page = 'frontend/index';
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
        
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}