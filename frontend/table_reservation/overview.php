<?php

try
{
    
    if(@$_REQUEST['do'] == 'findTable')
    {
        
        $disableLoginScreen = true;
        
    }

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/table_reservation/controller/CalsyTableReservationFrontendController.class.php';

    //$menu->setActiveMenuPoint(68);
        
    $contentController = new CalsyTableReservationFrontendController();    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
