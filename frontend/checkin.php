<?php

try
{
    
    $disableLoginScreen = true;
    $dontShowHeaderHtml = true;
    $dontShowFooterHtml = true;
        
    require_once 'includes/header.inc.php';
    
    $contentController = new CalsyCheckinFrontendController();
    
    $contentController->setAction('landingPage');
    
    PerisianControllerWeb::handleContent($contentController);
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
        
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}