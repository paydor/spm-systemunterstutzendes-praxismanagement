<?php

$dontShowHeaderHtml = true;
$dontShowFooterHtml = true;

$page = 'frontend/index_empty';

$customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
$customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";