<?php

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/quick_log/controller/CalsyQuickLogFrontendController.class.php';

    $menu->setActiveMenuPoint(71);
        
    $contentController = new CalsyQuickLogFrontendController();    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
