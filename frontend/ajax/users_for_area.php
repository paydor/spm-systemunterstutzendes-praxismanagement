<?php

// Retrieves a formatted list of users for the specified area identifier parameter

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'calsy/area/CalsyArea.class.php';

    $areaId = PerisianFrameworkToolbox::getRequest('a');
    
    $userDummy = new CalsyUserBackend();
    $userList = CalsyAreaUserBackend::getUsersForArea($areaId, false, false);
    
    $formattedUserList = array();
    
    for($i = 0; $i < count($userList); ++$i)
    {
                
        $formattedUser = array(
            
            $userDummy->field_pk => $userList[$i]->{$userDummy->field_pk},
            $userDummy->field_fullname => $userList[$i]->{$userDummy->field_fullname},
            $userDummy->field_color => $userList[$i]->{$userDummy->field_color},
            $userDummy->field_image_profile => CalsyUserBackend::getUserImageProfile($userList[$i]->{$userDummy->field_pk})
            
        );
        
        array_push($formattedUserList, $formattedUser);
        
    }
    
    echo json_encode($formattedUserList);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
