<?php

// Various functions used for the frontend login 

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';

    $requestedUtil = PerisianFrameworkToolbox::getRequest('u');
    $mail = PerisianFrameworkToolbox::getRequest('m');
    
    $account = CalsyUserFrontend::getUserFrontendForEmailAndPassword($mail, '', true);
    
    $result = array(
        
        'success' => false
        
    );
    
    if($requestedUtil == 'm')
    {
        
        // Validation check
        
        $result = array(

            "is_valid" => $account->{$account->field_pk} > 0,
            "is_password_set" => strlen($account->{$account->field_login_password}) > 0,
            "is_active" => $account->{$account->field_registration_status} == 'active'

        );
            
    }
    else if($requestedUtil == 'sp')
    {
        
        $result = array(

            "pl" => $account->{$account->field_pk},
            "strlen_pw" => strlen($account->{$account->field_login_password}) ,
            "status" => $account->{$account->field_registration_status}

        );
        
        // Set the password
        
        if($account->{$account->field_pk} > 0 && strlen($account->{$account->field_login_password}) == 0 && $account->{$account->field_registration_status} == 'inactive')
        {
            
            $sentPassword = PerisianFrameworkToolbox::getRequest('p');
            
            $account->{$account->field_login_password} = CalsyUserBackend::encryptPassword($sentPassword);
            
            $account->save();
            
            $account->sendEmailActivation();
            
            $result['success'] = true;            
            
        }
        
    }
    
    echo json_encode($result);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
