<?php

// Retrieves a frontend user by his e-mail address and password

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';

    $mail = PerisianFrameworkToolbox::getRequest('m');
    $password = PerisianFrameworkToolbox::getRequest('p');
    
    $userFrontend = CalsyUserFrontend::getUserFrontendForEmailAndPassword($mail, $password);
    
    echo json_encode(array("fullname" => $userFrontend->getFullName()));

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
