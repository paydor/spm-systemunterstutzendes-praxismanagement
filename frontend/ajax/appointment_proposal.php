<?php

try
{
    
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    
    require_once 'calsy/calendar/controller/CalsyCalendarAppointmentAssistantController.class.php';
        
    $contentController = new CalsyCalendarAppointmentAssistantController();
        
    if(@$_REQUEST['do'] == 'bookableTimeInfo')
    {
     
        $contentController->setAction('bookableTimeInfo');
        
    }
    else if($_REQUEST['do'] == 'bookableAmountWarning')
    {
        
        $contentController->setAction('bookableAmountWarning');
        
    }
    else if($_REQUEST['do'] == 'getBookableTimeImage')
    {
        
        $contentController->setAction('getBookableTimeImage');
        
    }
    else
    {
    
        $contentController->setAction('appointmentProposal');
    
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}