<?php

try
{
    
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    
    require_once 'calsy/calendar/controller/CalsyCalendarAppointmentAssistantController.class.php';
        
    $contentController = new CalsyCalendarAppointmentAssistantController();
        
    if(@$_REQUEST['do'] == 'getTileInfo')
    {
     
        $contentController->setAction('getTileInfo');
        
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}