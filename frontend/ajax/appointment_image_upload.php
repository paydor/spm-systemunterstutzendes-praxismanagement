<?php

$disableLoginScreen = true;
$dontShowHeader = true;
$dontShowFooter = true;

require_once '../includes/header.inc.php';

/**
 * Handles file uploads
 * 
 * @author Peter Hamm
 * @return void
 */
function actionUpload()
{

    $uploadResult = array(

        'success' => false

    );

    if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') != 1)
    {

        return $uploadResult;

    }

    try
    {
        
        $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);

    }
    catch(Exception $e)
    {

        $uploadResult = array(

            "success" => false,
            "message" => $e->getMessage()

        );

    }

    return $uploadResult;

}

$result = actionUpload();

echo json_encode($result);

require_once '../includes/footer.inc.php';