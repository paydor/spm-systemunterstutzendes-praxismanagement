<?php

try
{
    
    $disableLoginScreen = true;
    $dontShowHeaderHtml = true;
    $dontShowFooterHtml = true;

    require_once 'includes/header.inc.php';
        
    require_once 'calsy/calendar/controller/CalsySurveyInvitationController.class.php';
        
    $contentController = new CalsySurveyInvitationController();    
    PerisianControllerWeb::handleContent($contentController);
        
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
