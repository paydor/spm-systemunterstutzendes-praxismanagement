<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsySupplierController.class.php";

class CalsySupplierControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsySupplierController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsySupplier";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('suppliers_read');
        
        $data = parent::getObject($identifier);

        return $data;
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('suppliers_write');
        
        return $this->updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('suppliers_write');
        
        $dummy = $this->getDatabaseObject();
                
        return parent::updateObject($identifier);
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('suppliers_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('suppliers_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteWithRelations();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('suppliers_read');
        
        $this->setAction('list');
        
        $result = $this->finalize();
                        
        return $result;
        
    }
        
}
