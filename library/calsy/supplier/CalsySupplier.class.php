<?php

/**
 * Suppliers
 *
 * @author Peter Hamm
 * @date 2016-02-04
 */
class CalsySupplier extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_supplier';
    public $field_pk = 'calsy_supplier_id';
    public $field_title = 'calsy_supplier_title';
    public $field_fullname = 'calsy_supplier_fullname';
    public $field_address_street = 'calsy_supplier_address_street';
    public $field_address_street_number = 'calsy_supplier_address_street_number';
    public $field_address_additional = 'calsy_supplier_address_additional';
    public $field_address_zip = 'calsy_supplier_address_zip';
    public $field_address_city = 'calsy_supplier_address_city';
    public $field_address_country = 'calsy_supplier_address_country';
    public $field_contact_phone_number = 'calsy_supplier_contact_phone_number';
    public $field_contact_fax = 'calsy_supplier_contact_fax';
    public $field_contact_email = 'calsy_supplier_contact_email';
    public $field_area = 'calsy_supplier_area';
    public $field_area_additional = 'calsy_supplier_area_additional';
    public $field_office_name = 'calsy_supplier_office_name';
    public $field_responsible_chamber = 'calsy_supplier_responsible_chamber';
    public $field_association = 'calsy_supplier_association';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_fullname', 'field_address_street', 'field_address_additional', 
        'field_address_zip', 'field_address_city', 'field_address_country', 
        'calsy_supplier_contact_email', 'calsy_supplier_contact_email', 'calsy_supplier_area', 'calsy_supplier_area_additional',
        'calsy_supplier_office_name', 'calsy_supplier_responsible_chamber', 'calsy_supplier_association');
    
    // Cache for entries
    private static $entryList = array();
    
    public $formatted_name = 'calsy_supplier_fullname_formatted';
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves a list of suppliers
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getSupplierList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        for($i = 0; $i < count($results); ++$i)
        {
            
            $results[$i][$instance->formatted_name] = static::formatName($results[$i][$instance->field_title], $results[$i][$instance->field_fullname]);
            
        }
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of suppliers
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getSupplierCount($search = "")
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves a suppliers by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsySupplier
     */
    public static function getSupplierById($id)
    {
        
        if(!isset(self::$entryList[$id]))
        {
            
            self::$entryList[$id] = new static($id);
            
        }
        
        return self::$entryList[$id];
        
    }
    
    /**
     * Retrieves a formatted name for this supplier,
     * consisting of the title and the name
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getFormattedName()
    {
        
        return static::formatName($this->{$this->field_title}, $this->{$this->field_fullname});
        
    }
    
    /**
     * Retrieves a formatted name for the specified title and fullname
     * 
     * @author Peter Hamm
     * @param String $title
     * @param String $fullname
     * @return String
     */
    public static function formatName($title, $fullname)
    {
        
        $parts = Array(
            
            $title,
            $fullname,
            
        );
        
        return trim(implode(" ", $parts));
        
    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the current supplier and its relations (e.g. orders and calendar entries connected to them)
     * 
     * @date 2016-02-05
     * @author Peter Hamm
     * @return void
     */
    public function deleteWithRelations()
    {
        
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} <= 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10823));
            
        }
        
        // Delete all related orders
        {
            
            $filter = CalsyOrder::createFilter(0, $this->{$this->field_pk});

            $relatedOrders = CalsyOrder::getOrderList($filter);
            $dummyOrder = new CalsyOrder();

            for($i = 0; $i < count($relatedOrders); ++$i)
            {

                $order = new CalsyOrder($relatedOrders[$i][$dummyOrder->field_pk]);
                $order->deleteWithRelations();

            }
        
        }
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
