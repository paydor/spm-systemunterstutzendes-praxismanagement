<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'framework/import/PerisianDataImport.class.php';
require_once 'modules/CalsyUserFrontendModule.class.php';
require_once 'calsy/order/CalsyOrder.class.php';
require_once 'calsy/quick_log/CalsyQuickLog.class.php';
require_once 'external/PHPExcel/PHPExcel.php';

/**
 * Frontend user controller
 *
 * @author Peter Hamm
 * @date 2016-11-09
 */
class CalsyUserFrontendController extends PerisianController
{
    
    /**
     * Retrieves the possible import types.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected function getImportTypesEnabled()
    {
        
        $types = PerisianDataImport::getAllPossibleImportTypes(array(

            'hoefler_customers'

        ));
        
        return $types;
        
    }
    
    /**
     * Displays a form to edit quick log settings for a frontend user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionQuickLogSettings()
    {
          
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/quick_log_settings'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $userFrontendId = $this->getParameter('u');
        
        $rightsAvailable = CalsyQuickLog::getTypeRights();
        $rightsSelected = CalsyQuickLog::getTypeRightsForUserFrontend($userFrontendId);
                
        $result = Array(
            
            'title_form' => str_replace("%1", CalsyUserFrontend::getFullnameForUserById($userFrontendId), lg('p5f03475fc248a')),
            'rights_available' => $rightsAvailable,
            'rights_selected' => $rightsSelected,
            'user_frontend_id' => $userFrontendId
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves quick log settings for the specified frontend user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveQuickLogSettings()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => lg('10484')
            
        );
        
        try
        {
            
            $userFrontendId = $this->getParameter('u');
            $rights = $this->getParameter('rights');
            
            if((int)$userFrontendId <= 0)
            {
                
                throw new PerisianException(lg('10409'));
                
            }
            
            foreach($rights as $rightKey => $rightValue)
            {
                
                CalsyUserFrontendSetting::setSettingValue($userFrontendId, $rightKey, $rightValue);
                
            }
            
            $result['success'] = true;
            $result['message'] = lg('10669');
            
        }
        catch(PerisianException $e)
        {
            
            $result['error'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {

        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/user_frontend/overview'
            
        );
        
        if(CalsyUserFrontendModule::isEnabledDataMerging())
        {
            
            $renderer['page_menu_right'] = 'calsy/user_frontend/menu/merge_right';
            
        }
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyUserFrontend();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_name_last);
        $search = $this->getParameter('search', '');
                        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
        //
        
        $result = array(
            
            'list' => CalsyUserFrontend::getUserFrontendList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyUserFrontend::getUserFrontendCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            
            'import_types_enabled' => $this->getImportTypesEnabled()
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable(10829));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $calendarEntryId = (int)$this->getParameter('calendarEntryId');

        }
                
        $editId = $this->getParameter('editId');
        $editEntry = new CalsyUserFrontend($editId);
        
        $result = array(
            
            'object' => $editEntry,
            'id' => $editId,
            
            'calendar_entry_id' => $calendarEntryId,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable(10827)
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to edit a frontend user's password.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditPassword()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/edit_password'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        // Parameter setup
        {
            
            $editId = (int)$this->getParameter('editId');

        }
        
        // Password policy info
        {
            
            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();
            
            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;
                        
        }
        
        $result = array(
            
            'id' => $editId,
            'title_form' => PerisianLanguageVariable::getVariable(10105),
            'security_policy_password' => $passwordPolicyInfo
            
        );
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Saves a new password for a frontend user
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSavePassword()
    {
        
        $renderer = array();
        
        // Parameter setup
        {
            
            $editId = (int)$this->getParameter('editId');
            $step = (int)$this->getParameter('step');

        }
        
        if($step == 1)
        {
            
            $renderer = array(

                'json' => true

            );
            
            $result = array(

                'success' => false

            );
            
            $passwordOne = $this->getParameter('user_frontend_password_1');
            $passwordTwo = $this->getParameter('user_frontend_password_2');
            
            if(PerisianValidation::checkPassword($passwordOne, $passwordTwo) && PerisianValidation::checkId($editId))
            {
                
                $password = PerisianUser::encryptPassword($passwordOne);

                $editObject = new CalsyUserFrontend($editId);
                $editObject->{$editObject->field_login_password} = $password;
                
                $result = array(

                    'success' => true,
                    'id' => $editObject->save()

                );
                
            }
            else
            {
                
                $result['message'] = PerisianValidation::getLatestExceptionMessage();
                
            }
            
        }
        else if($step == 2)
        {
            
            $renderer = array(

                'blank_page' => true,
                'page' => 'calsy/user_frontend/save'

            );
            
            $title = PerisianLanguageVariable::getVariable(10066);
            $message = PerisianLanguageVariable::getVariable(10106) . ' ' . PerisianLanguageVariable::getVariable(10102);

            if(strlen($editId) > 0)
            {

                $title = PerisianLanguageVariable::getVariable(10107);
                $message = PerisianLanguageVariable::getVariable("p576bfc539f679");

            }
            
            $result = array(

                'title' => $title,
                'message' => $message

            );
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    private function getExportFields($entryList)
    {
        
        $userDummy = new CalsyUserFrontend();
            
        $fields = Array(
            
            Array(
                
                'title' => lg('10078'),
                'field' => $userDummy->field_pk
                
            ),
            
            Array(
                
                'title' => lg('10559'),
                'field' => $userDummy->field_name_last
                
            ),
            
            Array(
                
                'title' => lg('10560'),
                'field' => $userDummy->field_name_first
                
            )
            
        );
        
        $additionalFields = Array();
        
        foreach($entryList as $entry)
        {
            
            $userObject = new CalsyUserFrontend($entry[$userObject->field_pk]);

            foreach($userObject->getCustomFields() as $customField)
            {
                
                for($i = 0; $i < count($additionalFields); ++$i)
                {
                    
                    if((is_array($additionalFields[$i]['field']) && in_array($customField['id'], $additionalFields[$i]['field'])) || $additionalFields[$i]['field'] == $customField['id'])
                    {
                        
                        // Already, in the list, skip.
                        
                        continue 2;
                        
                    }
                    else if($additionalFields[$i]['title'] == $customField['label'])
                    {
                        
                        if(!is_array($additionalFields[$i]['field']))
                        {
                         
                            $additionalFields[$i]['field'] = Array($additionalFields[$i]['field']);
                            
                        }
                        
                        if(!in_array($customField['id'], $additionalFields[$i]['field']))
                        {
                            
                            array_push($additionalFields[$i]['field'], $customField['id']);
                            
                        }
                        
                        continue 2;
                        
                    }
                                        
                }
                                
                $newEntry = Array(
                    
                    'custom_field' => true,
                    'title' => $customField['label'],
                    'field' => $customField['id']
                    
                );
                
                array_push($additionalFields, $newEntry);
                
            }
            
        }
        
        $fields = array_merge($fields, $additionalFields);
                
        return $fields;
        
    }
    
    /**
     * Tries to retrieve the value for the field with the specified key from the specified entry.
     * 
     * @author Peter Hamm
     * @param Array $fieldKey
     * @param Array $entry
     * @return string
     */
    private function getExportFieldValue($fieldData, $entry, CalsyUserFrontend $userObject)
    {
                
        if(@$fieldData['custom_field'])
        {
            
            $customFields = $userObject->getCustomFields();
            
            if(!is_array($fieldData['field']))
            {
                
                $fieldData['field'] = Array($fieldData['field']);
                
            }
            
            foreach($customFields as $customField)
            {
                
                if(in_array($customField['id'], $fieldData['field']))
                {
                    
                    //return $fieldData['title'] . ' -> ' . $userObject->{$userObject->field_pk};
                    
                    return strlen($customField['value']) > 0 ? $customField['value'] : "-";
                    
                }
                
            }
            
        }
        else if(isset($entry[$fieldData['field']]))
        {
            
            return strlen($entry[$fieldData['field']]) > 0 ? $entry[$fieldData['field']] : "-";
            
        }
        
        return "-";
        
    }
    
    /**
     * Provides data to display a form to the data import.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionExport()
    {
        
        $step = $this->getParameter('step');
        $type = $this->getParameter('type');
        
        try
        {
            
            $userDummy = new CalsyUserFrontend();

            $entryList = CalsyUserFrontend::getUserFrontendList("", $userDummy->field_name_last, "ASC", 0, 0);

            $excelFile = new PHPExcel();
            $excelFile->setActiveSheetIndex(0);
            $excelSheet = $excelFile->getActiveSheet();

            $displayedFields = $this->getExportFields($entryList);
            $fieldExportLimit = count($displayedFields);
            
            for($i = 0, $column = "A"; $i < $fieldExportLimit; ++$i)
            {

                // Add the titles at the top of the table.

                $excelSheet->SetCellValue($column . "1", $displayedFields[$i]["title"]);

                $excelSheet->getStyle($column . "1")->getFont()->setBold(true);

                $excelSheet->getColumnDimension($column)->setAutosize(true);

                ++$column;

            }

            for($j = 0; $j < count($entryList); ++$j)
            {

                // Go through every selected entry and add it 
                
                $userObject = new CalsyUserFrontend($entryList[$j][$userDummy->field_pk]);

                for($i = 0, $column = "A"; $i < $fieldExportLimit; ++$i)
                {

                    $excelSheet->SetCellValue($column . ($j + 2), $this->getExportFieldValue($displayedFields[$i], $entryList[$j], $userObject));

                    ++$column;

                }

            }
            
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="calsy-user-export.xlsx"');
            
            $excelWriter = PHPExcel_IOFactory::createWriter($excelFile, "Excel2007");
            $excelWriter->save('php://output');

        } 
        catch(Exception $e) 
        {

            $error = lg(10689);
            $error = str_replace("%", $e->getMessage(), $error);

            throw new PerisianException($error);

        }

        exit;
        
    }
    
    /**
     * Provides data to display a form to the data import.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionImport()
    {
        
        $renderer = array(

            'blank_page' => true,
            'page' => 'calsy/user_frontend/import'

        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'title_form' => PerisianLanguageVariable::getVariable(10491),
            'import_types_enabled' => $this->getImportTypesEnabled()
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     * @throws PerisianException
     */
    protected function actionDelete()
    {
        
        global $user;
                                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        if(!$user->isAdmin())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
            
        }
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyUserFrontend($deleteId);
        $entryObj->delete();
        
        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }    
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
     
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            
        }
        
        $saveElement = new CalsyUserFrontend($editId);
        $saveElement->setObjectDataFromArray($this->getParameters());
        
        $newId = $saveElement->save();
        
        $saveElement->{$saveElement->field_pk} = $newId;
        
        CalsyUserFrontend::updateCustomFieldsForUser($newId, $this->getParameter('fields_custom'));
                
        if($newId > 0)
        {
            
            $result = array(
                
                'success' => true, 
                'newId' => $newId,
                'fullname' => $saveElement->getFullName()
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Retrieves additional data for a frontend user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionAdditionalData()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/additional_data'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $identifier = $this->getParameter('m');
            
        }
        
        $entryObj = new CalsyUserFrontend($identifier);
        
        $additionalDataRaw = json_decode(stripslashes($entryObj->{$entryObj->field_additional_data}));
        
        $additionalData = "";
        
        if(count($additionalDataRaw) > 0)
        {
            
            foreach($additionalDataRaw as $key => $value)
            {

                $additionalData .= "<b>" . $key . "</b>: " . $value . "<br>";

            }
            
        }
        else
        {
            
            $additionalData = "Keine weiteren Daten.";
            
        }
        
        $result = array(
            
            'title_form' => PerisianLanguageVariable::getVariable('p5766b1cf79dc3'),
            'data_additional' => $additionalData
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data to display a history of a user's calendar entries
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetCalendarHistory()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/calendar_history'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!CalsyUserFrontendModule::isEnabledHistory())
        {
            
            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;
            
        }
        
        // Parameter setup
        {
            
            $limit = (int)$this->getParameter('limit', 5);
            
            $userFrontendId = (int)$this->getParameter('userFrontendId');
            $userFrontend = new CalsyUserFrontend($userFrontendId);

        }
                
        // Retrieve the calendar entries
        {
                        
            // Build the filter object
            {

                $filter = new CalsyCalendarFilter();

                $filter->setUserFrontendIdentifiers($userFrontendId);
                
                $filter->setShowUnconfirmed(true);
                $filter->setShowCanceled(true);
                $filter->setIgnoreConfirmationStatus(true);

            }

            $listCalendarEntries = CalsyCalendarEntry::getCalendarEntriesForFilter($filter, -1, -1, '', 'pk', 'DESC', 0, $limit);
                        
        }
        
        $pageTitle = str_replace("%1", $userFrontend->getFullName(), PerisianLanguageVariable::getVariable('p58430670008d9'));
        $pageTitle = str_replace("%2", $limit, $pageTitle);
        
        $result = array(
            
            'id_user_frontend' => $userFrontendId,
                        
            'title_form' => $pageTitle,
            
            'list' => $listCalendarEntries
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to validate a merge
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionValidateMerge()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_frontend/validate_merge'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $mergeIds = $this->getParameter('m');
            
        }
                
        $mergeObjectOne = new CalsyUserFrontend($mergeIds[0]);
        $mergeObjectTwo = new CalsyUserFrontend($mergeIds[1]);
        
        $mergeObjectProposal = CalsyUserFrontend::getMergeProposal($mergeObjectOne, $mergeObjectTwo);
        
        $result = array(
            
            'object_one' => $mergeObjectOne,
            'object_two' => $mergeObjectTwo,
            'object_proposal' => $mergeObjectProposal,
                        
            'title_form' => PerisianLanguageVariable::getVariable('p59c4044767c46')
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Tries to merge two entries, also updates any existing relations.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionMerge()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $dummy = new CalsyUserFrontend();
            
            $mergeIds = $this->getParameter('m');
            $passwordSource = (int)$this->getParameter($dummy->field_login_password . '_merge');
            
            if(count($mergeIds) != 2)
            {
                
                throw new PerisianException('p58c92a7a3f1d4');
                
            }
            
        }
        
        {

            $mergeObjectOne = new CalsyUserFrontend($mergeIds[0]);
            $mergeObjectTwo = new CalsyUserFrontend($mergeIds[1]);

            // Update the first object with the new data
            {

                $mergeObjectOne->setObjectDataFromArray($this->getParameters());
                
                if(strlen($mergeObjectOne->{$mergeObjectOne->field_login_password}) == 0 && strlen($mergeObjectTwo->{$mergeObjectTwo->field_login_password}) > 0)
                {
                    
                    $passwordSource = 1;
                    
                }
                else if(strlen($mergeObjectOne->{$mergeObjectOne->field_login_password}) > 0 && strlen($mergeObjectTwo->{$mergeObjectTwo->field_login_password}) == 0)
                {
                    
                    $passwordSource = 0;
                    
                }
                
                if($passwordSource == 1)
                {
                    
                    // Take the password from the second object
                    $mergeObjectOne->{$mergeObjectOne->field_login_password} = $mergeObjectTwo->{$mergeObjectTwo->field_login_password};
                    
                }
                
            }
            
            // Take over all calendar entries to the first object
            {
                
                CalsyCalendarEntryUserFrontendOrder::moveEntriesByUserFrontendIds($mergeObjectTwo->{$mergeObjectTwo->field_pk}, $mergeObjectOne->{$mergeObjectOne->field_pk});
                
            }
                    
            $mergeObjectTwo->delete();
            $mergeObjectOne->save();
        
        }
        
        $result = array(
            
            'success' => true,
            
            'object_one' => $mergeObjectOne,
            'object_two' => $mergeObjectTwo
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data to display a some basic frontend user info
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetUserFrontendInfoById()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $requestedUserId = (int)$this->getParameter('u');

        }
                
        // Retrieve the calendar entries
        {
            
            $userFrontend = new CalsyUserFrontend($requestedUserId);
         
            $data = Array(
                
                $userFrontend->field_pk => $userFrontend->{$userFrontend->field_pk},
                $userFrontend->field_name_first => $userFrontend->{$userFrontend->field_name_first},
                $userFrontend->field_name_last => $userFrontend->{$userFrontend->field_name_last},
                        
                'count_calendar_entries' => CalsyCalendarEntry::getCountForUserFrontendId($userFrontend->{$userFrontend->field_pk})
                
            );
            
        }
        
        $result = array(
            
            'success' => true,
                        
            'data' => $data
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}