<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';
require_once 'calsy/privacy/CalsyPrivacyAccountDeletionLog.class.php';
require_once 'modules/CalsyVmosoModule.class.php';

/**
 * Controller for frontend users to edit their account settings.
 *
 * @author Peter Hamm
 * @date 2016-12-31
 */
class CalsyUserFrontendAccountController extends PerisianController
{
        
    /**
     * Provides an overview of settings
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionOverview()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'page' => 'frontend/account/settings'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Load the rights
        {
            
            $disabledList = $this->getListSettingsDisabled();
               
            $standardSettingList = $userFrontend->getSettingObject()->getPossibleSettings('', $disabledList);
            $accountSettings = $userFrontend->getSettingObject()->getAssociativeValues();
            
            $languageList = PerisianLanguage::getFrontendLanguages();
            
            $calendarSettingsList = CalsyUserBackend::filterCalendarSettings($standardSettingList);
            
            if(!isset($accountSettings[PerisianSetting::getIdForName('account_calendar_notification_default_time')]))
            {

                // If not set for the frontend user, retrieve the system's default
                $accountSettings[PerisianSetting::getIdForName('account_calendar_notification_default_time')] = PerisianSystemSetting::getSettingValue('calendar_default_time_notifications');

            } 
            
            if(!isset($accountSettings[PerisianSetting::getIdForName('account_calendar_notification_is_enabled')]))
            {

                // If not set for the user, retrieve the system's default
                $accountSettings[PerisianSetting::getIdForName('account_calendar_notification_is_enabled')] = PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications_accounts');

            }

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {
                
                foreach($standardSettingList as $standardSettingListKey => &$standardSettingListEntry)
                {
                    
                    if(isset($accountSettings[$standardSettingListEntry[$userFrontend->getSettingObject()->foreign_field_pk]]))
                    {
                        
                        $standardSettingList[$standardSettingListKey]['value'] = $accountSettings[$standardSettingListEntry[$userFrontend->getSettingObject()->foreign_field_pk]];
                        
                    }

                }
                
                // Add the calendar notification settings to a seperate list
                $calendarNotificationSettingList = PerisianUserSetting::retrieveSettingsByPrefix('account_calendar_notification_', $standardSettingList, true);
                
                // Chat settings
                {
                    
                    $settingsVmosoChat = PerisianUserSetting::retrieveSettingsByPrefix('account_vmoso_chat_', $standardSettingList, true);
                    
                    // Kick out the chat id
                    PerisianUserSetting::retrieveSettingsByPrefix('account_vmoso_chat_id', $settingsVmosoChat, true);
                    
                }
                                                
            }

            // Possible notification times
            {
               
                $calendarNotificationTimes = CalsyCalendarNotification::getPossibleNotificationTimes();
                
            }

        }
        
        $pageDescription = PerisianLanguageVariable::getVariable(10127);
        
        //
        
        $result = array(
            
            'list_languages' => $languageList,
            'list_settings_calendar_notifications' => $calendarNotificationSettingList,
            'list_times_calendar_notification' => $calendarNotificationTimes,
            
            'field_upload_image_profile' => $userFrontend->getImageProfileData()
            
        );
        
        if(CalsyVmosoModule::isCustomChatLayoutForUserFrontendEnabled())
        {
            
            $result['list_settings_vmoso_chat'] = $settingsVmosoChat;
            $result['list_layouts_chat'] = CalsyVmosoModule::getListChatLayouts();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a page to confirm the deletion of a user's account.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionPrivacyDeleteAccountConfirmation()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/account/privacy_delete_account_confirmation'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array();
        
        $this->setResultValue('result', $result);
        
    }
      
    /**
     * Provides a disclosure statement of privacy data.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @global bool $dontShowHeaderHtml
     * @global bool $dontShowFooterHtml
     * @return void
     */
    protected function actionPrivacy()
    {
        
        global $userFrontend;
        global $dontShowHeaderHtml;
        global $dontShowFooterHtml;
        
        $renderer = array(
            
            'page' => 'frontend/account/privacy_disclosure'
            
        );
        
        //
        
        $do = $this->getParameter('do');
        
        $dataUser = $userFrontend->getData($userFrontend->field_pk . ' = ' . $userFrontend->{$userFrontend->field_pk});
        
        $result = array(
            
            'data_disclosure' => $userFrontend->getDataDisclosure(),
            'data_user' => $dataUser[0]
                        
        );   
        
        if($do == 'delete')
        {
            
            $dontShowHeaderHtml = true;
            $dontShowFooterHtml = true;
            
            $renderer = array(

                'page' => 'frontend/account/privacy_delete_account'

            );
            
            // Log the deletion
            {
                
                $deletionData = Array(
                    
                    'type' => get_class($userFrontend),
                    'time' => time(),
                    'deleted' => Array()
                    
                );
                
                foreach($result['data_disclosure'] as $key => $value)
                {
                    
                    $deletionData['deleted'][$key] = $value['count_entries'];
                    
                }
            
                $logEntry = new CalsyPrivacyAccountDeletionLog();
                
                $logEntry->{$logEntry->field_name} = $userFrontend->{$userFrontend->field_email};
                $logEntry->{$logEntry->field_data} = json_encode($deletionData);
                
                $logEntry->save();
                
            }
            
            $userFrontend->delete();
            
        }
        
        // 
                
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @global $user
     * @global $userFrontend
     * @return void
     */
    protected function actionUpload()
    {
        
        global $user;
        global $userFrontend;
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        // Parameter setup
        {
            
            $subjectId = $this->getParameter('subjectId');
        
            if(isset($user) && is_a($user, 'CalsyUserBackend') && ($user->isAdmin() || $user->isDeveloper()))
            {

                $userObj = new CalsyUserFrontend($subjectId);

            }
            else
            {

                $userObj = $userFrontend;

            }
            
        }
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        try
        {

            $resetFile = false;

            if($this->getInternal('reset') === true)
            {

                $uploadResult = array(

                    "success" => true,
                    "message" => PerisianLanguageVariable::getVariable(11132)

                );

                $resetFile = true;

            }
            else
            {

                $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                
            }

            $pathExistingImage = $userObj->{$userObj->field_image_profile};

            PerisianUploadFileManager::deleteFile('image', $pathExistingImage);

            $userObj->{$userObj->field_image_profile} = ($resetFile ? "" : $uploadResult["file"]);
            $userObj->save();

        
        }
        catch(Exception $e)
        {
                       
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Resets uploaded files
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetUpload()
    {
        
        $this->setInternal('reset', true);
        
        $this->actionUpload();
        
    }
    
    /**
     * Retrieves a list of disabled setting.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected function getListSettingsDisabled()
    {
        
        $list = array();
        
        return $list;
        
    }
    
    /**
     * Changes the language for the frontend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionChangeLanguage()
    {
        
        global $userFrontend;
     
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'success' => false
            
        );
        
        try
        {
            
            $userFrontend->{$userFrontend->field_language_id} = $this->getParameter('lg');
            
            $userFrontend->save();
            
            $result['success'] = true;
            
        }
        catch(PerisianException $e)
        {
            
        }

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves the settings
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionSave()
    {
        
        global $userFrontend;
     
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $disabledList = $this->getListSettingsDisabled();
        
        $settingObj = new PerisianSetting();
        
        $standardSettingList = $userFrontend->getSettingObject()->getPossibleSettings();
        
        $settingList = array_merge($standardSettingList);
        $newValues = Array();

        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }

        foreach($possibleSettings as $possibleSettingId)
        {
                        
            if(isset($this->parameters['s_' . $possibleSettingId]) && !in_array($possibleSettingId, $disabledList))
            {

                $newObj = new CalsyUserFrontendSetting($possibleSettingId, $userFrontend->{$userFrontend->field_pk});
                $newObj->{$newObj->field_setting_value} = $this->getParameter('s_' . $possibleSettingId);
                $newObj->save();
                
            }

        }
        
        //
        
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}