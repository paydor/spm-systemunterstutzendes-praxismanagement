<?php

require_once 'framework/abstract/PerisianController.class.php';

/**
 * Main frontend user registration controller 
 *
 * @author Peter Hamm
 * @date 2017-03-07
 */
class CalsyUserFrontendRegistrationController extends PerisianController
{
    
    private $isAsync = false;
    
    /**
     * Registers and logs in a new frontend user, asynchronously.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionRegistrationAsync()
    {
        
        $this->isAsync = true;
        
        return $this->actionOverView();
        
    }
    
    /**
     * Registers a new frontend user
     * 
     * @author Peter Hamm
     * @return void
     */ 
    protected function actionOverview()
    {
        
        $valid = true;
        $error = PerisianLanguageVariable::getVariable(10643);

        $registration_name_first = $this->getParameter('registration_name_first');
        $registration_name_last = $this->getParameter('registration_name_last');
        $registration_gender = $this->getParameter('registration_gender');
        $registration_email = $this->getParameter('registration_email');
        $registration_phone = $this->getParameter('registration_phone');
        $registration_pw_one = $this->getParameter('registration_pw_one');
        $registration_pw_two = $this->getParameter('registration_pw_two');
        
        $gtcText = PerisianFrameworkToolbox::securityRevert(PerisianSystemSetting::getSettingValue('frontend_gtc_registration'));

        $action = (strlen(@$registration_name_first) > 0 || $this->isAsync ? "save" : "register");
        
        // Password policy info
        {
            
            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();
            
            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;
                        
        }

        if($action == "save")
        {

            if(strlen($registration_name_first) < 2)
            {

                $valid = false;
                $error .= "<br/>" . PerisianLanguageVariable::getVariable(10874);

            }

            if(strlen($registration_name_last) < 2)
            {

                $valid = false;
                $error .= "<br/>" . PerisianLanguageVariable::getVariable(10874);

            }      

            if(strlen($registration_email) < 5)
            {

                $valid = false;
                $error .= "<br/>" . PerisianLanguageVariable::getVariable(10094);

            }

            if(!PerisianValidation::checkPassword($registration_pw_one, $registration_pw_two))
            {

                $valid = false;
                $error .= "<br/>" . PerisianValidation::getLatestExceptionMessage();

            }

        }

        if($action == "save" && $valid)
        {
            
            $isFromTableReservation = $this->getParameter('tr', false) == '1';

            // The registration data seems to be okay so far,
            // try to save the new frontend user (may still fail)

            $newUserFrontend = new CalsyUserFrontend();

            $newUserFrontend->{$newUserFrontend->field_name_first} = $registration_name_first;
            $newUserFrontend->{$newUserFrontend->field_name_last} = $registration_name_last;
            $newUserFrontend->{$newUserFrontend->field_sex} = $registration_gender;
            $newUserFrontend->{$newUserFrontend->field_login_password} = PerisianUser::encryptPassword($registration_pw_one);
            $newUserFrontend->{$newUserFrontend->field_email} = $registration_email;
            $newUserFrontend->{$newUserFrontend->field_phone} = $registration_phone;
            $newUserFrontend->{$newUserFrontend->field_language_id} = PerisianSystemConfiguration::getLanguageId();
            $newUserFrontend->{$newUserFrontend->field_registration_status} = "inactive";
            $newUserFrontend->{$newUserFrontend->field_created} = PerisianFrameworkToolbox::sqlTimestamp();
            
            if($isFromTableReservation) 
            {
                
                $newUserFrontend->{$newUserFrontend->field_registration_status} = 'active';
                
            }

            try
            {

                $result = $newUserFrontend->save();

                $newUserFrontend->{$newUserFrontend->field_pk} = $result;

            }
            catch(Exception $e)
            {

                unset($result);

            }

            if(!@$result)
            {

                $error .= "<br/>" . PerisianLanguageVariable::getVariable(10875);

                $valid = false;

            }
            else
            {

                try
                {

                    if(!$isFromTableReservation)
                    {
                        
                        $newUserFrontend->sendEmailActivation();
                    
                    }
                    
                }
                catch(Exception $e)
                {
                    
                    // The activation mail could not be sent.

                }

            }

        }

        if($action == "register" || ($action == "save" && !$valid))
        {

            // No data entered - or it's invalid data: 
            // Provide data for the registration form
            
            $providedEmail = $this->getParameter('m');

            $registration_email = strlen($providedEmail) > 0 ? $providedEmail : $registration_email;

            $page = 'frontend/registration/registration';

        }
        else if($action == "save" && $valid)
        {
            
            // The registration was successful:
            // Provide data for the success screen

            $page = 'frontend/registration/registration_success';

            $title = PerisianLanguageVariable::getVariable(10647);
            $message = PerisianLanguageVariable::getVariable(10648);

        }
        
        // Result building
        {
            
            $result = array(

                'id' => $newUserFrontend->{$newUserFrontend->field_pk},
                'success' => $valid,
                'error' => !$valid,
                'valid' => $valid,
                'url_image_welcome' => static::getUrlWelcomeImage(),
                'text_gtc' => $gtcText,
                'security_policy_password' => $passwordPolicyInfo

            );
            
            if(isset($title))
            {
                
                $result['title'] = $title;
                
            }
            
            if(isset($message))
            {
                
                $result['message'] = $message;
                
            }
            
            if(isset($registration_email))
            {
                
                $result['registration_email'] = $registration_email;
                
            }
            
            if(isset($registration_phone))
            {
                
                $result['registration_phone'] = $registration_phone;
                
            }
            
            if(isset($registration_name_first))
            {
                
                $result['registration_name_first'] = $registration_name_first;
                
            }
            
            if(isset($registration_name_last))
            {
                
                $result['registration_name_last'] = $registration_name_last;
                
            }
            
            if(isset($providedEmail))
            {
                
                $result['email_provided'] = $providedEmail;
                
            }
            
            if(!$result['valid'])
            {
                
                $result['message'] = $error;
                
            }
            
        }
        
        if($this->isAsync)
        {
            
            echo json_encode($result);
            
            exit;
            
        }
        
        $this->setResultValue('result', $result);
        
        //
        
        $renderer = array(
            
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
    }
    
    /**
     * Tries to confirm an account and activate it.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     */
    protected function actionAccountConfirmation()
    {
        
        $renderer = array(
            
            'page' => 'frontend/registration/registration_success'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $code = $this->getParameter('c');

        $result = array(

            'success' => false

        );
        
        if(@$code && strlen($code) > 5)
        {
            
            // The user clicked on the link in an e-mail that was sent to him.
            
            $activationDummy = new CalsyUserFrontendActivation();
            
            $activation = CalsyUserFrontendActivation::getByCode($code);
                        
            $userFrontend = null;
            
            if($activation[$activationDummy->field_fk])
            {
                
                $userFrontend = new CalsyUserFrontend($activation[$activationDummy->field_fk]);
                
            }
            
            if(@$userFrontend)
            {
                
                $title = PerisianLanguageVariable::getVariable(10649);
                $message = PerisianLanguageVariable::getVariable(10650);

                $userFrontend->{$userFrontend->field_registration_status} = "active";
                $userFrontend->save();

                $forgottenEntry = new CalsyUserFrontendActivation();
                $forgottenEntry->deleteByUserId($userFrontend->{$userFrontend->field_pk});
                
            }
            else
            {
                
                // The code is not valid anymore
                
                $link = "Location: " . PerisianFrameworkToolbox::getServerAddress(false);
                header($link);
                
            }

            $result = array(

                'success' => true,
                'title' => $title,
                'message' => $message

            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Recovery and changing of passwords.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @returns void
     */
    protected function actionResetPassword()
    {
                
        $recoveryEmail = $this->getParameter('rp_email');
        $providedEmail = $this->getParameter('m');
        $code = $this->getParameter('c');
        
        $success = true;
        $result = Array();
        
        // Password policy info
        {
            
            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();
            
            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;
                        
        }
        
        if(@$code && strlen($code) > 5)
        {
            
            // Provide data to display a form to reset the password of the user.
            // This can either be related to user having forgotten his password,
            // or to initially setting up a password for the user account.
            
            $result['initial'] = $this->getParameter('i');
            $result['is_initial'] = @$result['initial'] == "1";
            $result['code'] = $code;
            
            $forgottenDummy = new CalsyUserFrontedPasswordForgotten();
            
            $passwordForgotten = CalsyUserFrontedPasswordForgotten::getByCode($code);
            
            $userFrontend = null;
            
            if($passwordForgotten[$forgottenDummy->field_fk])
            {
                
                $userFrontend = new CalsyUserFrontend($passwordForgotten[$forgottenDummy->field_fk]);
                
            }
                 
            if($userFrontend)
            {
                                
                $setPasswordOne = $this->getParameter('rp_pw_one');
                $setPasswordTwo = $this->getParameter('rp_pw_two');
                
                if(PerisianValidation::checkPassword($setPasswordOne, $setPasswordTwo))
                {
                    
                    $title = PerisianLanguageVariable::getVariable(10633);
                    $message = PerisianLanguageVariable::getVariable(10634);
                    
                    $page = 'frontend/registration/registration_success';
                    
                    $userFrontend->{$userFrontend->field_login_password} = PerisianUser::encryptPassword($setPasswordOne);
                    $userFrontend->save();
                    
                    $forgottenEntry = new CalsyUserFrontedPasswordForgotten();
                    $forgottenEntry->deleteByUserId($userFrontend->{$userFrontend->field_pk});
                    
                }
                else
                {
                    
                    $lastError = PerisianValidation::getLatestExceptionMessage();
                    
                    if(strlen($lastError) > 0 && (strlen($setPasswordOne) > 0 || strlen($setPasswordTwo) > 0))
                    {
                        
                        $success = false;
                        $result['error_message'] = $lastError;
                        
                    }
                    
                    $page = 'frontend/registration/password_set';
                    
                }
                
            }
            else
            {
                
                // The code is not valid anymore
                
                $link = "Location: " . PerisianFrameworkToolbox::getServerAddress(false);
                header($link);
                
            }
            
        }
        else
        {
            
            $message = PerisianLanguageVariable::getVariable(10702);

            if(!@$recoveryEmail || strlen($recoveryEmail) < 3)
            {

                $page = 'frontend/registration/password_forgotten';

            }
            else
            {

                $success = true;
                $errorLanguageId = 10624;

                $page = 'frontend/registration/registration_success';

                try
                {

                    $userFrontend = new CalsyUserFrontend();

                    $userFrontend->getUserFrontendByEmail($recoveryEmail);
                    
                    if(strlen($userFrontend->{$userFrontend->field_email}) >= 3)
                    {
                        
                        try
                        {
                            
                            $success = $userFrontend->sendEmailPasswordForgotten();
                        
                        } 
                        catch(Exception $e) 
                        {
                            
                            throw new PerisianException($e->getMessage());

                        }

                        if(!$success)
                        {

                            $errorLanguageId = 10625;

                        }

                    }
                    else
                    {

                        $success = false;

                    }

                }
                catch(PerisianException $e)
                {

                    $success = false;
                    $message = $e->getMessage();

                }

                $title = PerisianLanguageVariable::getVariable($success ? 10622 : 10623);
                $message = $success ? PerisianLanguageVariable::getVariable(10621) : $message;

            }
            
        }
        
        // Result building
        {
            
            $result['success'] = $success;
            $result['url_image_welcome'] = static::getUrlWelcomeImage();
            $result['email_provided'] = $providedEmail;
            $result['security_policy_password'] = $passwordPolicyInfo;
            
            if(isset($title))
            {
                
                $result['title'] = $title;
                
            }
            
            if(isset($message))
            {
                
                $result['message'] = $message;
                
            }
                    
            
        }
        
        $this->setResultValue('result', $result);
        
        //
        
        $renderer = array(
            
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Checks whether an email address is already registered or not.
     * 
     * @author Peter Hamm
     * @returns void
     */
    protected function actionCheckEmailRegistration()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $mail = $this->getParameter('m');
        
        $result = array(
            
            "result" => false,
            "message" => ""
            
        );
        
        $result["result"] = CalsyUserFrontend::getCountForEmail($mail) == 0;
        $result["message"] = PerisianLanguageVariable::getVariable($result["result"] ? 11036 : 10875);
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Checks whether the specified login data is valid or not.
     * 
     * @author Peter Hamm
     * @returns void
     */
    protected function actionCheckLoginCredentials()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $mail = $this->getParameter('m');
        $password = $this->getParameter('p');
        
        $result = array(
            
            "result" => false,
            "message" => ""
            
        );
        
        try
        {
            
            $result["result"] = CalsyUserFrontend::isValidLogin($mail, $password);

        }
        catch(PerisianException $e)
        {
         
            $result["result"] = false;
            
        }
        
        $result["message"] = PerisianLanguageVariable::getVariable($result["result"] ? 11036 : 11037);
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves the URL to the custom logo of the system
     * 
     * @author Peter Hamm
     * @return String
     */
    protected static function getUrlWelcomeImage()
    {
        
        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        return $customLogoUrl;
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}