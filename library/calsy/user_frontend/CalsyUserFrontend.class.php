<?php

require_once "calsy/order/CalsyOrder.class.php";
require_once "modules/CalsyUserFrontendRegistrationAndCalendarModule.class.php";
require_once "calsy/calendar/CalsyCalendarEntry.class.php";
require_once "calsy/calendar/CalsyCalendarEntryUserFrontendOrder.class.php";
require_once 'framework/settings/PerisianDependentSetting.class.php';
require_once 'framework/mail/PerisianMail.class.php';
require_once 'framework/abstract/PerisianUserRegistrationAbstract.class.php';
require_once 'framework/PerisianUser.class.php';

/**
 * Frontend user
 *
 * @author Peter Hamm
 * @date 2016-01-29
 */
class CalsyUserFrontend extends PerisianDatabaseModel
{
    
    const SETTING_VALUE_SYSTEM_CUSTOM_FIELDS = 'module_setting_calsy_user_frontend_custom_fields';
    const SETTING_VALUE_USER_CUSTOM_FIELDS = 'user_frontend_custom_fields';
    
    const DEFAULT_USER_ID = '-1337';
    
    // Main table settings
    public $table = 'calsy_user_frontend';
    public $field_pk = 'calsy_user_frontend_id';
    public $field_name_first = 'calsy_user_frontend_name_first';
    public $field_name_last = 'calsy_user_frontend_name_last';
    
    public $field_external_id = 'calsy_user_frontend_external_id';
    public $field_additional_data = 'calsy_user_frontend_additional_data';
    public $field_fax = 'calsy_user_frontend_fax';
    public $field_phone = 'calsy_user_frontend_phone';
    public $field_phone_mobile = 'calsy_user_frontend_phone_mobile';
    public $field_email = 'calsy_user_frontend_email';
    public $field_email_contact = 'calsy_user_frontend_email_contact';
    public $field_login_password = 'calsy_user_frontend_login_password';
    public $field_language_id = 'calsy_user_frontend_language_id';
    public $field_sex = 'calsy_user_frontend_sex';
    public $field_address_street = 'calsy_user_frontend_address_street';
    public $field_address_street_number = 'calsy_user_frontend_address_street_number';
    public $field_address_additional = 'calsy_user_frontend_address_additional';
    public $field_address_zip = 'calsy_user_frontend_address_zip';
    public $field_address_city = 'calsy_user_frontend_address_city';
    public $field_address_country = 'calsy_user_frontend_address_country';
    public $field_image_profile = 'calsy_user_frontend_image_profile';
    public $field_registration_status = 'calsy_user_frontend_registration_status';
    public $field_last_login = 'calsy_user_frontend_last_login';
    public $field_created = 'calsy_user_frontend_created';
    
    public $fullname = 'calsy_user_frontend_fullname';
    
    protected $searchFields = Array("field_pk", "CONCAT_WS(' ', calsy_user_frontend_name_first, calsy_user_frontend_name_last)", "calsy_user_frontend_phone",
        "calsy_user_frontend_fax", "calsy_user_frontend_email", "calsy_user_frontend_address_street", "calsy_user_frontend_address_additional",
        "calsy_user_frontend_address_zip", "calsy_user_frontend_address_city");
    
    protected $loginSuccessful = false;
    protected $loginSuccessfulOverride = false;
    
    protected $userFrontendSetting = null;
    
    private $customFieldList = null;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            if($entryId == static::DEFAULT_USER_ID)
            {

                $this->{$this->field_pk} = $entryId;
                $this->{$this->field_name_first} = PerisianLanguageVariable::getVariable('p5b03118c5ff01');
                $this->{$this->field_name_last} = '';
                $this->{$this->field_sex} = 'male';
                $this->{$this->field_image_profile} = $this->getImageProfileAddress();
                
            }
            else
            {

                $query = "{$this->field_pk} = '{$entryId}'";
                $this->loadData($query);

                $this->{$this->fullname} = $this->getFullName();

            }
            
        }

        return;

    }
        
    /**
     * Retrieves a list of all fields that are safe to edit for the frontend user himself.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getSafeFields()
    {
        
        $userFrontendObject = new static();
        
        $fieldExcludes = Array(

            'field_pk',
            'field_email',
            'field_email_contact',
            'field_external_id',
            'field_additional_data',
            'field_login_password',
            'field_language_id',
            'field_image_profile',
            'field_registration_status',
            'field_last_login',
            'field_created'

        );

        $validFields = $userFrontendObject->getFieldVars($fieldExcludes);
        
        return $validFields;
        
    }
    
    /**
     * Retrieves this user's custom fields.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getCustomFields()
    {
        
        if(!is_array($this->customFieldList))
        {

            $defaultCustomFieldList = $this->getCustomFieldsDefault();
            $filledCustomFieldList = $this->getCustomFieldValues();

            $customList = static::combineCustomFieldDefaultWithValues($defaultCustomFieldList, $filledCustomFieldList);

            if(count($customList) == 0)
            {

                $entry = Array(

                    'id' => uniqid('c_'),
                    'label' => '',
                    'value' => ''

                );

                array_push($customList, $entry);

            }
            
            $this->customFieldList = $customList;

        }
                
        return $this->customFieldList;
        
    }
    
    /**
     * Updates custom field data for the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @param Array $customFieldData
     * @return void
     */
    public static function updateCustomFieldsForUser($userId, $fieldData)
    {
        
        $saveElement = new static($userId);
        
        $saveElement->updateCustomFields($fieldData);
        
    }
    
    /**
     * Updates this user's custom field data.
     * 
     * @author Peter Hamm
     * @param Array $customFieldData
     * @return void
     */
    protected function updateCustomFields($customFieldData)
    {
        
        $validFields = Array();
        
        foreach($customFieldData as $customField)
        {
			
			$id = @$customField['id'];

            $fieldId = PerisianFrameworkToolbox::security($id);

            if(strlen($fieldId) == 0)
            {
                
                continue;
                
            }
            
            array_push($validFields, $customField);
            
        }
        
        $encodedFields = json_encode($validFields);
        
        $this->setSettingValue(static::SETTING_VALUE_USER_CUSTOM_FIELDS, $encodedFields);
        
    }
    
    /**
     * Combines default custom fields with values provided.
     * 
     * @author Peter Hamm
     * @param Array $defaultList
     * @param Array $valueList
     * @return Array
     */
    protected static function combineCustomFieldDefaultWithValues($defaultList, $valueList)
    {
        
        $result = $defaultList;
        
        foreach($valueList as $filledEntry)
        {
            
            foreach($result as &$existingEntry)
            {
                
                if($existingEntry['id'] == $filledEntry['id'])
                {
                    
                    $existingEntry['label'] = $filledEntry['label'];
                    $existingEntry['value'] = $filledEntry['value'];
                    
                    continue 2;
                    
                }
                
            }
            
            // Was not found in the default, add the whole field to the list.
            array_push($result, $filledEntry);
            
        }
        
        return $result;
        
    }
    
    /**
     * Retrieves a list of custom fields that each user has by default.
     * 
     * @todo Make this configurable
     * @author Peter Hamm
     * @return Array
     */
    protected function getCustomFieldsDefault()
    {
        
        $list = Array();
        
        $data = PerisianSystemSetting::getSettingValue(static::SETTING_VALUE_SYSTEM_CUSTOM_FIELDS);

        if(strlen($data) > 0)
        {
            
            $list = json_decode(($data), true);
            
            foreach($list as &$entry)
            {
                
                $entry['value'] = '';
                
            }
            
        }

        return $list;
        
    }
    
    /**
     * Retrieves all of the custom fields for this user.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getCustomFieldValues()
    {
        
        $decodedData = Array();
        
        if($this->{$this->field_pk} > 0)
        {
            
            $data = $this->getSettingValue(static::SETTING_VALUE_USER_CUSTOM_FIELDS);

            $decodedData = json_decode(stripslashes($data), true);

        }
        
        return $decodedData;
        
    }
    
    /**
     * Retrieves this user's contact email address.
     * Defaults to the standard email address of this user.
     * 
     * @author Peter Hamm
     * @return type
     */
    public function getContactMailAddress()
    {
        
        $address = $this->{$this->field_email};
        
        if(strlen($this->{$this->field_email_contact}) > 0)
        {
            
            $address = $this->{$this->field_email_contact};
            
        }
        
        return $address;
        
    }
    
    /**
     * Retrieves a list of all data stored in connection with this account.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getDataDisclosure()
    {
        
        $result = Array();
        
        {
            
            {

                $entryDummy = new CalsyCalendarEntry();
            
                $filter = new CalsyCalendarFilter();

                $filter->setUserFrontendIdentifiers($this->{$this->field_pk});
                $filter->setIgnoreConfirmationStatus(true);

                $dataCalendar = CalsyCalendarEntry::getCalendarEntriesForFilter($filter);

                $dataCalendarFormatted = Array();
                
                for($i = 0; $i < count($dataCalendar); ++ $i)
                {
                    
                    $formattedData = Array(
                        
                        $entryDummy->field_pk => $dataCalendar[$i][$entryDummy->field_pk],
                        $entryDummy->field_title => $dataCalendar[$i][$entryDummy->field_title],
                        $entryDummy->timestamp_begin => $dataCalendar[$i][$entryDummy->timestamp_begin],
                        $entryDummy->timestamp_end => $dataCalendar[$i][$entryDummy->timestamp_end]
                        
                    );
                    
                    array_push($dataCalendarFormatted, $formattedData);
                    
                }
                
            }
        
            $result['calendar'] = Array(

                'title' => PerisianLanguageVariable::getVariable('p5aeddf62cc7b2'),
                'count_entries' => count($dataCalendar),
                'data' => $dataCalendarFormatted

            );
            
        }
            
        if(CalsyOrderModule::isEnabled())
        {
            
            $orderDummy = new CalsyOrder();
            $orderFilter = CalsyOrder::createFilter($this->{$this->field_pk});
            
            $dataOrder = CalsyOrder::getOrderList($orderFilter);
            
            $dataOrderFormatted = Array();

            for($i = 0; $i < count($dataOrder); ++ $i)
            {

                $formattedData = Array(

                    $orderDummy->field_pk => $dataOrder[$i][$orderDummy->field_pk],
                    $orderDummy->field_title => $dataOrder[$i][$orderDummy->field_title]

                );

                array_push($dataOrderFormatted, $formattedData);

            }
                        
            $result['order'] = Array(

                'title' => PerisianLanguageVariable::getVariable('10793'),
                'count_entries' => CalsyOrder::getOrderCount($orderFilter),
                'data' => $dataOrderFormatted

            );
            
        }
        
        return $result;
        
    }
    
    /**
     * Retrieves a human readable string for the specified gender enumerator ('none', 'male' or 'female).
     * 
     * @author Peter Hamm
     * @param String $enum
     * @return String
     */
    public static function getGenderForEnum($enum)
    {
        
        if($enum == 'male')
        {
            
            $variableId = '10831';
            
        }
        else if($enum == 'female')
        {
            
            $variableId = '10832';
            
        }
        else
        {
            
            $variableId = '10833';
            
        }
        
        $genderString = PerisianLanguageVariable::getVariable($variableId);
        
        return $genderString;
        
    }
    
    /**
     * Retreves the profile image address for the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserFrontendImageProfile($userId)
    {
        
        $userObj = new static($userId);
        
        return $userObj->getImageProfileAddress();
        
    }
    
    /**
     * Takes two frontend user objects and compares them to one another.
     * A new frontend user object is returned, and depending on which fields are filled in which of the input object,
     * a basic proposal for a new merged dataset is returned.
     * 
     * @author Peter Hamm
     * @param CalsyUserFrontend $mergingObjectOne
     * @param CalsyUserFrontend $mergingObjectTwo
     * @return CalsyUserFrontend
     */
    public static function getMergeProposal(CalsyUserFrontend $mergingObjectOne, CalsyUserFrontend $mergingObjectTwo)
    {
        
        $mergeProposalObject = new static();
        
        $saveFields = $mergingObjectOne->getSaveFields();
        
        for($i = 0; $i < count($saveFields); ++$i)
        {
            
            $fieldName = $saveFields[$i];
            $fieldValue = null;
            
            // Find out which value of which object to use
            {
                
                if($fieldName == $mergeProposalObject->field_sex)
                {
                    
                    if($mergingObjectOne->{$fieldName} != 'none')
                    {
                        
                        $fieldValue = $mergingObjectOne->{$fieldName};
                        
                    }
                    else
                    {
                        
                        $fieldValue = $mergingObjectTwo->{$fieldName};
                        
                    }
                    
                }
                else
                {
                    
                    if(strlen($mergingObjectOne->{$fieldName}) > 0)
                    {
                        
                        $fieldValue = $mergingObjectOne->{$fieldName};
                        
                    }
                    else
                    {
                        
                        $fieldValue = $mergingObjectTwo->{$fieldName};
                        
                    }
                    
                }
                
            }
            
            $mergeProposalObject->{$fieldName} = $fieldValue;
            
        }
        
        return $mergeProposalObject;
        
    }
    
    /**
     * Retrieves the list of fields and their values, as an associative array.
     * Excepts the user's password
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getFieldValues()
    {
     
        $data = parent::getFieldValues();
        
        unset($data[$this->field_login_password]);
        
        return $data;
        
    }
    
    /**
     * Retrieves the user's profile image address, if one is set.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getImageProfileAddress()
    {
        
        if($this->{$this->field_pk} == static::DEFAULT_USER_ID)
        {
            
            $address = PerisianFrameworkToolbox::getServerAddress() . 'assets/img/user_default.png';
            
            return $address;
            
        }
        
        $data = $this->getImageProfileData();
        
        $address = "";
        
        if(strlen($data['image']['name']) > 0 && strlen($data['image']['url']) > 0)
        {
            
            $address = $data['image']['url'];
            
        }
        
        return $address;
        
    }
    
    /**
     * Retrieves the meta data of the profile image of this user (if applicable).
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getImageProfileData()
    {
        
        try
        {
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $this->{$this->field_image_profile});
            
        }
        catch(PerisianException $e)
        {
            
            $fileInfo = null;
            
        }
        
        $data = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => 'calsy_user_frontend_image_profile',
            "title" => PerisianLanguageVariable::getVariable('p591b287ccb77c'),
            "description" => PerisianLanguageVariable::getVariable('p591b28a1a5ca8'),

            "image" => $fileInfo

        );
            
        return $data;
        
    }
    
    /**
     * Loads a frontend user's settings
     *
     * @author Peter Hamm
     * @param integer $userFrontendId A frontend user's ID
     * @return void
     */
    protected function loadSettings($userFrontendId = 0)
    {

        $this->userFrontendSetting = self::loadSettingsForUserFrontend($userFrontendId);

        return;
        
    }
    
    /**
     * Creates a settings object and loads the data.
     * 
     * @param String $userFrontendId
     * @return \CalsyUserFrontendSetting
     */
    protected static function loadSettingsForUserFrontend($userFrontendId)
    {

        PerisianFrameworkToolbox::security($userFrontendId);
        
        $settings = new CalsyUserFrontendSetting();
        $settings->setDependency($userFrontendId);
        $settings->getSetting();
        
        return $settings;
        
    }
    
    /**
     * Retrieves the frontend user settings for this current object.
     * Loads it if it is not yet loaded.
     * 
     * @author Peter Hamm
     * @return CalsyUserFrontendSetting
     */
    protected function getUserSetting() 
    {
        
        if(!is_object($this->userFrontendSetting))
        {
            
            $this->loadSettings();
            
        }
        
        return $this->userFrontendSetting;
        
    }
    
    /**
     * Sets this frontend user's setting for the specified setting ID or name.
     * 
     * @author Peter Hamm
     * @param mixed $settingIdOrName Either an ID or a setting name.
     * @param String value
     * @return String
     */
    public function setSettingValue($settingIdOrName, $value)
    {
        
        $settingObject = $this->getUserSetting();
                
        $settingObject->setSettingValue($this->{$this->field_pk}, $settingIdOrName, $value);
                
        return true;
        
    }
    
    /**
     * Retrieves this frontend user's setting for the specified setting ID or name.
     * 
     * @author Peter Hamm
     * @param mixed $settingIdOrName Either an ID or a setting name.
     * @return String
     */
    public function getSettingValue($settingIdOrName)
    {
        
        $settingObject = $this->getUserSetting();
        
        $settingValue = $settingObject->getSetting(0, $settingIdOrName);
        
        if(is_array($settingValue) && count($settingValue) != 1)
        {
            
            return null;
            
        }
        
        return $settingValue;
        
    }

    /**
     * Returns the setting object for this frontend user
     *
     * @author Peter Hamm
     * @return CalsyUserFrontendSetting
     */
    public function getSettingObject()
    {
        
        return $this->userFrontendSetting;
        
    }
    
    /**
     * Sets this object's values from the server's post data.
     * 
     * @author Peter Hamm
     * @param bool $includePrimaryKey Optional, default: false. If set to true, this will include the primary key as well.
     * @param array $excludeFields Optional, will not be considered for CalsyUserFrontend / overwritten by the class.
     * @return void
     */
    public function setObjectDataFromPost($includePrimaryKey = false, $excludeFields = array()) 
    {
        
        $excludeFields = array(
            
            $this->field_language_id,
            $this->field_login_password,
            $this->field_registration_status,
            $this->field_last_login,
            $this->field_created
            
        );
        
        parent::setObjectDataFromPost($includePrimaryKey, $excludeFields);
        
    }
    
    /**
     * Retrieves the full name for the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return boolean
     */
    public static function getFullnameForUserById($userId)
    {
        
        try
        {
            
            $user = new static($userId);

            return $user->getFullName();

        }
        catch(PerisianException $e)
        {
            
        }
        
        return lg('p5b03118c5ff01');
        
    }
    
    /**
     * Retrieves the frontend user's full name as a single string.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getFullName()
    {
        
        $names = array();
        
        if(isset($this->{$this->field_name_first}) && strlen($this->{$this->field_name_first}) > 0)
        {
            
            array_push($names, $this->{$this->field_name_first});
            
        }
        
        if(isset($this->{$this->field_name_last}) && strlen($this->{$this->field_name_last}) > 0)
        {
            
            array_push($names, $this->{$this->field_name_last});
            
        }
        
        $value = implode(" ", $names);
        
        return $value;
        
    }
    
    /**
     * Retrieves the frontend user's full name as a single string.
     * 
     * @author Peter Hamm
     * @param $entry A frontend user entry.
     * @return String
     */
    private function getFullNameFromEntryArray($entry)
    {
        
        $names = array();
        
        if(isset($entry[$this->field_name_first]))
        {
            
            array_push($names, $entry[$this->field_name_first]);
            
        }
        
        if(isset($entry[$this->field_name_last]))
        {
            
            array_push($names, $entry[$this->field_name_last]);
            
        }
        
        $value = implode(" ", $names);
        
        return $value;
        
    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);
            $this->loadSettings($data[0][$this->field_pk]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves a list of frontend users
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getUserFrontendList($search = "", $orderBy = "calsy_user_frontend_fullname", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new static();
                
        $query = $instance->buildSearchString($search);
        
        if($orderBy == "pk")
        {
            
            $orderBy = $instance->field_pk;
            
        }
        else if($orderBy == $instance->fullname)
        {
            
            $orderBy  = "CONCAT({$instance->field_name_last}, {$instance->field_name_first})";
            
        }
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        for($i = 0; $i < count($results); ++$i)
        {
        
            $results[$i][$instance->fullname] = $instance->getFullNameFromEntryArray($results[$i]);
            
        }
        
        return $results;
        
    }
    
    /**
     * Retrieves a formatted list of frontend users, e.g. the password field is 
     * stripped and a formatted profile image address is added.
     * Also includes custom field data.
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param bool $includeDefaultUser Optional, include a default dummy user?
     * @return array
     */
    public static function getUserFrontendListFormattedWithCustomFields($search = "", $orderBy = "calsy_user_frontend_fullname", $order = "ASC", $offset = 0, $limit = 0, $includeDefaultUser = false)
    {
        
        $userList = static::getUserFrontendListFormatted($search, $orderBy, $order, $offset, $limit, $includeDefaultUser);
        
        $dummy = new static();
        
        foreach($userList as &$userEntry)
        {
            
            $userObject = new static($userEntry[$dummy->field_pk]);
            
            $userEntry['fields_custom'] = $userObject->getCustomFields();
            
        }
        
        return $userList;
        
    }
    
    /**
     * Retrieves a formatted list of frontend users, e.g. the password field is 
     * stripped and a formatted profile image address is added.
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param bool $includeDefaultUser Optional, include a default dummy user?
     * @return array
     */
    public static function getUserFrontendListFormatted($search = "", $orderBy = "calsy_user_frontend_fullname", $order = "ASC", $offset = 0, $limit = 0, $includeDefaultUser = false)
    {
        
        $instance = new static();
        
        $unformattedList = static::getUserFrontendList($search, $orderBy, $order, $offset, $limit);
        
        $formattedList = Array();
        
        for($i = 0; $i < count($unformattedList); ++$i)
        {
            
            $formattedEntry = $unformattedList[$i];
            
            if(strlen($formattedEntry[$instance->field_image_profile]) > 0)
            {
            
                $profileImageFileInfo = PerisianUploadFileManager::getFileInfo('image', $formattedEntry[$instance->field_image_profile]);
                
                $formattedEntry[$instance->field_image_profile . '_address'] = @$profileImageFileInfo['url'];
            
            }
            
            unset($formattedEntry[$instance->field_login_password]);
                
            array_push($formattedList, $formattedEntry);
            
        }
        
        // Display a "Deleted user" if there are calendar entries for deleted users.
        if($includeDefaultUser)
        {
            
            $defaultUser = new CalsyUserFrontend(CalsyUserFrontend::DEFAULT_USER_ID);

            $defaultUserFields = $defaultUser->getFieldValues();
            
            $defaultUserFields[$defaultUser->fullname] = $defaultUser->{$defaultUser->field_name_first};

            array_push($formattedList, $defaultUserFields);
            
        }
        
        return $formattedList;
        
    }
    
    /**
     * Retrieves the count of all frontend user for the specified frontend user
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getUserFrontendCount($search = "")
    {
        
        $instance = new static();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves a frontend user by his external identifier
     * 
     * @author Peter Hamm
     * @param String $externalIdentifier
     * @return CalsyUserFrontend
     */
    public static function getUserFrontendForExternalId($externalIdentifier)
    {
        
        PerisianFrameworkToolbox::security($externalIdentifier);
        
        $userFrontend = new static();
                
        if(strlen($externalIdentifier) > 0)
        {
            
            $query = $userFrontend->field_external_id . ' = "' . $externalIdentifier . '"';

            $results = $userFrontend->getData($query);

            if(count($results) > 0)
            {

                $userFrontend = new static($results[0][$userFrontend->field_pk]);

            }
            
        }
        
        return $userFrontend;
        
    }
    
    /**
     * Retrieves a frontend user by his email and password.
     * 
     * @author Peter Hamm
     * @param String $emailAddress
     * @param String $password Unencrypted
     * @param bool $ignorePassword Optional, default: false.
     * @return CalsyUserFrontend
     */
    public static function getUserFrontendForEmailAndPassword($emailAddress, $password, $ignorePassword = false)
    {
        
        PerisianFrameworkToolbox::security($emailAddress);
        
        $password = CalsyUserBackend::encryptPassword($password);
        
        $userFrontend = new static();
                
        $query = $userFrontend->field_email . ' = "' . $emailAddress . '"';
        
        if(!$ignorePassword)
        {
            
            $query .= ' AND ' . $userFrontend->field_login_password . ' = "' . $password . '"';
                
        }
               
        $results = $userFrontend->getData($query);
        
        if(count($results) > 0)
        {
            
            $userFrontend = new static($results[0][$userFrontend->field_pk]);
            
        }
        
        return $userFrontend;
        
    }
    
    /**
     * Gets the number of frontend users registered with this email address
     * 
     * @param String $emailAddress
     * @return int
     */
    public static function getCountForEmail($emailAddress)
    {
        
        PerisianFrameworkToolbox::security($emailAddress);
        
        $instance = new static();
                
        $query = $instance->field_email . ' = "' . $emailAddress . '"';
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    
    /**
     * Checks if the specified email address and password resemble a valid login.
     * 
     * @param String $emailAddress
     * @param String $password Unencrypted password
     * @return bool
     */
    public static function isValidLogin($emailAddress, $password)
    {
        
        PerisianFrameworkToolbox::security($emailAddress);
        PerisianFrameworkToolbox::security($password);
        
        $password = PerisianUser::encryptPassword($password);
        
        $instance = new static();
                
        $query = $instance->field_email . ' = "' . $emailAddress . '" AND ' . $instance->field_login_password . ' = "' . $password . '"';
                
        $results = $instance->getCount($query);
        
        return $results > 0;
        
    }
    
    /**
     * Deletes the current frontend user
     * 
     * @author Peter Hamm
     * @return void
     */
    public function delete()
    {
        
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} <= 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10823));
            
        }
                
        $userFrontendOrderDummy = new CalsyCalendarEntryUserFrontendOrder();
        $orderDummy = new CalsyOrder();
        $calendarEntryDummy = new CalsyCalendarEntry();
        
        $calendarEntriesToDelete = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForUserFrontend($this->{$this->field_pk});
        $ordersToDelete = CalsyOrder::getOrderList(CalsyOrder::createFilter($this->{$this->field_pk}));

        if(PerisianUser::isEnabledAnonymizationOnDelete())
        {

            // Keep all related data, but anonymize it and make a default user the owner.
            
            CalsyCalendarEntryUserFrontendOrder::changeUserFrontendIdForEntries($this->{$this->field_pk}, static::DEFAULT_USER_ID);
            CalsyOrder::changeUserFrontendIdForEntries($this->{$this->field_pk}, static::DEFAULT_USER_ID);
                        
        }
        else
        {

            // Delete all related data

            for($i = 0; $i < count($calendarEntriesToDelete); ++ $i)
            {

                // Deleting the calendar entry and the relation to the orders

                $entryId = $calendarEntriesToDelete[$i][$userFrontendOrderDummy->field_pk];

                $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder($entryId);

                $calendarEntryDummy->delete($calendarEntryDummy->field_pk . "=" . $userFrontendOrderObject->{$userFrontendOrderObject->field_fk});
                $userFrontendOrderDummy->delete($userFrontendOrderDummy->field_pk . "=" . $entryId);

            }
        
            for($i = 0; $i < count($ordersToDelete); ++ $i)
            {

                // Deleting the orders 

                $orderDummy->delete($orderDummy->field_pk . "=" . $ordersToDelete[$i][$orderDummy->field_pk]);

            }

        }
        
        CalsyCalendarEntryNotificationRelationAccount::deleteNotificationSettingsForRelation($this->{$this->field_pk});
        
        parent::delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }

    /**
     * Checks if there is an entry for the specified frontend user email and -password
     * in the database.
     *
     * @author Peter Hamm
     * @param String $email The frontend user's email address
     * @param String $password The frontend user's password
     * @return boolean Does this email / password combination exist or not?
     */
    public function checkLogin($email, $password)
    {

        PerisianFrameworkToolbox::security($email);
        PerisianFrameworkToolbox::security($password);

        if(strlen($email) < 3 && strlen($password) < 5)
        {
            return false;
        }
       
        if($this->{$this->field_registration_status} == 'inactive')
        {

            $this->loginSuccessful = false;
            
            return false;

        }

        $query = "{$this->field_email} = '{$email}' AND {$this->field_login_password} = '{$password}'";
                
        $this->loginSuccessful = $this->loadData($query) == 1;

        return $this->loginSuccessful;

    }
    
    /**
     * Unsets the session values for this frontend user
     * 
     * @author Peter Hamm
     * @return void
     */
    protected static function unsetSessionValues()
    {
        
        PerisianSession::unsetSessionValue('accountEmail');
        PerisianSession::unsetSessionValue('accountPassword');
        
    }
    
    /**
     * Fired directly before the object is saved to the database.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
        
        $this->{$this->field_email} = trim($this->{$this->field_email});
        
        if(strlen($this->{$this->field_email}) < 5)
        {
            
            unset($this->{$this->field_email});
            
        }
       
        return true;
        
    }
    
    /**
     * Handles the login / logout of a frontend user
     * Returns an array containing a frontend user object, plus two booleans indicating
     * his status
     *
     * @author Peter Hamm
     * @param String $action May be 'login' or 'logout'
     * @param Sring $email Optional, will take the "login_user_frontend_email" request if empty
     * @param Sring $password Optional, will take the "login_user_frontend_password" request if empty
     * @param bool $noRedirect Optional, if set to true there'll be no redirect after the action.
     * @return static A frontend user object
     */
    static public function handleLogin($action, $email = '', $password = '', $noRedirect = false)
    {

        PerisianFrameworkToolbox::security($action);
        PerisianFrameworkToolbox::security($email);
                
        $email = strlen($email) > 0 ? $email : PerisianFrameworkToolbox::getRequest('login_email');
        $password = CalsyUserBackend::encryptPassword(strlen($password) > 0 ? ($password) : PerisianFrameworkToolbox::getRequest('login_password'));
        
        $userFrontend = new static();
        
        if($action == 'login' || $action == 'loginAsync')
        {
            
            PerisianSession::setSessionValue('accountEmail', $email);
            PerisianSession::setSessionValue('accountPassword', $password);
            
        }
        else if($action == 'logout')
        {

            static::unsetSessionValues();
            
            PerisianLanguageVariable::setLanguage($userFrontend->getLanguage());
            
            return $userFrontend;

        }
        
        $loginFailed = false;
        
        try
        {
            
            $userFrontend->checkLogin(PerisianSession::getSessionValue('accountEmail'), PerisianSession::getSessionValue('accountPassword'));
            
        }
        catch(Exception $e)
        {
            
            static::unsetSessionValues();
            
            $loginFailed = true;
            
        }
                
        PerisianLanguageVariable::setLanguage($userFrontend->getLanguage());
        
        if($loginFailed || $userFrontend->{$userFrontend->field_registration_status} == 'inactive')
        {
            
            if($action == 'loginAsync')
            {
                
                $result = Array(
                    
                    'success' => false,
                    'async' => true,
                    'error' => 'user_inactive'
                    
                );
                
                echo json_encode($result);
                
                exit;
                
            }

            return $userFrontend;

        }

        if($action == 'loginAsync')
        {

            $result = Array(

                'success' => true,
                'async' => true

            );

            echo json_encode($result);
            
            exit;

        }

        if($action == 'login' && $userFrontend->isLoggedIn())
        {
            
            // If the frontend user logged in successfully, he is being redirected
            // to the page he initially requested (or '/index/' by default).
            // Also, the login timestamp is being saved to the database

            $userFrontend->{$userFrontend->field_last_login} = PerisianFrameworkToolbox::sqlTimestamp();
            $userFrontend->save($userFrontend->field_last_login);
            
            if(!$noRedirect)
            {

                $redirect = PerisianFrameworkToolbox::getRequest('redirect');

                if(strpos($redirect, 'do=logout') !== false)
                {
                    
                    $redirect = str_replace(Array('?do=logout', '&do=logout'), '', $redirect);
                    
                }
                
                if(!isset($redirect) || strlen($redirect) == 0)
                {
                    
                    $redirect = PerisianFrameworkToolbox::getServerAddress(false) . 'index/';
                    
                }
                
                header("Location: {$redirect}");
                
            }

        }
        else if($userFrontend->isLoggedIn() && strpos(PerisianFrameworkToolbox::getServerVar('SCRIPT_NAME'), 'login.php') !== false)
        {

            if(!$noRedirect)
            {
                
                // Direct requests of the login script when logged in
                // are being redirected to the index file as well.

                $redirect = PerisianFrameworkToolbox::getServerAddress(false) . 'index/';

                header("Location: {$redirect}");
                
            }

        }

        return $userFrontend;

    }
    
    /**
     * Returns true if the user is logged in, else: false
     *
     * @author Peter Hamm
     * @return boolean User logged in or not?
     */
    public function isLoggedIn()
    {
        
        if($this->loginSuccessfulOverride)
        {
            
            $isLoggedIn = true;
            
        }
        else
        {
            
            $isLoggedIn = $this->{$this->field_registration_status} == "inactive" ? false : $this->loginSuccessful;
            
        }
        
        return $isLoggedIn; 
        
    }
    
    /**
     * Sets a value that indicates whether the frontend user should be treated as logged in or not.
     * Set to false, the default check if the user is logged in is used.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setIsLoggedInOverride($value)
    {
        
        $this->loginSuccessfulOverride = $value;
        
    }
    
    /**
     * Retrieves the language ID of this frontend user
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return int Language ID
     */
    public function getLanguage()
    {

        $languageSettingId = PerisianFrameworkToolbox::getConfig('system/setting_id/default_language');
        $settingObj = new PerisianSystemSetting();
        $specifiedLanguage = $settingObj->getSetting($languageSettingId);
        
        $languageObject = new PerisianLanguage();
        $languageList = PerisianLanguage::getLanguages();
        
        // Check if the frontend user wants to switch the language
        {
            
            $languageParam = PerisianFrameworkToolbox::getRequest('lg');
            
            if(isset($languageParam) && strlen($languageParam) > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i][$languageObject->field_pk] == $languageParam)
                    {
                        
                        // Save the language into a cookie.
                        PerisianSession::setSessionValue('userFrontendLanguage', $languageParam);
                        
                        return $languageParam;
                        
                    }
                    
                }
                
            }
            
        }

        if($this->isLoggedIn())
        {

            $userFrontendLanguage = $this->{$this->field_language_id};

            if($userFrontendLanguage > 0)
            {
                
                PerisianSession::unsetSessionValue('userFrontendLanguage');
                $specifiedLanguage = $userFrontendLanguage;
                
            }

        }
        else
        {
            
            // Check if there's a cookie set, use it if true.
            $sessionValue = PerisianSession::getSessionValue('userFrontendLanguage');
            
            if(isset($sessionValue) && strlen($sessionValue) > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i][$languageObject->field_pk] == $sessionValue)
                    {
                        
                        $specifiedLanguage = $sessionValue;
                        
                        break;
                        
                    }
                    
                }
                
            }
            
        }
        
        return $specifiedLanguage;

    }
    
    /**
     * Tries to load a frontend user by his e-mail address
     * 
     * @author Peter Hamm
     * @param String $emailAddress
     * @return void
     */
    public function getUserFrontendByEmail($emailAddress)
    {
        
        PerisianFrameworkToolbox::security($emailAddress);
        
        $query = $this->field_email . ' = "' . $emailAddress . '"';
        
        $this->loadData($query);
        
    }
    
    /**
     * Sends an e-mail out to the currently loaded user on how activate his newly registered account.
     * 
     * @author Peter Hamm
     * @return boolean Could the e-mail be sent or not?
     */
    public function sendEmailActivation()
    {
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        $result = CalsyUserFrontendActivation::addEntryForUser($this->{$this->field_pk});
        
        if(!$result["success"])
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $subject = PerisianLanguageVariable::getVariable(10645);
                
                $activationLink = PerisianFrameworkToolbox::getServerAddress(false) . "registration/?do=r&c=" . $result["code"];

                $content = PerisianLanguageVariable::getVariable(10646) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $activationLink, $content);
                $content = str_replace("%3", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Sends an e-mail out to the currently loaded frontend user on how to change his forgotten password
     * 
     * @author Peter Hamm
     * @param boolean $initial Is this actually a request for the user to set a custom password for the first login?
     * @return boolean Could the e-mail be sent or not?
     */
    public function sendEmailPasswordForgotten($initial = false)
    {
        
        if(strlen($this->{$this->field_email}) < 3)
        {
                        
            return false;
            
        }
        
        $result = CalsyUserFrontedPasswordForgotten::addEntryForUser($this->{$this->field_pk});
        
        if(!$result["success"])
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }

        // Send the e-mail to the user
        {
            
            // The text content
            {
                
                $subject = PerisianLanguageVariable::getVariable($initial ? 10628 : 10618);
                
                $passwordLink = PerisianFrameworkToolbox::getServerAddress(false) . "registration/?do=rp&c=" . $result["code"] . ($initial ? "&i=1" : "");

                $content = PerisianLanguageVariable::getVariable($initial ? 10627 : 10626) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $passwordLink, $content);
                $content = str_replace("%3", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);

            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Sends a default calendar entry info mail with custom content and subject.
     * Will include the calendar entry in ICS format as an attachment.
     * 
     * @author Peter Hamm
     * @param String $subject
     * @param String $customContent
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser
     * @param CalsyUserFrontend $sendingUserFrontend
     * @param bool $ignoreTimezone
     * @return boolean
     */
    protected function sendEmailCalendarEntryInfo($subject, $customContent, CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
                
        if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled())
        {
                        
            return false;
            
        }
        
        if(is_null($sendingUserFrontend) && is_null($sendingUser))
        {
                        
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
                
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress(false) . "calendar/overview/?se=" . $calendarEntry->{$calendarEntry->field_pk};
                
                if(!is_null($sendingUser))
                {
                    
                    $customContent = str_replace("%sendingUserFullName", $sendingUser->{$sendingUser->field_fullname}, $customContent);
                    
                }
                
                $content = PerisianLanguageVariable::getVariable(10969) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $customContent, $content);
                $content = str_replace("%3", $entryDetails, $content);
                $content = str_replace("%4", $displayLink, $content);
                $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
                
            // Add the calendar entry in ICS format as an attachment
            {
                
                $export = new CalsyCalendarExport($calendarEntry->{$calendarEntry->field_pk});
                
                $export->setRequiredUserFrontendId($this->{$this->field_pk});
                
                $attachment = Array(
                    
                    "name" => "calSy.ics",
                    "path" => $export->getExportFilePath()
                        
                );
                
                PerisianMail::addAttachmentToMessage($mailMessage, $attachment);
                
                $export->deleteExportFile();
            
            }
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Sends an e-mail that an entry has been edited
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryChanged(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }

        // Build the content
        {

            $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);

            $subject = PerisianLanguageVariable::getVariable('p5925bd669bdd7') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            $content = PerisianLanguageVariable::getVariable('p5925bd669bdd7');

        }
                
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Sends an e-mail that a new entry has been made for this frontend user and
     * that a payment is required to confirm it.
     * 
     * @param String $paymentLink An URL to send the user to send a payment to.
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryNewPaymentRequired($paymentLink, CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Build the content
        {

            $subject = PerisianLanguageVariable::getVariable(10966) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            
            $content = PerisianLanguageVariable::getVariable('p59f68363a7ba0');
            $content = str_replace("%1", $paymentLink, $content);

        }
        
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Sends e-mails that a new entry has been made for this frontend user
     * and their table reservation.
     * 
     * @param Array $calendarEntries An array of CalsyCalendarEntry objects
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Were the mails sent successfully?
     */
    protected function sendEmailCalendarEntriesNewTableReservation(Array $calendarEntries, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_array($calendarEntries) && count($calendarEntries) > 0)
        {
            
            foreach($calendarEntries as $calendarEntry)
            {
                
                $this->sendEmailCalendarEntryNewTableReservation($calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);
                    
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Sends an e-mail that a new entry has been made for this frontend user
     * and their table reservation.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    protected function sendEmailCalendarEntryNewTableReservation(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Build the content
        {

            $isExistingEntry = strlen($calendarEntry->{$calendarEntry->field_pk}) > 0;
            $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);

            $subject = lg('p5f7b328c52882') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            
            $content = lg('p5f7b38001e6c9');
            $content .= "\n\n<b>" . lg('p5f6d760edd4b4') . "</b>\n";
            
            {
                
                $tableId = CalsyTableReservation::getTableIdForCalendarEntry($calendarEntry->{$calendarEntry->field_pk});
                $tableTitle = CalsyTableReservation::getTableTitle($tableId);
                $locationTitle = CalsyTableReservation::getLocationTitleForTable($tableId);
                $numberPersons = CalsyTableReservation::getNumberOfPersonsForCalendarEntry($calendarEntry->{$calendarEntry->field_pk});
                
                $content .= $locationTitle . ', ' . $tableTitle . "\n";
                $content .= str_replace("%1", $numberPersons, lg('p5f6d6e0549363')) . "\n";
                
            }

        }
        
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Sends an e-mail that a new entry has been made for this frontend user.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryNew(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Build the content
        {

            $isExistingEntry = strlen($calendarEntry->{$calendarEntry->field_pk}) > 0;
            $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);

            $subject = PerisianLanguageVariable::getVariable($entryByUserFrontend ? 10966 : 10965) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            $content = PerisianLanguageVariable::getVariable($entryByUserFrontend ? 10968 : 10967);

        }
        
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Sends an e-mail as a reminder for an upcoming appointment
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryReminder(CalsyCalendarEntry $calendarEntry, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled() || !CalsyCalendarNotification::isEnabled($this))
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        $isExistingEntry = strlen($calendarEntry->{$calendarEntry->field_pk}) > 0;
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress(false) . "calendar/overview/?se=" . $calendarEntry->{$calendarEntry->field_pk};
                
                $subject = PerisianLanguageVariable::getVariable(11193) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                $adjustedMailContent = PerisianLanguageVariable::getVariable(11194);
                                
                $content = PerisianLanguageVariable::getVariable(10969) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $adjustedMailContent, $content);
                $content = str_replace("%3", $entryDetails, $content);
                $content = str_replace("%4", $displayLink, $content);
                $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Sends an e-mail that a calendar entry has been confirmed.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param bool $ignoreTimezone
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryConfirmed(CalsyCalendarEntry $calendarEntry, $ignoreTimezone = false)
    {
        
        $ignoreTimezone = true;
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled())
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress(false) . "calendar/overview/?se=" . $calendarEntry->{$calendarEntry->field_pk};
                
                $userBackendObject = new CalsyUserBackend(@$calendarEntry->{$calendarEntry->field_user_id});
                
                $subject = PerisianLanguageVariable::getVariable(10970) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                
                $adjustedMailContent = strlen($userBackendObject->{$userBackendObject->field_fullname}) > 0 ? PerisianLanguageVariable::getVariable(10896) . ": " . $userBackendObject->{$userBackendObject->field_fullname} : "";
                
                // Determine Which message to send
                {
                    
                    $originalMessage = $calendarEntry->isPaymentRequired() ? 'p5a3196bdc7d1f' : 10971;
                
                }
                
                $content = PerisianLanguageVariable::getVariable($originalMessage) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $entryDetails, $content);
                $content = str_replace("%3", $adjustedMailContent, $content);
                $content = str_replace("%4", $displayLink, $content);
                $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
                
        return true;
        
    }
    
    /**
     * Sends an e-mail that the users has been invited to a calendar survey.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntry $calendarEntry
     * @param String $invitationCode
     * @param bool $ignoreTimezone
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailSurveyInvitation(CalsyCalendarEntry $calendarEntry, $invitationCode, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled())
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
                        
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $subject = lg('p5e4c4d1ef2da9') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                
                $content = $this->getEmailContentSurveyInvitation($calendarEntry, $invitationCode, $ignoreTimezone);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Retrieves the content for the invitation mail to send to users.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntry $calendarEntry
     * @param String $invitationCode
     * @param boolean $ignoreTimezone Optional, default: false
     * @return String
     */
    public function getEmailContentSurveyInvitation($calendarEntry, $invitationCode, $ignoreTimezone = false)
    {
            
        $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
        
        $callbackLink = PerisianFrameworkToolbox::getServerAddress(false) . "invitation/?e=" . $calendarEntry->{$calendarEntry->field_pk} . '&c=' . $invitationCode;
        
        {

            $userBackendObject = new CalsyUserBackend($calendarEntry->{$calendarEntry->field_user_id});

            $adjustedMailContent = "";

            $adjustedMailContent .= strlen($userBackendObject->{$userBackendObject->field_fullname}) > 0 ? PerisianLanguageVariable::getVariable(10896) . ": " . $userBackendObject->{$userBackendObject->field_fullname} : "";

        }

        $entryDetails .= $adjustedMailContent;

        $content = PerisianLanguageVariable::getVariable('p5e4c4dd17a217') . "\n\n";
        $content = str_replace("%1", $this->getFullName(), $content);
        $content = str_replace("%2", $entryDetails, $content);
        $content = str_replace("%3", $callbackLink, $content);
        $content = str_replace("%4", PerisianFrameworkToolbox::timestampToDate(), $content);

        $content = nl2br($content);
        
        return $content;
        
    }
    
    /**
     * Sends an e-mail that a calendar entry has been canceled.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntry $calendarEntry
     * @param bool $ignoreTimezone
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryCanceled(CalsyCalendarEntry $calendarEntry, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(!CalsyUserFrontendRegistrationAndCalendarModule::isEnabled())
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
                        
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->{$this->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $userBackendObject = new CalsyUserBackend($calendarEntry->{$calendarEntry->field_user_id});
                $userFrontendOrderObject = CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($calendarEntry->{$calendarEntry->field_pk}, $this->{$this->field_pk});
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress(false) . "calendar/overview/?se=" . $calendarEntry->{$calendarEntry->field_pk};
                
                $subject = PerisianLanguageVariable::getVariable(11158) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                
                {
                    
                    $adjustedMailContent = "";
                    
                    if(strlen($userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation_message}) > 0)
                    {
                        
                        $adjustedMailContent .= "\n\n";
                        $adjustedMailContent .= '<span style="border: 1px solid #f7d7be; background-color:#faebd4; color: #8a6d3b; padding: 15px; display: inline-block;"><b>' . PerisianLanguageVariable::getVariable('p575ecd6d29468') . ":</b> ";
                        $adjustedMailContent .= $userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation_message} . "</span>\n\n";
                        
                    }
                    else
                    {
                        
                        $adjustedMailContent .= "\n\n";
                        
                    }
                    
                    $adjustedMailContent .= strlen($userBackendObject->{$userBackendObject->field_fullname}) > 0 ? PerisianLanguageVariable::getVariable(10896) . ": " . $userBackendObject->{$userBackendObject->field_fullname} : "";
                    
                }
                
                $entryDetails .= $adjustedMailContent;
                
                $content = PerisianLanguageVariable::getVariable(11159) . "\n\n";
                $content = str_replace("%1", $this->getFullName(), $content);
                $content = str_replace("%2", $entryDetails, $content);
                $content = str_replace("%3", $displayLink, $content);
                $content = str_replace("%4", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->getContactMailAddress(), 'name' => $this->getFullName())

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
}

/**
 * Handles settings for frontend users
 *
 * @author Peter Hamm
 * @date 2016-05-09
 */
class CalsyUserFrontendSetting extends PerisianDependentSetting
{

    // Basic values for the specific settings
    public $table = 'calsy_user_frontend_setting';
    public $field_pk = 'calsy_user_frontend_setting_id';
    public $field_fk = 'calsy_user_frontend_setting_setting_id';
    public $field_setting_value = 'calsy_user_frontend_setting_value';
    public $field_setting_dependency = 'calsy_user_frontend_setting_user_frontend_id';

    public $setting_name = 'account';
    
}

/**
 * Handles forgotten passwords.
 *
 * @author Peter Hamm
 */
class CalsyUserFrontedPasswordForgotten extends PerisianUserRegistrationAbstract
{

    public $table = 'calsy_user_frontend_password_forgotten';
    public $field_pk = 'calsy_user_frontend_password_forgotten_id';
    public $field_fk = 'calsy_user_frontend_password_forgotten_user_frontend_id';
    public $field_code = 'calsy_user_frontend_password_forgotten_code';
    public $field_time = 'calsy_user_frontend_password_forgotten_time';
        
}

/**
 * Handles activation codes.
 * 
 * @author Peter Hamm
 */
class CalsyUserFrontendActivation extends PerisianUserPasswordForgotten
{
    
    public $table = 'calsy_user_frontend_activation';
    public $field_pk = 'calsy_user_frontend_activation_id';
    public $field_fk = 'calsy_user_frontend_activation_user_frontend_id';
    public $field_code = 'calsy_user_frontend_activation_code';
    public $field_time = 'calsy_user_frontend_activation_time';
        
}
