<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'CalsyVmosoChatUserFrontend.class.php';
require_once 'modules/CalsyVmosoModule.class.php';
require_once 'CalsyVmosoChatUserFrontend.class.php';
require_once 'abstract/CalsyVmosoChatControllerAbstract.class.php';

/**
 * Vmoso Frontend User chat controller
 *
 * @author Peter Hamm
 * @date 2017-03-20
 */
class CalsyVmosoChatUserFrontendController extends CalsyVmosoChatControllerAbstract
{
        
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionOverview()
    {
        
        global $userFrontend;
                
        $renderer = array(
            
            'page' => 'frontend/calsy/vmoso/chat'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $chatLogEntries = $this->getConnection()->getMessageLogList();
        
        $result = array(
            
            'action' => 'overview',
            'list_logs' => $chatLogEntries,
            'title' => PerisianLanguageVariable::getVariable('p58d3fb8249c4e'),
            'key_layout_chat' => CalsyVmosoModule::getChatLayoutForUserFrontend($userFrontend->{$userFrontend->field_pk})
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a list of chat messages.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionGetMessages()
    {
                
        $this->setParameter('key', $this->getConnection()->getStoredChatKey());
        
        parent::actionGetMessages();
        
        $this->addProfileImagesToLog($this->result['result']['list']);
                
    }
    
    /**
     * Retrieves the connection to Vmoso.
     * 
     * @author Peter Hamm
     * @global $userFrontend
     * @return ICalsyVmosoChat
     */
    protected function getConnection()
    {
        
        global $userFrontend;
        
        if(!isset($userFrontend) || !is_a($userFrontend, "CalsyUserFrontend"))
        {
            
            // Invalid user data
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p5808ec44add6d'));
            
        }
             
        if(is_null($this->connection))
        {
            
            $this->connection = new CalsyVmosoChatUserFrontend($userFrontend->{$userFrontend->field_pk});

        }
        
        return $this->connection;
        
    }
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionLog()
    {
                
        global $userFrontend;
        
        $renderer = array(
            
            'page' => 'frontend/calsy/vmoso/chat_log'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        {
            
            $selectedLogId = $this->getParameter('l');
                
            $chatLogEntries = $this->getConnection()->getMessageLogList();
            
            $logEntry = new CalsyVmosoChatUserFrontendLog($selectedLogId);
            
            if($logEntry->{$logEntry->field_user_frontend_id} != $this->getConnection()->getUserFrontendId())
            {
                
                // Wrong user, no access.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10954'));
                
            }
            
            {
                
                $formattedTime = CalsyVmosoChatUserFrontend::getFormattedLogTime(PerisianFrameworkToolbox::sqlTimestampToPhp($logEntry->{$logEntry->field_last_update}));

                $title = PerisianLanguageVariable::getVariable('p58d3ea7a2b99f');
                $title = str_replace('%1', $formattedTime, $title);
                
            }
                        
        }
        
        // Enhance the log messages with data, e.g. profile images
        {
            
            $logMessages = $logEntry->getLogData();
            
            $this->addProfileImagesToLog($logMessages);
            
        }
        
        $result = array(
            
            'action' => 'log',
            'list_logs' => $chatLogEntries,
            
            'title' => $title,
            'timestamp' => $logEntry->{$logEntry->field_last_update},
            'messages' => $logMessages,
                    
            'key_layout_chat' => CalsyVmosoModule::getChatLayoutForUserFrontend($userFrontend->{$userFrontend->field_pk})
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Adds profile images to the message log specified.
     * 
     * @author Peter Hamm
     * @global $userFrontend
     * @param Array $logMessages Is treated as a reference.
     * @return Array
     */
    protected function addProfileImagestoLog(&$logMessages)
    {
                
        global $userFrontend;
        
        for($i = 0; $i < count($logMessages); ++$i)
        {

            $profileImageAddress = $userFrontend->getImageProfileAddress();
            
            if(@$logMessages[$i]['type'] == 'user' && strlen($profileImageAddress) > 0)
            {

                $logMessages[$i]['image'] = $profileImageAddress;

            }

        }
        
        return $logMessages;
        
    }
        
    /**
     * Archives the frontend user's current chat and prepares a new one.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionArchiveChat()
    {
                
        $renderer = array(
            
            'json' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
                                    
            $this->getConnection()->archiveChat();
            
            $chatLogEntries = $this->getConnection()->getMessageLogList();
            
        }
        
                
        $result = Array(
            
            'success' => true,
            'error' => false,
            'list_logs' => $chatLogEntries
            
        );
        
        $this->setResultValue('result', $result);
        
    }
        
}