<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/CalsyVmosoModule.class.php';
require_once 'abstract/CalsyVmosoChatControllerAbstract.class.php';

/**
 * Vmoso backend chat controller
 *
 * @author Peter Hamm
 * @date 2017-05-29
 */
class CalsyVmosoChatBackendController extends CalsyVmosoChatControllerAbstract
{
            
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionOverview()
    {
        
        global $user;
        
        $chatToOpen = $this->getParameter('l');
                
        $renderer = array(
            
            'page' => 'calsy/vmoso_backend/chat'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $title = PerisianLanguageVariable::getVariable('p58d3fb8249c4e');
        
        if(strlen($chatToOpen) > 0)
        {
            
            $title = $this->getConnection()->getChatTitle($chatToOpen);
            
        }
                        
        $result = array(
            
            'action' => 'overview',
            'title' => $title,
            'key' => $chatToOpen,
            'key_layout_chat' => CalsyVmosoBackendModule::getChatLayoutForUser($user->{$user->field_pk})
            
        );
            
        if(strlen($chatToOpen) > 0)
        {
            
            $result['participants'] = $this->getConnection()->getChatParticipants($chatToOpen);
            
        }
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to create a new chat.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionNewChatForm()
    {
        
        global $user;
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/vmoso_backend/chat_new'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $listUsers = CalsyVmosoBackendModule::getCalsyVmosoUserList();
                
        if(count($listUsers) > 0)
        {
            
            for($i = 0; $i < count($listUsers); ++$i)
            {
                
                if($listUsers[$i][$user->field_pk] == $user->{$user->field_pk})
                {
                    
                    // Kick out the user requesting.
                    
                    unset($listUsers[$i]);
                    
                    break;
                    
                }
                
            }
            
        }
                
        $result = array(
                        
            'title_form' => PerisianLanguageVariable::getVariable('p59540365579f9'),
            'list_users' => $listUsers
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Creates a new chat with the data specified.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionCreateNewChat()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $participants = $this->getParameter('participants');
            $title = $this->getParameter('title');
            $userVmosoKey = PerisianUserSetting::getSettingValue($user->{$user->field_pk}, 'user_vmoso_account_key');
            
        }
        
        if(strlen($userVmosoKey) == 0)
        {
            
            // Invalid Vmoso user data
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p59442dbec9549'));
            
        }
        
        if(is_array($participants) && !in_array($userVmosoKey, $participants))
        {
            
            array_push($participants, $userVmosoKey);
            
        }
           
        $this->getConnection()->setNewChatData($title, $participants);
        $data = $this->getConnection()->createNewTask();
                      
        $result = array(
            
            'success' => true,
            'error' => false,
            'data' => $data
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Sends a message with the parameters specified.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionSendMessage()
    {

        $chatKey = $this->getParameter('key');
        
        $this->getConnection()->setChatKey($chatKey);
        
        parent::actionSendMessage();
        
    }
    
    /**
     * Retrieves a list of chat messages.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionGetMessages()
    {
        
        $inputKey = $this->getParameter('key');
        
        $key = strlen($inputKey) > 0 ? $inputKey : $this->getConnection()->getStoredChatKey();
        
        $this->setParameter('key', $key);
        
        parent::actionGetMessages();
                
    }
    
    /**
     * Retrieves a list of existing chats for the current user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetChatList()
    {
        
        $renderer = array(
            
            'json' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $taskList = $this->getConnection()->getChatList();
                        
        $result = array(
            
            'success' => true,
            'error' => false,
            'data' => $taskList
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves the connection to Vmoso.
     * 
     * @author Peter Hamm
     * @global $user
     * @return CalsyVmosoChatBackend
     */
    protected function getConnection()
    {
        
        global $user;
        
        if(!isset($user) || !is_a($user, "CalsyUserBackend"))
        {
            
            // Invalid user data
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p5808ec44add6d'));
            
        }
             
        if(is_null($this->connection))
        {
            
            $this->connection = new CalsyVmosoChatBackend($user->{$user->field_pk});

        }
        
        return $this->connection;
        
    }
            
}