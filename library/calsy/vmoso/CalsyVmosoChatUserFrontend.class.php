<?php

require_once("CalsyVmosoApi.class.php");
require_once("abstract/CalsyVmosoChatConnectionAbstract.class.php");
require_once("CalsyVmosoChatUserFrontendLog.class.php");

/**
 * Live chat functionality via the Vmoso API for calSy frontend users.
 * 
 * @author Peter Hamm
 * @date 2017-03-16
 */
class CalsyVmosoChatUserFrontend extends CalsyVmosoChatConnectionAbstract
{
    
    protected $userFrontend = null;
    
    /**
     * Creates a new instance, requires the identifier of an existing frontend user.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     */
    public function __construct($userFrontendId)
    {
                
        $this->userFrontend = new CalsyUserFrontend($userFrontendId);
        
        parent::__construct();
        
        $this->connection->setAllowTokenCache(true);
        
    }
    
    public function test()
    {
        
        $this->connection->getUserInfo();
        
    }
    
    /**
     * Retrieves the identifier of this object's associated frontend user.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getUserFrontendId()
    {
        
        return $this->userFrontend->{$this->userFrontend->field_pk};
        
    }
    
    /**
     * If the current frontend user opened a chat before, this will return that
     * conversation's Vmoso thread identifier to load all content and messages from.
     * 
     * The value is empty if the user didn't create a chat yet or if the previous chat
     * has just been archived and no message was sent yet.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getStoredChatKey()
    {
        
        $chatId = CalsyUserFrontendSetting::getSettingValue($this->userFrontend->{$this->userFrontend->field_pk}, static::SETTING_KEY_USER_FRONTEND_CHAT_ID);
        
        return $chatId;
        
    }
    
    /**
     * Retrieves the chat recipients that were set up in the backend.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getChatRecipients()
    {
                
        $recipientString = PerisianSystemSetting::getSettingValue(static::SETTING_KEY_RECIPIENTS_CHAT);
        
        $recipients = explode(",", $recipientString);
        
        return $recipients;
        
    }
        
    /**
     * Retrieves the list of existing logs for the current frontend user.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getMessageLogList()
    {
        
        $dummy = new CalsyVmosoChatUserFrontendLog();
        
        $formattedList = Array();
        
        $logList = CalsyVmosoChatUserFrontendLog::getLogList($this->userFrontend->{$this->userFrontend->field_pk});
        
        for($i = 0; $i < count($logList); ++$i)
        {
                        
            $formattedTime = static::getFormattedLogTime(PerisianFrameworkToolbox::sqlTimestampToPhp($logList[$i][$dummy->field_last_update]));
            
            $title = PerisianLanguageVariable::getVariable('p58d3ea7a2b99f');
            $title = str_replace('%1', $formattedTime, $title);
            
            $entry = Array(
                
                'id' => $logList[$i][$dummy->field_pk],
                'title' => $title,
                'count_messages' => $logList[$i][$dummy->field_count_messages]
                
            );
            
            array_push($formattedList, $entry);
            
        }
        
        return $formattedList;
        
    }
    
    /**
     * Creates a new task via the Vmoso API, it will generate a key to start a chat with.
     * 
     * @author Peter Hamm
     * @param Array $resultData Optional
     * @return Array The result data.
     */
    public function createNewTask(&$resultData = Array())
    {
        
        if(!is_array($resultData))
        {
            
            $resultData = Array();
            
        }
        
        $chatName = PerisianLanguageVariable::getVariable('p58cbd16d3f05e');

        $chatName = str_replace('%userFrontendName', $this->userFrontend->getFullName(), $chatName);
        $chatName = str_replace('%timeAndDate', PerisianFrameworkToolbox::timestampToDateTime(), $chatName);

        $recipients = $this->getChatRecipients();
        
        if(!in_array($this->getConnection()->getUserInfoEmailAddress(), $recipients))
        {

            array_push($recipients, $this->getConnection()->getUserInfoEmailAddress());

        }

        $resultData['api'] = $this->getConnection()->createChat($chatName, $recipients);

        CalsyUserFrontendSetting::setSettingValue($this->userFrontend->{$this->userFrontend->field_pk}, static::SETTING_KEY_USER_FRONTEND_CHAT_ID, $resultData['api']['key']);

        $resultData['calsy'] = Array(

            'id' => $resultData['api']['key'],
            'status' => 'new'

        );
        
        return $resultData;
        
    }
    
    /**
     * Save the chat log to the database, for the frontend user
     * 
     * @author Peter Hamm
     * @param Array $chatLog
     * @return void
     */
    protected function archiveChatLog($chatLog)
    {
        
        $logObject = new CalsyVmosoChatUserFrontendLog();

        $logObject->{$logObject->field_user_frontend_id} = $this->userFrontend->{$this->userFrontend->field_pk};
        $logObject->{$logObject->field_last_update} = PerisianFrameworkToolbox::sqlTimestamp();
        $logObject->{$logObject->field_count_messages} = count($chatLog);

        $logObject->setLogData($chatLog);

        $logObject->save();

        // Reset the user's chat ID to nil.
        CalsyUserFrontendSetting::setSettingValue($this->userFrontend->{$this->userFrontend->field_pk}, static::SETTING_KEY_USER_FRONTEND_CHAT_ID, '');
        
    }
    
}