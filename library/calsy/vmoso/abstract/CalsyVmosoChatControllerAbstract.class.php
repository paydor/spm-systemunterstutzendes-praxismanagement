<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/CalsyVmosoModule.class.php';

/**
 * Vmoso chat controller abstract
 *
 * @author Peter Hamm
 * @date 2017-05-29
 */
abstract class CalsyVmosoChatControllerAbstract extends PerisianController
{
    
    protected $connection = null;
        
    /**
     * Retrieves a list of chat messages.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionGetMessages()
    {
                
        $renderer = array(
            
            'json' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $page = $this->getParameter('page');
        
        $connection = $this->getConnection();
                
        $messageList = $connection->getLatestMessages($this->getParameter('key'), PerisianFrameworkToolbox::securityRevert($page));
        $formattedMessageList = $this->formatMessages($messageList);
        
        $nextPageInfo = $connection->getNextPageInfo();
        
        $result = Array(
            
            'success' => true,
            'list' => $formattedMessageList,
            'page_next' => $nextPageInfo
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Formats the incoming message list so it can be handled more easily.
     * 
     * @author Peter Hamm
     * @param Array $messageList
     * @return Array
     */
    protected function formatMessages($messageList)
    {
                
        $result = $this->getConnection()->formatMessages($messageList);
        
        sort($result);
        
        return $result;
        
    }
    
    /**
     * Sends a message with the parameters specified.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function actionSendMessage()
    {
        
        $renderer = array(
            
            'json' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $messageString = $this->getParameter('message');
        
        $messageString = strip_tags(PerisianFrameworkToolbox::securityRevert($messageString));
        
        $sendData = $this->getConnection()->sendMessage($messageString);
        
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves the connection to Vmoso.
     * 
     * @author Peter Hamm
     * @global $userFrontend
     * @return ICalsyVmosoChat
     */
    abstract protected function getConnection();
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}