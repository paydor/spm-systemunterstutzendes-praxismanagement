<?php

require_once("calsy/vmoso/CalsyVmosoApi.class.php");

/**
 * Live chat functionality via the Vmoso API for calSy.
 * 
 * @author Peter Hamm
 * @date 2017-05-29
 */
abstract class CalsyVmosoChatConnectionAbstract
{
    
    const SETTING_KEY_USER_BACKEND_CHAT_ID = 'user_backend_vmoso_chat_id';
    const SETTING_KEY_USER_FRONTEND_CHAT_ID = 'account_vmoso_chat_id';
    const SETTING_KEY_RECIPIENTS_CHAT = 'module_setting_calsy_vmoso_chat_recipients';
    
    protected $connection = null;
    
    protected $taskCache = Array();
    protected $messageList = Array();
    protected $nextPage = null;
    
    
    /**
     * Creates a new instance
     * 
     * @author Peter Hamm
     */
    public function __construct()
    {
        
        $this->getConnection();
        
    }
        
    /**
     * Retrieves the instantiated connection to the Vmoso API.
     * 
     * @author Peter Hamm
     * @return CalsyVmosoApi
     */
    protected function getConnection()
    {
        
        if(is_null($this->connection))
        {
            
            $this->connection = new CalsyVmosoApi();
            
        }
        
        return $this->connection;
        
    }
    
    /**
     * Retrieves information about the connected chat user.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getUserInfo()
    {
        
        $userInfo = $this->getConnection()->getUserInfo();
        
        return $userInfo;
        
    }
    
    /**
     * Retrieves the Vmoso chat key for the frontend user.
     * If no chat is found (or if it was disabled/deleted), a new chat is created.
     * 
     * @author Peter Hamm
     * @param bool $forceNewChat Optional, set this to true to force creating a new chat. Default: false.
     * @return String
     */
    public function retrieveChatKey($forceNewChat = false)
    {
        
        $data = $this->retrieveChat($forceNewChat);
        
        return $this->getStoredChatKey();
        
    }
    
    /**
     * Attempts to send the specified message to the chat with the specified key at the Vmoso API.
     * This will try to send the message up to five times, each time with a delay of one second.
     * Should the message not be delivered by then, an exception is thrown.
     * 
     * @param String $chatKey The key of the chat you want to send the message to
     * @param String $message The actual chat message.
     * @param int $internalCounter Optional, internal counting only
     * @return boolean
     */
    protected function sendMessageToChat($chatKey, $message, $internalCounter = 0)
    {
        
        try
        {
                  
            $resultData['api'] = $this->getConnection()->postComment($chatKey, $message);
            
        }
        catch(PerisianException $e)
        {
            
            // Try again, up to five times...
            
            if($internalCounter < 5)
            {

                // Wait for one second.
                usleep(1000000);
                
                // Try again...
                $this->sendMessageToChat($chatKey, $message, ($internalCounter + 1));
                
            }
            else
            {
                
                throw $e;
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Sends a chat message via the Vmoso API.
     * 
     * @param String $message
     * @return Array
     */
    public function sendMessage($message)
    {
        
        $key = $this->retrieveChatKey();
                        
        $resultData = Array(
            
            'calsy' => array(
                
                'id' => $key
                
            ),
            
            'api' => null
            
        );
        
        $this->sendMessageToChat($key, $message);
            
        return $resultData;
        
    }
    
    /**
     * Retrieves the title for the chat/task with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return String
     */
    public function getChatTitle($key)
    {
        
        $taskData = $this->getTask($key);
                
        return $taskData['name'];
        
    }
    
    /**
     * Takes unformatted participant data, e.g. from a Vmoso task, 
     * and returns it in a more well formatted way.
     * 
     * @author Peter Hamm
     * @param Array $unformattedParticipantData
     * @return Array
     */
    protected function getFormattedChatParticipant($unformattedParticipantData)
    {
        
        $newParticipant = Array(

            'vmoso_key' => $unformattedParticipantData['key'],
            'name' => $unformattedParticipantData['displayName']

        );
        
        return $newParticipant;
        
    }
    
    /**
     * Retrieves the list of participants for the chat/task with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return Array
     */
    public function getChatParticipants($key)
    {
        
        $taskData = $this->getTask($key);
        
        $listParticipants = $taskData['allParticipants'];
        $listFormattedParticipants = Array();

        for($i = 0; $i < count($listParticipants); ++$i)
        {
            
            array_push($listFormattedParticipants, $this->getFormattedChatParticipant($listParticipants[$i]));            
            
        }
                
        return $listFormattedParticipants;
        
    }
    
    /**
     * Retrieves the data for the chat/task with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return String
     */
    public function getTask($key)
    {
        
        if(!isset($this->taskCache[$key]))
        {
            
            $this->taskCache[$key] = $this->getConnection()->getTask($key);
            
        }
        
        return $this->taskCache[$key];
        
    }
    
    /**
     * Retrieves the count of chat messages from the Vmoso API.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getMessageCount()
    {
        
        $key = $this->getStoredChatKey();
        
        $resultData = Array(
            
            'calsy' => array(
                
                'id' => $key
                
            ),
            
            'api' => null
            
        );
        
        if(strlen($key) == 0)
        {
            
            return 0;
            
        }
        
        $resultData['api'] = $this->getConnection()->getCommentCount($key);
            
        return $resultData;
        
    }
    
    /**
     * Retrieves the list of the latest chat messages for the 
     * from the Vmoso API.
     * 
     * Will return null if no chat exists yet.
     * 
     * @author Peter Hamm
     * @param String $key The key of the chat to load the messages for
     * @param Array $page Optional, see http://sandbox.vmoso.com/swagger/docs/vmoso-api-doc.html#_paginationrecord - Default: empty
     * @return Array
     */
    public function getLatestMessages($key, $page = null)
    {
                
        $resultData = Array(
            
            'calsy' => array(
                
                'id' => $key
                
            ),
            
            'api' => null
            
        );
        
        if(strlen($key) == 0)
        {
            
            return null;
            
        }
                
        {
        
            $limit = 5;

            if(!is_null($page) && isset($page['limit']))
            {

                $limit = $page['limit'];

            }

            $apiResult = $this->getConnection()->getComments($key, $limit, $page);
            
            $resultData['api'] = $apiResult['comments'];

            $resultData['calsy']['page'] = $apiResult['pager'];
            $resultData['calsy']['can_comment'] = $apiResult['canComment'];

            $this->handleLoadedMessages($resultData);
            
        }
            
        return $resultData['api'];
        
    }
    
    /**
     * Tries to retrieve older messages by going further with the pagination.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getOlderMessages()
    {
        
        if(!is_null($this->nextPage) && !isset($this->nextPage['c_next']))
        {
            
            return Array();
            
        }
        
        return $this->getLatestMessages($this->nextPage);
        
    }
    
    /**
     * Retrieves the complete list of loaded messages of this object.
     * You can load messages into the list by calling 
     * $this->getLatestMessages() and $this->getOlderMessages()
     * 
     * @return type
     */
    public function getMessageList()
    {
        
        return $this->messageList;
        
    }
    
    /**
     * Handles loaded messages and stores them in this object's cache.
     * 
     * @author Peter Hamm
     * @param Array $resultData
     * @return void
     */
    protected function handleLoadedMessages($resultData)
    {
        
        // Save information on how to get the next page
        {
        
            $nextPage = $resultData['calsy']['page']['pg'];
            
            if(@$nextPage['c_next'])
            {
                
                // There is a next page, set the pointer to the following page.
                
                $nextPage['page'] = 'N';
                
            }
        
            $this->nextPage = $nextPage;
        
        }
        
        for($i = 0; $i < count($resultData['api']); ++$i)
        {
            
            $messageId = $resultData['api'][$i]['commentID'];
            
            if(!isset($this->messageList[$messageId]))
            {
                
                $this->messageList[$messageId] = $resultData['api'][$i];
                
            }
            
        }
        
    }
    
    /**
     * Retrieves information about how to retrieve the next page
     * @return type
     */
    public function getNextPageInfo()
    {
        
        return $this->nextPage;
        
    }
    
    /**
     * Tries to retrieve a chat.
     * A new chat will be created and returned if either none exists yet or
     * if the flag $forceNewChat is set to true.
     * 
     * @author Peter Hamm
     * @param bool $forceNewChat Optional, set to true to force the creation of a new chat. Default: false
     * @return Array
     */
    public function retrieveChat($forceNewChat = false)
    {
     
        $resultData = Array(
            
            'calsy' => array(
                
                'status' => 'none'
                
            ),
            
            'api' => null
            
        );
        
        $storedChatIdentifier = '';
        
        if(!$forceNewChat)
        {
            
            // Load the stored chat identifier (if available).
            
            $storedChatIdentifier = $this->getStoredChatKey();
                        
        }
        
        if(strlen($storedChatIdentifier) > 0)
        {
            
            // Load the existing task
                        
            try 
            {
                
                $resultData['api'] = $this->getConnection()->getTask($storedChatIdentifier);
                
                $createNewTask = $resultData['api']['enable'] == 'no';
                
            } 
            catch (Exception $e) 
            {
                
                // Could not load the task, create a new one.
                $createNewTask = true;
                
            }
            
            if($createNewTask)
            {
                
                // This task was deleted or disabled on the Vmoso server, create a new one
                
                return $this->retrieveChat(true);
                
            }
                 
            $resultData['calsy'] = Array(
                
                'id' => $resultData['api']['key'],
                'status' => 'existing'
                
            );
            
        }
        else
        {
            
            // Create a new task
            
            $this->createNewTask($resultData);
            
        }
        
        return $resultData;
        
    }
        
    /**
     * Formates the list of Vmoso chat messages in the format used by the system.
     * 
     * @author Peter Hamm
     * @param Array $messageList
     * @return Array
     */
    public function formatMessages($messageList)
    {
        
        $apiUser = $this->getConnection()->getUserInfo();
        
        $result = Array();
        
        for($i = 0; $i < count($messageList); ++$i)
        {
            
            $entry = Array(
                
                'identifier' => $messageList[$i]['commentID'],
                'date' => $messageList[$i]['timeupdated'],
                'content' => $messageList[$i]['text'],
                'type' => $messageList[$i]['creator']['key'] == $apiUser['key'] ? 'user' : 'remote',
                'creator_key' => $messageList[$i]['creator']['key'],
                'creator_name' => $messageList[$i]['creator']['displayName']
                
            );
            
            array_push($result, $entry);
            
        }
        
        return $result;
        
    }
    
    /**
     * Archives all of the content of the current chat to the database
     * and resets the identifier of the chat, so that a new chat can 
     * esily be created.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function archiveChat()
    {
        
        $chatLog = array();
        
        $messageCount = $this->getMessageCount();
        
        if(isset($messageCount['api']) && isset($messageCount['api']['all']) && $messageCount['api']['all'] > 0)
        {
            
            while(count($chatLog) < $messageCount['api']['all'])
            {
                
                $messages = $this->getOlderMessages();
                
                $formattedMessages = static::formatMessages($messages);
                
                $chatLog = array_merge($chatLog, $formattedMessages);
                
                
            }
            
            // Store it to the database...
            if(count($chatLog) > 0)
            {
            
                $this->archiveChatLog($chatLog);
            
            }
            
        }
        
    }
    
    /**
     * Takes a timestamp and puts out a formatted version of it, so it can be easily displayed.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return String
     */
    public static function getFormattedLogTime($timestamp)
    {
        
        $formattedTime = PerisianFrameworkToolbox::timestampToDateTime($timestamp) . ' ' . PerisianLanguageVariable::getVariable('10903');
        
        return $formattedTime;
        
    }
    
    /**
     * This should return the chat key from the last opened conversation on Vmoso.
     * The value is empty if thre is none.
     * 
     * @author Peter Hamm
     * @return String
     */
    abstract public function getStoredChatKey();
    
    /**
     * Should save the chat log to the database.
     * 
     * @author Peter Hamm
     * @param Array $chatLog
     * @return void
     */
    abstract protected function archiveChatLog($chatLog);
    
    /**
     * Creates a new task via the Vmoso API, it will generate a key to start a chat with.
     * 
     * @author Peter Hamm
     * @param Array $resultData
     * @return Array The result data.
     */
    abstract public function createNewTask(&$resultData = Array());
    
    /**
     * Retrieves the list of existing logs.
     * 
     * @author Peter Hamm
     * @return Array
     */
    abstract public function getMessageLogList();
    
}