<?php

require_once("CalsyVmosoApi.class.php");

/**
 * Functionality to store chat message logs.
 * 
 * @author Peter Hamm
 * @date 2017-03-22
 */
class CalsyVmosoChatUserFrontendLog extends PerisianDatabaseModel
{
    
    // Database specific members

    public $table = 'calsy_vmoso_chat_user_frontend_log';
    public $field_pk = 'calsy_vmoso_chat_user_frontend_log_id';
    public $field_user_frontend_id = 'calsy_vmoso_chat_user_frontend_log_user_frontend_id';
    public $field_content = 'calsy_vmoso_chat_user_frontend_log_content';
    public $field_last_update = 'calsy_vmoso_chat_user_frontend_log_last_update';
    public $field_count_messages = 'calsy_vmoso_chat_user_frontend_log_count_messages';

    /**
     * Creates a new object,
     * loads data if an entry's identifier is specified.
     *
     * @author Peter Hamm
     * @param int $entryId Optional, default: 0
     * @return void
     */
    public function __construct($entryId = 0)
    {

        parent::__construct('MySql', 'main');
        
        if(!empty($entryId) && $entryId > 0)
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

    }
    
    /**
     * Encodes the log data so it can be stored in the database.
     * 
     * @author Peter Hamm
     * @param Array $logData
     * @return String
     */
    public static function encodeLogData(Array $logData)
    {
        
        for($i = 0; $i < count($logData); ++$i)
        {
            
            $logData[$i]['content'] = base64_encode($logData[$i]['content']);
            
        }
        
        $logData = json_encode($logData);
        
        return $logData;
        
    }
    
    /**
     * Dcodes the log data so it can be displayed.
     * 
     * @author Peter Hamm
     * @param String $logData
     * @return Array
     */
    public static function decodeLogData($logData)
    {
        
        $logData = json_decode($logData, true);
                
        if(is_null($logData))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c7dad20c8d4'), $logData);
            
        }
        
        for($i = 0; $i < count($logData); ++$i)
        {
            
            $logData[$i]['content'] = base64_decode($logData[$i]['content']);
            
        }
        
        return $logData;
        
    }
    
    /**
     * Sets the log data for this object and automatically 
     * encodes it to be saved to the database.
     * 
     * @author Peter Hamm
     * @param Array $logData
     * @return void
     */
    public function setLogData(Array $logData)
    {
                
        $logData = static::encodeLogData($logData);
        
        $this->{$this->field_content} = $logData;
        
    }
    
    /**
     * Gets the log data for this object and automatically 
     * decodes it to be easily displayed.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getLogData()
    {
        
        $content = $this->{$this->field_content};
        
        $content = PerisianFrameworkToolbox::securityRevert($content);
                
        $logData = static::decodeLogData($content);
        
        return $logData;
        
    }
    
    /**
     * Retrieves a list of all chat logs for the specified filter.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return array
     */
    public static function getLogList($userFrontendId)
    {
                
        PerisianFrameworkToolbox::security($userFrontendId);
        
        if($userFrontendId <= 0)
        {
            
            // Invalid or missing parameter
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('10823'));
            
        }
        
        $instance = new static();
                
        $query = $instance->field_user_frontend_id . '="' . $userFrontendId . '"';
  
        $results = $instance->getData($query, $instance->field_last_update, "DESC");
        
        return $results;
        
    }
    
    /**
     * Retrieves the number of all chat logs for the specified filter.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return array
     */
    public static function getLogCount($userFrontendId)
    {
                
        PerisianFrameworkToolbox::security($userFrontendId);
        
        if($userFrontendId <= 0)
        {
            
            // Invalid or missing parameter
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('10823'));
            
        }
        
        $instance = new static();
                
        $query = $instance->field_user_frontend_id . '="' . $userFrontendId . '"';
  
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {
        
        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
}