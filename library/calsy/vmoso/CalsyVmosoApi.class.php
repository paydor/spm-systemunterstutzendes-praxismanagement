<?php

/**
 * Controller for the Vmoso API
 *
 * @author Peter Hamm
 * @date 2017-03-13
 */
class CalsyVmosoApi extends PerisianObject
{
     
    private $tokenSettingKey = 'module_setting_calsy_vmoso_connection_token_data';
    
    protected $connectionCredentials = Array(
        
        'server' => '',
        'client_id' => '',
        'user' => '',
        'password' => ''
        
    );
    
    protected $tokenData = Array();
    protected $allowTokenCache = false;
    
    protected static $errorCodesApiDefault = Array(
        
        '-600' => 'Missing assignees.',
        '-30' => 'User authentication error.',
        '-25' => 'Timestamp out of range.',
        '-24' => 'Unknown substitute user.',
        '-23' => 'Request authorization failure.',
        '-22' => 'Request authorization failure.',
        '-21' => 'Request security requirement (https) not met.',
        '-20' => 'No authenticated user.', 
        '-12' => 'Invalid/unknown request.',
        '-11' => 'Incomplete or missing input.',
        '-10' => 'Invalid or corrupt input.',
        '-9' => 'Remote system error.',
        '-4' => 'System maintenance error.',
        '-3' => 'Access control error.',
        '-2' => 'Internal system error.',
        '-1' => 'General system error.',
        '0' => 'Success!',
        '1' => 'Application error.',
        '2' => 'Item not found.',
        '3' => 'Item already exists (duplicate).',
        '4' => 'User has not been granted access to item.',
        '5' => 'User has no permission.',
        '6' => 'Invalid input.',
        '7' => 'Missing input.', 
        '23' => 'Auto self registration and send self registration email.',
        '24' => 'Reset password due to entering the wrong password more than 3 times.',
        '25' => 'User registered, but has not confirmed email.',
        '100' => 'User registered, but user name or password error.',
        '102' => 'Email address not allowed.',
        '104' => 'User alias already exists.',
        '105' => 'User already confirmed.', 
        '108' => 'Code 1 is a self invitation, code 2 is the invited contact is already a contact, and code 3 is an invalid email.',
        '109' => 'User not registered.',
        '200' => 'Insufficient permission to create a space.',
        '301' => 'Insufficient permission to delete a space.'
     
    );
        
    /**
     * Initializes the class
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct()
    {
        
        $this->checkCredentials();
        
        return parent::__construct();
        
    }
    
    /**
     * Tries to retrieve info about the Vmoso user that is currently connected to the API.
     * 
     * @author Peter Hamm
     * @return Array
     * @throws PerisianException
     */
    public function getUserInfo()
    {
        
        $data = array();
        
        $result = $this->makeRequest('users/me', $data, 'GET');
        
        $returnData = $result['displayRecord'];
        
        $returnData['email'] = $this->getUserInfoEmailAddress();
        
        return $returnData;
        
    }
    
    /**
     * Retrieves the email address of the Vmoso user of this connection.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getUserInfoEmailAddress()
    {
                
        return $this->getConnectionUser();
        
    }
    
    /**
     * Tries to retrieve a Vmoso user by their email address.
     * Will throw an Exception if the user is yet unknown to the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $emailAddress
     * @return Array
     * @throws PerisianException
     */
    public function getUserKeyForEmailAddress($emailAddress)
    {
        
        $data = array(
            
            'restricted' => false,
            'email' => Array($emailAddress)
            
        );
        
        $result = $this->makeRequest('tasks/participants/checkEmails', $data, 'GET');
        
        $returnData = @$result['emailInfo'][0];
        
        if(!isset($result['emailInfo']) || !isset($result['emailInfo'][0]) || $returnData['key'] == $emailAddress)
        {
            
            // User not found
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92fe651167'), null, 109);
            
        }
        
        return $returnData;
        
    }
        
    /**
     * Retrieves a task from the Vmoso API by its identifier.
     * 
     * @author Peter Hamm
     * @param String $taskId
     * @return Array
     */
    public function getTask($taskId)
    {
                        
        $data = Array();
        
        $result = $this->makeRequest('tasks/' . $taskId, $data, 'GET');
                
        $returnData = $result['task'];

        return $returnData;
        
    }   
    
    /**
     * Retrieves a paginated list of chats from the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $contentKey
     * @param Array $page Optional, see http://sandbox.vmoso.com/swagger/docs/vmoso-api-doc.html#_paginationrecord - Default: empty
     * @param int $limit Optional, default: 10
     * @return Array
     */
    public function getChatList($page = null, $limit = 10)
    {
        
        $page = ($page == null) ? "F" : $page;
        $limit = ($limit > 0) ? $limit : 10;
        
        $format = Array(
            
            'return_counts' => true,
            'return_ratings' => true,
            'return_sparc' => true,
            'return_acl' => true
            
        );
                        
        $data = Array(
            
            'pg.page' => $page,
            'pg.limit' => $limit,
                        
            'options.ruleKey' => 'activities',
            'options.highlightTag' => '',
            
            'options.filters' => Array(
                    
                Array(

                    'name' => 'levelFilter',
                    'value' => 'current'

                ),

                Array(

                    'name' => 'types',
                    'value' => 'chats'

                )

            ),
            
            'format' => json_encode($format)
            
        );
                
        $returnData = $this->makeRequest('streamv2/search', $data, 'GET');
        
        return $returnData;
        
    }
        
    /**
     * Retrieves comments for a content object from the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $contentKey
     * @param int $limit Optional, how many entries per page. Default: 10
     * @param Array $page Optional, see http://sandbox.vmoso.com/swagger/docs/vmoso-api-doc.html#_paginationrecord - Default: empty
     * @return Array
     */
    public function getComments($contentKey, $limit = 10, $page = null)
    {
        
        if($page == null)
        {
            
            $page = Array(
                
                'limit' => $limit,
                'page' => 'F'
                
            );
                        
        }
                
        $data = Array(
            
            'contentKey' => $contentKey,
            'pg' => json_encode($page)
            
        );
        
        $returnData = $this->makeRequest('comments/', $data, 'GET');
        
        return $returnData;
        
    }
    
    /**
     * Retrieves the count of comments for a content object from the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $contentKey
     * @return Array
     */
    public function getCommentCount($contentKey)
    {
                        
        $data = Array(
            
            'contentKey' => $contentKey
            
        );
        
        $result = $this->makeRequest('comment/counts', $data, 'GET');
        
        $returnData = $result['counts'];

        return $returnData;
        
    }
    
    /**
     * Posts a comment to a content object at the Vmoso API server.
     * 
     * @author Peter Hamm
     * @param String $contentKey
     * @param String $content
     * @return Array
     */
    public function postComment($contentKey, $content)
    {
                        
        $data = Array(
            
            'contentKey' => $contentKey,
            
            'comment' => Array(
                
                'text' => $content
                
            )
            
        );
        
        $result = $this->makeRequest('comments/create', $data, 'POST');
        
        $returnData = $result;

        return $returnData;
        
    }
    
    /**
     * Creates a new chat with the specified name and participants.
     * 
     * @author Peter Hamm
     * @param String $chatName
     * @param mixed $participantEmailAddresses Optional, either a list of or a single email address(es) of participants.
     * @param mixed $participantKeys Optional, either a list of or a single Vmoso user key(s) of participants.
     * @return Array
     */
    public function createChat($chatName, $participantEmailAddresses = Array(), $participantKeys = Array())
    {
        
        // Handle eventual email recipients
        {
        
            if(!is_array($participantEmailAddresses))
            {

                $participantEmailAddressesString = $participantEmailAddresses;

                $participantEmailAddresses = Array();

                if(strlen($participantEmailAddressesString) > 0)
                {

                    array_push($participantEmailAddresses, $participantEmailAddressesString);

                }

            }
            
        }
        
        // Handle eventual Vmoso user recipients
        {
        
            if(!is_array($participantKeys))
            {

                $participantKeysString = $participantKeys;

                $participantKeys = Array();

                if(strlen($participantKeysString) > 0)
                {

                    array_push($participantKeys, $participantKeysString);

                }

            }
            
        }
        
        $data = Array(
            
            'name' => $chatName,
            'addByEmail' => true
            
        );
        
        if(count($participantEmailAddresses) > 0)
        {
            
            $data['emails'] = $participantEmailAddresses;
            
        }
        
        if(count($participantKeys) > 0)
        {
            
            $data['keys'] = $participantKeys;
            
        }
        
        $result = $this->makeRequest('tasks/createChat', $data, 'POST');
        
        $returnData = $result['task'];
        
        return $returnData;
        
    }
    
    /**
     * Tries to load the stored connection token data from the database.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function loadStoredTokenData()
    {
        
        if($this->allowTokenCache)
        {
        
            $data = PerisianSystemSetting::getSettingValue($this->getTokenSettingKey());

            $decodedData = json_decode(stripslashes($data), true);

            $this->tokenData = $decodedData;
        
        }
        
    }
    
    /**
     * Sets the setting key for the API token.
     * 
     * @author Peter Hamm
     * @param String $key
     */
    public function setTokenSettingKey($key)
    {
        
        $this->tokenSettingKey = $key;
        
    }
    
    /**
     * Retrieves the setting key for the API token.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getTokenSettingKey()
    {
        
        return $this->tokenSettingKey;
        
    }
    
    /**
     * Stores this object's connection token data to the database.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected function storeTokenData()
    {
        
        if($this->allowTokenCache)
        {
        
            $value = json_encode($this->tokenData);

            $data = PerisianSystemSetting::setSettingValue($this->getTokenSettingKey(), $value);
            
        }
        
        return true;
        
    }
    
    /**
     * Tries to connect to the remote API server to generate a session token.
     * When successful, the session token is stored locally and is automatically reused for further API calls.
     * 
     * 
     * @author Peter Hamm
     * @param bool $forceRenew Optional, controls enforcing generation of a new token. Default: false.
     * @return Array Token data
     */
    protected function connect($forceRenew = false)
    {
        
        if($this->allowTokenCache && count($this->tokenData) == 0)
        {
            
            $this->loadStoredTokenData();
        
        }
        
        if($forceRenew || count($this->tokenData) == 0)
        {
                                    
            // We need to log in at the remote server anew.
            
            $data = Array(

                'auth' => Array(

                    'type' => 'password',
                    'username' => $this->getConnectionUser(),
                    'password' => $this->getConnectionPassword()

                ),

                'cid' => $this->getClientId()

            );
            
            $result = $this->makeRequest('login', $data, 'POST');
            
            $this->tokenData = @$result['cred'];
            
            $this->storeTokenData();
            
        }
        
        return $this->tokenData;
        
    }
    
    /**
     * Renews the connection token and automatically stores it to the database.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function renewToken()
    {
        
        $this->connect(true);
        
    }
    
    /**
     * Checks if all credentials for the connection are set.
     * If not, it'll try to load the credentials from the database, 
     * as they configured for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function checkCredentials()
    {
                
        if(strlen($this->connectionCredentials['server']) == 0)
        {
            
            $this->connectionCredentials['server'] = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_connection_server');
            
        }
        
        if(strlen($this->connectionCredentials['client_id']) == 0)
        {
            
            $this->connectionCredentials['client_id'] = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_connection_cid');
            
        }
        
        if(strlen($this->connectionCredentials['user']) == 0 && strlen($this->connectionCredentials['password']) == 0)
        {
            
            $this->connectionCredentials['user'] = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_connection_user');
            $this->connectionCredentials['password'] = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_connection_password');
            
        }
        
    }
    
    public static function onMaxExecutionTimeReached()
    {
        
       $error = error_get_last();
       
       throw new PerisianException('Timeout.', $error);

       die();
       
       print_r($error);
        
    }
    
    /**
     * Makes a request to the Vmoso API server and returns the result.
     * 
     * @author Peter Hamm
     * @param String $resource The resource on the remote server you want to request.
     * @param Array $data An associative array with the data to send.
     * @param String $type Optional, the request type. Default: 'GET'
     * @return Array
     * @throws PerisianException
     */
    protected function makeRequest($resource, $data = null, $type = 'GET')
    {
        
        //register_shutdown_function('CalsyVmosoApi::onMaxExecutionTimeReached');
        
        $this->checkCredentials();
        
        if(substr($resource, 0, 1) == '/')
        {
            
            $resource = substr($resource, 1);
            
        }
        
        if(count($this->tokenData) == 0 && $resource != 'login')
        {
                        
            $this->connect();
            
        }
        
        $type = strtoupper($type);
                
        // Build the connection string
        {
            
            $requestString = '';

            if(is_null($data))
            {
                
                $data = array();
                
            }
        
        }
        
        {
                        
            $resourceAddress = $this->getApiAddress() . $resource;
                                    
            $connection = curl_init();
            
            curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($connection, CURLOPT_TIMEOUT, 10);
            
            $headers = array();
            
            if($resource != 'login')
            {
                
                // Automatically add the token to the request.
                
                $headers = array(
                    
                    'X-CV-Authorization: TOKEN token="' . $this->tokenData['token'] . '"',

                );
            
            }

            if($type == 'POST')
            {
                
                curl_setopt($connection, CURLOPT_POST, true);
                
                if(!is_null($data))
                {
                    
                    $dataJson = json_encode($data);
                    
                    curl_setopt($connection, CURLOPT_POSTFIELDS, $dataJson);    
                    
                    array_push($headers, 'Content-Type: application/json');

                }
            
            }
            else
            {
                
                curl_setopt($connection, CURLOPT_CUSTOMREQUEST, $type); 
                
                $query = http_build_query($data);
                
                $resourceAddress .= (strpos($resourceAddress, "?") !== false ? "&" : "?") . $query;
                
            }
            
            // Take over the headers
            curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
            
            // Set the target URL
            curl_setopt($connection, CURLOPT_URL, $resourceAddress);
                        
            // Channel the output
            {
                
                $result = @curl_exec($connection);
                                
                $resultSuccessful = strlen($result) > 0;
                
            }
                  
            if(!$resultSuccessful)
            {
                           
                // The remote server could not be reached
                
                $connectionError = curl_error($connection);
                                
                $message = PerisianLanguageVariable::getVariable('p58a5ead79b720') . " (" . $type . ' @ "' . $resource . '")';
                
                throw new PerisianException($message, $connectionError);
                
            }
            
            curl_close($connection);
            
        }
        
        $result = json_decode(@$result, true);
                
        if(is_null(@$result))
        {
            
            // Unknown method
            
            $message = PerisianLanguageVariable::getVariable('10112') . "(" . $type . ' @ "' . $resource . '")';
            
            throw new PerisianException($message);
            
        }
        
        if($result['_hdr']['rc'] == -25 || $result['_hdr']['rc'] == -22)
        {
            
            // The token needs to be refreshed. Do it, then retry fetching the result.
            
            $this->renewToken();
            
            return $this->makeRequest($resource, $data, $type);
            
        }
        else if($result['_hdr']['rc'] != 0)
        {
            
            $this->handleErrorResult($resource, $type, $result);
            
        }
                
        return $result;
        
    }
    
    /**
     * Tries to retrieve a more detailed error message for
     * an API error code.      
     * 
     * @author Peter Hamm
     * @param int $errorCode
     * @return String
     */
    public static function getApiErrorMessageForCode($errorCode)
    {
        
        $message = PerisianLanguageVariable::getVariable('p58c7dad20c8d4');
                
        if(isset(static::$errorCodesApiDefault[$errorCode]))
        {
            
            $message = static::$errorCodesApiDefault[$errorCode];
            
        }
        
        return $message;
        
    }
    
    /**
     * Handles an API result set containing an error ('rc' code != 0) and
     * throws a proper exception.
     * 
     * @author Peter Hamm
     * @param String $resource The resource that was called and caused the error.
     * @param String $requestType The type of the request, e.g. "POST", "GET" and so on.
     * @param Array $result
     * @throws PerisianException
     * @return void
     */
    protected function handleErrorResult($resource, $requestType, $result)
    {
        
        $message = (isset($result['_hdr']['msg']) && strlen($result['_hdr']['msg']) > 0) ? $result['_hdr']['msg'] : '';
        
        if(substr($message, 0, 11) == '*error-ref:' || strlen($message) == 0)
        {
            
            // Retrieve a more helpful message.
            
            $message = $this->getApiErrorMessageForCode($result['_hdr']['rc']);
            
        }
        
        $debugInfo = $result;
        
        if($result['_hdr']['rc'] == 6)
        {
            
            // Invalid or missing parameter
            
            $message = PerisianLanguageVariable::getVariable('p58c92a7a3f1d4') . " ";
            
            if(!isset($result['_hdr']['errinfo']))
            {
                
                $message .= "?";
                
            }
            else
            {

                for($i = 0; $i < count(@$result['_hdr']['errinfo']); ++$i)
                {

                    if($i > 0)
                    {

                        $message .= ", ";

                    }

                    $message .= '"' . $result['_hdr']['errinfo'][$i]['name'] . '"';

                }
                
            }
            
        }
        else if($result['_hdr']['rc'] == -22 || $result['_hdr']['rc'] == -23)
        {
            
            // Authentication failed
            
            $message = PerisianLanguageVariable::getVariable('p58c92b6b49293');
            
        }
        
        $message .= " (" . $requestType . ' @ "' . $resource . '")';
        
        $exception = new PerisianException($message, $debugInfo, $result['_hdr']['rc']);
            
        throw $exception;
        
    }
    
    /**
     * Sets all of the credentials required for the connection to the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $clientId The client identificator provided by Vmoso.
     * @param String $user The Vmoso user name / account name to log on.
     * @param String $password The user's password.
     */
    public function setCredentials($clientId, $user, $password)
    {
        
        $this->setClientId($clientId);
        $this->setConnectionUser($user);
        $this->setConnectionPassword($password);
        
    }
    
    /**
     * Will validate the specified connection credentials.
     * 
     * @author Peter Hamm
     * @param String $clientId The client identificator provided by Vmoso.
     * @param String $user The Vmoso user name / account name to log on.
     * @param String $password The user's password.
     * @return bool True when valid, else false.
     */
    public function validateCredentials()
    {
                
        try
        {
            
            $this->connect();
            
        }
        catch(PerisianException $e)
        {
                        
            return false;
            
        }
                
        return true;
        
    }
    
    /**
     * Sets the client identifcator provided by Vmoso to connect to their API.
     * 
     * @author Peter Hamm
     * @param String $clientId
     * @throws PerisianException
     * @return void
     */
    protected function setClientId($clientId)
    {
        
        PerisianFrameworkToolbox::security($clientId);
        
        if(strlen($clientId) != 40)
        {
            
            // Invalid client identifier.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p57ab3ac859102'));
            
        }
        
        $this->connectionCredentials['client_id'] = $clientId;
                
    }
    
    /**
     * Sets the user / account name to connect to the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $user
     * @throws PerisianException
     * @return void
     */
    public function setConnectionUser($user)
    {
        
        PerisianFrameworkToolbox::security($user);
        
        if(strlen($user) < 3)
        {
            
            // Invalid user name.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c7cb273c7d2'));
            
        }
        
        $this->connectionCredentials['user'] = $user;
                
    }
    
    /**
     * Sets the password to connect to the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $password
     * @throws PerisianException
     * @return void
     */
    public function setConnectionPassword($password)
    {
        
        PerisianFrameworkToolbox::security($password);
        
        if(strlen($password) < 8)
        {
            
            // Invalid password.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c7cda7d78c1'));
            
        }
        
        $this->connectionCredentials['password'] = $password;
                
    }
    
    /**
     * Sets up whether token caching should be enabled or not.
     * It is disabled by default, in some cases it makes sense to allow it.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setAllowTokenCache($value)
    {
        
        if($value === true)
        {
            
            $this->allowTokenCache = true;
            
            return;
            
        }
        
        $this->allowTokenCache = false;
        
    }
    
    /**
     * Sets the Vmoso API server to connect to.
     * 
     * @author Peter Hamm
     * @param String $server
     * @throws PerisianException
     * @return void
     */
    public function setServer($server)
    {
        
        PerisianFrameworkToolbox::security($server);
        
        if(strlen($server) < 10)
        {
            
            // Invalid server address.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c7cbf80818b'));
            
        }
        
        $this->connectionCredentials['server'] = $server;
                
    }
    
    /**
     * Retrieves the user / account name used to connect to the Vmoso API.
     * 
     * @author Peter Hamm
     * @return String
     * @throws PerisianException
     */
    protected function getConnectionUser()
    {
                
        return $this->connectionCredentials['user'];
        
    }
    
    /**
     * Retrieves the password used to connect to the Vmoso API.
     * 
     * @author Peter Hamm
     * @return String
     * @throws PerisianException
     */
    protected function getConnectionPassword()
    {
                
        return $this->connectionCredentials['password'];
        
    }
    
    /**
     * Retrieves the client identificator used to connect to the Vmoso API.
     * 
     * @author Peter Hamm
     * @return String
     * @throws PerisianException
     */
    protected function getClientId()
    {
                
        return $this->connectionCredentials['client_id'];
        
    }
    
    /**
     * Retrieves the server address that has been set to this object.
     * 
     * @author Peter Hamm
     * @return String
     * @throws PerisianException
     */
    protected function getServer()
    {
        
        if(strlen($this->connectionCredentials['server']) < 10)
        {
            
            // Invalid server address.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c7cbf80818b'));
            
        }
        
        return $this->connectionCredentials['server'];
        
    }
    
    /**
     * Retrieves the full address of the remote api, depending on the set server address.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getApiAddress()
    {
                
        $server = $this->getServer();
        
        if(substr($server, -1) != '/')
        {
            
            $server .= '/';
            
        }
        
        if(substr($server, -5) != '/svc/')
        {
            
        
            $server .= '/svc/';
            
        }
        
        return $server;
        
    }
    
}
