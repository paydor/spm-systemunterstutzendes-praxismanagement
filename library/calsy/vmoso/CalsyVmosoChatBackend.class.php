<?php

require_once("CalsyVmosoApi.class.php");
require_once("abstract/CalsyVmosoChatConnectionAbstract.class.php");
require_once("CalsyVmosoChatUserFrontendLog.class.php");

/**
 * Live chat functionality via the Vmoso API for calSy backend users.
 * 
 * @author Peter Hamm
 * @date 2017-05-30
 */
class CalsyVmosoChatBackend extends CalsyVmosoChatConnectionAbstract
{
    
    protected $chatKey = '';
    protected $userBackend = null;
    
    protected $newChatTitle = '';
    protected $newChatParticipants = Array();
    
    /**
     * Creates a new instance, requires the identifier of an existing backend user.
     * 
     * @author Peter Hamm
     * @param int $userBackendId
     */
    public function __construct($userBackendId)
    {
        
        $this->userBackend = new CalsyUserBackend($userBackendId);
                        
        $vmosoUser = PerisianUserSetting::getSettingValue($userBackendId, 'user_vmoso_account'); 
        $vmosoPassword = PerisianUserSetting::getSettingValue($userBackendId, 'user_vmoso_account_password'); 
                
        if(strlen($vmosoUser) == 0 || strlen($vmosoPassword) == 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p59442dbec9549'));
            
        }
        
        parent::__construct();
        
        $this->connection->setConnectionUser($vmosoUser);
        $this->connection->setConnectionPassword($vmosoPassword);
        
    }
    
    /**
     * Retrieves the identifier of this object's associated backend user.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getUserBackendId()
    {
        
        return $this->userBackend->{$this->userBackend->field_pk};
        
    }
    
    /**
     * Retrieves a calsy backend user by the specified Vmoso key.
     * 
     * @author Peter Hamm
     * @param String $vmosoKey
     * @return CalsyUserBackend
     */
    public static function getUserBackendByVmosoKey($vmosoKey)
    {
        
        $user = null;
        
        $userList = PerisianUserSetting::getDependencyForSettingValue('user_vmoso_account_key', $vmosoKey);
        
        if(count($userList) == 1)
        {
            
            $user = new CalsyUserBackend($userList[0]);
            
        }
        
        return $user;
        
    }
    
    /**
     * Takes unformatted participant data, e.g. from a Vmoso task, 
     * and returns it in a more well formatted way.
     * 
     * @author Peter Hamm
     * @param Array $unformattedParticipantData
     * @return Array
     */
    protected function getFormattedChatParticipant($unformattedParticipantData)
    {
        
        $newParticipant = parent::getFormattedChatParticipant($unformattedParticipantData);
        
        // Check if this Vmoso user is connected to a calsy user.
        if(strlen($newParticipant['vmoso_key']) > 0)
        {
                        
            $user = CalsyVmosoChatBackend::getUserBackendByVmosoKey($newParticipant['vmoso_key']);
            
            if(!is_null($user))
            {
                
                $newParticipant['calsy_id'] = $user->{$user->field_pk};
                $newParticipant['name'] = $user->{$user->field_fullname};
                $newParticipant['image'] = $user->getImageProfileAddress();
                
            }
            
        }
        
        return $newParticipant;
        
    }
      
    /**
     * Formats the list of Vmoso chat messages in the format used by the system.
     * 
     * @author Peter Hamm
     * @param Array $messageList
     * @return Array
     */
    public function formatMessages($messageList)
    {
        
        $formattedMessages = parent::formatMessages($messageList);
        
        // Add profile images
        {
        
            $profileImageList = Array();

            for($i = 0; $i < count($formattedMessages); ++$i)
            {
                                
                if(!isset($profileImageList[$formattedMessages[$i]['creator_key']]))
                {
                    
                    // Find the calsy profile image for this Vmoso user key.
                    
                    $profileImageAddress = '';
                    
                    $userIdList = PerisianUserSetting::getDependencyForSettingValue('user_vmoso_account_key', $formattedMessages[$i]['creator_key']);
                    
                    if(count($userIdList) == 1)
                    {
                        
                        $profileImageAddress = CalsyUserBackend::getUserImageProfile($userIdList[0]);
                        
                    }
                    
                    $profileImageList[$formattedMessages[$i]['creator_key']] = $profileImageAddress;
                                        
                }
                
                
                $formattedMessages[$i]['image'] = $profileImageList[$formattedMessages[$i]['creator_key']];

            }
        
        }
        
        return $formattedMessages;
        
    }
    
    /**
     * If the current frontend user opened a chat before, this will return that
     * conversation's Vmoso thread identifier to load all content and messages from.
     * 
     * The value is empty if the user didn't create a chat yet or if the previous chat
     * has just been archived and no message was sent yet.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getStoredChatKey()
    {
        
        $chatId = $this->userBackend->getSetting(static::SETTING_KEY_USER_BACKEND_CHAT_ID);     
        $chatId = PerisianSetting::decryptString($chatId);
        
        return $chatId;
        
    }
    
    /**
     * Sets the Vmoso key used e.g. to send messages to a chat/task.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return void
     */
    public function setChatKey($key)
    {
        
        $this->chatKey = $key;
        
    }
    
    /**
     * Gets the Vmoso key used e.g. to send messages to a chat/task.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getChatKey()
    {
        
        return $this->chatKey;
        
    }
    
    /**
     * Sends a chat message via the Vmoso API.
     * 
     * @param String $message
     * @return Array
     */
    public function sendMessage($message)
    {
        
        $key = $this->getChatKey();
                        
        $resultData = Array(
            
            'calsy' => array(
                
                'id' => $key
                
            ),
            
            'api' => null
            
        );
        
        $this->sendMessageToChat($key, $message);
            
        return $resultData;
        
    }
    
    /**
     * Sets the data required to create a new task/chat.
     * 
     * @author Peter Hamm
     * @param String $newChatTitle
     * @param Array $newChatParticipants
     * @return void
     */
    public function setNewChatData($newChatTitle, $newChatParticipants)
    {
        
        $this->newChatTitle = $newChatTitle;
        $this->newChatParticipants = $newChatParticipants;
        
    }
         
    /**
     * Creates a new task via the Vmoso API, it will generate a key to start a chat with.
     * 
     * @author Peter Hamm
     * @param Array $resultData Optional
     * @return Array The result data.
     */
    public function createNewTask(&$resultData = Array())
    {
        
        if(!is_array($resultData))
        {
            
            $resultData = Array();
            
        }
        
        $chatName = PerisianLanguageVariable::getVariable('p58cbd16d3f05e');

        // Parameter verification
        {
            
            if(strlen($this->newChatTitle) < 1)
            {

                // Invalid chat title

                throw new PerisianException(PerisianLanguageVariable::getVariable('p58793ca0090a3'));

            }

            if(!is_array($this->newChatParticipants) || count($this->newChatParticipants) <= 1)
            {

                // Invalid participant list

                throw new PerisianException(PerisianLanguageVariable::getVariable('p59540f3709ce6'));

            }
            
            
        }

        $resultData['api'] = $this->getConnection()->createChat($this->newChatTitle, Array(), $this->newChatParticipants);

        //$this->userBackend->setSetting(static::SETTING_KEY_USER_BACKEND_CHAT_ID, $resultData['api']['key']);
        
        $resultData['calsy'] = Array(

            'id' => $resultData['api']['key'],
            'status' => 'new'

        );
        
        return $resultData;
        
    }
        
    /**
     * Retrieves a paginated, formatted list of chats from the Vmoso API.
     * 
     * @author Peter Hamm
     * @param String $contentKey
     * @param Array $page Optional, see http://sandbox.vmoso.com/swagger/docs/vmoso-api-doc.html#_paginationrecord - Default: empty
     * @param int $limit Optional, default: 10
     * @return Array
     */
    public function getChatList($page = null, $limit = 10)
    {
        
        $listUnformatted = $this->getConnection()->getChatList($page, $limit);
        
        $listFormatted = Array(
            
            'list' => Array(),
            
            'pager' => @$listUnformatted['pager']
            
        );
        
        for($i = 0; $i < count($listUnformatted['stream']); ++$i)
        {
            
            $formattedEntry = Array(
                
                'name' => @$listUnformatted['stream'][$i]['record']['name'],
                'text_short' => '',
                'key' => @$listUnformatted['stream'][$i]['record']['key'],
                'type' => @$listUnformatted['stream'][$i]['record']['type'],
                'status' => @$listUnformatted['stream'][$i]['record']['status'],
                'count_participants' => @$listUnformatted['stream'][$i]['record']['participantCount'],
                
                'time_created' => @$listUnformatted['stream'][$i]['record']['timecreated'],
                'time_updated' => @$listUnformatted['stream'][$i]['record']['timeupdated'],
                
                'time_created_formatted' => PerisianFrameworkToolbox::timestampToDateTime(@$listUnformatted['stream'][$i]['record']['timecreated']),
                'time_updated_formatted' => PerisianFrameworkToolbox::timestampToDateTime(@$listUnformatted['stream'][$i]['record']['timeupdated']),
                
                'creator' => Array(
                    
                    'type' => @$listUnformatted['stream'][$i]['record']['creator']['type'],
                    'key' => @$listUnformatted['stream'][$i]['record']['creator']['key'],
                    'email' => @$listUnformatted['stream'][$i]['record']['creator']['email'],
                    'name' => @$listUnformatted['stream'][$i]['record']['creator']['displayName']
                    
                )
                
            );
                                    
            if(isset($listUnformatted['stream'][$i]['last_action']) && @count($listUnformatted['stream'][$i]['last_action']['containers']) > 0)
            {
                
                $formattedEntry['text_short'] = static::formatChatText(@$listUnformatted['stream'][$i]['last_action']['containers'][0]['record']['text']);
                
                $formattedEntry['text_short'] = strip_tags($formattedEntry['text_short']);
                $formattedEntry['text_short'] = strlen($formattedEntry['text_short']) > 30 ? (substr($formattedEntry['text_short'], 0, 27) . '...') : $formattedEntry['text_short'];
                                
            }
            
            array_push($listFormatted['list'], $formattedEntry);
                        
        }
                
        return $listFormatted;
        
    }
    
    /**
     * Formats chat text coming from the Vmoso API, e.g. strips unnecessary HTML comments.
     * 
     * @author Peter Hamm
     * @param String $text
     * @return String
     */
    protected static function formatChatText($text)
    {
        
        // Strip HTML comments
        $text = preg_replace('/<!--(.*)-->/Uis', '', $text);
        
        $text = nl2br($text);
        
        return $text;
        
    }
    
    /**
     * Not implemented.
     * 
     * @author Peter Hamm
     * @param Array $chatLog
     * @return null
     */
    protected function archiveChatLog($chatLog)
    {
        
        return null;
        
    }
    
    /**
     * Not implemented.
     * 
     * @author Peter Hamm
     * @return null
     */
    public function getMessageLogList()
    {
        
        return null;
        
    }
    
}