<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/CalsyMasterDataModule.class.php';

/**
 * Master data controller
 *
 * @author Peter Hamm
 * @date 2020-03-04
 */
class CalsyMasterDataController extends PerisianController
{
        
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {

        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/master_data/overview'
            
        );
                
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        CalsyMasterData::deleteExpiredEntries();
        
        $dummy = new CalsyMasterData();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_pk);
        $search = $this->getParameter('search', '');
                        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/master_data/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
        //
        
        $result = array(
            
            'list' => CalsyMasterData::getMasterDataList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyMasterData::getMasterDataCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
                
    }
        
    /**
     * Deletes an entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     * @throws PerisianException
     */
    protected function actionDelete()
    {
        
        global $user;
                                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        if(!$user->isAdmin())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
            
        }
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyMasterData($deleteId);
        $entryObj->deleteInvitation();
        
        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }    
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }

    /**
     * Provides data for a form to create invitations
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionCreateNewInvitation()
    {
        
        $dummyUser = new CalsyUserFrontend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/master_data/create_invitation'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        
        $result = array(
            
            'title_form' => lg('p5e6165be6ed34'),
            
            'default_expiration_date' => time() + 7 * 86400,
            
            'list_users_invitation' => CalsyMasterData::getDisplayableUserList(),
            'possible_fields' => CalsyMasterData::getPossibleFieldsMeta(),
            
            'dummy_user' => $dummyUser
            
        );
        
                
        $this->setResultValue('result', $result);
        
    }    
    
    /**
     * Sends invitations to the users specified as parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSendInvitations()
    {
        
        global $user;
                                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        if(!$user->isAdmin())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
            
        }
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => lg('p5e617eff8831e')
            
        );
        
        try
        {
            
            // Sanitize the selected field
            {
                            
                $fieldsRaw = $this->getParameter('fields');
                
                $allowedFields = CalsyMasterData::getPossibleFieldsMeta();
                
                $sanitizedFields = Array();
                
                foreach($fieldsRaw as $field)
                {
                    
                    
                    foreach($allowedFields as $allowedField)
                    {
                        
                        if($allowedField['key'] == $field)
                        {
                            
                            array_push($sanitizedFields, $field);
                            
                            break;
                            
                        }
                        
                    }
                    
                }

            }
            
            $selectedUsers = $this->getParameter('recipients');
            
            foreach($selectedUsers as $userId)
            {
                
                $invitationEntry = CalsyMasterData::getActiveEntryForUser($userId);
                                
                if(is_null($invitationEntry))
                {
                    
                    $invitationEntry = new CalsyMasterData();
                    
                    $invitationEntry->{$invitationEntry->field_user_id} = $userId;
                    $invitationEntry->{$invitationEntry->field_code} = CalsyMasterData::createCode();
                    
                }
                
                $invitationEntry->{$invitationEntry->field_timestamp_expiration} = (int)$this->getParameter('time_expiration');
                $invitationEntry->{$invitationEntry->field_text_custom} = PerisianFrameworkToolbox::security($this->getParameter('text'));
                $invitationEntry->{$invitationEntry->field_requested_fields} = json_encode($sanitizedFields);
                
                if(time() < $invitationEntry->{$invitationEntry->field_timestamp_expiration})
                {
                
                    $invitationEntry->sendEmailInvitation();
                
                }
                
                $invitationEntry->save();
                
            }
            
            $result['success'] = true;
            $result['message'] = lg('p5e618288a7011');
                        
        }
        catch(PerisianException $e)
        {
            
            print_r($e);
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
}