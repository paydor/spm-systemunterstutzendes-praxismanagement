<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/master_data/CalsyMasterData.class.php';

/**
 * Master data invitation controller
 *
 * @author Peter Hamm
 * @date 2020-03-05
 */
class CalsyMasterDataInvitationController extends PerisianController
{
    
    /**
     * Updates the master data for the frontend user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUpdateMasterData()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
            
        $callbackCode = $this->getParameter('code');
        
        $result = Array(
            
            'code' => $callbackCode,
            'success' => false,
            'message' => lg('p5e619a805892d')
            
        );
        
        try
        {
             
            $invitation = CalsyMasterData::getEntryForCode($callbackCode);
            
            if(is_null($invitation))
            {
                
                throw new PerisianException(lg('p5e618ce4174cd'));
                
            }
                                    
            $userFrontendObject = new CalsyUserFrontend($invitation->{$invitation->field_user_id});
                    
            if($userFrontendObject->{$userFrontendObject->field_pk} > 0)
            {
                
                $validFields = $invitation->getRequestedFields();
                
                foreach($validFields as $fieldName)
                {
                                        
                    $parameter = $this->getParameter($fieldName);
                    
                    if($fieldName == $userFrontendObject->field_name_last || $fieldName == $userFrontendObject->field_name_first)
                    {
                        
                        if(strlen($parameter) == 0)
                        {
                            
                            throw new PerisianException(lg("p5e619e2b40ad1"));
                            
                        }
                        
                    }
                    
                    $userFrontendObject->{$fieldName} = $parameter;
                    
                }
                
                $newId = $userFrontendObject->save();
                
                {
                    
                    $customFields = $userFrontendObject->getCustomFields();

                    foreach($customFields as &$customField)
                    {

                        $customField['value'] = $this->getParameter('custom_field_' . $customField['id']);

                    }
                    
                    CalsyUserFrontend::updateCustomFieldsForUser($newId, $customFields);
                
                }
                
                $result['success'] = true;
                $result['message'] = lg('p5e619a9e6c331');
                                
            }
                            
        }
        catch(PerisianException $e)
        {
            
            $result['message'] = $e->getMessage();
            
        }
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays the landing page when a user clicks an invitation link in the email.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionLandingPage()
    {
        
        $renderer = array(
            
            'page' => 'frontend/calsy/master_data/invitation_landing_page'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        $isValid = false;
        
        $userFrontendObject = null;
        
        try
        {
            
            $callbackCode = $this->getParameter('e');
                        
            $invitation = CalsyMasterData::getEntryForCode($callbackCode);
            
            if(is_null($invitation))
            {
                
                throw new PerisianException(lg('p5e618ce4174cd'));
                
            }
                                    
            $userFrontendObject = new CalsyUserFrontend($invitation->{$invitation->field_user_id});
            $annotations = $invitation->{$invitation->field_text_custom};
                    
            if($userFrontendObject->{$userFrontendObject->field_pk} > 0)
            {
                
                $isValid = true;
                
                $message = nl2br(lg('p5e618eb631602'));
                
            }
            
        }
        catch(Exception $e)
        {
            
            $isValid = false;
            $message = $e->getMessage();
            
        }
        
        if(!$isValid)
        {
            
            $code = "";
            $message = nl2br(lg('p5e4c5ceee28e1'));
            
        }

        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        $result = Array(
            
            'isValid' => $isValid,
            'code' => $callbackCode,
            'text_annotations' => $annotations,
            'invitation' => $invitation,
            'message' => $message,
            'url_image_welcome' => $customLogoUrl,
            'object' => $userFrontendObject
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays a reply to either a confirmation or rejection.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSuccess()
    {
        
        $renderer = array(
            
            'page' => 'frontend/calsy/master_data/invitation_success'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $code = $this->getParameter('e');
        
        $message = lg('p5e619a9e6c331');
        
        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        $result = Array(
            
            'code' => $code,
            'message' => $message,
            'url_image_welcome' => $customLogoUrl
            
        );
                      
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'landingPage';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}