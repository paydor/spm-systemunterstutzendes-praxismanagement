<?php

require_once "calsy/order/CalsyOrder.class.php";
require_once "modules/CalsyUserFrontendRegistrationAndCalendarModule.class.php";
require_once 'framework/mail/PerisianMail.class.php';

/**
 * Master data
 *
 * @author Peter Hamm
 * @date 2020-03-05
 */
class CalsyMasterData extends PerisianDatabaseModel
{
            
    // Main table settings
    public $table = 'calsy_master_data';
    public $field_pk = 'calsy_master_data_id';
    public $field_user_id = 'calsy_master_data_user_id';
    public $field_code = 'calsy_master_data_code';
    public $field_timestamp_expiration = 'calsy_master_data_timestamp_expiration';
    public $field_text_custom = 'calsy_master_data_text_custom';
    public $field_requested_fields = 'calsy_master_data_requested_fields';
    
    protected $searchFields = Array("field_pk");
        
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        $entryId = (int)$entryId;
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);
            
            $this->{$this->field_pk} = $entryId;
            
        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Deletes all entries that have an expired timestamp.
     * 
     * @author Peter Hamm
     * @return void
     */
    public static function deleteExpiredEntries()
    {
        
        $object = new static();
        
        $timestmapTreshold = time();
        
        return $object->delete($object->field_timestamp_expiration . "<=" . $timestmapTreshold);
        
    }
    
    /**
     * Retrieves a list of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getMasterDataList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new static();
                
        $query = $instance->buildSearchString($search);
        
        if($orderBy == "pk")
        {
            
            $orderBy = $instance->field_pk;
            
        }
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
                
        return $results;
        
    }
        
    /**
     * Retrieves the count of all frontend user for the specified frontend user
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getMasterDataCount($search = "")
    {
        
        $instance = new static();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Tries to retrieve a currently active entry by user.
     * Will return null if none was found.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return CalsyMasterData
     */
    public static function getActiveEntryForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        $object = new static();
        
        $result = null;
                
        if((int)$userId > 0)
        {
            
            $query = $object->field_user_id . ' = "' . $userId . '" AND ' . $object->field_timestamp_expiration . ' > ' . time();

            $results = $object->getData($query);

            if(count($results) > 0)
            {

                $result = new static($results[0][$object->field_pk]);

            }
            
        }
        
        return $result;
        
    }
    
    /**
     * Generates a random callback code for invitations.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function createCode()
    {
        
        $code = 'calsy_md_cb_' . uniqid('cci');
        $code = md5($code);
        
        return $code;
        
    }
    
    /**
     * Retrieves an entry by user id and code
     * 
     * @author Peter Hamm
     * @param int $userId
     * @param String $code
     * @return CalsyMasterData
     */
    public static function getEntryForUserIdAndCode($userId, $code)
    {
        
        PerisianFrameworkToolbox::security($userId);
        PerisianFrameworkToolbox::security($code);
        
        $object = new static();
                
        if((int)$userId > 0 && strlen($code) > 0)
        {
            
            $query = $object->field_user_id . ' = "' . $userId . '" AND ' . $object->field_code . ' = "' . $code . '" AND ' . $object->field_timestamp_expiration . ' < ' . time();

            $results = $object->getData($query);

            if(count($results) > 0)
            {

                $object = new static($results[0][$object->field_pk]);

            }
            
        }
        
        return $object;
        
    }
    
    /**
     * Retrieves an entry by user id and code
     * 
     * @author Peter Hamm
     * @param String $code
     * @return CalsyMasterData
     */
    public static function getEntryForCode($code)
    {
                
        PerisianFrameworkToolbox::security($code);
        
        $object = new static();
        
        $result = null;
                
        if(strlen($code) > 0)
        {
            
            $query = $object->field_code . ' = "' . $code . '" AND ' . $object->field_timestamp_expiration . ' > ' . time();
            
            $results = $object->getData($query);

            if(count($results) > 0)
            {

                $result = new static($results[0][$object->field_pk]);

            }
            
        }
        
        return $result;
        
    }
       
    /**
     * Deletes the current invitation.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteInvitation()
    {
        
        $query = $this->field_pk . "=" . $this->{$this->field_pk};
                
        return parent::delete($query);
        
    }
    
    /**
     * Tries to load an entry by its code.
     * 
     * @author Peter Hamm
     * @param String $code
     * @return void
     */
    public function getInvitationByCode($code)
    {
        
        PerisianFrameworkToolbox::security($code);
        
        $query = $this->field_code . ' = "' . $code . '"';
        
        $this->loadData($query);
        
    }
    
    /**
     * Retrieves a list of all possible fields with their meta data (e.g. field name).
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getPossibleFieldsMeta()
    {
        
        $dummy = new CalsyUserFrontend();
        
        $fieldNames = Array(
            
            $dummy->field_name_first => lg('10560'),
            $dummy->field_name_last => lg('10559'),
            $dummy->field_sex => lg('10830'),
            $dummy->field_phone => lg('10486'),
            $dummy->field_phone_mobile => lg('p5e7278d605a1d'),
            $dummy->field_fax => lg('10562'),
            $dummy->field_address_street => lg('10175'),
            $dummy->field_address_street_number => lg('10554'),
            $dummy->field_address_additional => lg('10555'),
            $dummy->field_address_zip => lg('10556'),
            $dummy->field_address_city => lg('10565'),
            $dummy->field_address_country => lg('10177')
            
        );
        
        $fieldsWithNames = Array();
        
        // Standard fields
        {
            
            $validFields = CalsyUserFrontend::getSafeFields();

            foreach($validFields as $fieldKey)
            {

                $fieldMeta = Array(

                    'name' => $fieldNames[$fieldKey],
                    'key' => $fieldKey,
                    'is_custom' => false

                );

                array_push($fieldsWithNames, $fieldMeta);

            }
        
        }
                
        // Custom fields
        {
            
            $customFields = $dummy->getCustomFields();

            foreach($customFields as $customField)
            {
                
                $fieldMeta = Array(

                    'name' => $customField['label'],
                    'key' => $customField['id'],
                    'is_custom' => true

                );

                array_push($fieldsWithNames, $fieldMeta);

            }
        
        }
        
        return $fieldsWithNames;
        
    }
    
    /**
     * Retrieves the number of requested default fields for this entry.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getCountRequestedFieldsDefault()
    {
        
        $count = 0;
        
        $fields = $this->getRequestedFields();
        
        $possibleFields = static::getPossibleFieldsMeta();
        
        foreach($fields as $field)
        {
            
            foreach($possibleFields as $possibleField)
            {
                
                if($possibleField['key'] == $field)
                {
                    
                    if(!$possibleField['is_custom'])
                    {
                        
                        ++$count;
                        
                    }
                    
                    break;
                    
                }
                
            }
                        
        }
        
        return $count;
        
    }
    
    /**
     * Retrieves the number of requested custom fields for this entry.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getCountRequestedFieldsCustom()
    {
        
        $count = 0;
        
        $fields = $this->getRequestedFields();
        
        $possibleFields = static::getPossibleFieldsMeta();
        
        foreach($fields as $field)
        {
            
            foreach($possibleFields as $possibleField)
            {
                
                if($possibleField['key'] == $field)
                {
                    
                    if($possibleField['is_custom'])
                    {
                        
                        ++$count;
                        
                    }
                    
                    break;
                    
                }
                
            }
                        
        }
        
        return $count;
        
    }
    
    /**
     * Retrieves the requested fields for this entry.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getRequestedFields()
    {
        
        return json_decode($this->{$this->field_requested_fields}, true);
        
    }
    
    /**
     * Retrieves whether the field with the specified key is requested or not.
     * 
     * @author Peter Hamm
     * @param String $fieldKey
     * @return boolean
     */
    public function isFieldRequested($fieldKey)
    {
        
        $requestedFields = $this->getRequestedFields();
        
        return in_array($fieldKey, $requestedFields);
        
    }
    
    /**
     * Sends an e-mail out to this object's user, inviting them to fill out their master data.
     * 
     * @author Peter Hamm
     * @return boolean Could the e-mail be sent or not?
     */
    public function sendEmailInvitation()
    {
        
        if((int)$this->{$this->field_user_id} <= 0 || strlen($this->{$this->field_code}) < 3)
        {
            
            return false;
            
        }
        
        $userFrontend = new CalsyUserFrontend($this->{$this->field_user_id});
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $userFrontend->{$userFrontend->field_language_id};

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
                
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                //field_text_custom
                
                $subject = PerisianLanguageVariable::getVariable('p5e616d42da7f2');
                
                $invitationLink = PerisianFrameworkToolbox::getServerAddress(false) . "master_data/?e=" . $this->{$this->field_code};

                $content = PerisianLanguageVariable::getVariable('p5e616e696f590') . "\n\n";
                $content = str_replace("%1", $userFrontend->getFullName(), $content);
                $content = str_replace("%2", $invitationLink, $content);
                $content = str_replace("%3", PerisianFrameworkToolbox::timestampToDateTime($this->{$this->field_timestamp_expiration}), $content);
                $content = str_replace("%4", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                if(strlen($this->{$this->field_text_custom}) > 0)
                {
                    
                    $annotationText = "\n" . lg('p5e67e0ba8344a') . "\n" . $this->{$this->field_text_custom} . "\n";
                    
                    $content = str_replace("%5", $annotationText, $content);
                                        
                }
                else
                {
                    
                    $content = str_replace("%5", "", $content);
                    
                }
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $userFrontend->getContactMailAddress(), 'name' => $userFrontend->getFullName())

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Retrieves a list of users for the invitations
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getDisplayableUserList()
    {
        
        $dummy = new CalsyUserFrontend();
                
        $list = array();
        
        $listUserFrontend = CalsyUserFrontend::getUserFrontendList('', $dummy->fullname, "ASC", 0, 0);
        
        foreach($listUserFrontend as $userFrontend)
        {
            
            $invitation = static::getActiveEntryForUser($userFrontend[$dummy->field_pk]);
            
            $entry = Array();
            
            $entry['id'] = $userFrontend[$dummy->field_pk];
            $entry['name'] = $userFrontend[$dummy->fullname];
            
            if(!is_null($invitation))
            {
                
                $entry['invitation_sent'] = true;
                $entry['invitation_code'] = $invitation->{$invitation->field_code};
                $entry['invitation_date_expiration'] = $invitation->{$invitation->field_timestamp_expiration};
                
            }
            
            $entry['selected'] = false;
                        
            array_push($list, $entry);
            
        }
        
        return $list;
        
    }
        
}
