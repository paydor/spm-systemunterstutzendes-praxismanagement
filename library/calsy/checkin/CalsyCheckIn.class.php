<?php

require_once "CalsyCheckInLog.class.php";

/**
 * Checkins
 *
 * @author Peter Hamm
 * @date 2020-06-15
 */
class CalsyCheckIn extends PerisianDatabaseModel
{
    
    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';
    
    // Main table settings
    public $table = 'calsy_checkin';
    public $field_pk = 'calsy_checkin_id';
    public $field_fk = 'calsy_checkin_user_frontend_id';
    public $field_time_in  = 'calsy_checkin_time_in';
    public $field_time_out = 'calsy_checkin_time_out';
    public $field_status = 'calsy_checkin_status';
    public $field_description = 'calsy_checkin_description';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_fk', 'field_description');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
        
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $filter Optional, default: null
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '', $filter = null)
    {
        
        PerisianFrameworkToolbox::security($filter);
        
        $query = $this->buildSearchString($search);
        
        $containedAreaIds = Array();
        
        // Filter
        {
            
            if(is_array($filter) && isset($filter['user_frontend']) && is_array($filter['user_frontend']) && count($filter['user_frontend']) == 1 && (int)$filter['user_frontend'][0] > 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_fk . " = \"" . $filter['user_frontend'][0] . "\"";
                
            }
                        
        }
        
        return $query;
        
    }
    
    /**
     * Does a quick checkin for the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @param String $description Optional, a description for the entry. Default: empty string.
     * @return boolean
     */
    public static function doQuickCheckInForUserFrontend($userFrontendId, $description = '')
    {
        
        $userFrontendId = (int)$userFrontendId;
        
        if($userFrontendId <= 0)
        {
            
            return false;
            
        }
        
        $object = new static();
        
        $object->{$object->field_status} = static::STATUS_OPEN;
        $object->{$object->field_fk} = $userFrontendId;
        $object->{$object->field_time_in} = time();
        
        if(strlen($description) > 0)
        {
            
            $object->{$object->field_description} = $description;
            
        }
        
        $object->save();
        
        return true;
        
    }
    
    /**
     * Fired directly after the object was saved to the database.
     * Can be overriden.
     * 
     * @author Peter Hamm
     * @param int $primaryKey Optional, default: ''
     * @return void
     */
    protected function onSaveCompleted($primaryKey = '')
    {
        
        $this->{$this->field_pk} = $primaryKey;
        
        CalsyCheckInLog::logCheckIn($this);
       
    }
    
    /**
     * Does a quick checkout for the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return boolean
     */
    public static function doQuickCheckOutForUserFrontend($userFrontendId)
    {
        
        $userFrontendId = (int)$userFrontendId;
        
        if($userFrontendId <= 0)
        {
            
            return false;
            
        }
        
        $filter = Array(
            
            'user_frontend' => Array($userFrontendId)
            
        );
        
        $list = static::getCheckInList("", $dummy->field_time_in, "DESC", 0, 1, $filter);
        
        $dummy = static::getInstance();
                
        if(count($list) == 1)
        {
            
            
            $object = new static($list[0][$dummy->field_pk]);
            
            print_r($object);
            $object->{$object->field_status} = static::STATUS_CLOSED;
            $object->{$object->field_time_out} = time();
                        
            $object->save();
            
        }
        
        return true;
        
    }
    
    /**
     * Retrieves information about the checkin status for the specified frontend user.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return Array
     */
    public static function getCheckInStatusForUserFrontend($userFrontendId)
    {
        
        $result = Array(
            
            'status' => static::STATUS_CLOSED,
            'status_since' => 0
            
        );
      
        $dummy = new static();
        
        $userFrontendId = (int)$userFrontendId;
        
        if($userFrontendId <= 0)
        {
            
            return $result;
            
        }
        
        $filter = Array(
            
            'user_frontend' => Array($userFrontendId)
            
        );
        
        $list = static::getCheckInList("", $dummy->field_time_in, "DESC", 0, 1, $filter);
        
        if(count($list) == 1)
        {
            
            $result['data'] = $list[0];
            $result['status'] = $list[0][$dummy->field_status];
            $result['status_since'] = $list[0][$dummy->field_status] == static::STATUS_OPEN ? $list[0][$dummy->field_time_in] : $list[0][$dummy->field_time_out];
            
        }
        
        return $result;
        
    }
        
    /**
     * Retrieves a list of checkins
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param mixed $filter Optional, default: null
     * @return array
     */
    public static function getCheckInList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0, $filter = null)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of checkins
     * 
     * @author Peter Hamm
     * @param String $search
     * @param mixed $groupFilter Optional, default: null
     * @return array
     */
    public static function getCheckInCount($search = "", $filter = null)
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
        
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteCheckIn()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
