<?php

require_once 'CalsyCheckIn.class.php';
require_once 'calsy/vmoso/CalsyVmosoApi.class.php';

class CalsyCheckInLogSynch extends PerisianDatabaseModel
{
    
    const TYPE_VMOSO = 'vmoso';
       
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';
    
    // Main table settings
    public $table = 'calsy_checkin_log_synch';
    public $field_pk = 'calsy_checkin_log_synch_id';
    public $field_type  = 'calsy_checkin_log_synch_type';
    public $field_time = 'calsy_checkin_log_synch_time';
    public $field_status = 'calsy_checkin_log_synch_status';
    public $field_meta = 'calsy_checkin_log_synch_meta';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk');
    
    protected static $entryList = array();
    
    protected static $instance;
    protected static $connectionVmoso;
    
    /**
     * Retrieves possible synchronisation times for Vmoso.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getSynchTimesVmoso()
    {
        
        $text = lg('p5efdfc38df2be');
        
        $time = 0;
        
        $times = Array();
        
        $stepsPerHour = 6;
        
        for($i = 0; $i < 24 * $stepsPerHour; ++$i)
        {
            
            $times[$time] = str_replace('%', gmdate("H:i", $time), $text);
            
            $time += 60 * 60 / $stepsPerHour;
            
        }
        
        return $times;
        
    }
    
    /**
     * Retrieves whether the checkin synchronisation for Vmoso is enabled.
     * 
     * @author Peter Hamm
     * @return Boolean
     */
    public static function isEnabledVmoso()
    {
        
        return PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_enabled') == "1";
        
    }
    
    /**
     * Retrieves log entries for the specified type and renders them into a form that can be synchronizes  for the specified type.  
     * 
     * You can also specify a timestamp, all entries created after it will be posted.
     * If you leave the default value, -1, only those new log entries since the last post will be posted.
     * 
     * @author Peter Hamm
     * @param String $type Foir example static::TYPE_VMOSO
     * @param int $fromTime Optional, default: -1
     * @return void
     */
    public static function getSynchContent($type, $fromTime = -1)
    {
        
        $validTypes = Array(static::TYPE_VMOSO);
        
        if(!in_array($type, $validTypes))
        {
            
            throw new PerisianException(lg('p59e66faf1f97b'));
            
        }
        
        $content = null;
        $filter = null;
        
        $dummy = CalsyCheckInLog::getInstance();
        
        if($fromTime >= 0)
        {
            
            $filter = Array(
                
                'minimum_time' => $fromTime
                
            );
            
        }
        
        $entries = CalsyCheckInLog::getCheckInLogList("", $dummy->field_time_log, "ASC", 0, 0, $filter);
                
        if($type == static::TYPE_VMOSO)
        {
            
            $lastSynchTime = CalsyCheckInLogSynch::getLastSynchTime($type);
            
            $timeBegin = $lastSynchTime < 0 ? lg('p5efe026933da0') : PerisianFrameworkToolbox::timestampToDateTime($lastSynchTime);
            
            $content = lg('p5efe01b40535d');
            $content = str_replace("%1", $timeBegin, $content);
            $content = str_replace("%2", PerisianFrameworkToolbox::timestampToDateTime(), $content);
            
            $rowCount = 0;
            
            if(count($entries) == 0)
            {
                
                $content .= "<br/>\n<br/>\n" . lg('p5efe02de0ea8e');
                
            }
            else
            {
                
                $table = '<table style="border: 1px solid #ccc" cellpadding="0" cellspacing="0">';
                
                $table .= '<tr style="background-color: #ccc; color: #000 padding: 12px; border: 1px solid #ccc">';
                
                $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
                $table .= lg('10964'); // Date and time
                $table .= '</td>';
                
                $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
                $table .= lg('10888'); // Further information
                $table .= '</td>';
                
                $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
                $table .= lg('p5e616611e37fe'); // User
                $table .= '</td>';
                
                $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px;">';
                $table .= lg('p5efb4ea4985b6'); // Log time
                $table .= '</td>';
                
                $table .= '<tr>';
                
                foreach($entries as $entry)
                {
                    
                    $backgroundColor = $rowCount % 2 == 0 ? '#fff' : '#eee';
                    
                    $row = '<tr style="background-color: ' . $backgroundColor . '">';
                                        
                    $row .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: center;">';
                    $row .= date("H:i", $entry[$dummy->field_time]) . ' ' . lg('10903'); // Date and time
                    $row .= '</td>';

                    $row .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: left;">';
                    $row .= lg('p5ee8e7636e3da') . ': "' . $entry[$dummy->field_description] . '"<br>' . "\n"; // Further information
                    $row .= lg('p5ee8e77f7f1be') . ': ' . lg($entry[$dummy->field_status] == CalsyCheckIn::STATUS_OPEN ? 'p5ee8e7d286fc9' : 'p5ee8e7e89a615');
                    $row .= '</td>';

                    $row .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: left;">';
                    $row .= CalsyUserFrontend::getFullnameForUserById($entry[$dummy->field_fk]); // User
                    $row .= '</td>';

                    $row .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; text-align: center;">';
                    $row .= PerisianFrameworkToolbox::timestampToDateTime($entry[$dummy->field_time]); // Log time
                    $row .= '</td>';
                    
                    $row .= '</tr>';
                    
                    $table .= "\n" . $row;
                    
                    ++$rowCount;
                    
                }
                
                $table .= '</table>';
                
                $content .= "<br/>\n<br/>\n" . $table;
                
                
            }
            
        }
        
        return $content;
        
    }
    
    /**
     * Synchronises log entries to Vmoso. 
     * You can specify a timestamp, all entries created after it will be posted.
     * If you leave the default value, -1, only those new log entries since the last post will be posted.
     * 
     * @author Peter Hamm
     * @param int $fromTime Optional, default: -1
     * @return void
     */
    public static function synchToVmoso($fromTime = -1)
    {
        
        if((int)$fromTime < 0)
        {
            
            $fromTime = static::getLastSynchTime(static::TYPE_VMOSO);
            
        }
           
        $content = static::getSynchContent(static::TYPE_VMOSO, $fromTime);
                
        $connection = static::getConnectionVmoso();
        
        $contentKey = PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_connection_chat_id');
        
        $connection->postComment($contentKey, $content);
        
    }
    
    /**
     * Retrieves the last time of synchronisation for the specified type.
     * Will return -1 if there was no synchronisation yet.
     * 
     * @author Peter Hamm
     * @param String $type For example static::TYPE_VMOSO
     */
    public static function getLastSynchTime($type)
    {
        
        $time = -1;
        
        $dummy = static::getInstance();
            
        if($type == static::TYPE_VMOSO)
        {
         
            $query = $dummy->field_type . ' = "' . static::TYPE_VMOSO . '" AND ' . $dummy->field_status . ' = "' . static::STATUS_SUCCESS . '"';
                        
            $results = $dummy->getData($query, $dummy->field_time, "DESC", 0, 1);

            if(count($results) == 1)
            {
                
                $time = $results[0][$dummy->field_time];
                
            }
            
        }
        
        return $time;
        
    }
    
    /**
     * Retrieves a connection to the Vmoso API with the configured credentials.
     * 
     * @author Peter Hamm
     * @return CalsyVmosoApi
     */
    public static function getConnectionVmoso()
    {
        
        if(is_null(static::$connectionVmoso))
        {
            
            $server = PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_connection_server');
            $clientId = PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_connection_cid');
            $user = PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_connection_user');
            $password = PerisianSystemSetting::getSettingValue('module_setting_calsy_checkin_synch_vmoso_connection_password');
            
            static::$connectionVmoso = new CalsyVmosoApi();
            
            static::$connectionVmoso->setServer($server);
            static::$connectionVmoso->setTokenSettingKey('module_setting_calsy_checkin_synch_vmoso_connection_token_data');
            static::$connectionVmoso->setCredentials($clientId, $user, $password);
                                    
        }
        
        return static::$connectionVmoso;
        
    }
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
}