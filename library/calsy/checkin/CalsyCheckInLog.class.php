<?php

/**
 * Checkins
 *
 * @author Peter Hamm
 * @date 2020-06-29
 */
class CalsyCheckInLog extends PerisianDatabaseModel
{
        
    // Main table settings
    public $table = 'calsy_checkin_log';
    public $field_pk = 'calsy_checkin_log_id';
    public $field_fk = 'calsy_checkin_log_user_frontend_id';
    public $field_fk_secondary = 'calsy_checkin_log_checkin_id';
    public $field_time  = 'calsy_checkin_log_time';
    public $field_status = 'calsy_checkin_log_status';
    public $field_description = 'calsy_checkin_log_description';
    public $field_meta = 'calsy_checkin_log_meta';
    public $field_time_log  = 'calsy_checkin_log_time_log';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_fk', 'field_description');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
        
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $filter Optional, default: null
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '', $filter = null)
    {
        
        PerisianFrameworkToolbox::security($filter);
        
        $query = $this->buildSearchString($search);
        
        $containedAreaIds = Array();
        
        // Filter
        {
            
            if(is_array($filter) && isset($filter['user_frontend']) && is_array($filter['user_frontend']) && count($filter['user_frontend']) == 1 && (int)$filter['user_frontend'][0] > 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_fk . " = \"" . $filter['user_frontend'][0] . "\"";
                
            }
            
            if(is_array($filter) && isset($filter['minimum_time']) && (int)$filter['minimum_time'] >= 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_time_log . " >= \"" . $filter['minimum_time'] . "\"";
                
            }
                         
        }
        
        return $query;
        
    }
    
    /**
     * Logs the current status of a CalsyCheckIn.
     * 
     * @author Peter Hamm
     * @param CalsyCheckIn $checkIn
     * @param Array $metaData Optional, default: Empty array
     * @return boolean
     */
    public static function logCheckIn(CalsyCheckIn $checkIn, $metaData = Array())
    {
        
        $object = new static();
        
        $object->{$object->field_fk} = $checkIn->{$checkIn->field_fk};
        $object->{$object->field_fk_secondary} = $checkIn->{$checkIn->field_pk};
        $object->{$object->field_time} = $checkIn->{$checkIn->field_status} == CalsyCheckIn::STATUS_CLOSED ? $checkIn->{$checkIn->field_time_out} : $checkIn->{$checkIn->field_time_in};
        $object->{$object->field_status} = $checkIn->{$checkIn->field_status};
        $object->{$object->field_description} = $checkIn->{$checkIn->field_description};
        $object->{$object->field_meta} = json_encode($metaData);
        $object->{$object->field_time_log} = time();
                
        $object->save();
        
        return true;
        
    }
    
    /**
     * Retrieves a list of checkin log entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param mixed $filter Optional, default: null
     * @return array
     */
    public static function getCheckInLogList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0, $filter = null)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of checkins
     * 
     * @author Peter Hamm
     * @param String $search
     * @param mixed $filter Optional, default: null
     * @return array
     */
    public static function getCheckInLogCount($search = "", $filter = null)
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
        
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteCheckInLog()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
