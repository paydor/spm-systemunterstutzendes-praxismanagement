<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/checkin/CalsyCheckIn.class.php';

/**
 * Checkin log controller
 *
 * @author Peter Hamm
 * @date 2020-06-29
 */
class CalsyCheckInLogController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/checkin/log/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyCheckInLog();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        $sorting = $this->getParameter('sorting', $dummy->field_time_log);
        $search = $this->getParameter('search', '');
        $userFilter = $this->getParameter('u', '');
        $filterUserFrontend = (int)$userFilter > 0 ? new CalsyUserFrontend($userFilter) : new CalsyUserFrontend(0);
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/checkin/log/list'
            
        );
        
        $filter = Array(
            
            'user_frontend' => Array($userFilter)
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyCheckInLog::getCheckInLogList($search, $sorting, $sortOrder, $offset, $limit, $filter),
            'count' => CalsyCheckInLog::getCheckInLogCount($search, $filter),
                        
            'dummy' => $dummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            'filter' => $filter,
            'filter_user_frontend' =>$filterUserFrontend
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information for different sources in JSON format.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $result = array(
            
            'success' => false
            
        );
        
        if($source == "user_frontend")
        {
            
            if(CalsyUserFrontendModule::isEnabled()) 
            {
                                
                $userFrontendDummy = new CalsyUserFrontend();
                
                $result = CalsyUserFrontend::getUserFrontendListFormatted("", "calsy_user_frontend_fullname", "ASC", 0, 0, $this->getParameter('showDefaultUser') != 'false');
                
            }
            
        }
        else if($source == "entry")
        {
                        
            $entry = new CalsyCheckInLog($this->getParameter('e'));
            $result = $entry->getSummary();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}