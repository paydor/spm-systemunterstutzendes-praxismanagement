<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/checkin/CalsyCheckIn.class.php';

/**
 * Checkin frontend controller
 *
 * @author Peter Hamm
 * @date 2020-06-15
 */
class CalsyCheckInFrontendController extends PerisianController
{
    
    /**
     * Checks the $_SERVER variable to see if a frontend user has opened a certain URL
     * and displays the landing page for the checkin system accordingly. 
     * 
     * @author Peter Hamm
     * @global Array $_SERVER
     * @return mixed Array or "false".
     */
    public static function checkDisplayLandingPageHook()
    {
        
        global $_SERVER;
                        
        if(strpos($_SERVER['SCRIPT_NAME'], '/frontend/index.php') !== false)
        {
            
            $data = static::getLandingPageData();
            
            return $data !== false && @$data['enabled'];
            
        }
        
        return false;
        
    }
    
    /**
     * Does a quick checkin for the currently logged in frontend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    public function actionQuickCheckIn()
    {
        
        global $userFrontend;
        
        $renderer = Array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            
            $landingPageData = static::getLandingPageData();
                        
            $success = CalsyCheckIn::doQuickCheckInForUserFrontend($userFrontend->{$userFrontend->field_pk}, @$landingPageData['description']);
            
        }
        
        $result = Array(
            
            'success' => $success
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Does a quick checkout for the currently logged in frontend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    public function actionQuickCheckOut()
    {
        
        global $userFrontend;
        
        $renderer = Array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $success = CalsyCheckIn::doQuickCheckOutForUserFrontend($userFrontend->{$userFrontend->field_pk});
            
        }
        
        $result = Array(
            
            'success' => $success
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays the current status of the user's checking and enables him
     * to change it.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    public function actionStatusCheckIn()
    {
        
        global $userFrontend;
        
        $renderer = Array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/checkin/landing_page/status'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = CalsyCheckIn::getCheckInStatusForUserFrontend($userFrontend->{$userFrontend->field_pk});
        
        $this->setResultValue('result', $result);
        
    }
    /**
     * Displays the message that the user who just registered or tried to log in is still inactive.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function actionInfoUserInactive()
    {
        
        $renderer = Array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/checkin/landing_page/user_inactive'
            
        );
                
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array();
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves landing page meta data.
     * 
     * @author Peter Hamm
     * @global Array $_SERVER
     * @return mixed Array or "false".
     */
    static public function getLandingPageData()
    {

        global $_SERVER;
        
        $hosts = Array(
            
            'schwimmbad.triathlonkids.de' => Array(
                
                'enabled' => true,
                'description' => lg('p5ee8e73cb0fcd')
                
            )
            
        );
        
        if(isset($_SERVER['HTTP_HOST']) || isset($_SERVER['SERVER_NAME']))
        {

            foreach($hosts as $host => $data)
            {

                if(strpos($_SERVER['HTTP_HOST'], $host) !== false || strpos($_SERVER['SERVER_NAME'], $host) !== false)
                {

                    return $data;

                }

            }

        }
        
        return false;
        
    }
        
    /**
     * Displays the landing page for the checkin, depending on the current URL.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionLandingPage()
    {
                
        $renderer = array(
            
            'page' => 'frontend/calsy/checkin/landing_page/landing_page'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $landingPageData = static::getLandingPageData();
        
        // Password policy info
        {

            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();

            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;

        }
        
        $result = array(
            
            'data' => $landingPageData,
            'text_gtc' => PerisianFrameworkToolbox::securityRevert(PerisianSystemSetting::getSettingValue('frontend_gtc_registration')),
            'security_policy_password' => $passwordPolicyInfo
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'frontend/calsy/checkin/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend;
     * @return void
     */
    protected function actionList()
    {
        
        global $userFrontend;
        
        $dummy = new CalsyCheckIn();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        $sorting = $this->getParameter('sorting', $dummy->field_time_in);
        $search = $this->getParameter('search', '');
        $userFilter = $userFrontend->{$userFrontend->field_pk};
        $filterUserFrontend = $userFrontend;
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/checkin/list'
            
        );
        
        $filter = Array(
            
            'user_frontend' => Array($userFilter)
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyCheckIn::getCheckInList($search, $sorting, $sortOrder, $offset, $limit, $filter),
            'count' => CalsyCheckIn::getCheckInCount($search, $filter),
                        
            'dummy' => $dummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            'filter' => $filter,
            'filter_user_frontend' =>$filterUserFrontend
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information for different sources in JSON format.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $result = array(
            
            'success' => false
            
        );
        
        if($source == "user_frontend")
        {
            
            if(CalsyUserFrontendModule::isEnabled()) 
            {
                                
                $userFrontendDummy = new CalsyUserFrontend();
                
                $result = CalsyUserFrontend::getUserFrontendListFormatted("", "calsy_user_frontend_fullname", "ASC", 0, 0, $this->getParameter('showDefaultUser') != 'false');
                
            }
            
        }
        else if($source == "entry")
        {
                        
            $entry = new CalsyCheckIn($this->getParameter('e'));
            $result = $entry->getSummary();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', lg('p5ee8e29ee694b'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummy = new CalsyCheckIn();
        $dummyUser = new CalsyUserFrontend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/checkin/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                        
        $editCheckIn = new CalsyCheckIn($this->getParameter('editId'));

        $result = array(
            
            'dummy' => $dummy,
            'object' => $editCheckIn,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : lg('p5ee8e2ac3226c'),
            
            'dummy_user' => $dummyUser
            
        );
                        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        $entry = new CalsyCheckIn($deleteId);
        
        if((int)($entry->{$entry->field_fk}) != $userFrontend->{$userFrontend->field_pk})
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyCheckIn($deleteId);
        $entryObj->deleteCheckIn();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionSave()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');            
        }
        
        {
                        
            $saveElement = new CalsyCheckIn($editId);
            
            $saveElement->setObjectDataFromArray($this->getParameters());
            
            $saveElement->{$saveElement->field_fk} = $userFrontend->{$userFrontend->field_pk};
            $saveElement->{$saveElement->field_status} = (int)$saveElement->{$saveElement->field_time_out} > 0 ? CalsyCheckIn::STATUS_CLOSED : CalsyCheckIn::STATUS_OPEN;
                        
            $newId = $saveElement->save();
            
            $result = Array(
                
                'success' => true, 
                'newId' => $newId
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}