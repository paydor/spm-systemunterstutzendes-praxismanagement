<?php

/**
 * API connectivity to a BigBlueButton server.
 * More info on the BBB API: http://docs.bigbluebutton.org/dev/api.html
 * @author Peter Hamm
 * @return void
 */
class CalsyBigBlueButtonApi
{
    
    private $server = '';
    private $secret = '';
    
    public function __construct($server, $secret)
    {
        
        $this->server = $server;
        $this->secret = $secret;
        
    }
    
    /**
     * Creates a new meeting. 
     * The first three parameters are mandatory, while $additionalParameters 
     * optionally takes an array with the non-mandatory parameters as defined in  
     * http://docs.bigbluebutton.org/dev/api.html#create
     * 
     * @author Peter Hamm
     * @param String $meetingIdentifier
     * @param String $passwordAttendee
     * @param String $passwordModerator
     * @param Array $additionalParameters Optional parameters, e.g. Array("name" => "Test meeting")
     * @return Array
     * @throws PerisianException
     */
    public function create($meetingIdentifier, $passwordAttendee, $passwordModerator, $additionalParameters = null)
    {
                
        $resource = "create";
            
        $data = Array(

            'meetingID' => $meetingIdentifier,
            'attendeePW' => $passwordAttendee,
            'moderatorPW' => $passwordModerator

        );

        static::addAdditionalParameters($data, $additionalParameters);

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Retrieves the URL to join an existing meeting. 
     * This URL may then be called in the browser.
     * 
     * The first parameters are mandatory, while $additionalParameters 
     * optionally takes an array with the non-mandatory parameters as defined in  
     * http://docs.bigbluebutton.org/dev/api.html#join
     * 
     * @author Peter Hamm
     * @param String $userName
     * @param String $meetingIdentifier
     * @param String $password
     * @param Array $additionalParameters Optional parameters, e.g. Array("userID" => "123")
     * @return Array
     * @throws PerisianException
     */
    public function getJoinURL($userName, $meetingIdentifier, $password, $additionalParameters = null)
    {
                            
        $data = Array(

            'fullName' => $userName,
            'meetingID' => $meetingIdentifier,
            'password' => $password,

        );

        static::addAdditionalParameters($data, $additionalParameters);

        $result = $this->getHttpTarget("join", $data);

        return $result;
        
    }
    
    /**
     * Retrieves whether a specific meeting is running or not.
     * http://docs.bigbluebutton.org/dev/api.html#isMeetingRunning
     * 
     * @author Peter Hamm
     * @param String $meetingIdentifier
     * @return Array
     * @throws PerisianException
     */
    public function isMeetingRunning($meetingIdentifier)
    {
                
        $resource = "isMeetingRunning";
            
        $data = Array(

            'meetingID' => $meetingIdentifier

        );

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Ends a specific meeting.
     * http://docs.bigbluebutton.org/dev/api.html#end
     * 
     * @author Peter Hamm
     * @param String $meetingIdentifier
     * @param String $password
     * @return Array
     * @throws PerisianException
     */
    public function end($meetingIdentifier, $password)
    {
                
        $resource = "end";
            
        $data = Array(

            'meetingID' => $meetingIdentifier,
            'password' => $password

        );

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Retrieves information about a specific meeting
     * http://docs.bigbluebutton.org/dev/api.html#getMeetingInfo
     * 
     * @author Peter Hamm
     * @param String $meetingIdentifier
     * @return Array
     * @throws PerisianException
     */
    public function getMeetingInfo($meetingIdentifier, $password = null)
    {
                
        $resource = "getMeetingInfo";
            
        $data = Array(

            'meetingID' => $meetingIdentifier

        );
        
        if(!is_null($password))
        {
            
            $data['password'] = $password;
            
        }

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Retrieves a list of existing meetings
     * http://docs.bigbluebutton.org/dev/api.html#getMeetings
     * 
     * @author Peter Hamm
     * @return Array
     * @throws PerisianException
     */
    public function getMeetings()
    {
                
        $resource = "getMeetings";

        $result = $this->makeApiCall($resource);

        return $result;
        
    }
    
    /**
     * Retrieves the recordings that are available for playback for a given 
     * meeting identifier (or set of meeting identifiers).
     * 
     * All of the parameters are optional, $optionalParameters takes an 
     * array with the non-mandatory parameters as defined in  
     * http://docs.bigbluebutton.org/dev/api.html#getRecordings
     * 
     * @author Peter Hamm
     * @param Array $optionalParameters Optional parameters
     * @return Array
     * @throws PerisianException
     */
    public function getRecordings($optionalParameters = null)
    {
                
        $resource = "getRecordings";
        
        static::addAdditionalParameters(Array(), $optionalParameters);

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Publish and unpublish recordings for specified identifiers.
     * http://docs.bigbluebutton.org/dev/api.html#publishRecordings
     * 
     * @author Peter Hamm
     * @param String $recordIdentifier
     * @param String $publish
     * @return Array
     * @throws PerisianException
     */
    public function publishRecordings($recordIdentifier, $publish)
    {
                
        $resource = "end";
            
        $data = Array(

            'recordID' => $recordIdentifier,
            'publish' => $publish

        );

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Deletes the record for specified identifier.
     * http://docs.bigbluebutton.org/dev/api.html#deleteRecordings
     * 
     * @author Peter Hamm
     * @param String $recordIdentifier
     * @return Array
     * @throws PerisianException
     */
    public function deleteRecordings($recordIdentifier)
    {
                
        $resource = "deleteRecordings";
            
        $data = Array(

            'recordID' => $recordIdentifier

        );
        
        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Updates a specific recording's meta data
     * http://docs.bigbluebutton.org/dev/api.html#updateRecordings
     * 
     * @author Peter Hamm
     * @param String $recordIdentifier
     * @param Array $additionalParameters Optional parameters, e.g. Array("meta" => ...")
     * @return Array
     * @throws PerisianException
     */
    public function updateRecordings($recordIdentifier, $additionalParameters = null)
    {
                
        $resource = "deleteRecordings";
            
        $data = Array(

            'recordID' => $recordIdentifier

        );
        
        static::addAdditionalParameters($data, $additionalParameters);

        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
    
    /**
     * Retrieves the server's configuration 
     * http://docs.bigbluebutton.org/dev/api.html#getDefaultConfigXML
     * 
     * @author Peter Hamm
     * @return Array
     * @throws PerisianException
     */
    public function getDefaultConfig()
    {
                
        $resource = "getDefaultConfigXML";
            
        $result = $this->makeApiCall($resource, null);

        return $result;
        
    }
    
    
    /**
     * Sets the server's configuration 
     * http://docs.bigbluebutton.org/dev/api.html#setConfigXML
     * 
     * @author Peter Hamm
     * @return Array
     * @throws PerisianException
     */
    /*
    public function setConfig($meetingIdentifier, $configuration)
    {
                
        $resource = "setConfigXML";
        
        $data = Array(

            'meetingID' => $meetingIdentifier,
            'configXML' => $configuration,
            
            '_REQUEST_METHOD' => 'POST'

        );
            
        $result = $this->makeApiCall($resource, $data);

        return $result;
        
    }
     */
        
    /**
     * Adds optional, additional parameters to the request data.
     * 
     * @author Peter Hamm
     * @param Array $data Reference
     * @param Array $additionalParameters
     * @return void
     */
    protected static function addAdditionalParameters(&$data, $additionalParameters)
    {
        
        if(is_array($additionalParameters))
        {
            
            foreach($additionalParameters as $parameterKey => $parameterValue)
            {

                $data[$parameterKey] = $parameterValue;

            }
            
        }
        
    }

    /**
     * Retrieves the checksum required to make API calls 
     * depending on the data specified
     * 
     * @author Peter Hamm
     * @param String $resource
     * @param Array $data
     * @return String
     */
    protected function getCheckSum($resource, $data)
    {
        
        $checksum = sha1($resource . http_build_query($data) . $this->secret);
        
        return $checksum;
        
    }
    
    /**
     * Retrieves the targetted URL plus parameters from the data specified.
     * 
     * @author Peter Hamm
     * @param String $resource
     * @param Array $data
     * @return String
     */
    protected function getHttpTarget($resource, &$data)
    {
        
        $data['checksum'] = $this->getCheckSum($resource, $data);

        $query = $this->server . $resource;
        $query .= '?' . http_build_query($data);
        
        return $query;
        
    }
    
    /**
     * Makes a call to the API server.
     * 
     * @author Peter Hamm
     * @param String $resource
     * @param Array $data Optional
     * @throws PerisianException
     */
    protected function makeApiCall($resource, $data = null)
    {
        
        // Parameter initialization
        {

            $header = array(

                'Content-Type: application/x-www-form-urlencoded'

            );

            if(!isset($data))
            {
                
                $data = Array();
                
            }

            $targetUrl = $this->getHttpTarget($resource, $data);
            
            echo $targetUrl . "<br>\n";
            //exit;

        }

        $connection = curl_init();

        curl_setopt($connection, CURLOPT_URL, $targetUrl);
        curl_setopt($connection, CURLOPT_HTTPHEADER, $header);

        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($connection, CURLOPT_CUSTOMREQUEST, isset($data['_REQUEST_METHOD']) ? $data['_REQUEST_METHOD'] : "GET"); 

        // Channel the output
        {

            ob_start();

            $resultSuccessful = curl_exec($connection);

            $resultXml = $resultSuccessful ? ob_get_clean() : null;

            @ob_end_clean();

        }

        curl_close($connection);
        
        if(!$resultSuccessful)
        {

            // The remote server could not be reached

            throw new PerisianException(PerisianLanguageVariable::getVariable('p58a5ead79b720'));

        }

        // Result formatting
        {

            $resultJson = json_encode(simplexml_load_string($resultXml));
            $resultArray = json_decode($resultJson, true);
            
            $result = Array(
                
                'success' => $resultArray['returncode'] == 'SUCCESS',
                'data' => $resultArray
                
            );
            
        }
        
        return $result;
    
    }
    
}
