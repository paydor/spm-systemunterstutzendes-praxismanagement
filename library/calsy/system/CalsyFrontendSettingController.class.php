<?php

require_once 'framework/abstract/PerisianUploadController.class.php';

/**
 * Frontend setting controller
 *
 * @author Peter Hamm
 * @date 2016-12-30
 */
class CalsyFrontendSettingController extends PerisianUploadController
{
    
    /**
     * Retrieves a list of allowed upload setting names for this controller.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getAllowedUploadSettings()
    {
        
        $allowedUploadSettingNames = array('frontend_welcome_image', 'frontend_header_image', 'frontend_favorite_icon', 'frontend_backdrop_image');
        
        return $allowedUploadSettingNames;
        
    }
    
    /**
     * Retrieves all the relevant settings of this controller.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getListSettingsSpecific()
    {
        
        parent::getListSettingsSpecific();
        
        $settingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_', $this->listSettingsSystem, true);
        
        return $settingList;
        
    }
     
    /**
     * Provides data to display a form to edit an settings.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSettings()
    {
        
        $renderer = array(

            'page' => 'frontend_admin/frontend_settings'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $userId = $this->getParameter('u');
        $showAdminFunctions = false;

        // Load the settings
        {
            
            $this->listSettingsSystem = $this->getListSettingsSystem();
                     
            // Setting filtration
            {
                                
                // Add some WYSIWYG settings to seperate lists
                $frontendGtcSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_gtc', $this->listSettingsSystem, true);
                $frontendStartPageSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_start_information', $this->listSettingsSystem, true);
                $frontendStartPageRegistrationClosedSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_start_registration_closed_information', $this->listSettingsSystem, true);
                $frontendContactSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_contact_', $this->listSettingsSystem, true);
                
                // Add the frontend settings to a seperate list
                $frontendSettingList = $this->getListSettingsSpecific();
                                
            }
            
            // Image uploads
            {
                
                $uploadField = PerisianSystemSetting::getImageUploadData('frontend_welcome_image');
                $uploadFieldHeader = PerisianSystemSetting::getImageUploadData('frontend_header_image');
                $uploadFieldFavicon = PerisianSystemSetting::getImageUploadData('frontend_favorite_icon');
                $uploadFieldBackdrop = PerisianSystemSetting::getImageUploadData('frontend_backdrop_image');
                                
            }
            // Get the values for the language dropdown
            {
                
                $languageFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
                $languageList = PerisianLanguage::getLanguages();
                $languageListDropdown = Array();

                foreach($languageList as $languageItems)
                {
                    
                    $languageListDropdown[$languageItems[$languageFields['field_pk']]] = $languageItems[$languageFields['field_name']];
                    
                }

                $languageList = $languageListDropdown;
                
            }

            // Get the values for the calendar default end times
            {
                
                $hourList = CalsyCalendar::getTimeList(900, 900);
                
            }
            
            // The display statuses for the apointment assistant in the frontend
            {
                
                $appointmentAssistantStatusList = CalsyUserFrontendRegistrationAndCalendarModule::getStatusesForAppointmentAssistant();
                
            }

        }
        
        //
        
        $result = array(
                        
            'show_admin_functions' => $showAdminFunctions,
              
            'list_languages' => $languageList,
            'list_hours' => $hourList,
            
            'list_settings_frontend' => $frontendSettingList,
            'list_settings_frontend_start_page' => $frontendStartPageSettingList,
            'list_settings_frontend_start_page_registration_closed' => $frontendStartPageRegistrationClosedSettingList,
            'list_settings_frontend_contact' => $frontendContactSettingList,
            'list_settings_frontend_gtc' => $frontendGtcSettingList,
            
            'field_upload_image_welcome_frontend' => $uploadField,
            'field_upload_image_header_frontend' => $uploadFieldHeader,
            'field_upload_image_icon_favorite_frontend' => $uploadFieldFavicon,
            'field_upload_image_backdrop_frontend' => $uploadFieldBackdrop
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}