<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyFrontendSettingController.class.php";

class CalsyFrontendSettingControllerApi extends PerisianControllerRest
{
    
    /**
     * Handles authorization to this resource.
     * Only API system users may access this.
     * 
     * @author Peter Hamm
     * @throws RestException
     * @return bool
     */
    public function authorize()
    {
        
        $value = parent::authorize();
                
        return $value;
                
    }
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyFrontendSettingController";
        
    }
        
    /**
     * Retrieves the system's frontend setting list
     *
     * @author Peter Hamm
     * @url GET /list
     * @return Array
     */
    public function getSettingsFrontend()
    {
                
        $this->setAction('settings');
        
        $result = $this->finalize();
        
        $data = $result['list_settings_frontend'];
        
        return $data;
        
    }
       
    /**
     * Retrieves the system's General terms and condition (GTC) text
     *
     * @author Peter Hamm
     * @url GET /gtc
     * @return Array
     */
    public function getFrontendGTC()
    {
                
        $this->setAction('settings');
        
        $result = $this->finalize();
        
        $data = $result['list_settings_frontend_gtc'];
        
        return $data;
        
    }
        
}
