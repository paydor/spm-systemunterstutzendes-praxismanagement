<?php

require_once 'framework/abstract/PerisianUploadController.class.php';

require_once 'external/VersionControl/Git.php';

/**
 * Backend system setting controller
 *
 * @author Peter Hamm
 * @date 2016-11-28
 */
class CalsySystemSettingController extends PerisianUploadController
{
    
    /**
     * Retrieves the custom and default color settings for the system.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getSystemColors()
    {
        
        $colors = array(
            
            'current' => PerisianSystemConfiguration::getSystemColors(),
            'default' => PerisianSystemConfiguration::getSystemColors(true)
            
        );
        
        return $colors;
        
    }
    
    /**
     * Retrieves a list of allowed color settings for this controller.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getAllowedColorSettings()
    {
        
        $allowedSettingNames = array('backend_color_main', 'backend_color_main_alternative', 'backend_color_main_alternative_two', 'backend_color_secondary');
        
        return $allowedSettingNames;
        
    }
    
    /**
     * Retrieves a list of allowed upload setting names for this controller.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getAllowedUploadSettings()
    {
        
        $allowedUploadSettingNames = array('backend_welcome_image', 'backend_header_image', 'backend_favorite_icon', 'backend_backdrop_image');
        
        return $allowedUploadSettingNames;
        
    }
    
    /**
     * Resets the system colors to the default values.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetSystemColors()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $allowedColorSettings = $this->getAllowedColorSettings();
        
        $colors = $this->getSystemColors();
                
        foreach($allowedColorSettings as $possibleSettingKey)
        {
            
            $shortenedKey = str_replace('backend_color_', '', $possibleSettingKey);
            
            if(isset($colors['default'][$shortenedKey]))
            {

                $newObj = new PerisianSystemSetting($possibleSettingKey);
                $newObj->{$newObj->field_setting_value} = $colors['default'][$shortenedKey];
                $newObj->save();

            }
            
        }
        
        $data = array(

            "success" => true,
            "message" => PerisianLanguageVariable::getVariable(11132),
            "colors" => $colors['default']

        );

        $this->setResultValue('result', $data);
        
    }
    
    /**
     * Sets the system's colors to the specified values.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveSystemColors()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $allowedColorSettings = $this->getAllowedColorSettings();
        
        foreach($allowedColorSettings as $possibleSettingKey)
        {
            
            if(strlen($this->getParameter($possibleSettingKey)) > 0)
            {

                $newObj = new PerisianSystemSetting($possibleSettingKey);
                $newObj->{$newObj->field_setting_value} = PerisianFrameworkToolbox::getRequest($possibleSettingKey);
                $newObj->save();

            }

        }
        
        $data = array(

            "success" => true,
            "message" => PerisianLanguageVariable::getVariable(10669)

        );

        $this->setResultValue('result', $data);
        
    }
    
    /**
     * Triggered after saving settings.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function onAfterSave()
    {
        
        $appointmentAssistantDisplayType = $this->getParameter('s_' . PerisianSetting::getIdForName('module_setting_calsy_user_frontend_registration_and_calendar_is_appointment_assistant_enabled'), null);
        
        if($appointmentAssistantDisplayType != null)
        {
            
            PerisianSystemSetting::setSettingValue('module_setting_hidden_calsy_user_frontend_registration_and_calendar_appointment_assistant_status', $appointmentAssistantDisplayType == 'none' ? '0' : '1');
            
        }
        
    }
    
    /**
     * Saves settings.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
                
        $possibleSettings = Array();
        $settingObj = new PerisianSetting();
        $systemSettingObj = new PerisianSystemSetting();

        $settingList = $systemSettingObj->getPossibleSettings('admin');
        
        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }

        foreach($possibleSettings as $possibleSettingId)
        {
                        
            if(isset($this->parameters['s_' . $possibleSettingId]))
            {
                
                $settingValue = $this->getParameter('s_' . $possibleSettingId);

                $newValue = ($settingValue === false || $settingValue == null) ? "0" : $settingValue;
                
                $newObj = new PerisianSystemSetting($possibleSettingId);
                $newObj->{$newObj->field_setting_value} = $newValue;
                $newObj->save();
                
            }

        }
        
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
        
        $this->onAfterSave();
        
    }
     
    /**
     * Provides data to display a form to edit an settings
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSettingsLayout()
    {
     
        $renderer = array(

            'page' => 'system/system_layout'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $showAdminFunctions = false;

        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
            
            {
                                
                // Remove the language variable import server setting from the settings list
                PerisianSystemSetting::retrieveSettingsByPrefix('system_language_variable', $systemSettingList, true);
                
                // Add the calendar settings to a seperate list
                $calendarSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('calendar_', $systemSettingList, true);
                
                // System colors
                $colorSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('backend_color_', $systemSettingList, true);
                
                // Add the frontend settings to a seperate list
                $frontendSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_', $systemSettingList, true);
                
                // Add the module settings to a seperate list
                $moduleEnabledSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_enabled_', $systemSettingList, true);
                $moduleBlockedSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_blocked_', $systemSettingList, true);
                $moduleIconSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_icon_', $systemSettingList, true);
                
                // Add the system fallback settings to a seperate list
                $systemFallbackSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_fallback', $systemSettingList, true);
                
                // Add the database revision setting to a seperate list
                $databaseRevisionSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('database_revision', $systemSettingList, true);
                
                // Add the "bookable times" setting to a seperate list
                $bookableTimesSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_enabled_bookable_times', $systemSettingList, true);
                
                // Add the SMTP settings to a seperate list
                $defaultSmtpSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_default_smtp_', $systemSettingList, true);
                
                // Display setting list
                $displaySettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_show_', $systemSettingList, true);
                
                // Process engine setting list
                $processEngineSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('process_engine', $systemSettingList, true);
                
                // Various module settings
                $moduleSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting', $systemSettingList, true);
                
                // Different module specific settings
                {
                    
                    $moduleUserFrontendRegistrationSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_calsy_user_frontend_registration_and_calendar', $systemSettingList, true);
                    
                }
                
            }
            
            // Image uploads
            {
                
                $uploadField = PerisianSystemSetting::getImageUploadData('backend_welcome_image');
                $uploadFieldHeader = PerisianSystemSetting::getImageUploadData('backend_header_image');
                $uploadFieldFavIcon = PerisianSystemSetting::getImageUploadData('backend_favorite_icon');
                $uploadFieldBackdrop = PerisianSystemSetting::getImageUploadData('backend_backdrop_image');
                
            }       
            
            // Get the values for the language dropdown
            {
                
                $languageFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
                $languageList = PerisianLanguage::getLanguages();
                $languageListDropdown = Array();

                foreach($languageList as $languageItems)
                {
                    
                    $languageListDropdown[$languageItems[$languageFields['field_pk']]] = $languageItems[$languageFields['field_name']];
                    
                }

                $languageList = $languageListDropdown;
                
            }

            // Get the values for the calendar default end times
            {
                
                $dayList = CalsyCalendar::getDayNames();
                $hourList = CalsyCalendar::getTimeList(300, 900);
                                
            }

        }
        
        //
        
        $result = array(
                        
            'show_admin_functions' => $showAdminFunctions,
            
            'list_settings_color' => $colorSettingList,
            'list_settings_display' => $displaySettingList,
            
            'list_colors_system' => $this->getSystemColors(),
            
            'field_upload_image_welcome_backend' => $uploadField,
            'field_upload_image_header_backend' => $uploadFieldHeader,
            'field_upload_image_icon_favorite_backend' => $uploadFieldFavIcon,
            'field_upload_image_backdrop_backend' => $uploadFieldBackdrop
            
        );
        
        $this->setResultValue('result', $result);
        
    }
     
    /**
     * Provides data to display a form to edit an settings
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSettings()
    {
        
        $renderer = array(

            'page' => 'system/system_settings'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $showAdminFunctions = false;

        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();
            
            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
            
            {
                                
                // Remove the language variable import server setting from the settings list
                PerisianSystemSetting::retrieveSettingsByPrefix('system_language_variable', $systemSettingList, true);
                
                // Add the calendar settings to a seperate list
                $calendarSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('calendar_', $systemSettingList, true);
                
                // System colors
                $colorSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('backend_color_', $systemSettingList, true);
                
                // Add the frontend settings to a seperate list
                $frontendSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('frontend_', $systemSettingList, true);
                
                // Add the module settings to a seperate list
                $moduleEnabledSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_enabled_', $systemSettingList, true);
                $moduleBlockedSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_blocked_', $systemSettingList, true);
                $moduleIconSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_icon_', $systemSettingList, true);
                
                // Add the system fallback settings to a seperate list
                $systemFallbackSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_fallback', $systemSettingList, true);
                
                // Add the database revision setting to a seperate list
                $databaseRevisionSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('database_revision', $systemSettingList, true);
                
                // Add the "bookable times" setting to a seperate list
                $bookableTimesSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_enabled_bookable_times', $systemSettingList, true);
                
                // Add the SMTP settings to a seperate list
                $defaultSmtpSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_default_smtp_', $systemSettingList, true);
                
                // Display setting list
                $displaySettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_show_', $systemSettingList, true);
                
                // Process engine setting list
                $processEngineSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('process_engine', $systemSettingList, true);
                
                // Various module settings
                $moduleSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting', $systemSettingList, true);
                
                // Security setting list
                $securitySettingList = PerisianSystemSetting::retrieveSettingsByPrefix('system_security_', $systemSettingList, true);
                
                // Big blue button settings
                $processEngineSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('big_blue_button', $systemSettingList, true);
                
                // Different module specific settings
                {
                    
                    $moduleUserFrontendRegistrationSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_calsy_user_frontend_registration_and_calendar', $systemSettingList, true);
                    
                }
                
            }
            
            // Get the values for the language dropdown
            {
                
                $languageFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
                $languageList = PerisianLanguage::getLanguages();
                $languageListDropdown = Array();

                foreach($languageList as $languageItems)
                {
                    
                    $languageListDropdown[$languageItems[$languageFields['field_pk']]] = $languageItems[$languageFields['field_name']];
                    
                }

                $languageList = $languageListDropdown;
                
                $passwordOptionsList = PerisianValidation::getPasswordPolicyOptions();
                $passwordOptionsDetailsList = PerisianValidation::getPasswordPolicyDetails();
                
            }

            // Get the values for the calendar default end times
            {
                
                $dayList = CalsyCalendar::getDayNames();
                $hourList = CalsyCalendar::getTimeList(300, 900);
                                
            }
            
            // Get the values for the dropdown "Behaviour on user deletion"
            {
                
                
                $listBehaviourUserDeletion = Array(
                    
                    'delete' => PerisianLanguageVariable::getVariable('p5b00617154513'),
                    'anonymize' => PerisianLanguageVariable::getVariable('p5b00619f7b24a')
                    
                );
                
            }

        }
        
        //
        
        $result = array(
                        
            'show_admin_functions' => $showAdminFunctions,
            
            'list_settings_fallback_system' => $systemFallbackSettingList,
            'list_settings_smtp' => $defaultSmtpSettingList,
            'list_settings_system' => $systemSettingList,
            'list_settings_security' => $securitySettingList,
            'list_mail_services' => PerisianMail::getMailServices(),
            
            'list_languages' => $languageList,
            'list_security_password_options' => $passwordOptionsList,
            'list_security_password_options_details' => $passwordOptionsDetailsList,
            'list_behaviour_user_deletion' => $listBehaviourUserDeletion,
            'list_days' => $dayList,
            'list_hours' => $hourList
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides various information about the system's current status.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionStatus()
    {
        
        $renderer = array(

            'page' => 'system/system_status'

        );

        $this->setResultValue('renderer', $renderer);
                
        //
        
        $lastUpdates = array();
        
        $result = array(
            
            'success' => true
            
        );
        
        try
        {
            
            $systemStatus = array(
                
                
                'software_server' => array(

                    'label' => 'SERVER_SOFTWARE',
                    'value' => $_SERVER['SERVER_SOFTWARE']

                ),


                'VERSION_PHP' => array(

                    'label' => 'VERSION_PHP',
                    'value' => phpversion()

                )
                
            );
            
            {
                
                $timeZoneName = PerisianTimeZone::getTimeZoneName();
                $timeZoneHasDst = PerisianTimeZone::timeZoneHasDst($timeZoneName);
                
                $calendarStatus = array(
                    
                    'date_time' => array(
                        
                        'label' => PerisianLanguageVariable::getVariable('10964'),
                        'value' => date("H:i, d.m.Y", time())
                        
                    ),
                    
                    'date_time_gmt' => array(
                        
                        'label' => PerisianLanguageVariable::getVariable('10964') . ' (GMT)',
                        'value' => gmdate("H:i, d.m.Y", time())
                        
                    ),
                    
                    'timezone' => array(
                        
                        'label' => PerisianLanguageVariable::getVariable('p589e16a8a4b93'),
                        'value' => $timeZoneName . ' (' . PerisianTimeZone::getOffsetString() . ')'
                        
                    ),
                    
                    'timezone_has_dst' => array(
                        
                        'label' => PerisianLanguageVariable::getVariable('p589e1b225f8b6'),
                        'value' => $timeZoneHasDst
                        
                    )

                );
                
                if($timeZoneHasDst)
                {
                    
                    // Info about when the shift to and from DST happens
                    
                    $timeZoneDstInfo = PerisianTimeZone::getTimeZoneDstInfo($timeZoneName);
                                                                            
                    $calendarStatus['timezone_shift_daylight_saving_time'] = array(
                        
                        'label' => PerisianLanguageVariable::getVariable('p589e16df5bc54'),
                        'value' => PerisianFrameworkToolbox::timestampToDate($timeZoneDstInfo['begin']['ts']) . ', ' . gmdate("H:i", $timeZoneDstInfo['begin']['ts'] + (date('I') ? 3600 : 0)) . ' => ' . date("H:i", $timeZoneDstInfo['begin']['ts'])
                        
                    );
                                                       
                    $calendarStatus['timezone_shift_standard_time'] = array(
                        
                        'label' => PerisianLanguageVariable::getVariable('p589e16f508fd9'),
                        'value' => PerisianFrameworkToolbox::timestampToDate($timeZoneDstInfo['end']['ts']) . ', ' . gmdate("H:i", $timeZoneDstInfo['end']['ts'] + PerisianTimeZone::getTimeZoneOffsetInSeconds() + (date('I') ? 0 : 3600)) . ' => ' . date("H:i", $timeZoneDstInfo['end']['ts'])
                        
                    );
                
                }
            
            }
                
            // Retrieve the last updates from the repository
            {
                
                $repository = new VersionControl_Git('/var/www');
                $lastUpdates = $repository->getCommits('master', 10);
                
                $listLatestUpdatesFormatted = array();
                
                for($i = 0; $i < count($lastUpdates); ++$i)
                {
                 
                    $newEntry = array(
                        
                        'author' => strip_tags($lastUpdates[$i]->getAuthor()),
                        'message' => strip_tags($lastUpdates[$i]->getMessage()),
                        'timestamp' => $lastUpdates[$i]->getCreatedAt()
                        
                    );
                    
                    array_push($listLatestUpdatesFormatted, $newEntry);
                    
                }
                
            }
            
            $result['status_system'] = $systemStatus;
            $result['status_calendar'] = $calendarStatus;
            $result['list_updates_latest'] = $listLatestUpdatesFormatted;
        
        }
        catch(Exception $e)
        {
                        
            $result = array(
                
                'success' => false,
                'message' => PerisianLanguageVariable::getVariable('p57b458f118f5b') . "\n" . PerisianLanguageVariable::getVariable(10066) . ": \"" . $e->getMessage() . "\""
                
            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
                
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}