<?php

/**
 * Account deletion log
 *
 * @author Peter Hamm
 * @date 2018-05-06
 */
class CalsyPrivacyAccountDeletionLog extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'privacy_account_deletion_log';
    public $field_pk = 'privacy_account_deletion_log_id';
    public $field_name = 'privacy_account_deletion_log_name';
    public $field_data = 'privacy_account_deletion_log_data';
        
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
}
