<?php

class CalsyCalendarEntrySurvey
{
    
    const FLAG_SURVEY_AREA_ID = "survey_area_id";
    const FLAG_SURVEY_DATA_USERS = "survey_data_users";
    
    /**
     * Updates the area for the specified survey entry.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $areaId
     * @return void
     */
    public static function updateSurveyAreaForEntry($calendarEntryId, $areaId)
    {
        
        CalsyCalendarEntryFlag::saveFlagForEntry($calendarEntryId, CalsyCalendarEntrySurvey::FLAG_SURVEY_AREA_ID, $areaId);
        
    }
    
    /**
     * Updates the specified key and value pair for the specified survey entry's user data set.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userId
     * @param String $key
     * @param String $value
     * @return boolean Update successful?
     */
    public static function updateSurveyUserDataForEntry($calendarEntryId, $userId, $key, $value)
    {
        
        $hasUpdate = false;
        
        $userData = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userData as &$userEntry)
        {
            
            if($userEntry['id'] == $userId)
            {
                
                $userEntry[$key] = $value;
                
                $hasUpdate = true;
                
                break;
                
            }
            
        }
        
        if($hasUpdate)
        {
            
            static::saveUserDataForEntry($calendarEntryId, $userData);
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Accepts the invitation for the specified calendar entry and callback code.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param String $callbackCode
     * @return void
     */
    public static function acceptInvitation($calendarEntryId, $callbackCode)
    {
        
        $userId = static::getUserIdByCode($calendarEntryId, $callbackCode);
        
        if($userId > 0)
        {
            
            static::updateSurveyUserDataForEntry($calendarEntryId, $userId, 'status_accepted', true);
            static::updateSurveyUserDataForEntry($calendarEntryId, $userId, 'status_accepted_time', time());
            
        }
        
    }
    
    /**
     * Rejects the invitation for the specified calendar entry and callback code.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param String $callbackCode
     * @return void
     */
    public static function rejectInvitation($calendarEntryId, $callbackCode)
    {
        
        $userId = static::getUserIdByCode($calendarEntryId, $callbackCode);
        
        if($userId > 0)
        {
            
            static::updateSurveyUserDataForEntry($calendarEntryId, $userId, 'status_rejected', true);
            static::updateSurveyUserDataForEntry($calendarEntryId, $userId, 'status_rejected_time', time());
            
        }
        
    }
    
    /**
     * Stores the user data for the entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param Array $userData
     * @return boolean
     */
    private static function saveUserDataForEntry($calendarEntryId, $userData)
    {
        
        $calendarEntryId = (int)$calendarEntryId;
        
        if(!is_array($userData) || $calendarEntryId <= 0)
        {
            
            throw new PerisianException("Invalid parameters.");
            
        }
        
        CalsyCalendarEntryFlag::saveFlagForEntry($calendarEntryId, static::FLAG_SURVEY_DATA_USERS, json_encode($userData));
        
    }
    
    /**
     * Sets the survey user data flag for the calendar entry with the specified identifier.
     * If there is an existing value for the survey user data in the database, it is updated accordingly.
     * 
     * @author Peter amm
     * @param int $calendarEntryId
     * @param Array $surveyUserData
     * @return Array The updated user data.
     */
    public static function updateSurveyUsersForEntry($calendarEntryId, $surveyUserData)
    {
        
        $userData = Array();

        $userDataExisting = static::getUserDataForEntry($calendarEntryId);
                
        if(count($userDataExisting) > 0)
        {
                        
            // Go through all of the existing user data and compare it.
            foreach($userDataExisting as &$existingUserData)
            {
                
                // Check if this one is in the list
                
                foreach($surveyUserData as $newEntryUnformatted)
                {
                                        
                    if($newEntryUnformatted['id'] == $existingUserData['id'])
                    {
                        
                        // It's still in there, keep it.
                                                
                        continue 2;
                    
                    }
                    
                }
                
                // It was removed, delete it.
                                
                if($existingUserData['id'] > 0)
                {
                    
                    $existingUserData = null;
                    
                }
                
            }
            
            foreach($userDataExisting as $existingUserData)
            {
                
                if(!is_null($existingUserData))
                {
                    
                    array_push($userData, $existingUserData);
                    
                }
                
            }
        
        }
        
        // Add all new entries
        foreach($surveyUserData as $newEntryUnformatted)
        {
            
            if(is_null($newEntryUnformatted))
            {
                
                continue;
                
            }
            
            // Skip it if the user is already in the list.
            // In this case, the existing entry was updated already.
            foreach($userData as $existingUserData)
            {
                
                if($existingUserData['id'] == $newEntryUnformatted['id'])
                {
                    
                    continue 2;
                    
                }
                
            }
            
            $newEntry = static::getUserDataDummy();
            
            $newEntry['id'] = $newEntryUnformatted['id'];
            $newEntry['callback_code'] = static::createCallbackCode();
            
            array_push($userData, $newEntry);
            
        }
        
        static::saveUserDataForEntry($calendarEntryId, $userData);
        
        return $userData;
    
    }
    
    /**
     * Generates a random callback code for invitations.
     * 
     * @author Peter Hamm
     * @return String
     */
    private static function createCallbackCode()
    {
        
        $code = 'calsy_cb_' . uniqid('cci');
        $code = md5($code);
        
        return $code;
        
    }
    
    /**
     * Retrieves a dummy array of user data.
     * 
     * @author Peter Hamm
     * @return Array
     */
    private static function getUserDataDummy()
    {
        
        $dummy = Array(

            'id' => null,
            'callback_code' => '',
            'invitation_sent' => false,
            'invitation_sent_time' => 0,
            'status_accepted' => false,
            'status_accepted_time' => 0,
            'status_rejected' => false,
            'status_rejected_time' => 0

        );
        
        return $dummy;
        
    }
    
    /**
     * Tries to retrieve the user's identifier for the specified calendar survey entry identifier and callback code.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param String $callbackCode
     * @return int Will be 0 if none was found.
     */
    public static function getUserIdByCode($calendarEntryId, $callbackCode)
    {
                
        $userData = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userData as $userEntry)
        {
            
            if($userEntry['callback_code'] == $callbackCode)
            {
                
                return $userEntry['id'];
                
            }
            
        }
        
        return 0;
        
    }
    
    /**
     * Will return true if the user with the specified identifier has received an invitation
     * but did not yet answer to it.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userId
     * @return boolean
     */
    public static function isInvitationOpenForUser($calendarEntryId, $userId)
    {
        
        $calendarEntryId = (int)$calendarEntryId;
        $userId = (int)$userId;
        
        if($calendarEntryId <= 0 || $userId <= 0)
        {
            
            return false;
            
        }
        
        $isInvited = static::isUserInvited($calendarEntryId, $userId);
        $isConfirmed = static::hasUserConfirmed($calendarEntryId, $userId);
        $isRejected = static::hasUserRejected($calendarEntryId, $userId);
        
        $result = ($isInvited && !$isConfirmed && !$isRejected);
        
        return $result;
                
    }
    
    /**
     * Retrieves the amount of unanswered invitations for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getCountUnansweredForCalendarEntry($calendarEntryId)
    {
                
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        $count = count($userList);
        
        $count -= static::getCountAcceptedForCalendarEntry($calendarEntryId);
        $count -= static::getCountRejectedForCalendarEntry($calendarEntryId);
        
        return $count;
        
    }
    
    /**
     * Retrieves the amount of accepted invitations for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getCountAcceptedForCalendarEntry($calendarEntryId)
    {
        
        $count = 0;
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $entry)
        {
            
            if($entry['status_accepted'])
            {
                
                ++$count;
                
            }
            
        }
        
        return $count;
        
    }
    
    /**
     * Retrieves the amount of rejected invitations for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getCountRejectedForCalendarEntry($calendarEntryId)
    {
        
        $count = 0;
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $entry)
        {
            
            if($entry['status_rejected'])
            {
                
                ++$count;
                
            }
            
        }
        
        return $count;
        
    }
    
    /**
     * Will return true if the user with the specified identifier has accepted an invitation
     * for the survey entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userId
     * @return boolean
     */
    public static function hasUserConfirmed($calendarEntryId, $userId)
    {
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $userEntry)
        {
            
            if($userEntry['id'] == $userId && $userEntry['status_accepted'])
            {
                
                return true;
                
            }
            
        }
        
        return false;
                
    }
    
    /**
     * Will return true if the user with the specified identifier has rejected an invitation
     * for the survey entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userId
     * @return boolean
     */
    public static function hasUserRejected($calendarEntryId, $userId)
    {
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $userEntry)
        {
            
            if($userEntry['id'] == $userId && $userEntry['status_rejected'])
            {
                
                return true;
                
            }
            
        }
        
        return false;
                
    }
    
    /**
     * Will return true if the user with the specified identifier has received an invitation
     * for the survey entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userId
     * @return boolean
     */
    public static function isUserInvited($calendarEntryId, $userId)
    {
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $userEntry)
        {
            
            if($userEntry['id'] == $userId)
            {
                
                return true;
                
            }
            
        }
        
        return false;
                
    }
    
    /**
     * Retrieves any eventually existing user data for the survey entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return Array
     */
    public static function getUserDataForEntry($calendarEntryId)
    {
        
        $list = Array();
        
        $userData = CalsyCalendarEntryFlag::getFlagValueForEntry($calendarEntryId, static::FLAG_SURVEY_DATA_USERS);
                
        if(strlen($userData) > 0)
        {
            
            $list = json_decode($userData, true);
                        
        }
                
        return $list;
        
    }
    
    /**
     * Sends invitations to the users associated with the calendar survey with the specified identifier.
     * Only those users that have not received an invitation yet will be sent one, 
     * except when $resendToAlreadySent - Then every user in the survey user data entry gets one.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param boolean $resendToAlreadySent
     * @return boolean
     */
    public static function inviteUsersToSurvey($calendarEntryId, $resendToAlreadySent = false)
    {
        
        $entry = new CalsyCalendarEntry($calendarEntryId);
        
        if($entry->{$entry->field_is_survey} != "1")
        {
            
            return false;
            
        }
        
        $userList = static::getUserDataForEntry($calendarEntryId);
        
        foreach($userList as $userEntry)
        {
            
            $doSend = $resendToAlreadySent;
            
            if(!$doSend && !$userEntry['invitation_sent'])
            {
                
                $doSend = true;
                
            }
            
            if($doSend)
            {
                
                $userFrontendObject = new CalsyUserFrontend($userEntry['id']);
                                
                $userFrontendObject->sendEmailSurveyInvitation($entry, $userEntry['callback_code']);
                
                // Delay between mails
                usleep(50);
                
                static::updateSurveyUserDataForEntry($calendarEntryId, $userEntry['id'], 'invitation_sent', true);
                static::updateSurveyUserDataForEntry($calendarEntryId, $userEntry['id'], 'invitation_sent_time', time());
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Retrieves a list of users for the calendar survey entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return array
     */
    public static function getDisplayableUserListForSurvey($calendarEntryId)
    {
        
        $dummy = new CalsyUserFrontend();
                
        $list = array();
        
        $listUserFrontend = CalsyUserFrontend::getUserFrontendList('', $dummy->fullname, "ASC", 0, 0);
        
        foreach($listUserFrontend as $userFrontend)
        {
            
            $entry = static::getUserDataDummy();
            
            $entry['id'] = $userFrontend[$dummy->field_pk];
            $entry['name'] = $userFrontend[$dummy->fullname];
            $entry['selected'] = false;
                        
            array_push($list, $entry);
            
        }
        
        $existingUserData = static::getUserDataForEntry($calendarEntryId);
        
        $combinedList = static::combineUserLists($list, $existingUserData);
        
        return $combinedList;
        
    }
    
    /**
     * Updates $userList with the data from $existingUserList.
     * 
     * @author Peter Hamm
     * @param Array $userList Reference, will be updated.
     * @param Array $existingUserList Existing user data, e.g. from static::getUserDataForEntry($id)
     * @return Array The updated $userList, same as reference
     */
    private static function combineUserLists(&$userList, $existingUserList)
    {
        
        foreach($userList as &$userData)
        {
            
            foreach($existingUserList as $existingUserData)
            {
                
                if($userData['id'] == $existingUserData['id'])
                {
                    
                    foreach($userData as $userDataFieldKey => &$userDataFieldValue)
                    {
                        
                        if(isset($existingUserData[$userDataFieldKey]))
                        {
                            
                            $userDataFieldValue = $existingUserData[$userDataFieldKey];
                            
                        }
                        
                    }
                    
                    $userData['selected'] = true;
                    
                }
                
            }
            
        }
        
        return $userList;
        
    }
    
}
