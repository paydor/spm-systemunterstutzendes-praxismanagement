<?php

/**
 * Calendar entry images
 *
 * @author Peter Hamm
 * @date 2019-10-02
 */
class CalsyCalendarEntryImage extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_calendar_entry_image';
    public $field_pk = 'calsy_calendar_entry_image_id';
    public $field_fk = 'calsy_calendar_entry_image_calendar_entry_id';
    public $field_file = 'calsy_calendar_entry_image_file';
    public $field_filename = 'calsy_calendar_entry_image_filename';
    public $field_date_created = 'calsy_calendar_entry_image_date_created';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_file');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
        
    /**
     * Saves the data.
     * 
     * @author Peter Hamm
     * @param Array $saveFields
     * @return int The identifier of the saved entry.
     */
    public function save($saveFields = Array())
    {
        
        return parent::save($saveFields);
        
    }
        
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Retrieves a list of images for the specified calendar entry identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return Array An Array of related CalsyCalendarEntryImage objects
     */
    public static function getImagesForCalendarEntry($calendarEntryId)
    {
        
        $returnValue = Array();
                
        try 
        {
            
            $instance = new static();

            $query = "{$instance->field_fk} = '{$calendarEntryId}'";

            $result = $instance->getData($query, 'pk', 'DESC');

            if(count($result) > 0)
            {

                for($i = 0; $i < count($result); ++$i)
                {
                    
                    array_push($returnValue, new static($result[$i][$instance->field_area_id]));
                    
                }

            }
            
        } 
        catch (Exception $e) 
        {
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '')
    {
                
        $query = $this->buildSearchString($search);
                
        return $query;
        
    }
    
    /**
     * Retrieves a list of areas
     * 
     * @author Peter Hamm
     * @param String $query
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getImageList($query = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = static::getInstance();
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of areas
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getImageCount($search = "")
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an image by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyArea
     */
    public static function getImageById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    
    /**
     * Retrieves the URL for the specified uploaded image.
     * 
     * @author Peter Hamm
     * @param String $settingIdOrName
     * @return Sring
     */
    public static function getImageUploadUrl($settingIdOrName = '')
    {
        
        $imageSystemSetting = new static($settingIdOrName);
        
        return PerisianUploadFileManager::getUrlForFile('image', $imageSystemSetting->getValue());
        
    }
    
    public static function getImageListWithFileData($calendarEntryId)
    {
        
        $enrichedList = Array();
        
        $dummy = static::getInstance();
        
        $query = (int)$calendarEntryId > 0 ? ($dummy->field_fk . ' = "' . PerisianFrameworkToolbox::security($calendarEntryId) . '"') : '';
        
        if(strlen($query) > 0)
        {
            
            // Load the data of existing images.
            
            $listImages = static::getImageList($query);
            
            if(count($listImages) == 0)
            {
                
                array_push($enrichedList, static::getImageUploadData());
                
            } 
            else
            {
                
                foreach($listImages as $imageData)
                {

                    $imageEntry = static::getImageUploadData($imageData[$dummy->field_pk]);

                    array_push($enrichedList, $imageEntry);

                }

            }
            
        }
        else
        {
            
            array_push($enrichedList, static::getImageUploadData());
            
        }
        
        return $enrichedList;
        
    }
    
    /**
     * Takes a list of images and sets them for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param Array $imageData
     * @return boolean
     */
    public static function setImagesForCalendarEntry($calendarEntryId, $imageData)
    {
        
        if((int)$calendarEntryId <= 0)
        {
            
            return true;
            
        }
        
        // Remove all existing entries
        
        $dummy = new static();
        
        $query = $dummy->field_fk . ' = "' . PerisianFrameworkToolbox::security($calendarEntryId) . '"';
        $dummy->delete($query);
        
        foreach($imageData as $image)
        {
            
            $entry = new static();
            
            $entry->{$entry->field_fk} = $calendarEntryId;
            $entry->{$entry->field_file} = $image['file'];
            $entry->{$entry->field_filename} = $image['filename'];
            
            $entry->save();
            
        }
        
        return true;
        
    }
    
    public static function getImageUploadData($imageId = 0)
    {
                
        //$frontendWelcomeImageSetting = new PerisianSetting(0, $settingIdOrName);
        //$frontendWelcomeImageSystemSetting = new static($settingIdOrName);
        
        $fileInfo = null;
                
        try
        {
            if((int)$imageId > 0)
            {
                
                // Load file info
                
                $imageObject = new static($imageId);
                
                $fileInfo = PerisianUploadFileManager::getFileInfo('image', $imageObject->{$imageObject->field_file});
                
            }
            
            
        }
        catch(PerisianException $e)
        {
                        
        }
        
        $uploadField = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => "calsy_calender_entry_image",
            "title" => PerisianLanguageVariable::getVariable('p5d94f2dc1c0dc'),
            "description" => "Beschreibung",
            
            "image_id" => $imageId,
            "image" => $fileInfo

        );
        
        if(isset($imageObject))
        {
            
            $uploadField['title'] = $imageObject->{$imageObject->field_filename};
            
        }
                
        return $uploadField;
            
    }
        
    /**
     * Deletes an image
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteImage()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
