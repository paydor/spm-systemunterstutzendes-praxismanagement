<?php

/**
 * Flags for calendar entries
 *
 * @author Peter Hamm
 * @date 2016-08-17
 */
class CalsyCalendarEntryFlag extends PerisianDatabaseModel
{
    
    const SQL_SEPERATOR_VALUE = "+=+";
    const SQL_SEPERATOR_FLAG = "+-+";
    
    // Instance
    private static $instance = null;

    // Main table settings
    public $table = 'calsy_calendar_entry_flag';
    public $field_pk = 'calsy_calendar_entry_flag_id';
    public $field_fk = 'calsy_calendar_entry_flag_calsy_calendar_entry_id';
    public $field_title = 'calsy_calendar_entry_flag_title';
    public $field_value = 'calsy_calendar_entry_flag_value';
    
    protected static $entryFlagsExisting = Array();
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $this->{$this->field_pk} = $entryId;
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static public function getInstance()
    {

        if(!isset(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;

    }
    
    /**
     * Retrieves the flag value for the specified $flagTitle and the entry with the specified $entryId
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param String $flagTitle
     * @return mixed
     */
    public static function getFlagValueForEntry($entryId, $flagTitle)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        PerisianFrameworkToolbox::security($flagTitle);
        
        $instance = static::getInstance();
                
        $query = "{$instance->field_fk} = '{$entryId}' AND ";
        $query .= "{$instance->field_title} = '{$flagTitle}'";
        
        $result = $instance->getData($query);
                
        if(count($result) == 1)
        {
            
            return $result[0][$instance->field_value];
            
        }
        
        return null;
        
    }
    
    /**
     * Retrieves if the calendar entry with the specified identifier has the specified flag.
     * (This function uses a cache).
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param String $flagTitle
     * @return boolean
     */
    public static function entryHasFlag($entryId, $flagTitle)
    {
        
        if(!isset(static::$entryFlagsExisting[$entryId]))
        {
            
            static::$entryFlagsExisting[$entryId] = Array();
            
        }
        
        if(!isset(static::$entryFlagsExisting[$entryId][$flagTitle]))
        {
            
            static::$entryFlagsExisting[$entryId][$flagTitle] = static::getFlagValueForEntry($entryId, $flagTitle);
            
        }
        
        return static::$entryFlagsExisting[$entryId][$flagTitle];
        
    }
    
    /**
     * Retrieves all the flags for the specified entry.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return Array
     */
    public static function getFlagsForEntry($entryId)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        
        $instance = static::getInstance();
        
        $query = "{$instance->field_fk} = '{$entryId}'";
        $result = $instance->getData($query);
        
        $returnValue = array();
        
        for($i = 0; $i < count($result); ++$i)
        {
        
            $returnValue[$result[$i][$instance->field_title]] = $result[$i][$instance->field_value];
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Stores the specified $flagValue for the specified $flagTitle and the entry with the ID $entryId.
     * Will simply override any existing vaue with the specified title.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param String $flagTitle
     * @param String $flagValue
     * @return void
     */
    public static function saveFlagForEntry($entryId, $flagTitle, $flagValue)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        PerisianFrameworkToolbox::security($flagTitle);
        PerisianFrameworkToolbox::security($flagValue);
        
        $instance = static::getInstance();
        
        $query = "{$instance->field_fk} = '{$entryId}' AND ";
        $query .= "{$instance->field_title} = '{$flagTitle}'";
        
        $result = $instance->getData($query);
                
        if(count($result) == 1)
        {
            
            $editId = $result[0][$instance->field_pk];
            
        }
        
        $saveObject = new static(@$editId);
        
        $saveObject->{$saveObject->field_fk} = $entryId;
        $saveObject->{$saveObject->field_title} = $flagTitle;
        $saveObject->{$saveObject->field_value} = $flagValue;
        
        $saveObject->save();
        
    }
    
    /**
     * Deletes the flag with the specified $flagTitle 
     * for the entry with the specified $entryId
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param String $flagTitle
     * @return void
     */
    public static function deleteFlagForEntry($entryId, $flagTitle)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        PerisianFrameworkToolbox::security($flagTitle);
        
        $instance = static::getInstance();
        
        $query = "{$instance->field_fk} = '{$entryId}' AND ";
        $query .= "{$instance->field_title} = '{$flagTitle}'";
        
        $instance->delete($query);
        
    }
    
    /**
     * Deletes all the flags for the entry with the specified $entryId
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return void
     */
    public static function deleteFlagsForEntry($entryId)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        
        $instance = static::getInstance();
        
        $query = "{$instance->field_fk} = '{$entryId}'";
        
        $instance->delete($query);
        
    }
    
    /**
     * Generates a sub-select query string to retrieve flags for calendar entries.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getFlagSubSelectQueryString()
    {
        
        $flagObject = static::getInstance();
        $entryObject = CalsyCalendarEntry::getInstance();
        
        $sqlSeperatorValue = CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE;
        $sqlSeperatorFlag = CalsyCalendarEntryFlag::SQL_SEPERATOR_FLAG;

        $subSelect = "(SELECT GROUP_CONCAT(CONCAT({$flagObject->field_title}, '{$sqlSeperatorValue}', {$flagObject->field_value}),'{$sqlSeperatorFlag}') FROM {$flagObject->table} WHERE ";
        $subSelect .= "({$entryObject->field_parent_entry_id} IS NOT NULL AND {$flagObject->field_fk} = {$entryObject->field_parent_entry_id} OR {$entryObject->field_pk} = {$flagObject->field_fk})";
        $subSelect .= ") as flags ";
        
        return $subSelect;
    
    }
    
    /**
     * Formats the flag filter array.
     * 
     * @author Peter Hamm
     * @param array $flagFilter
     * @return array
     */
    static protected function formatFilterFlags($flagFilter)
    {
        
        $returnValue = @$flagFilter && count($flagFilter) > 0 ? $flagFilter : array();
                
        $hasFlagBookableTimes = false;
        $hasFlagWorkPlan = false;
        
        for($i = 0; $i < count($flagFilter); ++$i)
        {
            
            if($flagFilter[$i]['key'] == CalsyBookableTimesModule::FLAG_BOOKABLE_TIME)
            {
                
                $hasFlagBookableTimes = true;
                
            }
            
            if($flagFilter[$i]['key'] == CalsyWorkPlan::FLAG_WORK_PLAN_ID)
            {
                
                $hasFlagWorkPlan = true;
                
            }
            
        }
        
        if(!$hasFlagBookableTimes)
        {
        
            $flagBookableTimes = Array(
                
                'key' => CalsyBookableTimesModule::FLAG_BOOKABLE_TIME,
                'operator' => '!=',
                'value' => '1'
                
            );
            
            array_push($returnValue, $flagBookableTimes);
            
        }
        
        if(!$hasFlagWorkPlan)
        {
            
            $flagWorkPlan = Array(
                
                'key' => CalsyWorkPlan::FLAG_WORK_PLAN_ID,
                'operator' => '=',
                'value' => ''
                
            );
            
            array_push($returnValue, $flagWorkPlan);
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the query string to filter by a flag
     * 
     * @author Peter Hamm
     * @param Array $flagsList A list of flags, e.g. from CalsyCalendarFilter->getFlags()
     * @return String
     */
    public static function addFlagFilterToQueryString($originalQuery, $flagsList)
    {
        
        $returnValue = $originalQuery;
        
        $flagsList = static::formatFilterFlags($flagsList);
                                
        if(@$flagsList && count($flagsList) > 0)
        {

            $flagFilterItems = array();

            for($i = 0; $i < count($flagsList); ++$i)
            {
                
                $ignoredFlags = array(
                    
                    CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID,
                    CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE
                    
                );
                
                // Ignore certain flags here.
                if(in_array($flagsList[$i]['key'], $ignoredFlags))
                {
                    
                    //continue;
                    
                }
                                                
                if(!$flagsList[$i]['value'] || is_null($flagsList[$i]['value']) || strlen($flagsList[$i]['value']) == 0)
                {
                                                            
                    // Search for empty flag values
                    
                    $flagString = "'%" . $flagsList[$i]['key'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE . '%' . CalsyCalendarEntryFlag::SQL_SEPERATOR_FLAG . "%'";
                    
                    if($flagsList[$i]['operator'] == '!=')
                    {
                        
                        // The flag must have at least one character
                        
                        $flagString = "'%" . $flagsList[$i]['key'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE . '%_%' . CalsyCalendarEntryFlag::SQL_SEPERATOR_FLAG . "%'";

                        $flagQuery = "(result.flags LIKE {$flagString})";

                    }
                    else
                    {
                                                
                        // The flag must not have any character at all or not be contained at all.
                        
                        $flagString = "'%" . $flagsList[$i]['key'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE . '' . CalsyCalendarEntryFlag::SQL_SEPERATOR_FLAG . "%'";

                        $flagQuery = "(result.flags LIKE {$flagString} OR result.flags IS NULL OR result.flags NOT LIKE '%" . $flagsList[$i]['key'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE . "%')";

                    }

                }
                else
                {
                                        
                    // Search for a distinct flag value
                    
                    $flagString = "'%" . $flagsList[$i]['key'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_VALUE . @$flagsList[$i]['value'] . CalsyCalendarEntryFlag::SQL_SEPERATOR_FLAG . "%'";

                    if($flagsList[$i]['operator'] == '!=')
                    {

                        $flagQuery = "(result.flags IS NULL OR result.flags NOT LIKE {$flagString})";

                    }
                    else
                    {

                        $flagQuery = "result.flags LIKE {$flagString}";

                    }
                
                }
                                
                array_push($flagFilterItems, 
                        
                    Array(

                        'logic' => isset($flagsList[$i]['logic']) && strtolower($flagsList[$i]['logic']) != 'and' ? 'OR' : 'AND',
                        'query' => $flagQuery

                    )
                
                );

            }

            if(count($flagFilterItems) > 0)
            {
                
                $flagFilterStringAnd = '';
                $flagFilterStringOr = '';
                
                for($i = 0; $i < count($flagFilterItems); ++$i)
                {
                    
                    if(strtoupper($flagFilterItems[$i]['logic']) == 'AND')
                    {
                        
                        $flagFilterStringAnd .= (strlen($flagFilterStringAnd) > 0 ? ' AND ' : '') . $flagFilterItems[$i]['query'];
                    
                    }
                    else
                    {
                        
                        $flagFilterStringOr .= (strlen($flagFilterStringOr) > 0 ? ' OR ' : '') . $flagFilterItems[$i]['query'];
                        
                    }
                    
                }
                
                $flagFilterString = (strlen($flagFilterStringAnd) > 0 ? '(' . $flagFilterStringAnd . ')' : '');
                $flagFilterString .= (strlen($flagFilterStringAnd) > 0 && strlen($flagFilterStringOr) > 0 ? ' OR ' : '');
                $flagFilterString .= (strlen($flagFilterStringOr) > 0 ? $flagFilterStringOr : '');

                $returnValue = "FROM (SELECT *" . $originalQuery . ") as result WHERE " . $flagFilterString;

            }
            
        }
        
        return $returnValue;
        
    }
    
}
