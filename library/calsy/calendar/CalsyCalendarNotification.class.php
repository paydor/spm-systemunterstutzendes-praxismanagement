<?php

/**
 * Notifications for calendar entries
 *
 * @author Peter Hamm
 * @date 2016-05-08
 */
class CalsyCalendarNotification
{
    
    protected $timestampBegin;
    protected $interval;
    protected $notificationCount = 0;
    
    /**
     * Initializes the object so that notifications can later be sent out.
     * 
     * Don't forget to specify the $interval the notifications are sent out.
     * It should match the smallest possible time interval for notifications. (See static::getPossibleNotificationTimes())
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $interval
     */
    public function __construct($timestampBegin, $interval)
    {
        
        PerisianFrameworkToolbox::security($interval);
        PerisianFrameworkToolbox::security($timestampBegin);
        
        if($interval <= 0 || $interval > static::getSmallestPossibleNotificationTimeInterval())
        {
            
            // Invalid interval
            throw new PerisianException(PerisianLanguageVariable::getVariable(11188));
            
        }
        
        if($timestampBegin <= 0)
        {
            
            // Invalid time
            throw new PerisianException(PerisianLanguageVariable::getVariable(11189));
            
        }
        
        $this->interval = (int)$interval;
        $this->timestampBegin = (int)$timestampBegin;
        
    }
    
    /**
     * Retrieves the count of sent notifications.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getNotificationCount()
    {
        
        return $this->notificationCount;
        
    }
    
    /**
     * Checks if the calendar notifications are enabled or not.
     * 
     * @author Peter Hamm
     * @param mixed $userBackendOrUserFrontend Should be a backend user or a frontend user
     * @return bool
     */
    public static function isEnabled($userBackendOrUserFrontend)
    {
        
        $isEnabled = false;
                
        if($userBackendOrUserFrontend instanceof CalsyUserBackend)
        {
                        
            $isEnabled = PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications_users') == 1;
            
        }
        else if($userBackendOrUserFrontend instanceof CalsyUserFrontend) 
        {
            
            $isEnabled = PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications_accounts') == 1;
            
        }
        
        return $isEnabled;
        
    }
    
    /**
     * Retrieves the smallest possible notification time interval.
     * Will return 0 if none is defined.
     * 
     * @author Peter Hamm
     * @return int
     */
    protected static function getSmallestPossibleNotificationTimeInterval()
    {
        
        $notificationTimes = static::getPossibleNotificationTimes();
        
        $smallestTime = 0;
        
        foreach($notificationTimes as $time => $label)
        {
            
            $smallestTime = $time;
            
            break;
            
        }
        
        return $smallestTime;
        
    }
    
    /**
     * Retrieves the tallest possible notification time interval.
     * Will return 0 if none is defined.
     * 
     * @author Peter Hamm
     * @return int
     */
    protected static function getTallestPossibleNotificationTimeInterval()
    {
        
        $notificationTimes = static::getPossibleNotificationTimes();
        krsort($notificationTimes, SORT_NUMERIC);
                
        $tallestTime = 0;
        
        foreach($notificationTimes as $time => $label)
        {
            
            $tallestTime = $time;
            
            break;
            
        }
        
        return $tallestTime;
        
    }
    
    /**
     * Retrieves the possible notification times.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getPossibleNotificationTimes()
    {
        
        $notificationTimes = array(
            
            3600 => '1 ' . PerisianLanguageVariable::getVariable(11182),
            7200 => '2 ' . PerisianLanguageVariable::getVariable(11183),
            10800 => '3 ' . PerisianLanguageVariable::getVariable(11183),
            21600 => '6 ' . PerisianLanguageVariable::getVariable(11183),
            43200 => '12 ' . PerisianLanguageVariable::getVariable(11183),
            
            86400 => '1 ' . PerisianLanguageVariable::getVariable(11180),
            172800 => '2 ' . PerisianLanguageVariable::getVariable(11181),
            259200 => '3 ' . PerisianLanguageVariable::getVariable(11181)
            
        );
        
        ksort($notificationTimes, SORT_NUMERIC);
        
        return $notificationTimes;
        
    }
    
    /**
     * Retrieves all calendar entries that are considered for notifications.
     * 
     * @author Peter Hamm
     * @return array An Array of CalsyCalendarEntry objects
     */
    protected function getAfflictedCalendarEntries()
    {
        
        $timestampBegin = $this->timestampBegin;
        $timestampEnd = $timestampBegin + static::getTallestPossibleNotificationTimeInterval();
        
        $filter = new CalsyCalendarFilter();
        
        $entries = CalsyCalendarEntry::getCalendarEntriesForFilter($filter, $timestampBegin, $timestampEnd);
        
        $entryDummy = new CalsyCalendarEntry();
        $returnEntries = array();
        
        for($i = 0; $i < count($entries); ++$i)
        {
            
            $calendarEntry = new CalsyCalendarEntry($entries[$i][$entryDummy->field_pk]);
            
            if(!isset($calendarEntry->{$calendarEntry->field_parent_entry_id}) || empty($calendarEntry->{$calendarEntry->field_parent_entry_id}))
            {
                
                $calendarEntry->updateDurationFromChildren();
                $calendarEntry->removeTimeDurationTitle();
                
                array_push($returnEntries, $calendarEntry);
                
            }
            
        }
        
        return $returnEntries;
        
    }
    
    /**
     * Retrieves the notification settings from the system.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getNotificationSettingsForSystem()
    {
        
        $returnValue = array(
            
            'enabled' => (PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications') == 1),
            'time' => PerisianSystemSetting::getSettingValue('calendar_default_time_notifications')
            
        );
        
        return $returnValue;
        
    }
    
    /**
     * Sends out notifications to all afflicted calendar entries' users and frontend users
     * if the notification module is enabled system-wide.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function sendNotifications()
    {
                
        $calendarEntries = $this->getAfflictedCalendarEntries();
                
        for($i = 0; $i < count($calendarEntries); ++$i)
        {
                        
            $this->sendNotificationToUser($calendarEntries[$i]);
            $this->sendNotificationToAccounts($calendarEntries[$i]);
            
        }
                
    }
    
    /**
     * Checks if notifications should be send to the user related to the specifed calendar entry 
     * and then sends it, if applicable.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntry $entry
     * @return void
     */
    public function sendNotificationToUser(CalsyCalendarEntry $entry)
    {
        
        $entryId = $entry->{$entry->field_pk};
        $userId = $entry->{$entry->field_user_id};
                
        $notificationSetting = CalsyCalendarEntryNotificationRelationUser::getDataForCalendarEntryAndRelation($entryId, $userId);
        
        if($notificationSetting['enabled'] == 1)
        {
            
            $sendWithinFrom = $entry->{$entry->timestamp_begin} - $notificationSetting['time'];
            $sendWithinTo = $sendWithinFrom + $this->interval;
            
            if($this->timestampBegin >= $sendWithinFrom && $this->timestampBegin < $sendWithinTo)
            {
                
                $mailUser = new CalsyUserBackend($userId);
                $mailUser->sendEmailCalendarEntryReminder(new CalsyCalendarEntry($entryId), false);
                
                ++$this->notificationCount;
                
            }
            
        }
        
    }
    
    /**
     * Checks if notifications should be send to the accounts related to the specifed calendar entry 
     * and then sends it, if applicable.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntry $entry
     * @return void
     */
    public function sendNotificationToAccounts(CalsyCalendarEntry $entry)
    {
        
        $accounts = $entry->getUserFrontendWithOrders();
        
        for($i = 0; $i < count($accounts); ++$i)
        {
            
            $account = $accounts[$i]['user_frontend'];
            
            $entryId = $entry->{$entry->field_pk};
            $accountId = $account->{$account->field_pk};

            $notificationSetting = CalsyCalendarEntryNotificationRelationAccount::getDataForCalendarEntryAndRelation($entryId, $accountId);

            if($notificationSetting['enabled'] == 1)
            {

                $sendWithinFrom = $entry->{$entry->timestamp_begin} - $notificationSetting['time'];
                $sendWithinTo = $sendWithinFrom + $this->interval;

                if($this->timestampBegin >= $sendWithinFrom && $this->timestampBegin < $sendWithinTo)
                {
                    
                    $account = new CalsyUserFrontend($accountId);
                    $account->sendEmailCalendarEntryReminder(new CalsyCalendarEntry($entryId), false);
                    
                    ++$this->notificationCount;

                }

            }
            
        }
        
    }

}

/**
 * Handles the link between calendar entry and notification settings, for a user-defined relation $type.
 * 
 * @author Peter Hamm
 */
abstract class CalsyCalendarEntryNotificationRelation extends PerisianDatabaseModel
{
    
    public $table = 'calsy_calendar_entry_notification';
    public $field_pk = 'calsy_calendar_entry_notification_id';
    public $field_entry_id = 'calsy_calendar_entry_notification_entry_id';
    public $field_foreign_id = 'calsy_calendar_entry_notification_foreign_id';
    public $field_is_enabled = 'calsy_calendar_entry_notification_is_enabled';
    public $field_time = 'calsy_calendar_entry_notification_time';
    public $field_type = 'calsy_calendar_entry_notification_type';

    public $type = "";
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
        
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Deletes all notification settings for the specified calendar entry identifer.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return void
     */
    public static function deleteNotificationSettingsForCalendarEntry($calendarEntryId)
    {
        
        $dummy = new static();
        
        $query = "{$dummy->field_entry_id} = '{$calendarEntryId}'";
        
        $dummy->delete($query);
        
    }
    
    /**
     * Deletes all notification settings for the specified relation entry identifer.
     * 
     * @author Peter Hamm
     * @param int $relationId
     * @return void
     */
    public static function deleteNotificationSettingsForRelation($relationId)
    {
        
        $dummy = new static();
        
        $query = "{$dummy->field_foreign_id} = '{$relationId}'";
        
        $dummy->delete($query);
        
    }
    
    /**
     * Retrieves the notification the CalsyCalendarEntryNotificationRelation for the specified identifiers.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $relationId
     * @return CalsyCalendarEntryNotificationRelation
     */
    public static function getObjectForCalendarEntryAndRelation($calendarEntryId, $relationId)
    {
                
        $dummy = new static();
                
        $data = $dummy->getData(static::getQueryForEntryAndRelation($calendarEntryId, $relationId));
        
        $identifier = 0;
        
        if(count($data) > 0)
        {
            
            $identifier = $data[0][$dummy->field_pk];
            
        }
        
        $object = new static($identifier);
        
        $object->{$object->field_entry_id} = $calendarEntryId;
        $object->{$object->field_foreign_id} = $relationId;
        
        return $object;
        
    }
    
    /**
     * Retrieves the query string for the specified identifiers.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $relationId
     * @return String
     */
    protected static function getQueryForEntryAndRelation($calendarEntryId, $relationId)
    {
        
        PerisianFrameworkToolbox::security($calendarEntryId);
        PerisianFrameworkToolbox::security($relationId);
        
        $dummy = new static();
                
        return "{$dummy->field_entry_id} = '{$calendarEntryId}' AND {$dummy->field_foreign_id} = '{$relationId}' AND {$dummy->field_type} = '{$dummy->type}'";
        
    }
    
    /**
     * Retrieves the notification setting for the specified calendar entry identifier and relation identifer.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $relationId
     * @return array An associative array
     */
    public static function getDataForCalendarEntryAndRelation($calendarEntryId, $relationId)
    {
        
        PerisianFrameworkToolbox::security($calendarEntryId);
        PerisianFrameworkToolbox::security($relationId);
        
        $dummy = new static();
        
        $returnData = static::getDefaultValueForRelation($relationId);
        
        $data = $dummy->getData(static::getQueryForEntryAndRelation($calendarEntryId, $relationId));
        
        if(count($data) > 0)
        {
            
            $returnData['enabled'] = $data[0][$dummy->field_is_enabled] == 1;
            $returnData['time'] = $data[0][$dummy->field_time];
            
        }
        
        $returnData['type'] = $dummy->type;
        
        return $returnData;
        
    }
    
    /**
     * Retrieves a default value for the specified relation identifier. 
     * 
     * @author Peter Hamm
     * @param int $relationId
     * @return array
     */
    protected static function getDefaultValueForRelation($relationId)
    {
                
        $returnData = array(
            
            'enabled' => false,
            'time' => 0
            
        );
        
        return $returnData;
        
    }
    
    /**
     * Retrieves the default notification settings of the system
     * 
     * @author Peter Hamm
     * @return void
     */
    protected static function getDefaultValueForSystem()
    {
        
        $returnData = array(
            
            'enabled' => (PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications') == 1),
            'time' => PerisianSystemSetting::getSettingValue('calendar_default_time_notifications')
            
        );
                
        return $returnData;
        
    }
    
    /**
     * Fired directly before the object is saved to the database.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
        
        $this->{$this->field_type} = $this->type;
       
        return true;
        
    }
    
}

class CalsyCalendarEntryNotificationRelationUser extends CalsyCalendarEntryNotificationRelation
{
    
    public $type = "user";
    
    /**
     * Retrieves a default value for the specified relation identifier. 
     * 
     * @author Peter Hamm
     * @param int $relationId
     * @return array
     */
    protected static function getDefaultValueForRelation($relationId)
    {
            
        $relationSetting = new PerisianUserSetting();
             
        $returnData = static::getDefaultValueForSystem();
        
        $relationIsEnabled = $relationSetting->getSetting($relationId, 'user_calendar_notification_is_enabled');
        $relationTime = $relationSetting->getSetting($relationId, 'user_calendar_notification_default_time');
        
        if(strlen($relationIsEnabled) > 0)
        {
            
            $returnData['enabled'] = $relationIsEnabled == 1;
            
        }
        
        if(strlen($relationTime) > 0)
        {
            
            $returnData['time'] = $relationTime;
            
        }
                
        return $returnData;
        
    }
    
}

class CalsyCalendarEntryNotificationRelationAccount extends CalsyCalendarEntryNotificationRelation
{
    
    public $type = "account";
    
    /**
     * Retrieves a default value for the specified relation identifier. 
     * 
     * @author Peter Hamm
     * @param int $relationId
     * @return array
     */
    protected static function getDefaultValueForRelation($relationId)
    {
            
        $relationSetting = new CalsyUserFrontendSetting();
             
        $returnData = static::getDefaultValueForSystem();
        
        $relationIsEnabled = $relationSetting->getSetting($relationId, 'account_calendar_notification_is_enabled');
        $relationTime = $relationSetting->getSetting($relationId, 'account_calendar_notification_default_time');
        
        if(strlen($relationIsEnabled) > 0)
        {
            
            $returnData['enabled'] = $relationIsEnabled == 1;
            
        }
        
        if(strlen($relationTime) > 0)
        {
            
            $returnData['time'] = $relationTime;
            
        }
                
        return $returnData;
        
    }
    
}