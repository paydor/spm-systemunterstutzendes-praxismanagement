<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/appointment_assistant/CalsyAppointmentAssistantLog.class.php';


/**
 * Appointment assistant log controller
 *
 * @author Peter Hamm
 * @date 2018-03-25
 */
class CalsyAppointmentAssistantLogController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'appointment_assistant/log/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyAppointmentAssistantLog();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'appointment_assistant/log/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'dummy' => $dummy,
            
            'list' => CalsyAppointmentAssistantLog::getLog($sortOrder, $offset, $limit),
            'count' => CalsyAppointmentAssistantLog::getLogCount(),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder
            
        );
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Empties the complete log
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionEmptyLog()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        CalsyAppointmentAssistantLog::emptyLog();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}