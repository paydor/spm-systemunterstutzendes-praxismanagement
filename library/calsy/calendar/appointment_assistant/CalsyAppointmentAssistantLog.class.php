<?php

class CalsyAppointmentAssistantLog extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'calsy_appointment_assistant_log';
    public $field_pk = 'calsy_appointment_assistant_log_id';
    public $field_time = 'calsy_appointment_assistant_log_time';
    public $field_filter = 'calsy_appointment_assistant_log_filter';
    public $field_result = 'calsy_appointment_assistant_log_result';
    public $field_query = 'calsy_appointment_assistant_log_query';
    
    private static $queries = Array();
    
    /**
     * Retrieves whether the log is enabled or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isEnabled()
    {
        
        $value = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_appointment_assistant_logging_enabled');
                
        return $value == "1";
        
    }
        
    /**
     * Overwritten constructor.
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
            
        PerisianFrameworkToolbox::security($entryId);
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $this->{$this->field_pk} = $entryId;
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Resets the stored queries.
     * 
     * @author Peter Hamm
     * @return void
     */
    public static function resetQueries()
    {
        
        static::$queries = Array();
        
    }
    
    /**
     * Adds a query to the list of queries.
     * 
     * @author Peter Hamm
     * @param String Title A title for the query, e.g. what it does
     * @param String $query
     * @param bool $reset Reset previous queries? Default: false.
     * @return void
     */
    public static function addQuery($title, $query, $details = null, $reset = false)
    {
        
        array_push(static::$queries, Array('title' => $title, 'details' => $details, 'query' => $query));
        
        if($reset)
        {
            
            static::resetQueries();
            
        }
        
    }
        
    /**
     * Logs result data
     * 
     * @author Peter Hamm
     * @param CalsyCalendarFilter $filter
     * @param Array $result
     * @param bool $logQueries Optional, log the last appointment assistant log queries? Default: false.
     * @return int The identifier of the saved data set, or null if the logging is disabled
     */
    public static function logResult($filter, $result, $logQueries = false)
    {
        
        if(!static::isEnabled())
        {
            
            return null;
            
        }
        
        $object = new static();
        
        $object->{$object->field_filter} = static::encodeData(print_r($filter, true));
        $object->{$object->field_result} = static::encodeData(print_r($result, true));
        
        if($logQueries)
        {
            
            $object->{$object->field_query} = static::encodeData(print_r(static::$queries, true));
            
            static::resetQueries();
            
        }
        
        $object->{$object->field_time} = PerisianFrameworkToolbox::sqlTimestamp();
        
        $id = $object->save();
        
        return $id;
        
    }
    
    /**
     * Encodes data used for the log
     * 
     * @author Peter Hamm 
     * @param mixed $data An object or Array
     * @return String
     */
    public static function encodeData($data)
    {
        
        $data = base64_encode($data);
        
        return $data;
        
    }
    
    /**
     * Decodes data used for the log
     * 
     * @author Peter Hamm 
     * @param String The encoded data 
     * @return mixed $data An object or Array
     */
    public static function decodeData($data)
    {
        
        $data = base64_decode($data);
        
        return $data;
        
    }
    
    /**
     * Clears the whole log.
     * 
     * @author Peter Hamm
     * @return void
     */
    public static function emptyLog()
    {
        
        $entryDummy = new CalsyAppointmentAssistantLog();
        
        $entryDummy->delete('true');
        
    }
    
    
    /**
     * Retrives a list of log entries for the specified parameters.
     * 
     * @author Peter Hamm
     * @param String $order Optional. The sort order, either "ASC" or "DESC", default: "DESC".
     * @param int $offset Optional, default: 0
     * @param int $limit Optional, default: 25
     */
    static public function getLog($order = "DESC", $offset = 0, $limit = 25)
    {
        
        $entryDummy = new static();
        
        $order = strtoupper($order);
        $order = ($order != "ASC" && $order != "DESC") ? "DESC" : $order;
        
        $retrievedData = $entryDummy->getData('', $entryDummy->field_time, $order, $offset, $limit);
        
        $formattedReturnData = array();
        
        for($i = 0; $i < count($retrievedData); ++$i)
        {
            
            $entryData = $retrievedData[$i];
            
            $entryData[$entryDummy->field_filter] = static::decodeData($entryData[$entryDummy->field_filter]);
            $entryData[$entryDummy->field_result] = static::decodeData($entryData[$entryDummy->field_result]);
            $entryData[$entryDummy->field_query] = static::decodeData($entryData[$entryDummy->field_query]);
                                    
            array_push($formattedReturnData, $entryData);
            
        }
        
        return $formattedReturnData;
        
    }

    /**
     * Retrives the count of log entries for the specified parameters.
     * 
     * @author Peter Hamm
     * @return int
     */
    static public function getLogCount()
    {
        
        $entryDummy = new static();
        $entryCount = $entryDummy->getCount('');
        
        return $entryCount;
        
    }
    
}