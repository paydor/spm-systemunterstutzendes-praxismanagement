<?php

require_once 'modules/CalsyAreaModule.class.php';
require_once 'modules/CalsyUserFrontendModule.class.php';
require_once 'modules/CalsyUserBackendGroupModule.class.php';
require_once 'modules/CalsyOrderModule.class.php';
require_once 'modules/CalsyWorkPlanModule.class.php';
require_once 'modules/CalsyTableReservationModule.class.php';
require_once "CalsyCalendarFilter.class.php";
require_once "CalsyCalendarEntry.class.php";
require_once "CalsyCalendarNotification.class.php";
require_once "CalsyCalendarExport.class.php";

/**
 * Main calendar class of calSy
 *
 * @author Peter Hamm
 * @date 2016-01-12
 */
class CalsyCalendar
{
        
    // Instance
    private static $instance = null;
    
    // Entry instance
    private static $entryInstance = null;
    
    private static $dayNames = null;
    private static $displayableDays = null;
    private static $allowedDisplayTypes = array("year", "month", "week", "day");
    
    private static $userList = array();
    
    private static $calendarSettingsIds = array(
        
        "default_event_end_time" => 63
        
    );
    
    private static $calendarSettingList = array();
    
    private static $calendarHourlySegmentInterval = null;
    private static $calendarHourlyTimes = null;
    
    /**
     * Retrieves the calendar setting identifier for the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return int
     */
    protected static function getCalendarSettingId($key)
    {
        
        return @self::$calendarSettingsIds[$key];
        
    }
    
    /**
     * Retrieves a rounded version of the begin and end times between which appointments for 
     * the hourly views (weekly/daily) should be displayed.
     * 
     * E.g., if the begin time is specified as '09:23' in the system settings, this will return '9'.
     * Hence, if the end time is specified as '10:20', it will return '11'.
     * 
     * The default would be from '0' to '24'.
     * 
     * @author Peter Hamm
     * @return Array An associative Array containing 'begin' and 'end'.
     */
    public static function getHourlyTimes()
    {
        
        if(is_null(static::$calendarHourlyTimes))
        {
            
            $hourlyTimes = json_decode(PerisianFrameworkToolbox::securityRevert((PerisianSystemSetting::getSettingValue('calendar_times_hourly'))), true);
            
            if(isset($hourlyTimes['begin']))
            {
                
                $formattedTimes = array(
                    
                    'begin' => (int)substr($hourlyTimes['begin'], 0, 2),
                    'end' => (int)substr($hourlyTimes['end'], 0, 2)
                    
                );
                
                if((int)substr($hourlyTimes['end'], -2) > 0)
                {
                    
                    $formattedTimes['end'] += 1;
                    
                }
                
                static::$calendarHourlyTimes = $formattedTimes;
                
            }
            
        }
        
        if(is_null(static::$calendarHourlyTimes))
        {
            
            static::$calendarHourlyTimes = array(
                
                'begin' => 0,
                'end' => 24
                 
            );
            
        }
        
        return static::$calendarHourlyTimes;
        
    }
    
    /**
     * Retrieves the interval into which the segments in the 
     * hourly views (weekly/daily) should be seperated.
     * 
     * @author Peter Hamm
     * @return int
     */
    public static function getHourlySegmentInterval()
    {
        
        if(is_null(static::$calendarHourlySegmentInterval))
        {
            
            static::$calendarHourlySegmentInterval = PerisianSystemSetting::getSettingValue('calendar_interval_segmentation');
            
        }
        
        if(static::$calendarHourlySegmentInterval <= 0)
        {
            
            static::$calendarHourlySegmentInterval = 1800;
            
        }
        
        return static::$calendarHourlySegmentInterval;
        
    }
    
    /**
     * Retrieves the next full hour relative to the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return int
     */
    public static function getNextFullHour($timestamp)
    {
        
        $nextHourStampBase = $timestamp + 3600;
        
        return mktime(date("H", $nextHourStampBase), 0, 0, date("m", $nextHourStampBase), date("d", $nextHourStampBase), date("Y", $nextHourStampBase));
        
    }
    
    /**
     * Retrieves if the specified day is withing the opening times by the currently logged in entity.
     * For example:
     * If the logged in entity is a user, it's always bookable.
     * If it is another entity (e.g. a frontend user) and the bookable times module is not active, the opening times setting from the backend is considered.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @param int $checkTimestamp
     * @return boolean
     */
    public static function isDayBookable($checkTimestamp)
    {
        
        global $user, $userFrontend;
        
        if(isset($user) && $user->isLoggedIn())
        {
            
            return true;
            
        }
        
        // The earliest possible booking time is tomorrow
        if(!self::isTimestampAfterFirstSecondOfTomorrow($checkTimestamp))
        {
            
            return false;
            
        }
        
        $dayIndex = date("N", $checkTimestamp) - 1;
        
        if(isset($userFrontend) && $userFrontend->isLoggedIn())
        {
            
            // Is this witing the opening times (considering the bookable times are not activated)
            if(!CalsyBookableTimesModule::isEnabled() && CalsyOpeningTimesModule::isEnabled())
            {
                
                $openingTimes = CalsyOpeningTimesModule::getOpeningTimes();

                $dayIndexFound = false;

                for($i = 0; $i < count($openingTimes); ++ $i)
                {

                    if($openingTimes[$i]["day"] == $dayIndex)
                    {

                        $dayIndexFound = true;

                        break;

                    }

                }

                if(!$dayIndexFound)
                {

                    return false;

                }
                
            }
                        
            // Check if it falls on a holiday
            {
                
                $firstSecondOfDay = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($checkTimestamp);
                $lastSecondOfDay = PerisianTimeZone::getLastSecondOfDayAsTimestamp($checkTimestamp);
                
                $isOnHoliday = static::isTimespanOnHoliday($firstSecondOfDay, $lastSecondOfDay);
                
                if($isOnHoliday)
                {
                    
                    return false;
                    
                }
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Retrieves if the timespan with the specified $timestampBegin and $timestampEnd are within a holiday.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @return bool
     */
    public static function isTimespanOnHoliday($timestampBegin, $timestampEnd)
    {
        
        $entryInstance = static::getEntryInstance();
        
        return $entryInstance->isTimespanOnHoliday($timestampBegin, $timestampEnd);
        
    }
    
    /**
     * Checks if the specified timestamp is after the first second of tomorrow.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return boolean
     */
    public static function isTimestampAfterFirstSecondOfTomorrow($timestamp)
    {
        
        $firstMinuteOfTomorrow = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time() + 86400);

        if($timestamp < $firstMinuteOfTomorrow)
        {

            return false;

        }
        
        return true;
        
    }
    
    /**
     * Retrieves if the specified minute is bookable by the currently logged in entity.
     * If the logged in entity is a user, it's always bookable.
     * If it is another entity (e.g. a frontend user), the bookable times setting from the backend is considered.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @param int $checkTimestamp
     * @return boolean
     */
    public static function isMinuteBookable($checkTimestamp)
    {
        
        global $user, $userFrontend;
        
        if(isset($user) && $user->isLoggedIn())
        {
            
            return true;
            
        }
        
        // The earliest possible booking time is tomorrow
        if(!self::isTimestampAfterFirstSecondOfTomorrow($checkTimestamp))
        {
            
            return false;
            
        }
        
        $returnValue = true;
        
        if(CalsyBookableTimesModule::isEnabled())
        {
            
            // Check the configured bookable times
            
            $returnValue = static::isWithinBookableTimes($checkTimestamp);            
            
        }
        
        if(!$returnValue && CalsyOpeningTimesModule::isEnabled())
        {
            
            // Check the opening times

            $dayIndex = date("N", $checkTimestamp) - 1;

            if(isset($userFrontend) && $userFrontend->isLoggedIn())
            {

                $openingTimes = CalsyOpeningTimesModule::getOpeningTimes();

                // Find entries for the day index.
                for($i = 0; $i < count($openingTimes); ++ $i)
                {

                    if($openingTimes[$i]["day"] == $dayIndex)
                    {

                        $minuteInDay = ((int)date("H", $checkTimestamp) * 60) + (int)date("i", $checkTimestamp);

                        if($minuteInDay >= $openingTimes[$i]["from"] && $minuteInDay < $openingTimes[$i]["to"])
                        {

                            $returnValue = true;
                            
                            break;

                        }

                    }

                }

            }
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Depending on the setup, will either retrieve a shortcut based on the user name
     * or the first name of the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return string
     */
    public static function getUserNameForDailyWeeklyView($userId)
    {
        
        $returnValue = '-';
                
        if(PerisianSystemSetting::getSettingValue('calendar_view_daily_weekly_show_firstnames') == 1)
        {
            
            // Format the full name to show only the first name.
            
            $returnValue = CalsyUserBackend::getUserFullname($userId);
            
            if(!CalsyUserBackend::isSpecialUser($userId))
            {

                $returnValue = strpos($returnValue, ' ') !== false ? substr($returnValue, 0, strpos($returnValue, ' ')) : $returnValue;
                
            }
            
        }
        else
        {
            
            $returnValue = PerisianUser::getUserFullnameShortcut(CalsyUserBackend::getUserFullname($userId));
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves if the specified timesamp is within the system's bookable times.
     * 
     * @author peter Gamm
     * @param int $timestamp
     * @return boolean
     */
    public static function isWithinBookableTimes($timestamp)
    {
        
        $returnValue = false;
        
        if(CalsyBookableTimesModule::isEnabled())
        {
            
            $entryInstance = new CalsyCalendarEntry();
            
            $allowedTimes = CalsyCalendarEntry::getCalendarEntriesForFilter(CalsyBookableTimesModule::getEntryFilter(), $timestamp, PerisianTimeZone::getLastSecondOfDayAsTimestamp($timestamp));
            
            foreach($allowedTimes as $allowedTime)
            {
                
                if($allowedTime[$entryInstance->timestamp_begin] <= $timestamp && $allowedTime[$entryInstance->timestamp_end] >= $timestamp)
                {
                    
                    $returnValue = true;
                    
                    break;
                    
                }
                
            }
            
        }
        
        if(!$returnValue && CalsyOpeningTimesModule::isEnabled())
        {
            
            // Check the opening times
        
            $openingTimes = CalsyOpeningTimesModule::getOpeningTimes();

            for($i = 0; $i < count($openingTimes); ++$i)
            {

                if($openingTimes[$i]["day"] == (date("N", $timestamp) - 1))
                {

                    $firstMinuteOfTheDay = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestamp);

                    $from = $firstMinuteOfTheDay + $openingTimes[$i]["from"] * 60;
                    $to = $firstMinuteOfTheDay + $openingTimes[$i]["to"] * 60;

                    if($timestamp >= $from && $timestamp <= $to)
                    {

                        $returnValue = true;

                        break;
                        
                    }

                }

            }

        }
        
        if(!CalsyBookableTimesModule::isEnabled() && !CalsyOpeningTimesModule::isEnabled())
        {
            
            $returnValue = true;
            
        }
        
        return $returnValue;
        
    }
        
    /**
     * Retrieves the season for the specified month number (1-12).
     * 
     * @author Peter Hamm
     * @param int $month Values from 1 to 12.
     * @return string 'invalid', 'winter', 'spring', 'summer' or 'fall'.
     */
    public static function getSeasonForMonth($month)
    {
        
        $season = 'invalid';
        
        if($month == 1 || $month == 2 || $month == 12)
        {
            
            $season = 'winter';
            
        }
        elseif($month >= 3 && $month <= 5)
        {
            
            $season = 'spring';
            
        }
        elseif($month >= 6 && $month <= 8)
        {
            
            $season = 'summer';
            
        }
        elseif($month >= 9 && $month <= 11)
        {
            
            $season = 'fall';
            
        }
        
        return $season;
        
    }
    
    /**
     * Retrieves the default end time after the begin of an event, in seconds. 
     * (A.k.a. "Duration" of an event)
     * 
     * @author Peter Hamm
     * @date 2016-02-03
     * @return int Seconds
     */
    public static function getDefaultEventEndTime()
    {
        
        $defaultTime = (int)self::getCalendarSetting("default_event_end_time");
        
        if(strlen($defaultTime) == 0 || $defaultTime < 0)
        {
            
            $defaultTime = 3600;
            
        }
        
        return $defaultTime;
        
    }
    
    /**
     * Retrieves the default start time of an event as defined in the calendar settings, in seconds.
     * For example, the default value would be 43200, which resembles 12:00 p.m.
     * 
     * @author Peter Hamm
     * @date 2016-12-29
     * @return int Seconds
     */
    public static function getDefaultEventStartTime()
    {
        
        $defaultTime = PerisianSystemSetting::getSettingValue("calendar_default_start_time");
        
        if(strlen($defaultTime) == 0 || $defaultTime < 0)
        {
            
            $defaultTime = 43200;
            
        }
        
        return $defaultTime;
        
    }
    
    /**
     * Retrieves 
     */
    protected static function getCalendarSetting($settingKey)
    {
        
        if(!array_key_exists($settingKey, self::$calendarSettingsIds))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10820) . ': "' . $settingKey . '"');
            
        }
        
        if(!array_key_exists($settingKey, self::$calendarSettingList))
        {

            $setting = new PerisianSystemSetting(self::getCalendarSettingId($settingKey));
            
            self::$calendarSettingList[$settingKey] = $setting->getValue();
                
        }
        
        return self::$calendarSettingList[$settingKey];
        
    }
    
    /**
     * Compares two provides timestamps and returns whether they're on the same day or not.
     * 
     * @author Peter Hamm
     * @param int $timestampOne
     * @param int $timestampTwo
     */
    public static function isSameDay($timestampOne, $timestampTwo)
    {
        
        $dayOne = gmdate("Ymd", $timestampOne);
        $dayTwo = gmdate("Ymd", $timestampTwo);
        
        return $dayOne == $dayTwo;
        
    }
    
    /**
     * Generates a list with times in the specified interval, from $begin to $end.
     * 
     * @author Peter Hamm
     * @date 2016-02-03
     * @param int $interval Seconds to count up in each iteration, default: 900 (15 minutes)
     * @param int $begin Seconds to start from, default: 0
     * @param int $end Seconds to end at, default: 86400 (1 day)
     * @param bool $asClock Optional, changes the output format of the values to resemble a clock (e.g. '13:23h'). Default: false.
     * @return array
     */
    public static function getTimeList($interval = 900, $begin = 0, $end = 86400, $asClock = false)
    {
        
        $list = array();
        
        for($currentTime = $begin; $currentTime <= $end; $currentTime += $interval)
        {
                                    
            $list[$currentTime] = static::getTimeFormatted($currentTime, $asClock);
            
        }
        
        return $list;
        
    }
    
    /**
     * Formates the specified time in seconds into an easily read- and displayable value.
     * 
     * @author Peter Hamm
     * @param int $timeToFormatInSeconds
     * @param bool $asClock Optional, changes the output format of the values to resemble a clock (e.g. '13:23h'). Default: false.
     * @return String
     */
    public static function getTimeFormatted($timeToFormatInSeconds, $asClock = false)
    {
                
        $secondsInMinute = 60;
        $secondsInHour = 60 * $secondsInMinute;
        $secondsInDay = 24 * $secondsInHour;

        $days = floor($timeToFormatInSeconds / $secondsInDay);

        $hourSeconds = $timeToFormatInSeconds % $secondsInDay;
        $hours = floor($hourSeconds / $secondsInHour);

        $minuteSeconds = $hourSeconds % $secondsInHour;
        $minutes = floor($minuteSeconds / $secondsInMinute);

        $remainingSeconds = $minuteSeconds % $secondsInMinute;
        $seconds = ceil($remainingSeconds);

        $timeValues = array();

        if($days > 0)
        {

            array_push($timeValues, $days . " " . PerisianLanguageVariable::getVariable($days == 1 ? 10816 : 10812));

        }

        if($hours > 0)
        {

            array_push($timeValues, $hours . " " . PerisianLanguageVariable::getVariable($hours == 1 ? 10817 : 10813));

        }

        if($minutes > 0 || ($days == 0 && $hours == 0 && $minutes == 0 && $seconds == 0))
        {

            array_push($timeValues, $minutes . " " . PerisianLanguageVariable::getVariable($minutes == 1 ? 10818 : 10814));

        }

        if($seconds > 0)
        {

            array_push($timeValues, $seconds . " " . PerisianLanguageVariable::getVariable($seconds == 1 ? 10819 : 10815));

        }
        
        $returnValue = "";
        
        if($asClock)
        {
            
            $returnValue = ($hours < 10 ? "0" : "") . $hours . ":" . ($minutes < 10 ? "0" : "") . $minutes . " " . PerisianLanguageVariable::getVariable(10903);
            
        }
        else
        {
            
            $returnValue = implode(", ", $timeValues);
            
        }

        return $returnValue;
        
    }
    
    /**
     * Retrieves the localized name for the specified day index from 0 to 6.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getDayName($index)
    {
        
        $index = $index < 0 ? 0 : $index;
        $index = $index > 6 ? 6 : $index;
        
        $dayNames = self::getDayNames();
        
        return $dayNames[$index];
        
    }
    
    /**
     * Generates a nicely displayable time and date for the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return String
     */
    public static function getFormattedTimeLabel($timestamp)
    {
        
        $timeString = CalsyCalendar::getDayName(date("N", $timestamp) - 1) . ", " . date("d.m.y, H:i", $timestamp) . " " . PerisianLanguageVariable::getVariable(10903);
        
        return $timeString;
        
    }
    
    /**
     * Formats the inputted CSV list of user IDs into an array.
     * 
     * @author Peter Hamm
     * @param string $csvUserList
     * @returns array
     */
    public static function formatSelectedUsers($csvUserList)
    {
     
        $returnValue = array();
        $explodedList = explode(',', $csvUserList);
        
        for($i = 0; $i < count($explodedList); ++$i)
        {
            
            $intValue = (int)$explodedList[$i];
            
            if($intValue > 0 || $intValue == CalsyUserBackend::DEFAULT_USER_ID)
            {
                
                array_push($returnValue, $intValue);
                
            }
            
        }
        
        if(PerisianSystemSetting::getSettingValue('system_show_backend_users_sorted_by_key') == 1)
        {
         
            // Sort the user IDs by the specially defined sorting keys.
            $returnValue = CalsyUserBackend::sortUserIdsBySortingKey($returnValue);
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the user color
     * 
     * @author Peter Hamm
     * @param int $userId
     */
    public static function getUser($userId)
    {
        
        if(!isset(self::$userList[$userId]))
        {
            
            try 
            {
                
                $loadedUser = new PerisianUser($userId);
                
            } 
            catch(Exception $e) 
            {
                
                $loadedUser = new PerisianUser();
                
            }
            
            self::$userList[$userId] = $loadedUser;
            
        }
        
        return self::$userList[$userId];
        
    }
    
    /**
     * Retrieves international shortcut names for the days of the week.
     * 
     * @author Peter Hamm
     * @param int index Starts at 0.
     * @return String
     */
    public static function getDayNameShortInternational($index)
    {
                
        $shortcuts = Array(
            
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
            'Sun'
            
        );
        
        return @$shortcuts[$index];
        
    }

    /**
     * Retrieves localized shortcut names for the days of the week.
     * 
     * @author Peter Hamm
     * @param int index Starts at 0.
     * @return String
     */
    public static function getDayNameShort($index)
    {
                
        $dayName = self::getDayName($index);
        
        return substr($dayName, 0, 2);
        
    }
    
    /**
     * Retrieves the localized name for the specified month index.
     * 
     * @param int $index The index of the desired month, starting at 0.
     * @return string
     */
    public static function getMonthName($index)
    {
        
        $index = $index < 0 ? 0 : $index;
        $index = $index > 6 ? 6 : $index;
        
        return PerisianFrameworkToolbox::getMonthName($index);
        
    }

    /**
     * Retrieves localized shortcut names for the months.
     * 
     * @author Peter Hamm
     * @param int index Starts at 0.
     * @return String
     */
    public static function getMonthNameShort($index)
    {
                
        $monthName = self::getMonthName($index);
        
        return substr($monthName, 0, 3);
        
    }
    
    /**
     * Retrieves localized names for the days of the week.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getDayNames()
    {
        
        if(self::$dayNames == null)
        {
            
            self::$dayNames = array(
                
                PerisianLanguageVariable::getVariable(10354),
                PerisianLanguageVariable::getVariable(10355),
                PerisianLanguageVariable::getVariable(10356),
                PerisianLanguageVariable::getVariable(10357),
                PerisianLanguageVariable::getVariable(10358),
                PerisianLanguageVariable::getVariable(10359),
                PerisianLanguageVariable::getVariable(10353),
                
            );
            
        }
        
        return self::$dayNames;
        
    }
    
    /**
     * Retrieves the singleton instance of this object
     * 
     * @author Peter Hamm
     * @date 2016-01-14
     * @return CalsyCalendar
     */
    protected static function getInstance()
    {
        
        if(!isset(self::$instance))
        {
            
            self::$instance = new static();
            
        }
        
        return self::$instance;
        
    }
    
    /**
     * Retrieves the singleton instance of the calendar entry object
     * 
     * @author Peter Hamm
     * @date 2016-01-20
     * @return CalsyCalendar
     */
    protected static function getEntryInstance()
    {
        
        if(!isset(self::$entryInstance))
        {
            
            self::$entryInstance = new CalsyCalendarEntry();
            
        }
        
        return self::$entryInstance;
        
    }
    
    /**
     * Tidies up the $displayType string to an allowed value.
     * 
     * @author Peter Hamm
     * @param String $displayType
     * @return string A string in self::$allowedDisplayTypes. If the $displayType cannot be associated with any of them, "month" is returned by default
     */
    protected static function getProperDisplayType($displayType)
    {
        
        $displayType = strtolower($displayType);
        
        for($i = 0; $i < self::$allowedDisplayTypes; ++$i)
        {
            
            if($displayType == self::$allowedDisplayTypes[$i])
            {
                
                return $displayType;
                
            }
            
        }
        
        return "month";
        
    }
            
    /**
     * Retrieves calendar entries by the parameters specified.
     * 
     * @author Peter Hamm
     * @param String $displayType Either "year", "month", "week" or "day". Default: "month"
     * @param int $startDate A timestamp for the calendar, within the specified $displayType range (e.g. a timestamp in today or this month). Default: -1 (today, now).
     * @param CalsyCalendarFilter $filter The criteria to filter by. Optional, default: null
     * @return Array An associative array with general calendar information and calendar entries.
     */
    public static function getCalendarByType($displayType = "month", $startDate = -1, $filter = null)
    {
        
        if(is_null($filter))
        {
            
            $filter = new CalsyCalendarFilter();
            
        }
        
        $displayType = CalsyCalendar::getProperDisplayType($displayType);
        
        $startDate = CalsyCalendar::getFirstDayOfTypeAsTimestamp($displayType, $startDate > 0 ? $startDate : time());
        $endDate = 0;
                
        $entries = array();
        
        if($displayType == "year")
        {
        
            $entries = self::getEntriesForYear($startDate, $filter);
            
            $endDate = PerisianTimeZone::getLastSecondOfYearAsTimestamp($startDate);
            
        }
        else if($displayType == "month")
        {
        
            $entries = self::getEntriesForMonth($startDate, $filter);
            
            $endDate = PerisianTimeZone::getLastSecondOfMonthAsTimestamp($startDate);
            
        }
        else if($displayType == "week")
        {
        
            $entries = self::getEntriesForWeek($startDate, $filter);
            
            $endDate = PerisianTimeZone::getLastSecondOfWeekAsTimestamp($startDate);
                        
        }
        else if($displayType == "day")
        {
        
            $entries = self::getEntriesForDay($startDate, $filter);
            
            $endDate = PerisianTimeZone::getLastSecondOfDayAsTimestamp($startDate);
            
        }
        
        $matrix = self::getMatrix($displayType, $startDate, $entries, $filter);
        
        $returnInfo = Array(
            
            "type" => $displayType,
            "begin" => $startDate,
            "end" => $endDate,
            "filter" => $filter,
            "entries" => $entries,
            "matrix" => $matrix
            
        );
        
        return $returnInfo;
        
    }
    
    /**
     * Retrieves a matrix of days and calendar entries that can easily be displayed
     * 
     * @author Pater Hamm
     * @param String $displayType
     * @param int $startDate
     * @param Array $entries
     * @param CalsyCalendarfilter $filter Filter criteria
     * @return array
     */
    protected static function getMatrix($displayType, $startDate, $entries, $filter)
    {
        
        $matrix = array();
                
        $displayType = CalsyCalendar::getProperDisplayType($displayType);
        
        if($displayType == "year")
        {
            
            static::getMatrixForYear($matrix, $startDate, $entries, $filter->getShowNoDetail());
            
        }
        else if($displayType == "month")
        {
            
            static::getMatrixForMonth($matrix, $startDate, $entries);
            
        }
        else if($displayType == "week")
        {
            
            static::getMatrixForWeek($matrix, $startDate, $entries);
             
        }
        else if($displayType == "day")
        {
            
            static::getMatrixForDay($matrix, $startDate, $entries);
            
        }
        
        if($displayType == "year" || $displayType == "month")
        {

            static::enrichMatrixWithEntriesForOtherMonths($matrix, $filter);

        }
        
        return $matrix;
        
    }
    
    /**
     * Enriches the the specified matrix with entry data for the days that have the flag "isOtherMonth" set to true.
     * 
     * @author Peter Hamm
     * @param Array $matrix (By reference)
     * @param Array $filter A filter generated by static::createFilter
     * @return Array The matrix.
     */
    protected static function enrichMatrixWithEntriesForOtherMonths(&$matrix, $filter)
    {
        
        $parentSource = isset($matrix["weeks"]) ? "weeks" : "";
        $parentSource = isset($matrix["months"]) ? "months" : $parentSource;
        
        $noDetails = $filter->getShowNoDetail();
        
        for($i = 0; $i < count($matrix[$parentSource]); ++$i)
        {
            
            for($j = 0; $j < count($matrix[$parentSource][$i]); ++$j)
            {
                
                if(isset($matrix[$parentSource][$i][$j]["isOtherMonth"]) && $matrix[$parentSource][$i][$j]["isOtherMonth"])
                {
                    
                    if(count($matrix[$parentSource][$i][$j]["entries"] == 0))
                    {
                        
                        $entries = static::getEntriesForDay($matrix[$parentSource][$i][$j]["time"], $filter);
                        
                        $formattedEntries = static::formatEntriesForMatrixDay($entries, $matrix[$parentSource][$i][$j]["time"], $noDetails);
                        
                        $matrix[$parentSource][$i][$j]["entries"] = $formattedEntries;
                        
                    }

                }
                
            }
            
        }
        
        return $matrix;
        
    }
    
    /**
     * Should be used for example when the module CalsyBookableTimesModule is enabled,
     * it will enrich the data retrieved by static::getCalendarByType() with entries for bookable
     * times for the specified frontend user.
     * 
     * @author Peter Hamm
     * @param Array $calendarData The result of static::getCalendarByType()
     * @param bool $mergeBookableTimes Optional, whether to merge overlapping bookable times into a single entry or not.
     * @return Array
     */
    public static function enrichCalendarMatrixWithBookableTimes($calendarData, $mergeBookableTimes = false)
    {
        
        $enrichedResult = $calendarData;
        
        if($calendarData['type'] == 'year')
        {
                    
            for($i = 0; $i < count($enrichedResult['matrix']['months']); ++$i)
            {
                
                for($j = 0; $j < count($enrichedResult['matrix']['months'][$i]); ++$j)
                {
                              
                    CalsyBookableTimesModule::enrichCalendarMatrixDayWithBookableTimes($enrichedResult['matrix']['months'][$i][$j], false, $mergeBookableTimes);
                    
                }

            }
        
        }
        else if($calendarData['type'] == 'month')
        {
        
            for($i = 0; $i < count($enrichedResult['matrix']['weeks']); ++$i)
            {
                
                for($j = 0; $j < count($enrichedResult['matrix']['weeks'][$i]); ++$j)
                {
                              
                    CalsyBookableTimesModule::enrichCalendarMatrixDayWithBookableTimes($enrichedResult['matrix']['weeks'][$i][$j], true, $mergeBookableTimes);
                    
                }

            }
        
        }
        else if($calendarData['type'] == 'week')
        {
                    
            for($i = 0; $i < count($enrichedResult['matrix']['days']); ++$i)
            {
                       
                CalsyBookableTimesModule::enrichCalendarMatrixDayWithBookableTimes($enrichedResult['matrix']['days'][$i], true, $mergeBookableTimes);
                
            }
        
        }
        else if($calendarData['type'] == 'day')
        {
            
            CalsyBookableTimesModule::enrichCalendarMatrixDayWithBookableTimes($enrichedResult['matrix'], true, $mergeBookableTimes);
        
        }
        
        return $enrichedResult;
        
    }
        
    /**
     * Generates a date matrix for the specified day and entries.
     * 
     * @author Peter Hamm
     * @param array $matrix
     * @param int $startDate
     * @param array $entries
     * @return array
     */
    public static function getMatrixForDay(&$matrix, $startDate, $entries)
    {
        
        $displayedDayCount = 7;

        $entryTimestamp = $startDate;

        $isToday = date("Ymd", $entryTimestamp) == date("Ymd", time());
        $isPast = date("Ymd", $entryTimestamp) < date("Ymd", time());
        $isFuture = date("Ymd", $entryTimestamp) > date("Ymd", time());
        
        $entry = array(

            "time" => $entryTimestamp,
            "date" => date("d.m.Y", $entryTimestamp),
            "day" => strtolower(date("D", $entryTimestamp)),
            "isPast" => $isPast,
            "isToday" => $isToday,
            "isFuture" => $isFuture,
            "isBookable" => self::isDayBookable($entryTimestamp),
            "entries" => self::formatEntriesForMatrixDay($entries, $entryTimestamp)

        );
        
        $matrix = $entry;
        
        return $matrix;
        
    }
    
    /**
     * Generates a date matrix for the specified week and entries.
     * 
     * @author Peter Hamm
     * @param array $matrix
     * @param int $startDate
     * @param array $entries
     * @return array
     */
    public static function getMatrixForWeek(&$matrix, $startDate, $entries)
    {
        
        $displayedDayCount = 7;
        
        $matrix["days"] = array();

        for($i = 0; $i < $displayedDayCount; ++$i)
        {

            $entryTimestamp = $startDate + $i * 86400;

            $isToday = date("Ymd", $entryTimestamp) == date("Ymd", time());
            $isPast = date("Ymd", $entryTimestamp) < date("Ymd", time());
            $isFuture = date("Ymd", $entryTimestamp) > date("Ymd", time());

            $entry = array(

                "time" => $entryTimestamp,
                "date" => date("d.m.Y", $entryTimestamp),
                "day" => strtolower(date("D", $entryTimestamp)),
                "isPast" => $isPast,
                "isToday" => $isToday,
                "isFuture" => $isFuture,
                "isBookable" => self::isDayBookable($entryTimestamp),
                "entries" => self::formatEntriesForMatrixDay($entries, $entryTimestamp)

            );

            array_push($matrix["days"], $entry);

        }
        
        return $matrix;
        
    }
    
    /**
     * Generates a date matrix for the specified year and entries.
     * 
     * @author Peter Hamm
     * @param array $matrix
     * @param int $startDate
     * @param array $entries
     * @param bool $noDetails Optional. Set this to true to get simplified data with not further details. Useful for large queries, e.g. for a yearly view. Default: false
     * @return array
     */
    public static function getMatrixForYear(&$matrix, $startDate, $entries = null, $noDetails = false)
    {
        
        $matrix = array();
        
        $dayOffset = gmdate("N", $startDate) - 1;
        $lastSecondOfYear = PerisianTimeZone::getLastSecondOfYearAsTimestamp($startDate);
        $yearLength = gmdate("z", $lastSecondOfYear) + 1;
        
        $displayedDayCount = ceil(($yearLength + $dayOffset) / 7) * 7;

        for($i = 0; $i < $displayedDayCount; ++$i)
        {

            $entryTimestamp = $startDate;

            if($i < $dayOffset)
            {

                $entryTimestamp -= ($dayOffset - $i) * 86400;

            }
            else 
            {

                $entryTimestamp += ($i - $dayOffset) * 86400;

            }

            $isToday = date("Ymd", $entryTimestamp) == date("Ymd", time());
            $isPast = date("Ymd", $entryTimestamp) < date("Ymd", time());
            $isFuture = date("Ymd", $entryTimestamp) > date("Ymd", time());
            
            $entry = array(

                "time" => $entryTimestamp,
                "date" => date("d.m.Y", $entryTimestamp),
                "day" => strtolower(date("D", $entryTimestamp)),
                "isOtherMonth" => $i < $dayOffset,
                "isOtherYear" => date("Y", $entryTimestamp) != date("Y", $startDate),
                "isPast" => $isPast,
                "isToday" => $isToday,
                "isFuture" => $isFuture,
                "isBookable" => self::isDayBookable($entryTimestamp),
                "entries" => self::formatEntriesForMatrixDay($entries, $entryTimestamp, $noDetails)

            );

            array_push($matrix, $entry);

        }
             
        // Sorting it into months
        {
            
            $month = 0;
            $lastDate = PerisianTimeZone::getFirstDayOfYearAsTimestamp($startDate);

            $formattedMatrix = array("months" => array());

            // Format it so that it can be properly displayed as months. e.g. insert days to fill up the weeks.
            for($i = 0; $i < count($matrix); ++$i)
            {

                if(!isset($formattedMatrix["months"][$month]))
                {

                    $formattedMatrix["months"][$month] = array();

                }

                $entry = $matrix[$i];

                $weekNumber = floor($i / 7);

                $isLastYear = date("Y", $entry["time"]) < date("Y", $startDate);
                $isNextYear = date("Y", $entry["time"]) > date("Y", $startDate);

                if($isLastYear || $isNextYear)
                {

                    array_push($formattedMatrix["months"][$month], $entry);

                    continue;

                }

                $month = date("m", $entry["time"]) - 1;

                if(!isset($formattedMatrix["months"][$month]))
                {

                    $formattedMatrix["months"][$month] = array();

                }

                array_push($formattedMatrix["months"][$month], $entry);

            }
                        
        }
                
        // Fill in some days at the beginning and end of the months.
        {
            
            for($i = 0; $i < count($formattedMatrix["months"]); ++$i)
            {
                
                $firstDayTime = $formattedMatrix["months"][$i][0]["time"];
                
                $dayOffset = date("N", $firstDayTime) - 1;
                
                $timestampThisMonth = $formattedMatrix["months"][$i][$dayOffset]["time"];
                
                // Days in the beginning
                for($j = 0; $j < $dayOffset; ++$j)
                {
                    
                    $dummyEntryTimestamp = $firstDayTime - ($j + 1) * 86400;
                    
                    $dummyEntry = self::getDummyMatrixDay($dummyEntryTimestamp, $timestampThisMonth);
                    
                    array_unshift($formattedMatrix["months"][$i], $dummyEntry);
                    
                }
                
                // Days at the end
                for($j = count($formattedMatrix["months"][$i]); $j < 42; ++$j)
                {

                    $dummyEntryTimestamp = $formattedMatrix["months"][$i][count($formattedMatrix["months"][$i]) - 1]["time"] + 86400;

                    $dummyEntry = self::getDummyMatrixDay($dummyEntryTimestamp, $timestampThisMonth);

                    array_push($formattedMatrix["months"][$i], $dummyEntry);

                }
                
            }
            
        }
        
        $matrix = $formattedMatrix;
        
        return $formattedMatrix;
        
    }
    
    /**
     * Generates a dummy matrix entry.
     * 
     * @param The date for the dummy entry $dummyEntryDate
     * @param type $startDate
     */
    protected static function getDummyMatrixDay($dummyEntryTimestamp, $startDate)
    {
        
        $isToday = date("Ymd", $dummyEntryTimestamp) == date("Ymd", time());
        $isPast = date("Ymd", $dummyEntryTimestamp) < date("Ymd", time());
        $isFuture = date("Ymd", $dummyEntryTimestamp) > date("Ymd", time());

        $entry = array(

            "time" => $dummyEntryTimestamp,
            "date" => date("d.m.Y", $dummyEntryTimestamp),
            "day" => strtolower(date("D", $dummyEntryTimestamp)),
            "isOtherMonth" => date("Ym", $dummyEntryTimestamp) != date("Ym", $startDate),
            "isOtherYear" => date("Y", $dummyEntryTimestamp) != date("Y", $startDate),
            "isPast" => $isPast,
            "isToday" => $isToday,
            "isFuture" => $isFuture,
            "isBookable" => self::isDayBookable($dummyEntryTimestamp),
            "entries" => array()

        );
        
        return $entry;
        
    }
    
    /**
     * Generates a date matrix for the specified month and entries.
     * 
     * @author Peter Hamm
     * @param array $matrix
     * @param int $startDate
     * @param array $entries Optional
     * @return array
     */
    public static function getMatrixForMonth(&$matrix, $startDate, $entries = null)
    {
        
        $matrix["weeks"] = array();
        
        $dayOffset = date("N", $startDate) - 1;
        $monthLength = date("t", $startDate);
        $displayedDayCount = ceil(($monthLength + $dayOffset) / 7) * 7;
        
        for($i = 0; $i < $displayedDayCount; ++$i)
        {

            $entryTimestamp = $startDate;
            
            if($i < $dayOffset)
            {

                $entryTimestamp -= ($dayOffset - $i) * 86400;

            }
            else 
            {

                $entryTimestamp += ($i - $dayOffset) * 86400;

            }
            
            $entryTimestamp += !PerisianTimeZone::isTimestampInDST($entryTimestamp) ? 3600 : 0;
            
            $weekNumber = floor($i / 7);

            if(!isset($matrix["weeks"][$weekNumber]))
            {

                $matrix["weeks"][$weekNumber] = array();

            }

            $isToday = date("Ymd", $entryTimestamp) == date("Ymd", time());
            $isPast = date("Ymd", $entryTimestamp) < date("Ymd", time());
            $isFuture = date("Ymd", $entryTimestamp) > date("Ymd", time());

            $entry = array(

                "time" => $entryTimestamp,
                "date" => date("d.m.Y", $entryTimestamp),
                "day" => strtolower(date("D", $entryTimestamp)),
                "isOtherMonth" => ($i < $dayOffset || $i >= ($monthLength + $dayOffset)),
                "isPast" => $isPast,
                "isToday" => $isToday,
                "isFuture" => $isFuture,
                "isBookable" => self::isDayBookable($entryTimestamp),
                "entries" => self::formatEntriesForMatrixDay($entries, $entryTimestamp)

            );

            array_push($matrix["weeks"][$weekNumber], $entry);

        }
        
        return $matrix;
        
    }
    
    /**
     * Retrieves all the entries for the year the specified timestamp lies in.
     * 
     * @author Peter Hamm
     * @date 2016-01-14
     * @param int $timestampInYear A timestamp in the desired month
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @return Array
     */
    public static function getEntriesForYear($timestampInYear, $filter)
    {
        
        $timestampBegin = PerisianTimeZone::getFirstDayOfYearAsTimestamp($timestampInYear);
        $timestampEnd = PerisianTimeZone::getLastSecondOfYearAsTimestamp($timestampInYear);
        
        $entries = CalsyCalendarEntry::getEntriesBetween($timestampBegin, $timestampEnd, $filter);
        
        return $entries;
        
    }
    
    /**
     * Retrieves all the entries for the month the specified timestamp lies in.
     * 
     * @author Peter Hamm
     * @date 2016-01-14
     * @param int $timestampInMonth A timestamp in the desired month
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @return Array
     */
    public static function getEntriesForMonth($timestampInMonth, $filter)
    {
        
        $timestampBegin = PerisianTimeZone::getFirstDayOfMonthAsTimestamp($timestampInMonth);
        $timestampEnd = PerisianTimeZone::getLastSecondOfMonthAsTimestamp($timestampInMonth);
        
        $entries = CalsyCalendarEntry::getEntriesBetween($timestampBegin, $timestampEnd, $filter);
        
        return $entries;
        
    }
    
    /**
     * Retrieves all the entries for the week the specified timestamp lies in.
     * 
     * @author Peter Hamm
     * @date 2016-01-18
     * @param int $timestampInWeek A timestamp in the desired week
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @return Array
     */
    public static function getEntriesForWeek($timestampInWeek, $filter)
    {
        
        $timestampBegin = PerisianTimeZone::getFirstDayOfWeekAsTimestamp($timestampInWeek);
        $timestampEnd = PerisianTimeZone::getLastSecondOfWeekAsTimestamp($timestampInWeek);
        
        $entries = CalsyCalendarEntry::getEntriesBetween($timestampBegin, $timestampEnd, $filter);
                
        return $entries;
        
    }
    
    /**
     * Retrieves all the entries for the day the specified timestamp lies in.
     * 
     * @author Peter Hamm
     * @date 2016-01-18
     * @param int $timestampOnDay A timestamp at the desired day
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @return Array
     */
    public static function getEntriesForDay($timestampOnDay, $filter)
    {
        
        $day = date("d", $timestampOnDay);
        $month = date("m", $timestampOnDay);
        $year = date("Y", $timestampOnDay);
        
        $timestampBegin = mktime(0, 0, 0, $month, $day, $year);
        $timestampEnd = mktime(23, 59, 59, $month, $day, $year);
        
        $entries = CalsyCalendarEntry::getEntriesBetween($timestampBegin, $timestampEnd, $filter);
        
        return $entries;
        
    }
    
    /**
     * Formats the provided list of entries for the day matrix by enriching it with additional data.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @param array $entries The list of entries
     * @param int $timestampOnDay A timestamp on the day.
     * @param bool $noDetails Optional. Set this to true to get simplified data with not further details. Useful for large queries, e.g. for a yearly view. Default: false
     * @return array
     */
    protected static function formatEntriesForMatrixDay($entries, $timestampOnDay, $noDetails = false)
    {
        
        if(!is_array($entries))
        {
            
            return Array();
            
        }
        
        global $user, $userFrontend;
                
        $result = array();
        $entryObject = static::getEntryInstance();
        $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder();
        
        $day = date("d", $timestampOnDay);
        $month = date("m", $timestampOnDay);
        $year = date("Y", $timestampOnDay);
        
        $timestampBegin = mktime(0, 0, 0, $month, $day, $year);
        $timestampEnd = mktime(23, 59, 59, $month, $day, $year);
        
        for($i = 0; $i < count($entries); ++$i)
        {
            
            $isHoliday = $entries[$i][$entryObject->field_is_holiday] == 1;
            $isBlocked = false;
                        
            $entryBegin = strtotime($entries[$i][$entryObject->field_date_begin]);
            $entryEnd = strtotime($entries[$i][$entryObject->field_date_end]);
            
            $entries[$i]["startsToday"] = $entryBegin >= $timestampBegin && $entryBegin <= $timestampEnd;
            $entries[$i]["endsToday"] = $entryEnd >= $timestampBegin && $entryEnd <= $timestampEnd;
            $entries[$i]["overspansToday"] = $entryBegin < $timestampBegin && $entryEnd > $timestampEnd;
            
            $entries[$i]["wholeDay"] = date("Hi", $entries[$i][$entryObject->timestamp_begin]) == "0000" && date("Hi", $entries[$i][$entryObject->timestamp_end]) == "2359";
                        
            if(!$noDetails)
            {
                
                $entries[$i]["user"] = self::getUser($entries[$i][$entryObject->field_user_id]);

                // Assign (among others) a displayable color to the entry.
                // Depending on if we have either a logged in user or a logged in frontend user, different conditions apply.
                {

                    if($entries[$i][$entryObject->field_is_holiday] != 1 && isset($userFrontend) && !isset($user))
                    {

                        // Check if this user's calendar entry should be displayed as blocked for the frontend user.
                        
                        if(@$entries[$i][$userFrontendOrderObject->field_user_frontend_id] != $userFrontend->{$userFrontend->field_pk})
                        {

                            $entries[$i]["isBlockedForUser"] = true;

                            $entries[$i][$entryObject->field_title] = PerisianLanguageVariable::getVariable(10996);

                            $isBlocked = true;

                        }

                    }
                    
                    if($isHoliday || $isBlocked)
                    {

                        $entries[$i]["displayColor"] = "#e5e6e6";

                    }
                    else if(CalsyCalendarEntryFlag::entryHasFlag($entries[$i][$entryObject->field_pk], CalsyProcessEngineModule::FLAG_PROCESS_TO_START))
                    {
                        
                        if(CalsyProcessengineModule::useColorFromCreator() && isset($entries[$i]["user"]) && ($entries[$i]["user"]->{$entries[$i]["user"]->field_pk} > 0 || $entries[$i]["user"]->{$entries[$i]["user"]->field_pk} == CalsyUserBackend::DEFAULT_USER_ID))
                        {

                            $entries[$i]["displayColor"] = $entries[$i]["user"]->getColor();

                        }
                        else
                        {

                            $entries[$i]["displayColor"] = CalsyProcessEngineModule::getColorDefault();

                        }
                        
                    }
                    else 
                    {

                        if(!isset($userFrontend) && (isset($user) && $user->isLoggedIn()))
                        {
                                   
                            if(isset($entries[$i]["user"]) && ($entries[$i]["user"]->{$entries[$i]["user"]->field_pk} > 0 || $entries[$i]["user"]->{$entries[$i]["user"]->field_pk} == CalsyUserBackend::DEFAULT_USER_ID))
                            {
                                                                
                                $entries[$i]["displayColor"] = CalsyUserBackend::getUserColor($entries[$i]["user"]->{$entries[$i]["user"]->field_pk});
                                
                            }
                            else
                            {
                                
                                $entries[$i]["displayColor"] = CalsyCalendarEntryUserFrontendOrder::getColorForConfirmationStatus('confirmed');
                                
                            }

                        }
                        else if(isset($userFrontend) && $userFrontend->isLoggedIn())
                        {
                            
                            // Set the color depending on the confirmation status

                            $actualEntryId = !empty($entries[$i][$entryObject->field_parent_entry_id]) && $entries[$i][$entryObject->field_parent_entry_id] != '0' ? $entries[$i][$entryObject->field_parent_entry_id] : $entries[$i][$entryObject->field_pk];
                            
                            $userFrontendOrder = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListObjectsForCalendarEntry($actualEntryId, $userFrontend->{$userFrontend->field_pk});

                            $entries[$i]["displayColor"] = CalsyCalendarEntryUserFrontendOrder::getColorForConfirmationStatus($userFrontendOrder->{$userFrontendOrder->field_status_confirmation});


                        }
                        else
                        {

                            $entries[$i]["displayColor"] = CalsyCalendarEntryUserFrontendOrder::getColorForConfirmationStatus();

                        }
                        
                    }

                }
                
            }
            
            $startsToday = ($entryBegin < $timestampEnd && $entryBegin >= $timestampBegin);
            $endsToday = ($entryEnd <= $timestampEnd && $entryEnd > $timestampBegin);
            $spansOverToday = $entryBegin < $timestampBegin && $entryEnd > $timestampEnd;
            
            if($startsToday || $endsToday || $spansOverToday)
            {
                
                // The beginning or ending of the entry is on the specified day:
                // Therefore, add it to the matrix.
                
                array_push($result, $entries[$i]);
                
            }
            
        }
        
        return $result;
        
    }
    
    /**
     * Retrieves the first day of the month of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @date 2016-01-18
     * @param String $displayType The display type, e.g. "year", "month", "week" or "day".
     * @param int $timestamp A timestamp within the desired $displayType
     * @return int The first day within the $displayType, as a timestamp
     */
    public static function getFirstDayOfTypeAsTimestamp($displayType, $timestamp)
    {
        
        $displayType = self::getProperDisplayType($displayType);
        
        $value = "";
        
        if($displayType == "year")
        {
            
            $value = PerisianTimeZone::getFirstDayOfYearAsTimestamp($timestamp);
            
        }
        else if($displayType == "month")
        {
            
            $value = PerisianTimeZone::getFirstDayOfMonthAsTimestamp($timestamp);
            
        }
        if($displayType == "week")
        {
            
            $value = PerisianTimeZone::getFirstDayOfWeekAsTimestamp($timestamp);
            
        }
        if($displayType == "day")
        {
            
            $value = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestamp);
            
        }
        
        return $value;
        
    }  
    
    /**
     * Retrieves a list of users that can be selected to display their calendar entries.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @param array $selectedUserList A list of currently selected user IDs
     * @param array $customUserList Optional, an array of user objects. 
     * @param bool $includeDefaultUser Optional, default: false
     * @return array
     */
    public static function getDisplayableUserList($selectedUserList = null, $customUserList = array(), $includeDefaultUser = false)
    {
        
        global $user;
        
        $list = array();
        
        $userObject = new CalsyUserBackend();
        
        $useCustomList = count($customUserList) > 0;
        $unformattedList = $useCustomList ? $customUserList : (is_object($user) ? CalsyUserBackendGroup::getVisibleUserListForUserId($user->{$user->field_pk}) : Array());
        
        for($i = 0; $i < count($unformattedList); ++$i)
        {
            
            $isSelected = false;
            
            if($selectedUserList != null && count($selectedUserList) > 0)
            {
                
                $isSelected = in_array($useCustomList ? $customUserList[$i]->{$userObject->field_pk} : $unformattedList[$i][$userObject->field_pk], $selectedUserList);
                
            }
            
            $currentUserId = $useCustomList ? $customUserList[$i]->{$userObject->field_pk} : $unformattedList[$i][$userObject->field_pk];
            $currentUser = new CalsyUserBackend($currentUserId);
            
            if($currentUser->isInvisible())
            {
                                
                if(isset($user) && $user->{$user->field_pk} == $currentUserId)
                {
                    
                    // Display it for the current user
                    
                }
                else 
                {
                    
                    continue;

                }
                
            }
            
            $entry = array(
                
                $userObject->field_pk => $currentUserId,
                $userObject->field_fullname => $useCustomList ? $customUserList[$i]->{$userObject->field_fullname} : $unformattedList[$i][$userObject->field_fullname],
                $userObject->field_color => $useCustomList ? $customUserList[$i]->{$userObject->field_color} : $unformattedList[$i][$userObject->field_color],
                $userObject->field_image_profile => CalsyUserBackend::getUserImageProfile($currentUserId),
                "selected" => $isSelected
                
            );
            
            array_push($list, $entry);
            
        }
        
        // Display a "Deleted user" if there are calendar entries for deleted users.
        if($includeDefaultUser)
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setUserBackendIdentifiers(CalsyUserBackend::DEFAULT_USER_ID);
            $filter->setIgnoreConfirmationStatus(true);
            
            if(CalsyCalendarEntry::getCountCalendarEntriesForFilter($filter) > 0)
            {
                
                $deletedUser = new CalsyUserBackend(CalsyUserBackend::DEFAULT_USER_ID);
                
                $deletedUserFields = $deletedUser->getFieldValues();
                
                $deletedUserFields[$userObject->field_image_profile] = CalsyUserBackend::getUserImageProfile(CalsyUserBackend::DEFAULT_USER_ID);
                $deletedUserFields['selected'] = !is_null($selectedUserList) && in_array(CalsyUserBackend::DEFAULT_USER_ID, $selectedUserList);
                
                array_push($list, $deletedUserFields);

            }
            
        }
        
        return $list;
        
    }
    
    /**
     * Retrieves a list of the days that should be displayed in the calendar views.
     * 
     * @author Peter Hamm
     * @param bool $forceReload Optional, should the cache be refreshed and the setting be loaded anew? Default: false
     * @return Array
     */
    public static function getDisplayableDays($forceReload = false)
    {
        
        if(static::$displayableDays == null || $forceReload)
        {
            
            $settingValue = PerisianSystemSetting::getSettingValue('calendar_displayed_days');

            if(strlen($settingValue) < 2)
            {

                $settingValue = "[]";

            }

            static::$displayableDays = json_decode($settingValue, true);
            
        }
        
        return static::$displayableDays;
        
    }
    
    /**
     * Retrieves the list of day indices that are displayable for the specified view.
     * 
     * The different views are: "year", "month", "week" and "day".
     * 
     * @param String $view
     * @return array
     */
    public static function getDisplayableIndices($view)
    {
        
        $list = Array();
        
        $displayableDays = static::getDisplayableDays();
        
        for($i = 0; $i < count($displayableDays); ++$i)
        {
            
            if(isset($displayableDays[$i][$view]) && $displayableDays[$i][$view] == 1)
            {
                
                array_push($list, $i);
                
            }
            
        }
        
        return $list;
        
    }
    
    /**
     * Checks if the specified day should be displayed for the specified view.
     * 
     * Day index starts at 0, for Monday. 1 is Tuesday and so forth.
     * The different views are: "year", "month", "week" and "day".
     * 
     * @param int $dayIndex
     * @param String $view
     * @return bool
     */
    public static function isDayDisplayableForView($dayIndex, $view)
    {
        
        $displayableDays = static::getDisplayableDays();
        
        if(!isset($displayableDays[$dayIndex]))
        {
            
            return false;
            
        }
        
        if($displayableDays[$dayIndex][$view] == 1)
        {
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves how many days per week should be displayed for the specified view.
     * 
     * The different views are: "year", "month", "week" and "day".
     * 
     * @param String $view
     * @return int
     */
    public static function getCountDisplayableWeekDayForView($view)
    {
        
        $count = 0;
        
        $displayableDays = static::getDisplayableDays();
        
        if($view == 'day')
        {
            
            return 1;
            
        }
        
        for($i = 0; $i < count($displayableDays); ++$i)
        {
            
            if(isset($displayableDays[$i][$view]) && $displayableDays[$i][$view] == 1)
            {
                
                ++$count;
                
            }
            
        }
        
        return $count;
        
    }
    
}
