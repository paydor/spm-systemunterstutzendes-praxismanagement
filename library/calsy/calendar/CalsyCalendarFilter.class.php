<?php

/**
 * Used to filter calendar entries.
 * 
 * @author Peter Hamm
 * @date 2018-01-05
 */
class CalsyCalendarFilter
{
    
    private $flags = Array();
    
    private $userBackendIdentifiers = "-1";
    private $userFrontendIdentifiers = "-1";
    private $resourceIdentifiers = "-1";
    private $areaIdentifiers = "-1";
    private $orderIdentifiers = "-1";
    
    private $showHolidays = false;
    private $showSurveys = false;
    private $showUsersAsBlocked = false;
    private $showNoDetails = false;
    private $showUnconfirmed = false;
    private $showCanceled = false;
    
    private $ignoreConfirmationStatus = false;
    private $ignoreTimespanEdges = false;
    
    private $doFilterByAreaIdentifiers = false;
            
    /**
     * Retrieves the list of flags for this filter.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getFlags()
    {
        
        return $this->flags;
        
    }
    
    /**
     * Retrieves the value for the flag with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return mixed Null if the key is not contained, else: The value of the flag.
     */
    public function getFlagValue($key)
    {
        
        foreach($this->flags as $flagKey => $value)
        {
         
            if($flagKey == $key)
            {
                
                return $value;
                
            }
            
        }
        
        return null;
        
    }
    
    /**
     * Retrieves the list of backend user's identifiers as a CSV string,
     * or optionally, as an array.
     * 
     * @author Peter Hamm
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getUserBackendIdentifiers($asArray = false)
    {
                
        return $this->getIdentifiers($this->userBackendIdentifiers, $asArray);
        
    }
    
    /**
     * Retrieves the list of frontend user's identifiers as a CSV string,
     * or optionally, as an array.
     * 
     * @author Peter Hamm
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getUserFrontendIdentifiers($asArray = false)
    {
        
        return $this->getIdentifiers($this->userFrontendIdentifiers, $asArray);
        
    }
    
    /**
     * Retrieves the list of resource identifiers as a CSV string,
     * or optionally, as an array.
     * 
     * @author Peter Hamm
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getResourceIdentifiers($asArray = false)
    {
        
        return $this->getIdentifiers($this->resourceIdentifiers, $asArray);
        
    }
    
    /**
     * Sets whether the result should be filterd by area identifiers or not.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setDoFilterByAreaIdentifiers($value)
    {
        
        $this->doFilterByAreaIdentifiers = $value;
        
    }
    
    /**
     * Retrieves whether the result should be filterd by area identifiers or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public function getDoFilterByAreaIdentifiers()
    {
        
        return $this->doFilterByAreaIdentifiers;
        
    }
    
    /**
     * Retrieves the list of area identifiers as a CSV string,
     * or optionally, as an array.
     * 
     * @author Peter Hamm
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getAreaIdentifiers($asArray = false)
    {
        
        return $this->getIdentifiers($this->areaIdentifiers, $asArray);
        
    }
    
    /**
     * Retrieves the list of order identifiers as a CSV string,
     * or optionally, as an array.
     * 
     * @author Peter Hamm
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getOrderIdentifiers($asArray = false)
    {
        
        return $this->getIdentifiers($this->orderIdentifiers, $asArray);
        
    }
    
    /**
     * Retrieves the list of identifiers as a CSV string,
     * or optionally, as an array, from the specified variable.
     * 
     * @author Peter Hamm
     * @param mixed $variable
     * @param bool $asArray
     * @return mixed CSV string or Array
     */
    public function getIdentifiers($variable, $asArray = false)
    {
        
        if($asArray)
        {
            
            if(!is_array($variable))
            {
                
                return Array($variable);
                
            }
            
            return $variable;
            
        }
        
        $returnValue = $variable; 
        
        if(is_array($returnValue))
        {
            
            $returnValue = implode(",", $returnValue);
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves whether this filter is configured to show holidays or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowHolidays()
    {
        
        return $this->showHolidays;
        
    }
    
    /**
     * Retrieves whether this filter is configured to show surveys or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowSurveys()
    {
        
        return $this->showSurveys;
        
    }
    
    /**
     * Retrieves whether to true to select all of the specified user's appointments 
     * as blocked (except for the frontend user, when specified)
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowUsersAsBlocked()
    {
        
        return $this->showUsersAsBlocked;
        
    }
    
    /**
     * Retrieves whether to get simplified data with no further details.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowNoDetail()
    {
        
        return $this->showUsersAsBlocked;
        
    }
    
    /**
     * Retrieves whether to exclusively(!) show unconfirmed entries.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowUnconfirmed()
    {
        
        return $this->showUnconfirmed;
        
    }
    
    /**
     * Retrieves whether to exclusively(!) show canceled entries.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getShowCanceled()
    {
        
        return $this->showCanceled;
        
    }
    
    /**
     * Retrieves whether to whether ignore the confirmation status of entries or not.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getIgnoreConfirmationStatus()
    {
        
        return $this->ignoreConfirmationStatus;
        
    }
    
    /**
     * Retrieves whether to true to only consider calendar entries with times "bigger" and "smaller" 
     * instead of "bigger or equal" and "smaller or equal". 
     * The default value is false.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getIgnoreTimespanEdges()
    {
        
        return $this->ignoreTimespanEdges;
        
    }
    
    /**
     * Checks if a flag with the specified value and operator is set for this filter.
     * 
     * @author Peter Hamm
     * @param String $flagKey The key of the flag to filter by.
     * @param String $flagValue The value for the flag with the specified key to filter by.
     * @param String $operator Optional, default: '='. Valid operators are '=' and '!='.
     * @return bool
     */
    public function hasFlag($flagKey, $flagValue, $operator = '=')
    {
        
        for($i = 0; $i < count($this->flags); ++$i)
        {
            
            if($this->flags[$i]['key'] == $flagKey && $this->flags[$i]['value'] == $flagValue && $this->flags[$i]['operator'] == $operator)
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    }
        
    /**
     * Adds a calendar entry flag as a filter criterion.
     * 
     * @author Peter Hamm
     * @param String $flagKey The key of the flag to filter by.
     * @param String $flagValue The value for the flag with the specified key to filter by.
     * @param String $operator Optional, default: '='. Valid operators are '=' and '!='.
     * @parameter String $logic Optional, default: 'and'. How to chain this flag to the others. Valid logical operators are 'and' and 'or'.
     * @return void
     */
    public function addFlag($flagKey, $flagValue, $operator = '=', $logic = 'and')
    {
        
        if(strlen($logic) == 0)
        {
            
            $logic = 'and';
            
        }
        
        if(strlen($flagKey) < 1)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4'));
            
        }
        
        if($operator != '=' && $operator != '!=')
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4'), $operator);
            
        }
        
        if($logic != 'and' && $logic != 'or')
        {
                                    
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4'), $logic);
            
        }
        
        $filterFlag = Array(
            
            'key' => $flagKey,
            'value' => $flagValue,
            'operator' => $operator,
            'logic' => $logic
            
        );
        
        array_push($this->flags, $filterFlag);
        
    }
    
    /**
     * Sets whether to show holidays or not.
     * Set this to true to display holidays as appointments.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowHolidays($doShow)
    {
        
        $this->showHolidays = $doShow == true;
        
    }
    
    /**
     * Sets whether to show surveys or not.
     * Set this to true to display surveys as appointments.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowSurveys($doShow)
    {
        
        $this->showSurveys = $doShow == true;
        
    }
    
    /**
     * Set this to true to select all of the specified user's appointments as blocked (except for the frontend user, when specified)
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowUsersAsBlocked($doShow)
    {
        
        $this->showUsersAsBlocked = $doShow == true;
        
    }
    
    /**
     * Set this to true to get simplified data with no further details. 
     * Useful for large queries, e.g. for a yearly view.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowNoDetail($doShow)
    {
        
        $this->showNoDetails = $doShow == true;
        
    }
        
    /**
     * Set this to exclusively(!) show unconfirmed entries.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowUnconfirmed($doShow)
    {
        
        $this->showUnconfirmed = $doShow == true;
        
    }
    
    /**
     * Set this to exclusively(!) show canceled entries.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doShow
     * @return void
     */
    public function setShowCanceled($doShow)
    {
        
        $this->showCanceled = $doShow == true;
        
    }
    
    /**
     * Set this to whether ignore the confirmation status of entries or not.
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doIgnore
     * @return void
     */
    public function setIgnoreConfirmationStatus($doIgnore)
    {
        
        $this->ignoreConfirmationStatus = $doIgnore == true;
        
    }
    
    /**
     * Set this to true to only consider calendar entries with times "bigger" and "smaller" 
     * instead of "bigger or equal" and "smaller or equal". 
     * The default value is false.
     * 
     * @author Peter Hamm
     * @param bool $doIgnore
     * @return void
     */
    public function setIgnoreTimespanEdges($doIgnore)
    {
        
        $this->ignoreTimespanEdges = $doIgnore == true;
        
    }
    
    /**
     * Sets the identifiers for the backend users that should be filtered by.
     * The value may be a single identifier or an array of it,
     * it may also be "-1", indicating that this criterion should be ignored for filtering (It's the default value).
     * 
     * @author Peter Hamm
     * @param mixed $userBackendIdentifiers May be "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     */
    public function setUserBackendIdentifiers($userBackendIdentifiers)
    {
        
        $this->setIdentifiers($this->userBackendIdentifiers, $userBackendIdentifiers);
        
    }
    
    /**
     * Sets the identifiers for the frontend users that should be filtered by.
     * The value may be a single identifier or an array of it,
     * it may also be "-1", indicating that this criterion should be ignored for filtering (It's the default value).
     * 
     * @author Peter Hamm
     * @param mixed $userFrontendIdentifiers May be "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     */
    public function setUserFrontendIdentifiers($userFrontendIdentifiers)
    {
        
        $this->setIdentifiers($this->userFrontendIdentifiers, $userFrontendIdentifiers);
        
    }
    
    /**
     * Sets the identifiers for the resources that should be filtered by.
     * The value may be a single identifier or an array of it,
     * it may also be "-1", indicating that this criterion should be ignored for filtering (It's the default value).
     * 
     * @author Peter Hamm
     * @param mixed $resourceIdentifiers May be "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     */
    public function setResourceIdentifiers($resourceIdentifiers)
    {
        
        $this->setIdentifiers($this->resourceIdentifiers, $resourceIdentifiers);
        
    }
    
    /**
     * Sets the identifiers for the areas that should be filtered by.
     * The value may be a single identifier or an array of it,
     * it may also be "-1", indicating that this criterion should be ignored for filtering (It's the default value).
     * 
     * @author Peter Hamm
     * @param mixed $areaIdentifiers May be "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     */
    public function setAreaIdentifiers($areaIdentifiers)
    {
        
        $this->setIdentifiers($this->areaIdentifiers, $areaIdentifiers);
        
    }
    
    /**
     * Sets the identifiers for the orders that should be filtered by.
     * The value may be a single identifier or an array of it,
     * it may also be "-1", indicating that this criterion should be ignored for filtering (It's the default value).
     * 
     * @author Peter Hamm
     * @param mixed $orderIdentifiers May be "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     */
    public function setOrderIdentifiers($orderIdentifiers)
    {
        
        $this->setIdentifiers($this->orderIdentifiers, $orderIdentifiers);
        
    }
    
    /**
     * Takes a variable as a reference and tries to store the specified identifier values to it.
     * 
     * @author Peter Hamm
     * @param mixed $referenceVariable The variable to store the identifiers to.
     * @param mixed $orderIdentifiers May be special key values like "-1" / null, a single identifier, a CSV string of identifiers, or an array of identifiers.
     * @return void
     * @throws PerisianException
     */
    private function setIdentifiers(&$referenceVariable, $identifiers)
    {
        
        if(is_null($identifiers))
        {
            
            $identifiers = '-1';
            
        }
        
        if(!is_array($identifiers) && strpos($identifiers, ",") !== false)
        {
            
            $identifiers = explode(",", $identifiers);
            
        }
        
        if(!is_array($identifiers))
        {
            
            if(strtoupper($identifiers) == "NULL")
            {
                
                $identifiers = "NULL";
                
            }
            else if((int)$identifiers < 0)
            {
                
                $possibleSpecialValues = Array(
                    
                    CalsyUserBackend::DEFAULT_USER_ID,
                    CalsyUserFrontend::DEFAULT_USER_ID
                    
                );
                
                $referenceVariable = in_array($identifiers, $possibleSpecialValues) ? $identifiers : '-1';
                
                return;
                
            }
            
        }
        
        if(!is_array($identifiers))
        {
            
            $identifiers = Array($identifiers);
            
        }
        
        if((count($identifiers) == 1 && (int)$identifiers[0] < 0) && $identifiers != CalsyUserBackend::DEFAULT_USER_ID)
        {
            
            $referenceVariable = $identifiers[0] == CalsyUserBackend::DEFAULT_USER_ID ? CalsyUserBackend::DEFAULT_USER_ID : '-1';
            
            return;
            
        }
        
        for($i = 0; $i < count($identifiers); ++$i)
        {
            
            if((int)$identifiers[$i] < 0 && $identifiers[$i] != CalsyUserBackend::DEFAULT_USER_ID)
            {
                
                //throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4'), $identifiers);
                
            }
            
        }
        
        $referenceVariable = $identifiers;
        
    }
    
}