<?php

/**
 * Calendar entry group
 *
 * @author Peter Hamm
 * @date 2016-01-29
 */
class CalsyCalendarEntryGroup extends PerisianDatabaseModel
{
    
    // Instance
    private static $instance = null;

    // Main table settings
    public $table = 'calsy_calendar_entry_group';
    public $field_pk = 'calsy_calendar_entry_group_id';
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Deletes the group with the specified id if there's no more entries contained in it.
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return void
     */
    public static function deleteGroupIfEmpty($groupId)
    {
        
        PerisianFrameworkToolbox::security($groupId);
        
        if(strlen($groupId) > 0 && (int)$groupId > 0)
        {
            
            $instance = self::getInstance();
            
            $count = CalsyCalendarEntry::getCountForGroupId($groupId);
            
            if($count == 0)
            {
                
                $query = "{$instance->field_pk} = '{$groupId}'";
             
                $instance->delete($query, "", "", 0, 1);
                
            }
            
        }
        
    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static private function getInstance()
    {

        if(!isset(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;

    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
}
