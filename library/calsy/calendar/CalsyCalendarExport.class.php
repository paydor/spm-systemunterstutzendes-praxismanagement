<?php

require_once 'CalsyCalendarEntry.class.php';

require_once 'external/ICS/Exception/CalendarException.php';
require_once 'external/ICS/Exception/CalendarEventException.php';
require_once 'external/ICS/Model/Calendar.php';
require_once 'external/ICS/Model/CalendarEvent.php';
require_once 'external/ICS/Model/Relationship/Attendee.php';
require_once 'external/ICS/Model/Relationship/Organizer.php';
require_once 'external/ICS/Utility/Formatter.php';
require_once 'external/ICS/Utility/Provider.php';
require_once 'external/ICS/CalendarStream.php';
require_once 'external/ICS/CalendarExport.php';
require_once 'external/ICS/Constants.php';

class CalsyCalendarExport
{
    
    private $calendarEntry = null;
    private $calendarExport = null;
    private $exportFilePath = "";
    
    private $requiredUserFrontendId = 0;
    
    public function __construct($calendarEntryId)
    {
        
        $calendarEntryId = CalsyCalendarEntry::getActualEntryIdentifier($calendarEntryId);
        
        $this->calendarEntry = new CalsyCalendarEntry($calendarEntryId);
        
        $this->calendarEntry->updateDurationFromChildren();
        
        // Set up the export
        {
                            
            $this->calendarExport = new Jsvrcek\ICS\Model\Calendar();
            
            $this->calendarExport->setProdId('-//ConsSys IT AG//calSy//EN');
            $this->calendarExport->setTimezone(PerisianTimeZone::getTimeZone());
            
        }
                
    }
    
    /**
     * Handles the data input and prepares it to get exported.
     * This will automatically consider any set filter, e.g. a frontend user identifier.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function prepareData()
    {
                
        $entryList = Array($this->calendarEntry);
        $children = $this->calendarEntry->getChildren();
        
        $entryList = array_merge($entryList, $children);
        
        for($i = 0; $i < count($entryList); ++$i)
        {
            
            // Generate the event entry
            {
            
                $begin = new DateTime($entryList[$i]->{$entryList[$i]->field_date_begin});
                $end = new DateTime($entryList[$i]->{$entryList[$i]->field_date_end});

                $entryEvent = $this->getEntryEvent($begin, $end, $entryList[$i]->{$entryList[$i]->field_title}, $entryList[$i]->{$entryList[$i]->field_pk});
            
            }
            
            // Add the owner
            {
                
                try 
                {
                                        
                    $ownerUser = new CalsyUserBackend($entryList[$i]->{$entryList[$i]->field_user_id});        
                                        
                    $ownerSetting = $ownerUser->getSetting('user_language');
                            
                    $language = PerisianLanguage::getLanguageCode($ownerUser->getSetting('user_language'));
                    $this->addOrganizer($entryEvent, $ownerUser->{$ownerUser->field_fullname}, "", $language);
                    
                } 
                catch(PerisianException $e) 
                {
                    
                    // Do nothing
                    
                }
            
            }
            
            // Add the participant(s)
            {
   
                $participantUserList = $entryList[$i]->getUserFrontendWithOrders();
                                
                $countParticipants = 0;
                
                for($j = 0; $j < count($participantUserList); ++$j)
                {

                    try 
                    {
                        
                        $currentUserFrontendId = $participantUserList[$j]['object']->{$participantUserList[$j]['object']->field_user_frontend_id};
                        
                        if($this->requiredUserFrontendId > 0)
                        {
                            
                            // Check if this frontend user should be included
                            
                            if($this->requiredUserFrontendId != $currentUserFrontendId)
                            {
                                
                                continue;
                                
                            }
                            
                        }

                        $participantUser = new CalsyUserFrontend($currentUserFrontendId);
                                        
                        $language = PerisianLanguage::getLanguageCode($participantUser->getSettingValue('user_frontend_language'));
                        
                        $this->addAttendee($entryEvent, $participantUser->getFullName(), $participantUser->getContactMailAddress(), $language);
                        
                        ++$countParticipants;

                    } 
                    catch(PerisianException $e) 
                    {

                        // Do nothing
                        
                        continue;

                    }
                    
                }
            
            }
            
            if($this->requiredUserFrontendId > 0 && $countParticipants == 0)
            {
                
                // "The specified frontend user is not a participant of this calendar entry!".
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('p59d241b38976b'));
                
            }
            
            $this->calendarExport->addEvent($entryEvent);
            
        }
                
    }
    
    /**
     * Sets the required frontend user identifier to the specified value.
     * When set, only the parts of the calendar entry concerning that frontend user
     * will be considered for the export, it acts as a filter.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @throws PerisianException
     * @return void
     */
    public function setRequiredUserFrontendId($userFrontendId)
    {
        
        if($userFrontendId <= 0)
        {
            
            // "Invalid ID"
            
            throw new PerisianException(PerisianLanguageVariable::getVariable("p57ab3ac859102"));
            
        }
        
        $this->requiredUserFrontendId = $userFrontendId;
        
    }
        
    /**
     * Creates a new event for a calendar entry to add to the export.
     * 
     * @author Peter Hamm
     * @param DateTime $begin
     * @param DateTime $end
     * @param String $summary
     * @param mixed $eventUid A custom unique identifer, optional. Default: Empty string
     * @return \Jsvrcek\ICS\Model\CalendarEvent
     */
    protected function getEntryEvent(DateTime $begin, DateTime $end, $summary, $eventUid = "")
    {
        
        $entryEvent = new Jsvrcek\ICS\Model\CalendarEvent();
        
        $entryEvent->setStart($begin);//new \DateTime());
        $entryEvent->setEnd($end);
        
        $entryEvent->setSummary($summary);
        
        if(strlen($eventUid) > 0)
        {
            
            $entryEvent->setUid($eventUid);
            
        }
        
        return $entryEvent;
        
    }
    
    /**
     * Adds an organizer to the specified event entry.
     * 
     * @author Peter Hamm
     * @param Jsvrcek\ICS\Model\CalendarEvent $entryEvent
     * @param String $name
     * @param String $email Optional. Default: empty string
     * @param String $language Typically a two-letter language code. Optional, default: empty string.
     * @return void
     */
    protected function addOrganizer(Jsvrcek\ICS\Model\CalendarEvent $entryEvent, $name, $email = "", $language = "")
    {
        
        $organizer = new Jsvrcek\ICS\Model\Relationship\Organizer(new Jsvrcek\ICS\Utility\Formatter());
        
        $organizer->setName($name);
        
        if(strlen($language) > 0)
        {
            
            $organizer->setLanguage($language);
            
        }
        
        if(strlen($email) > 0)
        {
            
            $organizer->setValue($email);
        
        }
        
        
        $entryEvent->setOrganizer($organizer);
        
    }
    
    /**
     * Adds an attendee to the specified event entry.
     * 
     * @author Peter Hamm
     * @param Jsvrcek\ICS\Model\CalendarEvent $entryEvent
     * @param String $name
     * @param String $email Optional. Default: empty string
     * @param String $language Typically a two-letter language code. Optional, default: empty string.
     * @return void
     */
    protected function addAttendee(Jsvrcek\ICS\Model\CalendarEvent $entryEvent, $name, $email = "", $language = "")
    {
        
        $attendee = new Jsvrcek\ICS\Model\Relationship\Attendee(new Jsvrcek\ICS\Utility\Formatter());
        
        $attendee->setName($name);
        
        if(strlen($language) > 0)
        {
            
            $attendee->setLanguage($language);
            
        }
        
        if(strlen($email) > 0)
        {
            
            $attendee->setValue($email);
        
        }
        
        $entryEvent->addAttendee($attendee);
        
    }
    
    /**
     * Will return the generated export as raw text, which then can be easily
     * output as for example a ".ics" file to the browser or attachment to an email.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getExportRaw()
    {
                
        $this->prepareData();
        
        $calendarExport = new Jsvrcek\ICS\CalendarExport(new Jsvrcek\ICS\CalendarStream, new Jsvrcek\ICS\Utility\Formatter());
        $calendarExport->addCalendar($this->calendarExport);

        return $calendarExport->getStream();
        
    }
    
    /**
     * Sends a downloadable calendar file to the browser.
     * Makes use of sending headers.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function sendFileToBrowser()
    {
        
        $exportRaw = $this->getExportRaw();
        
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename=calSy.ics');
        
        echo $exportRaw;
        
    }
    
    /**
     * Generates a temporary file that may be used, for example, as an attachment to an email.
     * Don't forget the delete the file after with $this->deleteExportFile();
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getExportFilePath()
    {
        
        if(strlen($this->exportFilePath) == 0)
        {
            
            $fileName = uniqid("temp_");
                        
            $this->exportFilePath = PerisianFrameworkToolbox::getPathFilesTemporary() . $fileName;
                        
            $rawExport = $this->getExportRaw();
            
            if(@file_put_contents($this->exportFilePath, $rawExport) === false)
            {
                                
                throw new PerisianException($this->exportFilePath . ' ' . PerisianLanguageVariable::getVariable("10725"));
                
            }
            
            if(!file_exists($this->exportFilePath))
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable("10954"));
                
            }
            
        }
        
        return $this->exportFilePath;
        
    }
    
    /**
     * Deletes the file that was exported previously with $this->getExportFile()
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteExportFile()
    {
        
        if(strlen($this->exportFilePath) > 0)
        {
            
            try
            {
                
                @unlink($this->exportFilePath);
                
            }
            catch(Exception $e)
            {
                
                // Do nothing
                
            }
            
        }
        
        $this->exportFilePath = "";
        
    }
    
}