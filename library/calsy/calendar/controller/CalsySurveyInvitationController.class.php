<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';

/**
 * Survey invitation controller
 *
 * @author Peter Hamm
 * @date 2020-02-14
 */
class CalsySurveyInvitationController extends PerisianController
{
    
    /**
     * Displays the landing page when a user clicks an invitation link in the email.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionLandingPage()
    {
        
        $renderer = array(
            
            'page' => 'frontend/calsy/calendar/invitation_landing_page'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        try
        {
            
            $entryId = $this->getParameter('e');
            $callbackCode = $this->getParameter('c');
            
            $userFrontendId = CalsyCalendarEntrySurvey::getUserIdByCode($entryId, $callbackCode);
                        
            $userFrontendObject = new CalsyUserFrontend($userFrontendId);
                                                
            $calendarEntry = new CalsyCalendarEntry($entryId);
            
            $isValid = false;
            
            if($userFrontendObject->{$userFrontendObject->field_pk} > 0 && CalsyCalendarEntrySurvey::isInvitationOpenForUser($entryId, $userFrontendId))
            {
                
                $isValid = true;
                
                $message = nl2br(str_replace("%1", $userFrontendObject->getFullName(), lg('p5e4c5930a00ec')) . "\n\n");
                $message .= nl2br($calendarEntry->getDetailText($ignoreTimezone));
                
                {

                    $userBackendObject = new CalsyUserBackend($calendarEntry->{$calendarEntry->field_user_id});

                    $message .= strlen($userBackendObject->{$userBackendObject->field_fullname}) > 0 ? "\n" . PerisianLanguageVariable::getVariable(10896) . ": " . $userBackendObject->{$userBackendObject->field_fullname} : "";

                }
                
            }
            
        }
        catch(Exception $e)
        {
            
            $isValid = false;
            
        }
        
        if(!$isValid)
        {
            
            $code = "";
            $entryId = "";
            $message = lg('p5e4c5ceee28e1');
            
        }

        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        $result = Array(
            
            'isValid' => $isValid,
            'entryId' => $entryId,
            'code' => $callbackCode,
            'message' => $message,
            'url_image_welcome' => $customLogoUrl
            
        );
                
        try
        {
            
        }
        catch(PerisianException $e)
        {
            
            $result['message'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays a reply to either a confirmation or rejection.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionShowReply()
    {
        
        $renderer = array(
            
            'page' => 'frontend/calsy/calendar/invitation_reply'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $doConfirm = $this->getParameter('doConfirm');
        $doReject = $this->getParameter('doReject');
        
        $message = '';
        
        if($doConfirm)
        {
           
            $message = lg('p5e4c7d312109e');
            
        }
        elseif($doReject)
        {
            
            $message = lg('p5e4c7d440c02c');
            
        }
        
        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        $result = Array(
            
            'message' => $message,
            'url_image_welcome' => $customLogoUrl
            
        );
                      
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays a reply to an appointment's confirmation.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionConfirmReply()
    {
        
        $this->setParameter('doConfirm', true);
        
        return $this->actionShowReply();
        
    }
    
    /**
     * Displays a reply to an appointment's rejection.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionRejectReply()
    {
        
        $this->setParameter('doReject', true);
        
        return $this->actionShowReply();
        
    }
    
    /**
     * Confirms an appointment.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionConfirm()
    {
        
        $this->setParameter('doConfirm', true);
        
        return $this->actionUpdateInvitationStatus();
        
    }
    
    /**
     * Rejects an appointment.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionReject()
    {
        
        $this->setParameter('doReject', true);
        
        return $this->actionUpdateInvitationStatus();
        
    }
    
    /**
     * Retrieves information about tiles, for the appointment assistant.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUpdateInvitationStatus()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => lg('p5e4c733909692'),
            'confirmed' => false
            
        );
        
        try
        {
            
            $doReject = $this->getParameter('doReject');
            $doConfirm = $this->getParameter('doConfirm');
            
            $calendarEntryId = $this->getParameter('id');
            $callbackCode = $this->getParameter('code');
            
            $callbackUserId = CalsyCalendarEntrySurvey::getUserIdByCode($calendarEntryId, $callbackCode);            
            $isValid = CalsyCalendarEntrySurvey::isInvitationOpenForUser($calendarEntryId, $callbackUserId);
            
            $calendarEntry = new CalsyCalendarEntry($calendarEntryId);
            $callbackUser = new CalsyUserFrontend($callbackUserId);
            
            if($isValid)
            {
                
                if($doReject)
                {
                    
                    CalsyCalendarEntrySurvey::rejectInvitation($calendarEntryId, $callbackCode);
                    
                }
                elseif($doConfirm)
                {
                    
                    $areaId = CalsyCalendarEntryFlag::getFlagValueForEntry($calendarEntryId, CalsyCalendarEntrySurvey::FLAG_SURVEY_AREA_ID);
                 
                    $newEntryId = $calendarEntry->copyEntry();
                    
                    {
                        
                        $newEntry = new CalsyCalendarEntry($newEntryId);

                        $newEntry->{$newEntry->field_is_survey} = false;
                        
                        {
                            
                            $userFrontendsWithOrders = array(

                                'p' => $callbackUserId,
                                'a' => $areaId,
                                'c' => 'true'

                            );

                            $newEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));
                            
                        }
                        
                        $newEntry->save(Array(), false, $callbackUser);

                        CalsyCalendarEntryFlag::deleteFlagForEntry($newEntryId, CalsyCalendarEntrySurvey::FLAG_SURVEY_AREA_ID);
                        CalsyCalendarEntryFlag::deleteFlagForEntry($newEntryId, CalsyCalendarEntrySurvey::FLAG_SURVEY_DATA_USERS);
                        
                    }
                    
                    CalsyCalendarEntrySurvey::acceptInvitation($calendarEntryId, $callbackCode);
                    
                }
                    
                $result['success'] = true;
                $result['message'] = lg('p5e4c73979d0d6');
                $result['confirmed'] = $doConfirm && !$doReject;
            
            }
            
        }
        catch(PerisianException $e)
        {
            
            if(PerisianSystemConfiguration::isDebugMode())
            {
                
                $result['debug'] = $e->getMessage();
            
            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'landingPage';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}