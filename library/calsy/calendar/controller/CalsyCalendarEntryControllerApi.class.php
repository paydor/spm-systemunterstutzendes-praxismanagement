<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyCalendarController.class.php";

class CalsyCalendarEntryControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyCalendarController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyCalendarEntry";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('calendar_entry_read');
        
        $objectData = parent::getObject($identifier);
        
        $objectData['flags'] = CalsyCalendarEntryFlag::getFlagsForEntry($identifier);
        
        return $objectData;
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('calendar_entry_write');
        
        return static::updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('calendar_entry_write');
        
        
        // Special fields
        {
            
            $object = parent::getObjectWithFieldData($identifier);
            
            $timestampBegin = PerisianFrameworkToolbox::getRequest($object->timestamp_begin);
            $timestampEnd = PerisianFrameworkToolbox::getRequest($object->timestamp_end);
            
            if(is_null($timestampBegin) || strlen($timestampBegin) == 0)
            {
                
                // 'Invalid timestamp for the beginning.'
                
                $failure = Array(
                    
                    'success' => false,
                    'error' => PerisianLanguageVariable::getVariable('p5a81e641942db')
                    
                );
               
                return $failure;
                
            }
            
            if(is_null($timestampBegin) || strlen($timestampBegin) == 0)
            {
                
                // 'Invalid timestamp for the ending.'
                
                $failure = Array(
                    
                    'success' => false,
                    'error' => PerisianLanguageVariable::getVariable('p5a81e65644c7f') 
                    
                );
               
                return $failure;
                
            }
            
            $object->setTimestampBegin($timestampBegin);
            $object->setTimestampEnd($timestampEnd);
                        
            $result = parent::saveObject($object);
            
            $identifier = $result['id'];
            
        }
        
        // Flag handling
        if($identifier > 0)
        {
                               
            CalsyCalendarEntryFlag::deleteFlagsForEntry($identifier);

            $flags = PerisianFrameworkToolbox::getRequest('flags');
            
            if(is_array($flags))
            {

                foreach($flags as $key => $value)
                {

                    CalsyCalendarEntryFlag::saveFlagForEntry($identifier, $key, $value);

                }
                
            }

        }
        
        return $result;
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('calendar_entry_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('calendar_entry_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteEntry();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('calendar_entry_read');
        
        $this->setAction('list');
        
        // Set some parameters that are possibly passed by the 'filter' data.
        {
            
            $this->setParametersFromRequest();
            
            $filterData = @json_decode(@stripslashes($this->getParameter('filter')), true);
            
            if(isset($filterData['timestamp_begin']))
            {
                
                $_REQUEST['tb'] = $filterData['timestamp_begin'];
                
            }
            
            if(isset($filterData['timestamp_end']))
            {
                
                $_REQUEST['te'] = $filterData['timestamp_end'];
                
            }
                        
            if(isset($filterData['calsy_user_backend_id']))
            {
                
                $_REQUEST['u'] = $filterData['calsy_user_backend_id'];
                
            }
            
            if(isset($filterData['calsy_user_frontend_id']))
            {
                
                $_REQUEST['p'] = $filterData['calsy_user_frontend_id'];
                
            }
            
            if(isset($filterData['calsy_order_id']))
            {
                
                $_REQUEST['r'] = $filterData['calsy_order_id'];
                
            }
            
            if(isset($filterData['calsy_resource_id']))
            {
                
                $_REQUEST['re'] = $filterData['calsy_resource_id'];
                
            }
            
            if(isset($filterData['show_unconfirmed']))
            {
                
                $_REQUEST['ul'] = ($filterData['show_unconfirmed'] === true || $filterData['show_unconfirmed'] == 'true');
                $_REQUEST['su'] = $_REQUEST['ul'];
                $filterData['show_unconfirmed'] = $_REQUEST['ul'];
                
            }
            
            if(isset($filterData['show_canceled']))
            {
                
                $_REQUEST['c'] = ($filterData['show_canceled'] === true || $filterData['show_canceled'] == 'true');
                $filterData['show_canceled'] = $_REQUEST['c'];
                
            }
                        
        }
        
        $resultRaw = $this->finalize();
        
        if(isset($resultRaw['list']))
        {
        
            $result = Array(

                'list' => $resultRaw['list'],
                'count' => $resultRaw['count'],
                'offset' => $resultRaw['offset'],
                'limit' => $resultRaw['limit'],
                'sorting' => $resultRaw['sorting'],
                'order' => $resultRaw['order'],
                'search' => $resultRaw['search'],
                'filter' => $filterData

            );
                    
        }
        else
        {
            
            $result = $resultRaw;
            
        }
        
        return $result;
        
    }
    
}