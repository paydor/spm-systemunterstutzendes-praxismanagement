<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';

/**
 * Main calendar controller
 *
 * @author Peter Hamm
 * @date 2016-11-14
 */
class CalsyCalendarController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/event/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @param CalsyUserBackend $user
     * @return void
     */
    protected function actionList()
    {
        
        global $user;
        
        $dummy = new CalsyCalendarEntry();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $dummy->field_date_begin;
        $search = $this->getParameter('search', '');
        
        try
        {
            
            $entryToOpen = new CalsyCalendarEntry($this->getParameter('se'));
            $entryToOpen = ($entryToOpen->{$entryToOpen->field_pk} > 0 ? $entryToOpen : null);
            
        }
        catch(Exception $e)
        {
            
        }
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/event/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        // Initialization
        {

            $requestUnconfirmed = $this->getParameter('ul');
            $requestCanceled = $this->getParameter('c');

            $timestampBegin = strlen($this->getParameter('tb')) > 0 ? $this->getParameter('tb') : time();
            $timestampEnd = strlen($this->getParameter('te')) > 0 ? $this->getParameter('te') : -1;

            $showPast = ($timestampBegin <= 0);
            $sortOrder = strlen($this->getParameter('order')) > 0 ? $this->getParameter('order') : ($showPast ? "DESC" : "ASC");
                                                
        }

        // Filter handling
        {
            
            $action = $this->getAction();

            $filterUserId = $requestUnconfirmed ? '-1' : ($this->getParameter('u') > 0 || $this->getParameter('u') == CalsyUserBackend::DEFAULT_USER_ID ? $this->getParameter('u') : (empty($action) ? $user->{$user->field_pk} : '-1'));
            $filterUserFrontendId = $this->getParameter('p') == CalsyUserFrontend::DEFAULT_USER_ID ? $this->getParameter('p') : ($this->getParameter('p') > -1 ? $this->getParameter('p') : -1);
            $filterOrderId = $this->getParameter('r') > -1 ? $this->getParameter('r') : -1;
            $filterResourceId = CalsyResourceModule::isEnabled() ? ($this->getParameter('re') > -1 ? $this->getParameter('re') : -1) : -1;
            $filterAreaId = CalsyAreaModule::isEnabled() ? ($this->getParameter('a') > -1 ? $this->getParameter('a') : -1) : -1;
            $filterShowUnconfirmed = ($this->getParameter('su') == 1 ? 1 : 0);
            $filterShowCanceled = ($requestCanceled == 1 ? 1 : 0);
                        
            $filterUser = new CalsyUserBackend($filterUserId > 0 ? $filterUserId : 0);
            $filterUserFrontend = new CalsyUserFrontend($filterUserFrontendId > 0 || $filterUserFrontendId == CalsyUserFrontend::DEFAULT_USER_ID ? $filterUserFrontendId : 0);
            $filterOrder = new CalsyOrder($filterOrderId > 0 ? $filterOrderId : 0);
            $filterResource = new CalsyResource($filterResourceId > 0 ? $filterResourceId : 0);
            $filterArea = new CalsyArea($filterAreaId > 0 ? $filterAreaId : 0);
            
            {
                
                $filter = new CalsyCalendarFilter();
                
                $filter->setUserFrontendIdentifiers($filterUserFrontendId);
                $filter->setUserBackendIdentifiers($filterUserId);
                $filter->setOrderIdentifiers($filterOrderId);
                $filter->setResourceIdentifiers($filterResourceId);
                $filter->setAreaIdentifiers($filterAreaId);
                
                if((int)$filterAreaId > 0)
                {
                    
                    $filter->setDoFilterByAreaIdentifiers(true);
                    
                }
                
                $filter->setShowUnconfirmed($filterShowUnconfirmed);
                $filter->setShowCanceled($filterShowCanceled);
                
            }
                                    
        }
        
        $listTitle = $requestCanceled ? ($showPast ? 'p58209c9d0582d' : 'p58209c847d755') : ($filterShowUnconfirmed ? ($showPast ? 10922 : 10921) : ($filterUserId == @$user->{$user->field_pk} ? ($showPast ? 10856 : 10855) : ($showPast ? 'p5a4fdc5c7ea89' : 'p5a4fdc439f997')));
        
        $result = array(
            
            'title_list' => $listTitle,
            
            'list' => CalsyCalendarEntry::getCalendarEntriesForFilter($filter, $timestampBegin, $timestampEnd, $search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyCalendarEntry::getCountCalendarEntriesForFilter($filter, $timestampBegin, $timestampEnd, null, $search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            
            'filter_user_frontend_id' => $filterUserFrontendId,
            'filter_user_backend_id' => $filterUserId,
            'filter_order_id' => $filterOrderId,
            'filter_resource_id' => $filterResourceId,
            'filter_area_id' => $filterAreaId,
            'filter_show_unconfirmed' => $filterShowUnconfirmed,
            'filter_show_canceled' => $filterShowCanceled,
            'filter_show_past' => $showPast,
            
            'filter_user_backend' => $filterUser,
            'filter_user_frontend' => $filterUserFrontend,
            'filter_order' => $filterOrder,
            'filter_resource' => $filterResource,
            'filter_area' => $filterArea,
            
            'timestamp_calendar_start' => $timestampBegin,
            'timestamp_calendar_end' => $timestampEnd,
            
            'entry_to_open' => @$entryToOpen
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides an overview for the display of the calendar, globally.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverviewCalendarGlobal()
    {
        
        $this->setInternal('global', true);
        
        $this->actionOverviewCalendar();
        
    }
    
    /**
     * Provides an overview for the display of the calendar.
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */ 
    protected function actionOverviewCalendar()
    {
        
        global $user;
        
        $renderer = array(
            
            'page' => ($this->getInternal('global') === true ? 'index' : 'calsy/calendar/overview'),
            'page_menu_right' => 'calsy/calendar/menu/right'
            
        );
                
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $startTimestamp = $this->getParameter('t');
        
        try
        {
            
            $entryToOpen = new CalsyCalendarEntry($this->getParameter('se'));
            $entryToOpen = ($entryToOpen->{$entryToOpen->field_pk} > 0 ? $entryToOpen : null);
            
        }
        catch(Exception $e)
        {
            
        }
        
        $calendarStartDate = strlen($startTimestamp) > 0 ? $startTimestamp : time();
            
        $calendarUserFilter = $user->{$user->field_pk};
        
        if($this->getInternal('global'))
        {
            
            // For the global calendar: Load the stored list of users to filter by, if available

            $storedUserFilter = PerisianUserSetting::getSettingValue($user->{$user->field_pk}, 'user_calendar_filter_users');

            if(strlen($storedUserFilter) > 0)
            {

                $calendarUserFilter = $storedUserFilter;

            }
        
        }
                      
        $result = array(
            
            'entry_to_open' => $entryToOpen,
            'flag_is_global' => $this->getInternal('global') ? 1 : 0,
            
            'timestamp_calendar_start' => $calendarStartDate,
            'filter_user_backend' => $calendarUserFilter
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Retrieves the data to display a daily/weekly/monthly or yearly calendar view.
     * 
     * @author Peter Hamm
     * @param CalsyUserBackend $user Optional
     * @return void
     */
    protected function actionGetCalendar()
    {
        
        global $user;
        
        // Parameter setup
        {
        
            $calendarDisplayType = $this->getParameter('d');
            $calendarStartDate = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : time();
            $calendarFilterUserFrontend = $this->getParameter('p');
            $calendarFilterResource = $this->getParameter('r');
            $calendarFilterFlags = $this->getParameter('f');
            $calendarFilterUsers = $this->getParameter('u');
            $calendarIsGlobal = $this->getParameter('g') == 1;
            
            $calendarShowWorkPlan = false;
            
            if($calendarIsGlobal && is_object($user))
            {
                                
                if(strlen($calendarFilterUsers) > 0)
                {
                                        
                    // Store the filtered backend users 
                    PerisianUserSetting::setSettingValue($user->{$user->field_pk}, 'user_calendar_filter_users', $calendarFilterUsers);
                                        
                }
                else
                {
                    
                    // Load the stored filtered backend users
                    $calendarFilterUsers = PerisianUserSetting::getSettingValue($user->{$user->field_pk}, 'user_calendar_filter_users');
                                        
                }
                
            }
            
            $calendarFilterUsersArray = CalsyCalendar::formatSelectedUsers($calendarFilterUsers);
                                    
        }
        
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setUserBackendIdentifiers($calendarFilterUsers);
            $filter->setUserFrontendIdentifiers($calendarFilterUserFrontend);
            $filter->setResourceIdentifiers($calendarFilterResource);
            
            $filter->setShowHolidays(true);
            $filter->setShowNoDetail($calendarDisplayType == 'year');
                        
            if(is_array($calendarFilterFlags))
            {
                
                foreach($calendarFilterFlags as $calendarFilterFlagKey => $calendarFilterFlagData)
                {
                    
                    if($calendarFilterFlagKey == CalsyWorkPlan::FLAG_WORK_PLAN_ID)
                    {
                        
                        $filter->addFlag($calendarFilterFlagKey, '', '=');
                        
                        $calendarShowWorkPlan = true;
                        
                    }
                    
                    $filter->addFlag($calendarFilterFlagKey, @$calendarFilterFlagData['value'], @$calendarFilterFlagData['operator'], @$calendarFilterFlagData['logic']);
                                        
                }
                
            }
                        
        }
                        
        $calendarData = CalsyCalendar::getCalendarByType($calendarDisplayType, $calendarStartDate, $filter); 
        
        $page = 'calsy/calendar/element/calendar_month';
        
        if($calendarData['type'] == 'year')
        {
            
            $page = 'calsy/calendar/element/calendar_year';
            
        }
        else if($calendarData['type'] == 'week')
        {
                        
            $page = 'calsy/calendar/element/calendar_week_multi';
            
        }
        else if($calendarData['type'] == 'day')
        {
                        
            $page = 'calsy/calendar/element/calendar_day';
            
        }
        
        if($calendarShowWorkPlan)
        {
            
            // Insert a dummy "work plan" user
            array_unshift($calendarFilterUsersArray, CalsyUserBackend::DEFAULT_USER_ID_WORK_PLAN);
            
        }
        
        $result = array(
            
            'filter_user_backend' => $calendarFilterUsersArray,
            'filter_user_frontend' => $calendarFilterUserFrontend,
            
            'count_entities' => count($calendarFilterUsersArray),
            
            'data_calendar' => $calendarData,
            'times_hourly' => CalsyCalendar::getHourlyTimes()
            
        );
                        
        // If the flag filter for bookable times is set,
        //always display all hours in the hourly views
        if(is_array($calendarFilterFlags) && isset($calendarFilterFlags[CalsyBookableTimesModule::FLAG_BOOKABLE_TIME]))
        {
            
            if($calendarFilterFlags[CalsyBookableTimesModule::FLAG_BOOKABLE_TIME]['operator'] == "=" && $calendarFilterFlags[CalsyBookableTimesModule::FLAG_BOOKABLE_TIME]['value'] == "1")
            
            $result['times_hourly'] = Array(
                
                'begin' => 0,
                'end' => 24
                
            );
            
        }
        
        $this->setResultValue('result', $result);
        
        //
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
    }
    
    /**
     * Retrieves a list of backend users that can be selected to filter calendar entries, as well as resources.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUserSelectorWithResource()
    {
        
        $this->setInternal('with_resource', true);
        
        $this->actionUserSelector();
        
    }
    
    
    /**
     * Provides data to display a form to edit the calendar's settings
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSettings()
    {
        
        $renderer = array(

            'page' => 'calsy/calendar/settings'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter initializiation
        {
            
            $userId = $this->getParameter('u');
            $showAdminFunctions = false;
            
        }
        
        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
                        
            {
                                
                // Add the calendar settings to a seperate list
                $calendarSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('calendar_', $systemSettingList, true);
                
                // Specific appointment settings
                {
                    
                    $calendarAppointmentSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('calendar_on_', $calendarSettingList, true);
                    
                    if(!CalsyAreaModule::isEnabled())
                    {
                        
                        PerisianSystemSetting::retrieveSettingsByPrefix('calendar_on_select_account_filter_gender', $calendarAppointmentSettingList, true);
                        
                    }
                
                }
                
                // Different module specific settings
                {
                    
                    $modulePatientRegistrationSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_calsy_patient_registration_and_calendar', $systemSettingList, true);
                    
                }
                
            }
            
            // Get the values for the calendar default end times
            {
                
                $dayList = CalsyCalendar::getDayNames();
                $hourList = CalsyCalendar::getTimeList(300, 300);
                $timeList = CalsyCalendar::getTimeList(300, 0, 86100, true);
                $timeListSegmentation = CalsyCalendar::getTimeList(300, 300, 1800);
                                
                // Allow 5, 10, 15 and 30 minutes, but not 20 or 25.
                unset($timeListSegmentation['1200']);
                unset($timeListSegmentation['1500']);
                                
            }
            
            // Possible notification times
            {
               
                $calendarNotificationTimes = CalsyCalendarNotification::getPossibleNotificationTimes();
                
            }
            
            $displayedDays = CalsyCalendar::getDisplayableDays();

        }
                        
        $result = array(
            
            'list_times_calendar_notification' => $calendarNotificationTimes,
            'list_settings' => $calendarSettingList,
            'list_settings_appointments' => $calendarAppointmentSettingList,
            'list_settings_displayed_days' => $displayedDays,
            
            'list_days' => $dayList,
            'list_hours' => $hourList,
            'list_times' => $timeList,
            'list_times_segmentation' => $timeListSegmentation
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves the displayable days for the calendar
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected function actionSaveDisplayableDays()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $displayableDays = json_encode($this->getParameter('days'));
            
            PerisianSystemSetting::setSettingValue('calendar_displayed_days', $displayableDays);
            
        }
                
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves the calendar settings
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected function actionSaveSettings()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $possibleSettings = Array();
        $settingObj = new PerisianSetting();
        $systemSettingObj = new PerisianSystemSetting();

        $settingList = $systemSettingObj->getPossibleSettings('admin');
        
        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }

        foreach($possibleSettings as $possibleSettingId)
        {
            
            if(isset($_REQUEST['s_' . $possibleSettingId]))
            {

                $newObj = new PerisianSystemSetting($possibleSettingId);
                $newObj->{$newObj->field_setting_value} = PerisianFrameworkToolbox::getRequest('s_' . $possibleSettingId);
                $newObj->save();

            }

        }
        
        $result = array(
            
            'success' => true
            
        );
        
        return $result;
        
    }
    
    /**
     * Retrieves a list of backend users that can be selected to filter calendar entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUserSelector()
    {
        
        $selectedUsers = $this->getParameter('u');
        $selectedUserList = CalsyCalendar::formatSelectedUsers($selectedUsers);
                        
        $calendarUserList = CalsyCalendar::getDisplayableUserList($selectedUserList, null, true);
                                
        $result = array(
            
            'list_user_backend' => $calendarUserList,
            'text_detail_user_selector' => PerisianLanguageVariable::getVariable(10778)
            
        );
        
        if(CalsyWorkPlanModule::isEnabled())
        {
            
            $selectedWorkPlanId = $this->getParameter('wp');
            
            $result['list_work_plans'] = CalsyWorkPlan::getWorkPlanList();
            $result['work_plan_selected'] = new CalsyWorkPlan($selectedWorkPlanId);
                                    
        }
        
        $this->setResultValue('result', $result);
        
        //
        
        $page = 'calsy/calendar/element/detail_user_selector' . ($this->getInternal('with_resource') ? "_with_resource" : "");
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Retrieves data for a basic calendar entry browser
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */
    protected function actionEntryBrowser()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_entry_browser'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
        
            $showPast = $this->getParameter('sp') == "true";
            $offset = $this->getParameter('o') > 0 ? $this->getParameter('o') : 0;
            
            $entriesPerPage = 5;
            
            $order = "ASC";
            $formTitle = 10857;
        
        }
        
        if($showPast)
        {
            
            $formTitle = 10856;
            
            $order = "DESC";
            
            $timeBegin = -1;
            $timeEnd = time();
            
        }
        else
        {
            
            $formTitle = 10855;
            
            $timeBegin = time();
            $timeEnd = -1;
            
        }
        
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setUserBackendIdentifiers($user->{$user->field_pk});
            $filter->setShowNoDetail(true);
            
        }
        
        $events = CalsyCalendarEntry::getEntriesBetween($timeBegin, $timeEnd, $filter, $order, $offset, $entriesPerPage);
        $entryCount = CalsyCalendarEntry::getCountCalendarEntriesForFilter($filter, $timeBegin, $timeEnd);
        
        $moreEntriesAvailable = $entryCount > ($offset + $entriesPerPage);
        
        $result = array(
            
            'list' => $events,
            'count' => $entryCount,
            'more_entries_available' => $moreEntriesAvailable,
            
            'title_form' => PerisianLanguageVariable::getVariable($formTitle),
            'show_past' => $showPast,
            
            'offset' => $offset,
            'limit' => $entriesPerPage
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information for different sources in JSON format.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $result = array(
            
            'success' => false
            
        );
                
        if($source == "order")
        {
            
            if(CalsyOrderModule::isEnabled()) 
            {
                
                $filter = CalsyOrder::createFilter($this->getParameter('p'));

                $result = CalsyOrder::getOrderList($filter);
                
            }
            
        }
        else if($source == "user_frontend")
        {
            
            if(CalsyUserFrontendModule::isEnabled()) 
            {
                
                //$result = CalsyUserFrontend::getUserFrontendListFormatted();
                
                $userFrontendDummy = new CalsyUserFrontend();
                
                $result = CalsyUserFrontend::getUserFrontendListFormatted("", "calsy_user_frontend_fullname", "ASC", 0, 0, $this->getParameter('showDefaultUser') != 'false');
                
            }
            
        }
        else if($source == "resource")
        {
            
            if(CalsyResourceModule::isEnabled()) 
            {
                
                $resourceObject = new CalsyResource();

                $result = CalsyResource::getResourceList("", $resourceObject->field_title);
                
            }
            
        }
        else if($source == "area")
        {
            
            if(CalsyAreaModule::isEnabled()) 
            {
                
                $areaObject = new CalsyArea();

                $result = CalsyArea::getAreaList("", $areaObject->field_title);
                                
            }
            
        }
        else if($source == "user")
        {
            
            $areaId = $this->getParameter('area');
            
            $customUserList = array();
            
            if(strlen($areaId) > 0 && $areaId > 0)
            {
                
                $customUserList = CalsyAreaUserBackend::getUsersForArea($areaId, false, true);
                
            }
            
            $result = CalsyCalendar::getDisplayableUserList(null, $customUserList, true);
            
            if(PerisianSystemSetting::getSettingValue('calendar_on_select_user_filter_free') == 1)
            {
                
                // Return only the users that are available at this time
                
                $timestampBegin = $this->getParameter('begin');
                $timestampEnd = $this->getParameter('end');
                $editId = $this->getParameter('editId');
                                    
                if(strlen($timestampBegin) > 0 && strlen($timestampEnd) > 0)
                {
                    
                    $filteredResult = Array();
                    
                    for($i = 0; $i < count($result); ++$i)
                    {

                        if(!CalsyCalendarEntry::isUserBackendBookedBetween($result[$i]['user_id'], $timestampBegin, $timestampEnd, $editId))
                        {
                            
                            array_push($filteredResult, $result[$i]);
                            
                        }

                    }
                    
                    $result = $filteredResult;
                    
                    unset($filteredResult);
                    
                }
                
            }
            
        }
        else if($source == "entryCounts")
        {
            
            // Filter setup
            {
            
                $filterConfirmed = new CalsyCalendarFilter();
                
                $filterUnconfirmed = new CalsyCalendarFilter();
                $filterUnconfirmed->setShowUnconfirmed(true);
                
                $filterCanceled = new CalsyCalendarFilter();
                $filterCanceled->setShowCanceled(true);
            
            }
            
            $countEntriesConfirmed = CalsyCalendarEntry::getCountCalendarEntriesForFilter($filterConfirmed, time());
            $countEntriesUnconfirmed = CalsyCalendarEntry::getCountCalendarEntriesForFilter($filterUnconfirmed, time());
            $countEntriesCanceled = CalsyCalendarEntry::getCountCalendarEntriesForFilter($filterCanceled, time());
            
            $countData = Array(
                
                "confirmed" => $countEntriesConfirmed,
                "canceled" => $countEntriesCanceled,
                "unconfirmed" => $countEntriesUnconfirmed
                
            );
            
            $result = $countData;
            
        }
        else if($source == "entry")
        {
                        
            $entry = new CalsyCalendarEntry($this->getParameter('e'));
            $result = $entry->getSummary();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Sends a downloadable ".ics" calendar file to the browser, 
     * containing the data of the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionExportIcs()
    {
        
        $renderer = array(
            
            'blank_page' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $entryIdentifier = $this->getParameter('c');
                
        $export = new CalsyCalendarExport($entryIdentifier);

        $export->sendFileToBrowser();

    }
    
    /**
     * Shows information for an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionInfo()
    {
        
        $this->setInternal('info', true);
        
        $this->actionCreate();
        
        $result = $this->getResultValue('result');
        
        // Enrich the result
        {
            
            $matrix = Array();
                        
            $entryBegin = $result['entry']->{$result['entry']->timestamp_begin};            
            $startDate = CalsyCalendar::getFirstDayOfTypeAsTimestamp('year', $entryBegin);
            
            CalsyCalendar::getMatrixForYear($matrix, $startDate);
                        
            $result['data_calendar']['matrix'] = @$matrix['months'][gmdate("m", $entryBegin) - 1];
                        
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data for a form to book a work plan.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionBookWorkPlan()
    {
        
        $this->setInternal('book_work_plan', true);
        
        $this->actionCreate();
        
    }
    
    /**
     * Edits an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $editId = (int)$this->getParameter('e');
        $editEntry = new CalsyCalendarEntry($editId);
                
        if($this->getParameter('isWorkPlan') == 'true')
        {
            
            $this->actionBookWorkPlan();
            
        }
        else if($editEntry->{$editEntry->field_is_survey} == "1")
        {
                        
            $this->actionEditSurvey();
            
        }
        else
        {
            
            $this->setInternal('edit', true);
        
            $this->actionCreate();
        
        }
        
    }
    
    /**
     * Copies an existing entry to the specified new timestamp.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionCopyEntry()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'success' => false,
            'message' => ''
            
        );
        
        try
        {
            
            $copyId = $this->getParameter('id');
            $targetTimestamp = $this->getParameter('t');
            $targetUser = $this->getParameter('u');
            $keepHour = $this->getParameter('keepHour') == 1;
                        
            if(strlen($targetTimestamp) == 0)
            {
                
                // Invalid time.
                throw new PerisianException(PerisianLanguageVariable::getVariable('11056'));
                
            }
            
            $entry = new CalsyCalendarEntry($copyId);
            
            $targetTimestamp = PerisianTimezone::addBrowserDstOffsetToTimestamp($this->getParameter('dst') == 1, $targetTimestamp);
                                    
            $newIdentifier = $entry->copyEntry($targetTimestamp, $targetUser, $keepHour);
            
            $result['success'] = true;
            $result['id'] = $newIdentifier;
            
        }
        catch(PerisianException $e)
        {
            
            $result['message'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Resets uploaded files
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetUpload()
    {
        
        $this->setInternal('reset', true);
        
        $this->actionUpload();
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUpload()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') != 1)
        {
            
            $this->setResultValue('result', $uploadResult);
            
            return;
            
        }
        
        try
        {
             
            {
                
                $resetFile = false;

                if($this->getInternal('reset') === true)
                {
                    
                    $uploadResult = array(

                        "success" => true,
                        "message" => PerisianLanguageVariable::getVariable(11132)

                    );
                    
                    $resetFile = true;
                    
                }
                else
                {
                    
                    $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                    
                }
                
            }
        
        }
        catch(Exception $e)
        {
            
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Provides data to display a form to create a new or edit an exisitng entry.
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */
    protected function actionCreate()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_edit_entry'
            
        );
        
        $showInfo = $this->getInternal('info') === true;
        $showEdit = $this->getInternal('edit') === true;
        $showBookWorkPlan = $this->getInternal('book_work_plan') === true;
        
        if($showInfo)
        {
            
            $renderer['page'] = 'calsy/calendar/element/detail_info_entry';
            
        }
        else if($showBookWorkPlan)
        {
            
            $renderer['page'] = 'calsy/calendar/element/detail_book_work_plan';
            
        }
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $editId = (strlen($this->getParameter('e')) > 0 && ($showEdit || $showInfo || $showBookWorkPlan)) ? $this->getParameter('e') : null;
            $entryUserId = strlen($this->getParameter('u')) > 0 ? $this->getParameter('u') : null;
            $entryUserFrontendId = strlen($this->getParameter('p')) > 0 ? $this->getParameter('p') : null;
            $entryOrderId = strlen($this->getParameter('u')) > 0 ? $this->getParameter('p') : null;
            $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
            $viewType = strlen($this->getParameter('viewType')) > 0 ? $this->getParameter('viewType') : null;
            
            $copyId = (strlen($this->getParameter('c')) > 0 && $showEdit) ? $this->getParameter('c') : null;
            $isCopy = !is_null($copyId);
            
            if($isCopy)
            {
                
                $editId = $copyId;
                
            }
                        
            // Build the filter
            {
                
                $filter = new CalsyCalendarFilter();
                
                $filter->setUserBackendIdentifiers($user->{$user->field_pk});
                
            }
                        
            $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time());
            $timeEnd = strlen($this->getParameter('te')) > 0 ? $this->getParameter('te') : null;
                                    
        }
         
        if(!$showInfo && !$showInfo && !$isCopy)
        {
            
            // This is a new entry, set the default start time of it to the value specified in the settings.
            
            if($viewType == 'month')
            {
            
                $time = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
                $time += PerisianSystemSetting::getSettingValue('calendar_default_start_time');
                
                $time -= date("Z");
                                                
            }
            
            if((!PerisianTimeZone::isTimestampInDST($time) && !PerisianTimeZone::isDaySwitchToWinterTime($time)) || PerisianTimeZone::isDaySwitchToSummerTime($time))
            {

                $time -= 3600;
                
                if(!is_null($timeEnd))
                {
                    
                    $timeEnd -= 3600;
                    
                }

            }
            
        }
                                
        $editEntry = new CalsyCalendarEntry($editId);
        
        if(!$isCopy)
        {
            
            $editEntry->removeTimeDurationTitle();
            
        }
                
        $userCanEditEntry = true;
        $entryIsInSeries = false;
        
        // If the editing entry has a user ID provided, select that one. 
        // If a new user ID was provided, use that one.
        // If both are not there, use the current user's ID.
        {
        
            if($editEntry->{$editEntry->field_user_id} == CalsyUserBackend::DEFAULT_USER_ID)
            {
                
                $selectedUserId = $editEntry->{$editEntry->field_user_id};
                
            }
            else
            {
                
                if($this->getInternal('edit') == true || $this->getInternal('info') == true)
                {
                    
                    $selectedUserId = ($editEntry->{$editEntry->field_user_id} > 0 ? $editEntry->{$editEntry->field_user_id} : null);
                                        
                }
                else
                {
                    
                    $selectedUserId = $user->{$user->field_pk};
                    
                }
                
            }
                        
            try
            {
                
                $selectedUser = new CalsyUserBackend($selectedUserId);
                
            }
            catch(Exception $e)
            {
                
                $selectedUser = new CalsyUserBackend();
                
            }
                    
        }
        
        // Notification (reminder) options
        {

            $entryNotificationSettings = CalsyCalendarEntryNotificationRelationUser::getDataForCalendarEntryAndRelation($editId, $selectedUserId);
            $possibleNotificationTimes = CalsyCalendarNotification::getPossibleNotificationTimes();
            
        }
        
        // Selected resource
        {
        
            $selectedResource = $editEntry->getResource();
                    
        }
        
        if($editId > 0)
        {
                        
            $userCanEditEntry = ($user->canManageEventsForOthers() || (!$user->canManageEventsForOthers() && $selectedUser->{$selectedUser->field_pk} == $user->{$user->field_pk}));
            
            // Is this entry part of a series? (same group)
            $entryIsInSeries = CalsyCalendarEntry::getCountForGroupId($editEntry->{$editEntry->field_fk}) > 1;
            
            if($entryIsInSeries)
            {
                
                $entrySeries = array(

                    "id" => $editEntry->{$editEntry->field_fk},
                    "next" => $editEntry->getEntryIdFromSeries('next'),
                    "previous" => $editEntry->getEntryIdFromSeries('previous')

                );

            }
                        
        }
        
        // All this entry's associated userFrontend, orders and areas!
        {
            
            $userFrontendOrderList = $editEntry->getUserFrontendWithOrders();
                        
            if(count($userFrontendOrderList) == 0 && strlen($entryUserFrontendId) > 0)
            {
                                                
                $userFrontendOrderList = array();
                
                array_push($userFrontendOrderList, array(
                        
                    'user_frontend' => new CalsyUserFrontend($entryUserFrontendId)
                        
                ));
                
            }
            
        }
        
        // Selectable areas
        {
            
            $dummyArea = new CalsyArea();
            $areaList = CalsyArea::getAreaList("", $dummyArea->field_title);
            
        }
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = $time + (!PerisianTimeZone::isTimestampInDST($time) ? 3600 : 0);
            $calendarDateRangeEnd = !is_null($timeEnd) ? $timeEnd : ($calendarDateRangeBegin + CalsyCalendar::getDefaultEventEndTime());
                        
            if($isCopy)
            {
                
                $durationOldEntry = $editEntry->{$editEntry->timestamp_end} - $editEntry->{$editEntry->timestamp_begin};
                                
                $calendarDateRangeEnd = $calendarDateRangeBegin + $durationOldEntry;
                
            }
            else
            {
                
                if($editEntry->{$editEntry->field_pk} > 0)
                {

                    $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                    $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};

                }
                
            }
            
        }
        
        // Time duration blocks of the area module
        {
                       
            $listAreaTimes = CalsyArea::getTimeDurationElementAllAreas();
            $listAreaTimesActual = Array();
            
            foreach($listAreaTimes as $areaId => $listAreaTimesForArea)
            {
                
                for($i = 0; $i < count($listAreaTimesForArea); ++$i)
                {
                    
                    if($listAreaTimesForArea[$i]['type'] == 'block')
                    {

                        if(!isset($listAreaTimesActual[$areaId]))
                        {

                            $listAreaTimesActual[$areaId] = Array();

                        }

                        array_push($listAreaTimesActual[$areaId], $listAreaTimesForArea[$i]);

                    }
                     
                }
                
            }
            
        }
        
        // Any eventual children of this entry
        {
            
            $listChildren = $editEntry->getChildren();
            
        }
        
        $result = array(
            
            'id' => !$isCopy ? $editId : null,
            'entry' => $editEntry,
            
            'is_copy' => $isCopy,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            'entry_is_in_series' => $entryIsInSeries,
            'entry_settings_notifications' => $entryNotificationSettings,
            
            'filter' => $filter,
            
            'list_user_frontend' => $userFrontendOrderList,
            'list_area_times' => $listAreaTimes,
            'list_area_times_actual' => $listAreaTimesActual,
            'list_children' => $listChildren,
            
            'can_edit_entry' => $userCanEditEntry,
            
            'times_notification_possible' => $possibleNotificationTimes,
            
            'selected_user_backend' => $selectedUser
            
        );
                
        $result['title_form'] = PerisianLanguageVariable::getVariable(!isset($result['id']) ? 10347 : 10346);
        
        if(CalsyResourceModule::isEnabled())
        {
            
            $result['selected_resource'] = $selectedResource;
            
        }
        
        if(CalsyAreaModule::isEnabled())
        {
            
            $result['list_areas'] = $areaList;
            $result['dummy_area'] = $dummyArea;
            
        }
        
        if(CalsyTableReservationModule::isEnabled())
        {
         
            $result['number_persons'] = CalsyTableReservation::getNumberPersonsForCalendarEntry($editId);
            $result['list_locations'] = CalsyTableReservation::getLocationsWithTables();
            $result['selected_table'] = CalsyTableReservation::getTableIdForCalendarEntry($editId);
            $result['selected_location'] = CalsyTableReservation::getLocationIdForCalendarEntry($editId);
            
            $result['selected_location_object'] = !is_null($result['selected_location']) ? new CalsyTableReservationLocation($result['selected_location']) : null;
            $result['selected_location_tables'] = !is_null($result['selected_location_object']) ? $result['selected_location_object']->getTableObjects() : null;
            
        }
        
        if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
        {
            
            $result['list_images'] = CalsyCalendarEntryImage::getImageListWithFileData($editId);
            
        }
        
        if($editId > 0)
        {
            
            if(CalsyCalendarEntryFlag::entryHasFlag($editId, CalsyWorkPlan::FLAG_WORK_PLAN_ID))
            {
                
                $result['is_work_plan'] = true;
                $result['is_work_plan_booking'] = $showBookWorkPlan;
                
                $result['selected_user_backend'] = $showBookWorkPlan ? $user : $result['selected_user_backend'];
                
                $result['work_plan_title'] = CalsyWorkPlan::getTitleForIdentifier(CalsyCalendarEntryFlag::getFlagValueForEntry($editId, CalsyWorkPlan::FLAG_WORK_PLAN_ID));
                $result['work_plan_id'] = CalsyCalendarEntryFlag::getFlagValueForEntry($editId, CalsyWorkPlan::FLAG_WORK_PLAN_ID);
                $result['work_plan_area_id'] = CalsyCalendarEntryFlag::getFlagValueForEntry($editId, CalsyWorkPlan::FLAG_WORK_PLAN_ELEMENT_AREA_ID);
                $result['work_plan_area_title'] = CalsyArea::getTitleForIdentifier($result['work_plan_area_id']);
                
                $result['title_form'] = PerisianLanguageVariable::getVariable($showBookWorkPlan ? 'p5a57a3f4d72a2' : 'p5a343b35217c3');
                
            }
            elseif(CalsyCalendarEntryFlag::entryHasFlag($editId, CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID))
            {
                
                $result['work_plan_title'] = CalsyWorkPlan::getTitleForIdentifier(CalsyCalendarEntryFlag::getFlagValueForEntry($editId, CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID));
                
            }
            
        }
        
        if(isset($entrySeries))
        {
            
            $result['series'] = $entrySeries;
            
        }
        
        // Payment
        {
                        
            if($editId > 0 && CalsyAreaPrice::isPaymentActivated())
            {
                
                for($i = 0; $i < count($result['list_user_frontend']); ++$i)
                {
                    
                    if(!CalsyAreaPrice::isPaymentRequiredForArea($result['list_user_frontend'][$i]['area']->{$result['list_user_frontend'][$i]['area']->field_pk}))
                    {
                        
                        $result['list_user_frontend'][$i]['payment_required_for_area'] = false;
                        
                    }
                    else
                    {
                        
                        $result['list_user_frontend'][$i]['payment_required_for_area'] = true;

                        $transaction = CalsyAreaPrice::getPaymentTransaction($editId, $result['list_user_frontend'][$i]['user_frontend']->{$result['list_user_frontend'][$i]['user_frontend']->field_pk});

                        if($transaction != null)
                        {

                            if(!$transaction->isPaymentReceived())
                            {

                                $result['list_user_frontend'][$i]['payment_required'] = true;

                            }
                            else
                            {

                                $result['list_user_frontend'][$i]['payment_received'] = true;

                                $result['list_user_frontend'][$i]['payment_name_formatted'] = $transaction->getTransactionNameFormatted();
                                $result['list_user_frontend'][$i]['payment_date_received'] = $transaction->getTransactionDateUpdated();

                            }

                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to edit a process engine entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditProcess()
    {
        
        $this->setInternal('edit', true);
        
        $this->actionCreateProcess();
        
    }
    
    /**
     * Provides data to display a form to edit a holiday entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditHoliday()
    {
        
        $this->setInternal('edit', true);
        
        $this->actionCreateHoliday();
        
    }
    
    /**
     * Provides data to create a holiday entry.
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */
    protected function actionCreateHoliday()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_edit_holiday'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $editId = strlen($this->getParameter('e')) > 0 ? $this->getParameter('e') : null;
        $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : null;
        $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
        
        $editEntry = new CalsyCalendarEntry($editId);
        
        $userCanEditEntry = $user->isAdmin();
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
            $calendarDateRangeEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($time);
            
            if($editEntry->{$editEntry->field_pk} > 0)
            {
                
                $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};
                
                $userId = $editEntry->{$editEntry->field_user_id};
                
            }
            
        }
        
        $result = array(
            
            'id' => $editId,
            'entry' => $editEntry,
            
            'can_edit_entry' => $userCanEditEntry,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            
            'list_users' => CalsyCalendar::getDisplayableUserList()
            
        );
        
        if(strlen($userId) > 0)
        {
            
            $result['user_id'] = $userId;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to edit a survey entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditSurvey()
    {
        
        $this->setInternal('edit', true);
        
        $this->actionCreateSurvey();
        
    }
    
    /**
     * Provides data to create a survey entry.
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */
    protected function actionCreateSurvey()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_edit_survey'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $editId = strlen($this->getParameter('e')) > 0 ? $this->getParameter('e') : null;
        $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : null;
        $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
        
        $editEntry = new CalsyCalendarEntry($editId);
        
        $userCanEditEntry = $user->isAdmin();
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
            $calendarDateRangeEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($time);
            
            if($editEntry->{$editEntry->field_pk} > 0)
            {
                
                $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};
                
                $userId = $editEntry->{$editEntry->field_user_id};
                
            }
            
        }
        
        // The selected area for this entry
        {
            
            $selectedArea = null;
            
            if($editId > 0)
            {
                
                $selectedAreaId = (int)CalsyCalendarEntryFlag::getFlagValueForEntry($editId, CalsyCalendarEntrySurvey::FLAG_SURVEY_AREA_ID);
                    
                if($selectedAreaId > 0)
                {
                    
                    $selectedArea = new CalsyArea($selectedAreaId);
                    
                }
                
            }
        
        }
        
        $result = array(
            
            'id' => $editId,
            'entry' => $editEntry,
            
            'can_edit_entry' => $userCanEditEntry,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            
            'list_users' => CalsyCalendar::getDisplayableUserList(),
            'list_users_survey' => CalsyCalendarEntrySurvey::getDisplayableUserListForSurvey($editId),
                
            'area_selected' => $selectedArea
            
        );
        
        if(strlen($userId) > 0)
        {
            
            $result['user_id'] = $userId;
            
        }
        
        if(CalsyResourceModule::isEnabled())
        {
            
            $result['selected_resource'] = $selectedResource;
            
        }
        
        if(CalsyAreaModule::isEnabled())
        {
            
            $dummyArea = new CalsyArea();
            $areaList = CalsyArea::getAreaList("", $dummyArea->field_title);
            
            $result['list_areas'] = $areaList;
            $result['dummy_area'] = $dummyArea;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to create a process engine entry.
     * 
     * @author Peter Hamm
     * @global CalsyUser $user
     * @return void
     */
    protected function actionCreateProcess()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_edit_process'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $editId = strlen($this->getParameter('e')) > 0 ? $this->getParameter('e') : null;
        $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : null;
        $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
        $entryIsInSeries = false;
        
        $editEntry = new CalsyCalendarEntry($editId);
        
        $userCanEditEntry = $user->isAdmin();
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
            $calendarDateRangeEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($time);
            
            if($editEntry->{$editEntry->field_pk} > 0)
            {
                
                $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};
                
            }
            
        }
                
        if($editId > 0)
        {
                                    
            // Is this entry part of a series? (same group)
            $entryIsInSeries = CalsyCalendarEntry::getCountForGroupId($editEntry->{$editEntry->field_fk}) > 1;
                        
            if($entryIsInSeries)
            {
                
                $entrySeries = array(

                    "id" => $editEntry->{$editEntry->field_fk},
                    "next" => $editEntry->getEntryIdFromSeries('next'),
                    "previous" => $editEntry->getEntryIdFromSeries('previous')

                );

            }
                        
        }
        
        // Retrieve the list of possible processes
        {
            
            $processList = array();
            $processError = null;
            
            try
            {
                
                $processList = CalsyProcessEngineModule::getProcessList();
                
            }
            catch(PerisianException $e)
            {
                                
                $processError = $e->getMessage();
                
            }
            
        }
        
        $result = array(
            
            'id' => $editId,
            'entry' => $editEntry,
            
            'is_process' => true,
            
            'can_edit_entry' => $userCanEditEntry,
            
            'entry_is_in_series' => $entryIsInSeries,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            
            'list_processes' => $processList,
            
        );
        
        if(!is_null($processError))
        {
            
            $result['error'] = $processError;
            
        }
        
        if(isset($entrySeries))
        {
            
            $result['series'] = $entrySeries;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyCalendarEntry($deleteId);
        $entryObj->deleteEntry();
        
        $result = array(

            'success' => true,
            'message' => PerisianLanguageVariable::getVariable(10791)

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes a group of entries
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionDeleteGroup()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyCalendarEntry();
        $entryObj->deleteGroup($deleteId);
        
        $result = array(

            'success' => true,
            'message' => PerisianLanguageVariable::getVariable(11148)

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        try
        {

            $editId = strlen($this->getParameter('editId')) > 0 ? $this->getParameter('editId') : null;
            $workPlanId = strlen($this->getParameter('workPlanId')) > 0 ? $this->getParameter('workPlanId') : null;
            
            $userFrontendsWithOrders = $this->getParameter('userFrontendWithOrders');
            $notificationData = $this->getParameter('notificationData');
            
            $isHoliday = $this->getParameter('isHoliday') == "true" ? 1 : 0;
            $isSurvey = $this->getParameter('isSurvey') == "true" ? 1 : 0;
            $isProcess = $this->getParameter('isProcess') == "true" ? 1 : 0;
            
            $surveyUserData = $this->getParameter('surveyUserData');
            $surveyAreaId = $this->getParameter('surveyAreaId');
            
            $locationId = (int)$this->getParameter('location_id');
            $tableId = (int)$this->getParameter('table_id');
            $numberPersons = (int)$this->getParameter('number_persons');
            
            if(!$user->canManageEventsForOthers() && $this->getParameter('userId') != $user->{$user->field_pk})
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10809));
                
            }
            
            $beginFirstEntry = $this->getParameter('begin');
            $endFirstEntry = $this->getParameter('end');
                                    
            $timestampList = array(
                
                array(
                    
                    "begin" => $beginFirstEntry,
                    "end" => $endFirstEntry
                    
                )
                
            );

            if($editId == 0)
            {

                $group = new CalsyCalendarEntryGroup();
                $groupId = $group->save();
                            
                $seriesData = $this->getParameter('seriesData');
                $isSeries = (!$isProcess && !$isHoliday && !$isSurvey && $seriesData['isSeries'] == true) ? 1 : 0;
                
                $seriesInterval = $seriesData['interval'];
                $seriesIntervalEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($seriesData['intervalEnd']);
                
                if($seriesIntervalEnd > $timestampList[0]['begin'] && $seriesInterval > 0)
                {
                                        
                    $currentTimeBegin = $timestampList[0]['begin'] + $seriesInterval;
                    $currentTimeEnd = $timestampList[0]['end'] + $seriesInterval;
                                        
                    while($currentTimeBegin < $seriesIntervalEnd)
                    {
                                                                                                                        
                        array_push($timestampList, array(

                            "begin" => $currentTimeBegin,
                            "end" => $currentTimeEnd

                        ));
                        
                        $currentTimeBegin += $seriesInterval;
                        $currentTimeEnd += $seriesInterval;
                        
                    }
                    
                }

            }
            
            $entriesToSave = array();
            
            foreach($timestampList as $timestampValues)
            {
                
                $userId = $this->getParameter('userId');
                
                if($userId == CalsyUserBackend::DEFAULT_USER_ID)
                {
                    
                    $userId = null;
                    
                }
                
                if($isProcess)
                {
                    
                    $userId = $user->{$user->field_pk};
                    
                    $timestampValues['end'] = $timestampValues['begin'];
                    
                }
                
                $saveEntry = new CalsyCalendarEntry($editId);

                $groupId = $saveEntry->{$saveEntry->field_fk};
                
                if(is_array($this->getParameter('children')))
                {
                    
                    $saveEntry->setDataChildren($this->getParameter('children'));
                    
                }
                
                $description = $this->getParameter('description');
                $description = strlen($description) == 0 ? "" : $description;

                $saveEntry->{$saveEntry->field_is_holiday} = $isHoliday;
                $saveEntry->{$saveEntry->field_is_survey} = $isSurvey;
                $saveEntry->{$saveEntry->field_title} = $this->getParameter('title');
                $saveEntry->{$saveEntry->field_description} = $description;
                
                $saveEntry->setTimestampBegin($timestampValues['begin']);
                $saveEntry->setTimestampEnd($timestampValues['end']);
                
                if($isHoliday)
                {
                    
                    if($userId != '-1' && $userId > 0)
                    {
                        
                        // This is a holiday for a single user.
                        
                        $saveEntry->{$saveEntry->field_user_id} = $userId;
                        $saveEntry->{$saveEntry->field_is_holiday} = true;
                        
                    }
                    
                }
                else
                {
                    
                    $saveEntry->{$saveEntry->field_user_id} = $userId;

                    if(CalsyResourceModule::isEnabled())
                    {
                        
                        $resourceId = (int)$this->getParameter('resourceId');

                        $saveEntry->{$saveEntry->field_fk_secondary} = $resourceId;
                        
                    }
                    
                    if(CalsyUserFrontendModule::isEnabled())
                    {
                        
                        $autoConfirm = PerisianSystemSetting::getSettingValue('calendar_is_enabled_backend_auto_confirmation') == 1;
                        
                        if((is_null($editId) || !isset($editId)) && $autoConfirm)
                        {
                            
                            // Automatically confirm all the new entries for the frontend users
                                                        
                            for($i = 0; $i < count($userFrontendsWithOrders); ++$i)
                            {
                                
                                $userFrontendsWithOrders[$i]["c"] = "true";
                                $userFrontendsWithOrders[$i]["ca"] = "false";
                                
                            }
                            
                        }
                        
                    }
                        
                    $saveEntry->setUserFrontendWithOrders($userFrontendsWithOrders);
                    
                    // Let's see if we can save this entry at this date and time.
                    if(!$isSurvey)
                    {
                        
                        $saveEntry->checkOverlapping();
                        
                    }

                }
                
                array_push($entriesToSave, $saveEntry);
                
            }
            
            $entryIdentifiers = array();
            
            foreach($entriesToSave as $saveEntry)
            {
                
                $newEntryId = $saveEntry->save();
                   
                // Notification (reminder) settings for this entry
                if(@$saveEntry->{$saveEntry->field_user_id})
                {
                    
                    $notificationSetting = CalsyCalendarEntryNotificationRelationUser::getObjectForCalendarEntryAndRelation($newEntryId, $saveEntry->{$saveEntry->field_user_id});

                    $notificationSetting->{$notificationSetting->field_is_enabled} = @$notificationData['time'] > 0;
                    $notificationSetting->{$notificationSetting->field_time} = @$notificationData['time'];
                    
                    $notificationSetting->save();
                    
                }
                
                if($isProcess)
                {
                    
                    $processIdentifier = $this->getParameter('processIdentifier');
                    
                    // Save a flag with the process identifier.
                    CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyProcessEngineModule::FLAG_PROCESS_TO_START, $processIdentifier);
                    
                }
                
                if($workPlanId > 0)
                {
                    
                    CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID, $workPlanId);
                    
                }
                
                // Handle eventual images
                
                if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
                {
                    
                    $imageData = $this->getParameter('images');
                    
                    CalsyCalendarEntryImage::setImagesForCalendarEntry($newEntryId, $imageData);
                    
                }
                
                if($isSurvey)
                {
                    
                    // Handle the $surveyUserData
                    
                    CalsyCalendarEntrySurvey::updateSurveyUsersForEntry($newEntryId, $surveyUserData);
                    CalsyCalendarEntrySurvey::updateSurveyAreaForEntry($newEntryId, $surveyAreaId);
                    
                    CalsyCalendarEntrySurvey::inviteUsersToSurvey($newEntryId);
                                        
                }
                
                if($locationId > -1)
                {

                    //CalsyTableReservation::setLocationForCalendarEntry($newEntryId, $locationId);

                }
                
                if($tableId > -1)
                {

                    CalsyTableReservation::setTableForCalendarEntry($newEntryId, $tableId);

                }
                
                if($numberPersons > -1)
                {
                    
                    CalsyTableReservation::setNumberPersonsForCalendarEntry($newEntryId, $numberPersons);
                    
                }
                
                array_push($entryIdentifiers, $newEntryId);
                
            }
            
            $newIdentifiers = count($entryIdentifiers) == 1 ? $entryIdentifiers[0] : $entryIdentifiers;
            
            $result = array(
                
                "success" => true, 
                "message" => PerisianLanguageVariable::getVariable(10792), 
                "newId" => $newIdentifiers, 
                "groupId" => @$groupId
                    
            );
            
        } 
        catch(Exception $e) 
        {
                        
            if(isset($group) && (int)$groupId > 0)
            {
                
                $group->delete($group->field_pk . "={$groupId}");
                
            }
            
            if(isset($saveEntry) && isset($newId) && $newId > 0)
            {
                
                $saveEntry->delete($saveEntry->field_pk . "={$newId}");
                
            }
            
            $result = array(
                
                "success" => false, 
                "message" => $e->getMessage()
                    
            );

        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overviewCalendar';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}