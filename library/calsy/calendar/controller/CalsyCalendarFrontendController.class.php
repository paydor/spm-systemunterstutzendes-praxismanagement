<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/CalsyOpeningTimesModule.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';

/**
 * Main calendar controller for the frontend
 *
 * @author Peter Hamm
 * @date 2016-11-17
 */
class CalsyCalendarFrontendController extends PerisianController
{
    
    /**
     * Provides an overview for the display of the calendar.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */ 
    protected function actionOverviewCalendar()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'page' => 'frontend/calsy/calendar/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $startTimestamp = $this->getParameter('t');
                
        try
        {
            
            $entryToOpen = new CalsyCalendarEntry($this->getParameter('se'));
            $entryToOpen = ($entryToOpen->{$entryToOpen->field_pk} > 0 ? $entryToOpen : null);
            
        }
        catch(Exception $e)
        {
            
        }
                
        $calendarStartDate = strlen($startTimestamp) > 0 ? $startTimestamp : time();
        $calendarFilterUserFrontend = $userFrontend->{$userFrontend->field_pk};
        
        $result = array(
            
            'entry_to_open' => $entryToOpen,
            
            'timestamp_calendar_start' => $calendarStartDate,
            'filter_user_frontend' => $calendarFilterUserFrontend
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Retrieves data to display a short manual to the frontend user
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionManual()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/calendar/element/detail_manual'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $setting = new PerisianSystemSetting('module_setting_calsy_user_frontend_registration_and_calendar_is_user_selection_enabled');
            $userFrontendCanPickUserBackend = $setting->getValue();
            
        }
                
        $entryDummy = new CalsyCalendarEntry();
        
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setUserFrontendIdentifiers($userFrontend->{$userFrontend->field_pk});
            
        }
        
        $nextEvents = CalsyCalendarEntry::getCalendarEntriesForFilter($filter, time(), -1, '', $entryDummy->field_date_begin, 'ASC', 0, 1);
        $nextEvent = @$nextEvents[0];
        
        $userFrontendOrderObject = null;
        $confirmationStatusString = null;
        
        if(isset($nextEvent))
        {
            
            $userFrontendOrderObject = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListObjectsForCalendarEntry($nextEvent[$entryDummy->field_pk], $userFrontend->{$userFrontend->field_pk});
            $confirmationStatusString = PerisianLanguageVariable::getVariable(10522) . ': <span style="color:' . (CalsyCalendarEntryUserFrontendOrder::getColorForConfirmationStatus($userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation})) . '">' . PerisianLanguageVariable::getVariable($userFrontendOrderObject->isConfirmed() ? 10904 : 10905) . "</span>";
            
        }
        
        $openingTimeString = CalsyOpeningTimesModule::getOpeningTimesAsString();

        $result = array(
            
            'dummy_event' => $entryDummy,
            
            'string_opening_times' => $openingTimeString,
            'string_status_confirmation' => @$confirmationStatusString,
            
            'event_next' => $nextEvent,
            
            'object_user_frontend_order' => @$userFrontendOrderObject,
            'user_frontend_can_pick_user_backend' => $userFrontendCanPickUserBackend
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves the data to display a daily/weekly/monthly or yearly calendar view.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionGetCalendar()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/calendar_month'
            
        );
        
        //
                
        $calendarDisplayType = $this->getParameter('d');
        $calendarStartDate = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : time();
        $calendarFilterUserFrontend = $userFrontend->{$userFrontend->field_pk};
        $calendarFilterUsers = $this->getParameter('u');
        $calendarFilterFlags = $this->getParameter('f');
        
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setUserBackendIdentifiers($calendarFilterUsers);
            $filter->setUserFrontendIdentifiers($calendarFilterUserFrontend);
            
            $filter->setShowHolidays(true);
            $filter->setShowUsersAsBlocked(true);
            $filter->setShowNoDetail($calendarDisplayType == 'year');
            
            if(is_array($calendarFilterFlags))
            {
                
                foreach($calendarFilterFlags as $calendarFilterFlagKey => $calendarFilterFlagData)
                {
                    
                    $filter->addFlag($calendarFilterFlagKey, $calendarFilterFlagData['value'], $calendarFilterFlagData['operator']);
                    
                }
                
            }
            
        }
        
        $calendarData = CalsyCalendar::getCalendarByType($calendarDisplayType, $calendarStartDate, $filter);
        
        if(CalsyBookableTimesModule::isEnabled())
        {
                                
            $calendarData = CalsyCalendar::enrichCalendarMatrixWithBookableTimes($calendarData, ($calendarDisplayType != 'month'));
        
        }
        
        if($calendarDisplayType == 'year')
        {
                        
            $renderer['page'] = 'calsy/calendar/element/calendar_year';
            
        }
        else if($calendarDisplayType == 'week')
        {
                        
            $renderer['page'] = 'calsy/calendar/element/calendar_week_multi';
            
        }
        else if($calendarDisplayType == 'day')
        {
                        
            $renderer['page'] = 'calsy/calendar/element/calendar_day';
            
        }
        
        //
        
        $result = array(
            
            'data_calendar' => $calendarData
            
        );
        
        $this->setResultValue('result', $result);
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Retrieves data for different entities (/sources).
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionGetJson()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $fieldData = array(
            
            'success' => false
            
        );
                
        if($source == "entry")
        {
            
            $entryId = PerisianFrameworkToolbox::getRequest('e');
            
            $entry = new CalsyCalendarEntry($entryId);
            
            $entryIsBookableTime = $entry->hasFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME);
            
            $actualEntryId = !empty($entry->{$entry->field_parent_entry_id}) && $entry->{$entry->field_parent_entry_id} != '0' ? $entry->{$entry->field_parent_entry_id} : $entryId;
            
            $userFrontendOrderObject = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListObjectsForCalendarEntry($actualEntryId, $userFrontend->{$userFrontend->field_pk});
                        
            $htmlContentArray = array();
            
            $tagList = array();
            
            // Tag list
            {
                
                if($entryIsBookableTime)
                {
                  
                    array_push($tagList, '<span class="alert alert-info alert-inline">' . PerisianLanguageVariable::getVariable('p57ab295ddf9ec') . "</span>");

                }
                else
                {

                    if($entry->getIsHomeVisit())
                    {

                        array_push($tagList, '<span class="alert alert-info alert-inline">' . PerisianLanguageVariable::getVariable(10926) . "</span>");

                    }

                    // Confirmation status
                    {

                        $confirmationStatusStage = ($userFrontendOrderObject->isConfirmed() ? "success" : "warning");
                        $confirmationStatusStage = ($userFrontendOrderObject->isCanceled() ? "danger" : $confirmationStatusStage);

                        $confirmationStatusLabel = $userFrontendOrderObject->isConfirmed() ? 10904 : 10905;
                        $confirmationStatusLabel = $userFrontendOrderObject->isCanceled() ? 11160 : $confirmationStatusLabel;

                        $confirmationStatus = '<span class="alert alert-inline alert-' . $confirmationStatusStage . '">' . PerisianLanguageVariable::getVariable($confirmationStatusLabel) . "</span>";
                        array_push($tagList, $confirmationStatus);

                    }
                    
                    // Payment status
                    if(CalsyAreaPrice::isPaymentActivated())
                    {
                                
                        $userFrontendOrderList = $entry->getUserFrontendWithOrders();
                        
                        $areaId = 0;
                        
                        if(count($userFrontendOrderList) > 0)
                        {
                            
                            $areaId = $userFrontendOrderList[0]['area']->{$userFrontendOrderList[0]['area']->field_pk};
                            
                        }
                        
                        if($areaId > 0 && CalsyAreaPrice::isPaymentRequiredForArea($areaId))
                        {

                            $transaction = CalsyAreaPrice::getPaymentTransaction($entryId, $userFrontend->{$userFrontend->field_pk});

                            if($transaction != null)
                            {

                                if(!$transaction->isPaymentReceived())
                                {

                                    array_push($tagList, '<span class="alert alert-danger alert-inline">' . PerisianLanguageVariable::getVariable('p5a31c84f0b4f7') . "</span>");

                                }
                                else
                                {

                                    array_push($tagList, '<span class="alert alert-success alert-inline">' . PerisianLanguageVariable::getVariable('10004') . "</span>");

                                }

                            }

                        }
                        
                    }
                    
                }

                if(count($tagList) > 0)
                {

                    array_push($htmlContentArray, implode("&nbsp;", $tagList));

                } 
                
            }
            
            if(!$entryIsBookableTime && $entry->{$entry->field_user_id} > 0)
            {
                
                try
                {
                    
                    $entryUser = new CalsyUserBackend($entry->{$entry->field_user_id});

                    $userTitle = PerisianLanguageVariable::getVariable(10883) . ": " . $entryUser->{$entryUser->field_fullname};

                    // Backend user
                    array_push($htmlContentArray, $userTitle);
                    
                }
                catch(Exception $e)
                {
                    
                    $entryUser = new CalsyUserBackend();
                    
                }
                
            }
            
            if(strlen($entry->{$entry->field_description}) > 0)
            {
                
                // Entry description
                array_push($htmlContentArray, PerisianLanguageVariable::getVariable(10781) . ": " . mb_strimwidth($entry->{$entry->field_description}, 0, 20, "..."));
                
            }
            
            $fieldData['title'] = date("H:i", $entry->{$entry->timestamp_begin}) . " " . PerisianLanguageVariable::getVariable(10785) . " " . date("H:i", $entry->{$entry->timestamp_end});
            $fieldData['content'] = implode("<br>", $htmlContentArray);
                        
        }
        else if($source == "user")
        {
            
            $areaId = PerisianFrameworkToolbox::getRequest('area');
            
            $customUserList = array();
            
            if(strlen($areaId) > 0 && $areaId > 0)
            {
                
                $customUserList = CalsyAreaUserBackend::getUsersForArea($areaId);
                
            }
            
            $fieldData = CalsyCalendar::getDisplayableUserList(null, $customUserList);
            
        }
        
        $result = $fieldData;
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to create or edit entries.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @throws PerisianException
     */
    protected function actionCreate()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/calendar/element/detail_edit_entry'
            
        );
        
        $showInfo = $this->getInternal('info') === true;
        $showEdit = $this->getInternal('edit') === true;
        
        if($showInfo)
        {
            
            $renderer['page'] = 'calsy/calendar/element/detail_info_entry';
            
        }
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $editId = (strlen($this->getParameter('e')) > 0 && ($showEdit || $showInfo)) ? $this->getParameter('e') : null;
        $copyEntryId = strlen($this->getParameter('c')) > 0 ? $this->getParameter('c') : null;
        $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
        $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : null;
        $viewType = strlen($this->getParameter('viewType')) > 0 ? $this->getParameter('viewType') : null;
        $requestedUserBackendId = strlen($this->getParameter('u')) > 0 ? $this->getParameter('u') : 0;
        $requestedAreaId = strlen($this->getParameter('a')) > 0 ? $this->getParameter('a') : 0;
        
        $editEntry = new CalsyCalendarEntry(strlen($editId) > 0 ? $editId : (strlen($copyEntryId) > 0 ? $copyEntryId : 0));
        
        if(!$showInfo)
        {
            
            $editEntry->updateDurationFromChildren();
        
        }
        
        $editEntry->removeTimeDurationTitle();
        
        {
            
            $userFrontendCanPickUserBackend = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_user_selection_enabled');
            
        }
        
        try 
        {
            
            if(!$showInfo && !$showEdit && $requestedUserBackendId > 0)
            {
                
                $selectedUser = new CalsyUserBackend($requestedUserBackendId);
                $entryUser = $selectedUser;
                                
            }
            else
            {
                
                $selectedUser = new CalsyUserBackend($editEntry->{$editEntry->field_user_id});
                
            }
            
        } 
        catch(Exception $e) 
        {
            
            $selectedUser = new CalsyUserBackend();
            
        }
        
        $userFrontendCanEdit = true;
        $userFrontendCanCancel = false;
        
        $entryIsCopy = (strlen($editId) == 0 && strlen($copyEntryId) > 0);
        
        if(!$showInfo && !$showEdit && !$entryIsCopy)
        {
            
            // This is a new entry, set the default start time of it to the value specified in the settings.
                        
            if($viewType == 'month')
            {
            
                $time = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
                $time += PerisianSystemSetting::getSettingValue('calendar_default_start_time');
                
            }

            if((!PerisianTimeZone::isTimestampInDST($time) && !PerisianTimeZone::isDaySwitchToWinterTime($time)) || PerisianTimeZone::isDaySwitchToSummerTime($time))
            {

                $time -= 3600;
                
                if(isset($timeEnd) && !is_null($timeEnd))
                {
                    
                    $timeEnd -= 3600;
                    
                }

            }
            
        }
        
        if($editId > 0)
        {
            
            if(!$editEntry->containsUserFrontend($userFrontend->{$userFrontend->field_pk}))
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10725));
                
            }
            
            $entryIsConfirmedForUserFrontend = $editEntry->isConfirmedForUserFrontend($userFrontend->{$userFrontend->field_pk});
            $entryIsCanceledForUserFrontend = $editEntry->isCanceledForUserFrontend($userFrontend->{$userFrontend->field_pk});
            
            $userFrontendCanEdit = !$entryIsConfirmedForUserFrontend && !$entryIsCanceledForUserFrontend;
            $userFrontendCanCancel = CalsyUserFrontendRegistrationAndCalendarModule::userFrontendMayCancelAppointment() && $entryIsConfirmedForUserFrontend && $editEntry->{$editEntry->timestamp_begin} > time();
            
            try
            {
                
                $entryUser = new CalsyUserBackend($editEntry->{$editEntry->field_user_id});
                
            }
            catch(Exception $e)
            {
                
                $entryUser = new CalsyUserBackend();
                
            }
            
        }
        
        // All this entry's associated frontend users and orders.
        {
            
            $userFrontendOrderList = $editEntry->getUserFrontendWithOrders($userFrontend->{$userFrontend->field_pk});
            $userFrontendOrderObject = isset($userFrontendOrderList['object']) ? $userFrontendOrderList['object'] : null;
                                    
        }
        
        // Notification (reminder) options
        {

            $entryNotificationSettings = CalsyCalendarEntryNotificationRelationAccount::getDataForCalendarEntryAndRelation($editId, $userFrontend->{$userFrontend->field_pk});
            $possibleNotificationTimes = CalsyCalendarNotification::getPossibleNotificationTimes();
            
        }
        
        // Selectable areas
        {
            
            $dummyArea = new CalsyArea();
            $areaList = CalsyArea::getAreaList("", $dummyArea->field_title);
            
            if(PerisianSystemSetting::getSettingValue('calendar_on_select_account_filter_gender') == 1)
            {
                
                $areaListFiltered = Array();
                
                // Show only areas for the gender of the frontend user
                
                for($i = 0; $i < count($areaList); ++$i)
                {
                    
                    if($areaList[$i][$dummyArea->field_disabled_frontend] == "1")
                    {

                        continue;

                    }

                    if($areaList[$i][$dummyArea->field_gender] == 'none' || $areaList[$i][$dummyArea->field_gender] == $userFrontend->{$userFrontend->field_sex} || $userFrontend->{$userFrontend->field_sex} == 'none')
                    {
                        
                        array_push($areaListFiltered, $areaList[$i]);
                        
                    }
                    
                }
                
                $areaList = $areaListFiltered;
                
                unset($areaListFiltered);
                
            }
                        
            if(isset($userFrontendOrderList['area']))
            {
                
                $selectedArea = $userFrontendOrderList['area'];
                
            }
            else
            {
                
                
                if(!$showInfo && !$showEdit && $requestedAreaId > 0)
                {
                    
                    $selectedArea = new CalsyArea($requestedAreaId);
                    
                }
                else
                {
                    
                    $selectedArea = count($userFrontendOrderList) > 0 ? $userFrontendOrderList[0]['area'] : new CalsyArea();
                    
                }
                
            }
            
        }
        
        // Time duration blocks of the area module
        {
                       
            $listAreaTimes = CalsyArea::getTimeDurationElementAllAreas();
            $listAreaTimesActual = Array();
            
            foreach($listAreaTimes as $areaId => $listAreaTimesForArea)
            {
                
                for($i = 0; $i < count($listAreaTimesForArea); ++$i)
                {
                    
                    if($listAreaTimesForArea[$i]['type'] == 'block')
                    {

                        if(!isset($listAreaTimesActual[$areaId]))
                        {

                            $listAreaTimesActual[$areaId] = Array();

                        }

                        array_push($listAreaTimesActual[$areaId], $listAreaTimesForArea[$i]);

                    }
                     
                }
                
            }
            
        }
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = $time;
            $calendarDateRangeEnd = $calendarDateRangeBegin + CalsyCalendar::getDefaultEventEndTime();
                        
            if($editEntry->{$editEntry->field_pk} > 0)
            {
                
                $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};
                
            }
            
        }
        
        // Any eventual children of this entry
        {
            
            $listChildren = $editEntry->getChildren();
            
        }
                        
        $result = array(
            
            'id' => $editId,
            'id_entry_copy' => $copyEntryId, 
            
            'entry' => $editEntry,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            
            'area_selected' => $selectedArea,
            
            'object_user_frontend_order' => $userFrontendOrderObject,
            
            'can_edit_entry' => $userFrontendCanEdit,
            'can_cancel_user_frontend' => $userFrontendCanCancel,
            'user_frontend_can_pick_user_backend' => $userFrontendCanPickUserBackend,
            
            'is_entry_confirmed_for_user_frontend' => @$entryIsConfirmedForUserFrontend,
            'is_entry_canceled_for_user_frontend' => @$entryIsCanceledForUserFrontend,
            'is_entry_copy' => $entryIsCopy,
            
            'times_notification_possible' => $possibleNotificationTimes,
            'entry_settings_notifications' => $entryNotificationSettings,
            
            'selected_user_backend' => @$entryUser,
            
            'list_areas' => $areaList,
            'list_area_times' => $listAreaTimes,
            'list_area_times_actual' => $listAreaTimesActual,
            'list_children' => $listChildren,
            
            'dummy_area' => $dummyArea,
            
            'payment_required_for_area' => false
            
        );
        
        if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
        {
            
            $result['list_images'] = CalsyCalendarEntryImage::getImageListWithFileData($editId);
            
        }
        
        // Payment
        {
                        
            if($editId > 0 && CalsyAreaPrice::isPaymentActivated() && CalsyAreaPrice::isPaymentRequiredForArea($selectedArea->{$selectedArea->field_pk}))
            {
                
                $result['payment_required_for_area'] = true;
                
                $transaction = CalsyAreaPrice::getPaymentTransaction($editId, $userFrontend->{$userFrontend->field_pk});
                
                if($transaction != null)
                {
                    
                    if(!$transaction->isPaymentReceived())
                    {
                        
                        $result['payment_required'] = true;
                        
                    }
                    else
                    {
                        
                        $result['payment_received'] = true;
                        
                        $result['payment_name_formatted'] = $transaction->getTransactionNameFormatted();
                        $result['payment_date_received'] = $transaction->getTransactionDateUpdated();
                        
                    }
                    
                }
                
            }
            
        }
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Resets uploaded files
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetUpload()
    {
        
        $this->setInternal('reset', true);
        
        $this->actionUpload();
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUpload()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') != 1)
        {
            
            $this->setResultValue('result', $uploadResult);
            
            return;
            
        }
        
        try
        {
             
            {
                
                $resetFile = false;

                if($this->getInternal('reset') === true)
                {
                    
                    $uploadResult = array(

                        "success" => true,
                        "message" => PerisianLanguageVariable::getVariable(11132)

                    );
                    
                    $resetFile = true;
                    
                }
                else
                {
                    
                    $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                    
                }
                
            }
        
        }
        catch(Exception $e)
        {
            
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Provides data to display a form with payment options for a calendar entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @throws PerisianException
     */
    protected function actionPaymentOptions()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/calendar/element/detail_payment_options'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $entryId = $this->getParameter('e');
        
        $paymentUrl = "";
        
        // Retrieve the payment object
        {

            $existingPaymentObject = CalsyAreaPrice::getPaymentTransaction($entryId, $userFrontend->{$userFrontend->field_pk});

            if($existingPaymentObject != null)
            {

                $paymentType = $existingPaymentObject->getTransactionName();

                if($paymentType == 'paypal')
                {

                    $paymentId = $existingPaymentObject->{$existingPaymentObject->getFieldTransactionId()};

                }
                
                $paymentUrl = CalsyAreaPrice::getPaymentUrl($paymentType, $paymentId);

            }
            
        }
        
                                
        $result = array(
            
            'id' => $entryId,
            'payment_url' => $paymentUrl
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Shows information for an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionInfo()
    {
        
        $this->setInternal('info', true);
        
        $this->actionCreate();
        
        $result = $this->getResultValue('result');
        
        // Enrich the result
        {
            
            $matrix = Array();
                        
            $entryBegin = $result['entry']->{$result['entry']->timestamp_begin};            
            $startDate = CalsyCalendar::getFirstDayOfTypeAsTimestamp('year', $entryBegin);
                        
            CalsyCalendar::getMatrixForYear($matrix, $startDate);
                        
            $result['data_calendar']['matrix'] = @$matrix['months'][gmdate("m", $entryBegin) - 1];
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays a form to display bookable times between two timestamps.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionInfoBookableTime()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/calendar/element/detail_info_bookable_time'
            
        );
                
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $timestampBegin = $this->getParameter('t');
            $timestampEnd = $this->getParameter('te');
            
        }
        
        $timezoneOffsetBegin = 3600 + PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($timestampBegin);
        $timezoneOffsetEnd = 3600 + PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($timestampEnd);
        
        $timestampBegin += $timezoneOffsetBegin;
        $timestampEnd += $timezoneOffsetEnd;
        
        $formTitle = PerisianLanguageVariable::getVariable('p57ab295ddf9ec') . " - " . PerisianFrameworkToolbox::timestampToDateTime($timestampBegin) . " - " . (CalsyCalendar::isSameDay($timestampBegin, $timestampEnd) ? PerisianFrameworkToolbox::timestampToTime($timestampEnd) : PerisianFrameworkToolbox::timestampToDateTime($timestampEnd));
        $bookableTimes = CalsyBookableTimesModule::getBookableTimes($timestampBegin, $timestampEnd);
        $bookableTimesInfo = CalsyBookableTimesModule::getBookableTimesInfo($bookableTimes);
                
        $result = Array(
            
            'title_form' => $formTitle,
            'bookable_times' => $bookableTimes,
            'bookable_times_info' => $bookableTimesInfo,
            
            'timestamp_begin' => $timestampBegin,
            'timestamp_end' => $timestampEnd
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Edits an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $this->setInternal('edit', true);
        
        $this->actionCreate();
        
    }
    
    /**
     * Saves a calendar entry for a frontend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionSave()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        try
        {
            
            $autoConfirm = false;

            $editId = strlen($this->getParameter('editId')) > 0 ? $this->getParameter('editId') : null;
            $copyId = $editId == null && strlen($this->getParameter('copyId')) > 0 ? $this->getParameter('copyId') : null;
            
            $userFrontendCanPickUserBackend = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_user_selection_enabled');
            
            $makeCopy = is_null($editId) && $copyId > 0;

            $saveEntry = new CalsyCalendarEntry($makeCopy ? $copyId : $editId);
            $notificationData = $this->getParameter('notificationData');
            
            $areaId = $this->getParameter('areaId');
            $areaObject = null;
            $areaTimeDurationList = null;
                
            $timestampBegin = $this->getParameter('begin');
            $timestampEnd = $this->getParameter('end');
            
            // DST handling
            {
                
                $timeOffsetBegin = 0;
                $timeOffsetEnd = 0;
                                
                if($this->getParameter('dst') == 1)
                {
                    
                    // The browser is in DST, which means we need to 
                    // alter the offset if the entry is not.
                    
                    if(!PerisianTimeZone::isTimestampInDST($timestampBegin))
                    {
                        
                        $timeOffsetBegin -= 3600;
                        
                    }
                    
                    if(!PerisianTimeZone::isTimestampInDST($timestampEnd))
                    {
                        
                        $timeOffsetEnd -= 3600;
                        
                    }
                    
                }
                
                $timestampBegin += $timeOffsetBegin;
                $timestampEnd += $timeOffsetEnd;
                                
            }
            
            if($editId == 0)
            {

                $group = new CalsyCalendarEntryGroup();
                $groupId = $group->save();
                
                $saveEntry->{$saveEntry->field_fk} = $groupId;

            }

            if($userFrontendCanPickUserBackend)
            {
                
                $saveEntry->{$saveEntry->field_user_id} = $this->getParameter('userId');
                
            }
            
            if($makeCopy)
            {
                
                $saveEntry->{$saveEntry->field_pk} = null;
                
            }
            
            // Check if this area can be booked on this day
            if(isset($areaId) && (int)$areaId > 0)
            {
                
                $areaObject = CalsyArea::getAreaById($areaId);
                $areaTimeDurationList = $areaObject->getTimeDurationElements();
                
                // Update the duration
                {
                    
                    $timestampEnd = $timestampBegin + $areaTimeDurationList[0]['duration'];
                    
                }
                
                if(!CalsyBookableTimesModule::isEnabled() || (CalsyOpeningTimesModule::isEnabled() && CalsyBookableTimesModule::isEnabled() && $areaObject->{$areaObject->field_requires_bookable_time} != 1))
                {
                                        
                    // Check if this area is bookable on the day of the specified timestamps.

                    if(!CalsyArea::isBookableForTimestamp($areaId, $timestampBegin) || !CalsyArea::isBookableForTimestamp($areaId, $timestampEnd))
                    {

                        $weekdaysEnabled = $areaObject->getWeekdaysEnabled();
                        $possibleDays = Array();

                        for($i = 0; $i < count($weekdaysEnabled); ++$i)
                        {

                            array_push($possibleDays, CalsyCalendar::getDayNameShort($weekdaysEnabled[$i]));

                        }

                        $message = PerisianLanguageVariable::getVariable('p5901124935691');
                        
                        if(count($possibleDays) < 0)
                        {
                            
                            $message .= ' (' . PerisianLanguageVariable::getVariable('p59011423242fc') . ' ' . implode(", ", $possibleDays) . ')';
                        
                        }

                        throw new PerisianException($message);

                    }
                
                }
                
            }
            
            $saveEntry->{$saveEntry->field_description} = $this->getParameter('description');
            $saveEntry->setTimestampBegin($timestampBegin);
            $saveEntry->setTimestampEnd($timestampEnd);
                        
            if(CalsyUserFrontendModule::isEnabled())
            {
                
                $autoConfirm = CalsyCalendarEntry::isEnabledAutoConfirmationForUserFrontend($areaId);
                $confirmationStatus = $editId == 0 ? ($autoConfirm ? 'true' : 'open') : (@$existingUserFrontendWithOrder->{$existingUserFrontendWithOrder->field_status_confirmation} == 'confirmed' ? 'true' : 'false');
                
                $existingUserFrontendWithOrder = $editId == 0 ? new CalsyCalendarEntryUserFrontendOrder() : CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($editId, $userFrontend->{$userFrontend->field_pk});
                
                $userFrontendsWithOrders = array(
                    
                    'p' => $userFrontend->{$userFrontend->field_pk},
                    'a' => $areaId,
                    'c' => $confirmationStatus,
                    'ca' => (@$existingUserFrontendWithOrder->{$existingUserFrontendWithOrder->field_status_confirmation} == 'canceled' ? 'true' : 'false'),
                    'confirmation_status_message' => @$existingUserFrontendWithOrder->{$existingUserFrontendWithOrder->field_status_confirmation_message},
                    'r' => @$existingUserFrontendWithOrder->{$existingUserFrontendWithOrder->field_order_id},
                    'info_additional' => @$existingUserFrontendWithOrder->{$existingUserFrontendWithOrder->field_info_additional}
                    
                );

                $saveEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));
                
                if(isset($areaId) && (int)$areaId > 0)
                {
                    
                    // Add children data for the selected area, if applicable
                    
                    $childEntryList = Array();
                    $timeOffset = 0;
                    
                    if(is_array($areaTimeDurationList))
                    {
                        
                        for($i = 1; $i < count($areaTimeDurationList); ++$i)
                        {
                                                        
                            if($areaTimeDurationList[$i]['type'] == 'block')
                            {
                                
                                $areaTimeDurationData = Array(
                                    
                                    'begin' => ($timestampEnd + $timeOffset),
                                    'end' => ($timestampEnd + $timeOffset + $areaTimeDurationList[$i]['duration']),
                                    
                                    'title' => $areaTimeDurationList[$i]['title'],
                                    'description' => $saveEntry->{$saveEntry->field_description},
                                    
                                    'userId' => $saveEntry->{$saveEntry->field_user_id}
                                    
                                );
                                
                                array_push($childEntryList, $areaTimeDurationData);
                                
                            }
                            
                            $timeOffset += $areaTimeDurationList[$i]['duration'];
                            
                        }

                        $saveEntry->setDataChildren($childEntryList);

                    }
                    
                }

            }
            
            $saveEntry->checkOverlappingForUserFrontend($userFrontend->{$userFrontend->field_pk});
            
            // Set the entry title
            {
                
                $saveEntry->{$saveEntry->field_title} = $this->getParameter('title');
                $saveEntry->autoGenerateTitle(PerisianLanguageVariable::getVariable('p58ab37027b186'));
                
            }
              
            $newId = $saveEntry->save();
            
            // Handle eventual images

            if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
            {

                $imageData = $this->getParameter('images');
                
                if($editId == 0 || ($editId == 0 && count($imageData) > 0))
                {
                    
                    CalsyCalendarEntryImage::setImagesForCalendarEntry($newId, $imageData);
                    
                }

            }

            CalsyCalendarEntry::onAfterSaveNewEntryFrontend($saveEntry, $userFrontend);
            
            // Payment info
            {
                
                $needsPayment = false;

                if(CalsyAreaPrice::isPaymentActivated() && CalsyAreaPrice::isPaymentRequiredForArea($areaId))
                {
                    
                    $needsPayment = !$saveEntry->isPaidByUserFrontendId($userFrontend->{$userFrontend->field_pk});
                    
                }
            
            }
                        
            // Notification (reminder) settings for this entry
            {

                $notificationSetting = CalsyCalendarEntryNotificationRelationAccount::getObjectForCalendarEntryAndRelation($newId, $userFrontend->{$userFrontend->field_pk});

                $notificationSetting->{$notificationSetting->field_is_enabled} = $notificationData['time'] > 0;
                $notificationSetting->{$notificationSetting->field_time} = $notificationData['time'];

                $notificationSetting->save();

            }

            $result = array(
                
                "id" => $newId,
                "success" => true, 
                "needsPayment" => $needsPayment,
                "message" => PerisianLanguageVariable::getVariable(10792), 
                "newId" => $newId, 
                "groupId" => $saveEntry->{$saveEntry->field_fk}
                
            );
            
        } 
        catch(Exception $e) 
        {
            
            if(isset($group))
            {
                
                $group->delete($group->field_pk . "={$groupId}");
                
            }
            
            if(isset($saveEntry) && isset($newId) && $newId > 0)
            {
                
                $saveEntry->delete($saveEntry->field_pk . "={$newId}");
                
            }
            
            $result = array(
                
                "success" => false, 
                "message" => $e->getMessage()
                    
            );

        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Cancels an entry
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionCancel()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            "success" => false, 
            "message" => PerisianLanguageVariable::getVariable(10954)
                
        );
        
        {
            
            $entryId = PerisianFrameworkToolbox::getRequest('id');
            $cancellationReason = PerisianFrameworkToolbox::getRequest('reason');

            $cancelObject = new CalsyCalendarEntry($entryId);

            $entryIsConfirmedForUserFrontend = $cancelObject->isConfirmedForUserFrontend($userFrontend->{$userFrontend->field_pk});

            $userFrontendCanCancel = CalsyUserFrontendRegistrationAndCalendarModule::userFrontendMayCancelAppointment() && $entryIsConfirmedForUserFrontend && $cancelObject->{$cancelObject->timestamp_begin} > time();
            
            $ignoreTimezone = true;
            
        }
        
        if($userFrontendCanCancel)
        {
            
            CalsyCalendarEntryUserFrontendOrder::cancelForUserFrontend($entryId, $userFrontend->{$userFrontend->field_pk}, $cancellationReason);
        
            $result = array(

                "success" => true, 
                "message" => PerisianLanguageVariable::getVariable(11158)

            );
            
            try
            {
                
                // Inform the backend user that the appointment has just been canceled.
                
                $userBackend = new CalsyUserBackend($cancelObject->{$cancelObject->field_user_id});
                
                $userBackend->sendEmailCalendarEntryCanceled($cancelObject, $userFrontend, $ignoreTimezone);
                
            }
            catch(PerisianException $e)
            {
                
            }
            
            try
            {
                
                // Inform the frontend user that the appointment has just been canceled.
                                
                $userFrontend->sendEmailCalendarEntryCanceled($cancelObject, $ignoreTimezone);
                
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionDelete()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $deleteId = PerisianFrameworkToolbox::getRequest('deleteId');
        
        $deleteObject = new CalsyCalendarEntry($deleteId);
        
        $userFrontendCanEdit = !$deleteObject->isConfirmedForUserFrontend($userFrontend->{$userFrontend->field_pk});
        
        if($userFrontendCanEdit)
        {
            
            // Only delete it if the appointment can still be edited.
            $deleteObject->deleteEntry();
            
        }
        
        $result = array(
            
            "success" => true, 
            "message" => PerisianLanguageVariable::getVariable(10791)
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overviewCalendar';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}