<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'framework/paypal/PerisianPayPal.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/calendar/appointment_assistant/CalsyAppointmentAssistantLog.class.php';

/**
 * Appointment assistant controller
 *
 * @author Peter Hamm
 * @date 2016-11-17
 */
class CalsyCalendarAppointmentAssistantController extends PerisianController
{
    
    const TILES_PER_PAGE = 16;
    
    /**
     * Functionality to propose appointments.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionAppointmentProposal()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        try
        {
            
            // Initialization
            {
            
                $limit = 5;
                
                $areaId = $this->getParameter('a', 0);
                $userFilter = $this->getParameter('u', 0);
                                
                $timestampBegin = $this->getParameter('t');
                $timestampBegin = $timestampBegin < (PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time() + 86400) || strlen($timestampBegin) == 0) ? time() : $timestampBegin;
            
            }

            // Parameter filtration
            {

                $area = new CalsyArea($areaId);
                
                // Check whether we're looking through bookable times for this area or not.
                {
                    
                    $areaRequiresBookableTime = false;
                    
                    if(CalsyOpeningTimesModule::isEnabled() && CalsyBookableTimesModule::isEnabled())
                    {
                                                                        
                        $areaRequiresBookableTime = $area->{$area->field_requires_bookable_time} == '1';
                                                
                    }
                    else if(CalsyBookableTimesModule::isEnabled())
                    {
                                                
                        $areaRequiresBookableTime = true;
                        
                    }
                    
                }

                $appointmentDuration = $area->{$area->field_pk} > 0 ? $area->{$area->field_default_duration} : PerisianSystemSetting::getSettingValue('calendar_default_end_of_appointment_after_begin');
                $appointmentDistance = PerisianSystemSetting::getSettingValue('calendar_default_proposal_interval');
                
                // Take only backend users that are not invisible and that can be selected in the appointment assistant.
                {
                    
                    if(CalsyUserFrontendRegistrationAndCalendarModule::isUserSelectionEnabled())
                    {
                        
                        $userFilter = (strlen($userFilter) > 0 && (int)$userFilter > 0) ? $userFilter : 0;

                    }

                    if($userFilter == 0)
                    {
                        
                        $userList = CalsyAreaUserBackend::getUsersForArea($areaId, false, false);

                        if(count($userList) > 0)
                        {

                            $userFilter = array();

                        }
                        
                        for($i = 0; $i < count($userList); ++$i)
                        {

                            array_push($userFilter, $userList[$i]->{$userList[$i]->field_pk});

                        }
                        
                        if(count($userList) == 0)
                        {

                            throw new PerisianException("No users for the area!");

                        }

                    }
                                   
                }

            }

            $userFrontendFilter = (isset($userFrontend) && @$userFrontend->isLoggedIn()) ? $userFrontend->{$userFrontend->field_pk} : 0;
                        
            // Build the filter
            {
                
                $filter = new CalsyCalendarFilter();

                $filter->setUserFrontendIdentifiers($userFrontendFilter);
                $filter->setUserBackendIdentifiers($userFilter);
                $filter->setAreaIdentifiers($areaId);
                
                $filter->setIgnoreConfirmationStatus(true);
                $filter->setIgnoreTimespanEdges(true);
                
                $filter->addFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID, (CalsyBookableTimesModule::isEnabled() && CalsyAreaModule::isEnabled() ? ($areaId == 0 ? null : $areaId) : null));

            }
                        
            // Find the appointment proposals with the filters specified
            $appointmentProposals = CalsyBookableTimesModule::findBookableTimes($timestampBegin, $appointmentDuration, $appointmentDistance, $filter, $limit, $areaRequiresBookableTime);
                                                            
            // Find out the start time of the 'next' page
            {
                
                $timestampNext = $timestampBegin + ($limit + 1) * $appointmentDistance;
                
                if(count($appointmentProposals) > 0)
                {
                    
                    $timestampNext = $appointmentProposals[0]['begin'] + ($limit + 1) * $appointmentDistance;
                    
                }
                
            }

            $result = array(
                
                'time_from' => $timestampBegin,
                'time_from_formatted' => PerisianFrameworkToolbox::timestampToDateTime($timestampBegin),
                
                'time_next' => $timestampNext + PerisianTimeZone::getTimeZoneOffsetInSeconds(null, true),
                'time_next_formatted' => PerisianFrameworkToolbox::timestampToDateTime($timestampNext),
                
                'filter_user_backend' => $userFilter,
                
                'distance' => $appointmentDistance,
                'duration' => $appointmentDuration,
                
                'proposals' => $appointmentProposals
                
            );
            
            CalsyAppointmentAssistantLog::logResult($filter, $result, true);
            
        }
        catch(PerisianException $e)
        {
                        
            $result = array(
                
                'success' => false, 
                'message' => $e->getMessage(), 
                'display_message' => CalsyBookableTimesModule::isEnabled()
                    
            );
            
            $extendedResult = $result;
            
            if($result['message'] != PerisianLanguageVariable::getVariable('p57ab4b06056ee'))
            {
                
                $extendedResult['exception'] = $e;
                
            }
            
            CalsyAppointmentAssistantLog::logResult(@$filter, $extendedResult, true);

        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays a warning that the selected amount of bookings is higher than the amount
     * of bookings available.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionBookableAmountWarning()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/appointment_assistant/warning_bookable_amount'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $bookableAmount = PerisianFrameworkToolbox::getRequest('amountBookable');
        $bookableAmountSelected = PerisianFrameworkToolbox::getRequest('amountOfBookingsSelected');
                
        $result = array(
            
            "bookableAmount" => $bookableAmount,
            "bookableAmountSelected" => $bookableAmountSelected,
            "title_form" => PerisianLanguageVariable::getVariable('p5e49b227d18a1'),
            "message" => str_replace("%2%", $bookableAmount, str_replace("%1%", $bookableAmountSelected, PerisianLanguageVariable::getVariable('p5e49b46306afe')))
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays information about the bookable time with the specified identifier,
     * e.g. to be displayed in a modal window.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionBookableTimeInfo()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/appointment_assistant/info_bookable_time'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $bookableTimeId = PerisianFrameworkToolbox::getRequest('i');
        
        $isPublicInformationEnabled = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimeId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_PUBLIC_ANNOTATIONS_ENABLED) == "1";
                
        $bookalbeTimeData = null;
        
        if($isPublicInformationEnabled)
        {
            
            $entry = new CalsyCalendarEntry($bookableTimeId);
            
            $imagePath = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimeId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE);
            
            if(stripos($imagePath, "http://") === false && stripos($imagePath, "https://") === false && strlen($imagePath) > 0)
            {
                
                $imagePath = PerisianFrameworkToolbox::getServerAddress(false) . 'ajax/appointment_proposal/?do=getBookableTimeImage&i=' . $bookableTimeId;
                
            }
            
            $bookalbeTimeData = Array(
                
                "description" => $entry->{$entry->field_description},
                "image" => $imagePath
                
            );
            
        }
        
        $result = array(
            
            "data" => $bookalbeTimeData,
            "id" => $bookalbetimeId,
            "title_form" => PerisianLanguageVariable::getVariable('p5d384a5db672e')
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Directly prints out a bookable time's image's data.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetBookableTimeImage()
    {
        
        $bookableTimeId = PerisianFrameworkToolbox::getRequest('i');
        
        $isPublicInformationEnabled = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimeId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_PUBLIC_ANNOTATIONS_ENABLED) == "1";
                
        $bookalbeTimeData = null;
        
        if($isPublicInformationEnabled)
        {
            
            $entry = new CalsyCalendarEntry($bookableTimeId);
            
            $imagePath = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimeId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE);
            
            if(stripos($imagePath, "http://") === false && stripos($imagePath, "https://") === false)
            {
                
                $fileInfo = PerisianUploadFileManager::getFileInfo("image", $imagePath);
                $fileContents = PerisianUploadFileManager::getFile("image", $imagePath);

                $headerString = "Content-type: " . $fileInfo["type"] . ";";
                header($headerString);

                echo $fileContents;
                
            }
                        
        }
        
        exit;
        
    }
    
    /**
     * Tries to create an appointment for a frontend users.
     * 
     * @author Peter Hamm
     * @param global CalsyUserFrontend $userFrontend
     * @return void
     */ 
    protected function actionCreateAppointment()
    {
        
        global $userFrontend;
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        $result = array(

            "success" => false,
            "message" => PerisianLanguageVariable::getVariable(11048)

        );
        
        try
        {

            // Parameters
            {

                $isRegistered = $this->getParameter('isRegistered') == "true";
                $userFrontendData = $this->getParameter('user_frontend');
                $loginAfterSuccess = $this->getParameter('stay_logged_in') == 1;
                
                $appointmentData = $this->getParameter('appointment');
                $areaId = @$appointmentData["area"];
                
                $autoConfirm = CalsyCalendarEntry::isEnabledAutoConfirmationForUserFrontend($areaId);
                $confirmationStatus = $autoConfirm ? 'true' : 'open';
                                
            }

            // Appointment basics
            {

                $appointmentTime = $appointmentData['time'];
                $appointmentTimeEnd = $appointmentData['timeEnd'];

                if(!isset($appointmentTime) || strlen($appointmentTime) == 0)
                {

                    throw new PerisianException(PerisianLanguageVariable::getVariable(11056));

                }

                $annotation = $appointmentData["annotations"];

                $userBackend = new CalsyUserBackend(@$appointmentData['userBackend']);

                $area = new CalsyArea($areaId);
                $defaultDuration = strlen($areaId) > 0 ? $area->{$area->field_default_duration} : PerisianSystemSetting::getSettingValue('calendar_default_end_of_appointment_after_begin');
                
                // Checking if the appointment is overlapping with other entries
                {

                    {

                        $appointmentBegin = $appointmentTime;
                        $appointmentEnd = (strlen($appointmentTimeEnd) > 0 && (int)$appointmentTimeEnd > (int)$appointmentBegin) ? $appointmentTimeEnd : ($appointmentBegin + $defaultDuration);

                    }

                    $calendarEntry = new CalsyCalendarEntry();

                    if(CalsyUserFrontendRegistrationAndCalendarModule::isUserSelectionEnabled())
                    {

                        $calendarEntry->{$calendarEntry->field_user_id} = $userBackend->{$userBackend->field_pk};

                    }

                    $calendarEntry->{$calendarEntry->field_description} = strlen($annotation) > 0 ? $annotation : "";

                    $calendarEntry->setTimestampBegin($appointmentBegin);
                    $calendarEntry->setTimestampEnd($appointmentEnd);

                    if(!$isRegistered)
                    {

                        $userFrontendIdentifier = isset($userFrontend) && $userFrontend->{$userFrontend->field_pk} > 0 ? $userFrontend->{$userFrontend->field_pk} : 0;

                        {

                            $userFrontendsWithOrders = array(

                                'p' => $userFrontendIdentifier,
                                'a' => $areaId,
                                'c' => $confirmationStatus

                            );

                            $calendarEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));

                        }

                        // If we don't have an existing frontend user, check for overlapping entries here already.
                        // We do not have to check for the frontend user's overlapping entries - he has none yet.
                        $calendarEntry->checkOverlappingForUserFrontend($userFrontendIdentifier);

                    }

                }

            }

            // Handling the frontend user
            {

                $saveUserFrontend = null;
                $isExistingAccount = false;

                if(isset($userFrontend) && $userFrontend->isLoggedIn())
                {
                    
                    $saveUserFrontend = $userFrontend;
                    $isExistingAccount = true;

                    $userFrontendsWithOrders = array(

                        'p' => $saveUserFrontend->{$saveUserFrontend->field_pk},
                        'a' => $areaId,
                        'c' => $confirmationStatus

                    );

                    $calendarEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));

                }
                else
                {
                    
                    if($isRegistered)
                    {

                        // Load the existing frontend user
                        $saveUserFrontend = CalsyUserFrontend::getUserFrontendForEmailAndPassword($userFrontendData["email"], $userFrontendData["password"]);
                        
                        $userFrontendsWithOrders = array(

                            'p' => $saveUserFrontend->{$saveUserFrontend->field_pk},
                            'a' => $areaId,
                            'c' => $confirmationStatus
                                    
                        );
                            
                        $calendarEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));
                        
                        // Check for overlapping entries, with the actual frontend user as well.
                        $calendarEntry->checkOverlappingForUserFrontend($saveUserFrontend->{$saveUserFrontend->field_pk});
                        
                        $saveUserFrontend->setIsLoggedInOverride(true);
                        
                    }
                    else
                    {

                        // Create a new frontend user

                        $saveUserFrontend = new CalsyUserFrontend();

                        $isValid = true;
                        $errors = array();

                        if(strlen($userFrontendData["nameFirst"]) < 2)
                        {

                            $isValid = false;

                            array_push($errors, PerisianLanguageVariable::getVariable(10874));

                        }

                        if(strlen($userFrontendData["nameSecond"]) < 2)
                        {

                            $isValid = false;

                            array_push($errors, PerisianLanguageVariable::getVariable(10874));

                        }      

                        if(strlen($userFrontendData["emailOne"]) < 5)
                        {

                            $isValid = false;

                            array_push($errors, PerisianLanguageVariable::getVariable(10094));

                        }

                        if(!PerisianValidation::checkPassword($userFrontendData["passwordOne"], $userFrontendData["passwordTwo"]))
                        {

                            $isValid = false;

                            array_push($errors, PerisianValidation::getLatestExceptionMessage());

                        }

                        if($isValid)
                        {

                            // Save the frontend user

                            $saveUserFrontend->{$saveUserFrontend->field_name_first} = $userFrontendData["nameFirst"];
                            $saveUserFrontend->{$saveUserFrontend->field_name_last} = $userFrontendData["nameSecond"];
                            $saveUserFrontend->{$saveUserFrontend->field_sex} = $userFrontendData["gender"];
                            $saveUserFrontend->{$saveUserFrontend->field_login_password} = PerisianUser::encryptPassword($userFrontendData["passwordOne"]);
                            $saveUserFrontend->{$saveUserFrontend->field_email} = $userFrontendData["emailOne"];
                            $saveUserFrontend->{$saveUserFrontend->field_phone} = $userFrontendData["phone"];
                            $saveUserFrontend->{$saveUserFrontend->field_language_id} = PerisianSystemConfiguration::getLanguageId();
                            $saveUserFrontend->{$saveUserFrontend->field_registration_status} = "inactive";
                            $saveUserFrontend->{$saveUserFrontend->field_created} = PerisianFrameworkToolbox::sqlTimestamp();
                            
                            try
                            {
                                
                                $saveUserFrontend->{$saveUserFrontend->field_pk} = $saveUserFrontend->save();
                                
                            }
                            catch(PerisianException $e)
                            {
                                
                                if(is_a($e, "PerisianDatabaseException"))
                                {
                                                                                                            
                                    if(strpos($e->getSqlError(), "Duplicate") !== false)
                                    {
                                        
                                        // An user with the specified email address already exists.
                                        throw new PerisianException(PerisianLanguageVariable::getVariable('10644'));
                                        
                                    }
                                    
                                }
                                
                                throw $e;
                                
                            }

                            $mailResult = $saveUserFrontend->sendEmailActivation();

                            {

                                $userFrontendsWithOrders = array(

                                    'p' => $saveUserFrontend->{$saveUserFrontend->field_pk},
                                    'a' => $areaId,
                                    'c' => $confirmationStatus

                                );

                                $calendarEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));

                            }

                        }
                        else
                        {

                            // Could not create the account.
                            throw new PerisianException(implode(" ", $errors));

                        }

                    }

                }

                if($saveUserFrontend->{$saveUserFrontend->field_pk} <= 0 || strlen($saveUserFrontend->{$saveUserFrontend->field_pk}) == 0)
                {

                    // Account not found
                    throw new PerisianException(PerisianLanguageVariable::getVariable(11051));

                }

            }
            
            $entriesToSave = Array();
            $multipleBookingData = $this->getParameter('multiple-booking');
            
            if(is_array($multipleBookingData) && isset($multipleBookingData['amount']))
            {
                
                for($i = 0; $i < count($multipleBookingData['data']); ++$i)
                {
                    
                    $clonedEntry = clone $calendarEntry;
                    
                    $clonedEntry->{$clonedEntry->field_description} = strlen($clonedEntry->{$clonedEntry->field_description}) > 0 ? $clonedEntry->{$clonedEntry->field_description} . "\n\n" : "";
                    $clonedEntry->{$clonedEntry->field_description} .= lg('p5e49b8c0bc66d') . $multipleBookingData['data'][$i]['name'];
                    
                    array_push($entriesToSave, $clonedEntry);
                    
                }
                
            }
            else
            {
                
                array_push($entriesToSave, $calendarEntry);
                
            }
                        
            // Saving the appointment and its relations
            foreach($entriesToSave as $calendarEntry)
            {
                
                $userFrontend = $saveUserFrontend;

                // Create a group for the entry.
                {

                    $group = new CalsyCalendarEntryGroup();
                    
                    $groupId = $group->save();

                    $calendarEntry->{$calendarEntry->field_fk} = $groupId;

                }

                $calendarEntry->autoGenerateTitle(PerisianLanguageVariable::getVariable('p58ab37027b186'));
                
                // Saving tags for the calendar entry
                {
                    
                    $calendarEntry->setIsTaggedAsNew(true);
                
                }
                                
                $newCalendarEntryId = $calendarEntry->save();

                // Handle eventual images

                if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_image_upload') == 1)
                {

                    $imageData = Array($this->getParameter('image'));

                    CalsyCalendarEntryImage::setImagesForCalendarEntry($newCalendarEntryId, $imageData);

                }
                
                
                $result = CalsyCalendarEntry::onAfterSaveNewEntryFrontend($calendarEntry, $saveUserFrontend);
                               
            }

            // Do a login of the frontend user, if requested
            if($isRegistered && $loginAfterSuccess)
            {

                $loginUserFrontend = CalsyUserFrontend::handleLogin('login', $userFrontendData["email"], $userFrontendData["password"], true);

            }

        }
        catch(Exception $e)
        {
            
            $result["success"] = false;
            $result["message"] = $e->getMessage();
                        
            $result['e'] = nl2br(print_r($e, true));

        }
        
        //
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information about tiles, for the appointment assistant.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetTileInfo()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => lg('p5e17d1184806a'),
            'page' => 0,
            'hasNextPage' => false
            
        );
        
        try
        {
            
            $requestedPage = abs((int)$this->getParameter('page'));
            $requestedAreaGroup = (int)$this->getParameter('ag');
            
            $tileInfo = $this->getTileInfo($requestedPage, $requestedAreaGroup);
            
            if(count($tileInfo) > 0)
            {

                $result['tiles'] = $tileInfo['tiles'];

                $result['page'] = $tileInfo['page'];
                $result['hasNextPage'] = $tileInfo['hasNextPage'];
                
                $result['success'] = true;
                $result['message'] = '';
            
            }
            
        }
        catch(PerisianException $e)
        {
            
            $result['error'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information about tiles.
     * 
     * @author Peter Hamm
     * @param int $requestedPageIndex
     * @param int $filterAreaGroup Optional, an area group to filter by. Default: 0
     * @return Array
     */
    protected function getTileInfo($requestedPageIndex, $filterAreaGroup = 0)
    {
        
        $requestedPageIndex = (int)$requestedPageIndex;
        $filterAreaGroup = (int)$filterAreaGroup;
        
        $dummy = new CalsyCalendarEntry();
        
        $requestedOffset = $requestedPageIndex * static::TILES_PER_PAGE;
        
        {
            
            $filter = new CalsyCalendarFilter();

            $filterAreas = null;
            $areaGroup = $filterAreaGroup > 0 ? CalsyAreaGroup::getAreaGroupById($filterAreaGroup) : null;

            if($areaGroup != null)
            {

                $filterAreas = $areaGroup->getAreas();
                
                foreach($filterAreas as $key => $filterAreaId)
                {
                    
                    $filter->addFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID, $filterAreaId, '=', $key == 0 ? 'and' : 'or');
                    
                }

            }
                
        }
            
        $bookableTimes = CalsyBookableTimesModule::getBookableTimes(time(), -1, $filter, true, static::TILES_PER_PAGE + 1, $requestedOffset);
        
        $tileInfo = Array(
            
            'tiles' => Array(),
            'page' => $requestedPageIndex,
            'hasNextPage' => count($bookableTimes) > static::TILES_PER_PAGE
            
        );
        
        $counter = 0;
          
        foreach($bookableTimes as $bookableTime)
        {
            
            if($counter == static::TILES_PER_PAGE)
            {
                
                break;
                
            }
            
            $entryObject = new CalsyCalendarEntry($bookableTime[$dummy->field_pk]);
            
            $entryArea = $entryObject->getFlagValue(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID);
            
            $entryNumberBookable = (int)$entryObject->getFlagValue(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE);            
            
            $imageFileName = $entryObject->getFlagValue(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE);
            $imageInfo = PerisianUploadFileManager::getFileInfo('image', $imageFileName);
            $imageUrl = str_replace(PerisianFrameworkToolbox::getServerAddress(), PerisianFrameworkToolbox::getServerAddress(false), $imageInfo['url']);
            
            $timeFormatted = CalsyCalendarEntry::getFormattedEventTimeStringShort($entryObject->{$entryObject->timestamp_begin}, $entryObject->{$entryObject->timestamp_end});
            
            $entry = Array(
                
                'id' => $entryObject->{$entryObject->field_pk},
                'title' => $entryObject->{$entryObject->field_title},
                'area' => $entryArea,
                'image' => $imageUrl,
                'timestamp_begin' => $entryObject->{$entryObject->timestamp_begin},
                'timestamp_end' => $entryObject->{$entryObject->timestamp_end},
                'time_begin' => $entryObject->{$entryObject->field_date_begin},
                'time_end' => $entryObject->{$entryObject->field_date_end},
                'time_formatted' => $timeFormatted
                
            );
                
            if($entryNumberBookable > 0)
            {
                
                $areaFilter = new CalsyCalendarFilter();
                
                $areaFilter->setAreaIdentifiers(Array($entryArea));
                $areaFilter->setDoFilterByAreaIdentifiers(true);
                //$areaFilter->addFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID, $entryArea);
                
                $dstOffsetBegin = PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($entryObject->{$entryObject->timestamp_begin}) + PerisianTimeZone::getTimeZoneOffsetInSeconds(null, true);
                $dstOffsetEnd = PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($entryObject->{$entryObject->timestamp_end}) + PerisianTimeZone::getTimeZoneOffsetInSeconds(null, true);
                                                
                $entryNumberBooked = CalsyCalendarEntry::getNumberExistingEntries($areaFilter, $entryObject->{$entryObject->timestamp_begin} + $dstOffsetBegin, $entryObject->{$entryObject->timestamp_end} + $dstOffsetEnd);
                
                $entry['bookings_current'] = $entryNumberBooked;
                $entry['bookings_max'] = $entryNumberBookable;
                
            }
            
            array_push($tileInfo['tiles'], $entry);
            
            ++$counter;
            
        }
        
        return $tileInfo;
        
    }
    
    /**
     * Functionality to get the price for a specific area.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetAreaPrice()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        try
        {
            
            // Initialization
            {
                                
                $requestedAreaId = $this->getParameter('a', 0);
            
            }

            // Parameter filtration
            {

                $area = new CalsyArea($requestedAreaId);
                $price = CalsyAreaPrice::getPriceForAreaId($area->{$area->field_pk});

            }
            
            if($price->{$price->field_amount} <= 0)
            {
                
                throw new PerisianException("No price set for this area.");
                
            }

            $result = array(
                
                'price_net' => $price->getPriceAmountFormattedNet(),
                'price_gross' => $price->getPriceAmountFormattedGross(),
                'price_tax' => $price->getPriceAmountFormattedTax(),
                'currency' => $price->getCurrency(),
                'currency_symbol' => PerisianCurrency::getSymbolForCurrency($price->getCurrency()),
                'rate_tax' => $price->getRateVat(),
                'percentage_tax' => $price->getPercentageVat()
                    
            );

        }
        catch(PerisianException $e)
        {

            $result = array(
                
                'price_net' => 0,
                'price_gross' => 0,
                'price_tax' => 0,
                'currency' => '',
                'currency_symbol' => '',
                'rate_tax' => 0,
                'percentage_tax' => 0
                    
            );

        }
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'proposal';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}