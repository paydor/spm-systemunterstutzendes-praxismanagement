<?php

require_once 'framework/abstract/PerisianController.class.php';

require_once 'modules/CalsyBookableTimesModule.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';

/**
 * Controller to manage bookable times.
 *
 * @author Peter Hamm
 * @date 2016-11-18
 */
class CalsyBookableTimesController extends PerisianController
{
    
    /**
     * Provides data to edit a bookable time.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditBookableTime()
    {
        
        $this->setInternal('edit', true);
        
        $this->actionCreateBookableTime();
        
    }
    
    /**
     * Provides data to edit or create a bookable time.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionCreateBookableTime()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/bookable_times/element/detail_edit_bookable_time'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $editId = (strlen($this->getParameter('e')) > 0 && $this->getInternal('edit') === true) ? $this->getParameter('e') : null;
        $time = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : null;
        $calendarType = strlen($this->getParameter('t')) > 0 ? $this->getParameter('calendarType') : null;
        
        $editEntry = new CalsyCalendarEntry($editId);
        
        $canEditEntryUserBackend = true;
        
        // Selectable areas
        {
            
            $dummyArea = new CalsyArea();
            $areaList = CalsyArea::getAreaList("", $dummyArea->field_title);
            
            $selectedArea = $dummyArea;
            
            // The area for the bookable time
            if(CalsyAreaModule::isEnabled() && (int)($editEntry->{$editEntry->field_pk}) > 0)
            {

                $selectedAreaId = CalsyCalendarEntryFlag::getFlagValueForEntry($editEntry->{$editEntry->field_pk}, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID);
                $selectedArea = new CalsyArea($selectedAreaId);

            }
            
        }
        
        // The allowed maximum number of bookings for this time.
        {
            
            $entryNumberBookable = 1;
            $maximumAllowedNumberOfBookings = CalsyBookableTimesModule::MAXIMUM_NUMBER_OF_BOOKINGS;
            
            if((int)($editEntry->{$editEntry->field_pk}) > 0)
            {

                $entryNumberBookable = CalsyCalendarEntryFlag::getFlagValueForEntry($editEntry->{$editEntry->field_pk}, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE);

            }
            
        }
        
        // The displayed times
        {
            
            $calendarDateRangeBegin = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($time);
            $calendarDateRangeEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($time);
            
            if($editEntry->{$editEntry->field_pk} > 0)
            {
                
                $calendarDateRangeBegin = $editEntry->{$editEntry->timestamp_begin};
                $calendarDateRangeEnd = $editEntry->{$editEntry->timestamp_end};
                
            }
            else
            {
                
                $calendarDateRangeBegin += PerisianSystemSetting::getSettingValue('calendar_default_start_time') - date("Z");
                $calendarDateRangeEnd -= date("Z");
                
            }
            
        }        
        
        // If the edited entry has a user ID, select that one. 
        {
        
            $selectedUserId = ($editEntry->{$editEntry->field_user_id} > 0 ? $editEntry->{$editEntry->field_user_id} : 0);//$user->{$user->field_pk});
            
            try
            {
                
                $selectedUserBackend = new CalsyUserBackend($selectedUserId);
                
            }
            catch(Exception $e)
            {
                
                $selectedUserBackend = new CalsyUserBackend();
                
            }
                    
        }
        
        $entryIsInSeries = false;
        
        if($editId > 0)
        {
                        
            // Is this entry part of a series? (same group)
            $entryIsInSeries = CalsyCalendarEntry::getCountForGroupId($editEntry->{$editEntry->field_fk}) > 1;
            
            if($entryIsInSeries)
            {
                
                $entrySeries = array(

                    "id" => $editEntry->{$editEntry->field_fk},
                    "next" => $editEntry->getEntryIdFromSeries('next'),
                    "previous" => $editEntry->getEntryIdFromSeries('previous')

                );

            }
                        
        }        
                
        $result = array(
            
            'id' => $editId,
            'entry' => $editEntry,
            
            'can_edit_entry' => $canEditEntryUserBackend,
            
            'dummy_area' => $dummyArea,
            
            'entry_timestamp_begin' => $calendarDateRangeBegin,
            'entry_timestamp_end' => $calendarDateRangeEnd,
            'entry_is_in_series' => $entryIsInSeries,
            
            'list_areas' => $areaList,
            'area_selected' => @$selectedArea,
            'area_selected_id' => @$selectedAreaId,
            
            'number_bookable' => $entryNumberBookable,
            'number_bookable_allowed_maximum' => $maximumAllowedNumberOfBookings,
            
            'selected_user_backend' => $selectedUserBackend,
            'field_upload_image_bookable_time' => $editEntry->getBookableTimeImageData()
            
        );
        
        if(isset($entrySeries))
        {
            
            $result['series'] = $entrySeries;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @global $user
     * @return void
     */
    protected function actionUpload()
    {
                
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        // Parameter setup
        {
            
            $subjectId = $this->getParameter('subjectId');
                    
        }
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        try
        {

            $resetFile = false;

            if($this->getInternal('reset') === true)
            {

                $uploadResult = array(

                    "success" => true,
                    "message" => PerisianLanguageVariable::getVariable(11132)

                );

                $resetFile = true;

            }
            else
            {

                $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                
            }

        }
        catch(Exception $e)
        {
            
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Provides data to display a page on the side, 
     * with settings for the non-repeating bookable times
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionBookableTimesSettings()
    {
        
        global $user;
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/bookable_times/element/calendar_settings_bookable_times'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array();
        
        if($user->hasSetting('is_allowed_editing_all_users_bookable_times'))
        {
            
            $calendarUserList = CalsyCalendar::getDisplayableUserList(Array($user->{$user->field_pk}));
            
            $result['list_user_backend'] = $calendarUserList;
            $result['text_detail_user_selector'] = PerisianLanguageVariable::getVariable('p58da85949bc31');
            
        }
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for an overview of entries.
     * 
     * @author Peter Hamm
     * @param CalsyUserBackend $user
     * @return  void
     */
    protected function actionOverview()
    {
        
        global $user;
        
        $renderer = array(
            
            'page' => 'calsy/bookable_times/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $calendarStartDate = strlen($this->getParameter('t')) > 0 ? $this->getParameter('t') : time();
                
        $result = array(
            
            'timestamp_calendar_start' => $calendarStartDate,
            
            'filter_user_backend' => $user->{$user->field_pk}
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Saves a bookable time calendar entry.
     * Note: Has nothing to do with the repeating bookable times in the calendar settings.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionSaveBookableTime()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array();
        
        try
        {
            
            $showAnnotiationsPublicEnabled = $this->getParameter('showAnnotiationsPublicEnabled') == "1" ? "1" : "0";
            $bookableTimeImage = $this->getParameter('bookableTimeImage');
            $userId = strlen($this->getParameter('userId')) > 0 ? $this->getParameter('userId') : null;
            $editId = strlen($this->getParameter('editId')) > 0 ? $this->getParameter('editId') : null;
            $areaId = strlen($this->getParameter('areaId')) > 0 ? $this->getParameter('areaId') : null;
            $allowedNumberOfBookings = strlen($this->getParameter('allowedNumberOfBookings')) > 0 ? $this->getParameter('allowedNumberOfBookings') : null;
            
            if(!$user->hasSetting('is_allowed_editing_all_users_bookable_times') && $userId != $user->{$user->field_pk})
            {
                
                // Missing access rights, the user may not edit bookable times for other users.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10954'));
                
            }
            
            $beginFirstEntry = $this->getParameter('begin');
            $endFirstEntry = $this->getParameter('end');
                                    
            $timestampList = array(
                
                array(
                    
                    "begin" => $beginFirstEntry,
                    "end" => $endFirstEntry
                    
                )
                
            );

            if($editId == 0)
            {

                $group = new CalsyCalendarEntryGroup();
                $groupId = $group->save();
                
                // Series handling
                {
                    
                    $seriesData = $this->getParameter('seriesData');
                    $isSeries = ($seriesData['isSeries'] == true) ? 1 : 0;

                    $seriesInterval = $seriesData['interval'];
                    $seriesIntervalEnd = PerisianTimeZone::getLastSecondOfDayAsTimestamp($seriesData['intervalEnd']);

                    if($seriesIntervalEnd > $timestampList[0]['begin'] && $seriesInterval > 0)
                    {

                        $currentTimeBegin = $timestampList[0]['begin'] + $seriesInterval;
                        $currentTimeEnd = $timestampList[0]['end'] + $seriesInterval;

                        while($currentTimeBegin < $seriesIntervalEnd)
                        {

                            array_push($timestampList, array(

                                "begin" => $currentTimeBegin,
                                "end" => $currentTimeEnd

                            ));

                            $currentTimeBegin += $seriesInterval;
                            $currentTimeEnd += $seriesInterval;

                        }

                    }
                
                }
                
            }
            
            $entriesToSave = array();
            
            foreach($timestampList as $timestampValues)
            {
                
                $saveEntry = new CalsyCalendarEntry($editId);

                if(isset($groupId))
                {

                    $saveEntry->{$saveEntry->field_fk} = $groupId;

                }

                $saveEntry->{$saveEntry->field_title} = $this->getParameter('title');
                $saveEntry->{$saveEntry->field_user_id} = $userId;
                $saveEntry->{$saveEntry->field_description} = $this->getParameter('description');
                $saveEntry->setTimestampBegin($timestampValues['begin']);
                $saveEntry->setTimestampEnd($timestampValues['end']);
                
                array_push($entriesToSave, $saveEntry);
                
            }
            
            $entryIdentifiers = array();
            
            foreach($entriesToSave as $saveEntry)
            {
                
                $saveEntry->setSendNotifications(false);
                
                $newEntryId = $saveEntry->save();
                                   
                array_push($entryIdentifiers, $newEntryId);
                
                // Save a flag that this is a bookable time.
                CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME, '1');
                
                // Save a flag with the area
                if(CalsyAreaModule::isEnabled())
                {
                    
                    CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID, $areaId);
                    
                }
                
                // Save a flag with the maximum number of allowed bookings
                CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE, $allowedNumberOfBookings);
                
                // Store the flag whether to publicly show the description of the bookable time or not.
                CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_PUBLIC_ANNOTATIONS_ENABLED, $showAnnotiationsPublicEnabled);
                
                // Store the image URL for the bookable time
                CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE, $bookableTimeImage);
                
            }
            
            $newIdentifiers = count($entryIdentifiers) == 1 ? $entryIdentifiers[0] : $entryIdentifiers;
            
            $result = array(
                
                "success" => true, 
                "message" => PerisianLanguageVariable::getVariable('p57ab3ef12a0ea'), 
                "newId" => $newIdentifiers, 
                "groupId" => @$groupId
                    
            );
            
        } 
        catch(Exception $e) 
        {
            
            if(isset($group))
            {
                
                $group->delete($group->field_pk . "={$groupId}");
                
            }
            
            if(isset($saveEntry) && isset($newId) && $newId > 0)
            {
                
                $saveEntry->delete($saveEntry->field_pk . "={$newId}");
                
            }
            
            $result = array(
                
                "success" => false, 
                "message" => $e->getMessage()
                    
            );

        }
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionDeleteBookableTime()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $deleteId = PerisianFrameworkToolbox::getRequest('deleteId');
        
        $deleteObject = new CalsyCalendarEntry($deleteId);
        
        $deleteObject->deleteEntry();
        
        $result = array(
            
            "success" => true, 
            "message" => PerisianLanguageVariable::getVariable('p57ab3eb8b0edc')
                
        );
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}