<?php

/**
 * Stores frontend user and order data for calendar entries
 *
 * @author Peter Hamm
 * @date 2016-01-29
 */
class CalsyCalendarEntryUserFrontendOrder extends PerisianDatabaseModel
{
    
    // Instance
    private static $instance = null;

    // Main table settings
    public $table = 'calsy_calendar_entry_user_frontend_order';
    public $field_pk = 'calsy_calendar_entry_user_frontend_order_id';
    public $field_fk = 'calsy_calendar_entry_user_frontend_order_calendar_entry_id';
    public $field_user_frontend_id = 'calsy_calendar_entry_user_frontend_order_user_frontend_id';
    public $field_order_id = 'calsy_calendar_entry_user_frontend_order_order_id';
    public $field_area_id = 'calsy_calendar_entry_user_frontend_order_area_id';
    public $field_info_additional = 'calsy_calendar_entry_user_frontend_order_info_additional';
    public $field_message_to_client = 'calsy_calendar_entry_user_frontend_order_message_to_client';
    public $field_status_confirmation = 'calsy_calendar_entry_user_frontend_order_status_confirmation';
    public $field_status_confirmation_message = 'calsy_calendar_entry_user_frontend_order_status_message';
    
    protected $confirmationStates = array('none', 'open', 'canceled', 'confirmed');
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Changes the frontend user identifier for all entries associatioed to $identifierFrom
     * to the frontend user with the identifier $identifierTo 
     * 
     * @author Peter Hamm
     * @param int $identifierFrom
     * @param int $identifierTo
     * @return boolean
     */
    public static function changeUserFrontendIdForEntries($identifierFrom, $identifierTo)
    {
        
        $identifierFrom = (int)$identifierFrom;
        $identifierTo = (int)$identifierTo;
                
        if($identifierFrom == 0 || $identifierFrom == 0)
        {
            
            return false;
            
        }
        
        $object = static::getInstance();
        
        $data = static::getUserFrontendOrderListForUserFrontend($identifierFrom);
        
        for($i = 0; $i < count($data); ++$i)
        {
            
            $entry = new static($data[$i][$object->field_pk]);
            
            $entry->{$entry->field_user_frontend_id} = "" . $identifierTo;
                                    
            $entry->save();
            
        }
        
        return true;
        
    }
    
    /**
     * Deletes frontend users that do not have a calendar entry from the system.
     * Will return a message with result data.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function deleteUserFrontendEntriesWithoutCalendarEntry()
    {
        
        $usersWithAppointment = Array();
        $usersWithoutExternalId = Array();
                
        $result = $this->getData('');
        
        for($i = 0; $i < count($result); ++$i)
        {
            
            if($result[$i][$this->field_user_frontend_id] == 0)
            {
                
                continue;
                
            }
            
            if(!isset($usersWithAppointment[$result[$i][$this->field_user_frontend_id]]))
            {
                
                $usersWithAppointment[$result[$i][$this->field_user_frontend_id]] = 0;
                
            }
            
            ++$usersWithAppointment[$result[$i][$this->field_user_frontend_id]];
            
        }
        
        foreach($usersWithAppointment as $usersWithAppointmentUserId => $usersWithAppointmentCount)
        {
            
            $userFrontend = new CalsyUserFrontend($usersWithAppointmentUserId);
            
            if(strlen($userFrontend->{$userFrontend->field_external_id}) == 0)
            {
                
                array_push($usersWithoutExternalId, $userFrontend);
               
            }
            
        }
        
        //
        {
          
            // Retrieve all users that ought to be deleted...
            
            $userInstance = new CalsyUserFrontend();
            
            $query = $userInstance->field_pk . " NOT IN (" . implode(',', array_keys($usersWithAppointment)) . ")";
            
            $usersToDelete = $userInstance->getData($query);
            
            foreach($usersToDelete as $usertoDeleteData)
            {
                
                $userToDelete = new CalsyUserFrontend($usertoDeleteData[$userInstance->field_pk]);
                
                $userToDelete->delete();
                
            }
            
            $message =  "Successfully deleted " . count($usersToDelete) . " users, kept " . count($usersWithAppointment) . " users with apppintments, among which " . count($usersWithoutExternalId) . " have no external ID set!";
            
        }
        
        return $message;
        
    }
    
    /**
     * Retrieves if the confirmation and cancelation part when editing an entry should be displayed or not.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntryUserFrontendOrder $referenceObject
     * @return bool
     */
    public static function getShowConfirmationCancellation($referenceObject)
    {
        
        $showConfirmation = static::getShowConfirmation($referenceObject);
        $showCancellation = static::getShowCancellation($referenceObject);
        
        $returnValue = $showConfirmation || $showCancellation;
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves if the confirmationpart when editing an entry should be displayed or not.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntryUserFrontendOrder $referenceObject
     * @return bool
     */
    public static function getShowConfirmation($referenceObject)
    {
        
        $autoConfirmInBackend = PerisianSystemSetting::getSettingValue('calendar_is_enabled_backend_auto_confirmation') == "1";
        
        $isConfirmed = true;
        $isCanceled = true;
        
        if(isset($referenceObject))
        {
            
            $isConfirmed = $referenceObject->isConfirmed();
            $isCanceled = $referenceObject->isCanceled();
        
        }
        
        $returnValue = !$autoConfirmInBackend || (isset($referenceObject) && ($isConfirmed || $isCanceled));
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves if the confirmationpart when editing an entry should be displayed or not.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarEntryUserFrontendOrder $referenceObject
     * @return bool
     */
    public static function getShowCancellation($referenceObject)
    {
        
        $autoConfirmInBackend = PerisianSystemSetting::getSettingValue('calendar_is_enabled_backend_auto_confirmation') == "1";
        
        $isConfirmed = true;
        $isCanceled = true;
        
        if(isset($referenceObject))
        {
            
            $isConfirmed = $referenceObject->isConfirmed();
            $isCanceled = $referenceObject->isCanceled();
        
        }
                
        $returnValue = !$autoConfirmInBackend || (isset($referenceObject) && ($isConfirmed || $isCanceled));
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a color for the specified confirmation status.
     * 
     * @author Peter Gamm
     * @param String $confirmationStatus
     * @return string
     */
    public static function getColorForConfirmationStatus($confirmationStatus = '')
    {
        
        $color = "#ff8000";
        
        if($confirmationStatus == 'confirmed')
        {
            
            $color = "#00b319";
            
        }
        else if($confirmationStatus == 'canceled')
        {
            
            $color = "#f22314";
            
        }
        
        return $color;
        
    }
    
    /**
     * Retrieves the user_frontend associated with this entry.
     * 
     * @author Peter Hamm
     * @return CalsyUserFrontend
     */
    public function getUserFrontend()
    {
        
        return new CalsyUserFrontend($this->{$this->field_user_frontend_id});
        
    }
    
    /**
     * Retrieves the order associated with this entry.
     * 
     * @author Peter Hamm
     * @return CalsyOrder
     */
    public function getOrder()
    {
        
        return new CalsyOrder($this->{$this->field_order_id});
        
    }

    /**
     * Retrieves a list of all recipies
     * 
     * @author Peter Hamm
     * @paramt int $calendarEntryId
     * @return array
     */
    public static function getUserFrontendOrderListForOrder($orderId)
    {
        
        PerisianFrameworkToolbox::security($orderId);
        
        $instance = self::getInstance();
                
        $query = "{$instance->field_order_id} = '{$orderId}'";
                
        $results = $instance->getData($query);
        
        return $results;
        
    }

    /**
     * Retrieves a list of all frontend users
     * 
     * @author Peter Hamm
     * @paramt int $userFrontendId
     * @return array
     */
    public static function getUserFrontendOrderListForUserFrontend($userFrontendId)
    {
        
        PerisianFrameworkToolbox::security($userFrontendId);
        
        $instance = self::getInstance();
                
        $query = "{$instance->field_user_frontend_id} = '{$userFrontendId}'";
                
        $results = $instance->getData($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves the status of the confirmation, e.g. this can be "confirmed", "canceled", "open", or "none".
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getStatus()
    {
        
        return $this->{$this->field_status_confirmation};
        
    }
    
    /**
     * Retrieves a formatted status string of the confirmation.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getStatusString()
    {
        
        $status = $this->getStatus();
        
        $string = PerisianLanguageVariable::getVariable(10940);
        
        if($status == "confirmed")
        {
            
            $string = PerisianLanguageVariable::getVariable(10904);
            
        }
        elseif($status == "canceled")
        {
            
            $string = PerisianLanguageVariable::getVariable(11160);
        
        }
        
        return $string;
        
    }
    
    /**
     * Retrieves whether this entry is confirmed or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isConfirmed()
    {
        
        return $this->{$this->field_status_confirmation} == 'confirmed';
        
    }
    
    /**
     * Retrieves whether this entry is cacneled or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isCanceled()
    {
        
        return $this->{$this->field_status_confirmation} == 'canceled';
        
    }
    
    /**
     * Updates the confirmation status for the specified calendar entry and frontend user.
     * Does not check for access rights.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userFrontendId
     * @param String $cancellationReason
     * @return int The ID of the saved relation object.
     */
    public static function cancelForUserFrontend($calendarEntryId, $userFrontendId, $cancellationReason)
    {
        
        PerisianFrameworkToolbox::security($cancellationReason);
        
        $object = static::getUserFrontendOrderListObjectsForCalendarEntry($calendarEntryId, $userFrontendId);
        
        $object->{$object->field_status_confirmation} = 'canceled';
        $object->{$object->field_status_confirmation_message} = $cancellationReason;
        
        return $object->save();
        
    }
    
    /**
     * Retrieves a list of all frontend users and orders and their confirmation statuses as objects.
     * If a $user_frontendId is specified, only that associated object is returned. 
     * 
     * @author Peter Hamm
     * @param type $calendarEntryId
     * @return mixed An array of self() objects or a single self() object.
     */
    public static function getUserFrontendOrderListObjectsForCalendarEntry($calendarEntryId, $userFrontendId = 0)
    {
        
        $data = self::getUserFrontendOrderListForCalendarEntry($calendarEntryId);
        
        $list = array();
        
        $instance = self::getInstance();
        
        for($i = 0; $i < count($data); ++$i)
        {
            
            array_push($list, new static($data[$i][$instance->field_pk]));
            
        }
        
        if($userFrontendId > 0)
        {
            
            for($i = 0; $i < count($list); ++$i)
            {
                
                if($list[$i]->{$instance->field_user_frontend_id} == $userFrontendId)
                {
                    
                    return $list[$i];
                    
                }
                
            }
            
            return new static();
            
        }
        
        return $list;
        
    }
    
    /**
     * Moves all relations to the frontend user with the specified identifier $userFrontendIdFrom to 
     * the frontend user with the specified identifier $userFrontendIdTo.
     * 
     * @param type $userFrontenIdFrom
     * @param type $userFrontendIdTo
     * @throws PerisianException
     * @return void
     */
    public static function moveEntriesByUserFrontendIds($userFrontenIdFrom, $userFrontendIdTo)
    {
        
        $userFrontenIdFrom = (int)PerisianFrameworkToolbox::security($userFrontenIdFrom);
        $userFrontendIdTo = (int)PerisianFrameworkToolbox::security($userFrontendIdTo);
        
        if($userFrontenIdFrom <= 0 || $userFrontendIdTo <= 0)
        {
            
            // Invalid or missing parameter
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4'));
            
        }
        
        $instance = static::getInstance();
        
        $databaseConnection = $instance->getDatabaseConnection();
        
        $query = "UPDATE " . $instance->table . " SET " . $instance->field_user_frontend_id . " = '" . $userFrontendIdTo . "' WHERE " . $instance->field_user_frontend_id . " = '" . $userFrontenIdFrom . "'";
        
        $databaseConnection->save($query);
                        
    }

    /**
     * Retrieves a list of all frontend users and orders and their confirmation statuses.
     * 
     * @author Peter Hamm
     * @paramt int $calendarEntryId
     * @return array
     */
    public static function getUserFrontendOrderListForCalendarEntry($calendarEntryId)
    {
        
        PerisianFrameworkToolbox::security($calendarEntryId);
        
        $instance = self::getInstance();
                
        $query = "{$instance->field_fk} = '{$calendarEntryId}'";
                
        $results = $instance->getData($query);
        
        return $results;
        
    }
    
    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static public function getInstance()
    {

        if(!isset(self::$instance))
        {
            
            self::$instance = new self();
            
        }

        return self::$instance;

    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Deletes all entries for the specified calendar entry
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param int $user_frontendId Optional, a user_frontend's identifier. Default: 0 (ignored).
     * @return void
     */
    public static function deleteEntriesForCalendarEntry($entryId, $user_frontendId = 0)
    {
        
        PerisianFrameworkToolbox::security($entryId);
        PerisianFrameworkToolbox::security($user_frontendId);
        
        $object = new self();
        
        $query = $object->field_fk . " = {$entryId}";
        
        if($user_frontendId > 0)
        {
            
            $query .= " AND " . $object->field_user_frontend_id . " = {$user_frontendId}";
            
        }
        
        $object->delete($query);
        
    }
    
    /**
     * Deletes all entries related to the specified calendar $entryId with the user_frontend identifier not in $user_frontendIdentifiersToKeep
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @param Array $user_frontendIdentifiersToKeep Flat array with indexes
     */
    public static function deleteEntriesForCalendarEntryNotInList($entryId, $user_frontendIdentifiersToKeep)
    {
                
        if(!is_array($user_frontendIdentifiersToKeep) || count($user_frontendIdentifiersToKeep) == 0)
        {
            
            return;
            
        }
        
        PerisianFrameworkToolbox::security($user_frontendIdentifiersToKeep);
        
        $object = new self();
        
        $query = $object->field_fk . " = '{$entryId}'";
        $query .= " AND " . $object->field_user_frontend_id . " NOT IN (" . implode(",", $user_frontendIdentifiersToKeep) . ")";
                
        $object->delete($query);
        
    }
    
    /**
     * Fired directly before the object is saved to the database.
     * Makes sure that the confirmation status is a valid value.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
        
        if(!in_array($this->{$this->field_status_confirmation}, $this->confirmationStates))
        {
            
            $this->{$this->field_status_confirmation} = 'none';
            
        }
        
        return parent::onSaveStarted();
        
    }
    
    /**
     * Retrieves an existing or generates a new object of this class, depending whether an entry for it exists or not.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userFrontendId
     * @return CalsyCalendarEntryUserFrontendOrder
     */
    public static function getObjectForUserFrontendAndEntry($calendarEntryId, $userFrontendId)
    {
        
        $object = static::getUserFrontendOrderListObjectsForCalendarEntry($calendarEntryId, $userFrontendId);
        
        if(!is_object($object))
        {
            
            $object = new static();
            
        }
        
        if(!isset($object->{$object->field_pk}) || strlen($object->{$object->field_pk}) == 0)
        {
            
            $object = new static();
            
            $object->{$object->field_user_frontend_id} = $userFrontendId;
            $object->{$object->field_fk} = $calendarEntryId;
            
        }
        
        return $object;
        
    }
    
    /**
     * Retrieves the count of entries for the specified frontend user
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return int
     */
    public static function getCountForUserFrontendId($userFrontendId)
    {
        
        PerisianFrameworkToolbox::security($userFrontendId);
                
        if($userFrontendId > 0)
        {
            
            $object = self::getInstance();
            
            $query = $object->field_user_frontend_id . " = {$userFrontendId}";
                    
            $count = $object->getCount($query);
            
            return $count;
            
        }
        
        return 0;
        
    }
    
}
