<?php

require_once "CalsyCalendarEntryFlag.class.php";
require_once "CalsyCalendarEntryGroup.class.php";
require_once "CalsyCalendarEntryUserFrontendOrder.class.php";
require_once "CalsyCalendarEntryImage.class.php";
require_once "CalsyCalendarEntrySurvey.class.php";
require_once 'modules/CalsyResourceModule.class.php';
require_once 'modules/CalsyOpeningTimesModule.class.php';
require_once 'modules/CalsyBookableTimesModule.class.php';
require_once 'modules/CalsyProcessEngineModule.class.php';

/**
 * Calendar entries
 *
 * @author Peter Hamm
 * @date 2016-01-12
 */
class CalsyCalendarEntry extends PerisianDatabaseModel
{
    
    // Instance
    private static $instance = null;
    
    private static $lastQuery = '';

    // Main table settings
    public $table = 'calsy_calendar_entry';
    public $field_pk = 'calsy_calendar_entry_id';
    public $field_fk = 'calsy_calendar_entry_group_id';
    public $field_parent_entry_id = 'calsy_calendar_entry_parent_entry_id';
    public $field_fk_secondary = 'calsy_calendar_entry_resource_id';
    public $field_user_id = 'calsy_calendar_entry_user_id';
    public $field_date_begin = 'calsy_calendar_entry_date_begin';
    public $field_date_end = 'calsy_calendar_entry_date_end';
    public $field_title = 'calsy_calendar_entry_title';
    public $field_description = 'calsy_calendar_entry_description';
    public $field_is_holiday = 'calsy_calendar_entry_is_holiday';
    public $field_is_survey = 'calsy_calendar_entry_is_survey';
    
    public $timestamp_begin = 'calsy_calendar_entry_date_begin_timestamp';
    public $timestamp_end = 'calsy_calendar_entry_date_end_timestamp';
    
    protected $searchFields = Array('calsy_calendar_entry_id', 'calsy_calendar_entry_title', 'calsy_calendar_entry_description');
    
    protected $userFrontendWithOrders = null;
    protected $sendNotifications = true;
    public $paymentRequired = false;
    
    protected $dataChildren = Array();
    
    public $countChildren = 0;
    
    protected $tagIsNew = false;
    
    /**
     * Overwritten constructor.
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
            
        PerisianFrameworkToolbox::security($entryId);
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $this->{$this->field_pk} = $entryId;
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);

        }

        return;

    }
            
    /**
     * Retrieves the meta data of the profile image of this user (if applicable).
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getBookableTimeImageData()
    {
        
        try
        {
            
            $imageName = CalsyCalendarEntryFlag::getFlagValueForEntry($this->{$this->field_pk}, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE);
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $imageName);
            
        }
        catch(PerisianException $e)
        {
            
            $fileInfo = null;
            
        }
        
        $data = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => 'image_bookable_time',
            "title" => PerisianLanguageVariable::getVariable('p5d385846df6e1'),
            "description" => PerisianLanguageVariable::getVariable('p5d385861b03c1'),

            "image" => $fileInfo

        );
            
        return $data;
        
    }
    
    /**
     * Changes the user identifier for all entries associatioed to $identifierFrom
     * to the user with the identifier $identifierTo 
     * 
     * @author Peter Hamm
     * @param int $identifierFrom
     * @param int $identifierTo
     * @return boolean
     */
    public static function changeUserIdForEntries($identifierFrom, $identifierTo)
    {
        
        $identifierFrom = (int)$identifierFrom;
        $identifierTo = (int)$identifierTo;
                
        if($identifierFrom == 0 || $identifierFrom == 0)
        {
            
            return false;
            
        }
        
        $object = static::getInstance();
        
        $data = static::getEntriesForUserId($identifierFrom);
        
        for($i = 0; $i < count($data); ++$i)
        {
            
            $entry = new static($data[$i][$object->field_pk]);
            
            $entry->{$entry->field_user_id} = "" . $identifierTo;
                                    
            $entry->save();
            
        }
        
        return true;
        
    }
    
    /**
     * Checks if this entry or a child of it is 
     * @param type $timestampOnDay
     */
    public function isEventOrChildOnDay($timestampOnDay)
    {
        
        $timestampDayBegin = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestampOnDay);
        $timestampDayEnd = $timestampDayBegin + 86400;
        
        if($this->{$this->timestamp_begin} >= $timestampDayBegin && $this->{$this->timestamp_begin} < $timestampDayEnd)
        {
            
            return true;
            
        }
        
        if($this->{$this->timestamp_end} >= $timestampDayBegin && $this->{$this->timestamp_end} < $timestampDayEnd)
        {
            
            return true;
            
        }
        
        $children = $this->getChildren();
        
        for($i = 0; $i < count($children); ++$i)
        {
            
            if($children[$i]->{$children[$i]->timestamp_begin} >= $timestampDayBegin && $children[$i]->{$children[$i]->timestamp_begin} < $timestampDayEnd)
            {

                return true;

            }

            if($children[$i]->{$children[$i]->timestamp_end} >= $timestampDayBegin && $children[$i]->{$children[$i]->timestamp_end} < $timestampDayEnd)
            {

                return true;

            }
            
        }
        
        return false;
        
    }
    
    /**
     * Sets child data for this entry.
     * 
     * @author Peter Hamm
     * @param Array $data An array of data
     * @return void
     */
    public function setDataChildren($data)
    {
        
        $this->dataChildren = $data;
        
    }
    
    /**
     * Retrieves the child data for this entry, if applicable.
     * 
     * @author Peter Hamm
     * @return $data
     */
    public function getDataChildren()
    {
        
        return $this->dataChildren;
        
    }
    
    /**
     * Retrieves the resource associated with this entry.
     * 
     * @author Peter Hamm
     * @return CalsyResource
     */
    public function getResource()
    {
        
        $selectedResource = null;
            
        try
        {

            $selectedResource = new CalsyResource($this->{$this->field_fk_secondary});

        }
        catch(Exception $e)
        {

            $selectedResource = new CalsyResource();

        }
        
        return $selectedResource;
      
    }
    
    /**
     * Retrieves the backend user that is associated with this specific entry.
     * 
     * @author Peter Hamm
     * @return CalsyUserBackend
     */
    public function getUserBackend()
    {
        
        $selectedUser = null;
            
        try
        {
            
            $userId = $this->{$this->field_user_id};
            
            if($this->{$this->field_parent_entry_id} > 0 && strlen($this->{$this->field_user_id}) == 0 || $this->{$this->field_user_id} <= 0)
            {
                
                // This entry has no user id, but a parent -> Take the parent's user id.
                
                $parentEntry = new static($this->{$this->field_parent_entry_id});
                
                $userId = $parentEntry->{$parentEntry->field_user_id};
                
            }
            
            $selectedUser = new CalsyUserBackend($userId);
            

        }
        catch(Exception $e)
        {

            $selectedUser = new CalsyUserBackend();

        }
        
        return $selectedUser;
    
    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    public static function getInstance()
    {

        if(!isset(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;

    }
    
    /**
     * Retrieves a short summary for this entry, e.g. to be displayed in a popup.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getSummary()
    {
        
        $htmlContentArray = Array();                

        $userFrontends = $this->getUserFrontendWithOrders();
        $userFrontendList = $this->getUserFrontendListAsString();
        $hasChildren = $this->hasChildren();
        $isChild = @$this->{$this->field_parent_entry_id} > 0;
        
        $entryId = $this->{$this->field_pk};
        
        $isProcess = CalsyCalendarEntryFlag::entryHasFlag($entryId, CalsyProcessEngineModule::FLAG_PROCESS_TO_START);
        $isWorkPlan = CalsyCalendarEntryFlag::entryHasFlag($entryId, CalsyWorkPlan::FLAG_WORK_PLAN_ID);
        $isWorkPlanEntry = CalsyCalendarEntryFlag::entryHasFlag($entryId, CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID);

        if($isProcess)
        {
            
            {
            
                try
                {

                    $creator = new CalsyUserBackend($this->{$this->field_user_id});
                    
                    if(strlen($creator->{$creator->field_fullname}) > 0)
                    {
                        
                        $creatorInfo = PerisianLanguageVariable::getVariable('10025') . ": " . $creator->{$creator->field_fullname};

                        array_push($htmlContentArray, $creatorInfo);
                    
                    }

                }
                catch(PerisianException $e)
                {

                }

            }
            
            {
            
                $processIdentifier = CalsyCalendarEntryFlag::getFlagValueForEntry($entryId, CalsyProcessEngineModule::FLAG_PROCESS_TO_START);
                $processName = CalsyProcessEngineModule::getProcessNameForIdentifier($processIdentifier);

                $processInfo = PerisianLanguageVariable::getVariable('p58a22b627dfa6') . ": " . $processName;

                array_push($htmlContentArray, $processInfo);
                
            }

        }
        else
        {

            // Tag list
            {

                $tagList = CalsyCalendarEntry::getTagsForEntryAsHtml($entryId);

                if(strlen($tagList) > 0)
                {

                    array_push($htmlContentArray, $tagList);

                }

            }

        }

        if(!$isWorkPlan && !$isProcess && CalsyUserFrontendModule::isEnabled() && strlen($userFrontendList) > 0)
        {

            // List of frontend users

            $userFrontendList = PerisianLanguageVariable::getVariable(count($userFrontends) == 1 ? 10784 : 10806) . ": " . $userFrontendList;

            array_push($htmlContentArray, $userFrontendList);

        }

        if($hasChildren || $isChild)
        {

            // Count of backend users associated

            $countUserBackend = $this->getUserBackendCount();

            if($countUserBackend > 1)
            {

                $userBackendCount = str_replace("%1", $countUserBackend, PerisianLanguageVariable::getVariable('p590b21910d996'));

                array_push($htmlContentArray, $userBackendCount);

            }

        }

        if(CalsyResourceModule::isEnabled() && strlen($this->{$this->field_fk_secondary}) > 0 && $this->{$this->field_fk_secondary} > 0)
        {

            $resource = new CalsyResource($this->{$this->field_fk_secondary});

            // Entry title

            array_push($htmlContentArray, PerisianLanguageVariable::getVariable(10916) . ": " . $resource->{$resource->field_title});

        }

        if(strlen($this->{$this->field_description}) > 0)
        {

            // Entry remarks

            array_push($htmlContentArray, PerisianLanguageVariable::getVariable(10781) . ": " . mb_strimwidth($this->{$this->field_description}, 0, 20, "..."));

        }

        if(count($htmlContentArray) == 0)
        {

            array_push($htmlContentArray, PerisianLanguageVariable::getVariable(11149));

        }

        if($isProcess)
        {

            $fieldData['title'] = date("H:i", $this->{$this->timestamp_begin});

        }
        else
        {

            $fieldData['title'] = @date("H:i", $this->{$this->timestamp_begin}) . " " . PerisianLanguageVariable::getVariable(10785) . " " . @date("H:i", $this->{$this->timestamp_end});

            if(@date("Hi", $this->{$this->timestamp_begin}) == "0000" && @date("Hi", $this->{$this->timestamp_end}) == "2359")
            {

                // "Whole day"
                $fieldData['title'] = PerisianLanguageVariable::getVariable('10772');

            }

        }

        $fieldData['content'] = implode("<br>", $htmlContentArray);

        return $fieldData;
        
    }
    
    /**
     * Sets a flag that disables or enables the sending of notifications when saving this entry.
     * 
     * @author Peter Hamm
     * @param bool $enabled
     * @return void
     */
    public function setSendNotifications($enabled)
    {
        
        $this->sendNotifications = $enabled;
        
    }
    
    /**
     * Retrieves the flag that says whether to disable or enable the sending of notifications when saving this entry.
     * 
     * @author Peter Hamm
     * @return bool $enabled
     */
    public function getSendNotifications()
    {
        
        return $this->sendNotifications;
        
    }
    
    /**
     * Retrieves the ID(s) of entries withing the same series (group) as the current entry.
     * Returns either a single ID or an array of IDs, depending on the $selector.
     * 
     * @author Peter Hamm
     * @param String $selector Optional. Can be "next", "previous" or "all". First two return a single identifier. Default: "all".
     * @param int $groupId Optional, leave it empty to use the group of the current entry. Default: 0 (empty)
     * @return mixed String, Array of strings or null.
     */
    public function getEntryIdFromSeries($selector = "all", $groupId = 0)
    {
                
        $groupId = $groupId > 0 ? $groupId : $this->{$this->field_fk};
        
        if(strlen($groupId) == 0 || $groupId <= 0)
        {
            
            return null;
            
        }
        
        $returnValue = null;
        
        $query = "{$this->field_fk} = '{$groupId}' AND {$this->field_pk} != '{$this->{$this->field_pk}}'";
        
        if($selector == "next" || $selector == "previous")
        {
            
            $comparisonOperator = ($selector == "next" ? ">" : "<");
            
            $query .= " AND UNIX_TIMESTAMP({$this->field_date_begin}) {$comparisonOperator} {$this->{$this->timestamp_begin}}";
            
        }
                
        $results = $this->getData($query, $this->field_date_begin, ($selector == "next" ? "ASC" : "DESC"));
                
        if($selector != "next" && $selector != "previous")
        {
            
            $returnValue = array();
            
            for($i = 0; $i < count($results); ++$i)
            {
                
                array_push($returnValue, $results[$i][$this->field_pk]);
                
            }
            
        }
        else
        {
            
            $returnValue = count($results) == 0 ? null : $results[0][$this->field_pk];
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Checks whether the default ("unassigned") user is visible in the list of selectable users or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isDefaultCalendarUserVisible()
    {
        
        $calendarUserList = CalsyCalendar::getDisplayableUserList(null, null, true);
        $userDummy = new CalsyUserBackend();

        foreach($calendarUserList as $calendarUser)
        {

            if($calendarUser[$userDummy->field_pk] == CalsyUserBackend::DEFAULT_USER_ID)
            {

                return true;

            }

        }
        
        return false;
    
    }

    /**
     * Outputs a list of calendar entries within the two specified timestamps
     *
     * @author Peter Hamm
     * @date 2015-01-19
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @param String $order Optional: Either "ASC" for ascending or "DESC" for descending order, default: ascending.
     * @param int $offset Optional, an SQL offset. Default: No offset.
     * @param int $limit Optional, an SQL limit. Default: No limit.
     * @return Array A list of countries
     */
    static public function getEntriesBetween($timestampBegin, $timestampEnd, $filter, $order = "ASC", $offset = -1, $limit = -1)
    {
        
        global $user, $userFrontend;

        PerisianFrameworkToolbox::security($timestampBegin);
        PerisianFrameworkToolbox::security($timestampEnd);
        PerisianFrameworkToolbox::security($order);
        PerisianFrameworkToolbox::security($offset);
        PerisianFrameworkToolbox::security($limit);
        
        // Backend user filter 
        {

            if($filter->hasFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME, '1'))
            {

                $filterUsers = $filter->getUserBackendIdentifiers();

            }
            else
            {

                $filterUsers = $filter->getUserBackendIdentifiers();
                
                if($filterUsers == "-1" || strlen($filterUsers) == 0)
                {

                    $filterUsers = (@$user ? $user->{$user->field_pk} : "-1");

                }

            }
            
        }
        
        // Frontend user filter
        {
            
            $filterUserFrontend = $filter->getUserFrontendIdentifiers();

            if($filterUserFrontend == "-1" || strlen($filterUserFrontend) == 0)
            {

                $filterUserFrontend = (@$userFrontend ? @$userFrontend->{@$userFrontend->field_pk} : "-1");

            }
                        
        }
        
        $filterResources = $filter->getResourceIdentifiers();
        $filterShowHolidays = $filter->getShowHolidays();
        $filterShowSurveys = $filter->getShowSurveys();
        $filterShowUsersAsBlocked = $filter->getShowUsersAsBlocked();
                
        $instance = self::getInstance();
        $result = Array();
        
        $timestampBegin = $timestampBegin < 0 ? 0 : $timestampBegin;
        $timestampEnd = $timestampEnd < 0 ? 8640000000000000 : $timestampEnd; // Infinity or the actual value
        
        $selectFieldArray = $instance->getFieldVars();
        
        // Query building
        {
            
            $query = "WHERE ";
            
            // Simple filters
            {
                
                // Begin and/or end between the date cursor

                $sqlTimestampBegin = PerisianFrameworkToolbox::sqlTimestamp($timestampBegin);
                $sqlTimestampEnd = PerisianFrameworkToolbox::sqlTimestamp($timestampEnd);
                
                $query .= "(({$instance->field_date_begin} > " . $sqlTimestampBegin . " AND {$instance->field_date_begin} < " . $sqlTimestampEnd . ")";
                $query .= " OR ({$instance->field_date_end} > " . $sqlTimestampBegin . " AND {$instance->field_date_end} < " . $sqlTimestampEnd . "))";
                
                if($filterUserFrontend != "-1" && strlen($filterUserFrontend) > 0)
                {

                    $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder();

                    $query = "LEFT JOIN {$userFrontendOrderObject->table} ON ({$instance->field_parent_entry_id} IS NOT NULL AND {$userFrontendOrderObject->field_fk} = {$instance->field_parent_entry_id} OR {$userFrontendOrderObject->field_fk} = {$instance->field_pk}) " . $query;

                    $query .= " AND {$userFrontendOrderObject->field_user_frontend_id} IN (" . $filterUserFrontend . ")";

                    array_push($selectFieldArray, $userFrontendOrderObject->field_user_frontend_id);

                }

                if(CalsyResourceModule::isEnabled() && $filterResources != "-1" && strlen($filterResources) > 0)
                {

                    $query .= " AND {$instance->field_fk_secondary} IN (" . $filterResources . ")";

                }

                if($filterUsers != "-1" && strlen($filterUsers) > 0)
                {

                    $query .= ($filterShowUsersAsBlocked ? " OR " : " AND ") . "({$instance->field_user_id} IN (" . $filterUsers . ")";
                    
                    // If the default user is part of the selectable user, only show unassigned entries when it's explicitly demanded
                    $defaultUserIsInList = static::isDefaultCalendarUserVisible();
                                        
                    if(!$defaultUserIsInList || in_array(CalsyUserBackend::DEFAULT_USER_ID, explode(',', $filterUsers)))
                    {
                        
                        $query .= " OR {$instance->field_user_id} IS NULL)";
                        
                    }
                    else 
                    {
                        
                        $query .= ")";
                        
                    }

                }
                
                if($filterShowHolidays)
                {

                    $query .= " OR ({$instance->field_is_holiday} = 1 AND {$instance->field_user_id} IS NULL)";

                }
                
                if($filterShowSurveys)
                {

                    $query .= " OR ({$instance->field_is_survey} = 1)";

                }

                $fields = implode(',', $selectFieldArray);
                
            }
            
            // Building the final query
            {
                
                // Load the related flags
                {
                    
                    $finalQuery = ", " . CalsyCalendarEntryFlag::getFlagSubSelectQueryString();
                    
                }
                
                $finalQuery .= "FROM {$instance->table} " . $query;
                $finalQuery .= " GROUP BY {$instance->field_pk}";
                $finalQuery .= " ORDER BY {$instance->field_date_begin} {$order}";

                if($offset >= 0 && $limit >= 0)
                {

                    $finalQuery .= " LIMIT {$limit} OFFSET {$offset}";

                }
                
                // Flag filtering
                {

                    $finalQuery = CalsyCalendarEntryFlag::addFlagFilterToQueryString($finalQuery, $filter->getFlags());

                }
                
                $finalQuery = "SELECT {$fields} " . $finalQuery;
                                                                                      
            }
                        
        }
        
        // Retrieve, format and further filter the results
        {
            
            $results = $instance->getDatabaseConnection()->select($finalQuery);
                        
            $results = static::filterEntries($results, $user, $userFrontend);
            
            $returnValue = $instance->enrichResults($results);
        
        }
                
        return $returnValue;

    }
    
    /**
     * Retrieves a list of all entries associated with the specified $userIdentifier.
     * 
     * @author Peter Hamm
     * @param int $userIdentifier
     * @return Array
     */
    static public function getEntriesForUserId($userIdentifier)
    {
        
        $userIdentifier = (int)$userIdentifier;
        
        $object = static::getInstance();
        
        $data = $object->getData($object->field_user_id . ' = "' . $userIdentifier . '"');
        
        return $data;
        
    }
                
    /**
     * Retrieves whether this entry has the specified flag or not.
     * 
     * @author Peter Hamm
     * @param String $flagName
     * @return bool
     */
    public function hasFlag($flagName)
    {
        
        $hasFlag = CalsyCalendarEntryFlag::entryHasFlag($this->{$this->field_pk}, $flagName);
        
        return $hasFlag;
        
    }
             
    /**
     * Retrieves the value for the flag with the specified name.
     * 
     * @author Peter Hamm
     * @param String $flagName
     * @return mixed
     */
    public function getFlagValue($flagName)
    {
        
        $value = CalsyCalendarEntryFlag::getFlagValueForEntry($this->{$this->field_pk}, $flagName);
        
        return $value;
        
    }
    
    private static function filterEntries($entries, $userBackend, $userFrontend)
    {
                      
        $instance = static::getInstance();
        
        for($i = 0; $i < count($entries); ++ $i)
        {
            
            if(isset($userBackend))
            {
                
                // Check if this entry is a process. Then check the requirements for it... if not met, kick it out.
                $isProcess = CalsyCalendarEntryFlag::entryHasFlag($entries[$i][$instance->field_pk], CalsyProcessEngineModule::FLAG_PROCESS_TO_START);
                
                if($isProcess && (!CalsyProcessEngineModule::isEnabled() || !CalsyProcessEngineModule::userCanManageProcesses($userBackend)))
                {
                    
                    unset($entries[$i]);
                    
                    sort($entries);
                    
                    return static::filterEntries($entries, $userBackend, $userFrontend);
                    
                }
                
            }
            
        }
        
        return $entries;
        
    }
    
    /**
     * Retrieves data from the concrete class' specified $this->table.
     * 
     * @param Array
     * @return Array An associative array with the resulting entries
     */
    public function enrichResults($results)
    {
        
        for($i = 0; $i < count(@$results); ++$i)
        {
            
            $results[$i][$this->timestamp_begin] = strtotime($results[$i][$this->field_date_begin]);
            $results[$i][$this->timestamp_end] = strtotime($results[$i][$this->field_date_end]);
            
            if(CalsyCalendarEntryFlag::entryHasFlag($results[$i][$this->field_pk], CalsyWorkPlan::FLAG_WORK_PLAN_ID))
            {
                
                $results[$i][$this->field_user_id] = '-10';
                
            }
            else
            {
                
                if(strlen($results[$i][$this->field_user_id]) == 0)
                {

                    // If the default user is part of the selectable users liost, and there's no other user set as an owner of this entry, assign it to the virtual default user
                    $defaultUserIsInList = static::isDefaultCalendarUserVisible();
                
                    if($defaultUserIsInList)
                    {
                        
                        $results[$i][$this->field_user_id] = CalsyUserBackend::DEFAULT_USER_ID;
                        
                    }
                                    
                }
            
            }
            
        }
                
        return $results;
        
    }
    
    /**
     * Retrieves data from the concrete class' specified $this->table.
     * 
     * @param String $where Optional: WHERE clause, without the keyword 'WHERE', default: empty
     * @param String $sortyBy Optional: The key to sort by, default: primary key
     * @param String $order Optional: Sort results DESC or ASC, default: DESC
     * @param integer $limit Optional: Limits the output to $limit entries, default: no limit (0)
     * @param Array $fieldExcludes Optional: An array containing names of fields that shall not be retrieved, default: empty
     * @param Array $fieldList Optional, the opposite of $fieldExludes - The result will contain only the specified fields, default: empty
     * @return Array An associative array with the resulting entries
     */
    public function getData($where = '', $sortBy = 'pk', $order = 'DESC', $start = 0, $limit = 0, $fieldExcludes = Array(), $fieldList = Array())
    {
        
        $results = parent::getData($where, $sortBy, $order, $start, $limit, $fieldExcludes, $fieldList);
                
        return $this->enrichResults($results);
        
    }
    
    /**
     * Sets the begin date from the specified timestamp
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return void
     */
    public function setTimestampBegin($timestamp)
    {
        
        if((int)$timestamp > 0)
        {
            
            $beginDate = PerisianFrameworkToolbox::sqlTimestamp($timestamp);
                        
            $this->{$this->timestamp_begin} = $timestamp;
            $this->{$this->field_date_begin} = $beginDate;
            
        }
        
    }
    
    /**
     * Sets the end date from the specified timestamp
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return void
     */
    public function setTimestampEnd($timestamp)
    {
        
        if((int)$timestamp > 0)
        {
                        
            $endDate = PerisianFrameworkToolbox::sqlTimestamp($timestamp);
            
            $this->{$this->timestamp_end} = $timestamp;
            $this->{$this->field_date_end} = $endDate;
            
        }
        
    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);
            
            $this->countChildren = $this->getChildrenCount();

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves a string containing all names of all frontend users for this entry.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getUserFrontendListAsString()
    {
        
        $userFrontendList = "";
        $userFrontend = $this->getUserFrontendWithOrders();
                     
        for($i = 0; $i < count($userFrontend); ++$i)
        {
            
            if(!is_object(@$userFrontend[$i]["user_frontend"]))
            {
                
                continue;
                
            }
            
            if($i > 0)
            {

                $userFrontendList .= ", ";

            }

            $userFrontendList .= $userFrontend[$i]["user_frontend"]->getFullName();

        }
        
        return $userFrontendList;
        
    } 
    
    /**
     * Retrieves if this calendar entry contains the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return void
     */
    public function containsUserFrontend($userFrontendId)
    {
        
        $userFrontendList = $this->getUserFrontendWithOrders();
        
        for($i = 0; $i < count($userFrontendList); ++$i)
        {
            
            if($userFrontendList[$i]['user_frontend']->{$userFrontendList[$i]['user_frontend']->field_pk} == $userFrontendId)
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Checks if the entry with the specified identifier is confirmed for any frontend user or not.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return bool
     */
    public static function isConfirmedForAnyUserFrontend($entryId)
    {
     
        return self::isConfirmationStatusForAnyUserFrontend($entryId, 'confirmed');
        
    }
    
    /**
     * Checks if the entry with the specified identifier is unconfirmed for any frontend user or not.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return bool
     */
    public static function isUnconfirmedForAnyUserFrontend($entryId)
    {
     
        return self::isConfirmationStatusForAnyUserFrontend($entryId, 'confirmed', true);
        
    }
    
    /**
     * Checks if the entry with the specified identifier is canceled for any frontend user or not.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return bool
     */
    public static function isCanceledForAnyUserFrontend($entryId)
    {
     
        return self::isConfirmationStatusForAnyUserFrontend($entryId, 'canceled');
        
    }
    
    /**
     * Checks if the entry with the specified identifier has frontend users, but is yet unconfirmed.
     * 
     * @author Peter Hamm
     * @param uint $entryId
     * @return boolean
     */
    public static function hasUserFrontendButIsUnconfirmed($entryId)
    {
        
        $hasUserFrontend = !static::hasNoUserFrontend($entryId);
        $isConfirmedForAnyUserFrontend = static::isConfirmedForAnyUserFrontend($entryId) || static::isCanceledForAnyUserFrontend($entryId);
        
        return $hasUserFrontend && !$isConfirmedForAnyUserFrontend;
        
    }
    
    /**
     * Retrieves if this entry has no frontend users at all.
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return boolean
     */
    public static function hasNoUserFrontend($entryId)
    {
        
        $dummy = new CalsyCalendarEntryUserFrontendOrder();
        
        $userFrontendList = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForCalendarEntry($entryId);
        
        if(count($userFrontendList) == 0)
        {
            
            return true;
            
        }
        
        $userFrontendNames = array();
        
        for($i = 0; $i < count($userFrontendList); ++$i)
        {
            
            if(strlen($userFrontendList[$i][$dummy->field_user_frontend_id]) > 0 && $userFrontendList[$i][$dummy->field_user_frontend_id] > 0)
            {
                
                return false;
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Checks the entry with the specified identifier for confirmation statuses
     * @param int $entryId
     * @param String $confirmationStatus E.g. "confirmed", "none" or "unconfirmed"
     * @param bool $negative Optional: Perform a negative (/"not") select? Default: false.
     * @return boolean
     */
    protected static function isConfirmationStatusForAnyUserFrontend($entryId, $confirmationStatus, $negative = false)
    {
        
        $dummy = new CalsyCalendarEntryUserFrontendOrder();
        
        $confirmationList = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForCalendarEntry($entryId);
        
        for($i = 0; $i < count($confirmationList); ++$i)
        {
            
            if(!$negative && $confirmationList[$i][$dummy->field_status_confirmation] == $confirmationStatus)
            {
                
                return true;
                
            }
            else if($negative && $confirmationList[$i][$dummy->field_status_confirmation] != $confirmationStatus)
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Checks if this calendar entry is canceled for the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return bool
     */
    public function isCanceledForUserFrontend($userFrontendId)
    {
        
        $status = $this->getConfirmationStatusForUserFrontend($userFrontendId);
        
        return $status == 'canceled';
        
    }
    
    /**
     * Checks if this calendar entry is confirmed for the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return bool
     */
    public function isConfirmedForUserFrontend($userFrontendId)
    {
        
        $status = $this->getConfirmationStatusForUserFrontend($userFrontendId);
        
        return $status == 'confirmed';
        
    }
    
    /**
     * Confirms the calendar entry for the specified frontend user identifier after
     * he sent a payment.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return bool Operation successful or not?
     */
    public function confirmForUserFrontendWithPayment($userFrontendId)
    {
        
        $this->setIsPaymentRequired(true);
            
        $this->confirmForUserFrontend($userFrontendId);
        
        
    }
    
    /**
     * Confirms the calendar entry for the specified frontend user identifier
     * (if the frontend user is already a participant). 
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return bool Operation successful or not?
     */
    public function confirmForUserFrontend($userFrontendId)
    {
        
        PerisianFrameworkToolbox::security($userFrontendId);
                
        try
        {
            
            $userFrontendObject = new CalsyUserFrontend($userFrontendId);
        
            $userFrontendOrderData = $this->getUserFrontendWithOrders($userFrontendId);
            
            if($userFrontendOrderData['object']->{$userFrontendOrderData['object']->field_status_confirmation} == 'open')
            {

                $userFrontendOrderData['object']->{$userFrontendOrderData['object']->field_status_confirmation} = 'confirmed';
                $userFrontendOrderData['object']->{$userFrontendOrderData['object']->field_status_confirmation_message} .= PerisianLanguageVariable::getVariable('p59f67cf908ed5');

                $userFrontendOrderData['object']->save();

                try
                {
                    
                    $userFrontendObject->sendEmailCalendarEntryConfirmed($this, false);
                
                }
                catch(PerisianException $e)
                {
                    
                }
            
            }
            
            if($this->{$this->field_user_id} > 0)
            {
                
                $ownerUser = new CalsyUserBackend($this->{$this->field_user_id});

                try
                {
                    
                    $ownerUser->sendEmailCalendarEntryPaymentReceived($this, $ownerUser, $userFrontendObject, false);
                
                }
                catch(PerisianException $e)
                {
                    
                }

            }
            
        }
        catch(PerisianException $e)
        {
                                
            return false;
            
        }
        
        return true;
       
    }
    
    /**
     * Gets the confirmation status for this calendar entry 
     * and the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return string Either 'none', 'confirmed' or 'open'
     */
    public function getConfirmationStatusForUserFrontend($userFrontendId)
    {
        
        $userFrontendList = $this->getUserFrontendWithOrders();
        
        for($i = 0; $i < count($userFrontendList); ++$i)
        {
            
            if($userFrontendList[$i]['user_frontend']->{$userFrontendList[$i]['user_frontend']->field_pk} == $userFrontendId)
            {
                
                return $userFrontendList[$i]['object']->{$userFrontendList[$i]['object']->field_status_confirmation};
                
            }
            
        }
        
        return 'none';
        
    }
    
    /**
     * Retrieves a formatted, human readable time string
     * of the beginning and end of this entry.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getFormattedTimeString()
    {
                
        $timestampBegin = $this->{$this->timestamp_begin};
        $timestampEnd = $this->{$this->timestamp_end};
        
        $formattedString = static::getFormattedEventTimeString($timestampBegin, $timestampEnd);
                
        return $formattedString;
        
    }
    
    /**
     * Gets a list of tags for the specified entry identifier.
     * 
     * @author Peter Hamm
     * @param int $eventId
     * @return array
     */
    public static function getTagsForEntry($eventId)
    {
        
        $eventId = static::getActualEntryIdentifier($eventId);
        
        $event = new static($eventId);
        
        $isUnconfirmedForAnyUserFrontend = CalsyCalendarEntry::isUnconfirmedForAnyUserFrontend($eventId);
        $isCanceledForAnyUserFrontend = CalsyCalendarEntry::isCanceledForAnyUserFrontend($eventId);
        $hasChildren = $event->hasChildren();
        
        $hasNoUserFrontend = CalsyCalendarEntry::hasNoUserFrontend($eventId);
        
        $isSpecialHomeVisit = !CalsyResourceModule::isEnabled() ? false : CalsyResource::isSpecialItem("home_visit", $event->{$event->field_fk_secondary});
        
        $isWorkPlan = CalsyCalendarEntryFlag::entryHasFlag($eventId, CalsyWorkPlan::FLAG_WORK_PLAN_ID);
        $isWorkPlanEntry = CalsyCalendarEntryFlag::entryHasFlag($eventId, CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID);
        
        $isSurvey = CalsyCalendarEntryFlag::entryHasFlag($eventId, CalsyCalendarEntrySurvey::FLAG_SURVEY_DATA_USERS);
        
        $tags = array();
        
        if($isWorkPlan || $isWorkPlanEntry)
        {

            $workPlanIdentifier = CalsyCalendarEntryFlag::getFlagValueForEntry($eventId, $isWorkPlan ? CalsyWorkPlan::FLAG_WORK_PLAN_ID : CalsyWorkPlan::FLAG_ENTRY_WORK_PLAN_ID);
            $workPlanName = CalsyWorkPlan::getTitleForIdentifier($workPlanIdentifier);

            $workPlanInfo = PerisianLanguageVariable::getVariable('p5a340dbeaf5af') . ": " . $workPlanName;

            // "Work plan"
            
            $tag = array(

                "type" => "warning",
                "text" => $workPlanName,
                "key" => "work-plan"

            );

            array_push($tags, $tag);

        }
        
        if($isSurvey)
        {
            
            $tag = array(

                "type" => "info",
                "text" => lg('p5e4ae1f968fee'),
                "key" => "survey"

            );

            array_push($tags, $tag);
            
        }
        
        if($hasChildren)
        {
            
            // "Split time"
            
            $tag = array(

                "type" => "info",
                "text" => PerisianLanguageVariable::getVariable('p58ffc956964be'),
                "key" => "info-split-time"

            );

            array_push($tags, $tag);
           
        }
        
        if(!$isSurvey && !$isWorkPlan && CalsyUserFrontendModule::isEnabled() && !CalsyBookableTimesModule::isEntryBookableTime($eventId))
        {
            
            if($hasNoUserFrontend)
            {
                
                // "No frontend user"

                $tag = array(

                    "type" => "warning",
                    "text" => PerisianLanguageVariable::getVariable(10976),
                    "key" => "warning-no-user-frontend"

                );

                array_push($tags, $tag);

            }
            else if($isCanceledForAnyUserFrontend && !$hasNoUserFrontend)
            {
                
                // "Canceled"
                
                $tag = array(

                    "type" => "warning",
                    "text" => PerisianLanguageVariable::getVariable(11160),
                    "key" => "warning-canceled"

                );

                array_push($tags, $tag);
                
            }
            else if($isUnconfirmedForAnyUserFrontend && !$hasNoUserFrontend)
            {
                
                // "Unconfirmed"

                $tag = array(

                    "type" => "danger",
                    "text" => PerisianLanguageVariable::getVariable(10940),
                    "key" => "danger-unconfirmed"

                );

                array_push($tags, $tag);

            }
            
            // Payment status
            if(CalsyAreaPrice::isPaymentActivated())
            {

                $userFrontendOrderList = $event->getUserFrontendWithOrders();

                $areaId = 0;

                if(count($userFrontendOrderList) > 0)
                {

                    $areaId = @$userFrontendOrderList[0]['area']->{$userFrontendOrderList[0]['area']->field_pk};
                    $userFrontendId = is_object($userFrontendOrderList[0]['user_frontend']) ? $userFrontendOrderList[0]['user_frontend']->{$userFrontendOrderList[0]['user_frontend']->field_pk} : 0;

                }

                if($areaId > 0 && CalsyAreaPrice::isPaymentRequiredForArea($areaId))
                {

                    $transaction = CalsyAreaPrice::getPaymentTransaction($event->{$event->field_pk}, $userFrontendId);

                    if($transaction != null)
                    {

                        if(!$transaction->isPaymentReceived())
                        {
                            
                            $tag = array(

                                "type" => "danger",
                                "text" => PerisianLanguageVariable::getVariable('p5a31c84f0b4f7'),
                                "key" => "danger-unpaid"

                            );
                            
                            array_push($tags, $tag);

                        }
                        else
                        {
                            
                            $tag = array(

                                "type" => "success",
                                "text" => PerisianLanguageVariable::getVariable('10004'),
                                "key" => "success-paid"

                            );
                            
                            array_push($tags, $tag);

                        }

                    }

                }

            }
            
        }
        
        if(CalsyResourceModule::isEnabled() && $isSpecialHomeVisit)
        {
            
            // "Home visit"
            
            $tag = array(
                
                "type" => "info",
                "text" => PerisianLanguageVariable::getVariable(10926),
                "key" => "info-home-visit"
                
            );
            
            array_push($tags, $tag);
            
        }
        
        return $tags;
        
    }
    
    /**
     * Retrieves the count of unique backend users associated with this entry, including the children.
     * If this is a child entry itself, all siblings and the parents are respected
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getUserBackendCount()
    {
        
        $userList = $this->getUserBackendList();
        
        return count($userList);
        
    }
    
    /**
     * Retrieves a list of unique backend user names associated with this entry, including the children.
     * If this is a child entry itself, all siblings and the parents are respected
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getUserBackendListAsString()
    {
        
        $userList = $this->getUserBackendList();
        $nameList = Array();
        
        for($i = 0; $i < count($userList); ++$i)
        {
            
            array_push($userList, $userList[$i]->{$userList[$i]->field_fullname});
        
        }
    
        return implode(", ", $nameList);
        
    }
    
    /**
     * Retrieves a list of all unique backend users associated with this entry, including the children.
     * If this is a child entry itself, all siblings and the parents are respected
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getUserBackendList()
    {
        
        $userList = Array();
        $userIdList = Array();
                
        $mainEntryId = $this->{$this->field_parent_entry_id} > 0 ? $this->{$this->field_parent_entry_id} : $this->{$this->field_pk};
        $mainEntry = new static($mainEntryId);
        
        array_push($userList, new CalsyUserBackend($mainEntry->{$mainEntry->field_user_id}));
        array_push($userIdList, $mainEntry->{$mainEntry->field_user_id});
        
        $childrenList = $mainEntry->getChildren();
             
        for($i = 0; $i < count($childrenList); ++$i)
        {
            
            if($childrenList[$i]->{$childrenList[$i]->field_user_id} > 0 && !in_array($childrenList[$i]->{$childrenList[$i]->field_user_id}, $userIdList))
            {
                
                array_push($userList, new CalsyUserBackend($childrenList[$i]->{$childrenList[$i]->field_user_id}));
                array_push($userIdList, $childrenList[$i]->{$childrenList[$i]->field_user_id});
                            
            }

        }
        
        return $userList;
        
    }
    
    /**
     * Retrieves a displayable HTML string containing all of the entry's tags.
     * 
     * @author Peter Hamm
     * @param int $eventId
     * @return String
     */
    public static function getTagsForEntryAsHtml($eventId)
    {
        
        $tagList = Array();
        
        try
        {
            
            $tagList = static::getTagsForEntry($eventId);
            
        }
        catch(PerisianException $e)
        {
            
        }
        
        $tagHtmlArray = array();
        
        for($j = 0; $j < count($tagList); ++$j)
        {
            
            $content = $tagList[$j]["text"];
            
            if(isset($tagList[$j]["key"]))
            {
                
                if($tagList[$j]["key"] == "info-split-time")
                {
                    
                    $content = '<i class="md md-call-split"></i> ' . $content;
                
                }
                
                if($tagList[$j]["key"] == "work-plan")
                {
                    
                    $content = '<i class="md md-work"></i> ' . $content;
                
                }
                
                if($tagList[$j]["key"] == "survey")
                {
                    
                    $content = '<i class="md md-poll"></i> ' . $content;
                
                }
                
            }
            
            array_push($tagHtmlArray, '<span class="alert alert-' . $tagList[$j]["type"] . ' alert-inline">' . $content . '</span>');
        
        }
        
        return (count($tagHtmlArray) > 0 ? "&nbsp;" : "") . implode("&nbsp;", $tagHtmlArray);
        
    }
    
    /**
     * Retrieves whether this entry has children or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function hasChildren()
    {
        
        $childrenCount = $this->getChildrenCount();
        
        return $childrenCount > 0;
        
    }
    
    /**
     * Retrieves how many children this calendar entry has.
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getChildrenCount()
    {
                
        if(strlen($this->{$this->field_pk}) == 0 || $this->{$this->field_pk} <= 0)
        {
            
            return 0;
            
        }
    
        $query = $this->field_parent_entry_id . ' = "' . $this->{$this->field_pk} . '"';
        
        $this->countChildren = $this->getCount($query);
        
        return $this->countChildren;
    
    }
    
    /**
     * Retrieves a list of objects of all children of this entry.
     * 
     * @author Peter Hamm
     * @return array An array of calendar entry objects.
     */
    public function getChildren()
    {
        
        $childrenList = Array();
        
        if(strlen($this->{$this->field_pk}) == 0 || $this->{$this->field_pk} <= 0)
        {
            
            return $childrenList;
            
        }
    
        $query = $this->field_parent_entry_id . ' = "' . $this->{$this->field_pk} . '"';
        $result = $this->getData($query);
        
        for($i = 0; $i < count($result); ++$i)
        {
                        
            $childEntry = new static($result[$i][$this->field_pk]);
            
            $childEntry->removeTimeDurationTitle();
            
            array_push($childrenList, $childEntry);
            
        }
        
        return $childrenList;
    
    }
    
    /**
     * Retrieves the actual identifier of the entry with the specified identifier..
     * That means: If the entry has a parent entry identifier set, that identifier is returned.
     * If not, the entry's own identifier is returned. 
     * 
     * @author Peter Hamm
     * @param int $eventId
     * @return int
     */
    public static function getActualEntryIdentifier($eventId)
    {
        
        $element = new static($eventId);
        
        $actualEntryIdentifier = strlen($element->{$element->field_parent_entry_id}) > 0 && $element->{$element->field_parent_entry_id} != '0' ? $element->{$element->field_parent_entry_id} : $element->{$element->field_pk};
     
        return $actualEntryIdentifier;
        
    }
    
    /**
     * Retrieves the frontend user(s) related to this entry.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId Optional, if set > 0 it returns only the entry for the specified frontend user. Default: 0
     * @return array A list of CalsyUserFrontend objects
     */
    public function getUserFrontendWithOrders($userFrontendId = 0)
    {
                
        $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder();
        
        $actualEntryIdentifier = static::getActualEntryIdentifier($this->{$this->field_pk});
        
        $entryList = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForCalendarEntry($actualEntryIdentifier);
        
        $objectList = array();
                
        for($i = 0; $i < count($entryList); ++$i)
        {
            
            if(isset($userFrontendId) && $userFrontendId > 0 && $userFrontendId != $entryList[$i][$userFrontendOrderObject->field_user_frontend_id])
            {
                                
                continue;
                
            }
            
            try
            {
               
                $elementUserFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder($entryList[$i][$userFrontendOrderObject->field_pk]);
                
            }
            catch(Exception $e)
            {
                
                $elementUserFrontendOrderObject = null;
                
            }
            
            try
            {
               
                $elementUserFrontendObject = new CalsyUserFrontend($entryList[$i][$userFrontendOrderObject->field_user_frontend_id]);
                
            }
            catch(Exception $e)
            {
                
                $elementUserFrontendObject = null;
            
            }
            
            try
            {
               
                $elementOrderObject = new CalsyOrder($entryList[$i][$userFrontendOrderObject->field_order_id]);
                
            }
            catch(Exception $e)
            {
                
                $elementOrderObject = null;
                
            }
            
            try
            {
               
                $elementAreaObject = new CalsyArea($entryList[$i][$userFrontendOrderObject->field_area_id]);
                
            }
            catch(Exception $e)
            {
                
                $elementAreaObject = null;
                
            }
            
            $element = array(
                
                'id' => $entryList[$i][$userFrontendOrderObject->field_pk],
                'object' => @$elementUserFrontendOrderObject,
                'user_frontend' => @$elementUserFrontendObject,
                'order' => @$elementOrderObject,
                'area' => @$elementAreaObject,
                'info_additional' => $entryList[$i][$userFrontendOrderObject->field_info_additional],
                'message_to_client' => $entryList[$i][$userFrontendOrderObject->field_message_to_client]
                
            );
            
            array_push($objectList, $element);
            
        }
        
        if($userFrontendId > 0 && count($objectList) == 1)
        {
            
            return $objectList[0];
            
        }
        
        return $objectList;
        
    }
    
    /**
     * Deletes the group/series with the specified identifier.
     * Leave the $groupId unspecified or 0 to use the current entry's series.
     * 
     * @author Peter Hamm
     * @param int $groupId Optional, default: 0
     * @return boolean
     */
    public function deleteGroup($groupId = 0)
    {
        
        PerisianFrameworkToolbox::security($groupId);

        if(empty($groupId) || $groupId == 0)
        {
            
            $groupId = $this->{$this->field_fk};
            
        }
        
        if(empty($groupId) || $groupId == 0)
        {
            
            return false;
            
        }
        
        $deleteIds = $this->getEntryIdFromSeries('all', $groupId);
        
        for($i = 0; $i < count($deleteIds); ++$i)
        {
            
            $deleteObject = new static($deleteIds[$i]);
            $deleteObject->deleteEntry();
            
        }
        
    }
    
    /**
     * Deletes an entry by ID
     * 
     * @param int $entryId Optional: An entry's ID, default: ID of the object
     * @return void
     */
    public function deleteEntry($entryId = 0)
    {
  
        PerisianFrameworkToolbox::security($entryId);

        if(empty($entryId) || $entryId == 0)
        {
            
            $entryId = $this->{$this->field_pk};
            $deleteObject = $this;
            
        }
        
        if(!isset($deleteObject))
        {
            
            $deleteObject = new self($entryId);
            
        }
            
        $query = "{$this->field_pk} = {$entryId} OR ";
        $query .= "{$this->field_parent_entry_id} = {$entryId}";
        
        $this->delete($query);
        
        CalsyCalendarEntryFlag::deleteFlagsForEntry($entryId);
        
        CalsyCalendarEntryUserFrontendOrder::deleteEntriesForCalendarEntry($entryId);
        CalsyCalendarEntryGroup::deleteGroupIfEmpty($deleteObject->{$deleteObject->field_fk});
        
        CalsyCalendarEntryNotificationRelationUser::deleteNotificationSettingsForCalendarEntry($entryId);
        
        return;

    }
    
    /**
     * Deletes entries by parent ID
     * 
     * @param int $entryId Optional: An entry's ID, default: ID of the object
     * @return void
     */
    protected function deleteEntriesWithParentId($entryId = 0)
    {
  
        PerisianFrameworkToolbox::security($entryId);

        if(empty($entryId) || $entryId == 0)
        {
            
            $entryId = $this->{$this->field_pk};
            $deleteObject = $this;
            
        }
        
        if(!isset($deleteObject))
        {
            
            $deleteObject = new self($entryId);
            
        }
        
        $query = "{$this->field_parent_entry_id} = {$entryId}";
        
        $this->delete($query);
                
        return;

    }
    
    /**
     * Retrieves the count of entries within the specified group.
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return int
     */
    public static function getCountForGroupId($groupId)
    {
        
        PerisianFrameworkToolbox::security($groupId);
        
        if($groupId > 0)
        {
            
            $object = self::getInstance();
            
            $query = $object->field_fk . " = {$groupId}";
                    
            $count = $object->getCount($query);
            
            return $count;
            
        }
        
        return 0;
        
    }
    
    /**
     * Retrieves the count of entries for the specified frontend user
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return int
     */
    public static function getCountForUserFrontendId($userFrontendId)
    {
        
        return CalsyCalendarEntryUserFrontendOrder::getCountForUserFrontendId($userFrontendId);
        
    }
    
    /**
     * Retrieves the list of fields and their values, as an associative array
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getFieldValues()
    {
        
        $fieldsAndValues = parent::getFieldValues();
        
        $fieldsAndValues[$this->timestamp_begin] = @$this->{$this->timestamp_begin};
        $fieldsAndValues[$this->timestamp_end] = @$this->{$this->timestamp_end};
        
        return $fieldsAndValues;
        
    }
        
    /**
     * Retrieves if the timespan with the specified $timestampBegin and $timestampEnd are within a holiday.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @return bool
     */
    public static function isTimespanOnHoliday($timestampBegin, $timestampEnd)
    {
        
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->setShowHolidays(true);
            $filter->setIgnoreConfirmationStatus(true);
            $filter->setIgnoreTimespanEdges(true);
            
        }
                
        $query = "SELECT COUNT(*) as count " . static::getQueryForFilter($filter, $timestampBegin, $timestampEnd);
                
        $instance = static::getInstance();
        
        $database = $instance->getDatabaseConnection();
        $result = $database->select($query);
        
        $count = is_array($result) && @$result[0]['count'] ? $result[0]['count'] : 0;
        
        return $count > 0;
        
    }
    
    /**
     * Generates a query to select calendar entries by the specified $filter (from self::createFilter())
     * 
     * @author Peter Hamm
     * @param CalsyCalendarFilter $filter
     * @param int $timestampFrom
     * @param int $timestampTo
     * @return string
     * @throws PerisianException
     */
    public static function getQueryForFilter($filter, $timestampFrom = -1, $timestampTo = -1, $sortByDate = false, $limit = 0, $offset = 0)
    {
                
        $limit = (int)$limit;
        $offset = (int)$offset;
                
        if(!is_a($filter, "CalsyCalendarFilter"))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4') . ' Filter', $filter);
            
        }
        
        PerisianFrameworkToolbox::security($timestampFrom);
        PerisianFrameworkToolbox::security($timestampTo);
                
        $relationObject = CalsyCalendarEntryUserFrontendOrder::getInstance();
        $entryObject = static::getInstance();
        
        // Load the related flags
        {
            
            $query = ", " . CalsyCalendarEntryFlag::getFlagSubSelectQueryString();
           
        }
        
        // Left join the frontend user/order relation
        {
          
            $query .= "FROM {$entryObject->table} LEFT JOIN {$relationObject->table} ON ";
            $query .= "({$entryObject->field_parent_entry_id} IS NOT NULL AND {$relationObject->field_fk} = {$entryObject->field_parent_entry_id} OR {$entryObject->field_pk} = {$relationObject->field_fk}) ";
            
        }
        
        // Simple filters
        {
            
            $filterItems = array();
            $filterItemString = "";
            
            // Frontend users
            {
                
                $filterUserFrontendIdList = $filter->getUserFrontendIdentifiers(true);

                for($i = 0; $i < count($filterUserFrontendIdList); ++$i)
                {
                    
                    $userFrontendId = (int)$filterUserFrontendIdList[$i];

                    if($userFrontendId == CalsyUserFrontend::DEFAULT_USER_ID || $userFrontendId > 0)
                    {

                        array_push($filterItems, "{$relationObject->field_user_frontend_id} = '{$filterUserFrontendIdList[$i]}'");

                    }

                }
                
            }
                        
            // Backend users
            {
                
                $filterUserBackendIdList = $filter->getUserBackendIdentifiers(true);
                
                if(count($filterUserBackendIdList) > 0 && !in_array('-1', $filterUserBackendIdList))
                {

                    $addNull = false;

                    for($i = 0; $i < count($filterUserBackendIdList); ++$i)
                    {

                        if($filterUserBackendIdList[$i] == '0' || strtolower($filterUserBackendIdList[$i]) == 'null')
                        {

                            if(strtolower($filterUserBackendIdList[$i]) == 'null')
                            {

                                $filterUserBackendIdList[$i] = "0";

                            }

                            $addNull = true;

                            break;

                        }

                    }

                    // A group of users

                    $userFilterString = "{$entryObject->field_user_id} IN (" . implode(",", $filterUserBackendIdList) . ")";

                    $userFilterString = $addNull ? '(' . $userFilterString . " OR {$entryObject->field_user_id} IS NULL)" : $userFilterString;

                    array_push($filterItems, $userFilterString);

                }
                else
                {

                    // All valid users
                    array_push($filterItems, "({$entryObject->field_user_id} >= 0 OR {$entryObject->field_user_id} IS NULL)");

                }
                
            }
                        
            // Orders
            {
                
                $filterOrderIdList = $filter->getOrderIdentifiers(true);

                for($i = 0; $i < count($filterOrderIdList); ++$i)
                {

                    if(@(int)$filterOrderIdList[$i] > 0)
                    {

                        array_push($filterItems, "{$relationObject->field_order_id} = '{$filterOrderIdList[$i]}'");

                    }

                }
                
            }
            
            // Areas
            if($filter->getDoFilterByAreaIdentifiers())
            {
                
                $filterAreaIdList = $filter->getAreaIdentifiers(true);

                for($i = 0; $i < count($filterAreaIdList); ++$i)
                {

                    if(@(int)$filterAreaIdList[$i] > 0)
                    {

                        array_push($filterItems, "{$relationObject->field_area_id} = '{$filterAreaIdList[$i]}'");

                    }

                }
                
            }
            
            // Resources
            if(CalsyResourceModule::isEnabled())
            {
                
                $filterResourceIdList = $filter->getResourceIdentifiers(true);

                for($i = 0; $i < count($filterResourceIdList); ++$i)
                {

                    if(@(int)$filterResourceIdList[$i] > 0)
                    {

                        array_push($filterItems, "{$entryObject->field_fk_secondary} = '{$filterResourceIdList[$i]}'");

                    }

                }
                
            }

            if($filter->getShowHolidays())
            {

                array_push($filterItems, "({$entryObject->field_is_holiday} = 1 AND {$entryObject->field_user_id} IS NULL)");

            }

            if(!$filter->getIgnoreConfirmationStatus())
            {

                $confirmationQueryArray = array();
                $confirmationQuery = "";
                
                if($filter->getShowCanceled() || $filter->getShowUnconfirmed())
                {
                    
                    if($filter->getShowCanceled())
                    {

                        array_push($confirmationQueryArray, $relationObject->field_status_confirmation . ' = "canceled"');

                    }
                    else if($filter->getShowUnconfirmed())
                    {

                        array_push($confirmationQueryArray, $relationObject->field_status_confirmation . ' != "confirmed" AND ' . $relationObject->field_status_confirmation . ' != "canceled"');

                    }
                    
                    $confirmationQuery .= implode(' OR ', $confirmationQueryArray);

                }
                else
                {
                    
                    $confirmationQuery .= $relationObject->field_status_confirmation . ' != "canceled"';
                    
                }
                
                array_push($filterItems, $confirmationQuery);

            }

            $filterItemString = implode(" AND ", $filterItems);
            
            if(count($filterItems) > 0)
            {

                $query .= " WHERE ";
                $query .= $filterItemString;

            }
                        
        }
        
        // Areas & timestamp checks
        {
           
            $timestampList = array();
            
            $filterAreaIdList = $filter->getAreaIdentifiers(true);
                        
            if(CalsyAreaModule::isEnabled() && count($filterAreaIdList) > 0 && (int)$filterAreaIdList[0] != -1)
            {
                        
                for($i = 0; $i < count($filterAreaIdList); ++$i)
                {
                   
                    $filterArea = new CalsyArea($filterAreaIdList[$i]);
                    $areaTimeList = $filterArea->getTimeDurationElements();

                    $accumulatedDuration = 0;
                    $index = 0;

                    $freeTimeCount = 0;
                    $freeTimeList = array();

                    foreach($areaTimeList as $timeElement)
                    {

                        $timeElementDuration = (int)$timeElement['duration'];

                        if($timeElement['type'] != 'free')
                        {

                            $accumulatedDuration += $timeElementDuration;

                            ++$index;

                            continue;

                        }

                        $currentTimestampBegin = $timestampFrom + $accumulatedDuration;
                        $currentTimestampEnd = $index == count($areaTimeList) - 1 ? $timestampTo : ($currentTimestampBegin + $timeElementDuration);

                        array_push($freeTimeList, array(

                            'begin' => $currentTimestampBegin, 
                            'end' => $currentTimestampEnd

                        ));

                        {

                            $accumulatedDuration += $timeElementDuration;

                            ++$index;

                        }

                        $accumulatedDuration += $timeElementDuration;

                        ++$freeTimeCount;

                    }

                    if($freeTimeCount == 0)
                    {

                        array_push($timestampList, array(

                            'begin' => $timestampFrom,
                            'end' => $timestampTo

                        ));

                    }
                    else
                    {

                        $timestampList = array_merge($timestampList, $freeTimeList);

                    }
                    
                }
                
            }
            else
            {
                
                array_push($timestampList, array(
                
                    'begin' => $timestampFrom,
                    'end' => $timestampTo
                        
                ));
                
            }
            
            $timestampQueryList = array();
            
            foreach($timestampList as $timestampEntry)
            {
                
                $timestampFrom = $timestampEntry['begin'];
                $timestampTo = $timestampEntry['end'];

                if($timestampFrom >= 0 || $timestampTo >= 0)
                {

                    $comparisonOperatorBiggerThan = $filter->getIgnoreTimespanEdges() ? '>' : '>=';
                    $comparisonOperatorSmallerThan = $filter->getIgnoreTimespanEdges() ? '<' : '<=';

                    $newQuery = " (";

                    if($timestampFrom >= 0)
                    {
                        
                        $sqlTimestampFrom = PerisianFrameworkToolbox::sqlTimestamp($timestampFrom);

                        $newQuery .= "({$entryObject->field_date_begin} {$comparisonOperatorBiggerThan} " . $sqlTimestampFrom . " OR {$entryObject->field_date_end} {$comparisonOperatorBiggerThan} " . $sqlTimestampFrom . ")";

                    }

                    if($timestampFrom >= 0 && $timestampTo >= 0)
                    {

                        $newQuery .= " AND ";

                    }

                    if($timestampTo >= 0)
                    {
                        
                        $sqlTimestampTo = PerisianFrameworkToolbox::sqlTimestamp($timestampTo);

                        $newQuery .= "({$entryObject->field_date_end} {$comparisonOperatorSmallerThan} " . $sqlTimestampTo . " OR {$entryObject->field_date_begin} {$comparisonOperatorSmallerThan} " . $sqlTimestampTo . ")";

                    }

                    $newQuery .= ")";
                    
                    array_push($timestampQueryList, $newQuery);

                }
                
            }
            
            if(count($timestampQueryList) > 0)
            {

                if(strlen($filterItemString) > 0)
                {

                    $query .= " AND (";

                }
                else
                {
                    
                    $query .= " WHERE ";
                    
                }

                $query .= implode(" OR ", $timestampQueryList);

                if(strlen($filterItemString) > 0)
                {

                    $query .= ")";

                }
                
            }
            
        }
        
        // Flag filtering
        {
                                    
            $query = CalsyCalendarEntryFlag::addFlagFilterToQueryString($query, $filter->getFlags());

        }
        
        if($sortByDate)
        {
            
            $query .= " ORDER BY " . $entryObject->field_date_begin . " ASC";
            
        }
        
        if($limit > 0)
        {
            
            $query .= " LIMIT ";
            
            if($offset > 0)
            {
                
                $query .= $offset . ",";
                
            }
            
            $query .= $limit;
            
        }
                
        return $query;
        
    }
        
    /**
     * Retrieves how many free bookings the bookable time with the specified identifier has left.
     * 
     * @author Peter Hamm
     * @param int $bookableTimeEntryId
     * @return int The number of available bookings, can also be -1 for infinite.
     */
    public static function getMaximumAllowedNumberOfEntriesForBookableTime($bookableTimeEntryId)
    {
        
        $bookableTimeEntry = new static($bookableTimeEntryId);
        
        $timestampFrom = $bookableTimeEntry->{$bookableTimeEntry->timestamp_begin};
        $timestampTo = $bookableTimeEntry->{$bookableTimeEntry->timestamp_end};
        
        $countFreeBookings = (int)CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimeEntryId, CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE);
                
        if($countFreeBookings > 0)
        {
            
            $countFreeBookings -= static::getNumberExistingEntries(null, $timestampFrom, $timestampTo);
            $countFreeBookings = $countFreeBookings < 0 ? 0 : $countFreeBookings;
        
        }
        else
        {
            
            $countFreeBookings = -1;
            
        }
        
        //static::getMaximumAllowedNumberOfEntriesInTimespan(null, $timestampFrom, $timestampTo, null, false);
        
        return (int)$countFreeBookings;
        
    }
    
    /**
     * Retrieve the already existing entries for the specified filter and timespan.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarFilter $filter Optional, default: null.
     * @param int $timestampFrom Optional, default: -1 (No start time)
     * @param int $timestampTo Optional, default: -1 (No end time)
     * @param array $excludeIds IDs of entries that should not be considered Optional, default: none.
     * @return int
     * @todo Filter by area, ...
     */
    public static function getNumberExistingEntries($filter = null, $timestampFrom = -1, $timestampTo = -1, $excludeIds = array())
    {
        
        PerisianFrameworkToolbox::security($timestampFrom);
        PerisianFrameworkToolbox::security($timestampTo);
        PerisianFrameworkToolbox::security($excludeIds);
        
        if(is_null($filter))
        {
            
            $filter = new CalsyCalendarFilter();
        }
        
        // 
        {
            
            $entry = static::getInstance();
                        
            $query = "SELECT * " . self::getQueryForFilter($filter, $timestampFrom, $timestampTo);
            
            if(is_array($excludeIds) && count($excludeIds) > 0)
            {

                $excludeIdString = implode(',', $excludeIds);

                if(strlen($excludeIdString) > 0)
                {

                    $query .= " AND {$entry->field_pk} NOT IN (" . $excludeIdString . ")";
                    $query .= " AND {$entry->field_parent_entry_id} NOT IN (" . $excludeIdString . ")";

                } 

            }
                                    
            $database = $entry->getDatabaseConnection();
            
            $existingEntries = $database->select($query);
            $numberExistingEntries = count($existingEntries);
                        
            foreach($existingEntries as $potentiallyOverlappingEntry)
            {

                // Ignore this overlapping entry if it has been canceled.
            
                if(static::isCanceledForAnyUserFrontend($potentiallyOverlappingEntry[$entry->field_pk]))
                {
                
                    --$numberExistingEntries;

                    continue;

                }
                
            }

        }
        
        return $numberExistingEntries;
        
    }
    
    /**
     * Retrieves the maximum amount of allowed entries for 
     * the specified filters and timespan.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarFilter $filter
     * @param int $timestampFrom Optional, default: -1 (No start time)
     * @param int $timestampTo Optional, default: -1 (No end time)
     * @param array $excludeIds IDs of entries that should not be considered Optional, default: none.
     * @return int
     */
    public static function getMaximumAllowedNumberOfEntriesInTimespan($filter, $timestampFrom = -1, $timestampTo = -1, $excludeIds = array())
    {
                
        $entry = static::getInstance();
        
        $numberExistingEntries = static::getNumberExistingEntries($filter, $timestampFrom, $timestampTo, $excludeIds);
                 
        $returnValue = 0;
                
        if(CalsyBookableTimesModule::isEnabled())
        {
                                
            // Find all the bookable times with the specified filter and timespan.
            {
                
                $bookableTimes = CalsyBookableTimesModule::getBookableTimes($timestampFrom, $timestampTo, $filter);
                                
            }
            
            // Sum up all of the allowed numbers of simultaneous entries.
            for($i = 0; $i < count($bookableTimes); ++$i)
            {

                $bookableTimeMaximumNumber = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimes[$i][$entry->field_pk], CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE);
                
                if(!is_null($bookableTimeMaximumNumber) && (int)$bookableTimeMaximumNumber == 0)
                {
                    
                    // This entry has an unlimited maximum number of bookings.
                    return PHP_INT_MAX;

                }
                
                $bookableTimeMaximumNumber = ($bookableTimeMaximumNumber == null ? 1 : $bookableTimeMaximumNumber);

                $returnValue = $bookableTimeMaximumNumber > $returnValue ? $bookableTimeMaximumNumber : $returnValue;

            }
            
            $returnValue -= $numberExistingEntries;
            
        }
        else
        {
                                
            $numberOfUsers = count($filter->getUserBackendIdentifiers(true));
            
            $returnValue = $numberOfUsers - $numberExistingEntries;
            
        }
        
        $returnValue = $returnValue > 0 ? $returnValue : 0;
                
        return $returnValue;
        
    }
    
    /**
     * Retrieves a count of calendar entries for the specified filter (from self::createFilter) within the specified timestamps.
     * 
     * @param CalsyCalendarFilter $filter
     * @param int $timestampFrom Optional, default: -1 (No start time)
     * @param int $timestampTo Optional, default: -1 (No end time)
     * @param array $excludeIds IDs of entries that should not be considered Optional, default: none.
     * @return int
     */
    public static function getCountCalendarEntriesForFilter($filter, $timestampFrom = -1, $timestampTo = -1, $excludeIds = array(), $search = '')
    {
        
        PerisianFrameworkToolbox::security($timestampFrom);
        PerisianFrameworkToolbox::security($timestampTo);
        PerisianFrameworkToolbox::security($excludeIds);
        
        $entry = new self();
        
        $query = self::getQueryForFilter($filter, $timestampFrom, $timestampTo);
        $query = "SELECT COUNT({$entry->field_pk}) AS count, from_unixtime({$timestampFrom}) as time_from, from_unixtime({$timestampTo}) as time_to " .  $query;
                        
        if(is_array($excludeIds) && count($excludeIds) > 0)
        {
            
            $excludeIdString = implode(',', $excludeIds);
            
            if(strlen($excludeIdString) > 0)
            {
                
                $query .= " AND {$entry->field_pk} NOT IN (" . $excludeIdString . ")";
                $query .= " AND {$entry->field_parent_entry_id} NOT IN (" . $excludeIdString . ")";
                
            } 
            
        }
        
        $searchAddition = $entry->buildSearchString($search);
        
        if(strlen($searchAddition) > 0)
        {
            
            $query .= " AND (" . $searchAddition . ")";
            
        }
                        
        $database = $entry->getDatabaseConnection();
        $result = $database->select($query);
                
        if(count($result) == 0)
        {
            
            return 0;
            
        }
        
        return $result[0]['count'];
        
    }
    
    /**
     * Generates a simplified detail text for this entry.
     * 
     * @author Peter Hamm
     * @param bool $ignoreTimezone
     * @return string
     */
    public function getDetailText($ignoreTimezone = false)
    {
        
        $detailedInfo = "";
        $detailedInfoArray = array();
        
        // Event title
        {
            
            $eventTitle = PerisianLanguageVariable::getVariable(10848) . ": \"" . $this->{$this->field_title} . "\"";
            array_push($detailedInfoArray, $eventTitle);
            
        }
        
        // Description
        if(strlen($this->{$this->field_description}) > 0)
        {
            
            $eventDescription = PerisianLanguageVariable::getVariable(10781) . ": \"" . $this->{$this->field_description} . "\"";
            array_push($detailedInfoArray, $eventDescription);
            
        }
        
        // Date and time
        {
            
            $eventTime = PerisianLanguageVariable::getVariable(10964) . ": " . static::getFormattedEventTimeString($this->{$this->timestamp_begin}, $this->{$this->timestamp_end}, $ignoreTimezone);
            array_push($detailedInfoArray, $eventTime);
            
        }
        
        $detailedInfo .= implode("<br>", $detailedInfoArray);
        
        return $detailedInfo;
        
    }
    
    /**
     * Formats two given timestamps so that it can easily read by humans, as a short string.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @param bool $ignoreTimezone
     * @return string
     */
    public static function getFormattedEventTimeStringShort($timestampBegin, $timestampEnd, $ignoreTimezone = false)
    {
                
        $instance = static::getInstance();
        
        $dateFunction = $ignoreTimezone ? 'gmdate' : 'date';
        
        $timeString = $dateFunction("d.m, H:i", $timestampBegin) . "h - ";
        $timeString .= $dateFunction(CalsyCalendar::isSameDay($timestampBegin, $timestampEnd) ? "H:i" : "d.m, H:i", $timestampEnd) . "h";
        
        return $timeString;
        
    }
    
    /**
     * Formats two given timestamps so that it can easily read by humans.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @param bool $ignoreTimezone
     * @return string
     */
    public static function getFormattedEventTimeString($timestampBegin, $timestampEnd, $ignoreTimezone = false)
    {
                
        $instance = static::getInstance();
        
        $dateFunction = $ignoreTimezone ? 'gmdate' : 'date';
        
        $timeString = CalsyCalendar::getDayName($dateFunction("N", $timestampBegin) - 1) . ", " . $dateFunction("d.m.y, H:i", $timestampBegin) . " " . PerisianLanguageVariable::getVariable(10903) . " ";
        
        $timeString .= PerisianLanguageVariable::getVariable('p5a11bd260685f') . " ";
        $timeString .= (CalsyCalendar::isSameDay($timestampBegin, $timestampEnd) ? "" : CalsyCalendar::getDayName($dateFunction("N", $timestampEnd) - 1) . " ") . $dateFunction(CalsyCalendar::isSameDay($timestampBegin, $timestampEnd) ? "H:i" : "d.m.y, H:i", $timestampEnd) . " " . PerisianLanguageVariable::getVariable(10903);
        
        return $timeString;
        
    }
    
    /**
     * Retrieves if this entry is a home visit or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getIsHomeVisit()
    {
        
        $value = !CalsyResourceModule::isEnabled() ? false : isset($this->{$this->field_fk_secondary}) && $this->{$this->field_fk_secondary} == CalsyResource::getSpecialItemId("home_visit");
        
        return $value;
        
    }
    
    /**
     * Sets the last executed SQL query, e.g. used for logging.
     * 
     * @author Peter Hamm
     * @param String $query
     * @return void
     */
    private static function setLastQuery($query)
    {
        
        static::$lastQuery = $query;
        
    }
    
    /**
     * Retrieves the last query done by this class.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getLastQuery()
    {
        
        return static::$lastQuery;
        
    }
    
    /**
     * Retrieves calendar entries for the specified filter within the specified timestamps.
     * 
     * @param CalsyCalendarFilter $filter The filter criteria.
     * @param int $timestampFrom Optional, default: -1 (No start time)
     * @param int $timestampTo Optional, default: -1 (No end time)
     * @param String $search Optional, additional search string. Default: None
     * @param String $sortBy Optional, the field to sort by. Default: primary key
     * @param String $order Optional, either "ASC" or "DESC". Default: "ASC"
     * @param int $start Optional, default: 0
     * @param int $limit Optional, default: No limit
     * @return array
     */
    public static function getCalendarEntriesForFilter($filter, $timestampFrom = -1, $timestampTo = -1, $search = '', $sortBy = 'pk', $order = 'ASC', $start = 0, $limit = 0)
    {
        
        $entry = new self();
                    
        $query = self::getQueryForFilter($filter, $timestampFrom, $timestampTo);
        
        $query = "SELECT * " .  $query;
        
        $searchAddition = $entry->buildSearchString($search);
        
        if(strlen($searchAddition) > 0)
        {
            
            $query .= " AND (" . $searchAddition . ")";
            
        }
        
        if($sortBy == "pk")
        {
            
            $sortBy = $entry->field_pk;
            
        }
        
        $query .= " GROUP BY {$entry->field_pk}";
        
        $query .= " ORDER BY {$sortBy} {$order}";
        
        if($start > 0 || $limit > 0)
        {
            
            $query .= " LIMIT {$start}, {$limit}";
            
        }
                                                
        $database = $entry->getDatabaseConnection();
        $results = $database->select($query);
        
        static::setLastQuery($query);
                
        if(count($results) == 0)
        {
            
            return array();
            
        }
        
        for($i = 0; $i < count($results); ++$i)
        {
            
            $results[$i][$entry->timestamp_begin] = strtotime($results[$i][$entry->field_date_begin] . " UTC");
            $results[$i][$entry->timestamp_end] = strtotime($results[$i][$entry->field_date_end] . " UTC");
            
        }
        
        return $results;
        
    }
        
    /**
     * If the area module is active and this entry has an area set, that area object is returned.
     * If not, null is returned.
     * 
     * @author Peter Hamm
     * @return CalsyArea
     */
    public function getAreaFromUserFrontend()
    {
        
        $returnValue = null;
                
        $areaId = 0;
        
        if(@$this->userFrontendWithOrders)
        {

            foreach($this->userFrontendWithOrders as $userFrontendWithOrder)
            {

                if(strlen($userFrontendWithOrder['a']) > 0 && $userFrontendWithOrder['a'] > 0)
                {

                    $areaId = $userFrontendWithOrder['a'];

                    break;

                }

            }

            if($areaId > 0)
            {
                
                $returnValue = new CalsyArea($areaId);

            }
           
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the times to check for overlaps.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected function getAreaTimesToCheck()
    {
        
        $timestampsToCheck = array();
        
        $entryArea = $this->getAreaFromUserFrontend();
        
        if(CalsyAreaModule::isEnabled() && $entryArea != null)
        {
            
            $areaTimeList = $entryArea->getTimeDurationElements();
            
            $accumulatedDuration = 0;
            $index = 0;

            foreach($areaTimeList as $timeElement)
            {

                $timeElementDuration = (int)$timeElement['duration'];

                if($timeElement['type'] == 'free')
                {

                    $accumulatedDuration += $timeElementDuration;

                    ++$index;

                    continue;

                }

                $timestampBegin = $this->{$this->timestamp_begin} + $accumulatedDuration;
                $timestampEnd = $index == count($areaTimeList) - 1 ? $this->{$this->timestamp_end} : ($timestampBegin + $timeElementDuration);

                array_push($timestampsToCheck, array('begin' => $timestampBegin, 'end' => $timestampEnd));

                {

                    $accumulatedDuration += $timeElementDuration;

                    ++$index;

                }

            }
            
        }
        else
        {
            
            // Standard calendar entry without an area
            array_push($timestampsToCheck, array('begin' => $this->{$this->timestamp_begin}, 'end' => $this->{$this->timestamp_end}));
            
        }
        
        return $timestampsToCheck;

    }
    
    /**
     * Checks for overlapping entries.
     * Throws PerisianException objects if any overlaps occure.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return bool Will return false if no overlapping entries were found.
     */
    public function checkOverlapping()
    {

        $entryFilter = array($this->{$this->field_pk});
        
        $timestampsToCheck = $this->getAreaTimesToCheck();
        
        foreach($timestampsToCheck as $timestampToCheck)
        {
            
            $timeBegin = $timestampToCheck['begin'];
            $timeEnd = $timestampToCheck['end'];
        
            $this->checkOverlappingUserBackend($this->{$this->field_user_id}, $timeBegin, $timeEnd);

            // Check if there is already an entry for this resource at the specified time.
            if(CalsyResourceModule::isEnabled() && (strlen($this->{$this->field_fk_secondary}) > 0 || $this->{$this->field_fk_secondary} > 0))
            {
                
                // Build the filter
                {
                
                    $filterResource = new CalsyCalendarFilter();
                    
                    $filterResource->setResourceIdentifiers($this->{$this->field_fk_secondary});
                    
                    $filterResource->setShowUnconfirmed(true);
                    $filterResource->setIgnoreConfirmationStatus(true);
                    $filterResource->setIgnoreTimespanEdges(true);
                
                }

                $numberOverlappingEntriesResource = CalsyCalendarEntry::getCountCalendarEntriesForFilter($filterResource, $timeBegin, $timeEnd, $entryFilter);

                if($numberOverlappingEntriesResource > 0)
                {

                    // It could be that multiple reservations for this specific resource are allowed, check it.
                    if(!CalsyResource::isMultipleBookingAllowedForResource($this->{$this->field_fk_secondary}))
                    {

                        throw new PerisianException(PerisianLanguageVariable::getVariable(10917));

                    }

                }

            }
            
        }
        
        return false;
        
    }
    
    /**
     * Checks if the backend user with the specified 
     * identifier is booked within two specified timestamps.
     * 
     * @auhtor Peter Hamm
     * @param int $userBackendId
     * @param int $timeBegin
     * @param int $timeEnd
     * @param int $calendarEntryIdToIgnore Optional, specify a calendar entry's ID that should be ignored checking for overlaps. Default: none.
     * @return boolean
     */
    public static function isUserBackendBookedBetween($userBackendId, $timeBegin, $timeEnd, $calendarEntryIdToIgnore = 0)
    {
        
        $returnValue = false;
        
        $dummy = new static($calendarEntryIdToIgnore > 0 ? $calendarEntryIdToIgnore : null);
        
        try
        {
            
            $dummy->checkOverlappingUserBackend($userBackendId, $timeBegin, $timeEnd);
            
        } 
        catch(PerisianException $e) 
        {
        
            $returnValue = true;

        }
    
        return $returnValue;
        
    }
    
    /**
     * Checks if there is already an entry for the 
     * backend user with the specified identifier, at the specified time.
     * 
     * @author Peter Hamm
     * @param int $userBackendId
     * @param int $timeBegin
     * @param int $timeEnd
     * @throws PerisianException
     * @return bool Will return false if no overlapping entries were found.
     */
    protected function checkOverlappingUserBackend($userBackendId, $timeBegin, $timeEnd)
    {
        
        if(PerisianSystemSetting::getSettingValue('calendar_is_enabled_user_multi_booking') == '1')
        {
            
            return true;
        
        }
        
        if((strlen($userBackendId) > 0 || $userBackendId > 0))
        {
            
            $entryFilter = array($this->{$this->field_pk});
                        
            // Build the filter
            {
                
                $filter = new CalsyCalendarFilter();
            
                $filter->setUserBackendIdentifiers($userBackendId);
                
                $filter->setShowUnconfirmed(true);
                $filter->setShowCanceled(false);
                $filter->setShowHolidays(false);
                $filter->setShowSurveys(false);
                $filter->setIgnoreConfirmationStatus(true);
                $filter->setIgnoreTimespanEdges(true);
                
            }
            
            $listOverlappingEntriesUser = CalsyCalendarEntry::getCalendarEntriesForFilter($filter, $timeBegin, $timeEnd, $entryFilter);
            $numberOverlappingEntriesUser = count($listOverlappingEntriesUser);
            
            foreach($listOverlappingEntriesUser as $potentiallyOverlappingEntry)
            {

                // Ignore this overlapping entry if it has been canceled.

                if(static::isCanceledForAnyUserFrontend($potentiallyOverlappingEntry[$this->field_pk]))
                {

                    --$numberOverlappingEntriesUser;

                    continue;

                }

                // Ignore this overlapping entry if it's the one being edited.

                if(isset($this->{$this->field_pk}) && strlen($this->{$this->field_pk}) > 0 && $potentiallyOverlappingEntry[$this->field_pk] == $this->{$this->field_pk})
                {

                    --$numberOverlappingEntriesUser;

                    continue;

                }

            }

            if($numberOverlappingEntriesUser > 0)
            {

                throw new PerisianException(PerisianLanguageVariable::getVariable(10894));

            }

        }
        
        return false;
        
    }
    
    /**
     * Checks for overlapping entries for the specified frontend user identifier.
     * Throws PerisianException objects if any overlaps occure.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId Optional, default: 0 (will not check for any frontend user's overlapping entries).
     * @param bool $ignoreTimezone Optional, default: false
     * @throws PerisianException
     * @return bool Will return false if no overlapping entries were found.
     */
    public function checkOverlappingForUserFrontend($userFrontendId = 0)
    {
        
        global $settings;
        
        $userFrontendCanPickTherapist = CalsyUserFrontendRegistrationAndCalendarModule::isUserSelectionEnabled();
        
        $entryFilter = array($this->{$this->field_pk});
        
        $entryArea = $this->getAreaFromUserFrontend();
        $timestampsToCheck = $this->getAreaTimesToCheck();
        
        foreach($timestampsToCheck as $timestampToCheck)
        {
            
            $timeBegin = $timestampToCheck['begin'];
            $timeEnd = $timestampToCheck['end'];
        
            // UserFrontend may not book in the past, the earliest possible date is either "today" or "tomorrow", depending on the configuration
            {
                
                if(PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_enabled_booking_same_day') == '1')
                {
                    
                    $firstMinuteToday = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time());
                    
                    if($timeBegin < $firstMinuteToday || $timeEnd < $firstMinuteToday)
                    {

                        throw new PerisianException(PerisianLanguageVariable::getVariable('p5d417e123ccca'));

                    }
                    
                }
                else
                {

                    $firstMinuteTomorrow = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time() + 86400);

                    if($timeBegin < $firstMinuteTomorrow || $timeEnd < $firstMinuteTomorrow)
                    {

                        throw new PerisianException(PerisianLanguageVariable::getVariable(10936));

                    }
                    
                }
                
            }

            if(!(CalsyOpeningTimesModule::isEnabled() && CalsyBookableTimesModule::isEnabled() && $entryArea->{$entryArea->field_requires_bookable_time} == 1))
            {

                // UserFrontend may only book within certain times specified in the backend
                if(!CalsyCalendar::isWithinBookableTimes($timeBegin) || !CalsyCalendar::isWithinBookableTimes($timeEnd))
                {

                    // Check is currently disabled, needs debugging
                    //throw new PerisianException(PerisianLanguageVariable::getVariable(10941));

                }
                
            }

            // Check if there's already an overlapping holiday
            {
                
                // Build the filter
                {
                
                    $holidayFilter = new CalsyCalendarFilter();
                                        
                    $holidayFilter->setShowUnconfirmed(true);
                    $holidayFilter->setShowHolidays(true);
                    $holidayFilter->setIgnoreConfirmationStatus(true);
                    $holidayFilter->setIgnoreTimespanEdges(true);
                
                }

                $numberOverlappingEntriesHolidays = CalsyCalendarEntry::getCountCalendarEntriesForFilter($holidayFilter, $timeBegin, $timeEnd, $entryFilter);

                if($numberOverlappingEntriesHolidays > 0)
                {

                    throw new PerisianException(PerisianLanguageVariable::getVariable(10986));

                }

            }

            // Check if there's already an overlapping entry for the frontend user
            if($userFrontendId > 0)
            {
                
                // Build the filter
                {
                
                    $userFrontendFilter = new CalsyCalendarFilter();
                    
                    $userFrontendFilter->setUserFrontendIdentifiers($userFrontendId);
                                        
                    $userFrontendFilter->setShowUnconfirmed(true);
                    $userFrontendFilter->setShowHolidays(true);
                    $userFrontendFilter->setIgnoreTimespanEdges(true);
                
                }
            
                $numberOverlappingEntriesUserFrontend = CalsyCalendarEntry::getCountCalendarEntriesForFilter($userFrontendFilter, $timeBegin, $timeEnd, $entryFilter);

                if($numberOverlappingEntriesUserFrontend > 0)
                {

                    throw new PerisianException(PerisianLanguageVariable::getVariable(10933));

                }

            }
            
            if($userFrontendCanPickTherapist)
            {
                
                // Check if there's already an overlapping entry for the selected user
                                
                if(!CalsyOpeningTimesModule::isEnabled() && CalsyBookableTimesModule::isEnabled())
                {
                    
                    // Special case: The bookable times module is active, get the maximum of allowed entries at that time.
                    
                    // Build the filter
                    {

                        $filterUser = new CalsyCalendarFilter();

                        $filterUser->setUserBackendIdentifiers($this->{$this->field_user_id});

                        $filterUser->setShowUnconfirmed(true);
                        $filterUser->setIgnoreConfirmationStatus(true);
                        $filterUser->setIgnoreTimespanEdges(true);

                    }
                                        
                    $maximumNumberOfAllowedEntries = static::getMaximumAllowedNumberOfEntriesInTimespan($filterUser, $timeBegin, $timeEnd, $entryFilter);
                                        
                    if($maximumNumberOfAllowedEntries <= 0)
                    {
                        
                        throw new PerisianException(PerisianLanguageVariable::getVariable('p57f5199a11493'));
                        
                    }
                    
                }
                else
                {
                    
                    $this->checkOverlappingUserBackend($this->{$this->field_user_id}, $timeBegin, $timeEnd);
                    
                }
              
            }

            // If no user is selected. The number of appointments at this point of time
            // must not be higher than the number of users.
            /*
            {
            
                // Build the filter
                {

                    $generalFilter = new CalsyCalendarFilter();

                    $generalFilter->setShowUnconfirmed(true);
                    $generalFilter->setIgnoreConfirmationStatus(true);
                    $generalFilter->setIgnoreTimespanEdges(true);

                }

                $numberOverlappingEntriesGeneral = CalsyCalendarEntry::getCountCalendarEntriesForFilter($generalFilter, $timeBegin, $timeEnd, $entryFilter);

                if($numberOverlappingEntriesGeneral >= CalsyUserBackend::getUserCount())
                {

                    throw new PerisianException(PerisianLanguageVariable::getVariable(10934));

                }

            }
            */
            
        }
        
        return false;
        
    }
    
    /**
     * Checks whether a payment by the specified frontend user was received for this entry or not.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     */
    public function isPaidByUserFrontendId($userFrontendId)
    {
        
        $paymentReceived = CalsyAreaPrice::isPaymentReceivedFromUserFrontend($this->{$this->field_pk}, $userFrontendId);
        
        return $paymentReceived;
        
    }
    
    /**
     * Sets the list of frontend users with their orders and confirmation statuses.
     * Will be saved when save() is called, if set.
     * 
     * @author peter Hamm
     * @param array $userFrontendWithOrders
     * @return void
     */
    public function setUserFrontendWithOrders($userFrontendWithOrders)
    {
        
        $this->userFrontendWithOrders = $userFrontendWithOrders;
        
    }
    
    /**
     * Sets the value whether this calendar entry requires/required a payment.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setIsPaymentRequired($value)
    {
        
        $this->paymentRequired = $value;
        
    }
    
    /**
     * Retrieves if this calendar entry requires/required a payment.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isPaymentRequired()
    {
        
        return $this->paymentRequired;
        
    }
    
    /**
     * Sets the value whether this calendar entry should be tagged as being a new entry.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setIsTaggedAsNew($value)
    {
        
        $this->tagIsNew = $value;
        
    }
    
    /**
     * Retrieves if this calendar entry was somehow tagged as being a new entry.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isTaggedAsNew()
    {
        
        return $this->tagIsNew;
        
    }
    
    /**
     * Stores the frontend user with orders and their confirmation statuses
     * 
     * @author Peter Hamm
     * @param bool $isRelevantEdit Optional, should indicate whether the entry got changed relevantly or not.
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function handleUserFrontendWithOrders($isRelevantEdit = false)
    {
        
        global $user, $userFrontend;
        
        if(is_array($this->userFrontendWithOrders))
        {
            
            $userFrontendIdentifiersToKeep = array();
            $notificationUsersForArea = array();
        
            for($i = 0; $i < count($this->userFrontendWithOrders); ++$i)
            {

                $userFrontendId = @$this->userFrontendWithOrders[$i]["p"];
                $orderId = CalsyOrderModule::isEnabled() ? @$this->userFrontendWithOrders[$i]["r"] : "";
                $areaId = CalsyAreaModule::isEnabled() ? @$this->userFrontendWithOrders[$i]["a"] : "";
                $infoAdditional = @$this->userFrontendWithOrders[$i]["info_additional"];
                $messageToClient = @$this->userFrontendWithOrders[$i]["message_to_client"];
                
                $currentUserFrontend = new CalsyUserFrontend($userFrontendId);
                
                if(empty($userFrontendId) && empty($orderId) && empty($areaId))
                {
                    
                    continue;
                    
                }
                
                if(isset($userFrontendId) && $userFrontendId > 0)
                {
                    
                    array_push($userFrontendIdentifiersToKeep, $userFrontendId);
                
                }
                
                // Handle the confirmation / canceled status
                {
                    
                    // Confirmation status
                    {
                        
                        $confirmationStatus = @$this->userFrontendWithOrders[$i]["c"] == "true" ? "confirmed" : "open";
                                                
                    }
                    
                    // Cancellation status
                    {
                     
                        $confirmationStatus = @$this->userFrontendWithOrders[$i]["ca"] == "true" ? "canceled" : $confirmationStatus;
                    
                    }
                    
                    $confirmationStatusMessage = @$this->userFrontendWithOrders[$i]["confirmation_status_message"];
                    
                }
                
                $userFrontendOrderObject = CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($this->{$this->field_pk}, $userFrontendId);
                
                $isNewEntry = $this->isTaggedAsNew() || strlen($userFrontendOrderObject->{$userFrontendOrderObject->field_pk}) == 0;
                $isExistingAndBeingConfirmed = !$isNewEntry && ($userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation} != "confirmed") && $confirmationStatus == "confirmed";
                $isExistingAndBeingCanceled = !$isNewEntry && ($userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation} != "canceled") && $confirmationStatus == "canceled";

                $userFrontendOrderObject->{$userFrontendOrderObject->field_fk} = $this->{$this->field_pk};
                $userFrontendOrderObject->{$userFrontendOrderObject->field_user_frontend_id} = $userFrontendId;
                $userFrontendOrderObject->{$userFrontendOrderObject->field_info_additional} = $infoAdditional;
                $userFrontendOrderObject->{$userFrontendOrderObject->field_message_to_client} = $messageToClient;
                $userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation} = $confirmationStatus;
                $userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation_message} = $confirmationStatusMessage;
                
                if(CalsyAreaModule::isEnabled())
                {
                    
                    $userFrontendOrderObject->{$userFrontendOrderObject->field_area_id} = $areaId;
                    
                }
                
                if(CalsyOrderModule::isEnabled())
                {
                    
                    $userFrontendOrderObject->{$userFrontendOrderObject->field_order_id} = $orderId;
                    
                }

                $userFrontendOrderObject->save();
                
                // Notification sending 
                {
                    
                    // Send an email to the frontend user
                    {

                        $infoUserFrontend = new CalsyUserFrontend($userFrontendId);
                        
                        if($isNewEntry && isset($user))
                        {
                            
                            $sendStatus = $infoUserFrontend->sendEmailCalendarEntryNew($this, $user, $userFrontend, true);
                            
                        }
                        else if($isExistingAndBeingConfirmed)
                        {

                            $sendStatus = $infoUserFrontend->sendEmailCalendarEntryConfirmed($this, true);

                        }
                        else if($isExistingAndBeingCanceled)
                        {

                            $sendStatus = $infoUserFrontend->sendEmailCalendarEntryCanceled($this, true);

                        }
                        else if($isRelevantEdit)
                        {
                            
                            $sendStatus = $infoUserFrontend->sendEmailCalendarEntryChanged($this, $user, $userFrontend, true);
                          
                        }
                        
                    }

                    if(CalsyAreaModule::isEnabled())
                    {
                        
                        $areaObject = new CalsyArea($areaId);
                        
                        $this->setIsPaymentRequired(CalsyAreaPrice::isPaymentActivated() && CalsyAreaPrice::isPaymentRequiredForArea($areaId));
                        
                        // Send a notification to the backend users designated to receive them for that area,
                        // but only if no distinct backend user is specified for this entry.
                        if($isNewEntry && (!isset($this->{$this->field_user_id}) || strlen($this->{$this->field_user_id}) == 0 || $this->{$this->field_user_id} <= 0))
                        {
                            
                            if(!isset($notificationUsersForArea[$areaId]))
                            {

                                // Load and cache which users need to receive notifications for this area and entry.
                                $notificationUsersForArea[$areaId] = CalsyAreaUserBackendNotification::getUsersForArea($areaId);

                            }
                        
                            foreach($notificationUsersForArea[$areaId] as $notificationUser)
                            {
                                
                                // Don't send it to the user that owns the entry anyways.
                                if(@$this->{$this->field_user_id} != $notificationUser->{$notificationUser->field_pk})
                                {

                                    $sendStatus = $notificationUser->sendEmailCalendarEntryNew($this, $user, $currentUserFrontend, true);

                                }

                            }
                        
                        }
                        
                    }
                    
                }

            }
            
            // Delete all frontend user / order relations for this entry that were not in the list
            CalsyCalendarEntryUserFrontendOrder::deleteEntriesForCalendarEntryNotInList($this->{$this->field_pk}, $userFrontendIdentifiersToKeep);

        }
        
    }
    
    /**
     * Adds the speified time element title to the specified string.
     * 
     * @author Peter Hamm
     * @param String $string
     * @param String $timeElementTitle
     * @param Array $timeElementArray
     * @return String
     */
    protected static function addTimeDurationTitle($string, $timeElementTitle, $timeElementArray)
    {
        
        $firstTimeElementTitle = ' - ' . $timeElementArray[0]['title'];
        
        if(substr($string, -strlen($firstTimeElementTitle)) == $firstTimeElementTitle)
        {
                       
            $string = substr($string, 0, strlen($string) - strlen($firstTimeElementTitle));
           
        }
        
        $string .= strlen($timeElementTitle) > 0 ? (' - ' . $timeElementTitle) : '';
        
        return $string;
        
    }
    
    /**
     * Adds the speified time element title to the specified string.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function removeTimeDurationTitle()
    {
        
        if(!CalsyAreaModule::isEnabled())
        {
            
            return;
            
        }
        
        $area = CalsyArea::getAreaForCalendarEntry($this->{$this->field_pk});
        
        if($area != null)
        {       
            
            $timeElementArray = $area->getTimeDurationElements();
            
            $string = $this->{$this->field_title};
            
            $firstTimeElementTitle = ' - ' . $timeElementArray[0]['title'];
        
            if(substr($string, -strlen($firstTimeElementTitle)) == $firstTimeElementTitle)
            {

                $string = substr($string, 0, strlen($string) - strlen($firstTimeElementTitle));

            }

            $this->{$this->field_title} = $string;
                
        }
        
    }
    
    /**
     * Updates the duration based on the entry's children elements.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function updateDurationFromChildren()
    {
        
        if(!CalsyAreaModule::isEnabled())
        {
            
            return;
            
        }
                        
        $query = "{$this->field_parent_entry_id} = '{$this->{$this->field_pk}}'";
        
        $results = $this->getData($query, $this->field_date_end, "DESC");
        
        $longestElementIdentifier = count($results) == 0 ? null : $results[0][$this->field_pk];
        
        if(!empty($longestElementIdentifier))
        {
            
            $longestElement = new static($longestElementIdentifier);
            
            $this->setTimestampEnd($longestElement->{$longestElement->timestamp_end});
            $this->{$this->field_date_end} = $longestElement->{$longestElement->field_date_end};
            
        }
        
    }
    
    /**
     * Depending on if this entry has children,
     * this function updates the actual duration / ending timestamp of the calendar entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function updateDuration()
    {
        
        if($this->countChildren > 0)
        {

            $this->updateDurationFromChildren();

        }

    }
    
    /**
     * Checks if this is an entry that has been edited in relevant data,
     * e.g. the start and end times or description.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected function checkIsRelevantEdit()
    {
        
        if($this->isTaggedAsNew() || !isset($this->{$this->field_pk}) || strlen($this->{$this->field_pk}) == 0)
        {
            
            return false;
            
        }
        
        // Load the original entry data and compare.
        
        $originalEntry = new static($this->{$this->field_pk});
        
        if($originalEntry->{$originalEntry->timestamp_begin} != $this->{$this->timestamp_begin})
        {
                                    
            return true;
            
        }
                
        if($originalEntry->{$originalEntry->timestamp_end} != $this->{$this->timestamp_end})
        {
                                   
            return true;
            
        }
        
        if($originalEntry->{$originalEntry->field_description} != $this->{$this->field_description})
        {
                        
            return true;
            
        }
        
    }
    
    /**
     * Retrieves whether  new entries made by a frontend user should be automatically confirmed or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isEnabledAutoConfirmationForUserFrontend($areaId)
    {
        
        $autoConfirm = !CalsyAreaPrice::isPaymentRequiredForArea($areaId) && PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_automatically_confirm_entries') == 1;
     
        return $autoConfirm;
        
    }
    
    /**
     * Should be fired after saving a calendar entry in the frontend.
     * 
     * @author Peter Hamm
     * @param CalsyCalendarentry $calendarEntry
     * @param CalsyUserFrontend $userFrontend
     * @throws PerisianException
     */
    public static function onAfterSaveNewEntryFrontend($calendarEntry, $userFrontend)
    {
        
        // Parameter setup
        {
            
            $area = $calendarEntry->getAreaFromUserFrontend();
            $areaId = $area->{$area->field_pk};
            
            $autoConfirm = static::isEnabledAutoConfirmationForUserFrontend($areaId);
            $isExistingAccount = $userFrontend->isLoggedIn() ? true : false;
        
        }
        
        if($calendarEntry->isPaymentRequired())
        {
            
            // Generate or retrieve the transaction with PayPal.
            {
                
                // Try to retrieve any existing transaction, if possible...
                $existingPaymentObject = CalsyAreaPrice::getPaymentTransaction($calendarEntry->{$calendarEntry->field_pk}, $userFrontend->{$userFrontend->field_pk});
                
                if($existingPaymentObject != null)
                {
                    
                    $paymentType = $existingPaymentObject->getTransactionName();
                    
                    if($paymentType == 'paypal')
                    {
                        
                        $paymentId = $existingPaymentObject->{$existingPaymentObject->getFieldTransactionId()};
                        
                    }
                    
                }
                else
                {
               
                    $price = CalsyAreaPrice::getPriceForAreaId($areaId);

                    $payPal = new PerisianPayPal();

                    // The item we want to sell
                    {

                        $details = Array(

                            $userFrontend->field_pk => $userFrontend->{$userFrontend->field_pk}

                        );

                        $item = new PerisianPayPalItem();

                        $item->title = $calendarEntry->{$calendarEntry->field_title};
                        $item->description = PerisianLanguageVariable::getVariable('p59f4f62208253');
                        $item->customIdentifier = $calendarEntry->{$calendarEntry->field_pk};
                        $item->price = $price->getPriceAmountFormattedNet();
                        $item->currencyCode = $price->getCurrency();
                        $item->taxRate = $price->getRateTax();
                        $item->type = PerisianPayPalTransaction::TYPE_CALENDAR_ENTRY;
                        $item->setDetails($details);

                        $payPal->setItem($item);

                    }

                    $transactionResult = $payPal->createPayment();

                    if($transactionResult["success"])
                    {

                        $paymentId = $transactionResult['payment_id'];
                        $paymentType = 'paypal';

                    }
                    else
                    {

                        // "Your payment cannot be processed at this time. Please try again later."
                        throw new PerisianException(PerisianLanguageVariable::getVariable('p59f4f281771ce'), $transactionResult);

                    }

                }
                
                $result["success"] = true;
                $result["payment_url"] = CalsyAreaPrice::getPaymentUrl($paymentType, $paymentId);
                $result["message"] = "<br>" . PerisianLanguageVariable::getVariable('p59f4ea2d4bf1c');

                // Send an email to the frontend user with all details
                $mailSendStatus = $userFrontend->sendEmailCalendarEntryNewPaymentRequired($result["payment_url"], $calendarEntry, null, $userFrontend, true);

            }

        }
        else
        {

            $result["success"] = true;
            $result["message"] = "<br>" . PerisianLanguageVariable::getVariable($isExistingAccount ? 11053 : 11052);

            // Send an email to the frontend user with all details
            $mailSendStatus = $autoConfirm ? $userFrontend->sendEmailCalendarEntryConfirmed($calendarEntry) : $userFrontend->sendEmailCalendarEntryNew($calendarEntry, null, $userFrontend, true);

        }
        
        return $result;
        
    }
    
    /**
     * Saves this class' field variables to the specified table and database
     *
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @global CalsyUserFrontend $userFrontend
     * @param Array $saveFields Optional: An array containing names of fields that shall be saved, default: save all fields
     * @param bool $ignoreUserFrontendWithOrders Optional, internal use. Default: false.
     * @param CalsyUserFrontend $userFrontendOverride Optional, default false
     * @return integer The primary key ID of the saved database row - or 0 if the query failed
     */
    public function save($saveFields = Array(), $ignoreUserFrontendWithOrders = false, $userFrontendOverride = null)
    {
        
        global $user, $userFrontend;
        
        if(!is_null($userFrontendOverride))
        {
            
            $userFrontend = $userFrontendOverride;
            
        }
        
        $entryByUserFrontend = !isset($user) && isset($userFrontend);
        
        $isNewEntry = $this->isTaggedAsNew() || !isset($this->{$this->field_pk}) || strlen($this->{$this->field_pk}) == 0;
        $isChildEntry = isset($this->{$this->field_parent_entry_id}) && $this->{$this->field_parent_entry_id} > 0;
        $isRelevantEdit = $this->checkIsRelevantEdit();
  
        $sendNotifications = $this->getSendNotifications();
        
        if($this->{$this->field_user_id} <= 0 && $this->{$this->field_user_id} != CalsyUserBackend::DEFAULT_USER_ID)
        {
            
            $this->{$this->field_user_id} = PerisianDatabaseModel::DB_VALUE_NULL;
            
        }
                
        $this->{$this->field_pk} = parent::save($saveFields);
        
        if($this->{$this->field_user_id} == PerisianDatabaseModel::DB_VALUE_NULL)
        {
            
            unset($this->{$this->field_user_id});
            
        }
                
        if(true)
        {
                
            if(!$ignoreUserFrontendWithOrders)
            {
                
                $this->handleUserFrontendWithOrders($isRelevantEdit);
                
            }
            
            if($sendNotifications && !$isChildEntry)
            {
                
                // Inform the owner of this entry that it has been created
                try
                {
                    
                    $infoUser = new CalsyUserBackend(@$this->{$this->field_user_id});
                    
                    // Language handling
                    {

                        $previousLanguage = PerisianLanguageVariable::getLanguageId();

                        $userLanguage = $infoUser->getSetting('user_language');

                        if($userLanguage > 0)
                        {

                            PerisianLanguageVariable::setLanguage($userLanguage);

                        }

                    }
                    
                    if($isNewEntry)
                    {
                                                
                        // It's a completely new entry
                        
                        $sendStatus = $infoUser->sendEmailCalendarEntryNew($this, $user, $userFrontend, true);
                    
                    }
                    else if($isRelevantEdit)
                    {
                                                
                        // This was an edit, check if relevant data changed (e.g. the times)
                                                                    
                        $sendStatus = $infoUser->sendEmailCalendarEntryChanged($this, $user, $userFrontend, true);
                        
                    }
                    
                    // Change the language back to the default
                    {

                        PerisianLanguageVariable::setLanguage($previousLanguage);

                    }
                    
                }
                catch(PerisianException $e)
                {
                    
                    // The mail could not be sent or the user was not found... ignore it.
                
                }

            }
            
        }
                
        // Handle the children
        {
            
            $this->handleChildren();
           
        }
        
        return $this->{$this->field_pk};
        
    }
    
    /**
     * Handles this entry's children, if they were set before.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function handleChildren()
    {
        
        // Delete all existing ones.
        $this->deleteEntriesWithParentId($this->{$this->field_pk});
 
        $childData = $this->getDataChildren();
        
        if(!@$childData || count($childData) == 0)
        {
            
            return;
            
        }
        
        for($i = 0; $i < count($childData); ++$i)
        {
        
            // Generate a new element.
            $saveElement = new static($this->{$this->field_pk});

            $saveElement->{$saveElement->field_pk} = null;
            $saveElement->{$saveElement->field_fk} = null;

            $saveElement->{$saveElement->field_title} = @$childData[$i]['title'];
            $saveElement->{$saveElement->field_description} = @$childData[$i]['description'];
            $saveElement->{$saveElement->field_user_id} = @$childData[$i]['userId'];
            
            $saveElement->{$saveElement->field_parent_entry_id} = $this->{$this->field_pk};

            if(CalsyResourceModule::isEnabled())
            {

                $saveElement->{$saveElement->field_fk_secondary} = @$childData[$i]['resourceId'];

            }

            $saveElement->setTimestampBegin($childData[$i]['begin']);
            $saveElement->setTimestampEnd($childData[$i]['end']);
            
            $saveElement->save(Array(), false);
            
        }
       
    }
    
    /**
     * Directly makes a copy of this entry at the specified timestamp.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @param int $newTimestamp
     * @param int $newUserId Optional: The identifier of the user you want to copy it to. Set it to zero to keep the same user. Default: 0
     * @param bool $keepHour Optional: Set this to true to take over the hour of the appointment. Default: false.
     * @return int The identifier of the newly copied entry.
     * @throws PerisianException
     */
    public function copyEntry($newTimestamp = 0, $newUserId = 0, $keepHour = false)
    {
        
        global $user;
        
        if($newTimestamp == 0)
        {
            
            $newTimestamp = $this->{$this->timestamp_begin} + PerisianTimeZone::getTimeZoneOffsetInSeconds();
            
        }
        
        if(!isset($this->{$this->field_pk}))
        {
            
            // Invalid ID, this entry must be saved before.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('10409'));
        
        }
        
        if($newUserId <= 0 || strlen($newUserId) == 0)
        {
            
            $newUserId = $this->{$this->field_user_id};
            
        }
        
        if(isset($user) && !$user->canManageEventsForOthers() && $user->{$user->field_pk} != $newUserId)
        {
            
            // Missing rights, the user may not edit events for others
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('10954'));
            
        }
        
        // Copy the actual entry
        {
            
            $copyEntry = clone $this;
            
            $copyEntry->{$copyEntry->field_user_id} = $newUserId;

            unset($copyEntry->{$copyEntry->field_pk});

            $duration = $copyEntry->{$copyEntry->timestamp_end} - $copyEntry->{$copyEntry->timestamp_begin};
            $beginTime = $copyEntry->{$copyEntry->timestamp_begin} - PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($copyEntry->{$copyEntry->timestamp_begin});

            if(!$keepHour)
            {
                                
                if(isset($copyEntry->previous_parent_id) && $copyEntry->previous_parent_id > 0)
                {
                    
                    // Find the time distance to the parent and add it.
                    
                    $parentEntry = new static($copyEntry->previous_parent_id);
                    $parentEntryBeginTime = $parentEntry->{$parentEntry->timestamp_begin} - PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($parentEntry->{$parentEntry->timestamp_begin});
                    
                    $beginTime -= $parentEntryBeginTime;
                    
                }
                else
                {
                    
                    $beginTime = 0;
                    
                }
                
            }
            
            $copyEntry->setTimestampBegin($newTimestamp + $beginTime);
            $copyEntry->setTimestampEnd($newTimestamp + $beginTime + $duration);

            if(isset($this->{$this->field_fk}) && strlen($this->{$this->field_fk}) > 0)
            {
                
                // Create a new group for this new entry, but only if the original had one.
            
                $group = new CalsyCalendarEntryGroup();
                $groupId = $group->save();

                $copyEntry->{$copyEntry->field_fk} = $groupId;

            }

            // Save the new entry...
            $newEntryId = $copyEntry->save();

            // Take over the frontend users with orders
            {

                $userFrontendOrderList = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListObjectsForCalendarEntry($this->{$this->field_pk});

                foreach($userFrontendOrderList as $userFrontendOrder)
                {

                    $newUserFrontendOrder = clone $userFrontendOrder;

                    unset($newUserFrontendOrder->{$newUserFrontendOrder->field_pk});

                    $newUserFrontendOrder->{$newUserFrontendOrder->field_fk} = $newEntryId;

                    $newUserFrontendOrder->save();

                }

            }
        
            // Copy the original entry's flags
            {

                $flags = CalsyCalendarEntryFlag::getFlagsForEntry($this->{$this->field_pk});
                
                foreach($flags as $flagTitle => $flagValue)
                {
                    
                    CalsyCalendarEntryFlag::saveFlagForEntry($newEntryId, $flagTitle, $flagValue);
                 
                }

            }

        }
        
        // Copy eventual children of this entry
        {
            
            $query = "{$this->field_parent_entry_id} = '{$this->{$this->field_pk}}'";

            $childrenEntries = $this->getData($query, $this->field_date_end, "DESC");
            
            for($i = 0; $i < count($childrenEntries); ++ $i)
            {
                
                $childEntry = new static($childrenEntries[$i][$this->field_pk]);
                
                $childEntry->previous_parent_id = $childEntry->{$childEntry->field_parent_entry_id};
                $childEntry->{$childEntry->field_parent_entry_id} = $newEntryId;
                
                $childEntry->copyEntry($newTimestamp, $newUserId, $keepHour);
                
            }
            
        }
        
        return $newEntryId;
        
    }
    
    /**
     * Retrieves free times within the specified list of times and the specified duration.
     * 
     * @author Peter Hamm
     * @param Array $freeTimes
     * @param int $duration In seconds
     * @param int $distance In seconds: How far apart should the appointment proposals be? E.g. '300' for 5 minutes.
     * @return array
     */
    public static function getFreeAppointments($freeTimes, $duration, $distance)
    {
                
        $freeTimesWithDuration = array();
        
        for($i = 0; $i < count($freeTimes); ++$i)
        {
            
            $currentFrom = $freeTimes[$i]['from'];
            $currentTo = $freeTimes[$i]['to'];
            
            $currentTimeBegin = isset($freeTimes[$i]['time_begin']) ? $freeTimes[$i]['time_begin'] : $currentFrom;
            $currentTimeEnd = isset($freeTimes[$i]['time_end']) ? $freeTimes[$i]['time_end'] : $currentTo;
            
            while($currentFrom < $currentTo)
            {
                
                $currentDuration = $duration;
                $currentEnd = $currentFrom + $currentDuration;
                
                if(isset($freeTimes[$i]['time_end']))
                {
                    
                    //$currentDuration = $currentTimeEnd - $currentTimeBegin;
                    
                }
                
                if($currentEnd <= $currentTo)
                {
                    
                    $freeTimeWithDuration = array(

                        "begin" => $currentFrom,
                        "end" => $currentEnd,
                        "duration" => $currentDuration,
                        "distance" => $distance,
                        "label" => CalsyCalendarEntry::getFormattedEventTimeString($currentTimeBegin, $currentEnd, true),
                        "flags" => @$freeTimes[$i]['flags']

                    );

                    array_push($freeTimesWithDuration, $freeTimeWithDuration);

                }
                                
                $currentFrom = $currentEnd;
                
            }
            
        }
                
        return $freeTimesWithDuration;
        
    }
    
    /**
     * Combines the positive list of allowed times on the specified day with the negative list of existing entries
     * to a new positive list with actual free, bookable times.
     * 
     * @author Peter Hamm
     * @param Array $enhancedAllowedTimesOnDay
     * @param Array $existingEntries
     * @return Array
     */
    public static function combineAllowedTimesWithBlockedTimes($enhancedAllowedTimesOnDay, $existingEntries)
    {
        
        $instance = static::getInstance();
                
        $freeTimes = $enhancedAllowedTimesOnDay;
                
        for($i = 0; $i < count($existingEntries); ++$i)
        {
            
            //@todo here
            if(static::isCanceledForAnyUserFrontend($existingEntries[$i][$instance->field_pk]))
            {
                
                continue;
            
            }
            
            $entryBegin = $existingEntries[$i][$instance->timestamp_begin];
            $entryEnd = $existingEntries[$i][$instance->timestamp_end];
        
            for($j = 0; $j < count($freeTimes); ++$j)
            {
                
                if(!isset($freeTimes[$j]))
                {
                    
                    continue;
                    
                }
                
                $bookableTimeBegin = $freeTimes[$j]["from"];
                $bookableTimeEnd = $freeTimes[$j]["to"];
                                
                if($entryBegin > $bookableTimeBegin && $entryEnd < $bookableTimeEnd)
                {
                                                            
                    // Begins and ends within the bounds
                    
                    $newBookableTime = array(
                        
                        "from" => $entryEnd,
                        "to" => $bookableTimeEnd
                        
                    );
                    
                    array_push($freeTimes, $newBookableTime);
                    
                    $freeTimes[$j]["to"] = $entryBegin;
                    
                    
                }
                else if($entryBegin == $bookableTimeBegin && $entryEnd < $bookableTimeEnd)
                {
                            
                    if($entryEnd == $bookableTimeEnd)
                    {
                        
                        unset($freeTimes[$j]);
                        
                    }
                    else
                    {
                        
                        $freeTimes[$j]["from"] = $entryEnd;
                        
                    }
                    
                }
                else if($entryBegin > $bookableTimeBegin && $entryEnd == $bookableTimeEnd)
                {
                    
                    if($entryBegin == $bookableTimeBegin)
                    {
                        
                        unset($freeTimes[$j]);
                        
                    }
                    else
                    {
                        
                        $freeTimes[$j]["to"] = $entryBegin;
                        
                    }
                    
                }
                else if($entryBegin < $bookableTimeBegin && $entryEnd > $bookableTimeEnd)
                {
                    
                    unset($freeTimes[$j]);
                    
                }
                else if($entryBegin < $bookableTimeBegin && $entryEnd > $bookableTimeBegin && $entryEnd <= $bookableTimeEnd)
                {
                    
                    $freeTimes[$j]["from"] = $entryEnd;
                    
                }
                else if($entryBegin == $bookableTimeBegin && $entryEnd >= $bookableTimeEnd)
                {
                    
                    $freeTimes[$j]["from"] = $entryEnd;
                    
                }
                else if($entryBegin > $bookableTimeBegin && $entryBegin < $bookableTimeEnd && $entryEnd > $bookableTimeEnd)
                {
                    
                    $freeTimes[$j]["to"] = $entryBegin;
                    
                }
                
            }
            
        }
        
        // Reindex the array.
        array_splice($freeTimes, 0, 0);
            
        return $freeTimes;
        
    }
    
    /**
     * Retrieves the bookable times for the specified day index from the specified $bookableTimes list.
     * 
     * @author Peter Hamm
     * @param int $dayIndex
     * @param Array $bookableTimes Created by CalsyCalendar::getBookableTimesForUserFrontend()
     * @return array
     */
    protected static function getBookableTimesForDayIndex($dayIndex, $bookableTimes)
    {
        
        $bookableTimesForDay = array();
        
        // Take the values that came as a parameter:
        // These were defined as weekly repetitive times.

        for($i = 0; $i < count($bookableTimes); ++$i)
        {

            if($bookableTimes[$i]["day"] == $dayIndex)
            {

                array_push($bookableTimesForDay, $bookableTimes[$i]);

            }

        }
        
        return $bookableTimesForDay;
        
    }
    
    /**
     * Sorts a list of entries (as associative Arrays) by their start date.
     * 
     * @author Peter Hamm
     * @param Array $entryList Reference
     * @return void
     */
    public static function sortEntriesByDate(&$entryList)
    {
        
        $dummy = static::getInstance();
        $dateList = Array();
        
        for($i = 0; $i < count($entryList); ++$i)
        {
            
            array_push($dateList, $entryList[$i][$dummy->timestamp_begin]);
            
            
        }
        
        array_multisort($dateList, $entryList);
        
    }
            
    /**
     * Tries to automatically generate a title for this entry, 
     * and then sits it to this object - but only if no title is set for it yet (or is an empty string).
     * 
     * @author Peter Hamm
     * @param String $title A default title that may contain variables. See the calendar settings in the backend.
     * @return void
     */
    public function autoGenerateTitle($title)
    {
        
        if(!isset($this->{$this->field_title}) || strlen($this->{$this->field_title}) == 0)
        {
            
            $this->{$this->field_title} = $this->getAutoGeneratedTitle($title);
        
        }        
    
    }
    
    /**
     * Tries to automatically generate a title for this entry.
     * This is related to the Javascript function Calendar.getAutoTitleVariables().
     * 
     * @author Peter Hamm
     * @param String $title
     * @return String
     */
    public function getAutoGeneratedTitle($title)
    {
        
        $possibleVariables = $this->getPossibleAutoGeneratedTitleVariables();
        
        $variablesFound = 0;
        $error = false;
        
        for($i = 0; $i < count($possibleVariables); ++ $i)
        {
            
            if(strpos($title, $possibleVariables[$i]['variable']) !== false)
            {
                
                    // Check if the found variable has any value yet
                // or alternatively, if the value is not in the collection of illegal values.
                // If the check fails, the title is being set empty.

                if(strlen($possibleVariables[$i]['value']) == 0)
                {

                    // This is an empty value

                    $error = true;

                }
                else
                {

                    if(isset($possibleVariables[$i]['illegalValues']) && count($possibleVariables[$i]['illegalValues']) > 0)
                    {

                        // Is this an illegal value?

                        for($j = 0; $j < count($possibleVariables[$i]['illegalValues']); ++$j)
                        {

                            if($possibleVariables[$i]['illegalValues'][$j] == $possibleVariables[$i]['value'])
                            {

                                // This is an illegal value
                            
                                $error = true;

                                break;

                            }

                        }

                    }

                }

                if($error)
                {

                    $title = '';

                    break;

                }
                
                ++$variablesFound;
                
            }
        
        }
        
        if(!$error && $variablesFound > 0)
        {

            // Replace the variables in the string

            for($i = 0; $i < count($possibleVariables); ++$i)
            {

                $newValue = isset($possibleVariables[$i]['displayedValue']) ? $possibleVariables[$i]['displayedValue'] : $possibleVariables[$i]['value'];
                
                $title = str_replace($possibleVariables[$i]['variable'], $newValue, $title);

            }

        }
        
        if(strlen($title) == 0)
        {
            
            // Fallback title
            $title = PerisianLanguageVariable::getVariable(11014);
            
        }

        return $title;
        
    }
    
    /**
     * Retrieves all possible variables (and their values) for the auto-generation of a title for this entry.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected function getPossibleAutoGeneratedTitleVariables()
    {
        
        // Initialize
        {
        
            $userFrontendName = '';
            $areaName = '';
            $areaId = 0;
            
            try
            {

                if(count($this->userFrontendWithOrders) > 0)
                {

                    $userFrontend = new CalsyUserFrontend($this->userFrontendWithOrders[0]['p']);
                    $userFrontendName = $userFrontend->getFullName();

                    $area = new CalsyArea($this->userFrontendWithOrders[0]['a']);
                    $areaName = $area->{$area->field_title};
                    $areaId = $area->{$area->field_pk};

                }
                
            } 
            catch(PerisianException $e) 
            {
                
            }
            
        }
        
        $fields = array(
            
            array(
                
                'variable' => '%userBackendName',
                'value' => CalsyUserBackend::getUserFullname($this->{$this->field_user_id})
                
            ),
            
            array(
                
                'variable' => '%userFrontendName',
                'value' => $userFrontendName
                
            ),
            
            array(
                
                'variable' => '%areaName',
                'illegalValues' => array(
                    
                    '0'
                    
                ),
                'value' => $areaId,
                'displayedValue' => $areaName
                
            )
            
        );
        
        return $fields;
       
    }
    
}
