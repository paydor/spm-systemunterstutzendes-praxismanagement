<?php

/**
 * Access management for API users
 *
 * @author Peter Hamm
 * @date 2016-11-28
 */
class CalsyUserApiAccess extends PerisianDatabaseModel
{
    
    public $table = 'calsy_user_api_access';
    
    public $field_pk = 'calsy_user_api_access_id';
    public $field_api_user_id = 'calsy_user_api_access_calsy_user_api_id';
    public $field_resource = 'calsy_user_api_access_resource';
    public $field_type = 'calsy_user_api_access_type';
    
    protected $searchFields = array();
        
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);
            
        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves a list of all entries
     * 
     * @author Peter Hamm
     * @param int $apiUserId
     * @return array
     */
    public static function getAccessRightsForUserApi($apiUserId)
    {
        
        PerisianFrameworkToolbox::security($apiUserId);
                
        $instance = new self();
                
        $query = $instance->field_api_user_id . ' = "' . $apiUserId . '"';
                
        $results = $instance->getData($query);
        
        // Combine the possible resources with the user's access rights.
        {
            
            $resourceList = array();

            $possibleResources = static::getPossibleResources();
            
            foreach($possibleResources as $possibleResourceName => $possibleResourceValue)
            {
                
                foreach($possibleResourceValue['types'] as $typeKey)
                {

                    $access = false;

                    $typeKey = strtolower($typeKey);
                    
                    for($i = 0; $i < count($results); ++$i)
                    {

                        if($results[$i][$instance->field_resource] == $possibleResourceName && $results[$i][$instance->field_type] == $typeKey)
                        {

                            $access = true;

                            break;

                        }

                    }

                    $possibleResourceValue['access_' . $typeKey] = $access;

                }
                
                $resourceList[$possibleResourceName] = $possibleResourceValue;

            }
            
        }
                
        return $resourceList;
        
    }
    
    /**
     * Retrieves a list of possible resources that API users can access.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getPossibleResources()
    {
        
        $list = array(
            
            'areas' => array(
                
                'name' => PerisianLanguageVariable::getVariable('11015'),
                'types' => array('read', 'write')
                
            ),
            
            'bookable_times' => array(
                
                'name' => PerisianLanguageVariable::getVariable('p57a8e9ed626d3'),
                'types' => array('read', 'write')
                
            ),
            
            'calendar_entry' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10776') . ' & ' . PerisianLanguageVariable::getVariable('10060'),
                'types' => array('read', 'write')
                
            ),
            
            'language_variables' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10018'),
                'types' => array('read', 'write')
                
            ),
            
            'orders' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10793'),
                'types' => array('read', 'write')
                
            ),
            
            'resources' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10908'),
                'types' => array('read', 'write')
                
            ),
            
            'settings_calendar' => array(
                
                'name' => PerisianLanguageVariable::getVariable('11124'),
                'types' => array('read', 'write')
                
            ),
            
            'settings_frontend' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10878'),
                'types' => array('read', 'write')
                
            ),
            
            'settings_system' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10027'),
                'types' => array('read', 'write')
                
            ),
            
            'suppliers' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10795'),
                'types' => array('read', 'write')
                
            ),
            
            'user_backend' => array(
                
                'name' => PerisianLanguageVariable::getVariable('p5a82ffe553a29'),
                'types' => array('read', 'write')
                
            ),
            
            'user_frontend' => array(
                
                'name' => PerisianLanguageVariable::getVariable('10794'),
                'types' => array('read', 'write')
                
            ),
            
            'work_plans' => array(
                
                'name' => PerisianLanguageVariable::getVariable('p5a841a540bddb'),
                'types' => array('read', 'write')
                
            )
            
        );
        
        return $list;
        
    }
    
    /**
     * Deletes all access rights for the API user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $apiUserId
     * @return void
     */
    public function deleteAccessForApiUser($apiUserId)
    {
        
        PerisianFrameworkToolbox::security($apiUserId);
        
        $dummy = new static();
        
        $query = $dummy->field_api_user_id . '="' . $apiUserId . '"';
        
        parent::delete($query);
        
    }
        
    /**
     * Deletes the current entry and its relations
     * 
     * @author Peter Hamm
     * @return void
     */
    public function delete()
    {
                
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} <= 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10823));
            
        }
                
        parent::delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
        
}