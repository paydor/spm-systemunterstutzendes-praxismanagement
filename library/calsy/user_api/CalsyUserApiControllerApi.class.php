<?php

require_once "framework/controller/PerisianControllerRest.class.php";
require_once "CalsyUserApiController.class.php";

/**
 * API controller for API users,
 * handles token generation ann renewal.
 * 
 * @author Peter Hamm
 * @date 2016-11-25
 */
class CalsyUserApiControllerApi extends PerisianControllerRest
{
    
    /**
     * Overridden
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public function authorize() 
    {
        
        return true;
        
    }
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyUserApiController";
        
    }
    
    /**
     * Authenticates an API user and retrieves a token.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function getToken()
    {
                                
        $this->setAction('getToken');
        $result = $this->finalize();
        
        $this->validateTokenResult($result);
        
        return $result;
        
    }
    
    /**
     * Renews an API token that has not yet expired.
     *
     * @author Peter Hamm
     * @url POST /renew
     * @return Array
     */
    public function renewToken()
    {
                
        $this->setAction('renewToken');
        $result = $this->finalize();
        
        $this->validateTokenResult($result);
        
        return $result;
        
    }
    
    /**
     * Validates generated tokens, checks if the API is active and if the API user is a system API user.
     * 
     * @author Peter Hamm
     * @param Array $result 
     * @return Array
     */
    protected function validateTokenResult(&$result)
    {
        
        $userSet = null;
        
        if(isset($result['key']))
        {

            $userSet = $this->setUserApiForToken($result['key']);

        }
                        
        if(!$this->isApiActive() || !$userSet)
        {
                        
            // Missing access rights, because of one of these cases:
            // - The API is inactive
            // - The API is inactive and the API user is not a system API user
            // - The user is unknown 
            
            $result = array(
                
                'code' => 401,
                'message' => PerisianLanguageVariable::getVariable(10954)
                
            );
            
        }
        
        return $result;
        
    }
    
}