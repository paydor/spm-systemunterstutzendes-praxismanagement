<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'CalsyUserApi.class.php';

/**
 * API user controller
 *
 * @author Peter Hamm
 * @date 2016-11-24
 */
class CalsyUserApiController extends PerisianController
{
        
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/user_api/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyUserApi();
        
        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_name);
        $search = $this->getParameter('search', '');
                        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_api/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
                
        //
        
        $result = array(
            
            'list' => CalsyUserApi::getUserApiList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyUserApi::getUserApiCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
                
    }    
    
    /**
     * Stores access rights
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveAccess()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                        
        $editId = $this->getParameter('editId');
        $editEntry = new CalsyUserApi($editId);
        
        $possibleList = CalsyUserApiAccess::getPossibleResources();
        $newAccessData = $this->getParameter('data');
                
        $dummy = new CalsyUserApiAccess();
        $dummy->deleteAccessForApiUser($editId);
        
        foreach($possibleList as $resourceName => $resourceInfo)
        {
                        
            foreach($resourceInfo['types'] as $type)
            {
                
                if(isset($newAccessData[$resourceName]) && $newAccessData[$resourceName][$type] == "1")
                {

                    $accessRight = new CalsyUserApiAccess();

                    $accessRight->{$accessRight->field_api_user_id} = $editId;
                    $accessRight->{$accessRight->field_resource} = $resourceName;
                    $accessRight->{$accessRight->field_type} = $type;

                    $accessRight->save();

                }
                
            }
            
        }
                
        $result = array(
            
            'success' => true,
            'message' => PerisianLanguageVariable::getVariable(10669)
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data to display a form to edit API user's access rights.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditAccess()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_api/edit_access'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                        
        $editId = $this->getParameter('editId');
        $editEntry = new CalsyUserApi($editId);
        
        $accessList = CalsyUserApiAccess::getAccessRightsForUserApi($editId);
        
        $result = array(
            
            'object' => $editEntry,
            'id' => $editId,
            
            'list_access' => $accessList,
            
            'title_form' => str_replace("%", $editEntry->{$editEntry->field_name}, PerisianLanguageVariable::getVariable('p583c9412cf7f5'))
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable('p583718f90fcee'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_api/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                        
        $editId = $this->getParameter('editId');
        $editEntry = new CalsyUserApi($editId);
        
        $result = array(
            
            'object' => $editEntry,
            'id' => $editId,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable('p5837195ccad23')
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to edit an API user's password.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditPassword()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_api/edit_password'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        // Parameter setup
        {
            
            $editId = (int)$this->getParameter('editId');

        }
        
        // Password policy info
        {
            
            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();
            
            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;
                        
        }
        
        $result = array(
            
            'id' => $editId,
            'title_form' => PerisianLanguageVariable::getVariable(10105),
            'security_policy_password' => $passwordPolicyInfo
            
        );
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Saves a new password for an API user
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSavePassword()
    {
        
        $renderer = array();
        
        // Parameter setup
        {
            
            $editId = (int)$this->getParameter('editId');
            $step = (int)$this->getParameter('step');

        }
        
        if($step == 1)
        {
            
            $renderer = array(

                'json' => true

            );
            
            $result = array(

                'success' => false

            );
      
            $passwordOne = $this->getParameter('user_api_password_1');
            $passwordTwo = $this->getParameter('user_api_password_2');
            
            if(PerisianValidation::checkPassword($passwordOne, $passwordTwo) && PerisianValidation::checkId($editId))
            {
                
                $password = PerisianUser::encryptPassword($passwordOne);

                $editObject = new CalsyUserApi($editId);
                $editObject->{$editObject->field_password} = $password;
                
                $result = array(

                    'success' => true,
                    'id' => $editObject->save()

                );
                
            }
            else
            {
                
                $result['message'] = PerisianValidation::getLatestExceptionMessage();
                
            }
            
        }
        else if($step == 2)
        {
            
            $renderer = array(

                'blank_page' => true,
                'page' => 'calsy/user_api/save'

            );
            
            $title = PerisianLanguageVariable::getVariable(10066);
            $message = PerisianLanguageVariable::getVariable(10106) . ' ' . PerisianLanguageVariable::getVariable(10102);

            if(strlen($editId) > 0)
            {

                $title = PerisianLanguageVariable::getVariable(10107);
                $message = PerisianLanguageVariable::getVariable("p576bfc539f679");

            }
            
            $result = array(

                'title' => $title,
                'message' => $message

            );
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Deletes an entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     * @throws PerisianException
     */
    protected function actionDelete()
    {
        
        global $user;
                                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        if(!$user->isAdmin())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
            
        }
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyUserApi($deleteId);
        $entryObj->delete();
        
        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }    
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
     
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            
        }
        
        $saveElement = new CalsyUserApi($editId);
        $saveElement->setObjectDataFromArray($this->getParameters());
                
        $newId = $saveElement->save();
        
        if($newId > 0)
        {
            
            $result = array(
                
                'success' => true, 
                'newId' => $newId,
                'name' => $saveElement->{$saveElement->field_name}
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Retrieves a token for the specified API user credentials.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetToken()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(

            'success' => false

        );
        
        {
            
            $name = $this->getParameter('name');
            $password = $this->getParameter('password');
            
            if(CalsyUserApi::isValidLogin($name, $password))
            {
                
                $tokenResult = CalsyUserApi::getToken($name, $password);
                
                if(!is_null($tokenResult))
                {

                    $result = $tokenResult;

                }
                
            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Creates a new token with a specified token that has not yet been expired.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionRenewToken()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(

            'success' => false

        );
        
        {
            
            $oldToken = $this->getParameter('token');
            
            $tokenResult = CalsyUserApi::renewToken($oldToken);

            if(!is_null($tokenResult))
            {
                
                $result = $tokenResult;
                
            }
            
        }
        
        $this->setResultValue('result', $result);        
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}