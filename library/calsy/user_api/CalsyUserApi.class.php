<?php

require 'CalsyUserApiAccess.class.php';

/**
 * API user
 *
 * @author Peter Hamm
 * @date 2016-11-24
 */
class CalsyUserApi extends PerisianDatabaseModel
{
    
    public $table = 'calsy_user_api';
    
    public $field_pk = 'calsy_user_api_id';
    public $field_name = 'calsy_user_api_name';
    public $field_password = 'calsy_user_api_password';
    public $field_token = 'calsy_user_api_token';
    public $field_token_expires = 'calsy_user_api_token_expires';
    public $field_is_system = 'calsy_user_api_is_system';
    
    protected $searchFields = Array("field_name");
    
    protected $accessRights;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);
            
        }

        return;

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Saves the entry.
     * Will throw an error if a new user is created and an existing user already has the specified name.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return int The ID of the saved entry.
     */
    public function save()
    {
        
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} == 0)
        {
            
            // This is a new entry, check if an API user with this name already exists.
            
            if(strlen($this->{$this->field_name}) == 0 || static::getUserApiCountForName($this->{$this->field_name}) > 0)
            {
                            
                throw new PerisianException(PerisianLanguageVariable::getVariable('p58af2d5ea4e2e'));
                
            }
                        
        }
        
        return parent::save();
        
    }
    
    /**
     * Retrieves the access rights for this API users.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getAccessRights()
    {
        
        if(!isset($this->accessRights))
        {
            
            $this->accessRights = CalsyUserApiAccess::getAccessRightsForUserApi($this->{$this->field_pk});
            
        }
        
        return $this->accessRights;
        
    }
    
    /**
     * Checks if this user has the specified access rights, e.g. for the areas 'areas_read' or 'areas_write'.
     * 
     * @author Peter Hamm
     * @param String $type
     * @return bool
     */
    public function hasAccessRights($type)
    {
        
        if($this->{$this->field_is_system})
        {
            
            return true;
            
        }
        
        $rights = $this->getAccessRights();
        
        $identifier = substr($type, 0, strrpos($type, '_'));
        $accessType = 'access_' . substr($type, strrpos($type, '_') + 1);
        
        $returnValue = false;
        
        if(@$rights[$identifier][$accessType] == 1)
        {
            
            $returnValue = true;
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a list of all entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getUserApiList($search = "", $orderBy = "", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
        
        if($orderBy == "pk")
        {
            
            $orderBy = $instance->field_pk;
            
        }
                
        $results = $instance->getData($query, $orderBy, $order);
        
        return $results;
        
    }
    
    /**
     * Builds a search string
     * 
     * @author Peter Hamm
     * @param String $search
     * @return String
     */
    public function buildSearchString($search)
    {
        
        $query = parent::buildSearchString($search);
        
        $query .= (strlen($query) > 0 ? ' AND ' : '') . "{$this->field_is_system} != 1";
        
        return $query;
        
    }
    
    /**
     * Retrieves the count of all entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getUserApiCount($search = "")
    {
        
        $instance = new static();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of all entries with the specified name.
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getUserApiCountForName($name)
    {
        
        PerisianFrameworkToolbox::security($name);
        
        $instance = new static();
                
        $query = $instance->field_name . '="' . $name . '"';
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an API user by his token.
     * The token must not yet be expired.
     * 
     * @author Peter Hamm
     * @param String $token
     * @return CalsyUserApi
     */
    public static function getUserApiForToken($token)
    {
        
        PerisianFrameworkToolbox::security($token);
        
        $entry = new static();
                
        $query = $entry->field_token . ' = "' . $token . '"';
        $query .= ' AND ' . $entry->field_token_expires . ' > "' . time() . '"';
        
        $results = $entry->getData($query);
        
        if(count($results) > 0)
        {
            
            $entry = new static($results[0][$entry->field_pk]);
            
        }
        
        return $entry;
        
    }
    
    /**
     * Retrieves an API user by his name and password.
     * 
     * @author Peter Hamm
     * @param String $name
     * @param String $password Unencrypted
     * @param bool $ignorePassword Optional, default: false.
     * @return CalsyUserApi
     */
    public static function getUserApiForNameAndPassword($name, $password, $ignorePassword = false)
    {
        
        PerisianFrameworkToolbox::security($name);
        
        $password = CalsyUserBackend::encryptPassword($password);
        
        $entry = new static();
                
        $query = $entry->field_name . ' = "' . $name . '"';
        
        if(!$ignorePassword)
        {
            
            $query .= ' AND ' . $entry->field_password . ' = "' . $password . '"';
                
        }
       
        $results = $entry->getData($query);
        
        if(count($results) > 0)
        {
            
            $entry = new static($results[0][$entry->field_pk]);
            
        }
        
        return $entry;
        
    }
        
    /**
     * Checks if the specified combination of name and password resemble a valid login.
     * 
     * @param String $name
     * @param String $password Unencrypted password
     * @return bool
     */
    public static function isValidLogin($name, $password)
    {
        
        PerisianFrameworkToolbox::security($name);
        PerisianFrameworkToolbox::security($password);
        
        $password = PerisianUser::encryptPassword($password);
        
        $instance = new static();
                
        $query = $instance->field_name . ' = "' . $name . '" AND ' . $instance->field_password . ' = "' . $password . '"';
                        
        $results = $instance->getCount($query);
        
        return $results > 0;
        
    }
    
    /**
     * Generates a new unique token for this entry.
     * 
     * @author Peter Hamm
     * @return boolean True if successful, false if not.
     */
    public function generateNewToken()
    {
        
        if(strlen($this->{$this->field_name}) == 0)
        {
            
            return false;
            
        }
        
        $token = md5($this->{$this->field_name} . uniqid()) . md5(time() . uniqid()) . md5(uniqid());
        
        if(static::tokenExists($token))
        {
            
            // That token already exists in the database, create a new one.
            
            return $this->generateNewToken();
            
        }
            
        $this->{$this->field_token} = $token;
        $this->{$this->field_token_expires} = time() + 30 * 86400;
        
        $this->save();
        
        return true;
        
    }
    
    /**
     * Checks whether the specified $token already exists in the database or not.
     * 
     * @author Peter Hamm
     * @param String $token
     * @return boolean
     */
    public static function tokenExists($token)
    {
        
        PerisianFrameworkToolbox::security($token);
        
        $entry = new static();
                
        $query = $entry->field_token . ' = "' . $token . '"';

        $results = $entry->getData($query);
        
        if(count($results) > 0)
        {
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves or generates a new access
     * token for the specified credentials.
     * 
     * @author Peter Hamm
     * @param String $name
     * @param String $password Unencrypted password
     * @return Array
     */
    public static function getToken($name, $password)
    {
        
        $entry = static::getUserApiForNameAndPassword($name, $password);
                
        if(!isset($entry->{$entry->field_pk}))
        {
            
            return null;
            
        }
        
        $tokenData = array();
        
        if(strlen($entry->{$entry->field_token}) > 0 && $entry->{$entry->field_token_expires} > time())
        {
            
            // A valid token already exists.
            $tokenData['key'] = $entry->{$entry->field_token};
            $tokenData['expires'] = $entry->{$entry->field_token_expires};
            
        }
        else 
        {
            
            // No token exists yet or it has expired... create a new one.
            $tokenData = static::createNewToken($name, $password);
            
        }
        
        return $tokenData;
        
    }
    
    /**
     * Generates a new access token for the specified credentials.
     * The token is going to be valid for 30 days.
     * 
     * @author Peter Hamm
     * @param String $name
     * @param String $password Unencrypted password
     * @return Array
     */
    public static function createNewToken($name, $password)
    {
        
        $entry = static::getUserApiForNameAndPassword($name, $password);
        
        $tokenData = null;
        
        if(!isset($entry->{$entry->field_pk}))
        {
            
            return $tokenData;
            
        }
        
        if($entry->generateNewToken())
        {
            
            $tokenData = array(

                'key' => $entry->{$entry->field_token},
                'expires' => $entry->{$entry->field_token_expires}

            );
            
        }
            
        return $tokenData;
        
    }
    
    /**
     * Generates a new access token for the specified old token.
     * The token is going to be valid for 30 days.
     * 
     * @author Peter Hamm
     * @param String $token A valid, not yet expired existing token.
     * @return Array
     */
    public static function renewToken($oldToken)
    {
        
        $entry = static::getUserApiForToken($oldToken);

        $tokenData = null;
        
        if(!isset($entry->{$entry->field_pk}))
        {
            
            return $tokenData;
            
        }
        
        if($entry->generateNewToken())
        {
            
            $tokenData = array(

                'key' => $entry->{$entry->field_token},
                'expires' => $entry->{$entry->field_token_expires}

            );
            
        }
        
        return $tokenData;
        
    }
    
    /**
     * Deletes the current entry and its relations
     * 
     * @author Peter Hamm
     * @return void
     */
    public function delete()
    {
        
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} <= 0)
        {
            
            // Missing parameters
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10823));
            
        }
        
        if($this->{$this->field_is_system} == 1)
        {
            
            // System users may not be deleted.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58af34126437f'));
            
        }
                
        parent::delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
    /**
     * Tries to load an API user by his name
     * 
     * @author Peter Hamm
     * @param String $name
     * @return void
     */
    public function getUserByName($name)
    {
        
        PerisianFrameworkToolbox::security($name);
        
        $query = $this->field_name . ' = "' . $name . '"';
        
        $this->loadData($query);
        
    }
        
}