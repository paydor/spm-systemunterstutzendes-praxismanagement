<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'CalsyUserBackendGroup.class.php';

/**
 * Backend user group controller
 *
 * @author Peter Hamm
 * @date 2017-04-10
 */
class CalsyUserBackendGroupController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        global $user;
        
        $visibleUsers = CalsyUserBackendGroup::getVisibleUserListForUserId($user->{$user->field_pk});
                
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/user_backend_group/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyUserBackendGroup();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_name);
        $search = $this->getParameter('search', '');
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_backend_group/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $result = array(
            
            'list' => CalsyUserBackendGroup::getGroupList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyUserBackendGroup::getGroupCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a simple list of all entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetGroupList()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $list = CalsyUserBackendGroup::getGroupList();

        usort($list, 'CalsyUserBackendGroup::compareByName');

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable('p58eb75b7de6fa'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/user_backend_group/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $selectedUsers = array();
        $selectedUserIdentifiers = array();
        
        $editId = $this->getParameter('editId');
                
        $editGroup = new CalsyUserBackendGroup($editId);

        $selectedUsers = $editGroup->getUsers(true);
        
        $visibleGroupIdentifiers = explode(",", $editGroup->{$editGroup->field_view_rights});
        $groupList = Array();
        
        if($editId > 0)
        {
            
            $groupDummy = new CalsyUserBackendGroupMember();
            
            $usersInGroupList = CalsyUserBackendGroupMember::getListForGroup($editId);
            $usersInGroupIdentifiers = Array();
            
            for($i = 0; $i < count($usersInGroupList); ++$i)
            {
                
                array_push($usersInGroupIdentifiers, $usersInGroupList[$i][$groupDummy->field_fk_secondary]);
                
            }
            
            for($i = 0; $i < count($selectedUsers); ++$i)
            {
                
                if(in_array($selectedUsers[$i]->{$selectedUsers[$i]->field_pk}, $usersInGroupIdentifiers))
                {
                    
                    array_push($selectedUserIdentifiers, $selectedUsers[$i]->{$selectedUsers[$i]->field_pk});
                
                }

            }
            
            $groupList = CalsyUserBackendGroup::getGroupList();
            
            for($i = 0; $i < count($groupList); ++$i)
            {
                
                if($groupList[$i]['calsy_user_backend_group_id'] == $editId)
                {
                    
                    unset($groupList[$i]);
                    
                    break;
                    
                }
                
            }
            
            sort($groupList);
        
        }
                
        $result = array(
            
            'object' => $editGroup,
            
            'list_users' => CalsyUserBackend::getUserList($dummyUser->field_fullname, "ASC"),
            'list_users_selected' => $selectedUsers,
            'list_users_selected_identifiers' => $selectedUserIdentifiers,
            
            'list_groups' => $groupList,
            'list_groups_selected_identifiers' => $visibleGroupIdentifiers,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable('p58eb75d80f0f7')
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        $deleteId = $this->getParameter('deleteId');

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyUserBackendGroup($deleteId);
        $entryObj->deleteGroup();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            $userList = $this->getParameter('users');
            $viewRights = $this->getParameter('viewRights');
            
        }
        
        {
            
            $saveElement = new CalsyUserBackendGroup($editId);
            
            $saveElement->setObjectDataFromArray($this->getParameters());
            
            if(count($viewRights) > 0)
            {
                
                $saveElement->{$saveElement->field_view_rights} = implode(",", $viewRights);
            
            }
            
            $newId = $saveElement->save();
            
            if(@$newId > 0)
            {

                // Save the group-user relations
                {

                    CalsyUserBackendGroupMember::deleteForGroup($newId);

                    for($i = 0; $i < count($userList); ++$i)
                    {

                        $relation = new CalsyUserBackendGroupMember();

                        $relation->{$relation->field_fk} = $newId;
                        $relation->{$relation->field_fk_secondary} = $userList[$i];

                        $relation->save();

                    }

                    $result = array('success' => true, 'newId' => $newId);

                }

            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}