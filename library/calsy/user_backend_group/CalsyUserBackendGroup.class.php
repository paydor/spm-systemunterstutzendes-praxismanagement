<?php

/**
 * Backend user group handling
 *
 * @author Peter Hamm
 * @date 2017-04-10
 */
class CalsyUserBackendGroup extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_user_backend_group';
    public $field_pk = 'calsy_user_backend_group_id';
    public $field_name = 'calsy_user_backend_group_name';
    public $field_view_rights = 'calsy_user_backend_group_view_rights';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_name');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves a list of all users that are visible for the user with the specified identifier.
     * Note that administrators can always see everybody.
     * Other users may only see the users in their group, respectively the 
     * users from the groups that the group is configured to be allowed to see.
     * 
     * @param int $userId
     * @return array
     */
    public static function getVisibleUserListForUserId($userId)
    {
                
        $userObject = new CalsyUserBackend($userId);
        
        $sortBy = $userObject->field_fullname;
        
        if(PerisianSystemSetting::getSettingValue('system_show_backend_users_sorted_by_key') == 1)
        {
            
            $sortBy =  $userObject->field_sorting_custom . " ASC, " . $sortBy;
            
        }
        
        $visibleUserList = Array();
        
        if(CalsyUserBackendGroupModule::isEnabled() && !$userObject->isAdmin())
        {
            
            // Load the user IDs by the groups
            
            $userGroupMemberDummy = new CalsyUserBackendGroupMember();
            
            $visibleUserIdList = Array();
            $visibleGroupIdList = Array();
            
            {
                
                // First load all of the groups the user can see
                $userGroups = CalsyUserBackendGroupMember::getListForUser($userObject->{$userObject->field_pk});

                for($i = 0; $i < count($userGroups); ++$i)
                {

                    $groupObject = new CalsyUserBackendGroup($userGroups[$i][$userGroupMemberDummy->field_fk]);

                    $visibleGroups = Array($groupObject->{$groupObject->field_pk});
                    $additionalGroups = explode(",", $groupObject->{$groupObject->field_view_rights});
                                        
                    $visibleGroups = array_merge($visibleGroups, $additionalGroups);
                    
                    for($j = 0; $j < count($visibleGroups); ++$j)
                    {
                        
                        if(in_array($visibleGroups[$j], $visibleGroupIdList))
                        {
                            
                            continue;
                            
                        }
                        
                        array_push($visibleGroupIdList, $visibleGroups[$j]);
                        
                    }

                }
            
            }
            
            {
                
                // Load the identifiers of the visible users
                
                for($i = 0; $i < count($visibleGroupIdList); ++$i)
                {
                    
                    $userIdListForGroup = CalsyUserBackendGroupMember::getListForGroup($visibleGroupIdList[$i]);
                    
                    for($j = 0; $j < count($userIdListForGroup); ++$j)
                    {
                        
                        if(in_array($userIdListForGroup[$j][$userGroupMemberDummy->field_fk_secondary], $visibleUserIdList))
                        {
                            
                            continue;
                            
                        }
                        
                        array_push($visibleUserIdList, $userIdListForGroup[$j][$userGroupMemberDummy->field_fk_secondary]);
                        
                    }
                    
                }
                
                $query = "{$userObject->field_pk} IN(" . implode(',', $visibleUserIdList) . ")";
                
                $visibleUserList = $userObject->getData($query, $sortBy, "ASC");
                
            }
            
        }
        else
        {
            
            // Either the group module is deactivated or the user is an administrator
            
            $visibleUserList = $userObject->getData("", $sortBy, "ASC");
            
        }
        
        return $visibleUserList;
        
    }
    
    /**
     * Saves the data.
     * 
     * @author Peter Hamm
     * @param Array $saveFields
     * @return int The identifier of the saved entry.
     */
    public function save($saveFields = Array())
    {
                        
        return parent::save($saveFields);
        
    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Compares two arrays of group data by their "name" key.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByName($a, $b) 
    {
        
        $object = static::getInstance();
        
        return strcasecmp($a[$object->field_name], $b[$object->field_name]);

    }
    
    /**
     * Retrieves a list of groups
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getGroupList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of groups
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getGroupCount($search = "")
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves a group by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyUserBackendGroup
     */
    public static function getGroupById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
    
    /**
     * Retrieves a list of users dedicated for this group, as user objects.
     * 
     * @author Peter Hamm
     * @param bool $showInvisible Show invisible users? Optional. Default: false
     * @return Array
     */
    public function getUsers($showInvisible = false)
    {
        
        return CalsyUserBackendGroupMember::getUsersForGroup($this->{$this->field_pk});
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the group
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteGroup()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
        CalsyUserBackendGroupMember::deleteForGroup($this->{$this->field_pk});
        
    }
    
}

/**
 * Group-user relation
 *
 * @author Peter Hamm
 * @date 2017-04-10
 */
class CalsyUserBackendGroupMember extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_user_backend_group_member';
    public $field_pk = 'calsy_user_backend_group_member_id';
    public $field_fk = 'calsy_user_backend_group_member_group_id';
    public $field_fk_secondary = 'calsy_user_backend_group_member_user_backend_id';
        
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves a list of group-user relations for the specified user identifier
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return array
     */
    public static function getListForUser($userId)
    {
                
        $instance = new static();
                
        PerisianFrameworkToolbox::security($userId);
        $query = $instance->field_fk_secondary . '="' . $userId . '"';
                
        $results = $instance->getData($query);
                
        return $results;
        
    }
    
    /**
     * Retrieves a list of group-user relations for the specified group identifier
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return array
     */
    public static function getListForGroup($groupId)
    {
                
        $instance = new static();
                
        PerisianFrameworkToolbox::security($groupId);
        $query = $instance->field_fk . '="' . $groupId . '"';
                
        $results = $instance->getData($query);
                
        return $results;
        
    }
    
    /**
     * Retrieves the count of group-user relations
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return array
     */
    public static function getCountForGroup($groupId = 0)
    {
        
        $instance = new static();
                
        PerisianFrameworkToolbox::security($groupId);
        $query = $instance->field_fk . '="' . $groupId . '"';
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
        
    /**
     * Retrieves a list of groups related to the specified user identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getGroupsForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        $instance = new static();
        
        $list = static::getListForUser($userId);
        
        $groupList = array();
        
        for($i = 0; $i < count($list); ++$i)
        {
            
            $group = new CalsyUserBackendGroup($list[$i][$instance->field_fk]);
            
            array_push($groupList, $group);
            
        }
        
        return $groupList;
        
    }
        
    /**
     * Retrieves an alphabetically sorted (by the user's full name) 
     * list of users related to the specified group identifier.
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return Array
     */
    public static function getUsersForGroup($groupId = 0)
    {
        
        PerisianFrameworkToolbox::security($groupId);
        
        $instance = new static();
        $userDummy = new CalsyUserBackend();
        
        $userList = array();
        $list = array();
        
        $fieldPrimaryKey = "";
        
        if(strlen($groupId) == 0 || $groupId == 0)
        {
            
            $list = CalsyUserBackend::getUserList();
            
            $fieldPrimaryKey = $userDummy->field_pk;
                        
        }
        else
        {
            
            $list = static::getListForGroup($groupId);
            
            $fieldPrimaryKey = $instance->field_fk_secondary;
            
        }
        
        for($i = 0; $i < count($list); ++$i)
        {
            
            try
            {
                
                $groupUser = new CalsyUserBackend($list[$i][$fieldPrimaryKey]);
                
            }
            catch(PerisianException $e)
            {
                                
                continue;
                
            }
            
            array_push($userList, $groupUser);
            
        }
        
        usort($userList, "static::compareFullname");
        
        return $userList;
        
    }
    
    /**
     * Compares two CalsyBackendUser objects by their full name
     * 
     * @author Peter Hamm
     * @param CalsyUserBackend $a
     * @param CalsyUserBackend $b
     * @return int
     */
    protected static function compareFullname($a, $b)
    {

        return strcmp($a->{$a->field_fullname}, $b->{$b->field_fullname});

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes all entries for the specified group.
     * 
     * @author Peter Hamm
     * @param int $groupId
     * @return void
     */
    public static function deleteForGroup($groupId)
    {
        
        PerisianFrameworkToolbox::security($groupId);
        
        if(strlen($groupId) == 0)
        {
            
            return;
            
        }
        
        $instance = new static();
        
        $instance->delete($instance->field_fk . '="' . $groupId . '"');
        
    }
        
    /**
     * Deletes all entries for the specified user.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return void
     */
    public static function deleteForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        if(strlen($userId) == 0)
        {
            
            return;
            
        }
        
        $instance = new static();
        
        $instance->delete($instance->field_fk_secondary . '="' . $userId . '"');
        
    }
    
}
