<?php

require_once "framework/abstract/PerisianEnhancedFields.class.php";

/**
 * Additional custom fields for appointments
 *
 * @author Peter Hamm
 * @date 2016-04-12
 */
class CalsyAppointmentField extends PerisianEnhancedFields
{
        
    // Main table settings
    public $table = 'calsy_appointment_enhanced_field';
    public $field_pk = 'calsy_appointment_enhanced_field_id';
    public $field_index = 'calsy_appointment_enhanced_field_index';
    public $field_identifier = 'calsy_appointment_enhanced_field_identifier';
    public $field_page = 'calsy_appointment_enhanced_field_page';
    public $field_title = 'calsy_appointment_enhanced_field_title';
    public $field_type = 'calsy_appointment_enhanced_field_type';
    public $field_data = 'calsy_appointment_enhanced_field_data';
    
    protected $prefix = 'calsy_appointment_';
    
    /**
     * Finds a value/path for the area group filter with the specified identifier, to the specified area.
     * 
     * @author Peter Hamm
     * @param String $fieldIdentifier
     * @param int $areaId
     * @return string
     */
    public static function getAreaGroupValueForArea($fieldIdentifier, $areaId)
    {
        
        $areaId = (int)$areaId;
        $fieldIdentifier = PerisianFrameworkToolbox::security($fieldIdentifier);
        
        if($areaId > 0 && strlen($fieldIdentifier) > 0)
        {
            
            $groupDummy = CalsyAreaGroup::getInstance();
            
            $groupList = CalsyAreaGroup::getAreaGroupList();
            $desiredGroupId = null;
            
            foreach($groupList as $group)
            {
                
                $areaList = json_decode($group[$groupDummy->field_areas], true);
                
                if(in_array($areaId, $areaList))
                {
                    
                    $desiredGroupId = (int)$group[$groupDummy->field_pk];
                    
                    break;
                    
                }
                
            }
            
            if($desiredGroupId > 0)
            {
                
                $enhancedField = static::getInstance();
                
                $query = $enhancedField->field_type . ' = "area-group-filter"';
                $query .= " AND ";
                $query .= $enhancedField->field_data . ' LIKE "%\"value\":\"' . $desiredGroupId . '\"%"';
                $query .= " AND ";
                $query .= $enhancedField->field_data . ' LIKE "%\"identifier\":\"' . $fieldIdentifier . '\"%"';
            
                $data = $enhancedField->getData($query);
                                
                if(count($data) > 0)
                {
                    
                    $fieldData = json_decode($data[0][$enhancedField->field_data], true);
                    
                    if(isset($fieldData['conditions']) && count($fieldData['conditions']) > 0)
                    {
                        
                        foreach($fieldData['conditions'] as $condition)
                        {
                            
                            if($condition['identifier'] == $fieldIdentifier)
                            {
                                
                                return $condition['value'];
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
        }
        
        return '';
        
    }
        
}