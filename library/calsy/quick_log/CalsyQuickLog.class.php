<?php

/**
 * Quick logs
 *
 * @author Peter Hamm
 * @date 2020-07-03
 */
class CalsyQuickLog extends PerisianDatabaseModel
{
    
    private $isUpdate = false;
    
    protected static $connectionVmoso;
    
    const TYPE_WATER_EXAMINATION = 'water_examination';
    
    private static $typeFields = Array(
        
        'water_examination' => Array(
            
            'chlorine_free' => Array(
                
                'title' => 'p5f032f0586d86',
                'info' => 'p5f032f11558a0'
                
            ),
            
            'chlorine_bound' => Array(
                
                'title' => 'p5f032f19c5416',
                'info' => 'p5f032f210c55a'
                
            ),
            
            'ph' => Array(
                
                'title' => 'p5f032f2aae493',
                'info' => 'p5f032f3430c96'
                
            ),
            
            'alkalinity' => Array(
                
                'title' => 'p5f032f3d80a06',
                'info' => 'p5f032f46b960d'
                
            )
            
        )
        
    );
    
    private static $typeNames = Array(
        
        'water_examination' => 'p5f032e0472721'
        
    );
    
    private static $typeRights = Array(
        
        'water_examination' => 'account_quick_log_is_water_examiner'
        
    );
        
    // Main table settings
    public $table = 'calsy_quick_log';
    public $field_pk = 'calsy_quick_log_id';
    public $field_fk = 'calsy_quick_log_user_frontend_id';
    public $field_type  = 'calsy_quick_log_type';
    public $field_data  = 'calsy_quick_log_data';
    public $field_time = 'calsy_quick_log_time';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_fk', 'field_data');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Retrieves the custom fields for the specified quick log type.
     * 
     * @author Peter Hamm
     * @param String $type
     * @return Array
     */
    public static function getTypeFields($type)
    {
        
        if(isset(static::$typeFields[$type]))
        {
            
            return static::$typeFields[$type];
            
        }
        
        return Array();
        
    }
    
    /**
     * Retrieves a list of possible rights for types.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getTypeRights()
    {
        
        return static::$typeRights;
        
    }
    
    /**
     * Retrieves whether the user with the specified identifier has access
     * to the quick log feature or not.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return bool
     */
    public static function userFrontendHasQuickLogRights($userFrontendId)
    {
        
        $rights = static::getTypeRightsForUserFrontend($userFrontendId);
        
        return count($rights) > 0;
        
    }
    
    /**
     * Retrieves what quick log types the user with the specified identifier
     * has access to.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return Array
     */
    public static function getTypeRightsForUserFrontend($userFrontendId)
    {
     
        $possibleRights = static::getTypeRights();
        $allowedRights = Array();
        
        foreach($possibleRights as $rightKey => $userSettingKey)
        {
            
            if(CalsyUserFrontendSetting::getSettingValue($userFrontendId, $userSettingKey) == '1')
            {
                
                array_push($allowedRights, $userSettingKey);
                
            }
            
        }
        
        return $allowedRights;
        
    }
    
    /**
     * Retrieves whether the frontend user with the specified identifier has the right
     * to post/edit quick log entries for the specified type or not.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @param String $type For example static::TYPE_WATER_EXAMINATION
     * @return boolean
     */
    public static function userFrontendHasRightForType($userFrontendId, $type)
    {
        
        $rights = static::getTypeRightsForUserFrontend($userFrontendId);
        
        return in_array($type, $rights);
              
    }
    
    /**
     * Retrieves the name of the specified type.
     * 
     * @author Peter Hamm
     * @param String $type
     * @return String
     */
    public static function getTypeName($type)
    {
        
        if(isset(static::$typeNames[$type]))
        {
            
            return lg(static::$typeNames[$type]);
            
        }
        
        return lg('11087');
        
    }
    
    /**
     * Sets the data for this entry, only stores 
     * the values from the Array that are allowed for this object's set type.
     * 
     * @author Peter Hamm
     * @param Array $data
     * @return void
     */
    public function setData($data)
    {
        
        if(is_array($data) && isset($this->{$this->field_type}))
        {
            
            $allowedFields = static::getTypeFields($this->{$this->field_type});
            
            $validData = Array();
            
            foreach($data as $entry)
            {
                
                if(array_key_exists($entry['key'], $allowedFields))
                {
                    
                    $validEntry = $allowedFields[$entry['key']];
                    $validEntry['value'] = $entry['value'];
                    
                    $validData[$entry['key']] = $validEntry;
                    
                }
                
            }
            
            $this->{$this->field_data} = json_encode($validData);
            
        }
        
    }
    
    /**
     * Retrieves a data array from the specified input.
     * 
     * @author Peter Hamm
     * @param mixed $data JSON string or Array
     * @return Array
     */
    private static function getDataArray($data)
    {
        
        if(!is_array($data))
        {
            
            $data = json_decode($data, true);
            
        }
        
        return $data;
        
    }
    
    /**
     * Retrieves formatted data.
     * 
     * @author Peter Hamm
     * @param mixed $data JSON string or Array
     * @return Array
     */
    public static function getFormattedData($data)
    {
        
        $result = Array();
        
        $data = static::getDataArray($data);
        
        foreach($data as $key => $data)
        {
            
            $row = Array(
                
                'key' => $key,
                'title' => lg($data['title']),
                'info' => isset($data['info']) && strlen($data['info']) > 0 ? lg($data['info']) : '',
                'value' => $data['value']
                
            );
            
            array_push($result, $row);
            
        }
        
        return $result;
        
    }
    
    /**
     * Retrieves the data for the specified key.
     * 
     * @author Peter Hamm
     * @param mixed $data JSON string or array
     * @param String $key
     * @return string
     */
    public static function getValueFromData($data, $key)
    {
        
        $data = static::getDataArray($data);
        
        if(isset($data[$key]))
        {
            
            return @$data[$key]['value'];
            
        }
        
        return '';
        
    }
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
        
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $filter Optional, default: null
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '', $filter = null)
    {
        
        PerisianFrameworkToolbox::security($filter);
        
        $query = $this->buildSearchString($search);
        
        $containedAreaIds = Array();
        
        // Filter
        {
            
            if(is_array($filter) && isset($filter['user_frontend']) && is_array($filter['user_frontend']) && count($filter['user_frontend']) == 1 && (int)$filter['user_frontend'][0] > 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_fk . " = \"" . $filter['user_frontend'][0] . "\"";
                
            }
                        
        }
        
        return $query;
        
    }
    
    /**
     * Does a quick log for the frontend user with the specified identifier and data.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @param String $type or example static::TYPE_WATER_EXAMINATION
     * @param Array $data
     * @return boolean
     */
    public static function doQuickLogForUserFrontend($userFrontendId, $type, $data)
    {
        
        $userFrontendId = (int)$userFrontendId;
        PerisianFrameworkToolbox::security($type);
        
        if($userFrontendId <= 0)
        {
            
            return false;
            
        }
        
        $object = new static();
        
        $object->{$object->field_fk} = $userFrontendId;
        $object->{$object->field_time_in} = time();
        $object->{$object->field_type} = $type;
        $object->{$object->field_data} = json_encode($data);
        
        $object->save();
        
        return true;
        
    }
    
    /**
     * Saves this class' field variables to the specified table and database
     *
     * @author Peter Hamm
     * @param Array $saveFields Optional: An array containing names of fields that shall be saved, default: save all fields
     * @return integer The primary key ID of the saved database row - or 0 if the query failed
     */
    public function save($saveFields = Array())
    {
        
        if((int)$this->{$this->field_pk} > 0)
        {
            
            $this->isUpdate = true;
            
        }
        
        return parent::save($saveFields);
        
    }
    
    /**
     * Fired directly after the object was saved to the database.
     * Can be overriden.
     * 
     * @author Peter Hamm
     * @param int $primaryKey Optional, default: ''
     * @return void
     */
    protected function onSaveCompleted($primaryKey = '')
    {
                
        $this->{$this->field_pk} = $primaryKey;
        
        try
        {
            
            $this->postToVmoso();
            
        }
        catch(Exception $e)
        {
            
        }
               
    }
                
    /**
     * Retrieves a list of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param mixed $filter Optional, default: null
     * @return array
     */
    public static function getQuickLogList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0, $filter = null)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param mixed $groupFilter Optional, default: null
     * @return array
     */
    public static function getQuickLogCount($search = "", $filter = null)
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $filter);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
        
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteQuickLog()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
    /**
     * Retrieves a connection to the Vmoso API with the configured credentials.
     * 
     * @author Peter Hamm
     * @return CalsyVmosoApi
     */
    public static function getConnectionVmoso()
    {
        
        if(is_null(static::$connectionVmoso))
        {
            
            $server = PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_connection_server');
            $clientId = PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_connection_cid');
            $user = PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_connection_user');
            $password = PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_connection_password');
            
            static::$connectionVmoso = new CalsyVmosoApi();
            
            static::$connectionVmoso->setServer($server);
            static::$connectionVmoso->setTokenSettingKey('module_setting_calsy_quick_log_synch_vmoso_connection_token_data');
            static::$connectionVmoso->setCredentials($clientId, $user, $password);
                                    
        }
        
        return static::$connectionVmoso;
        
    }
    
    /**
     * Posts the current log entry to Vmoso. 
     * 
     * @author Peter Hamm
     * @return void
     */
    public function postToVmoso()
    {
        
        if(PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_enabled') != "1")
        {
            
            return;
            
        }
                   
        $content = $this->getPostContent();
                
        $connection = static::getConnectionVmoso();
        
        $contentKey = PerisianSystemSetting::getSettingValue('module_setting_calsy_quick_log_synch_vmoso_connection_chat_id');
        
        $connection->postComment($contentKey, $content);
        
    }
    
    /**
     * Generates HTML code to be posted to Vmoso, for this entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function getPostContent()
    {
        
        $filter = null;
        
        {
            
            $table = '<table style="border: 1px solid #ccc" cellpadding="0" cellspacing="0">';

            $table .= '<tr style="background-color: #ccc; color: #000 padding: 12px; border: 1px solid #ccc">';

            $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
            $table .= lg('10964'); // Date and time
            $table .= '</td>';

            $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
            $table .= lg('p5f032cf424ab6'); // Data & values
            $table .= '</td>';

            $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc;">';
            $table .= lg('p5e616611e37fe'); // User
            $table .= '</td>';

            $table .= '<td style="padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px;">';
            $table .= lg('p5f0333c01b430'); // Type
            $table .= '</td>';

            $table .= '<tr>';

            {
                
                $entryData = static::getFormattedData($this->{$this->field_data});
                
                $backgroundColor = '#fff';

                $row = '<tr style="background-color: ' . $backgroundColor . '">';

                $row .= '<td style="vertical-aign: top; padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: center;">';
                $row .= date("d.m.Y - H:i", $this->{$this->field_time}) . ' ' . lg('10903'); // Date and time
                $row .= '</td>';

                $row .= '<td style="vertical-aign: top; padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: left;">';
                
                foreach($entryData as $dataEntry)
                {
                    
                    if(count($entryData) > 0)  
                    {

                        $row .= '<b>' . $dataEntry['title'] . ':</b> ' . $dataEntry['value'] . '<br/>';

                    }
                    else
                    {

                        $row .= lg('p5f035ba9c899f');

                    }
                    
                }
                
                $row .= '</td>';

                $row .= '<td style="vertical-aign: top; padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; border-right: 1px solid #ccc; text-align: left;">';
                $row .= CalsyUserFrontend::getFullnameForUserById($this->{$this->field_fk}); // User
                $row .= '</td>';

                $row .= '<td style="vertical-aign: top; padding-left: 12px; padding-right: 12px; padding-top: 6px; padding-bottom: 6px; text-align: left;">';
                $row .= static::getTypeName($this->{$this->field_type}); // Type
                $row .= '</td>';

                $row .= '</tr>';

                $table .= "\n" . $row;

                ++$rowCount;

            }

            $table .= '</table>';

            $content = lg($this->isUpdate ? 'p5f036310852bd' : 'p5f03631dd5ce7');
            $content .= "<br/>\n<br/>\n" . $table;


        }
        
        return $content;
        
    }
    
}
