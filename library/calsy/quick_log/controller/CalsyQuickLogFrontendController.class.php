<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/quick_log/CalsyQuickLog.class.php';

/**
 * Quick log frontend controller
 *
 * @author Peter Hamm
 * @date 2020-07-06
 */
class CalsyQuickLogFrontendController extends PerisianController
{
        
    /**
     * Does a quick checkin for the currently logged in frontend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    public function actionQuickLog()
    {
        
        global $userFrontend;
        
        $renderer = Array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $type = '';
            $data = Array();
                        
            $success = CalsyQuickLog::doQuickLogForUserFrontend($userFrontend->{$userFrontend->field_pk}, $type, $data);
            
        }
        
        $result = Array(
            
            'success' => $success
            
        );
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'frontend/calsy/quick_log/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend;
     * @return void
     */
    protected function actionList()
    {
        
        global $userFrontend;
        
        $dummy = new CalsyQuickLog();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        $sorting = $this->getParameter('sorting', $dummy->field_time);
        $search = $this->getParameter('search', '');
        $userFilter = $userFrontend->{$userFrontend->field_pk};
        $filterUserFrontend = $userFrontend;
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/quick_log/list'
            
        );
        
        $filter = Array(
            
            'user_frontend' => Array($userFilter)
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyQuickLog::getQuickLogList($search, $sorting, $sortOrder, $offset, $limit, $filter),
            'count' => CalsyQuickLog::getQuickLogCount($search, $filter),
                        
            'dummy' => $dummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            'filter' => $filter,
            'filter_user_frontend' =>$filterUserFrontend
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves information for different sources in JSON format.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $result = array(
            
            'success' => false
            
        );
        
        if($source == "user_frontend")
        {
            
            if(CalsyUserFrontendModule::isEnabled()) 
            {
                                
                $userFrontendDummy = new CalsyUserFrontend();
                
                $result = CalsyUserFrontend::getUserFrontendListFormatted("", "calsy_user_frontend_fullname", "ASC", 0, 0, $this->getParameter('showDefaultUser') != 'false');
                
            }
            
        }
        else if($source == "entry")
        {
                        
            $entry = new CalsyQuickLog($this->getParameter('e'));
            $result = $entry->getSummary();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', lg('p5f033398c58fa'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummy = new CalsyQuickLog();
        $dummyUser = new CalsyUserFrontend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/quick_log/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                        
        $editEntry = new CalsyQuickLog($this->getParameter('editId'));

        $result = array(
            
            'dummy' => $dummy,
            'object' => $editEntry,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : lg('10307'),
            
            'dummy_user' => $dummyUser
            
        );
                        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        $entry = new CalsyQuickLog($deleteId);
        
        if((int)($entry->{$entry->field_fk}) != $userFrontend->{$userFrontend->field_pk})
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyQuickLog($deleteId);
        $entryObj->deleteQuickLog();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionSave()
    {
        
        global $userFrontend;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');            
        }
        
        {
                        
            $saveElement = new CalsyQuickLog($editId);
            
            $saveElement->setObjectDataFromArray($this->getParameters());
            
            $saveElement->{$saveElement->field_fk} = $userFrontend->{$userFrontend->field_pk};
            
            $saveElement->setData($this->getParameter('type_fields'));
                        
            $newId = $saveElement->save();
            
            $result = Array(
                
                'success' => true, 
                'newId' => $newId
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}