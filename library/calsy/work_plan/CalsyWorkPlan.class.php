<?php

require_once 'calsy/calendar/CalsyCalendar.class.php';

/**
 * Work plan
 *
 * @author Peter Hamm
 * @date 2017-12-15
 */
class CalsyWorkPlan extends PerisianDatabaseModel
{
    
    const FLAG_WORK_PLAN_ID = 'work_plan_id';
    const FLAG_WORK_PLAN_ELEMENT_ID = 'work_plan_element_id';
    const FLAG_WORK_PLAN_ELEMENT_AREA_ID = 'work_plan_element_area_id';
    const FLAG_ENTRY_WORK_PLAN_ID = 'work_plan_entry_id';
        
    // Main table settings
    public $table = 'calsy_work_plan';
    public $field_pk = 'calsy_work_plan_id';
    public $field_title = 'calsy_work_plan_title';
    public $field_elements = 'calsy_work_plan_elements';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title');
        
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
                        
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Removes an applied work plan with the specified identifier for the week around
     * the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $timestampInWeek
     * @param int $workPlanId
     * @return bool
     */
    public static function deleteForWeek($timestampInWeek, $workPlanId)
    {
        
        $success = false;
        
        if($workPlanId <= 0 || (int)$timestampInWeek <= 0)
        {
                        
            return false;
            
        }
        
        //
                
        // Load the existing entries for the week
        {
            
            $calendarData = static::getCalendarByType('week', $timestampInWeek, $workPlanId);
            
            if(count($calendarData['entries']) > 0)
            {
                
                $calendarEntryDummy = new CalsyCalendarEntry();
                
                $groupId = $calendarData['entries'][0][$calendarEntryDummy->field_fk];
                
                if($groupId > 0)
                {
                    
                    $calendarEntryDummy->deleteGroup($groupId);
                    
                }
                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Applies the work plan to the week spanning around the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $timestampInWeek
     * @return bool
     */
    public function applyForWeek($timestampInWeek)
    {
        
        $success = false;
        
        if((int)$this->{$this->field_pk} <= 0 || (int)$timestampInWeek <= 0)
        {
                        
            return false;
            
        }
        
        $elements = $this->getElements();
        
        $timestampWeekBegin = CalsyCalendar::getFirstDayOfTypeAsTimestamp('week', $timestampInWeek);
                
        if(count($elements) > 0)
        {
            
            $entryGroup = new CalsyCalendarEntryGroup();
            $entryGroupId = $entryGroup->save();
            
            for($i = 0; $i < count($elements); ++$i)
            {

                $timestampFrom = $timestampWeekBegin + ((int)$elements[$i]['day_from'] * 86400) + (int)$elements[$i]['time_from'];
                $timestampTo = $timestampWeekBegin + ((int)$elements[$i]['day_to'] * 86400) + (int)$elements[$i]['time_to'];

                // Build the calendar entry
                {

                    $calendarEntry = new CalsyCalendarEntry();

                    $calendarEntry->{$calendarEntry->field_title} = $elements[$i]['title'];
                    $calendarEntry->{$calendarEntry->field_fk} = $entryGroupId;

                    $calendarEntry->setTimestampBegin($timestampFrom);
                    $calendarEntry->setTimestampEnd($timestampTo);

                }

                $entryId = $calendarEntry->save();

                CalsyCalendarEntryFlag::saveFlagForEntry($entryId, static::FLAG_WORK_PLAN_ID, $this->{$this->field_pk});
                CalsyCalendarEntryFlag::saveFlagForEntry($entryId, static::FLAG_WORK_PLAN_ELEMENT_ID, $elements[$i]['id']);
                CalsyCalendarEntryFlag::saveFlagForEntry($entryId, static::FLAG_WORK_PLAN_ELEMENT_AREA_ID, $elements[$i]['area_id']);

            }
            
        }
        
        return true;
        
    }
        
    /**
     * Takes calendar data for a month and checks if there are work plan entries
     * in the week starting from $dayNumber within the $calendarData Array.
     * 
     * @author Peter Hamm
     * @param Array $calendarData
     * @param int $dayNumber
     * @return boolean
     */
    public static function weekInMonthHasEntries($calendarData, $dayNumber)
    {
        
        $weekNumberInMonth = floor($dayNumber / 7);
        $dayNumber = $weekNumberInMonth * 7;
        
        for($i = $dayNumber; $i < $dayNumber + 7; ++$i)
        {
            
            if(count($calendarData[$i]['entries']) > 0)
            {

                return true;

            }
            
        }
        
        
        return false;
        
    }
    
    /**
     * Retrieves a work plan calendar for the specified timespan and work plan ID.
     * 
     * @author Peter Hamm
     * @param String $type Can be "year", "month", "week" or "day"
     * @param int $timestamp
     * @param int $workPlanId
     * @return Array
     */
    public static function getCalendarByType($type, $timestamp, $workPlanId)
    {
                
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
                        
            $filter->setShowNoDetail($type == 'year');
            $filter->addFlag(CalsyWorkPlan::FLAG_WORK_PLAN_ID, $workPlanId);
            
        }

        $calendarData = CalsyCalendar::getCalendarByType($type, $timestamp, $filter);
        
        return $calendarData;
        
    }
    
    /**
     * Takes an array of work plan elements and sets it for this object.
     * 
     * @author Peter Hamm
     * @param Array $workPlanElements
     * @return void
     */
    public function setElements($workPlanElements)
    {
                
        if(!is_array($workPlanElements))
        {
            
            return;
            
        }
        
        for($i = 0; $i < count($workPlanElements); ++$i)
        {
            
            if(!isset($workPlanElements[$i]['id']) || strlen($workPlanElements[$i]['id']) == 0)
            {
                
                $workPlanElements[$i]['id'] = uniqid('wp_');
                
            }
            
            $workPlanElements[$i]['title'] = htmlentities($workPlanElements[$i]['title']);
            
        }
        
        $this->{$this->field_elements} = json_encode($workPlanElements);
        
    }
    
    /**
     * Retrieves the elements of the work plan.
     * 
     * @author Peter Hamm
     * @param bool $raw Return the raw JSON? Optional, default: false
     * @return mixed JSON string if $raw is true, else: decoded JSON as Array.
     */
    public function getElements($raw = false)
    {
        
        $workPlanElements = $this->{$this->field_elements};
        
        $workPlanElements = static::decodeWorkPlanElements($workPlanElements);
        
        if($raw)
        {
            
            $workPlanElements = json_encode($workPlanElements, true);
            
        }
        
        return $workPlanElements;
        
    }
    
    /**
     * Takes an encoded string of work plan elements and returns the value decoded.
     * 
     * @author Peter Hamm
     * @param String $encodedWorkPlanElements
     * @return Array
     */
    public static function decodeWorkPlanElements($encodedWorkPlanElements)
    {
        
        if(strlen($encodedWorkPlanElements) < 3)
        {
            
            $encodedWorkPlanElements = '[]';
            
        }
        
        $workPlanElements = json_decode($encodedWorkPlanElements, true);

        if(!is_array($workPlanElements))
        {

            $workPlanElements = Array();

        }
        
        for($i = 0; $i < count($workPlanElements); ++$i)
        {
            
            $workPlanElements[$i]['time_formatted'] = gmdate("H:i", $workPlanElements[$i]["time_from"]) . ' - ' . gmdate("H:i", $workPlanElements[$i]["time_to"]);
            $workPlanElements[$i]['title'] = html_entity_decode($workPlanElements[$i]['title']);
                     
        }
        
        return $workPlanElements;
        
    }
    
    /**
     * Retrieves a list of work plans
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getWorkPlanList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Retrieves the title of the work plan with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getTitleForIdentifier($id)
    {
        
        $entry = new static($id);
        
        return $entry->{$entry->field_title};
        
    }
    
    /**
     * Deletes the work plan and all related calendar entries.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function deleteWorkPlan()
    {
        
        if($this->{$this->field_pk} <= 0)
        {
            
            return false;
            
        }
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
        // @todo: Delete all related calendar entries
        
        return true;
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
}
