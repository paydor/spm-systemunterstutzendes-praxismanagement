<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyWorkPlanController.class.php";

class CalsyWorkPlanControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyWorkPlanController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyWorkPlan";
        
    }
    
    /**
     * Handles custom routes.
     * 
     * @author Peter Hamm
     * @param String $route
     * @return Array
     */
    protected function handleRoute($route)
    {
        
        if(strtolower($route) == 'apply')
        {

            return $this->applyForWeek();

        }
        
        if(strtolower($route) == 'unapply')
        {

            return $this->unapplyForWeek();

        }
        
        if(strtolower($route) == 'applications')
        {

            return $this->getApplicationListForYear();

        }
        
        $error = Array(
            
            'success' => false,
            'error' => PerisianLanguageVariable::getVariable('10112') . '"' . $route . '"'
            
        );
        
        return $error;
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
                
        if((int)$identifier <= 0)
        {
                          
            return $this->handleRoute($identifier);
            
        }
        
        $this->requireAccess('work_plans_read');
        
        try
        {

            $data = parent::getObject($identifier);

            $dummy = $this->getDatabaseObject();

            if(isset($data[$dummy->field_elements]))
            {

                $data[$dummy->field_elements] = CalsyWorkPlan::decodeWorkPlanElements($data[$dummy->field_elements]); 

            }
            
        }
        catch(PerisianException $e)
        {
            
            $data = Array(
                
                'success' => false,
                'error' => PerisianLanguageVariable::getVariable('10223')
                
            );
            
        }

        return $data;
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('work_plans_write');
        
        return $this->updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('work_plans_write');
             
        $object = $this->getObjectWithFieldData($identifier);
                
        {
            
            $elements = PerisianFrameworkToolbox::getRequest($object->field_elements);
            $elements = html_entity_decode(stripslashes($elements));
                        
            $elements = @json_decode($elements, true);
                 
            if($elements == false)
            {
                
                $error = Array(
                    
                    'succes' => false,
                    'error' => PerisianLanguageVariable::getVariable('p5a8420e06943f'),
                    'debug' => PerisianControllerRestObject::getLastJsonError()
                    
                );
                
                return $error;
                
            }
            
            $object->setElements($elements);
                        
        }
        
        $result = $this->saveObject($object);
                
        return $result;
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
                        
        if((int)$identifier <= 0)
        {
                                      
            return $this->handleRoute($identifier);
            
        }
        
        $this->requireAccess('work_plans_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('work_plans_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteWorkPlan();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('work_plans_read');
        
        $this->setAction('getListWorkPlans');
        
        $result = $this->finalize();
        
        // Formatting
        {

            $dummy = $this->getDatabaseObject();

            for($i = 0; $i < count($result['list']); ++$i)
            {
                
                $result['list'][$i][$dummy->field_elements] = CalsyWorkPlan::decodeWorkPlanElements($result['list'][$i][$dummy->field_elements]);                

            }

        }
                        
        return $result;
        
    }
    
    /**
     * Applies a work plan to the week that contains the given timestamp.
     *
     * @author Peter Hamm
     * @url POST /apply
     * @return Array
     */
    public function applyForWeek()
    {
        
        $this->requireAccess('work_plans_write');
        
        {
            
            $workPlanId = PerisianFrameworkToolbox::getRequest('work_plan_id');
            $timestampInWeek = PerisianFrameworkToolbox::getRequest('timestamp');
            
            if($workPlanId <= 0 || is_null($timestampInWeek))
            {

                $result = Array(

                    'success' => false, 
                    'error' => PerisianLanguageVariable::getVariable('10823')

                );

                return $result;

            }
            
            PerisianFrameworkToolbox::setRequest('p', $workPlanId);
            PerisianFrameworkToolbox::setRequest('t', $timestampInWeek);
            
        }
        
        $this->setAction('applyWorkPlanForWeek');
                
        $result = $this->finalize();
                                
        return $result;
        
    }
    
    /**
     * Unapplies a work plan to the week that contains the given timestamp.
     *
     * @author Peter Hamm
     * @url DELETE /unapply
     * @return Array
     */
    public function unapplyForWeek()
    {
        
        $this->requireAccess('work_plans_write');
        
        {
            
            $workPlanId = PerisianFrameworkToolbox::getRequest('work_plan_id');
            $timestampInWeek = PerisianFrameworkToolbox::getRequest('timestamp');
            
            if($workPlanId <= 0 || is_null($timestampInWeek))
            {

                $result = Array(

                    'success' => false, 
                    'error' => PerisianLanguageVariable::getVariable('10823')

                );

                return $result;

            }
            
            PerisianFrameworkToolbox::setRequest('p', $workPlanId);
            PerisianFrameworkToolbox::setRequest('t', $timestampInWeek);
            
        }
        
        $this->setAction('deleteWorkPlanForWeek');
        
        $result = $this->finalize();
                                
        return $result;
        
    }
    
    /**
     * Gets a list calendar entries for the specified applied work plan in the specified year.
     *
     * @author Peter Hamm
     * @url GET /applications
     * @return Array
     */
    public function getApplicationListForYear()
    {
        
        $this->requireAccess('work_plans_read');
        
        {
            
            $workPlanId = PerisianFrameworkToolbox::getRequest('work_plan_id');
            $timestampInYear = PerisianFrameworkToolbox::getRequest('timestamp');
            
            if($workPlanId <= 0 || is_null($timestampInYear))
            {

                $result = Array(

                    'success' => false, 
                    'error' => PerisianLanguageVariable::getVariable('10823')

                );

                return $result;

            }
            
            PerisianFrameworkToolbox::setRequest('p', $workPlanId);
            PerisianFrameworkToolbox::setRequest('t', $timestampInYear);
            
        }
        
        $this->setAction('getWorkPlanApplicationYear');
        
        $result = $this->finalize();
        
        // Formatting
        {
            
            unset($result['object']);
            unset($result['data_calendar']['matrix']);
            
        }
                        
        return $result;
        
    }
    
}
