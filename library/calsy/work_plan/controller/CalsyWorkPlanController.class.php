<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/work_plan/CalsyWorkPlan.class.php';

/**
 * Work plan controller
 *
 * @author Peter Hamm
 * @date 2017-12-15
 */
class CalsyWorkPlanController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
                
        $renderer = array(
            
            'page' => 'calsy/work_plan/overview',
            'page_menu_right' => 'calsy/work_plan/menu/right'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $planId = $this->getParameter('p', -1);
        $dummy = CalsyWorkPlan::getInstance();
        $listWorkPlans = CalsyWorkPlan::getWorkPlanList('', $dummy->field_title);
        
        //
        
        $result = array(
            
            'id' => $planId,
            'dummy' => $dummy,
            'list_work_plans' => $listWorkPlans
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data for a form to adda new work plan
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->actionEdit();
        
        //
                
        $result = $this->getResultValue('result');
        
        $result['title_form'] = PerisianLanguageVariable::getVariable('p5a343269d271f');
        
        $this->setResultValue('result', $result);
        
    }    
    
    /**
     * Provides data for a form to adda new work plan
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/new'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        
        $planId = $this->getParameter('p', -1);
        $object = new CalsyWorkPlan($planId > 0 ? $planId : null);
                
        $result = array(
                        
            'id' => $planId,
            'object' => $object,
            'title_form' => PerisianLanguageVariable::getVariable('p5a343b35217c3')
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes / unapplies the work plan with the given identifier from the week
     * that contains the given timestamp.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionDeleteWorkPlanForWeek()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => ''
            
        );
        
        {
            
            $planId = $this->getParameter('p', -1);
            $timestampInWeek = $this->getParameter('t', -1);
            
            $result['success'] = CalsyWorkPlan::deleteForWeek($timestampInWeek, $planId);
            $result['message'] = PerisianLanguageVariable::getVariable($result['success'] ? 'p576989fe6143f' : 'p5a4d53dd62a7d');
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Applies the work plan with the specified ID to the week that spans around the
     * specified timestamp.
     * 
     * @author Peter Hamm
     * @return void 
     */
    protected function actionApplyWorkPlanForWeek()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => ''
            
        );
        
        {
            
            $planId = $this->getParameter('p', -1);
            $timestampInWeek = $this->getParameter('t', -1);
            
            $object = new CalsyWorkPlan($planId > 0 ? $planId : null);
            
            $result['success'] = $object->applyForWeek($timestampInWeek);
            $result['message'] = PerisianLanguageVariable::getVariable($result['success'] ? 'p576989fe6143f' : 'p5a4d60b31e8c1');
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a year for the specified work plan, to see and control
     * its applications.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetWorkPlanApplicationYear()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/work_plan_application_year'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $planId = $this->getParameter('p', -1);
            $timestampInYear = $this->getParameter('t', -1);
            $object = new CalsyWorkPlan($planId > 0 ? $planId : null);
                    
            $calendarData = CalsyWorkPlan::getCalendarByType('year', $timestampInYear, $planId);
            
        }
                
        $result = array(
                        
            'id' => $planId,
            'object' => $object,
            'time' => $timestampInYear,
            'data_calendar' => $calendarData,
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to mnage the applications of a work plan.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionApplyWorkPlan()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/apply_work_plan'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $planId = $this->getParameter('p', -1);
        $object = new CalsyWorkPlan($planId > 0 ? $planId : null);
                
        $result = array(
                        
            'id' => $planId,
            'object' => $object,
            'title_form' => PerisianLanguageVariable::getVariable('p5a3fdfc2be2b9')
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves the list of existing work plans.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetListWorkPlans()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $dummy = new CalsyWorkPlan();
        
        $listWorkPlans = CalsyWorkPlan::getWorkPlanList('', $dummy->field_title);
        
        $result = Array(
            
            'list' => $listWorkPlans
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to add a new work plan element.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionAddWorkPlanElement()
    {
        
        $this->actionEditWorkPlanElement();
        
        //
        
        {
            
            $dayFrom = (int)$this->getParameter('day', 0);
            $timeFrom = (int)$this->getParameter('time', 3600 * 8);
            
            $dayTo = $dayFrom;
            $timeTo = $timeFrom + 3600;
            
            while($timeTo >= 86400)
            {
                
                ++$dayTo;
                $timeTo -= 86400;
                
            }
            
        }
        
        $result = $this->getResultValue('result');
        
        $result['title_form'] = PerisianLanguageVariable::getVariable('p5a369f3c2e934');
        $result['day_from'] = $dayFrom;
        $result['time_from'] = $timeFrom;
        $result['day_to'] = $dayTo;
        $result['time_to'] = $timeTo;
        
        $this->setResultValue('result', $result);        
        
    }
    
    /**
     * Provides data to display a form to edit a work plan element.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditWorkPlanElement()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/new_element'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $dayFrom = 0;
            $timeFrom = 3600 * 8;
            $dayTo = 0;
            $timeTo = $timeFrom + 3600;
                        
            $elementId = $this->getParameter('e', '');
            $planId = $this->getParameter('p', -1);
            $object = new CalsyWorkPlan($planId > 0 ? $planId : null);
            
            // Selectable areas
            {

                $dummyArea = new CalsyArea();
                $areaList = CalsyArea::getAreaList("", $dummyArea->field_title);

            }
            
            $elementList = $object->getElements();
            $element = null;
            
            if(strlen($elementId) > 0)
            {
                
                for($i = 0; $i < count($elementList); ++$i)
                {

                    if($elementList[$i]['id'] == $elementId)
                    {
                        
                        $element = $elementList[$i];
                        
                        $dayFrom = $element['day_from'];
                        $timeFrom = $element['time_from'];
                        
                        $dayTo = $element['day_to'];
                        $timeTo = $element['time_to'];
                        
                        break;
                        
                    }

                }
                
            }

        }
                
        $result = array(
                        
            'id' => $elementId,
            'work_plan_id' => $planId,
                        
            'object' => $object,
            'element' => $element,
            
            'day_from' => $dayFrom,
            'time_from' => $timeFrom,
            'day_to' => $dayTo,
            'time_to' => $timeTo,
            
            'list_days' => CalsyCalendar::getDayNames(),
            
            'title_form' => PerisianLanguageVariable::getVariable('p5a369fc02b768')
            
        );
        
        if(CalsyAreaModule::isEnabled())
        {
            
            $result['list_areas'] = $areaList;
            $result['dummy_area'] = $dummyArea;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an element from a work plan.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionDeleteWorkPlanElement()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $success = false;
            
            $workPlanId = $this->getParameter('p', 0);
            $elementId = $this->getParameter('e');
            
            $workPlan = new CalsyWorkPlan($workPlanId);
            
            if(strlen($elementId) > 0)
            {
                
                $oldElementList = $workPlan->getElements();
                
                for($i = 0; $i < count($oldElementList); ++$i)
                {
                    
                    if($oldElementList[$i]['id'] == $elementId)
                    {
                        
                        unset($oldElementList[$i]);
                        
                        // Reindex
                        $oldElementList = array_values($oldElementList);
                        
                        break;
                        
                    }
                    
                }
                
                $workPlan->setElements($oldElementList);
                
                $workPlan->save();
                
            }
            
            $elementList = $workPlan->getElements();
            
            $success = true;
            
        }
        
        $result = Array(
            
            'id' => $workPlanId,
            'success' => $success,
            
            'work_plan_elements' => $elementList
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Sets the work plan elements for a work plan.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveWorkPlanElements()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        {
            
            $workPlanId = $this->getParameter('p', 0);
            $workPlanElements = $this->getParameter('elements');
            
            $workPlan = new CalsyWorkPlan($workPlanId);
            $workPlan->setElements($workPlanElements);
            $workPlan->save();
            
            $elementList = $workPlan->getElements();
            
        }
        
        $result = Array(
            
            'id' => $workPlanId,
            'success' => true,
            
            'work_plan_elements' => $elementList
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a work plan.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetWorkPlan()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/work_plan_week'
            
        );
        
        //
        
        $result = Array();
        
        $workPlanId = $this->getParameter('p', 0);
        
        $systemColors = PerisianSystemConfiguration::getSystemColors();
        
        if($workPlanId >= 0)
        {

            $workPlan = new CalsyWorkPlan($workPlanId);

            $result = Array(

                'id' => $workPlanId,
                'object' => $workPlan,
                'elements' => $workPlan->getElements(true),
                'element_color' => $systemColors["main_alternative_two"]

            );
        
        }
        else
        {
            
            $renderer['page'] = 'calsy/work_plan/work_plan_empty';
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves work plan details.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetWorkPlanDetails()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/work_plan/menu/work_plan_details'
            
        );
        
        //
        
        $result = Array();
        
        $workPlanId = $this->getParameter('p', 0);
        
        if($workPlanId > 0)
        {
                    
            $workPlan = new CalsyWorkPlan($workPlanId);

            $result = Array(

                'id' => $workPlanId,
                'object' => $workPlan

            );
            
        }
        else
        {
            
            $renderer['page'] = 'calsy/work_plan/menu/work_plan_details_empty';
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('p');
            
        }
        
        {
                        
            $saveElement = new CalsyWorkPlan($editId);
            $saveElement->{$saveElement->field_title} = $this->getParameter($saveElement->field_title);
                        
            $newId = $saveElement->save();
                        
        }
        
        $result = array(
            
            'success' => true, 
            'id' => $newId
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionDeleteWorkPlan()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $deleteId = $this->getParameter('p');
            
        }
        
        {
                        
            $deleteElement = new CalsyWorkPlan($deleteId);
            
            $success = $deleteElement->deleteWorkPlan();
                        
        }
        
        $result = array(
            
            'success' => $success,
            'id' => $deleteId
                
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}