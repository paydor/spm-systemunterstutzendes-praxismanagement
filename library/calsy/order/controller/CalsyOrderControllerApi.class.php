<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyOrderController.class.php";

class CalsyOrderControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyOrderController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyOrder";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('orders_read');
        
        try
        {
        
            $data = parent::getObject($identifier);
            
            $formattedData = Array();
            
            foreach($data as $key => $value)
            {
                                
                if(substr($key, 0, 11) == "calsy_order")
                {
                    
                    $formattedData[$key] = $data[$key];
                    
                }
                
            }
        
        }
        catch(PerisianException $e)
        {
            
            $formattedData = Array(
                
                'success' => false,
                'error' => PerisianLanguageVariable::getVariable('10223')
                
            );
            
        }
        
        return $formattedData;
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('orders_write');
        
        return $this->updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('orders_write');
        
        $dummy = $this->getDatabaseObject();
                
        return parent::updateObject($identifier);
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('orders_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('orders_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteWithRelations();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('orders_read');
        
        $this->setAction('list');
        
        // Filter setup
        {

            $filterUserFrontendId = PerisianFrameworkToolbox::getRequest('filter_user_frontend_id');

            if($filterUserFrontendId > 0)
            {

                PerisianFrameworkToolbox::setRequest('p', $filterUserFrontendId);

            }

            $filterSupplierId = PerisianFrameworkToolbox::getRequest('filter_supplier_id');
            
            if($filterSupplierId > 0)
            {

                PerisianFrameworkToolbox::setRequest('d', $filterSupplierId);

            }
        
        }
        
        $result = $this->finalize();
        
        unset($result['filter_user_frontend']);
        unset($result['filter_supplier']);
                        
        return $result;
        
    }
    
}
