<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/CalsyOrderModule.class.php';
/**
 * Area controller
 *
 * @author Peter Hamm
 * @date 2016-11-09
 */
class CalsyOrderController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/order/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyOrder();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search');
        
        // Filter setup
        {
            
            $filterUserFrontendId = $this->getParameter('p') > 0 || $this->getParameter('p') == CalsyUserFrontend::DEFAULT_USER_ID ? $this->getParameter('p') : 0;
            $filterSupplierId = CalsySupplierModule::isEnabled() ? ($this->getParameter('d') > 0 ? $this->getParameter('d') : 0) : 0;

            try
            {
                
                $filterUserFrontend = new CalsyUserFrontend($filterUserFrontendId);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
            try
            {
                
                $filterSupplier = new CalsySupplier($filterSupplierId);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
            $filter = CalsyOrder::createFilter($filterUserFrontendId, $filterSupplierId);
            
        }
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/order/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyOrder::getOrderList($filter, $search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyOrder::getOrderCount($filter, $search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        if($filterUserFrontendId > 0)
        {
            
            $result['filter_user_frontend_id'] = $filterUserFrontendId;
            
            if(!is_null(@$filterUserFrontend))
            {
                
                $result['filter_user_frontend'] = $filterUserFrontend;
                
            }
            
        }
        
        if($filterSupplierId > 0)
        {
            
            $result['filter_supplier_id'] = $filterSupplierId;
            
            if(!is_null(@$filterSupplier))
            {
                
                $result['filter_supplier'] = @$filterSupplier;
                
            }
            
        }
        
        $this->setResultValue('result', $result);
                
    }
        
    /**
     * Retrieves JSON data for the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $data = array();
        
        if($source == "user_frontend")
        {
            
            $data = CalsyUserFrontend::getUserFrontendListFormatted("", "calsy_user_frontend_fullname", "ASC", 0, 0, $this->getParameter('showDefaultUser') != 'false');
            
        }
        else if($source == "supplier")
        {
            
            $data = CalsySupplier::getSupplierList();
            
        }
        
        $result = array(
            
            'data' => $data
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Retrieves a simple list of all areas.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOrderListEntry()
    {
        
        $orderId = $this->getParameter('r');
        $type = $this->getParameter('t');
        
        if($orderId < 1)
        {
            
            // Invalid identifier
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p57ab3ac859102'));
            
        }
        
        $renderer = array(
            
            'page' => 'calsy/order/order_list_entry_' . ($type == 'edit' ? 'edit' : 'view')
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $order = new CalsyOrder($orderId);
        $currentEntry = (array) $order;

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable(10801));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'page' => 'calsy/order/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $editId = strlen($this->getParameter('e')) > 0 ? $this->getParameter('e') : 0;
            
        }
        
        $editEntry = new CalsyOrder($editId);
        
        $result = array(
            
            'object' => $editEntry,
            'id' => $editId,
            
            'user_frontend' => $editEntry->getUserFrontend(),
            'supplier' => $editEntry->getSupplier(),
            
            'title_page' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable(10799)
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyOrder($deleteId);
        $entryObj->deleteWithRelations();
        
        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            
        }
        
        $saveElement = new CalsyOrder($editId);
        $saveElement->{$saveElement->field_title} = $this->getParameter($saveElement->field_title);
        $saveElement->{$saveElement->field_use_count} = $this->getParameter($saveElement->field_use_count);
        
        if(CalsyUserFrontendModule::isEnabled())
        {
            
            $saveElement->{$saveElement->field_fk} = $this->getParameter($saveElement->field_fk);
            
        }
        
        if(CalsySupplierModule::isEnabled())
        {
            
            $saveElement->{$saveElement->field_fk_secondary} = $this->getParameter($saveElement->field_fk_secondary);
            
        }
        
        $newId = $saveElement->save();
        
        if($newId > 0)
        {
            
            $result = array(
                
                'success' => true, 
                'newId' => $newId
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
                
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}