<?php

require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';
require_once 'modules/CalsySupplierModule.class.php';

/**
 * Order
 *
 * @author Peter Hamm
 * @date 2016-01-29
 */
class CalsyOrder extends PerisianDatabaseModel
{   

    // Instance
    private static $instance = null;
    
    // Main table settings
    public $table = 'calsy_order';
    public $field_pk = 'calsy_order_id';
    public $field_fk = 'calsy_order_user_frontend_id';
    public $field_fk_secondary = 'calsy_order_supplier_id';
    public $field_title = 'calsy_order_title';
    public $field_use_count = 'calsy_order_use_count';
    public $field_file = 'calsy_order_file';
    
    public $foreign_table = 'calsy_user_frontend';
    public $foreign_field_pk = 'calsy_user_frontend_id';
    
    public $foreign_secondary_table = 'calsy_supplier';
    public $foreign_secondary_field_pk = 'calsy_supplier_id';
    
    public $field_foreign_fullname_concat = 'CONCAT_WS(" ", calsy_user_frontend_name_first, calsy_user_frontend_name_last) as calsy_supplier_fullname';
    public $field_foreign_secondary_fullname = 'calsy_supplier_fullname';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_fk', 'field_title', 'field_foreign_fullname_concat', 'calsy_supplier_fullname');
    
    protected static $userFrontendList = Array();
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId) && $entryId > 0)
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static public function getInstance()
    {

        if(!isset(self::$instance))
        {
            
            self::$instance = new self();
            
        }

        return self::$instance;

    }
    
    /**
     * Changes the frontend user identifier for all entries associatioed to $identifierFrom
     * to the frontend user with the identifier $identifierTo 
     * 
     * @author Peter Hamm
     * @param int $identifierFrom
     * @param int $identifierTo
     * @return boolean
     */
    public static function changeUserFrontendIdForEntries($identifierFrom, $identifierTo)
    {
        
        $identifierFrom = (int)$identifierFrom;
        $identifierTo = (int)$identifierTo;
                
        if($identifierFrom == 0 || $identifierFrom == 0)
        {
            
            return false;
            
        }
        
        $object = static::getInstance();
        $filter = static::createFilter($identifierFrom);
        $data = static::getOrderList($filter);
        
        for($i = 0; $i < count($data); ++$i)
        {
            
            $entry = new static($data[$i][$object->field_pk]);
            
            $entry->{$entry->field_fk} = "" . $identifierTo;
            $entry->{$entry->foreign_field_pk} = "" . $identifierTo;
                                    
            $entry->save();
            
        }
        
        return true;
        
    }
    
    /**
     * Gets fired just before the save fields get put together.
     * Save fields can be filtered here when overridden.
     * 
     * @param type $handleFields
     * @return Array 
     */
    protected function filterHandleFields($handleFields)
    {
        
        $filterIndex = array_search($this->field_foreign_fullname_concat, $handleFields);
        
        if($filterIndex >= 0)
        {
            
            unset($handleFields[$filterIndex]);
            
        }
        
        $filterIndex = array_search($this->foreign_secondary_field_pk, $handleFields);
        
        if($filterIndex >= 0)
        {
            
            unset($handleFields[$filterIndex]);
            
        }
        
        $filterIndex = array_search($this->field_foreign_secondary_fullname, $handleFields);
        
        if($filterIndex >= 0)
        {
            
            unset($handleFields[$filterIndex]);
            
        }
        
        return $handleFields;
        
    }
    
    /**
     * Retrieves the frontend user related to this entry.
     * 
     * @author Peter Hamm
     * @return CalsyUserFrontend
     */
    public function getUserFrontend()
    {
        
        return new CalsyUserFrontend($this->{$this->field_fk});
        
    }
    
    /**
     * Retrieves the supplier related to this entry.
     * 
     * @author Peter Hamm
     * @return CalsySupplier
     */
    public function getSupplier()
    {
        
        return new CalsySupplier($this->{$this->field_fk_secondary});
        
    }
    
    /**
     * Builds a search string with the specified parameters.
     * 
     * @author Peter Hamm
     * @param array $filter
     * @param String $search
     * @return string
     */
    public function buildSearchString($filter, $search)
    {

        PerisianFrameworkToolbox::security($search);
        
        $query = parent::buildSearchString($search);
        
        if(@$filter["userFrontendId"] > 0 || @$filter["userFrontendId"] == CalsyUserFrontend::DEFAULT_USER_ID)
        {
            
            if(strlen($query) > 0)
            {

                $query = "(" . $query . ") AND ";

            }
            
            $query .= $this->field_fk . " = '" . @$filter["userFrontendId"] . "'";
            
        }
        
        if(@$filter["supplierId"] > 0)
        {
            
            if(strlen($query) > 0)
            {

                $query = "(" . $query . ") AND ";

            }
            
            $query .= $this->field_fk_secondary . " = '" . @$filter["supplierId"] . "'";
            
        }
        
        return $query;
        
    }
    
    /**
     * Generates a formatted HTML string for the two specified numbers.
     * 
     * @author Peter Hamm
     * @param int $countOfApplications
     * @param int $maximumCountOfApplications
     * @return String
     */
    public static function formatApplications($countOfApplications, $maximumCountOfApplications)
    {
        
        $percentage = (int)$countOfApplications / (int)$maximumCountOfApplications;
        
        $addition = "";
        
        if($percentage <=1)
        {
            
            $color = "0ec700";
            
        }
        else
        {
            
            $color = "f80000";
            
        }
        
        if($percentage == 1)
        {
            
            $addition = " &#10003;";
            
        }
        else if($percentage >= 1)
        {
            
            $addition = " &#10006;";
            
        }
        
        $result = '<span style="color: #' . $color . '">' . $countOfApplications . " " . PerisianLanguageVariable::getVariable(10840) . " "  . $maximumCountOfApplications . $addition . "</span>";
        
        return $result;
        
    }
    
    /**
     * Generates an associative array out of the filter IDs specified.
     * Can later be used to filter lists by the parameters specified.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @param int $supplierId
     * @return array
     */
    public static function createFilter($userFrontendId = 0, $supplierId = 0)
    {
        
        $filter = array();
        
        $filter['userFrontendId'] = PerisianFrameworkToolbox::security($userFrontendId);
        $filter['supplierId'] = PerisianFrameworkToolbox::security($supplierId);
        
        return $filter;
        
    }
    
    /**
     * Retrieves a list of all orders for the specified filter
     * 
     * @author Peter Hamm
     * @param array $filter A filter array generated by self::createfilter();
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getOrderList($filter = array(), $search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($filter, $search);
        
        if($orderBy == "pk")
        {
            
            $orderBy = $instance->field_pk;
            
        }
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of all orders for the specified filter
     * 
     * @author Peter Hamm
     * @param array $filter A filter array generated by self::createfilter();
     * @param String $search
     * @return array
     */
    public static function getOrderCount($filter = array(), $search = "")
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($filter, $search);
                
        $results = $instance->getCount($query, $instance->field_title);
        
        return $results;
        
    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {
        
        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves a frontend user by its identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return CalsyUserFrontend
     */
    public static function getUserFrontendById($userFrontendId)
    {
        
        if(!isset(self::$userFrontendList[$userFrontendId]))
        {
            
            self::$userFrontendList[$userFrontendId] = new CalsyUserFrontend($userFrontendId);
            
        }
        
        return self::$userFrontendList[$userFrontendId];
        
    }
    
    /**
     * Retrieves the number of existing applications for the specified order id.
     * 
     * @author Peter Hamm 
     * @param int $orderId
     * @return int
     */
    public static function getNumberOfExistingApplications($orderId)
    {
        
        $entries = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForOrder($orderId);
        
        return count($entries);
        
    }
    
    /**
     * Deletes the current order and its relations (e.g. calendar entries)
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteWithRelations()
    {
        
        if(!isset($this->{$this->field_pk}) || $this->{$this->field_pk} <= 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10823));
            
        }
                
        $userFrontendOrderDummy = new CalsyCalendarEntryUserFrontendOrder();
        $calendarEntryDummy = new CalsyCalendarEntry();
        
        $calendarEntriesToDelete = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForOrder($this->{$this->field_pk});
        
        for($i = 0; $i < count($calendarEntriesToDelete); ++ $i)
        {
            
            // Deleting the calendar entry and the relation to the orders
            
            $entryId = $calendarEntriesToDelete[$i][$userFrontendOrderDummy->field_pk];
            
            $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder($entryId);
            
            $calendarEntryDummy->delete($calendarEntryDummy->field_pk . "=" . $userFrontendOrderObject->{$userFrontendOrderObject->field_fk});
            $userFrontendOrderDummy->delete($userFrontendOrderDummy->field_pk . "=" . $entryId);
            
        }
        
        parent::delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
