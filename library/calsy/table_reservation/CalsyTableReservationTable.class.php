<?php

require_once 'CalsyTableReservationLocation.class.php';

/**
 * Table reservations
 *
 * @author Peter Hamm
 * @date 2020-09-22
 */
class CalsyTableReservationTable extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_table_reservation_table';
    public $field_pk = 'calsy_table_reservation_table_id';
    public $field_fk = 'calsy_table_reservation_table_location_id';
    public $field_title = 'calsy_table_reservation_table_title';
    public $field_capacity = 'calsy_table_reservation_table_capacity';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves the object for this table's location.
     * 
     * @author Peter Hamm
     * @return \CalsyTableReservationLocation
     */
    public function getLocation()
    {
        
        return new CalsyTableReservationLocation($this->{$this->field_fk});
        
    }
    
    /**
     * Checks if the table with the specified identifier is able to get booked at the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $tableId
     * @param int $timestamp
     * @return bool
     */
    public static function isBookableForTimestamp($tableId, $timestamp)
    {
        
        /*
        $areaObject = static::getAreaById($areaId);

        $weekdaysEnabled = $areaObject->getWeekdaysEnabled();
        
        $dayIndex = date("N", $timestamp) - 1;
        
        if(!in_array($dayIndex, $weekdaysEnabled))
        {
            
            return false;
            
        }
        
        return true;
         */
        
    }
        
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Compares two arrays of table data by their title.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByTitle($a, $b) 
    {
        
        $object = static::getInstance();
        
        return strcasecmp($a[$object->field_title], $b[$object->field_title]);

    }
    
    /**
     * Retrieves the table for the specified calendar entry identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return CalsyTableReservationTable Either null if none was found, or the actual object.
     */
    public static function getTableForCalendarEntry($calendarEntryId)
    {
        
        // @ to do
        
        /*
        $returnValue = null;
                
        try 
        {
            
            $instance = new CalsyCalendarEntryUserFrontendOrder();

            $query = "{$instance->field_fk} = '{$calendarEntryId}'";

            $result = $instance->getData($query, 'pk', 'ASC', 0, 1);

            if(count($result) > 0)
            {

                $returnValue = new static($result[0][$instance->field_area_id]);

            }
            
        } 
        catch (Exception $e) 
        {
            
        }
        
        return $returnValue;
         */
        
    }
    
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $locationFilter Optional, set this to a single index or an array of group indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '', $locationFilter = null)
    {
        
        PerisianFrameworkToolbox::security($locationFilter);
        
        $query = $this->buildSearchString($search);
        
        $containedTableIds = Array();
        
        // Filter by the groups
        {
            
            if(!is_array($locationFilter) && (int)$locationFilter > 0)
            {
                
                $locationFilter = Array((int)$locationFilter);
                
            }

            if(is_array($locationFilter))
            {

                for($i = 0; $i < count($locationFilter); ++$i)
                {

                    $containedTableIds = array_unique(array_merge($containedTableIds, $locationFilter));

                }

            }

            if(count($containedTableIds) > 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_fk . " IN (" . implode(",", $containedTableIds) . ")";


            }
            
        }
        
        return $query;
        
    }
    
    /**
     * Retrieves the title for the table with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getTitleForIdentifier($id)
    {
        
        $title = "";
        
        try
        {

            $object = new static($id);
            $title = $object->{$object->field_title};
        
        }
        catch(PerisianException $e)
        {
            
        }
        
        return $title;
        
    }
    
    /**
     * Retrieves a list of tables
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param mixed $locationFilter Optional, set this to a single index or an array of location indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return array
     */
    public static function getTableList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0, $locationFilter = null)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $locationFilter);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the location identifier for the table with the specified
     * identifier.
     * 
     * @author Peter Hamm
     * @param int $tableId
     * @return int
     */
    public static function getLocationIdForTable($tableId)
    {
        
        try
        {
            
            $table = new static($tableId);
        
        }
        catch(PerisianException $e)
        {
            
            return null;
            
        }
        
        return $table->{$table->field_fk};
        
    }
    
    /**
     * Retrieves the capacity of the table at the specified time.
     * 
     * @author Peter Hamm
     * @param int $time
     * @return int
     */
    public function getFreeCapacity($time)
    {
        
        return $this->{$this->field_capacity};
        
    }
    
    /**
     * Retrieves the capacity for the location with the specified identifier.
     * 
     * @author Peter Hamm
     * @param mixed $locationFilter Optional, set this to a single index or an array of location indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return int
     */
    public static function getCapacityForLocation($locationFilter = null)
    {
            
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $locationFilter);
                        
        $results = $instance->getData($query);
        
        $count = 0;
                
        foreach($results as $result)
        {
            
            $count += (int)$result[$instance->field_capacity];
            
        }
        
        return $count;
        
    }
    
    /**
     * Retrieves the count of tables
     * 
     * @author Peter Hamm
     * @param String $search
     * @param mixed $locationFilter Optional, set this to a single index or an array of group indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return array
     */
    public static function getTableCount($search = "", $locationFilter = null)
    {
        
        $instance = static::getInstance();
        
        $query = $instance->buildEnhancedSearchString($search, $locationFilter);
                                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an entry by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return static
     */
    public static function getTableById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(lg(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the table
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteTable()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
