<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/table_reservation/CalsyTableReservation.class.php';

/**
 * Table reservation backend controller
 *
 * @author Peter Hamm
 * @date 2020-09-22
 */
class CalsyTableReservationTableController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/table_reservation/overview_table'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyTableReservationTable();
        $locationDummy = new CalsyTableReservationLocation();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search', '');
        $locationFilter = $this->getParameter('l', '');
        
        $location = new CalsyTableReservationLocation($locationFilter);
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/table_reservation/list_table'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyTableReservationTable::getTableList($search, $sorting, $sortOrder, $offset, $limit, $locationFilter),
            'count' => CalsyTableReservationTable::getTableCount($search, $groupFilter),
            
            'list_locations' => CalsyTableReservationLocation::getLocationList(),
            
            'dummy' => $dummy,
            'dummy_location' => $locationDummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            
            'location_filter' => $locationFilter,
            'location' => $location
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a simple list of all entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetTableList()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $locationFilter = $this->getParameter('l', '');
        
        $list = CalsyTableReservationTable::getTableList($locationFilter);

        //usort($list, 'CalsyArea::compareByTitle');

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', lg('p5f6cede14e687'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/table_reservation/edit_table'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
              
        {
            
            $editTable = new CalsyTableReservationTable($this->getParameter('editId'));
            
            $locationDummy = new CalsyTableReservationLocation();

            $listLocations = CalsyAreaGroup::getAreaGroupList();
            $listGroupsSelected = CalsyAreaGroup::getGroupsForAreaById($editArea->{$editArea->field_pk});
            
            $listLocationsSelectedIdentifiers = Array();
            
            for($i = 0; $i < count($listGroupsSelected); ++$i)
            {
                
                array_push($listLocationsSelectedIdentifiers, $listGroupsSelected[$i][$groupDummy->field_pk]);
                
            }
        
        }
        
        $result = array(
            
            'object' => $editTable,
            
            'list_locations' => $listLocations,
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : lg('p5f6cee911d9e5'),
            
            'dummy_location' => $locationDummy
            
        );
                        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(lg(10130));

        }

        $deleteId = $this->getParameter('deleteId');

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyTableReservationTable($deleteId);
        $entryObj->deleteTable();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'success' => false
            
        );
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
                        
        }
        
        {
                        
            $saveElement = new CalsyTableReservationTable($editId);
            
            $saveElement->setObjectDataFromArray($this->getParameters());
                        
            $newId = $saveElement->save();
            
            $result['success'] = true;
            $result['id'] = $newId;
                        
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}