<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/table_reservation/CalsyTableReservation.class.php';

class CalsyTableReservationFrontendController extends PerisianController
{
        
    /**
     * Finds a table for the specified parameters.
     * 
     * @author Peter Hamm
     * @todo This is just a prototype
     * @return void
     */
    protected function actionFindTable()
    {
        
        $renderer = Array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => nl2br(lg('p5f6d61eb8969b'))
            
        );
        
        try
        {
            
            $time = $this->getParameter('time', time() + PerisianTimeZone::getTimeZoneOffsetInSeconds());
            $persons = $this->getParameter('persons', 1);
            
            $result['proposals'] = CalsyTableReservation::getProposals($time, $persons);
            
            if(count($result['proposals']) > 0)
            {

                $result['success'] = true;
                $result['message'] = lg('p5f6d654da7a72');

            }
            
        }
        catch(PerisianException $e)
        {
            
            $result['message'] = nl2br(lg('p5f6d62322f5e6'));
            $result['error'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Makes a table reservation after the user clicked on the "Approve" button.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return void
     */
    protected function actionApproveReservation()
    {
        
        global $userFrontend;
        
        $renderer = array(

            'blank_page' => true,
            'page' => 'frontend/calsy/table_reservation/landing_page/confirmation'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false,
            'message' => lg('p5f716426cc34e')
            
        );
        
        try
        {
            
            //$reservationTime = (int)$this->getParameter('time', 0);
            //$tableId = (int)$this->getParameter('table', 0);
            
            $tables = (int)$this->getParameter('tables', 0);
            $numberPersonsTotal = (int)$this->getParameter('persons', 0);
            $numberPersons = $numberPersonsTotal;
            
            $locationObjects = Array();
            $tableObjects = Array();
            $calendarEntries = Array();
            
            foreach($tables as $selectedTable)
            {
                
                //$numberPersons -= $numberPersonsTotal;
                
                if($numberPersons <= 0)
                {
                    
                    break;
                    
                }
               
                $table = new CalsyTableReservationTable($selectedTable['id']);
                $reservationTime = $selectedTable['time'];
                
                $capacityAtTable = $table->getFreeCapacity($reservationTime);
                $capacityAtTable = $capacityAtTable > $numberPersons ? $numberPersons : $capacityAtTable;
                $numberPersons -= $capacityAtTable;
                
                $location = $table->getLocation();

                $calendarEntryId = CalsyTableReservation::makeReservation($reservationTime, $tableId, $userFrontend->{$userFrontend->field_pk}, $capacityAtTable);
                $calendarEntry = new CalsyCalendarEntry($calendarEntryId);
                
                array_push($locationObjects, $table);
                array_push($tableObjects, $location);
                array_push($calendarEntries, $calendarEntry);
                
            }
            
            $result = Array(
                
                'success' => true,
                'message' => nl2br(lg('p5f716171afb68')),
                
                'locations' => $locationObjects,
                'tables' => $tableObjects,
                
                'time_reservation' => $reservationTime,
                'number_persons' => $numberPersons

            );
            
            $userFrontend->sendEmailCalendarEntriesNewTableReservation($calendarEntries, null, $userFrontend);
            
        }
        catch(PerisianException $e)
        {
            
            $result['error'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Displays an "approval overview" page to the user where he can confirm
     * his reservation on.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionReservationApproval()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'frontend/calsy/table_reservation/landing_page/approval'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $table = new CalsyTableReservationTable($this->getParameter('table'));
        $location = $table->getLocation();
        
        $result = Array(
            
            'location' => $location,
            'table' => $table
            
        );
                
        $this->setResultValue('result', $result);
        
    }
       
    /**
     * Displays the landing page for the checkin, depending on the current URL.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionLandingPage()
    {
                
        $renderer = array(
            
            'page' => 'frontend/calsy/table_reservation/landing_page/landing_page'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        // Password policy info
        {

            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();

            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;

        }
        
        $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
        $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
        
        {
            
            $beginTimestamp = time() + PerisianTimeZone::getTimeZoneOffsetInSeconds();
            
            // Round the beginning time to the "closest" 15 minutes
            {
                
                $beginTimeMinutes = date("i", $beginTimestamp);
                $beginTimestamp = $beginTimestamp - (60 * ($beginTimeMinutes % 15)) + (30 * 60);
                
            }
            
            $beginDate = PerisianFrameworkToolbox::timestampToDate($beginTimestamp);
            $beginTime = PerisianFrameworkToolbox::timestampToTime($beginTimestamp, "H:i");
            
            
        }
        
        $result = array(
            
            'begin_date' => $beginDate,
            'begin_time' => $beginTime,
            
            'text_gtc' => PerisianFrameworkToolbox::securityRevert(PerisianSystemSetting::getSettingValue('frontend_gtc_registration')),
            'security_policy_password' => $passwordPolicyInfo,
            'url_image_welcome' => $customLogoUrl,
            
            'number_persons' => 2
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}
