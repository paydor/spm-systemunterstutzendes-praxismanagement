<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/table_reservation/CalsyTableReservation.class.php';

/**
 * Table reservation location backend controller
 *
 * @author Peter Hamm
 * @date 2020-09-23
 */
class CalsyTableReservationLocationController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/table_reservation/overview_location'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /** 
     * Handles image uploads
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUploadImage()
    {
         
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        // Parameter setup
        {
            
            $subjectId = $this->getParameter('subjectId');
                    
        }
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        try
        {

            $resetFile = false;

            if($this->getInternal('reset') === true)
            {

                $uploadResult = array(

                    "success" => true,
                    "message" => lg(11132)

                );

                $resetFile = true;

            }
            else
            {

                $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                
            }

        }
        catch(Exception $e)
        {
            
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyTableReservationLocation();
        $tableDummy = new CalsyTableReservationTable();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search', '');
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/table_reservation/list_location'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyTableReservationLocation::getLocationList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyTableReservationLocation::getLocationCount($search),
                        
            'dummy' => $dummy,
            'dummy_table' => $locationDummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            'locationFilter' => $locationFilter
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a simple list of all entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetTableList()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $locationFilter = $this->getParameter('l', '');
        
        $tableDummy = CalsyTableReservationTable::getInstance();
        
        $list = CalsyTableReservationTable::getTableList($locationFilter, $tableDummy->field_pk);

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', lg('p5f6cefc88e510'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/table_reservation/edit_location'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
              
        {
            
            $editLocation = new CalsyTableReservationLocation($this->getParameter('editId'));
                                
        }
        
        $result = array(
            
            'object' => $editLocation,
            'field_location_image' => $editLocation->getImageData(),
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : lg('p5f6cefd78ad94'),
            
        );
                        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(lg(10130));

        }

        $deleteId = $this->getParameter('deleteId');

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyTableReservationLocation($deleteId);
        $entryObj->deleteLocation();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            'success' => false
            
        );
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
                        
        }
        
        {
                        
            $saveElement = new CalsyTableReservationLocation($editId);
                        
            $saveElement->setObjectDataFromArray($this->getParameters());
                        
            $newId = $saveElement->save();
            
            $result['success'] = true;
            $result['id'] = $newId;
                        
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}