<?php

require_once 'CalsyTableReservationTable.class.php';

/**
 * Table reservation handler
 *
 * @author Peter Hamm
 * @date 2020-09-25
 */
class CalsyTableReservation
{
    
    const FLAG_NUMER_PERSONS = 'table_reservation_number_persons';
    const FLAG_LOCATION_ID = 'table_reservation_location_id';
    const FLAG_TABLE_ID = 'table_reservation_table_id';
    
    /**
     * Retirves whether the clanedar entry with the specified identifier is a table reservation or not.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return boolean
     */
    public static function isCalendarEntryTableReservation($calendarEntryId)
    {
        
        $tableId = static::getTableIdForCalendarEntry($calendarEntryId);
        $numberPersons = static::getNumberOfPersonsForCalendarEntry($calendarEntryId);
                
        if($tableId > 0 && $numberPersons > 0)
        {
                        
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Tries to retrieve the number of persons for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getNumberOfPersonsForCalendarEntry($calendarEntryId)
    {
        
        return (int)CalsyCalendarEntryFlag::getFlagValueForEntry($calendarEntryId, static::FLAG_NUMER_PERSONS);
        
    }
    
    /**
     * Retrieves the structure of locations and their associated tables.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getLocationsWithTables()
    {
        
        $structure = Array();
        
        $dummy = CalsyTableReservationLocation::getInstance();
        
        $locationList = CalsyTableReservationLocation::getLocationList('', $dummy->field_title, 'ASC');
        
        foreach($locationList as $locationData)
        {
            
            $locationObject = new CalsyTableReservationLocation($locationData[$dummy->field_pk]);
            
            array_push($structure, $locationObject);
            
        }
        
        return $structure;
        
    }
    
    /**
     * Retrieves the identifier of the location / table associated with the 
     * calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getLocationIdForCalendarEntry($calendarEntryId)
    {
        
        $calendarEntryId = (int)$calendarEntryId;
        
        if($calendarEntryId <= 0)
        {
            
            return null;
            
        }
        
        $tableId = static::getTableIdForCalendarEntry($calendarEntryId);
        $locationId = CalsyTableReservationTable::getLocationIdForTable($tableId);
        
        return $locationId;
        
    }
    
    /**
     * Sets the location ID flag for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $locationId
     * @return void
     */
    public static function setLocationForCalendarEntry($calendarEntryId, $locationId)
    {
        
        CalsyCalendarEntryFlag::saveFlagForEntry($calendarEntryId, static::FLAG_LOCATION_ID, $locationId);
        
    }
    
    /**
     * Sets the table ID flag for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $tableId
     * @return void
     */
    public static function setTableForCalendarEntry($calendarEntryId, $tableId)
    {
        
        CalsyCalendarEntryFlag::saveFlagForEntry($calendarEntryId, static::FLAG_TABLE_ID, $tableId);
        
    }
    
    /**
     * Sets the number of persons flag for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $numberPersons
     * @return void
     */
    public static function setNumberPersonsForCalendarEntry($calendarEntryId, $numberPersons)
    {
        
        CalsyCalendarEntryFlag::saveFlagForEntry($calendarEntryId, static::FLAG_NUMER_PERSONS, $numberPersons);
        
    }
    
    /**
     * Retrieves the number of persons for the table reservation associated to 
     * the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getNumberPersonsForCalendarEntry($calendarEntryId)
    {
        
        $calendarEntryId = (int)$calendarEntryId;
        
        if($calendarEntryId <= 0)
        {
            
            return null;
            
        }
        
        return (int)CalsyCalendarEntryFlag::getFlagValueForEntry($calendarEntryId, static::FLAG_NUMER_PERSONS);
        
    }
    
    /**
     * Tries to retrieve the table identifier for the calendar entry with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return int
     */
    public static function getTableIdForCalendarEntry($calendarEntryId)
    {
        
        $calendarEntryId = (int)$calendarEntryId;
        
        if($calendarEntryId <= 0)
        {
            
            return null;
            
        }
        
        return (int)CalsyCalendarEntryFlag::getFlagValueForEntry($calendarEntryId, static::FLAG_TABLE_ID);
        
    }
    
    /**
     * Retrieves the title for the table with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return String
     */
    public static function getTableTitle($tableId)
    {
                
        $title = lg('p5f717a9561d86');
        
        if((int)$tableId > 0)
        {
            
            try
            {
                
                $table = new CalsyTableReservationTable($tableId);
                
                $title = $table->{$table->field_title};
                
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        return $title;
        
    } 
    /**
     * Retrieves the title for the location of the table with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return String
     */
    public static function getLocationTitleForTable($tableId)
    {
                
        $title = lg('p5f717a9561d86');
        
        if((int)$tableId > 0)
        {
            
            try
            {
                
                $table = new CalsyTableReservationTable($tableId);
                $location = $table->getLocation();
                
                $title = $location->{$location->field_title};
                
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        return $title;
        
    }
    
    /**
     * Will try to make a reservation for the specified parameters.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @param int $time
     * @param int $tableId
     * @param int $userFrontendId
     * @param int $numberPersons
     * @return int The identifier of the newly created calendar entry.
     */
    public static function makeReservation($time, $tableId, $userFrontendId, $numberPersons)
    {
        
        $duration = 3600;
        
        if(!isset($time) || strlen($time) == 0)
        {

            throw new PerisianException(lg(11056));

        }
        
        $numberPersons = (int)$numberPersons;
        $tableId = (int)$tableId;
        $userFrontendId = (int)$userFrontendId;
        
        if($numberPersons <= 0 || $tableId <= 0 || $userFrontendId <= 0)
        {
            
            throw new PerisianException(lg('p58c92a7a3f1d4'));
            
        }
        
        {
            
            $table = new CalsyTableReservationTable($tableId);
            $location = $table->getLocation();
            $userFrontend = new CalsyUserFrontend($userFrontendId);
            
        }

        // Checking if the appointment is overlapping with other entries
        {

            $calendarEntry = new CalsyCalendarEntry();

            $calendarEntry->setTimestampBegin($time);
            $calendarEntry->setTimestampEnd($time + $duration);

            {

                $userFrontendsWithOrders = array(

                    'p' => $userFrontendId,
                    'c' => 'true'

                );

                $calendarEntry->setUserFrontendWithOrders(array($userFrontendsWithOrders));

            }

            // The frontend user must not have other appointments at this time.
            $calendarEntry->checkOverlappingForUserFrontend($userFrontendIdentifier);

        }

        // Create a group for the entry
        {

            $group = new CalsyCalendarEntryGroup();

            $groupId = $group->save();

            $calendarEntry->{$calendarEntry->field_fk} = $groupId;

        }

        // Generate the title
        {
            
            $calendarTitle = lg('p5f7175f6d00ef');
            
            $calendarTitle = str_replace("%locatioName%", $location->{$location->field_title}, $calendarTitle);
            $calendarTitle = str_replace("%numberPersons%", $numberPersons, $calendarTitle);
            $calendarTitle = str_replace("%userName%", $userFrontend->getFullName(), $calendarTitle);
            
            $calendarEntry->{$calendarEntry->field_title} = $calendarTitle;
            
        }
        
        // Save it.
        $newCalendarEntryId = $calendarEntry->save();
        
        // Meta data
        {
            
            CalsyCalendarEntryFlag::saveFlagForEntry($newCalendarEntryId, static::FLAG_TABLE_ID, $tableId);
            CalsyCalendarEntryFlag::saveFlagForEntry($newCalendarEntryId, static::FLAG_NUMER_PERSONS, $numberPersons);
            
        }
        
        return $newCalendarEntryId;
        
    }
        
    /**
     * Will search for free tables at the specified time for the specified amount of persons.
     * 
     * @todo This is just a prototype
     * @author Peter Hamm
     * @param int $time
     * @param int $amountPersons
     * @return Array
     */
    public static function getProposals($time, $amountPersons)
    {
        
        $time = ($time <= 0 ? time + PerisianTimeZone::getTimeZoneOffsetInSeconds() : $time);
        $amountPersons = ($amountPersons <= 0 ? 1 : $amountPersons);
        
        $result = Array();
        
        $locationObject = CalsyTableReservationLocation::getInstance();
        $tableObject = CalsyTableReservationTable::getInstance();
        
        $listLocations = CalsyTableReservationLocation::getLocationList();
        
        foreach($listLocations as $locationData)
        {
            
            $location = new CalsyTableReservationLocation($locationData[$locationObject->field_pk]);
            
            $tableIdentifiers = $location->getTables();
            
            foreach($tableIdentifiers as $tableIdentifier)
            {
                
                $imageData = $location->getImageData();
                $imageUrl = isset($imageData['image']) && isset($imageData['image']['url']) ? $imageData['image']['url'] : '';
                
                $table = new CalsyTableReservationTable($tableIdentifier);
                
                $entry = Array(
                    
                    'time' => $time,
                    'time_formatted' => PerisianFrameworkToolbox::timestampToDate($time) . ', ' . PerisianFrameworkToolbox::timestampToTime($time, "H:i") . ' ' . lg('10903'),
                    
                    'table_id' => $tableIdentifier,
                    'table_title' => $table->{$table->field_title},
                    'table_capacity' => $table->getFreeCapacity($time),
                    'table_capacity_text' => str_replace("%1", $table->getFreeCapacity($time), lg('p5f6d6e0549363')),
                      
                    'location_id' => $location->{$location->field_pk},
                    'location_title' => $location->{$location->field_title},
                    'location_image_url' => $imageUrl
                    
                );
                
                //if($amountPersons <= $entry['table_capacity'])
                if($entry['table_capacity'] > 0)
                {
                    
                    array_push($result, $entry);
                    
                }
                
            }
            
        }
        
        return $result;
        
    }
    
}
