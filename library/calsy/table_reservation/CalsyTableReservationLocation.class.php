<?php

/**
 * Table reservation locations
 *
 * @author Peter Hamm
 * @date 2020-09-23
 */
class CalsyTableReservationLocation extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_table_reservation_location';
    public $field_pk = 'calsy_table_reservation_location_id';
    public $field_title = 'calsy_table_reservation_location_title';
    public $field_description = 'calsy_table_reservation_location_description';
    public $field_address = 'calsy_table_reservation_location_address';
    public $field_image = 'calsy_table_reservation_location_image';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves the URL of the image for this location.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getImageUrl()
    {
        
        $imageData = $this->getImageData();
        $imageUrl = isset($imageData['image']) && isset($imageData['image']['url']) ? $imageData['image']['url'] : '';
        
        return $imageUrl;
        
    }
       
    /**
     * Retrieves the meta data of the image of this entry (if applicable).
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getImageData()
    {
        
        try
        {
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $this->{$this->field_image});
            
        }
        catch(PerisianException $e)
        {
            
            $fileInfo = null;
            
        }
        
        $data = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => $this->field_image,
            "title" => lg('p5f6d2afd0cd70'),
            "description" => lg('p5d385861b03c1'),

            "image" => $fileInfo

        );
            
        return $data;
        
    }
    
    /**
     * Retrieves a list of table objects associated with this location.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getTableObjects()
    {
     
        $tableObjectList = Array();
        $tableList = $this->getTables();
        
        foreach($tableList as $tableId)
        {
            
            try
            {
                
                array_push($tableObjectList, new CalsyTableReservationTable($tableId));
                
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        return $tableObjectList;
        
    }
    
    /**
     * Retrieves the identifiers of all tables for this location.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function getTables()
    {
        
        $tableIdentifiers = Array();
        
        $dummy = CalsyTableReservationTable::getInstance();
        
        $tables = CalsyTableReservationTable::getTableList('', $dummy->field_title, 'ASC', 0, 0, $this->{$this->field_pk});
        
        foreach($tables as $table)
        {
            
            array_push($tableIdentifiers, $table[$dummy->field_pk]);
            
        }
        
        return $tableIdentifiers;
        
    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Compares two arrays of area data by their "name" key.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByTitle($a, $b) 
    {
        
        $object = static::getInstance();
        
        return strcasecmp($a[$object->field_title], $b[$object->field_title]);

    }
        
    /**
     * Retrieves the title for the table with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getTitleForIdentifier($id)
    {
        
        $title = "";
        
        try
        {

            $object = new static($id);
            $title = $object->{$object->field_title};
        
        }
        catch(PerisianException $e)
        {
            
        }
        
        return $title;
        
    }
    
    /**
     * Retrieves a list of entries
     * 
     * @author Peter Hamm
     * @param String $search Optional
     * @param int $orderBy Optional, default: primary key
     * @param int $order Optional, default: ascending
     * @param int $offset Optional, default: 0
     * @param int $limit Optional, default: 0
     * @return Array
     */
    public static function getLocationList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildSearchString($search);
                        
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        // Enhance the results
        for($i = 0; $i < count($results); ++$i)
        {
            
            $capacity = 0;
            $tables = 0;
            
            $results[$i]['number_tables'] = CalsyTableReservationTable::getTableCount('', $results[$i][$instance->field_pk]);
            $results[$i]['overall_capacity'] = CalsyTableReservationTable::getCapacityForLocation($results[$i][$instance->field_pk]);
            
        }
        
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of entries
     * 
     * @author Peter Hamm
     * @param String $search Optional
     * @return Array
     */
    public static function getLocationCount($search = "")
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an entry by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyArea
     */
    public static function getLocationById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(lg(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteLocation()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
        // @to do: Delete all tables
        
    }
    
}
