<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyRemoteController.class.php";

class CalsyRemoteControllerApi extends PerisianControllerRest
{
    
    /**
     * Handles authorization to this resource.
     * Only API system users may access this.
     * 
     * @author Peter Hamm
     * @throws RestException
     * @return bool
     */
    public function authorize()
    {
        
        $value = parent::authorize();
        
        $this->requireSystemUser();
        
        return $value;
                
    }
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyRemoteController";
        
    }
        
    /**
     * Retrieves the system's current status
     *
     * @author Peter Hamm
     * @url GET /system
     * @return Array
     */
    public function getSystemStatus()
    {
                
        $this->setAction('systemStatus');
        
        return $this->finalize();
        
    }
        
    /**
     * Retrieves a list of all of the system's modules
     *
     * @author Peter Hamm
     * @url GET /modules
     * @return Array
     */
    public function getModuleList()
    {
                
        $this->setAction('listModules');
        
        return $this->finalize();
        
    }
    
    /**
     * Enables the module with the specified identifier.
     *
     * @author Peter Hamm
     * @url PATCH /modules/enable/$moduleIdentifier
     * @return Array
     */
    public function moduleEnable($moduleIdentifier)
    {
        
        return $this->toggleEnable($moduleIdentifier, true);
        
    }
    
    /**
     * Disables the module with the specified identifier.
     *
     * @author Peter Hamm
     * @url PATCH /modules/disable/$moduleIdentifier
     * @return Array
     */
    public function moduleDisable($moduleIdentifier)
    {
        
        return $this->toggleEnable($moduleIdentifier, false);
        
    }
    
    /**
     * Toggles the enabled status of the module with the specified identifier to the
     * specified new status.
     * 
     * @author Peter Hamm
     * @param String $moduleIdentifier
     * @param bool $moduleEnabled
     * @return Array
     */
    private function toggleEnable($moduleIdentifier, $moduleEnabled)
    {
        
        $controller = $this->getControllerInstance();
        
        $controller->setAction('toggleModuleEnabled');
        
        $controller->setParameter('moduleIdentifier', $moduleIdentifier);
        $controller->setParameter('moduleEnabled', $moduleEnabled);
                
        $controller->execute();
        
        $result = $controller->getResult();
        
        return $result['result'];
        
    }
    
    /**
     * Blocks the module with the specified identifier.
     *
     * @author Peter Hamm
     * @url PATCH /modules/block/$moduleIdentifier
     * @return bool
     */
    public function moduleBlock($moduleIdentifier)
    {
        
        return $this->toggleBlock($moduleIdentifier, true);
        
    }
    
    /**
     * Unblocks the module with the specified identifier.
     *
     * @author Peter Hamm
     * @url PATCH /modules/unblock/$moduleIdentifier
     * @return bool
     */
    public function moduleUnblock($moduleIdentifier)
    {
        
        return $this->toggleBlock($moduleIdentifier, false);
        
    }
    
    /**
     * Toggles the block status of the module with the specified identifier to the
     * specified new status.
     * 
     * @author Peter Hamm
     * @param String $moduleIdentifier
     * @param bool $moduleEnabled
     * @return Array
     */
    private function toggleBlock($moduleIdentifier, $moduleBlock)
    {
        
        $controller = $this->getControllerInstance();
        
        $controller->setAction('toggleModuleBlocked');
        
        $controller->setParameter('moduleIdentifier', $moduleIdentifier);
        $controller->setParameter('moduleBlocked', $moduleBlock);
                
        $controller->execute();
        
        $result = $controller->getResult();
        
        return $result['result'];
        
    }
    
}
