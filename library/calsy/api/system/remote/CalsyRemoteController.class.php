<?php

require_once 'calsy/system/CalsySystemSettingController.class.php';

/**
 * Provides remote management functionality.
 *
 * @author Peter Hamm
 * @date 2017-02-22
 */
class CalsyRemoteController extends PerisianController
{
    
    /**
     * Retrieves the system's current status
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSystemStatus()
    {
                
        $controller = new CalsySystemSettingController();
        $controller->setAction('status');
        
        $result = $controller->execute();
        
        $this->setResultValue('result', $result['result']);
                
    } 
        
    /**
     * Provides data for an overview of settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionListModules()
    {
                
        $moduleList = PerisianModuleManager::getModuleList(false, true);
        
        $result = array(
            
            'list_modules' => $moduleList
            
        );
        
        $this->setResultValue('result', $result);
                
    } 
    
    /**
     * Toggles the 'enabled' status of a module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionToggleModuleEnabled()
    {
        
        $moduleIdentifier = $this->getParameter('moduleIdentifier');
        $moduleEnabled = $this->getParameter('moduleEnabled');
        
        $result = PerisianModuleManager::toggleModuleEnabled($moduleIdentifier, $moduleEnabled);
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Toggles the 'blocked' status of a module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionToggleModuleBlocked()
    {
                
        $moduleIdentifier = $this->getParameter('moduleIdentifier');
        $moduleBlocked = $this->getParameter('moduleBlocked');
                          
        $result = PerisianModuleManager::toggleModuleBlocked($moduleIdentifier, $moduleBlocked);
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
                
        $action = $this->getAction();
                
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}