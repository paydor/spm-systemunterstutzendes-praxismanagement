<?php

require "external/Jacwright/RestServer/RestServer.php";

require "calsy/user_api/CalsyUserApiControllerApi.class.php";

require "calsy/area/controller/CalsyAreaControllerApi.class.php";
require "calsy/calendar/controller/CalsyCalendarEntryControllerApi.class.php";
require "calsy/system/CalsyFrontendSettingControllerApi.class.php";
require "calsy/user_backend/controller/CalsyUserBackendControllerApi.class.php";
require "calsy/user_frontend/controller/CalsyUserFrontendControllerApi.class.php";
require "calsy/order/controller/CalsyOrderControllerApi.class.php";
require "calsy/resource/controller/CalsyResourceControllerApi.class.php";
require "calsy/supplier/controller/CalsySupplierControllerApi.class.php";
require "calsy/work_plan/controller/CalsyWorkPlanControllerApi.class.php";

require "calsy/api/system/remote/CalsyRemoteControllerApi.class.php";
require "framework/controller/PerisianLanguageVariableControllerApi.class.php";

/**
 * Main API class of the system.
 * 
 * @author Peter Hamm
 * @date 2017-01-12
 */
class CalsyApi
{
    
    protected $restController = null;
    
    /**
     * Creates a new instance of the API.
     * 
     * @author Peter Hamm
     * @param bool $debug Optional, set to true to enable the debug mode.
     * @author Peter Hamm
     */
    public function __construct($debug = false)
    {

        $this->handleLanguage();
        
        $this->restController = new RestServer($debug ? 'debug' : 'production');
        
        // Token generation & authentification
        $this->restController->addClass('CalsyUserApiControllerApi', '/token');
        
        // Basic API controllers
        {
            
            $this->restController->addClass('CalsyAreaControllerApi', '/areas');
            $this->restController->addClass('CalsyCalendarEntryControllerApi', '/calendar/entries');
            $this->restController->addClass('CalsyFrontendSettingControllerApi', '/setting_frontend');
            $this->restController->addClass('CalsyUserBackendControllerApi', '/users_backend');
            $this->restController->addClass('CalsyUserFrontendControllerApi', '/users_frontend');
            $this->restController->addClass('CalsyOrderControllerApi', '/orders');
            $this->restController->addClass('CalsyResourceControllerApi', '/resources');
            $this->restController->addClass('CalsySupplierControllerApi', '/suppliers');
            $this->restController->addClass('CalsyWorkPlanControllerApi', '/work_plans');
            
        }
        
        // System API controllers (System User only)
        {
            
            $this->restController->addClass('CalsyRemoteControllerApi', '/remote');
            $this->restController->addClass('PerisianLanguageVariableControllerApi', '/language');
            
        }
        
    }
    
    /**
     * Handles the call to the API depending on the specified request parameters.
     * 
     * @author Peter Hamm
     * @reuturn void
     */
    public function handle()
    {
                
        $this->restController->handle();
        
    }
    
    /**
     * Handles the language for the API.
     * The language can either be set by a two-letter code, e.g. 'en' for English or 'de' for German (etc.).
     * If the language parameter is not set, the API language defaults to English.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function handleLanguage()
    {
                        
        $requestedLanguage = PerisianFrameworkToolbox::getRequest('language');
        $requestedLanguageId = PerisianLanguage::getLanguageIdentifier($requestedLanguage);
        
        if($requestedLanguageId == 0)
        {
            
            $requestedLanguage = 'en';
            $requestedLanguageId = PerisianLanguage::getLanguageIdentifier($requestedLanguage);
            
        }
        
        PerisianLanguageVariable::setLanguage($requestedLanguageId);
        
    }
    
}