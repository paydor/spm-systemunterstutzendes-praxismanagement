<?php

/**
 * Process engine server connection and requests
 * 
 * @author Peter Hamm
 * @date 2017-02-16
 */
class CalsyProcessEngineConnection
{
    
    protected $server = '';
    protected $loginUser = '';
    protected $loginPassword = '';
    
    protected $isConnected = false;
    protected $connectionToken = '';
    
    protected $processListCached = null;
    
    /**
     * Instantiates a connection object to a ProcessEngine server.
     * 
     * @author Peter Hamm
     * @param String $server
     * @param String $user
     * @param String $password
     * @return void
     */
    public function __construct($server, $user, $password) 
    {
        
        $this->server = $server;
        $this->loginUser = $user;
        $this->loginPassword = $password;
        
    }
    
    /**
     * Tries to connect to the ProcessEngine server.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected function connect()
    {
        
        if($this->isConnected)
        {

            return true;
            
        }
        
        {
            
            $data = array(
                
                'loginname' => $this->loginUser,
                'password' => $this->loginPassword
                
            );
                                                
            $result = $this->makeRequest('user', $data, 'POST');
            
            $this->connectionToken = $result['data'];
                        
        }
        
        $this->isConnected = true;
        
        return true;
        
    }
    
    /**
     * Tries to retrieve a list of all processes on the ProcessEngine server.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getProcessList()
    {
                
        if(is_null($this->processListCached))
        {
                                    
            $this->connect();
                        
            $resource = 'process/' . $this->connectionToken;

            $data = array();

            $connectionData = $this->makeRequest($resource, $data);
            
            $processList = $connectionData['data'];
            
            // Sort the processes by name
            {
                
                function sortByName($a, $b)
                {
                    
                    return strcmp($a["name"], $b["name"]);
                    
                }

                usort($processList, "sortByName");
                
            }

            $this->processListCached = $processList;
            
        }
        
        return $this->processListCached;
        
    }
    
    /**
     * Starts the process with the specified identifier, if the process engine module is activated.
     * 
     * @author Peter Hamm
     * @param String $processIdentifier
     * @return String The process' instance identifier.
     */
    public function startProcess($processIdentifier, $processTitle = '')
    {
        
        if(!CalsyProcessEngineModule::isEnabled())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58a22d853dbf4'));
            
        }
        
        $this->connect();

        $resource = 'process/' . $this->connectionToken . '/' . $processIdentifier;
        
        $data = array(
            
            'title' => strlen($processTitle) == 0 ? 'Process' : $processTitle
            
        );

        $returnData = $this->makeRequest($resource, $data, 'POST');
        
        $returnValue = $returnData['data'];

        return $returnValue;
        
    }
    
    /**
     * Makes a request to the ProcessEngine server and returns the result.
     * 
     * @author Peter Hamm
     * @param String $resource The resource on the remote server you want to request.
     * @param Array $data An associative array with the data to send.
     * @param String $type Optional, the request type. Default: 'GET'
     * @return Array
     * @throws PerisianException
     */
    protected function makeRequest($resource, $data = null, $type = 'GET')
    {
                
        // Build the connection string
        {
            
            $requestString = '';

            if(!is_null($data))
            {
                
                foreach($data as $key => $value) 
                { 

                    $requestString .= $key . '=' . urlencode($value) . '&'; 

                }

                $requestString = rtrim($requestString,'&');
            
            }
        
        }
        
        // Parameter initialization
        {
            
            $type = strtoupper($type);
            
            $header = array(
                
                'Content-Type: application/x-www-form-urlencoded'
                
            );
            
        }

        {
            
            $connection = curl_init();

            curl_setopt($connection, CURLOPT_URL, $this->server . $resource);
            curl_setopt($connection, CURLOPT_HTTPHEADER, $header);
            curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
                        
            if($type == 'POST')
            {
                
                curl_setopt($connection, CURLOPT_POST, true);
                
                if(!is_null($data))
                {

                    curl_setopt($connection, CURLOPT_POSTFIELDS, $requestString);

                }
            
            }
            else
            {
                
                curl_setopt($connection, CURLOPT_CUSTOMREQUEST, $type); 
                
            }
                        
            // Channel the output
            {
                
                ob_start();

                $resultSuccessful = curl_exec($connection);

                $result = $resultSuccessful ? ob_get_clean() : null;
                
                @ob_end_clean();
                
            }
            
            if(!$resultSuccessful)
            {
                
                // The remote server could not be reached
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('p58a5ead79b720'));
                
            }
            
            curl_close($connection);
            
        }
        
        if(strpos($result, '<html>') !== false)
        {
                        
            // A remote server error occured.
            
            $errorMessage = PerisianLanguageVariable::getVariable('p58a5d260b12ea');
            $errorMessage = str_replace('%1', $resource, $errorMessage);
                        
            throw new PerisianException($errorMessage);
            
        }
        
        $result = json_decode($result, true);
        
        return $result;
        
    }
    
}