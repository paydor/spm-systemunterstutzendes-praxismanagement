<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyResourceController.class.php";

class CalsyResourceControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyResourceController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyResource";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('resources_read');
        
        return parent::getObject($identifier);
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('resources_write');
        
        return $this->updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('resources_write');
        
        $dummy = $this->getDatabaseObject();
        
        $isBookableMultiple = PerisianFrameworkToolbox::getRequest($dummy->field_bookable_multiple);
        $preventDeletion = PerisianFrameworkToolbox::getRequest($dummy->field_prevent_deletion);
        
        PerisianFrameworkToolbox::setRequest($dummy->field_bookable_multiple, ($isBookableMultiple === true || $isBookableMultiple == 'true' || $isBookableMultiple == 1) ? '1' : 0);
        PerisianFrameworkToolbox::setRequest($dummy->field_prevent_deletion, ($preventDeletion === true || $preventDeletion == 'true' || $preventDeletion == 1) ? '1' : 0);
        
        return parent::updateObject($identifier);
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('resources_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('resources_read');
        
        $this->setAction('list');
        
        $result = $this->finalize();
                        
        return $result;
        
    }
    
}
