<?php

/**
 * Calendar resources
 *
 * @author Peter Hamm
 * @date 2016-02-29
 */
class CalsyResource extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_calendar_resource';
    public $field_pk = 'calsy_calendar_resource_id';
    public $field_title = 'calsy_calendar_resource_title';
    public $field_description = 'calsy_calendar_resource_description';
    public $field_bookable_multiple = 'calsy_calendar_resource_bookable_multiple';
    public $field_prevent_deletion = 'calsy_calendar_resource_prevent_deletion';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title', 'field_description');
    
    protected static $specialItemIds = Array("other" => 1, "home_visit" => 2);
    
    // Cache for entries
    private static $entryList = array();
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves if the specified item identifier is the special item with the specified key.
     * 
     * @author Peter Hamm
     * @param String $specialItemKey
     * @param int $itemId
     * @return bool
     */
    public static function isSpecialItem($specialItemKey, $itemId)
    {
        
        $value = false;
        
        if(!CalsyResourceModule::isEnabled())
        {
            
            return false;
            
        }
        
        if(isset(self::$specialItemIds[$specialItemKey]))
        {
            
            $value = self::$specialItemIds[$specialItemKey] == $itemId;
            
        }
        
        return $value;
        
    }
    
    /**
     * Returns the identifier for the special item with the specified key.
     * 
     * @author Peter Hamm
     * @param String $specialItemKey
     * @return int Will return 0 if no entry with that key was found. 
     */
    public static function getSpecialItemId($specialItemKey)
    {
        
        $key = 0;
        
        if(isset(self::$specialItemIds[$specialItemKey]))
        {
            
            $key = self::$specialItemIds[$specialItemKey];
            
        }
        
        return $key;
        
    }
    
    /**
     * Retrieves a list of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getResourceList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getResourceCount($search = "")
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves a resource by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyUserFrontend
     */
    public static function getResourceById($id)
    {
        
        if(!isset(self::$entryList[$id]))
        {
            
            self::$entryList[$id] = new static($id);
            
        }
        
        return self::$entryList[$id];
        
    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Deletes this entry
     * 
     * @author Peter Hamm
     * @return void
     */
    public function delete()
    {
        
        if(!$this->isDeletable())
        {
            
            throw new PerisianException(10944);
            
        }
    
        if(isset($this->{$this->field_pk}) && $this->{$this->field_pk} > 0)
        {
            
            parent::delete($this->field_pk . " = {$this->{$this->field_pk}}");
            
        }
        
    }
    
    /**
     * Checks whether the resource with the specified identifier is bookable multiple times at the same time or not.
     * 
     * @author Peter Hamm
     * @param int $resourceId
     * @return bool
     */
    public static function isMultipleBookingAllowedForResource($resourceId)
    {
        
        $resource = new static($resourceId);
    
        return $resource->getIsBookableMultiple();
        
    }
    
    /**
     * Gets whether this entry is bookable multiple times at the same time or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function getIsBookableMultiple()
    {
        
        return $this->{$this->field_bookable_multiple} == 1;
        
    }
    
    /**
     * Sets the "bookable multiple times at the same time" status for this entry to true or false.
     * 
     * @author Peter Hamm
     * @param bool $value
     * @return void
     */
    public function setIsBookableMultiple($value)
    {
        
        $this->{$this->field_bookable_multiple} = ($value > 0 || $value === true) ? 1 : 0;
        
    }
    
    /**
     * Gets whether this entry is deletable or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isDeletable()
    {
        
        $value = $this->{$this->field_prevent_deletion} != 1;
        
        return $value;
        
    }
    
}
