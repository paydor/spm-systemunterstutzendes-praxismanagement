<?php

require_once 'CalsyAreaGroup.class.php';
require_once 'CalsyAreaPrice.class.php';

/**
 * Areas
 *
 * @author Peter Hamm
 * @date 2016-03-29
 */
class CalsyArea extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_area';
    public $field_pk = 'calsy_area_id';
    public $field_title = 'calsy_area_title';
    public $field_default_duration = 'calsy_area_default_duration';
    public $field_elements = 'calsy_area_elements';
    public $field_bookable_times = 'calsy_area_bookable_times';
    public $field_gender = 'calsy_area_gender';
    public $field_disabled_frontend = 'calsy_area_disabled_frontend';
    public $field_requires_bookable_time = 'calsy_area_requires_bookable_time';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Checks if the area with the specified identifier is able to get booked at the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @param int $timestamp
     * @return bool
     */
    public static function isBookableForTimestamp($areaId, $timestamp)
    {
        
        $areaObject = static::getAreaById($areaId);

        $weekdaysEnabled = $areaObject->getWeekdaysEnabled();
        
        $dayIndex = date("N", $timestamp) - 1;
        
        if(!in_array($dayIndex, $weekdaysEnabled))
        {
            
            return false;
            
        }
        
        return true;
        
    }
    
    /**
     * Sets the day indices that are enabled for this area.
     * 
     * @author Peter Hamm
     * @return void
     * @param Array $weekdayList A simple Array of day indices, e.g. '0' for Monday' or '6' for Sunday.
     */
    public function setWeekdaysEnabled($weekdayList)
    {
        
        if(!is_array($weekdayList))
        {
            
            return false;
            
        }
        
        $weekdaysEnabled = Array();
        
        for($i = 0; $i < count($weekdayList); ++$i)
        {
            
            $dayData = Array(
                
                'day' => $weekdayList[$i],
                'enabled' => true
                
            );
            
            array_push($weekdaysEnabled, $dayData);
            
        }
        
        $this->{$this->field_bookable_times} = json_encode($weekdaysEnabled);
                
    }
    
    /**
     * Retrieves a list of enabled weekday indices as a simple Array.
     * 
     * @author Peter Hamm
     * @return array
     */
    public function getWeekdaysEnabled()
    {
        
        $daysEnabled = Array();
        
        if(isset($this->{$this->field_bookable_times}))
        {
            
            $bookableTimes = json_decode($this->{$this->field_bookable_times}, true);
            
            if(is_array($bookableTimes))
            {
            
                foreach($bookableTimes as $bookableTime)
                {

                    if($bookableTime['enabled'])
                    {

                        array_push($daysEnabled, $bookableTime['day']);

                    }

                }
            
            }
            
        }
        
        return $daysEnabled;
        
    }
    
    /**
     * Saves the data.
     * 
     * @author Peter Hamm
     * @param Array $saveFields
     * @return int The identifier of the saved entry.
     */
    public function save($saveFields = Array())
    {
        
        // Try to set the duration from the elements.
        if(isset($this->{$this->field_elements}) && strlen($this->{$this->field_elements}) > 0)
        {

            $timeElementArray = json_decode($this->{$this->field_elements}, true);

            $accumulatedDuration = 0;

            foreach($timeElementArray as $timeElement)
            {
                                
                $accumulatedDuration += (int)$timeElement['duration'];

            }

            $this->{$this->field_default_duration} = $accumulatedDuration;

        }
        
        $this->validateFields();
        
        return parent::save($saveFields);
        
    }
    
    /**
     * Validates the field of this object, e.g. before saving.
     * Will throw an exception with further information if the validation should fail somewhere.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function validateFields()
    {
        
        if(strlen($this->{$this->field_title}) == 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58793ca0090a3'));
            
        }
        
    }
    
    /**
     * Retrieves a list of all time duration elements, of all areas.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getTimeDurationElementAllAreas()
    {
        
        $timeList = Array();
        
        $areaDummy = new static();
        $areaList = static::getAreaList();
        
        for($i = 0; $i < count($areaList); ++$i)
        {
            
            $areaObject = new static($areaList[$i][$areaDummy->field_pk]);
            
            $timeElements = $areaObject->getTimeDurationElements();
            
            $timeList[$areaList[$i][$areaDummy->field_pk]] = $timeElements;
            
        }
        
        return $timeList;
        
    }
    
    /**
     * Retrieves an array of this area's time duration elements.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getTimeDurationElements()
    {
        
        $elements = json_decode($this->{$this->field_elements}, true);
        
        if(is_array($elements))
        {
            
            foreach($elements as &$timeElement)
            {

                $timeElement['title'] = preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $timeElement['title']);
                $timeElement['title'] = html_entity_decode($timeElement['title']);

            }
        
        }
        
        return $elements;
        
    }
    
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Compares two arrays of area data by their "name" key.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByTitle($a, $b) 
    {
        
        $object = static::getInstance();
        
        return strcasecmp($a[$object->field_title], $b[$object->field_title]);

    }
    
    /**
     * Retrieves the area for the specified calendar entry identifier.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @return CalsyArea Either null if none was found, or the actual CalsyArea object.
     */
    public static function getAreaForCalendarEntry($calendarEntryId)
    {
        
        $returnValue = null;
                
        try 
        {
            
            $instance = new CalsyCalendarEntryUserFrontendOrder();

            $query = "{$instance->field_fk} = '{$calendarEntryId}'";

            $result = $instance->getData($query, 'pk', 'ASC', 0, 1);

            if(count($result) > 0)
            {

                $returnValue = new static($result[0][$instance->field_area_id]);

            }
            
        } 
        catch (Exception $e) 
        {
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Builds a search string with the specified searchterms.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $groupFilter Optional, set this to a single index or an array of group indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return String Your search query string
     */
    protected function buildEnhancedSearchString($search = '', $groupFilter = null)
    {
        
        PerisianFrameworkToolbox::security($groupFilter);
        
        $query = $this->buildSearchString($search);
        
        $containedAreaIds = Array();
        
        // Filter by the groups
        {
            
            if(!is_array($groupFilter) && (int)$groupFilter > 0)
            {
                
                $groupFilter = Array((int)$groupFilter);
                
            }

            if(is_array($groupFilter))
            {

                for($i = 0; $i < count($groupFilter); ++$i)
                {

                    $currentGroup = new CalsyAreaGroup($groupFilter[$i]);

                    $groupAreaIds = $currentGroup->getAreas();

                    $containedAreaIds = array_unique(array_merge($containedAreaIds, $groupAreaIds));

                }

            }

            if(count($containedAreaIds) > 0)
            {

                if(strlen($query) > 0)
                {

                    $query .= " AND ";

                }

                $query .= $this->field_pk . " IN (" . implode(",", $containedAreaIds) . ")";


            }
            
        }
        
        return $query;
        
    }
    
    /**
     * Retrieves the title for the area with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getTitleForIdentifier($id)
    {
        
        $title = "";
        
        try
        {

            $area = new static($id);
            $title = $area->{$area->field_title};
        
        }
        catch(PerisianException $e)
        {
            
        }
        
        return $title;
        
    }
    
    /**
     * Retrieves a list of areas
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @param mixed $groupFilter Optional, set this to a single index or an array of group indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return array
     */
    public static function getAreaList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0, $groupFilter = null)
    {
                
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $groupFilter);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of areas
     * 
     * @author Peter Hamm
     * @param String $search
     * @param mixed $groupFilter Optional, set this to a single index or an array of group indices to filter by, e.g. Array(1, 5, 9, ...); Default: null
     * @return array
     */
    public static function getAreaCount($search = "", $groupFilter = null)
    {
        
        $instance = static::getInstance();
                
        $query = $instance->buildEnhancedSearchString($search, $groupFilter);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an area by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyArea
     */
    public static function getAreaById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
    
    /**
     * Retrieves a list of users dedicated for this area, as user objects.
     * 
     * @author Peter Hamm
     * @param bool $showInvisible Show invisible users? Optional. Default: false
     * @return Array
     */
    public function getUsers($showInvisible = false)
    {
        
        return CalsyAreaUserBackend::getUsersForArea($this->{$this->field_pk}, $showInvisible, true);
        
    }
    
    /**
     * Retrieves a list of users who shall receive notifications dedicated for this area, as user objects.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getNotificationUsers()
    {
        
        return CalsyAreaUserBackendNotification::getUsersForArea($this->{$this->field_pk}, true, true);
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteArea()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}

/**
 * Area-user relation
 *
 * @author Peter Hamm
 * @date 2016-03-29
 */
class CalsyAreaUserBackend extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_area_user_backend';
    public $field_pk = 'calsy_area_user_backend_id';
    public $field_fk = 'calsy_area_user_backend_area_id';
    public $field_fk_secondary = 'calsy_area_user_backend_user_id';
        
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Retrieves a list of area-user relations for the specified user identifier
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return array
     */
    public static function getListForUser($userId)
    {
                
        $instance = new static();
                
        PerisianFrameworkToolbox::security($userId);
        $query = $instance->field_fk_secondary . '="' . $userId . '"';
                
        $results = $instance->getData($query);
                
        return $results;
        
    }
    
    /**
     * Retrieves a list of area-user relations for the specified area identifier
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return array
     */
    public static function getListForArea($areaId)
    {
                
        $instance = new static();
                
        PerisianFrameworkToolbox::security($areaId);
        $query = $instance->field_fk . '="' . $areaId . '"';
                
        $results = $instance->getData($query);
                
        return $results;
        
    }
    
    /**
     * Retrieves the count of area-user relations
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return array
     */
    public static function getCountForArea($areaId = 0)
    {
        
        $instance = new static();
                
        PerisianFrameworkToolbox::security($areaId);
        $query = $instance->field_fk . '="' . $areaId . '"';
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
        
    /**
     * Retrieves a list of areas related to the specified user identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getAreasForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        $instance = new static();
        
        $list = static::getListForUser($userId);
        
        $areaList = array();
        
        for($i = 0; $i < count($list); ++$i)
        {
            
            $area = new CalsyArea($list[$i][$instance->field_fk]);
            
            array_push($areaList, $area);
            
        }
        
        return $areaList;
        
    }
        
    /**
     * Retrieves an alphabetically sorted (by the user's full name) 
     * list of users related to the specified area identifier.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @param bool $showInvisible Show invisible users? Optional. Default: false
     * @param bool $showCannotBeSelectedInAppointmentAssistant Optioonal: Show users that cannot be selected in the appointment assistant? Default: false
     * @return Array
     */
    public static function getUsersForArea($areaId = 0, $showInvisible = false, $showCannotBeSelectedInAppointmentAssistant = false)
    {
        
        PerisianFrameworkToolbox::security($areaId);
        
        $instance = new static();
        $userDummy = new CalsyUserBackend();
        
        $userList = array();
        $list = array();
        
        $fieldPrimaryKey = "";
        
        if(strlen($areaId) == 0 || $areaId == 0)
        {
            
            $list = CalsyUserBackend::getUserList();
            
            $fieldPrimaryKey = $userDummy->field_pk;
                        
        }
        else
        {
            
            $list = static::getListForArea($areaId);
            
            $fieldPrimaryKey = $instance->field_fk_secondary;
            
        }
        
        for($i = 0; $i < count($list); ++$i)
        {
            
            try
            {
                
                $areaUser = new CalsyUserBackend($list[$i][$fieldPrimaryKey]);
                
            }
            catch(PerisianException $e)
            {
                                
                continue;
                
            }
            
            if($areaUser->isInvisible() && !$showInvisible)
            {
                                
                continue;
                
            }
            
            if(!$areaUser->canBeSelectedInAppointmentAssistant() && !$showCannotBeSelectedInAppointmentAssistant)
            {
             
                continue;
                
            }
            
            array_push($userList, $areaUser);
            
        }
        
        usort($userList, "static::compareFullname");
        
        return $userList;
        
    }
    
    /**
     * Compares two CalsyBackendUser objects by their full name
     * 
     * @author Peter Hamm
     * @param CalsyUserBackend $a
     * @param CalsyUserBackend $b
     * @return int
     */
    protected static function compareFullname($a, $b)
    {

        return strcmp($a->{$a->field_fullname}, $b->{$b->field_fullname});

    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes all entries for the specified area.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return void
     */
    public static function deleteForArea($areaId)
    {
        
        PerisianFrameworkToolbox::security($areaId);
        
        if(strlen($areaId) == 0)
        {
            
            return;
            
        }
        
        $instance = new static();
        
        $instance->delete($instance->field_fk . '="' . $areaId . '"');
        
    }
        
    /**
     * Deletes all entries for the specified user.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return void
     */
    public static function deleteForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        if(strlen($userId) == 0)
        {
            
            return;
            
        }
        
        $instance = new static();
        
        $instance->delete($instance->field_fk_secondary . '="' . $userId . '"');
        
    }
    
}

/**
 * Area-user notification relation
 *
 * @author Peter Hamm
 * @date 2016-05-28
 */
class CalsyAreaUserBackendNotification extends CalsyAreaUserBackend
{
    
    // Main table settings
    public $table = 'calsy_area_user_backend_notification';
    public $field_pk = 'calsy_area_user_backend_notification_id';
    public $field_fk = 'calsy_area_user_backend_notification_area_id';
    public $field_fk_secondary = 'calsy_area_user_backend_notification_user_id';
    
}