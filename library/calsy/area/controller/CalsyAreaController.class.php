<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'framework/paypal/PerisianPayPal.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/area/CalsyArea.class.php';

/**
 * Area controller
 *
 * @author Peter Hamm
 * @date 2016-11-08
 */
class CalsyAreaController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/area/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyArea();
        $groupDummy = new CalsyAreaGroup();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search', '');
        $groupFilter = $this->getParameter('g', '');
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/area/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyArea::getAreaList($search, $sorting, $sortOrder, $offset, $limit, $groupFilter),
            'count' => CalsyArea::getAreaCount($search, $groupFilter),
            
            'list_groups' => CalsyAreaGroup::getAreaGroupList(),
            
            'dummy' => $dummy,
            'dummy_group' => $groupDummy,
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            'groupFilter' => $groupFilter
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a simple list of all areas.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetAreaList()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $list = CalsyArea::getAreaList();

        usort($list, 'CalsyArea::compareByTitle');

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable(11019));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/area/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $selectedUsers = array();
        $selectedUserIdentifiers = array();
        $selectedUserNotificationIdentifiers = array();
                
        $editArea = new CalsyArea($this->getParameter('editId'));

        $selectedUsers = $editArea->getUsers(true);
        
        for($i = 0; $i < count($selectedUsers); ++$i)
        {

            array_push($selectedUserIdentifiers, $selectedUsers[$i]->{$selectedUsers[$i]->field_pk});

        }

        $selectedNotificationUsers = $editArea->getNotificationUsers();

        for($i = 0; $i < count($selectedNotificationUsers); ++$i)
        {

            array_push($selectedUserNotificationIdentifiers, $selectedNotificationUsers[$i]->{$selectedNotificationUsers[$i]->field_pk});

        }
        
        // Build a list of selectable hours
        {
            
            $listHours = CalsyCalendar::getTimeList(300, 300, 28800);
            $listHours += CalsyCalendar::getTimeList(3600, 28800, 57600);
            
        }
        
        {
            
            $groupDummy = new CalsyAreaGroup();

            $listGroups = CalsyAreaGroup::getAreaGroupList('', $groupDummy->field_title, 'ASC');
            $listGroupsSelected = CalsyAreaGroup::getGroupsForAreaById($editArea->{$editArea->field_pk});
            
            $listGroupsSelectedIdentifiers = Array();
            
            for($i = 0; $i < count($listGroupsSelected); ++$i)
            {
                
                array_push($listGroupsSelectedIdentifiers, $listGroupsSelected[$i][$groupDummy->field_pk]);
                
            }
        
        }
        
        $result = array(
            
            'object' => $editArea,
            
            'list_hours' => $listHours,
            'list_users' => CalsyUserBackend::getUserList($dummyUser->field_fullname, "ASC"),
            'list_users_selected' => $selectedUsers,
            'list_users_selected_identifiers' => $selectedUserIdentifiers,
            'list_users_selected_notification_identifiers' => $selectedUserNotificationIdentifiers,
            'list_groups' => $listGroups,
            'list_groups_selected_identifiers' => $listGroupsSelectedIdentifiers,
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable(11020),
            
            'dummy_group' => $groupDummy
            
        );
        
        if(CalsyAreaPrice::isPaymentActivated())
        {
            
            $result['price'] = CalsyAreaPrice::getPriceForAreaId($editArea->{$editArea->field_pk});
            
            $result['list_rates_vat'] = PerisianCurrency::getRatesVat();
            $result['list_currencies'] = PerisianCurrency::getCurrencies();
            $result['list_methods_payment'] = CalsyAreaPrice::getPossiblePaymentMethods();
            
        }
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        $deleteId = $this->getParameter('deleteId');

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyArea($deleteId);
        $entryObj->deleteArea();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            $userList = $this->getParameter('users');
            $timeElements = $this->getParameter('timeElements');
            $userNotificationList = $this->getParameter('notificationUsers');
            $weekdaysEnabled = $this->getParameter('weekdaysEnabled');
            $groupsEnabled = $this->getParameter('groupsEnabled');
            
        }
        
        {
            
            $areaPriceDummy = CalsyAreaPrice::getInstance();
            
            $saveElement = new CalsyArea($editId);
            $saveElement->setObjectDataFromArray($this->getParameters());
            $saveElement->{$saveElement->field_elements} = json_encode($timeElements, JSON_UNESCAPED_UNICODE);
            
            if(CalsyUserFrontendRegistrationAndCalendarModule::isEnabled())
            {
                                
                $saveElement->setWeekdaysEnabled($weekdaysEnabled);
                
            }
            
            $newId = $saveElement->save();
            
            CalsyAreaGroup::setGroupsForArea($newId, $groupsEnabled);
            
            // Save the price, if applicable
            if(strlen($this->getParameter($areaPriceDummy->field_amount)) > 0 && (float)$this->getParameter($areaPriceDummy->field_amount) > 0)
            {
                
                $areaPrice = CalsyAreaPrice::getPriceForAreaId($newId);
                
                $areaPrice->setPercentageVat($this->getParameter('calsy_area_price_percentage_vat'));
                $areaPrice->setAmount($this->getParameter($areaPrice->field_amount));
                $areaPrice->{$areaPrice->field_currency} = $this->getParameter($areaPrice->field_currency);
                
                // Check which payment methods are selected
                {
                    
                    $selectedPaymentMethods = Array();
                    $possiblePaymentMethods = CalsyAreaPrice::getPossiblePaymentMethods();
                    
                    foreach($possiblePaymentMethods as $possiblePaymentMethodCode => $possiblePaymentMethodName)
                    {
                        
                        if($this->getParameter('calsy_area_price_payment_method_' . $possiblePaymentMethodCode) == 1)
                        {
                                                        
                            array_push($selectedPaymentMethods, $possiblePaymentMethodCode);
                            
                        }
                        
                    }
                    
                    $areaPrice->setPaymentMethods($selectedPaymentMethods);
                    
                }
                                                
                $areaPrice->save();
                
            }
            else
            {
            
                // Invalid price, remove it
                
                $areaPrice = CalsyAreaPrice::removePriceForAreaId($newId);
                
            }
            
            if(@$newId > 0)
            {

                // Save the area-user relations
                {

                    CalsyAreaUserBackend::deleteForArea($newId);

                    for($i = 0; $i < count($userList); ++$i)
                    {

                        $relation = new CalsyAreaUserBackend();

                        $relation->{$relation->field_fk} = $newId;
                        $relation->{$relation->field_fk_secondary} = $userList[$i];

                        $relation->save();

                    }

                    $result = array('success' => true, 'newId' => $newId);

                }

                // Save the area-user notification relations
                {

                    CalsyAreaUserBackendNotification::deleteForArea($newId);

                    for($i = 0; $i < count($userNotificationList); ++$i)
                    {

                        $relation = new CalsyAreaUserBackendNotification();

                        $relation->{$relation->field_fk} = $newId;
                        $relation->{$relation->field_fk_secondary} = @$userNotificationList[$i];

                        $relation->save();

                    }

                    $result = array('success' => true, 'newId' => $newId);

                }

            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}