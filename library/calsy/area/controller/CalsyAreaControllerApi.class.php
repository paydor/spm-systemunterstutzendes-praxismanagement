<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyAreaController.class.php";

class CalsyAreaControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyAreaController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyArea";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('areas_read');
        
        return parent::getObject($identifier);
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('areas_write');
        
        return parent::updateObject(0);
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('areas_write');
        
        return parent::updateObject($identifier);
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('areas_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('areas_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteArea();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('areas_read');
        
        $this->setAction('list');
        
        $result = $this->finalize();
        
        if(isset($result['list']) && count($result['list']) > 0)
        {
        
            $areaDummy = new CalsyArea();
            
            foreach($result['list'] as &$area)
            {

                $area[$areaDummy->field_elements] = json_decode($area[$areaDummy->field_elements], true);

            }
        
        }
        
        return $result;
        
    }
    
}