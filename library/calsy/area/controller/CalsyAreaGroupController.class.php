<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/area/CalsyAreaGroup.class.php';

/**
 * Area group controller
 *
 * @author Peter Hamm
 * @date 2017-09-11
 */
class CalsyAreaGroupController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'calsy/area/group/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyAreaGroup();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search', '');
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/area/group/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyAreaGroup::getAreaGroupList($search, $sorting, $sortOrder, $offset, $limit),
            'count' => CalsyAreaGroup::getAreaGroupCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves a simple list of all area groups.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetAreaGroupList()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $list = CalsyAreaGroup::getAreaGroupList();

        usort($list, 'CalsyAreaGroup::compareByTitle');

        $result = array(
            
            'list' => $list
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable('p59b7d9edcca80'));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyArea = new CalsyArea();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'calsy/area/group/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $editAreaGroup = new CalsyAreaGroup($this->getParameter('editId'));
        
        $result = array(
            
            'object' => $editAreaGroup,
            
            'list_areas' => CalsyArea::getAreaList("", $dummyArea->field_title, 'ASC'),
            'list_areas_selected_identifiers' => $editAreaGroup->getAreas(),
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable('p59b7da0749397')
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        $deleteId = $this->getParameter('deleteId');

        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }

        $entryObj = new CalsyAreaGroup($deleteId);
        $entryObj->deleteAreaGroup();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            $areaList = $this->getParameter('areas');
            
        }
        
        {
            
            $saveElement = new CalsyAreaGroup($editId);
            $saveElement->setObjectDataFromArray($this->getParameters());
            $saveElement->setAreas($areaList);
                        
            $newId = $saveElement->save();
            
            if(@$newId > 0)
            {

                $result = array('success' => true, 'newId' => $newId);

            }
            
        }
        
        $this->setResultValue('result', $result);
        
    }

    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}