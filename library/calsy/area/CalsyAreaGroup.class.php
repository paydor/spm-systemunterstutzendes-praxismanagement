<?php

/**
 * Area groups
 *
 * @author Peter Hamm
 * @date 2017-09-11
 */
class CalsyAreaGroup extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_area_group';
    public $field_pk = 'calsy_area_group_id';
    public $field_title = 'calsy_area_group_title';
    public $field_areas = 'calsy_area_group_areas';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_title');
    
    protected static $entryList = array();
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Sets the groups for an area.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @param Array $listIdentifiersGroups A list of area group identifiers
     * @return void
     */
    public static function setGroupsForArea($areaId, $listIdentifiersGroups)
    {
        
        $dummy = static::getInstance();
        
        $groupList = static::getAreaGroupList();
        
        for($i = 0; $i < count($groupList); ++$i)
        {
            
            $object = new static($groupList[$i][$dummy->field_pk]);
            
            $areas = $object->getAreas();
            $updatedAreas = Array();
            
            for($j = 0; $j < count($areas); ++$j)
            {
                
                if($areas[$j] != $areaId)
                {
                    
                    array_push($updatedAreas, $areas[$j]);
                    
                }
                
            }
            
            if(is_array($listIdentifiersGroups) && in_array($groupList[$i][$dummy->field_pk], $listIdentifiersGroups))
            {

                // Okay, put it in
                
                array_push($updatedAreas, $areaId);

            }
            
            $object->setAreas($updatedAreas);
            
            $object->save();
            
        }
        
    }
        
    /**
     * Sets the areas for this group
     * 
     * @author Peter Hamm
     * @return void
     * @param Array $areaList A simple Array of area indices, e.g. Array('1', '2', '5', ...)
     */
    public function setAreas($areaList)
    {
        
        if(!is_array($areaList))
        {
            
            return false;
            
        }
        
        $this->{$this->field_areas} = json_encode($areaList);
        
    }
    
    /**
     * Retrieves a list of area indices that belong to this group.
     * 
     * @author Peter Hamm
     * @return array
     */
    public function getAreas()
    {
        
        $areaList = json_decode($this->{$this->field_areas}, true);
        
        if(!is_array($areaList))
        {
            
            $areaList = array();
            
        }
        
        return $areaList;
        
    }
    
    /**
     * Saves the data.
     * 
     * @author Peter Hamm
     * @param Array $saveFields
     * @return int The identifier of the saved entry.
     */
    public function save($saveFields = Array())
    {
        
        $this->validateFields();
        
        return parent::save($saveFields);
        
    }
    
    /**
     * Validates the field of this object, e.g. before saving.
     * Will throw an exception with further information if the validation should fail somewhere.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function validateFields()
    {
        
        if(strlen($this->{$this->field_title}) == 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58793ca0090a3'));
            
        }
        
    }
        
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Compares two arrays of area data by their "name" key.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByTitle($a, $b) 
    {
        
        $object = static::getInstance();
        
        return strcasecmp($a[$object->field_title], $b[$object->field_title]);

    }
    
    /**
     * Retrieves all groups the area with the specified identifier is a part of.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return Array
     */
    public static function getGroupsForAreaById($areaId)
    {
        
        PerisianFrameworkToolbox::security($areaId);
        
        $groupDummy = static::getInstance();
        
        $query = $groupDummy->field_areas . " LIKE '%\"" . $areaId . "\"%'";
                        
        $groupList = $groupDummy->getData($query, $groupDummy->field_title, 'ASC');
        
        return $groupList;
        
    }
    
    /**
     * Retrieves the list of titles of groups the area with the specified identifier is a part of.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return Array
     */
    public static function getGroupTitlesForAreaById($areaId)
    {
        
        $groupDummy = static::getInstance();
        
        $groupNames = Array();
        
        $groups = static::getGroupsForAreaById($areaId);
        
        for($i = 0; $i < count($groups); ++ $i)
        {
            
            array_push($groupNames, $groups[$i][$groupDummy->field_title]);
            
        }
        
        return $groupNames;
        
    }
    
    /**
     * Retrieves the count of areas included in the group with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $areaGroupId
     * @return int
     */
    public static function getAreaCountByGroupId($areaGroupId)
    {
        
        $entry = new static($areaGroupId);
        
        $areaIdList = $entry->getAreas();
        
        return count($areaIdList);
        
    }
    
    /**
     * Retrieves the titles of areas included in the group with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $areaGroupId
     * @return Array
     */
    public static function getAreaTitlesByGroupId($areaGroupId)
    {
        
        $entry = new static($areaGroupId);
        
        $areaDummy = new CalsyArea();
        $areaNameList = Array();
        
        $areaIdList = $entry->getAreas();
        
        for($i = 0; $i < count($areaIdList); ++$i)
        {
            
            try
            {
                
                $area = new CalsyArea($areaIdList[$i]);

                array_push($areaNameList, $area->{$area->field_title});
                
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        return $areaNameList;
        
    }
        
    /**
     * Retrieves a list of groups and their contained area keys.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getAreaGroupsWithContentKeys()
    {
                
        $instance = new self();
        
        $results = static::getAreaGroupList();
        
        $list = Array();
        
        for($i = 0; $i < count($results); ++$i)
        {
            
            $list[$results[$i][$instance->field_pk]] = json_decode($results[$i][$instance->field_areas]);
            
        }
        
        return $list;
        
    }
        
    /**
     * Retrieves a list of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getAreaGroupList($search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of entries
     * 
     * @author Peter Hamm
     * @param String $search
     * @return array
     */
    public static function getAreaGroupCount($search = "")
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($search);
                
        $results = $instance->getCount($query);
        
        return $results;
        
    }
    
    /**
     * Retrieves an entry by its identifier.
     * 
     * @author Peter Hamm
     * @param int $id
     * @return CalsyArea
     */
    public static function getAreaGroupById($id)
    {
                
        if(!isset(static::$entryList[$id]))
        {
            
            static::$entryList[$id] = new static($id);
            
        }
        
        return static::$entryList[$id];
        
    }
        
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
        
    /**
     * Deletes the area
     * 
     * @author Peter Hamm
     * @return void
     */
    public function deleteAreaGroup()
    {
        
        $this->delete($this->field_pk . "=" . $this->{$this->field_pk});
        
    }
    
}
