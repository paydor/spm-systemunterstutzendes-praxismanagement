<?php

require_once 'framework/currency/PerisianCurrency.class.php';
require_once 'modules/PerisianPayPalModule.class.php';

/**
 * Prices for areas
 *
 * @author Peter Hamm
 * @date 2017-10-27
 */
class CalsyAreaPrice extends PerisianDatabaseModel
{
    
    // Main table settings
    public $table = 'calsy_area_price';
    public $field_pk = 'calsy_area_price_id';
    public $field_fk = 'calsy_area_price_area_id';
    public $field_amount = 'calsy_area_price_amount';
    public $field_currency = 'calsy_area_price_currency';
    public $field_rate_vat = 'calsy_area_price_rate_vat';
    public $field_payment_methods = 'calsy_area_price_payment_methods';
    
    protected static $instance;
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Tries to retrieve a payment transaction for the specified calendar entry ID and user frontend ID.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userFrontendId
     * @return IPerisianPayment An existing payment transaction. Can be null if none exists yet.
     */
    public static function getPaymentTransaction($calendarEntryId, $userFrontendId)
    {
        
        $userFrontendDummy = new CalsyUserFrontend();
        
        if(PerisianPayPalModule::isEnabled())
        {
            
            $transactionType = PerisianPayPalTransaction::TYPE_CALENDAR_ENTRY;
            
            $transaction = PerisianPayPalTransaction::getTransaction($transactionType, $calendarEntryId, $userFrontendDummy->field_pk, $userFrontendId);
            
            if($transaction != null)
            {
                
                return $transaction;
                
            }
            
        }
        
        return null;
        
    }
    
    /**
     * Checks if a payment was received for the specified calendar entry by the specified frontend user.
     * 
     * @author Peter Hamm
     * @param int $calendarEntryId
     * @param int $userFrontendId
     * @return boolean
     */
    public static function isPaymentReceivedFromUserFrontend($calendarEntryId, $userFrontendId)
    {
        
        $userFrontendDummy = new CalsyUserFrontend();
     
        if(PerisianPayPalModule::isEnabled())
        {
            
            $transactionType = PerisianPayPalTransaction::TYPE_CALENDAR_ENTRY;
            
            $receivedPayPal = PerisianPayPalTransaction::isPaymentReceivedForTransaction($transactionType, $calendarEntryId, $userFrontendDummy->field_pk, $userFrontendId);
            
            if($receivedPayPal)
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves an URL to start a payment process for the specified type and payment identifier.
     * 
     * @author Peter Hamm
     * @param type $paymentType
     * @param type $paymentId Optional
     * @return String
     */
    public static function getPaymentUrl($paymentType, $paymentId = "")
    {
        
        $url =  PerisianFrameworkToolbox::getServerAddress(false) . 'payment/?t=' . $paymentType;
        
        if(strlen($paymentId) > 0)
        {
            
            $url .= '&p=' . $paymentId;
            
        }
        
        return $url;
        
    }
    
    /**
     * Checks if any kind of payment is required for the area with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return bool
     */
    public static function isPaymentRequiredForArea($areaId)
    {
        
        $area = new CalsyArea($areaId);
        
        $price = static::getPriceForAreaId($area->{$area->field_pk});
        
        if($price->{$price->field_amount} > 0)
        {
            
            return true;
            
        }
        
        return false;
        
    }
        
    /**
     * Retrieves or generates a singleton instance.
     * 
     * @author Peter Hamm
     * @return static
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Retrieves if payments are activated for the system at the moment.
     * This is the case, for example, if the PayPal module is active.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isPaymentActivated()
    {
     
        if(PerisianPayPalModule::isEnabled())
        {
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Removes all price information for the area with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return void
     */
    public static function removePriceForAreaId($areaId)
    {
        
        PerisianFrameworkToolbox::security($areaId);
                        
        if($areaId > 0)
        {
            
            try 
            {

                $instance = new static();

                $query = "{$instance->field_fk} = '{$areaId}'";

                $result = $instance->delete($query);

            } 
            catch (Exception $e) 
            {

            }
            
        }
        
    }
    
    /**
     * Retrieves all relevant price information for the area with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $areaId
     * @return static A CalsyAreaPrice object
     */
    public static function getPriceForAreaId($areaId)
    {
        
        PerisianFrameworkToolbox::security($areaId);
        
        $returnValue = new static();
        
        $returnValue->{$returnValue->field_fk} = $areaId;
                
        if($areaId > 0)
        {
            
            try 
            {

                $instance = new static();

                $query = "{$instance->field_fk} = '{$areaId}'";

                $result = $instance->getData($query, 'pk', 'ASC', 0, 1);

                if(count($result) > 0)
                {

                    $returnValue = new static($result[0][$instance->field_pk]);

                }

            } 
            catch (Exception $e) 
            {

            }
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves the formatted net price amount as a String.
     * 
     * @author Peter Hamm
     * @return float 
     */
    public function getPriceAmountFormattedNet()
    {
        
        return static::formatPriceAmount($this->{$this->field_amount});
        
    }
    
    /**
     * Retrieves the formatted tax price amount as a String.
     * 
     * @author Peter Hamm
     * @return String 
     */
    public function getPriceAmountFormattedTax()
    {
        
        $price = $this->getPriceAmountFormattedGross() - $this->getPriceAmountFormattedNet();
        
        return static::formatPriceAmount($price);
        
    }
    
    /**
     * Retrieves the formatted gross price amount as a String.
     * 
     * @author Peter Hamm
     * @return String 
     */
    public function getPriceAmountFormattedGross()
    {
        
        $price = $this->{$this->field_amount};
        
        $price *= $this->getRateVat();
        
        return static::formatPriceAmount($price);
        
    }
    
    /**
     * Retrieves the currency.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getCurrency()
    {
        
        $currency = $this->{$this->field_currency};
        
        if(strlen($currency) > 0)
        {
            
            // Check if it's a valid currency
            
            $validCurrencies = PerisianCurrency::getCurrencies();
            
            $currencyFound = false;
            
            foreach($validCurrencies as $code => $name)
            {
                
                if($currency == $code)
                {
                    
                    $currencyFound = true;
                    
                    break;
                    
                }
                
            }
            
            if(!$currencyFound)
            {
                
                $currency = "";
                
            }
            
        }
                
        if(strlen($currency) < 3)
        {
            
            $currency = PerisianPayPal::getDefaultCurrency();
            
        }
        
        $this->{$this->field_currency} = $currency;
        
        return $this->{$this->field_currency};
        
    }
    
    /**
     * Alias for getRateVat().
     * 
     * @author Peter Hamm
     * @return float
     */
    public function getRateTax()
    {
     
        return $this->getRateVat();
        
    }
    
    /**
     * Retrieves the VAT rate in the format as of e.g. "1.19".
     * 
     * @author Peter Hamm
     * @return float
     */
    public function getRateVat()
    {
                
        if($this->{$this->field_rate_vat} < 1)
        {
            
            $this->{$this->field_rate_vat} = PerisianPayPal::getDefaultRateVat();
            
        }
        
        return $this->{$this->field_rate_vat};
        
    }
    
    /**
     * Sets the VAT percentage, e.g. "19".
     * 
     * @author Peter Hamm
     * @param int $rateVat
     * @return void
     */
    public function setPercentageVat($percentageVat)
    {
        
        $rateVat = ($percentageVat > 0 ? ($percentageVat / 100) : 0) + 1;
        
        $this->setRateVat($rateVat);
        
    }
    
    /**
     * Sets the VAT rate, e.g. "1.19".
     * 
     * @author Peter Hamm
     * @param float $rateVat
     * @return void
     */
    public function setRateVat($rateVat)
    {
        
        $this->{$this->field_rate_vat} = $rateVat;
        
    }
    
    /**
     * Sets the price amount, e.g. "50,00" or "75.00".
     * Unifies different kinds of formattings.
     * 
     * @author Peter Hamm
     * @param float $amount
     * @return void
     */
    public function setAmount($amount)
    {
        
        $amount = str_replace(",", ".", $amount);
        
        $this->{$this->field_amount} = (float)$amount;
        
    }
    
    /**
     * Sets the specified value of payment methods to this object.
     * 
     * @author Peter Hamm
     * @param mixed $paymentMethods Either a single method or an array.
     * @return void
     */
    public function setPaymentMethods($paymentMethods)
    {
        
        if(!is_array($paymentMethods))
        {
            
            $paymentMethods = Array($paymentMethods);
            
        }
        
        $this->{$this->field_payment_methods} = json_encode($paymentMethods);
        
    }
    
    /**
     * Retrieves the payment methods stored for this object.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getPaymentMethods()
    {
        
        $paymentMethods = @json_decode($this->{$this->field_payment_methods}, true);
        
        if(!is_array($paymentMethods))
        {
            
            $paymentMethods = Array();
            
        }
        
        return $paymentMethods;
        
    }
    
    /**
     * Checks if this object has the specified payment method activated.
     * 
     * @author Peter Hamm
     * @param String $paymentMethodCode E.g. "paypal"
     * @return bool
     */
    public function hasPaymentMethod($paymentMethodCode)
    {
     
        PerisianFrameworkToolbox::security($paymentMethodCode);
        
        $paymentMethodCode = strtolower($paymentMethodCode);
        
        $paymentMethods = $this->getPaymentMethods();
        
        return in_array($paymentMethodCode, $paymentMethods);
        
    }
    
    /**
     * Retrieves a list of all possible payment methods available,
     * independently from which payment modules are actually activated.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getPossiblePaymentMethods()
    {
        
        $paymentMethods = Array(
            
            "paypal" => PerisianLanguageVariable::getVariable('p59dde99bc7d89')
            
        );
        
        return $paymentMethods;
        
    }
    
    /**
     * Retrieves the VAT percentage, e.g. "19".
     * 
     * @author Peter Hamm
     * @return int
     */
    public function getPercentageVat()
    {
        
        $percentage = round(($this->getRateVat() - 1) * 100);
                
        return (int)$percentage;
        
    }
    
    /**
     * Returns a formatted String for the specified price amount.
     * 
     * @author Peter Hamm
     * @param float $price
     * @return String
     */
    public static function formatPriceAmount($price)
    {
        
        if($price < 0)
        {
            
            $price = 0;
            
        }
        
        $price = round($price, 2);
        $price = number_format($price, 2, ".", "");
        
        return $price;
        
    }
    
}
