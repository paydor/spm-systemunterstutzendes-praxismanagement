<?php

require_once 'framework/PerisianUser.class.php';

/**
 * Extendes user class with additional functionality for calSy
 * 
 * @author Peter Hamm
 * @date 2016-02-02
 */
class CalsyUserBackend extends PerisianUser
{
    
    const DEFAULT_USER_ID_WORK_PLAN = '-10';
    
    private static $instance;
    
    public $field_bookable_times = 'user_bookable_times';
    
    private static $calendarSettingsIds = array(
        
        "can_manage_user_appointments" => 62,
        "can_receive_calendar_emails_other_users" => 81,
        "can_receive_calendar_emails_user_frontend" => 82,
        "user_calendar_appointment_assistant_selectable" => 139
        
    );
    
    private $calendarSettingList = array();
    
    protected $specialSystemUserData = Array(
        
        CalsyUserBackend::DEFAULT_USER_ID_WORK_PLAN => Array(
            
            // "Work plan"
            
            'fullname' => 'p5a340dbeaf5af',
            'color' => 'rgb(0, 179, 25)',
            'image' => ''
            
        ),
        
        PerisianUser::DEFAULT_USER_ID => Array(
            
            'fullname' => 'p5d3ea4cf16b01',
            'color' => '#00b319',
            'image' => ''
            
        )
        
    );
    
    /**
     * Constructor 
     * 
     * @param int $userId
     */
    public function __construct($userId = 0)
    {
        
        parent::__construct($userId);
                
        if(isset($userId) && strlen($userId) > 0 && (int)$userId > 0 && static::isSpecialUser($userId))
        {

            $this->{$this->field_fullname} = static::getUserFullname($userId);
            $this->{$this->field_image_profile} = static::getUserImageProfile($userId);
            $this->{$this->field_color} = static::getUserColor($userId);
            
        }
        
    }
    
    /**
     * Checks whether the user with the specified identifier is a special user or not.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return Boolean
     */
    public static function isSpecialUser($userId)
    {
        
        $instance = static::getInstance();
                        
        return isset($instance->specialSystemUserData[$userId]);      
        
    }
    
    /**
     * Retrieves an empty instance of this class.
     * 
     * @author Peter Hamm
     * @return static
     */
    protected static function getInstance()
    {
        
        if(is_null(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Tries to load the user's color for the specified identifier
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserColor($userId)
    {
        
        $color = '';
        
        if($userId > 0)
        {
                        
            $color = parent::getUserColor($userId);
            
        }
        else 
        {
                        
            $dummy = static::getInstance();

            if(isset($dummy->specialSystemUserData[$userId]))
            {
                
                $color = $dummy->specialSystemUserData[$userId]['color'];

            }
            
        }
        
        return $color;
        
    }
    
    /**
     * Retreves the profile image address for the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserImageProfile($userId)
    {
        
        $image = '';
        
        if($userId > 0)
        {
            
            $image = parent::getUserImageProfile($userId);
            
        }
        else 
        {
            
            $dummy = static::getInstance();
            
            if(isset($dummy->specialSystemUserData[$userId]))
            {
                
                $image = @$dummy->specialSystemUserData[$userId]['image'];
                
            }
            
        }
        
        return $image;
        
    }
    
    
    /**
     * Tries to load the user's fullname for the specified user ID
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserFullname($userId)
    {
        
        $fullname = '';
        
        if($userId > 0)
        {
            
            $fullname = parent::getUserFullname($userId);
                        
        }
        else 
        {
            
            $dummy = static::getInstance();
            
            if(isset($dummy->specialSystemUserData[$userId]))
            {
                                
                $fullname = PerisianLanguageVariable::getVariable($dummy->specialSystemUserData[$userId]['fullname']);
                
            }
            
        }
        
        return $fullname;
        
    }
    
    /**
     * Checks if the entry with the specified identifier is able to get 
     * booked at the day of the specified timestamp.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @param int $timestamp
     * @return bool
     */
    public static function isBookableOnDayForTimestamp($userId, $timestamp)
    {
        
        $object = new static($userId);

        $weekdaysEnabled = $object->getWeekdaysEnabled();
        
        $dayIndex = date("N", $timestamp) - 1;
        
        if(!in_array($dayIndex, $weekdaysEnabled))
        {
            
            return false;
            
        }
        
        return true;
        
    }
    
    /**
     * Sets the day indices that are enabled for this area.
     * 
     * @author Peter Hamm
     * @return bool Successful or not
     * @param Array $weekdayList A simple Array of day indices, e.g. '0' for Monday' or '6' for Sunday.
     */
    public function setWeekdaysEnabled($weekdayList)
    {
        
        if(!is_array($weekdayList))
        {
            
            return false;
            
        }
        
        $weekdaysEnabled = Array();
        
        for($i = 0; $i < count($weekdayList); ++$i)
        {
            
            $dayData = Array(
                
                'day' => $weekdayList[$i],
                'enabled' => true
                
            );
            
            array_push($weekdaysEnabled, $dayData);
            
        }
        
        $this->{$this->field_bookable_times} = json_encode($weekdaysEnabled);
        
        return true;
        
    }
    
    /**
     * Retrieves a list of enabled weekday indices as a simple Array.
     * 
     * @author Peter Hamm
     * @return array
     */
    public function getWeekdaysEnabled()
    {
        
        $daysEnabled = Array();
        
        if(isset($this->{$this->field_bookable_times}))
        {
            
            $bookableTimes = json_decode($this->{$this->field_bookable_times}, true);
            
            if(is_array($bookableTimes))
            {
            
                foreach($bookableTimes as $bookableTime)
                {

                    if($bookableTime['enabled'])
                    {

                        array_push($daysEnabled, $bookableTime['day']);

                    }

                }
            
            }
            
        }
        
        return $daysEnabled;
        
    }
    
    /**
     * Retrieves the calendar setting identifier for the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return int
     */
    public static function getCalendarSettingId($key)
    {
        
        return @self::$calendarSettingsIds[$key];
        
    }
    
    
    /**
     * Retrieves a list of all data stored in connection with this user.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getDataDisclosure()
    {
        
        $result = Array();
        
        {
            
            {

                $entryDummy = new CalsyCalendarEntry();
            
                $filter = new CalsyCalendarFilter();

                $filter->setUserBackendIdentifiers($this->{$this->field_pk});
                $filter->setIgnoreConfirmationStatus(true);

                $dataCalendar = CalsyCalendarEntry::getCalendarEntriesForFilter($filter);

                $dataCalendarFormatted = Array();
                
                for($i = 0; $i < count($dataCalendar); ++ $i)
                {
                    
                    $formattedData = Array(
                        
                        $entryDummy->field_pk => $dataCalendar[$i][$entryDummy->field_pk],
                        $entryDummy->field_title => $dataCalendar[$i][$entryDummy->field_title],
                        $entryDummy->timestamp_begin => $dataCalendar[$i][$entryDummy->timestamp_begin],
                        $entryDummy->timestamp_end => $dataCalendar[$i][$entryDummy->timestamp_end]
                        
                    );
                    
                    array_push($dataCalendarFormatted, $formattedData);
                    
                }
                
            }
        
            $result['calendar'] = Array(

                'title' => PerisianLanguageVariable::getVariable('p5aeddf62cc7b2'),
                'count_entries' => count($dataCalendar),
                'data' => $dataCalendarFormatted

            );
            
        }
        
        return $result;
        
    }
    
    /**
     * Deletes an user by ID, inclding all of his appointments.
     * 
     * @param int $userId Optional: An user's ID, default: ID of the object
     * @return void
     */
    public function deleteUser($userId = 0)
    {

        PerisianFrameworkToolbox::security($userId);
        
        if(empty($userId))
        {
            
            $userId = $this->{$this->field_pk};
            
        }
        
        if($userId > 0)
        {
            
            if(static::isEnabledAnonymizationOnDelete())
            {
             
                // Keep all related data, but anonymize it and make a default user the owner.
                
                CalsyCalendarEntry::changeUserIdForEntries($userId, static::DEFAULT_USER_ID);
                
            }
            else
            {
                
                // Delete all related data

                $calendarEntryDummy = new CalsyCalendarEntry();
                $calendarEntryDummy->delete($calendarEntryDummy->field_user_id . "=" . $userId);
                
            }
        
        }

        CalsyCalendarEntryNotificationRelationUser::deleteNotificationSettingsForRelation($userId);
        
        return parent::deleteUser($userId);

    }
    
    /**
     * Filter the calendar settings from the specified setting list.
     * 
     * @author Peter Hamm
     * @param array $settingList
     * @return array
     */
    public static function filterCalendarSettings(&$settingList)
    {
        
        $calendarSettingList = array();
        
        $settingDummy = new PerisianSetting();
        
        foreach($settingList as $settingKey => $value)
        {
            
            if(in_array($settingList[$settingKey][$settingDummy->field_pk], static::$calendarSettingsIds))
            {
                
                array_push($calendarSettingList, $settingList[$settingKey]);
                
                unset($settingList[$settingKey]);
                
            }
            
        }
        
        return $calendarSettingList;
        
    }
    
    /**
     * Retrieves if this user can receive calendar-related e-mails from others users. 
     * 
     * @date 2016-03-10
     * @author Peter Hamm
     * @return boolean
     */
    public function canReceiveCalendarMailFromOtherUsers()
    {
        
        for($i = 0; $i < count($this->calendarSettingList); ++$i)
        {
            
            $setting = $this->calendarSettingList[$i]["setting"];
            $userSetting = $this->calendarSettingList[$i]["userSetting"];
            
            if($setting->{$setting->field_pk} == self::$calendarSettingsIds["can_receive_calendar_emails_other_users"])
            {
                
                return $userSetting->getValue() == 1;
                
            }
            
        }
        
        return false;
        
    }
        
    /**
     * Retrieves if this user can receive calendar-related e-mails from frontend users. 
     * 
     * @date 2016-03-10
     * @author Peter Hamm
     * @return boolean
     */
    public function canReceiveCalendarMailFromUserFrontend()
    {
        
        for($i = 0; $i < count($this->calendarSettingList); ++$i)
        {
            
            $setting = $this->calendarSettingList[$i]["setting"];
            $userSetting = $this->calendarSettingList[$i]["userSetting"];
            
            if($setting->{$setting->field_pk} == self::$calendarSettingsIds["can_receive_calendar_emails_user_frontend"])
            {
                
                return $userSetting->getValue() == 1;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Checks whether this user can be selected in the appointment assistant or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public function canBeSelectedInAppointmentAssistant()
    {
        
        for($i = 0; $i < count($this->calendarSettingList); ++$i)
        {
            
            $setting = $this->calendarSettingList[$i]["setting"];
            $userSetting = $this->calendarSettingList[$i]["userSetting"];
            
            if($setting->{$setting->field_pk} == self::$calendarSettingsIds["user_calendar_appointment_assistant_selectable"])
            {
                                
                return $userSetting->getValue() == 1;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves if this user can create appointments for other users
     * 
     * @date 2016-02-02
     * @author Peter Hamm
     * @return boolean
     */
    public function canManageEventsForOthers()
    {
        
        for($i = 0; $i < count($this->calendarSettingList); ++$i)
        {
            
            $setting = $this->calendarSettingList[$i]["setting"];
            $userSetting = $this->calendarSettingList[$i]["userSetting"];
            
            if($setting->{$setting->field_pk} == self::$calendarSettingsIds["can_manage_user_appointments"])
            {
                
                return $userSetting->getValue() == 1;
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Overridden method, also loads calendar settings
     * 
     * @author Peter Hamm
     * @param type $userId
     */
    protected function loadSettings($userId = 0)
    {
        
        if($userId == 0)
        {
            
            $userId = $this->{$this->field_pk};
            
        }
        
        if($userId > 0)
        {
            
            parent::loadSettings($userId);
            
        }
        
        $this->loadPossibleCalendarSettings();
        
    }
    
    /**
     * Retrieves a list of all possible calendar settings for this user
     * 
     * @author Peter Hamm
     * @return array
     */
    public function getPossibleCalendarSettings()
    {
                     
        return $this->calendarSettingList;
        
    }
    
    /**
     * Retrieves a list of all possible calendar settings for this user
     * 
     * @author Peter Hamm
     * @return array
     */
    public function loadPossibleCalendarSettings()
    {
                
        $calendarSettingList = array();
        
        foreach(self::$calendarSettingsIds as $settingId)
        {
            
            $setting = new PerisianSetting('', $settingId);
            $userSetting = new PerisianUserSetting($settingId, $this->{$this->field_pk});

            array_push($calendarSettingList, array(
                
                'setting' => $setting, 
                'userSetting' => $userSetting
                
            ));

        }
        
        $this->calendarSettingList = $calendarSettingList;
        
    }
    
    /**
     * Checks if the user wants to receive calendar related e-mail from the system
     * and if so, sends an e-mail that a calendar entry has just been canceled
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserFrontend $sendingUserFrontend The frontend user who canceled it.
     * @param bool $ignoreTimezone Default: false
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryCanceled(CalsyCalendarEntry $calendarEntry, CalsyUserFrontend $sendingUserFrontend, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(is_null($sendingUserFrontend))
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
                
        // Check if the user wants to receive mail
        if(!$this->canReceiveCalendarMailFromUserFrontend())
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
                
        // Send the e-mail to the user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);

                // Further details
                {

                    if(CalsyAreaModule::isEnabled())
                    {

                        $areaDetails = CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($calendarEntry->{$calendarEntry->field_pk}, $sendingUserFrontend->{$sendingUserFrontend->field_pk});

                        $areaId = $areaDetails->{$areaDetails->field_area_id};

                        $areaObject = CalsyArea::getAreaById($areaId);

                        $entryDetails .= "\n" . PerisianLanguageVariable::getVariable(11059) . ': ' . $areaObject->{$areaObject->field_title};

                    }
                    
                    // Add the reason for the cancellation
                    {
                        
                        $userFrontendOrderObject = CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($calendarEntry->{$calendarEntry->field_pk}, $sendingUserFrontend->{$sendingUserFrontend->field_pk});

                        $adjustedMailContent = "";

                        if(strlen($userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation_message}) > 0)
                        {

                            $adjustedMailContent .= "\n\n";
                            $adjustedMailContent .= '<span style="border: 1px solid #f7d7be; background-color:#faebd4; color: #8a6d3b; padding: 15px; display: inline-block;"><b>' . PerisianLanguageVariable::getVariable('p575ecd6d29468') . ":</b> ";
                            $adjustedMailContent .= $userFrontendOrderObject->{$userFrontendOrderObject->field_status_confirmation_message} . "</span>";

                        }

                        $entryDetails .= $adjustedMailContent;

                    }
                    
                }
                                
                $subject = PerisianLanguageVariable::getVariable('p58bbcc312c06e') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                
                $adjustedMailContent = PerisianLanguageVariable::getVariable('p58bbccbd2c8ee');
                $adjustedMailContent = str_replace("%sendingUserFrontendFullName", $sendingUserFrontend->getFullName(), $adjustedMailContent);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress() . "event/overview/" . ($entryByUserFrontend ? "?ul=1&su=1&p=" . $sendingUserFrontend->{$sendingUserFrontend->field_pk} : "");

                $content = PerisianLanguageVariable::getVariable(10961) . "\n\n";
                $content = str_replace("%1", $this->{$this->field_fullname}, $content);
                $content = str_replace("%2", $adjustedMailContent, $content);
                $content = str_replace("%3", $entryDetails, $content);
                $content = str_replace("%4", $displayLink, $content);
                $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->{$this->field_email}, 'name' => $this->{$this->field_fullname})

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Sends out a default info email about a calendar entry with customized content.
     * Will include the calendar entry in ICS format as an attachment.
     * 
     * @author Peter Haamm
     * @param String $subject
     * @param String $customContent
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser
     * @param CalsyUserFrontend $sendingUserFrontend
     * @param bool $ignoreTimezone
     * @return boolean
     */
    protected function sendEmailCalendarEntryInfo($subject, $customContent, CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
                            
        if(is_null($sendingUserFrontend) && is_null($sendingUser))
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);
        
        // Check if the user wants to receive mail
        if(($entryByUserFrontend && !$this->canReceiveCalendarMailFromUserFrontend()) || (!$entryByUserFrontend && !$this->canReceiveCalendarMailFromOtherUsers()))
        {
                        
            return false;
            
        }
        
        // Send the e-mail to the user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);

                // Area details
                {
                    
                    if(@$sendingUserFrontend != null)
                    {
                        
                        if(CalsyAreaModule::isEnabled())
                        {

                            $areaDetails = CalsyCalendarEntryUserFrontendOrder::getObjectForUserFrontendAndEntry($calendarEntry->{$calendarEntry->field_pk}, $sendingUserFrontend->{$sendingUserFrontend->field_pk});

                            $areaId = $areaDetails->{$areaDetails->field_area_id};

                            $areaObject = CalsyArea::getAreaById($areaId);

                            $entryDetails .= "\n" . PerisianLanguageVariable::getVariable(11059) . ': ' . $areaObject->{$areaObject->field_title};

                        }
                        
                    }
                    else if(@$sendingUser != null)
                    {
                        
                        $userFrontendOrderDummy = new CalsyCalendarEntryUserFrontendOrder();
                     
                        $userFrontendOrderList = CalsyCalendarEntryUserFrontendOrder::getUserFrontendOrderListForCalendarEntry($calendarEntry->{$calendarEntry->field_pk});
                        
                        $userFrontendList = array();
                        $areaList = array();
                        
                        foreach($userFrontendOrderList as $userFrontendOrderRelation)
                        {
                            
                            $userFrontendObject = new CalsyUserFrontend($userFrontendOrderRelation[$userFrontendOrderDummy->field_user_frontend_id]);
                            array_push($userFrontendList, $userFrontendObject->getFullName());
                            
                            if(CalsyAreaModule::isEnabled())
                            {
                                
                                $areaObject = CalsyArea::getAreaById($userFrontendOrderRelation[$userFrontendOrderDummy->field_area_id]);
                                array_push($areaList, $areaObject->{$areaObject->field_title});

                            }
                            
                            if(count($userFrontendList) > 0)
                            {
                                
                                $entryDetails .= "\n" . PerisianLanguageVariable::getVariable(count($userFrontendList) == 1 ? 10784 : 10794) . ': ' . implode(", ", $userFrontendList);
                                
                            }
                            
                            if(count($areaList) > 0)
                            {
                                
                                $entryDetails .= "\n" . PerisianLanguageVariable::getVariable(count($areaList) == 1 ? 11059 : 11015) . ': ' . implode(", ", $areaList);
                                
                            }
                            
                        }
                        
                        if($calendarEntry->{$calendarEntry->field_user_id} != $this->{$this->field_pk})
                        {
                            
                            $calendarEntryUser = new CalsyUserBackend($calendarEntry->{$calendarEntry->field_user_id});
                            
                            $entryDetails .= "\n" . PerisianLanguageVariable::getVariable(10061) . ': ' . $calendarEntryUser->{$calendarEntryUser->field_fullname};
                            
                        }
                                                
                    }
                    
                }
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress() . "event/overview/" . ($entryByUserFrontend ? "?ul=1&su=1&p=" . $sendingUserFrontend->{$sendingUserFrontend->field_pk} : "");
                                
                // Handle content variables
                {
                    
                    $content = PerisianLanguageVariable::getVariable(10961) . "\n\n";
                    
                    $content = str_replace("%1", $this->{$this->field_fullname}, $content);
                    $content = str_replace("%2", $customContent, $content);
                    $content = str_replace("%3", $entryDetails, $content);
                    $content = str_replace("%4", $displayLink, $content);
                    $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);

                    $content = nl2br($content);
                
                }
                                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->{$this->field_email}, 'name' => $this->{$this->field_fullname})

            ));
                
            // Add the calendar entry in ICS format as an attachment
            try
            {
                
                $export = new CalsyCalendarExport($calendarEntry->{$calendarEntry->field_pk});
                
                $attachment = Array(
                    
                    "name" => "calSy.ics",
                    "path" => $export->getExportFilePath()
                        
                );
                
                PerisianMail::addAttachmentToMessage($mailMessage, $attachment);
                
                $export->deleteExportFile();
            
            }
            catch(PerisianException $e)
            {
                
                // The attachment could not be created.
                // Possible rights issue, set "files/temp/" to chmod 777.
                
            }
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                                
            }
            
        }
        
        return true;
        
    }
    
    /**
     * Checks if the user wants to receive calendar related e-mail from the system
     * and if so, sends an e-mail that a new entry has been made for him.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Default: false
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryNew(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }

        // Build the content
        {
            
            $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);
        
            $content = PerisianLanguageVariable::getVariable($entryByUserFrontend ? 10962 : (@$calendarEntry->{$calendarEntry->field_user_id} == @$sendingUser->{$sendingUser->field_pk} ? 10963 : 'p5749db935206a'));
            $content = $calendarEntry->isPaymentRequired() ? PerisianLanguageVariable::getVariable('p5a319db8c73ff') : $content;

            if(!is_null($sendingUser))
            {

                $content = str_replace("%sendingUserFullName", $sendingUser->{$sendingUser->field_fullname}, $content);

            }

            if(!is_null($sendingUserFrontend))
            {

                $content = str_replace("%sendingUserFrontendFullName", $sendingUserFrontend->getFullName(), $content);

            }
            
            $subject = PerisianLanguageVariable::getVariable($entryByUserFrontend ? 10959 : 10960) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            
        }
                
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);
                
        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Checks if the user wants to receive calendar related e-mail from the system
     * and if so, sends an e-mail that an entry has been paid for.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the backend user owning the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the backend user who paid.
     * @param bool $ignoreTimezone Default: false
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryPaymentReceived(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser, CalsyUserFrontend $sendingUserFrontend, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }

        // Build the context
        {
            
            $content = PerisianLanguageVariable::getVariable('p5a3198782e84b');
            $content = str_replace("%sendingUserFrontendFullName", $sendingUserFrontend->getFullName(), $content);

            $subject = PerisianLanguageVariable::getVariable('p5a31981c9b6f0') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
            
        }
        
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Checks if the user wants to receive calendar related e-mail from the system
     * and if so, sends an e-mail that an entry has been changed for him.
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param CalsyUserBackend $sendingUser Optional, the user creating the calendar entry.
     * @param CalsyUserFrontend $sendingUserFrontend Optional, the frontend user creating the calendar entry.
     * @param bool $ignoreTimezone Default: false
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryChanged(CalsyCalendarEntry $calendarEntry, CalsyUserBackend $sendingUser = null, CalsyUserFrontend $sendingUserFrontend = null, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }

        // Build the context
        {
            
            $entryByUserFrontend = is_null($sendingUser) && !is_null($sendingUserFrontend);
        
            $content = PerisianLanguageVariable::getVariable($entryByUserFrontend ? 'p5925bfc87b45d' : 'p5925bf5de6bab');

            if(!is_null($sendingUser))
            {

                $content = str_replace("%sendingUserFullName", $sendingUser->{$sendingUser->field_fullname}, $content);

            }

            if(!is_null($sendingUserFrontend))
            {

                $content = str_replace("%sendingUserFrontendFullName", $sendingUserFrontend->getFullName(), $content);

            }
            
            $subject = PerisianLanguageVariable::getVariable('p5925bd669bdd7') . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                        
        }
        
        $returnValue = $this->sendEmailCalendarEntryInfo($subject, $content, $calendarEntry, $sendingUser, $sendingUserFrontend, $ignoreTimezone);

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return $returnValue;
        
    }
    
    /**
     * Sends an e-mail as a reminder for an upcoming appointment
     * 
     * @param CalsyCalendarEntry $calendarEntry
     * @param bool $ignoreTimezone Optional, default: false.
     * @return boolean Was the mail sent successfully?
     */
    public function sendEmailCalendarEntryReminder(CalsyCalendarEntry $calendarEntry, $ignoreTimezone = false)
    {
        
        if(is_object($calendarEntry))
        {
            
            $calendarEntry = clone $calendarEntry;
            $calendarEntry->updateDuration();
            
        }
        
        if(!CalsyCalendarNotification::isEnabled($this))
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        $isExistingEntry = strlen($calendarEntry->{$calendarEntry->field_pk}) > 0;
        
        // Send the e-mail to the frontend user
        {
            
            // The text content
            {
                
                $entryDetails = $calendarEntry->getDetailText($ignoreTimezone);
                
                $displayLink = PerisianFrameworkToolbox::getServerAddress() . "calendar/overview/?se=" . $calendarEntry->{$calendarEntry->field_pk};
                
                $subject = PerisianLanguageVariable::getVariable(11193) . " - " . CalsyCalendarEntry::getFormattedEventTimeString($calendarEntry->{$calendarEntry->timestamp_begin}, $calendarEntry->{$calendarEntry->timestamp_end}, $ignoreTimezone);
                $adjustedMailContent = PerisianLanguageVariable::getVariable(11194);
                                
                $content = PerisianLanguageVariable::getVariable(10961) . "\n\n";
                $content = str_replace("%1", $this->{$this->field_fullname}, $content);
                $content = str_replace("%2", $adjustedMailContent, $content);
                $content = str_replace("%3", $entryDetails, $content);
                $content = str_replace("%4", $displayLink, $content);
                $content = str_replace("%5", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->{$this->field_email}, 'name' => $this->{$this->field_fullname})

            ));
            
            try
            {
                
                PerisianMail::sendMail($mailMessage);
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Checks whether this user should be invisible to other users not not
     *
     * @author Peter Hamm
     * @return boolean
    */
    public function isInvisible()
    {
        
        $setting = $this->getUserSetting();
        
        return self::isInvisibleSettingSet($this->getUserSetting());
        
    }

    /**
     * Checks whether this user should be invisible to other users not not
     *
     * @author Peter Hamm
     * @param PerisianUserSetting $settingsObject An optional object to read from
     * @return boolean
     */
    protected static function isInvisibleSettingSet($settingsObject)
    {

        if(!is_object(@$settingsObject))
        {
            
            return false;
            
        }
        
        return $settingsObject->getSetting(null, 'user_calendar_is_invisible') == 1;

    }
    
    /**
     * Retrieves a list of users that have the specified setting value set, additionally the value must not be empty.
     * 
     * @author Peter Hamm
     * @param String $settingKey
     * @return Array An array of user data
     */
    public static function getListUsersWhereSettingHasValueNotEmpty($settingKey)
    {
        
        $dependencyList = PerisianUserSetting::getDependencyForSettingValueIsNot($settingKey, '');
        
        $instance = new static();
        
        $listUsers = Array();
        
        if(count($dependencyList) > 0)
        {
            
            $query = "{$instance->field_pk} IN (" . implode(",", $dependencyList) . ")";

            $listUsers = $instance->getData($query, $instance->field_fullname, 'ASC', 0, 0, 'field_login_password');
        
        }
        
        return $listUsers;
        
    }
        
}
