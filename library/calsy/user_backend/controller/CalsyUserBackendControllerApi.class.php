<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "CalsyUserBackendController.class.php";

class CalsyUserBackendControllerApi extends PerisianControllerRestObject
{
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "CalsyUserBackendController";
        
    }
    
    /**
     * Returns the name of the standard object for this resource.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getObjectName()
    {
        
        return "CalsyUserBackend";
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url GET /$identifier
     * @return Array
     */
    public function getObject($identifier)
    {
        
        $this->requireAccess('user_backend_read');
        
        $data = parent::getObject($identifier);
        
        $dummyObject = parent::getDatabaseObject();
        
        unset($data[$dummyObject->field_login_password]);
        
        return $data;
        
    }
    
    /**
     * Creates a new object with the data posted.
     *
     * @author Peter Hamm
     * @url POST /
     * @return Array
     */
    public function createObject()
    {
        
        $this->requireAccess('user_backend_write');
        
        $object = $this->getObjectWithFieldData(0);
        
        $passwordUnencrypted = PerisianFrameworkToolbox::getRequest($object->field_login_password);
        
        if(strlen($passwordUnencrypted) > 0)
        {
            
            $object->{$object->field_login_password} = PerisianUser::encryptPassword($passwordUnencrypted);
        
            $result = $this->saveObject($object);
        
        }
        else
        {
            
            // Invalid password.
            
            $result = Array(
                
                'success' => false,
                'error' => PerisianLanguageVariable::getVariable('p58c7cda7d78c1')
                
            );
            
        }
        
        return $result;
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url PATCH /$identifier
     * @param mixed $identifier The identifier of the entry you want to update.
     * @return Array
     */
    public function updateObject($identifier)
    {
        
        $this->requireAccess('user_backend_write');
        
        $object = $this->getObjectWithFieldData($identifier);
        
        $passwordUnencrypted = PerisianFrameworkToolbox::getRequest($object->field_login_password);
        
        if(strlen($passwordUnencrypted) > 0)
        {
            
            $object->{$object->field_login_password} = PerisianUser::encryptPassword($passwordUnencrypted);
                
        }
        
        $result = $this->saveObject($object);
        
        return $result;
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @url DELETE /$identifier
     * @param mixed $identifier The identifier of the entry you want to delete.
     * @return Array
     */
    public function deleteObject($identifier)
    {
        
        $this->requireAccess('user_backend_write');
        
        return parent::deleteObject($identifier);
        
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $this->requireAccess('user_backend_write');
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->deleteUser();
        
    }
    
    /**
     * Gets a list of entries
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getList()
    {
        
        $this->requireAccess('user_backend_read');
        
        $this->setAction('list');
        
        $result = $this->finalize();
                
        return $result;
        
    }
    
}