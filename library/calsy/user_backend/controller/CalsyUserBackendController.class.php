<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'calsy/vmoso/CalsyVmosoApi.class.php';
require_once 'calsy/privacy/CalsyPrivacyAccountDeletionLog.class.php';

/**
 * Backend user controller
 *
 * @author Peter Hamm
 * @date 2016-11-09
 */
class CalsyUserBackendController extends PerisianController
{
    
    
    /**
     * Provides data to display a page to confirm the deletion of a user's account.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionPrivacyDeleteAccountConfirmation()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'user_backend/privacy/privacy_delete_account_confirmation'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array();
        
        $this->setResultValue('result', $result);
        
    }
      
    /**
     * Provides a disclosure statement of privacy data.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @global bool $dontShowHeaderHtml
     * @global bool $dontShowFooterHtml
     * @return void
     */
    protected function actionPrivacy()
    {
        
        global $user;
        global $dontShowHeaderHtml;
        global $dontShowFooterHtml;
        
        $renderer = array(
            
            'page' => 'user_backend/privacy/privacy_disclosure'
            
        );
        
        //
        
        $do = $this->getParameter('do');
        
        $dataUser = $user->getData($user->field_pk . ' = ' . $user->{$user->field_pk});
        
        $result = array(
            
            'data_disclosure' => $user->getDataDisclosure(),
            'data_user' => $dataUser[0]
                        
        );   
        
        if($do == 'delete')
        {
            
            $dontShowHeaderHtml = true;
            $dontShowFooterHtml = true;
            
            $renderer = array(

                'page' => 'user_backend/privacy/privacy_delete_account'

            );
            
            // Log the deletion
            {
                
                $deletionData = Array(
                    
                    'type' => get_class($user),
                    'time' => time(),
                    'deleted' => Array()
                    
                );
                
                foreach($result['data_disclosure'] as $key => $value)
                {
                    
                    $deletionData['deleted'][$key] = $value['count_entries'];
                    
                }
            
                $logEntry = new CalsyPrivacyAccountDeletionLog();
                
                $logEntry->{$logEntry->field_name} = $user->{$user->field_login_name};
                $logEntry->{$logEntry->field_data} = json_encode($deletionData);
                
                $logEntry->save();
                
            }
                        
            $user->deleteUser();
                        
        }
        
        // 
                
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'user_backend/management/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Resets uploaded files
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetUpload()
    {
        
        $this->setInternal('reset', true);
        
        $this->actionUpload();
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @global $user
     * @return void
     */
    protected function actionUpload()
    {
        
        global $user;
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        // Parameter setup
        {
            
            $subjectId = $this->getParameter('subjectId');
        
            if($user->isAdmin() || $user->isDeveloper())
            {

                $userObj = new CalsyUserBackend($subjectId);

            }
            else
            {

                $userObj = $user;

            }
            
        }
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        try
        {

            $resetFile = false;

            if($this->getInternal('reset') === true)
            {

                $uploadResult = array(

                    "success" => true,
                    "message" => PerisianLanguageVariable::getVariable(11132)

                );

                $resetFile = true;

            }
            else
            {

                $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                
            }

            $pathExistingImage = $userObj->{$userObj->field_image_profile};

            PerisianUploadFileManager::deleteFile('image', $pathExistingImage);

            $userObj->{$userObj->field_image_profile} = ($resetFile ? "" : $uploadResult["file"]);
            $userObj->save();

        }
        catch(Exception $e)
        {
                       
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyUserBackend();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        $sorting = $this->getParameter('sorting', $dummy->field_pk);
        $search = $this->getParameter('search');
          
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'user_backend/management/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => CalsyUserBackend::getUserList($sorting, $sortOrder, $offset, $limit, $search),
            'count' => CalsyUserBackend::getUserCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable(10085));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'user_backend/management/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter setup
        {
            
            $editId = strlen($this->getParameter('editId')) > 0 ? $this->getParameter('editId') : 0;
            
        }
        
        $editEntry = new CalsyUserBackend($editId);
        
        $result = array(
            
            'object' => $editEntry,
            'id' => $editId,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable(10090)
            
        );
        
        $this->setResultValue('result', $result);
                
    }
  
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionDelete()
    {
        
        global $user;
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        if(!$user->isAdmin())
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
            
        }
        
        //
        
        $deleteId = $this->getParameter('deleteId');
        
        if(strlen($deleteId) == 0)
        {

            $result = array(
                
                'success' => false
                
            );
            
            $this->setResultValue('result', $result);
            
            return;

        }
        
        $entryObj = new CalsyUserBackend($deleteId);
        $entryObj->deleteUser();
        
        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves an entry.
     * 
     * @author Peter Hamm
     * @global $user
     * @return void
     */
    protected function actionSave()
    {
        
        global $user;
    
        $renderer = array();
        
        // Parameter setup
        {
            
            $editId = $user->isAdmin() ? (int)$this->getParameter('editId') : $user->{$user->field_pk};
            $step = (int)$this->getParameter('step');

        }
        
        if($step == 1)
        {
            
            $renderer = array(

                'json' => true

            );
            
            $result = array(

                'success' => false

            );
            
            try
            {

                $userName = PerisianFrameworkToolbox::getRequest('user_name', 'post');
                $userFullName = PerisianFrameworkToolbox::getRequest('user_fullname', 'post');
                $userEmail = PerisianFrameworkToolbox::getRequest('user_email', 'post');
                $userPhone = PerisianFrameworkToolbox::getRequest('user_phone', 'post');
                $userStatus = PerisianFrameworkToolbox::getRequest('user_status', 'post');
                $userSortingCustom = PerisianFrameworkToolbox::getRequest('user_sorting_custom', 'post');

                // Validation check
                {

                    $validationCheck = true;

                    if(!PerisianValidation::checkRequired($userName))
                    {
                        
                        $validationCheck = false;
                        
                    }

                    if(!PerisianValidation::checkRequired($userFullName))
                    {
                        
                        $validationCheck = false;
                        
                    }

                    if(!PerisianValidation::checkEmail($userEmail))
                    {
                        
                        $validationCheck = false;
                        
                    }

                    if(!$validationCheck)
                    {
                        
                        // Invalid user data
                        throw new PerisianException(PerisianLanguageVariable::getVariable('p5808ec44add6d'));
                        
                    }

                }

                $userObj = new CalsyUserBackend($editId);
                
                $userObj->{$userObj->field_fullname} = $userFullName;
                $userObj->{$userObj->field_email} = $userEmail;
                $userObj->{$userObj->field_phone} = $userPhone;
                $userObj->{$userObj->field_status} = $userStatus;
                
                if($user->isAdmin() && PerisianSystemSetting::getSettingValue('system_show_backend_users_sorted_by_key') == 1)
                {
                    
                    $userObj->{$userObj->field_sorting_custom} = $userSortingCustom;
                    
                }

                if(empty($editId))
                {

                    // It's a completely new user, set the login name and a random password.
                    // Then inform this user by e-mail that he has to set a password.

                    $userObj->{$userObj->field_login_name} = $userName;
                    $userObj->{$userObj->field_login_password} = CalsyUserBackend::encryptPassword(CalsyUserBackend::getRandomPassword());
                    $userObj->{$userObj->field_created} = PerisianFrameworkToolbox::sqlTimestamp();
                    
                }
                
                // Save it
                {
                    
                    $newId = $userObj->save();

                    if(!empty($newId))
                    {

                        $result = array(

                            'success' => true,
                            'id' => $newId

                        );

                        if(empty($editId))
                        {

                            $userObj->{$userObj->field_pk} = $newId;

                        }

                    }
                    
                }

                
            }
            catch(PerisianException $e)
            {
                
                $result['message'] = $e->getMessage();
                
            }
            
        }
        else if($step == 2)
        {
            
            $renderer = array(

                'blank_page' => true,
                'page' => 'user_backend/management/save'

            );
            
            $title = PerisianLanguageVariable::getVariable(10066);
            $message = PerisianLanguageVariable::getVariable(10101) . ' ' . PerisianLanguageVariable::getVariable(10102);

            if(strlen($editId) > 0)
            {

                $title = PerisianLanguageVariable::getVariable(10104);
                $message = PerisianLanguageVariable::getVariable(10103) . ' ' . $editId;;

            }
                        
            $result = array(

                'title' => $title,
                'message' => $message

            );
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
             
    }
    
    /**
     * Saves specific user data that can be edited.
     * 
     * @author Peter Hamm
     * @global type $user
     * @return void
     */
    protected function actionSaveUserEdit()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            
        }
        
        if($user->isAdmin() || $user->isDeveloper())
        {
            
            $saveElement = new PerisianUser($editId);

        }
        else
        {

            $saveElement = $user;

        }
                        
        $saveElement->{$saveElement->field_phone} = $this->getParameter("user_phone");
        $saveElement->{$saveElement->field_email} = $this->getParameter("user_email");
        $saveElement->{$saveElement->field_color} = $this->getParameter("user_color", $saveElement->{$saveElement->field_color});

        if($user->isAdmin() && PerisianSystemSetting::getSettingValue('system_show_backend_users_sorted_by_key') == 1)
        {

            $saveElement->{$saveElement->field_sorting_custom} = $this->getParameter('user_sorting_custom');

        }
        
        $newId = $saveElement->save();
        
        if($newId > 0)
        {
            
            $result = array(
                
                'success' => true, 
                'newId' => $newId
                    
            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves calendar specific settings for the specified backend user.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionSaveCalendarSettings()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $editId = $this->getParameter('editId');
            $updatedSettings = $this->getParameter('settings', 'post');
            $disabledSettingsForOwnAccount = array();
            
        }

        // Get the possible settings
        if($user->isAdmin() || $user->isDeveloper())
        {
            
            $userObj = new CalsyUserBackend($editId);

        }
        else
        {

            $userObj = $user;

        }
        
        if($userObj->{$userObj->field_pk} == $user->{$user->field_pk})
        {
            
            array_push($disabledSettingsForOwnAccount, CalsyUserBackend::getCalendarSettingId('can_manage_user_appointments'));
            
        }
        
        try
        {
            
            $userObj->{$userObj->field_color} = PerisianFrameworkToolbox::getRequest("user_color", $userObj->{$userObj->field_color});
            $userObj->save();   
            
            // Save all the settings
            foreach($updatedSettings as $settingId => $settingValue)
            {
                
                if(PerisianSetting::getIsAdminSettingForId($settingId) && !$user->isAdmin() || in_array($settingId, $disabledSettingsForOwnAccount))
                {
                                        
                    // This is an administrator setting and the user editing it is not an administrator: Skip it.
                    // Or: The user is not allowed to edit this setting for his own account.
                    // Or: The user is a developer
                    continue;
                    
                }
                
                $settingObject = new PerisianUserSetting($settingId, $userObj->{$userObj->field_pk});
                $settingObject->setValue($settingValue);
                
                $settingObject->save();
                
            }
        
            $result["success"] = true;
            
        }
        catch(Exception $e)
        {
            
            $result["message"] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Provides data for a form to edit the current backend user's password.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditOwnPassword()
    {
                
        $this->setInternal('editOwnPassword', true);
        
        return $this->actionEditPassword();
        
    }
    
    /**
     * Provides data for a form to edit a specified backend user's password.
     * 
     * @global CalsyUserBackend $user
     * @author Peter Hamm
     * @return void
     */
    protected function actionEditPassword()
    {
        
        global $user;
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'user_backend/management/edit_password'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        $editId = null;
        $editOwnPassword = $this->getInternal('editOwnPassword');

        // Parameter setup
        if($editOwnPassword)
        {
            
            $editId = $user->{$user->field_pk};
            
        }
        else
        {
            
            $editId = (int)$this->getParameter('editId');
            
            if(!$user->isAdmin())
            {
                
                throw new PerisianException(lg('10725'));
                
            }

        }
        
        // Password policy info
        {
            
            $passwordPolicy = PerisianValidation::getPasswordPolicy();
            $passwordPolicyDetails = PerisianValidation::getPasswordPolicyDetails();
            
            $passwordPolicyInfo = $passwordPolicyDetails[$passwordPolicy];
            $passwordPolicyInfo['type'] = $passwordPolicy;
                        
        }
        
        $result = array(
            
            'id' => $editId,
            'edit_own_password' => $editOwnPassword,
            'title_form' => PerisianLanguageVariable::getVariable(10105),
            'security_policy_password' => $passwordPolicyInfo
            
        );
        
        $this->setResultValue('result', $result);
                        
    }
    
    /**
     * Saves the bookable weekdays for a user.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionSaveWeekdaysEnabled()
    {
                
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $subjectId = $this->getParameter('subjectId');
            
        }

        // Get the possible settings
        
        if($user->isAdmin() || $user->isDeveloper())
        {
            
            $userObj = new CalsyUserBackend($subjectId);

        }
        else
        {

            $userObj = $user;

        }
        
        $weekdaysEnabled = $this->getParameter('weekdaysEnabled');
        
        $userObj->setWeekdaysEnabled($weekdaysEnabled);
        
        $userObj->save();
        
        $result['success'] = true;
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves the specified settings for the specified user.
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionSaveSettings()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $adminSettingList = array();
            $calendarSettingList = array();
            $possibleSettings = array();
            $disabledList = array();
            $settingObj = new PerisianSetting();

            $subjectId = $this->getParameter('subjectId');
            
            if(strlen($subjectId) == 0)
            {
                
                $subjectId = $user->{$user->field_pk};
                
            }
            
        }

        // Get the possible settings
        
        if($user->isAdmin() || $user->isDeveloper())
        {
            
            $userObj = new CalsyUserBackend($subjectId);
            $adminSettingList = $userObj->getSettingObject()->getPossibleSettings('admin');

        }
        else
        {

            $userObj = $user;

        }
        
        if($userObj->{$userObj->field_pk} == $user->{$user->field_pk})
        {

            // Create a list with rights that the current user cannot change for himself
            
            $disabledSettingObj = new PerisianSystemSetting();
            $disabledSettings = explode(',', $disabledSettingObj->getSetting(7));

            foreach($disabledSettings as $disabledSetting)
            {
                
                $disabledList[] = (int)trim($disabledSetting);
                
            }

        }
        
        $standardSettingList = $userObj->getSettingObject()->getPossibleSettings();
        
        $settingList = array_merge($standardSettingList, $adminSettingList, $calendarSettingList);
        $newValues = Array();

        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }
        
        // Vmoso account settings need to be validated before saving
        {
            
            $vmosoUser = PerisianFrameworkToolbox::getRequest('s_' . PerisianSetting::getIdForName('user_vmoso_account'));
            $vmosoPassword = PerisianFrameworkToolbox::getRequest('s_' . PerisianSetting::getIdForName('user_vmoso_account_password'));
            
            if(strlen($vmosoUser) > 0 || strlen($vmosoPassword) > 0)
            {
                
                $apiConnection = new CalsyVmosoApi();
                
                $apiConnection->setConnectionUser($vmosoUser);
                $apiConnection->setConnectionPassword($vmosoPassword);
                
                if($apiConnection->validateCredentials())
                {
                    
                    $userInfo = $apiConnection->getUserInfo();
                    
                    // Save the Vmoso user key to the calsy user.
                    PerisianUserSetting::setSettingValue($userObj->{$userObj->field_pk}, 'user_vmoso_account_key', $userInfo['key']);
                    
                }
                else
                {
                    
                    // Invalid user data for Vmoso, cancel the save.
                    
                    throw new PerisianException(PerisianLanguageVariable::getVariable('p59442dbec9549'));
                    
                }
                
            }
            
        }

        foreach($possibleSettings as $possibleSettingId)
        {
            
            if(isset($_REQUEST['s_' . $possibleSettingId]) && !in_array($possibleSettingId, $disabledList))
            {

                $newObj = new PerisianUserSetting($possibleSettingId, $userObj->{$userObj->field_pk});
                $newObj->{$newObj->field_setting_value} = PerisianFrameworkToolbox::getRequest('s_' . $possibleSettingId);
                $newObj->save();

            }

        }
        
        $result['success'] = true;
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves the password for the currently logged in user.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveOwnPassword()
    {
                
        $this->setInternal('editOwnPassword', true);
        
        return $this->actionSavePassword();
        
    }
    
    /**
     * Saves a new password for a frontend user
     * 
     * @author Peter Hamm
     * @global CalsyUserBackend $user
     * @return void
     */
    protected function actionSavePassword()
    {
        
        global $user;
        
        $renderer = array();
        
        // Parameter setup
        {
            
            $editId = null;
            $editOwnPassword = $this->getInternal('editOwnPassword');

            // Parameter setup
            if($editOwnPassword)
            {

                $editId = $user->{$user->field_pk};

            }
            else
            {
                
                $editId = (int)$this->getParameter('editId');
                
                if(!$user->isAdmin())
                {
                    
                    throw new PerisianException(lg('10725'));
                    
                }
                
            }
            
            $step = (int)$this->getParameter('step');

        }
        
        if($step == 1)
        {
            
            $renderer = array(

                'json' => true

            );
            
            $result = array(

                'success' => false

            );
            
            $passwordOne = $this->getParameter('user_password_1');
            $passwordTwo = $this->getParameter('user_password_2');
            
            if(PerisianValidation::checkPassword($passwordOne, $passwordTwo) && PerisianValidation::checkId($editId))
            {
                
                $password = PerisianUser::encryptPassword($passwordOne);

                $editObject = new CalsyUserBackend($editId);
                $editObject->{$editObject->field_login_password} = $password;
                
                $result = array(

                    'success' => true,
                    'id' => $editObject->save()

                );
                
            }
            else
            {
                
                $result['message'] = PerisianValidation::getLatestExceptionMessage();
                
            }
            
        }
        else if($step == 2)
        {
            
            $renderer = array(

                'blank_page' => true,
                'page' => 'user_backend/management/save'

            );
            
            $title = PerisianLanguageVariable::getVariable(10066);
            $message = PerisianLanguageVariable::getVariable(10106) . ' ' . PerisianLanguageVariable::getVariable(10102);

            if(strlen($editId) > 0)
            {

                $title = PerisianLanguageVariable::getVariable(10107);
                $message = PerisianLanguageVariable::getVariable("p576bfc539f679");

            }
            
            $result = array(

                'title' => $title,
                'message' => $message

            );
            
        }
        
        $this->setResultValue('renderer', $renderer);
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to edit an user's settings
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSettings()
    {
        
        global $user;

        $renderer = array(

            'page' => 'user_backend/management/settings'

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter initializiation
        {
            
            $userId = $this->getParameter('u');
            $showAdminFunctions = false;
            $disabledList = Array();
            
        }
        
        if(empty($userId) || !$user->isAdmin())
        {
            
            $showAdminFunctions = $user->isDeveloper();
            $userObj = $user;
            
            //$menu->setActiveMenuPoint(24);

        }
        else
        {
            
            $showAdminFunctions = true;
            $userObj = new CalsyUserBackend($userId);
            
            //$menu->setActiveMenuPoint(5);
            
        }
        
        if($userObj->{$userObj->field_pk} == $user->{$user->field_pk})
        {

            // Create a list with rights that the current user cannot change for himself

            $disabledSettingObj = new PerisianSystemSetting();
            $disabledSettings = explode(',', $disabledSettingObj->getSetting(PerisianFrameworkToolbox::getConfig('system/setting_id/blocked_settings')));

            foreach($disabledSettings as $disabledSetting)
            {
                
                $disabledList[] = (int)trim($disabledSetting);
                
            }

        }

        // Load the rights
        {

            $adminSettingList = Array();
            
            if($showAdminFunctions)
            {
                
                $adminSettingList = $userObj->getSettingObject()->getPossibleSettings('admin', $disabledList, array(62));
                
            }
               
            $standardSettingList = $userObj->getSettingObject()->getPossibleSettings('', $disabledList);
            $userSettings = $userObj->getSettingObject()->getAssociativeValues();
            
            $calendarSettingsList = CalsyUserBackend::filterCalendarSettings($standardSettingList);

            if(!isset($userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_language')]))
            {

                // If no language was selected yet, set the default language
                $languageSettingId = PerisianFrameworkToolbox::getConfig('system/setting_id/default_language');
                $settingObj = new PerisianSystemSetting();
                $userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_language')] = $settingObj->getSetting($languageSettingId);
                
            }
            
            if(!isset($userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_format_time')]))
            {

                // If no time format was set yet, set the default value
                $timeSettingId = PerisianFrameworkToolbox::getConfig('system/setting_id/default_format_time');
                $settingObj = new PerisianSystemSetting();
                $userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_format_time')] = $settingObj->getSetting($timeSettingId);

            }
            
            if(!isset($userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_format_date')]))
            {

                // If no date format was set yet, set the default value
                $dateSettingId = PerisianFrameworkToolbox::getConfig('system/setting_id/default_format_date');
                $settingObj = new PerisianSystemSetting();
                $userSettings[PerisianFrameworkToolbox::getConfig('system/setting_id/user_format_date')] = $settingObj->getSetting($dateSettingId);

            }
            
            if(!isset($userSettings[PerisianSetting::getIdForName('user_calendar_notification_default_time')]))
            {

                // If not set for the user, retrieve the system's default
                $userSettings[PerisianSetting::getIdForName('user_calendar_notification_default_time')] = PerisianSystemSetting::getSettingValue('calendar_default_time_notifications');

            } 
            
            if(!isset($userSettings[PerisianSetting::getIdForName('user_calendar_notification_is_enabled')]))
            {

                // If not set for the user, retrieve the system's default
                $userSettings[PerisianSetting::getIdForName('user_calendar_notification_is_enabled')] = PerisianSystemSetting::getSettingValue('calendar_is_enabled_notifications_users');

            }

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {
                
                foreach($standardSettingList as $standardSettingListKey => &$standardSettingListEntry)
                {
                    
                    if(isset($userSettings[$standardSettingListEntry[$userObj->getSettingObject()->foreign_field_pk]]))
                    {
                        
                        $standardSettingList[$standardSettingListKey]['value'] = $userSettings[$standardSettingListEntry[$userObj->getSettingObject()->foreign_field_pk]];
                        
                    }

                }

                foreach($adminSettingList as $adminSettingListKey => &$adminSettingListEntry)
                {

                    if(isset($userSettings[$adminSettingListEntry[$userObj->getSettingObject()->foreign_field_pk]]))
                    {
                        
                        $adminSettingList[$adminSettingListKey]['value'] = $userSettings[$adminSettingListEntry[$userObj->getSettingObject()->foreign_field_pk]];
                        
                    }

                }
                
                // Filter the admin settings
                {
                    
                    if(!CalsyBookableTimesModule::isEnabled())
                    {
                        
                        // Kick out the setting, if the module is disabled / blocked
                        
                        PerisianUserSetting::retrieveSettingsByPrefix('is_allowed_editing_all_users_bookable_times', $adminSettingList, true);
                        
                    }
                    
                }
                
                // Add the calendar notification settings to a seperate list
                $calendarNotificationSettingList = PerisianUserSetting::retrieveSettingsByPrefix('user_calendar_notification_', $standardSettingList, true);
                
                // Add the Vmoso backend chat settings to a seperate list
                $vmosoBackendSettingList = PerisianUserSetting::retrieveSettingsByPrefix('user_vmoso', $standardSettingList, true);
                
                // Add the process engine settings to a seperate list
                $processEngineSettingList = PerisianUserSetting::retrieveSettingsByPrefix('process_engine_user', $adminSettingList, true);
                
                // Kick out some hidden settings
                {
                                        
                    PerisianUserSetting::retrieveSettingsByPrefix('user_backend_vmoso_chat_id', $standardSettingList, true);
                    PerisianUserSetting::retrieveSettingsByPrefix('user_layout', $standardSettingList, true);
                    PerisianUserSetting::retrieveSettingsByPrefix('user_calendar_filter_users', $standardSettingList, true);
                    
                }
                                                                
            }

            // Get the languages for the dropdowns in the list
            {
                
                $languageFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
                $languageList = PerisianLanguage::getLanguages();
                $languageListDropdown = Array();

                foreach($languageList as $languageItems)
                {
                    
                    $languageListDropdown[$languageItems[$languageFields['field_pk']]] = $languageItems[$languageFields['field_name']];
                    
                }

                $languageList = $languageListDropdown;

            }
            
            // Possible notification times
            {
               
                $calendarNotificationTimes = CalsyCalendarNotification::getPossibleNotificationTimes();
                
            }

        }

        if($showAdminFunctions)
        {

            $pageDescription = PerisianLanguageVariable::getVariable(10117) . '"' . $userObj->{$userObj->field_login_name} . '" ';
            $pageDescription .= '(' . $userObj->{$userObj->field_fullname} . ').';

        }
        else
        {

            $pageDescription = PerisianLanguageVariable::getVariable(10127);

        }
                
        $result = array(
            
            'id' => $userId,
            'object' => $userObj,
            'description_page' => $pageDescription,
            'show_admin_functions' => $showAdminFunctions,
            
            'list_languages' => $languageList,
            'list_times_notification' => $calendarNotificationTimes,
            
            'list_settings_admin' => $adminSettingList,
            'list_settings_standard' => $standardSettingList,
            'list_settings_calendar_notification' => $calendarNotificationSettingList,
            
            'field_upload_image_profile' => $userObj->getImageProfileData()
            
        );
        
        $result['list_layouts_chat'] = CalsyVmosoModule::getListChatLayouts();
        
        if($user->isAdmin() && CalsyProcessEngineModule::isEnabled())
        {
            
            $result['list_settings_process_engine'] = $processEngineSettingList;
            
        }
        
        if(CalsyVmosoBackendModule::isEnabled())
        {
            
            $result['list_settings_vmoso'] = $vmosoBackendSettingList;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}