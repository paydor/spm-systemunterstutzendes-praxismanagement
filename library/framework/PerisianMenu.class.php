<?php

require_once "framework/PerisianModuleManager.class.php";
require_once "calsy/quick_log/CalsyQuickLog.class.php";

/* 
 * The main menu class of the system,
 * you may extend this class for your needs
 *
 * @author Peter Hamm
 * @date 2010-10-28
 */
class PerisianMenu extends PerisianDatabaseModel
{

    // Database specific members

    public $table = 'system_menu';
    public $field_pk = 'system_menu_id';
    public $field_parent_id = 'system_menu_parent_id';
    public $field_language_variable_id = 'system_menu_language_variable_id';
    public $field_system_settings_required = 'system_menu_system_settings_required';
    public $field_user_settings_required = 'system_menu_user_settings_required';
    public $field_link = 'system_menu_link';
    public $field_order = 'system_menu_order';
    public $field_icon = 'system_menu_icon';

    // Various members
    public $activeMenuPoint = Array();
    
    protected $modulePointList;
    
    protected $pointIdModules = 44;
    
    protected static $instance;

    /**
     * Creates a new object
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct()
    {

        parent::__construct('MySql', 'main');

    }
    
    /**
     * Retrieves an instance of this class
     * 
     * @author Peter Hamm
     * @return PerisianMenu
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }

    /**
     * Gets the data for the specified menu point ID
     *
     * @author Peter Hamm
     * @param int $menuPointId Menu point ID
     * @return Array
     */
    public function getMenuPoint($menuPointId)
    {

        PerisianFrameworkToolbox::security($menuPointId);

        $where = "{$this->field_pk} = {$menuPointId}";
        $menuData = $this->getData($where);
        
        return $menuData[0];

    }

    /**
     * Gets the contents of the menu entry with the specified id, including
     * all children in N dimensions.
     *
     * If an user object is specified as 2nd parameter, only those
     * menu points are returned that this user is allowed to see.
     * 
     * Default: Gets the complete menu
     *
     * @author Peter Hamm
     * @param int $menuPointId Menu point ID
     * @param PerisianUser $userObject Optional: A user object, default: null
     * @param CalsyUserFrontend $userFrontend Optional, a frontend user object, default: null
     * @return Array Menu tree
     */
    public function getMenuPoints($menuPointId = 1, $userObject = null, $userFrontendObject = null)
    {
        
        global $user;

        PerisianFrameworkToolbox::security($menuPointId);

        $where = "{$this->field_parent_id} = {$menuPointId} AND {$this->field_pk} != {$menuPointId}";
        $menuData = $this->getData($where, $this->field_order, 'ASC');
        $menuTree = Array();
        
        for($i = 0, $m = count($menuData); $i < $m; ++$i)
        {

            // User permission check
            if(!$this->checkPermission($menuData[$i][$this->field_user_settings_required], $userObject))
            {
                
                continue;
                
            }
            
            // Frontend user permission check
            if(!$this->checkPermissionForUserFrontend($menuData[$i], $userFrontendObject))
            {
                
                continue;
                
            }
            
            // System permission check
            if(!$this->checkPermissionForSystem($menuData[$i][$this->field_system_settings_required]))
            {
                
                continue;
                
            }

            // Get this point's children and check if this is
            // a title point (would have no link specified then).
            //
            // If it then has no children, it is not being returned

            $link = $this->getFormattedLink($menuData[$i][$this->field_link]);
            $children = $this->getMenuPoints($menuData[$i][$this->field_pk], $userObject);
            
            if($menuData[$i][$this->field_pk] == $this->pointIdModules)
            {
                
                $this->addSystemModulesToList($children);
                
            }

            if(empty($link) && count($children) == 0)
            {
                
                continue;
                
            }
                
            $menuTree[$i]['id'] = $menuData[$i][$this->field_pk];
            $menuTree[$i]['title'] = PerisianLanguageVariable::getVariable($menuData[$i][$this->field_language_variable_id]);
            $menuTree[$i]['link'] = $link["link"];
            $menuTree[$i]['target'] = $link["target"];
            $menuTree[$i]['permission'] = explode(',', $menuData[$i][$this->field_user_settings_required]);
            $menuTree[$i]['permission_sysyem'] = explode(',', $menuData[$i][$this->field_system_settings_required]);
            $menuTree[$i]['icon'] = static::getIconForMenuPoint($menuData[$i]);
            $menuTree[$i]['children'] = $children;
            $menuTree[$i]['order'] = $menuData[$i][$this->field_order];
            
        }
        
        return $menuTree;
        
    }
    
    /**
     * Retrieves the icon for the specified menu point.
     * 
     * @author Peter Hamm
     * @param Array $menuPointData
     * @return String
     */
    protected static function getIconForMenuPoint($menuPointData)
    {
        
        $instance = static::getInstance();
        
        $returnValue = '';
        
        if(isset($menuPointData[$instance->field_icon]))
        {
            
            $returnValue = $menuPointData[$instance->field_icon];
            $link = $menuPointData[$instance->field_link];
            
            if($returnValue == "_module_backend")
            {
                
                if($link == '/vmoso_chat/overview/')
                {
                    
                    $link = '/vmoso_backend/chat/';
                    
                }
                else if($link == '/process_engine/log/')
                {
                    
                    $link = '/process_engine/overview/';
                    
                }
                                
                // Retrieve the icon from the module with this backend address
                $moduleInfo = PerisianModuleManager::getModuleInfoByProperty('backendAddress', $link);
                
            }
            else if($returnValue == "_module_frontend")
            {
                
                // Retrieve the icon from the module with this frontend address
                $moduleInfo = PerisianModuleManager::getModuleInfoByProperty('frontendAddress', $link);

            }
            else
            {
                
                return $returnValue;
                
            }
            
            $returnValue = (isset($moduleInfo) && !is_null($moduleInfo) ? $moduleInfo['icon'] : 'fa-question-circle');
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Adds the list of available system modules to the specified menu poin.
     * 
     * @author Peter Hamm
     * @param Array $menuPointList Reference!
     * @return array
     */
    protected function addSystemModulesToList(&$menuPointList)
    {
        
        $this->loadModuleMenuPoints();
        
        $menuPointList = array_merge($menuPointList, $this->modulePointList);
        
        return $menuPointList;
        
    }
    
    /**
     * Loads the available module menu points
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function loadModuleMenuPoints()
    {
        
        if(isset($this->modulePointList))
        {
            
            return $this->modulePointList;
            
        }
        
        $this->modulePointList = array();
                
        $modules = PerisianModuleManager::getModuleList();
        
        for($i = 0; $i < count($modules); ++$i)
        {
            
            $link = $this->getFormattedLink('/module/' . $modules[$i]['identifier'] . '/');
            
            $newPoint = array(
                
                'id' => 'module_' . $modules[$i]['identifier'],
                'title' => $modules[$i]['name'],
                'link' => $link['link'],
                'target' => $link['target'],
                'icon' => $modules[$i]['icon']
                
            );
            
            if(!$modules[$i]['isBlocked'])
            {
                
                // This module got blocked from showing up in the system.
                
                array_push($this->modulePointList, $newPoint);
                
            }
            
        }
        
    }
    
    /**
     * Retrieves a formatted link.
     * 
     * @author Peter Hamm
     * @param String $link
     * @return Array
     */
    protected function getFormattedLink($link)
    {
        
        $returnValue = array(
            
            "link" => $link,
            "target" => "_self"
            
        );
        
        if(strpos($link, ":") > -1)
        {
            
            $returnValue["target"] = "_blank";
            
        }
        else
        {
            
            $serverAddress = PerisianFrameworkToolbox::getServerAddress(PerisianFrameworkToolbox::getConfig('environment/admin_area'));
            
            if(strlen($serverAddress) > 0 && substr($serverAddress, -1) == '/' && substr($link, 0, 1) == '/')
            {
                
                $serverAddress = substr($serverAddress, 0, -1);
                
            }
            
            $returnValue["link"] = $serverAddress . $link;
            
        }
        
        return $returnValue;
        
    }

    /**
     * Sets the active menu point to the specified menu point ID.
     * You may retrieve that information from the $activeMenuPoint member.
     *
     * @author Peter Hamm
     * @global PerisianUser $user The global user object, to check permissions
     * @param int $menuPointId
     * @return void
     */
    public function setActiveMenuPoint($menuPointId)
    {

        global $user;

        $menuPoint = $this->getMenuPoint($menuPointId);

        // Check the user's permissions

        if(!$this->checkPermission($menuPoint[$this->field_user_settings_required], $user))
        {
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));
        }

        $this->activeMenuPoint = $menuPoint;
        
    }
    
    /**
     * Sets the active menu point to the 
     * specified module identifier
     * 
     * @author Peter Hamm
     * @param String $moduleIdentifier
     */
    public function setActiveMenuPointForModule($moduleIdentifier)
    {
        
        $moduleIdentifier = 'module_' . $moduleIdentifier;
        
        $this->loadModuleMenuPoints();
        
        for($i = 0; $i < count($this->modulePointList); ++$i)
        {
                        
            if($this->modulePointList[$i]['id'] == $moduleIdentifier)
            {
                
                $this->activeMenuPoint = $this->modulePointList[$i];
                $this->activeMenuPoint[$this->field_pk] = $this->modulePointList[$i]['id'];
                $this->activeMenuPoint[$this->field_parent_id] = $this->pointIdModules;
                                
            }
            
        }
        
    }
    
    /**
     * Returns the currently set menu point.
     *
     * @author Peter Hamm
     * @return Array
     */
    public function getActiveMenuPoint()
    {

        return $this->activeMenuPoint;

    }

    /**
     * Returns the ID of the currently set menu point.
     *
     * @author Peter Hamm
     * @return int The ID
     */
    public function getActiveMenuPointId()
    {

        return $this->activeMenuPoint[$this->field_pk];

    }
    
    /**
     * Checks frontend user permissions.
     * 
     * @author Peter Hamm
     * @param Array $menuEntry
     * @param CalsyUserFrontend $userFrontendObject
     * @return boolean
     */
    protected function checkPermissionForUserFrontend($menuEntry, $userFrontendObject = null)
    {
        
        $dummy = static::getInstance();
        
        if(is_object($userFrontendObject))
        {
            
            if($menuEntry[$dummy->field_link] == '/quick_log/overview/')
            {
                
                if(!CalsyQuickLog::userFrontendHasQuickLogRights($userFrontendObject->{$userFrontendObject->field_pk}))
                {
                    
                    return false;
                    
                }
                
            }

        }

        return true;
        
    }

    /**
     * Takes a CSV string of checkbox setting IDs that must be checked
     * and compares them to the settings within a user object.
     *
     * @author Peter Hamm
     * @param String $permissionString CSV string of checkbox setting IDs
     * @param PerisianUser $userObject The user object to compare to
     * @return boolean Permissiom granted or not?
     */
    protected function checkPermission($permissionString, $userObject = null)
    {

        $permissions = explode(',', $permissionString);

        if(is_object($userObject))
        {

            for($j = 0, $n = count($permissions); $j < $n; ++$j)
            {
                
                if($permissions[$j] > 0 && $userObject->getSettingObject()->getSetting(0, $permissions[$j]) != 1)
                {
                    
                    return false;
                    
                }

            }

        }

        return true;

    }
    
    /**
     * Takes a CSV string of checkbox setting IDs that must be checked
     * and compares them to the settings of the system.
     *
     * @author Peter Hamm
     * @param String $permissionString CSV string of checkbox setting IDs
     * @return boolean Permissiom granted or not?
     */
    protected function checkPermissionForSystem($permissionString)
    {

        $permissions = explode(',', $permissionString);

        for($j = 0, $n = count($permissions); $j < $n; ++$j)
        {
            
            if(strlen($permissions[$j]) == 0)
            {
                
                continue;
                
            }
            
            $permission = new PerisianSystemSetting($permissions[$j]);

            if($permission->getValue() != 1)
            {
                
                return false;
                
            }

        }

        return true;

    }
    
}
