<?php

require 'framework/abstract/PerisianObject.class.php';

/**
 * Model class for all database-related objects.
 *
 * Contains basic methods for saving, selecting and so on.
 *
 * @author Peter Hamm
 * @date 2010-10-24
 * @date 2017-05-24 
 */
abstract class PerisianDatabaseModel extends PerisianObject
{
    
    const DB_VALUE_NULL = "-=N-U-L-L=-";

    // Always set these variables in extended methods!
    public $table = '';
    public $field_pk = '';

    // Optional foreign settings (left join)
    public $field_fk = '';
    public $foreign_table = '';
    public $foreign_field_pk = '';
    
    // Optional foreign settings (right join)
    public $field_fk_secondary = '';
    public $foreign_secondary_table = '';
    public $foreign_secondary_field_pk = '';

    // Which fields to search by default
    protected $searchFields = Array();

    // The database connection
    private $db = null;

    // Set this to false if you should not need the automatic
    // creation of member variables depending on specified 'field_x' variables.
    protected $create_members = true;

    /**
     * Handles database connection of the specified type.
     * You may specify an individual instance name.
     *
     * Always call this constructor if you overwrite it!
     *
     * @author Peter Hamm
     * @param String $databaseType Optional: Type of the database, standard: Mysql
     * @param String $instanceName Optional: Name of a new or existing database instance, standard: main
     * @return void
     */
    public function __construct($databaseType = 'Mysql', $instanceName = 'main')
    {

        PerisianFrameworkToolbox::security($databaseType);
        PerisianFrameworkToolbox::security($instanceName);

        $databaseClassName = 'Perisian' . $databaseType;

        if(!class_exists($databaseClassName))
        {
            
            throw new PerisianException('The selected database type is not included within this distribution (Class \'' . get_class($this) . '\', type \'' . $databaseType . '\').');
            
        }

        $temporaryDatabaseObject = new $databaseClassName($instanceName);
        $this->db = $temporaryDatabaseObject->getInstance($instanceName);

        $fields = get_object_vars($this);

        if($this->create_members)
        {
            
            foreach($fields as $key => $field)
            {

                if(strpos($key, 'field_') !== false)
                {

                    if(strlen($field) > 0)
                    {
                                                
                        $this->{$field} = null;
                        
                    }
                    
                }

            }
            
        }

        return;

    }
    
    /**
     * Retrieves the specified amount of last queries made through the database object.
     * 
     * @author Peter Hamm
     * @param int $limit Optional, default: 5
     * @return Array
     */
    public function getLastQueries($limit = 5)
    {
        
        $log = $this->db->getQueryLog();
        
        $logSize = count($log);
        
        $limit = $limit > $logSize ? $logSize : $limit;
        
        return array_splice($log, $logSize - $limit, $limit);
        
    }

    /**
     * Catch calls to unknown methods
     *
     * @author Peter Hamm
     * @param String $functionName
     * @param Array $arguments
     * @return void
     */
    public function __call($functionName, $arguments)
    {

        throw new PerisianException(PerisianLanguageVariable::getVariable(10112) . $functionName . '()');

    }

    /**
     * Returns the count of entries of a query, 
     * grouped by month
     *
     * @author Peter Hamm
     * @param String $where The query
     * @param String $field The field the results should be grouped and counted by, based on months. Has to be an SQL timestamp field
     * @return array
     */
    public function getCountForGroupedFieldValuesMonthly($where, $field)
    {
        
        $this->escapeSql($field);

        $query = 'SELECT EXTRACT(YEAR_MONTH FROM "' . $field . '") as year_month_value, COUNT("' . $field . '") as entry_count FROM ' . $this->table;
        
        if(!empty($where))
        {
            
            $query .= ' WHERE ' . $where;
            
        }
        
        $query .= " GROUP BY year_month_value";
        
        $result = $this->db->select($query);
        
        $returnValue = array();
        
        for($i = 0; $i < count($result); ++$i)
        {
            
            $currentResult = array (
                
                "year" => substr($result[$i]["year_month_value"], 0, 4),
                "month" => substr($result[$i]["year_month_value"], 4, 2),
                "count" => $result[$i]["entry_count"]
            
            );
            
            $returnValue[] = array($field => $currentResult);
            
        }
        
        return $returnValue;

    }

    /**
     * Returns the count of entries of a query
     *
     * @author Peter Hamm
     * @param String $where The query
     * @param String $field The field the results should be grouped and counted by
     * @return array
     */
    public function getCountForGroupedFieldValues($where, $field)
    {
        
        $this->escapeSql($field);

        $query = 'SELECT "' . $field . '", COUNT("' . $field . '") as entry_count FROM ' . $this->table;
        
        if(!empty($where))
        {
            
            $query .= ' WHERE ' . $where;
            
        }
        
        $query .= ' GROUP BY "' . $field . '"';
        
        $result = $this->db->select($query);
        
        $returnValue = array();
        
        for($i = 0; $i < count($result); ++$i)
        {
            
            $returnValue[$result[$i][$field]] = $result[$i]["entry_count"];
            
        }

        return $returnValue;

    }
    
    /**
     * Retrieves the left join query string if appropriate.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getLeftJoinString()
    {
        
        $leftJoin = false;
        $fields = $this->getFieldVars();
        
        $query = "";
        
        // Optional left join
        if(strlen($this->field_fk) > 1 && strlen($this->foreign_table) > 1 && strlen($this->foreign_field_pk) > 1)
        {
            
            $selectForeignFields = Array();

            foreach($fields as $key => $field)
            {

                if(strpos($key, 'foreign_field_') !== false)
                {

                    if(strlen($field) > 1)
                    {

                        $selectForeignFields[] = $field;

                    }

                }

            }

            $foreignFieldString = implode(', ', $selectForeignFields);

            if(strlen($foreignFieldString) > 1)
            {

                $query .= ', ' . $foreignFieldString;

            }

            $leftJoin = true;

        }

        if($leftJoin)
        {

            $query .= ' LEFT JOIN ' . $this->foreign_table . ' ON ' . $this->field_fk . ' = ' . $this->foreign_field_pk;

        }
        
        return $query;
        
    }
    
    /**
     * Retrieves the secondary left join query string if appropriate.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected function getSecondaryLeftJoinString()
    {
        
        $join = false;
        $fields = $this->getFieldVars();
        
        $query = "";
        
        // Optional secondary
        if(strlen($this->field_fk_secondary) > 1 && strlen($this->foreign_secondary_table) > 1 && strlen($this->foreign_secondary_field_pk) > 1)
        {
            
            $selectForeignFields = Array();

            foreach($fields as $key => $field)
            {

                if(strpos($key, 'foreign_secondary_field_') !== false)
                {

                    if(strlen($field) > 1)
                    {

                        $selectForeignFields[] = $field;

                    }

                }

            }

            $foreignFieldString = implode(', ', $selectForeignFields);

            if(strlen($foreignFieldString) > 1)
            {

                $query .= ', ' . $foreignFieldString;

            }

            $join = true;

        }

        if($join)
        {
            
            $query .= ' LEFT JOIN ' . $this->foreign_secondary_table . ' ON ' . $this->field_fk_secondary . ' = ' . $this->foreign_secondary_field_pk;
            
        }
        
        return $query;
        
    }
    
    /**
     * Retrieves the connection to the database.
     * 
     * @author Peter Hamm
     * @return PerisianDatabaseModel
     */
    protected function getDatabaseConnection()
    {
        
        return $this->db;
        
    }
    
    /**
     * Returns the count of entries of a query
     *
     * @author Peter Hamm
     * @param String $where The query
     * @param String $fieldOptions Optional, use this if you want a distinct result for example. Default: '*'
     * @return integer
     */
    public function getCount($where, $fieldOptions = '*')
    {

        $query = "SELECT COUNT({$fieldOptions}) FROM {$this->table}";
        $query .= $this->getLeftJoinString();
        $query .= $this->getSecondaryLeftJoinString();
        
        if(!empty($where))
        {
            
            $query .= ' WHERE ' . $where;
            
        }
        
        $result = $this->db->select($query);
            
        $count = isset($result[0]["COUNT({$fieldOptions})"]) ? (int)$result[0]["COUNT({$fieldOptions})"] : 0;
        
        return $count;

    }

    /**
     * Returns the distinct number of entries
     *
     * @author Peter Hamm
     * @param String $query The query
     * @param String $distinctField The name of the distinct field
     * @return integer
     */
    public function getCountDistinct($where, $distinctField)
    {

        if(empty($distinctField))
        {
            
            return 0;
            
        }

        $distinction = "DISTINCT {$distinctField}";
            
        return $this->getCount($where, $distinction);

    }
    
    /**
     * Retrieves the list of fields and their values, as an associative array
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getFieldValues()
    {
        
        $fields = $this->getFieldVars();
        
        $fieldsAndValues = array();
        
        for($i = 0; $i < count($fields); ++$i)
        {
            
            $fieldsAndValues[$fields[$i]] = $this->{$fields[$i]};
            
        }
        
        return $fieldsAndValues;
        
    }

    /**
     * Returns the class' field variables
     *
     * @author Peter Hamm
     * @param Array $fieldExcludes Optional: An array containing names of fields that shall not be retrieved, default: empty
     * @param Array $fieldList Optional, the opposite of $fieldExcludes - The result will contain only the specified fields, default: empty
     * @param bool $includeForeignFields Optional: Whether to include foreign fields or not, default: false
     * @return Array
     */
    protected function getFieldVars($fieldExcludes = Array(), $fieldList = Array(), $includeForeignFields = false)
    {

        PerisianFrameworkToolbox::security($fieldExcludes);
        PerisianFrameworkToolbox::security($fieldList);

        if(!is_array($fieldExcludes))
        {
            
            $fieldExcludes = Array($fieldExcludes);
            
        }

        if(!is_array($fieldList))
        {
            
            $fieldList = Array($fieldList);
            
        }

        $fields = Array();

        if(empty($fieldList))
        {
            
            $fields = get_object_vars($this);
            
        }
        else
        {

            if(is_array($fieldList))
            {

                foreach($fieldList as $fieldName)
                {

                    if(isset($this->{$fieldName}))
                    {
                        
                        $fields[$fieldName] = $this->{$fieldName};
                        
                    }

                }

            }
            
        }
        
        $returnFields = Array();
        
        if(count($fields) == 1 && isset($fields[0]))
        {
            
            $fields = $fields[0];
            
        }

        foreach($fields as $key => $field)
        {

            if(strpos($key, 'foreign_field') !== false && !$includeForeignFields)
            {
                
                continue;
                
            }
            
            if(strpos($key, 'enhanced_field') !== false)
            {
                
                continue;
                
            }

            if(strpos($key, 'field_') !== false && !in_array($key, $fieldExcludes))
            {

                if(strlen($field) > 0)
                {
                    
                    $returnFields[] = $field;
                    
                }
                
            }

        }

        return $returnFields;

    }

    /**
     * Retrieves data from the concrete class' specified $this->table.
     * 
     * @param String $where Optional: WHERE clause, without the keyword 'WHERE', default: empty
     * @param String $sortBy Optional: The key to sort by, default: primary key
     * @param String $order Optional: Sort results DESC or ASC, default: DESC
     * @param integer $start Optional: Selection offset, default: no offset (0)
     * @param integer $limit Optional: Limits the output to $limit entries, default: no limit (0)
     * @param Array $fieldExcludes Optional: An array containing names of fields that shall not be retrieved, default: empty
     * @param Array $fieldList Optional, the opposite of $fieldExludes - The result will contain only the specified fields, default: empty
     * @param Array $listParameters Optional, used for prepared statements
     * @return Array An associative array with the resulting entries
     */
    public function getData($where = '', $sortBy = 'pk', $order = 'DESC', $start = 0, $limit = 0, $fieldExcludes = Array(), $fieldList = Array(), $listParameters = Array())
    {

        $this->escapeSql($sortBy);
        $this->escapeSql($order);
        $this->escapeSql($start);
        $this->escapeSql($limit);
        
        PerisianFrameworkToolbox::security($fieldExcludes);
        PerisianFrameworkToolbox::security($fieldList);

        $limit = (int)$limit;
        $start = (int)$start;
                
        $fields = $this->getFieldVars($fieldExcludes, $fieldList, true);
        
        $fieldString = implode(', ', $fields);

        $query = 'SELECT ' . $fieldString;
        $query .= ' FROM ' . $this->table;

        $query .= $this->getLeftJoinString();
        $query .= $this->getSecondaryLeftJoinString();

        if(!empty($where))
        {
            
            $query .= ' WHERE ' . $where;
            
        }

        if($sortBy == 'pk' || $sortBy == '')
        {
            
            $sortBy = $this->field_pk;
            
        }

        if(strtoupper($order) != 'DESC')
        {
            
            $order = 'ASC';
            
        }
        else
        {
            
            $order = 'DESC';
            
        }

        if(!empty($sortBy) && !empty($order))
        {
                        
            $query .= ' ORDER BY ' . $sortBy . ' ' . $order;
            
        }

        if($limit != 0)
        {
                        
            $query .= ' LIMIT ' . $limit;
            
        }

        if($start != 0)
        {
                        
            $query .= ' OFFSET ' . $start;
            
        }

        if(!is_object($this->db))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10132), PerisianLanguageVariable::getVariable(10133));
            
        }
                
        $data = $this->db->select($query, $listParameters);
        
        return $data;

    }
    
    /**
     * Escapes critical SQL string for special charaters.
     * 
     * @author Peter Hamm
     * @param String $string (Reference!)
     * @return String
     */
    private function escapeSql(&$string)
    {
        
        PerisianFrameworkToolbox::security($string);
        
        $string = $this->db->escapeString($string);
        $string = str_replace('"', '', $string);
        $string = str_replace("'", '', $string);
        
        return $string;
        
    }

    /**
     * Gets a distinct field from the database
     *
     * @author Peter Hamm
     * @param String $fieldName The field name to retrieve
     * @param String $where Optional: WHERE statement, without the 'WHERE'
     * @param String $sortyBy Optional: The key to sort by, default: primary key
     * @param String $order Optional: Sort results DESC or ASC, default: DESC
     * @param integer $limit Optional: Limits the output to $limit entries, default: no limit (0)
     * @return Array The distinct field values
     */
    public function getDistinct($fieldName, $where = '', $sortBy = 'pk', $order = 'DESC', $start = 0, $limit = 0, $additionalFields = array())
    {
        
        $this->escapeSql($sortBy);
        $this->escapeSql($order);
        $this->escapeSql($start);
        $this->escapeSql($limit);
        
        PerisianFrameworkToolbox::security($fieldName);
        
        $query = "SELECT DISTINCT {$fieldName} ";
        
        if(count($additionalFields) > 0)
        {
            
            for($i = 0; $i < count($additionalFields); ++$i)
            {
                
                $query .= ", " . $additionalFields[$i] . " ";
                
            }
            
        }
        
        $query .= "FROM {$this->table}";

        if(!empty($where))
        {
            
            $query .= ' WHERE ' . $where;
            
        }
        
        if($sortBy == 'pk')
        {
            
            $sortBy = $this->field_pk;
            
        }

        if(strtoupper($order) != 'DESC')
        {
            
            $order = 'ASC';
            
        }
        else
        {
            
            $order = 'DESC';
            
        }

        if(!empty($sortBy) && !empty($order))
        {
            
            $query .= ' ORDER BY ' . $sortBy . ' ' . $order;
            
        }

        if($limit != 0)
        {
            
            $query .= ' LIMIT ' . $limit;
            
        }

        if($start != 0)
        {
            
            $query .= ' OFFSET ' . $start;
            
        }      

        $temporaryResult = $this->db->select($query);
        $result = Array();
        
        for($i = 0, $m = count($temporaryResult); $i < $m; ++$i)
        {
        
            $result[] = (count($additionalFields) > 0 ? $temporaryResult[$i] : $temporaryResult[$i][$fieldName]);
        
        }

        return $result;
        
    }
    
    /**
     * Gets fired just before the save fields get put together.
     * Save fields can be filtered here when overridden.
     * 
     * @param type $handleFields
     * @return Array 
     */
    protected function filterHandleFields($handleFields)
    {
        
        return $handleFields;
        
    }
    
    /**
     * Sets this object's values from the specified array.
     * 
     * @author Peter Hamm
     * @param array $array
     * @param bool $includePrimaryKey Optional, default: false. If set to true, this will include the primary key as well.
     * @param array $excludeFields Optional, default: array(). Fields that should not be fetched automatically from the post.
     * @return void
     */
    public function setObjectDataFromArray($array, $includePrimaryKey = false, $excludeFields = array())
    {
        
        $saveFields = $this->getSaveFields();
                                
        for($i = 0; $i < count($saveFields); ++$i)
        {
             
            if(in_array($saveFields[$i], $excludeFields) || (!$includePrimaryKey && $saveFields[$i] == $this->field_pk))
            {
                                           
                continue;
                
            }
                        
            $value = @$array[$saveFields[$i]];
            
            if(isset($value))
            {
                
                $this->{$saveFields[$i]} = $value;
                
            }
            
        }
        
    }
    
    /**
     * Sets this object's values from the server's post data.
     * 
     * @author Peter Hamm
     * @param bool $includePrimaryKey Optional, default: false. If set to true, this will include the primary key as well.
     * @param array $excludeFields Optional, default: array(). Fields that should not be fetched automatically from the post.
     * @return void
     */
    public function setObjectDataFromPost($includePrimaryKey = false, $excludeFields = array())
    {
        
        $saveFields = $this->getSaveFields();
        
        for($i = 0; $i < count($saveFields); ++$i)
        {
            
            if(in_array($saveFields[$i], $excludeFields) || (!$includePrimaryKey && $saveFields[$i] == $this->field_pk))
            {
                
                continue;
                
            }
            
            $valueFromPost = PerisianFrameworkToolbox::getRequest($saveFields[$i], "post");
            
            if(isset($valueFromPost))
            {

                $this->{$saveFields[$i]} = $valueFromPost;
                
            }
            
        }
        
    }
    
    /**
     * Retrieves the list of fields that can be saved with this object
     * 
     * @author Peter Hamm
     * @param array $saveFields Optional, specify own fields here if desired.
     * @return array
     */
    public function getSaveFields($saveFields = array())
    {
        
        if(!is_array($saveFields))
        {
            
            $swapArray[0] = $saveFields;
            
            unset($saveFields);
            
            $saveFields = &$swapArray;
            
        }

        PerisianFrameworkToolbox::security($saveFields);

        $fields = $this->getFieldVars();
        $handleFields = Array();

        if(count($saveFields) > 0)
        {
            
            for($i = 0, $m = count($fields); $i < $m; ++$i)
            {

                if(!in_array($fields[$i], $saveFields))
                {
                    
                    continue;
                    
                }

                $handleFields[] = $fields[$i];

            }

        }
        else
        {
            
            $handleFields = $fields;
            
        }
        
        if(count($handleFields) == 0)
        {
            return 0;
        }
        
        $handleFields = $this->filterHandleFields($handleFields);

        PerisianFrameworkToolbox::security($handleFields);
        
        return $handleFields;
        
    }

    /**
     * Saves this class' field variables to the specified table and database
     *
     * @author Peter Hamm
     * @param Array $saveFields Optional: An array containing names of fields that shall be saved, default: save all fields
     * @return integer The primary key ID of the saved database row - or 0 if the query failed
     */
    public function save($saveFields = Array())
    {
        
        if(!$this->onSaveStarted())
        {
            
            // onSaveStart failed somehow.
            throw new PerisianException(PerisianLanguageVariable::getVariable(10724));
            
        }

        $handleFields = $this->getSaveFields($saveFields);

        // Decide whether to INSERT or UPDATE the database table
        
        if(empty($this->{$this->field_pk}))
        {

            // Insert

            $handleFieldValues = Array();
            $unsetFieldIndex = -1;

            for($i = 0, $m = count($handleFields); $i < $m; ++$i)
            {
                
                if($handleFields[$i] == $this->field_pk)
                {
                    
                    $unsetFieldIndex = $i;
                    
                    // Skip the primary key, because it is empty anyways and 
                    // would cause trouble this way in MySql strict mode.
                
                    continue;
                    
                }
                
                if(isset($this->{$handleFields[$i]}))
                {
                    
                    $handleFieldValues[] = "'{$this->{$handleFields[$i]}}'";
                    
                }
                else
                {
                    
                    unset($handleFields[$i]);
                    
                }

            }
            
            if($unsetFieldIndex >= 0)
            {
                
                // Remove the empty primary key.
                unset($handleFields[$unsetFieldIndex]);
                
            }
            
            $valueString = "";
            
            foreach($handleFieldValues as $fieldValue)
            {
                
                if(strlen($valueString) > 0)
                {
                    
                    $valueString .= ',';
                    
                }
                
                if(is_null($fieldValue) || strcmp($fieldValue, "'" . static::DB_VALUE_NULL . "'") == 0)
                {
                    
                    $valueString .= 'NULL';
                    
                }
                else
                {
                    
                    $valueString .= $fieldValue;
                    
                }
                
            }
            
            // implode(', ', $handleFieldValues)

            $query = "INSERT INTO {$this->table} ";
            $query .= "(" . implode(', ', $handleFields) . ") ";
            $query .= "VALUES (" . $valueString . ")";
                                                
            $pk = $this->db->save($query);

        }
        else
        {

            // Update

            $handleFieldsWithValues = Array();

            for($i = 0, $m = count($handleFields); $i < $m; ++$i)
            {
                
                if(isset($handleFields[$i]) && isset($this->{$handleFields[$i]}))
                {
                    
                    $fieldValue = (is_null($this->{$handleFields[$i]}) || $this->{$handleFields[$i]} == static::DB_VALUE_NULL) ? "NULL" : "'" . $this->{$handleFields[$i]} . "'";

                    $handleFieldsWithValues[] = "{$handleFields[$i]} = " . $fieldValue;
                
                }

            }

            $query = "UPDATE {$this->table} SET ";
            $query .= implode(', ', $handleFieldsWithValues) . ' ';
            $query .= "WHERE {$this->field_pk} = {$this->{$this->field_pk}} LIMIT 1";
            
            $this->db->save($query);
            $pk = $this->{$this->field_pk};

        }
        
        $this->onSaveCompleted($pk);

        return $pk;

    }
    
    /**
     * Fired directly before the object is saved to the database.
     * Can be overriden.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
       
        return true;
        
    }
    
    /**
     * Fired directly after the object was saved to the database.
     * Can be overriden.
     * 
     * @author Peter Hamm
     * @param int $primaryKey Optional, default: ''
     * @return void
     */
    protected function onSaveCompleted($primaryKey = '')
    {
       
    }

    /**
     * Builds a search string with the specified searchterms.
     *
     * You may overwrite the field names to search through (which are by default
     * set in a member of the concrete class) with the second parameter, $fieldNames.
     *
     * @author Peter Hamm
     * @param mixed $searchTerm Array or String: The search term(s)
     * @param mixed $fieldNames Optional: Array or String: The name(s) of the fields you want to search through
     * @return String Your search query string
     */
    public function buildSearchString($searchTerms, $fieldNames = '')
    {

        if($fieldNames == '')
        {
            
            $fieldNames = $this->searchFields;
            
        }

        if(!is_array($fieldNames))
        {
            
            $fieldNames = Array($fieldNames);
            
        }
        
        if(!is_array($searchTerms))
        {
            
            $searchTerms = Array($searchTerms);
            
        }

        $stringRaw = Array();
        $st = '';
        
        for($i = 0, $m = count($fieldNames), $s = count($searchTerms); $i < $m; ++$i)
        {
            
            if($m == $s)
            {
                
                $st = $searchTerms[$i];
                
            }
            else if($s == 1)
            {
                
                $st = $searchTerms[0];
                
            }
            
            if(isset($this->{$fieldNames[$i]}))
            {
                
                $fn = $this->{$fieldNames[$i]};
                
            }
            else
            {
                
                $fn = $fieldNames[$i];
                
            }

            if(isset($fn))
            {
                
                if($fn == $this->field_pk)
                {

                    $stInt = (int)$st;

                    if($stInt > 0)
                    {
                        $stringRaw[] = "{$fn} = '{$stInt}'";
                    }

                }
                else
                {
                    
                    if($st != '' && $fn != '')
                    {
                        $stringRaw[] = "{$fn} LIKE '%{$st}%'";
                    }

                }
                
            }

            $st = '';
            $fn = '';

        }

        return implode(' OR ', $stringRaw);

    }
    
    /**
     * Retrieves data from the concrete class' specified $this->table.
     *
     * @author Peter Hamm
     * @param String $where WHERE clause, without the keyword 'WHERE'
     * @param String $sortyBy Optional: The key to sort by, default: primary key
     * @param String $order Optional: Sort results DESC or ASC, default: DESC
     * @param integer $limit Optional: Limits the deletion to $limit entries, default: no limit (0)
     * @return void
     */
    public function delete($where, $sortBy = 'pk', $order = 'DESC', $start = 0, $limit = 0)
    {
        
        $this->escapeSql($sortBy);
        $this->escapeSql($order);

        $limit = (int)$limit;
        $start = (int)$start;

        $query = 'DELETE FROM ' . $this->table . ' WHERE ' . $where;

        if($sortBy == 'pk')
        {
            
            $sortBy = $this->field_pk;
            
        }

        if(strtoupper($order) != 'DESC')
        {
            
            $order = 'ASC';
            
        }
        else
        {
            
            $order = 'DESC';
            
        }

        if(!empty($sortBy) && !empty($order))
        {
            
            $query .= ' ORDER BY ' . $sortBy . ' ' . $order;
            
        }

        if($limit != 0)
        {
            
            $query .= ' LIMIT ' . $limit;
            
        }

        if($start != 0)
        {
            
            $query .= ' OFFSET ' . $start;
            
        }

        return $this->db->delete($query);

    }

    /**
     * Sets this class' 'field_X' member variables with data.
     *
     * @author Peter Hamm
     * @param Array $data An associative array with field names and values
     * @return boolean Operation performed successfully or not?
     */
    protected function fillFields($data)
    {

        PerisianFrameworkToolbox::security($data);

        if(!is_array($data))
        {
            
            return false;
            
        }

        foreach($data as $key => $value)
        {
            
            if(strpos($key, 'field_') == 0)
            {
                                
                $this->{$key} = PerisianFrameworkToolbox::securityRevert($value);
                
            }
            
        }

        return true;

    }

}
