<?php

/**
 * Interface for Database models like PerisianMysql
 *
 * @author Peter Hamm
 * @date 2010-10-24
 */
interface PerisianDatabaseInterface
{

    static public function getInstance($instanceName);
    public function __construct($instanceName);
    public function save($query);
    public function select($query, $parameters = null);
    public function delete($query);
    public function escapeString($string);
    
}
