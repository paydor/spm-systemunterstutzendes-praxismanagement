<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'PerisianDatabaseMigration.class.php';

/**
 * Handles database migration
 *
 * @author Peter Hamm
 * @date 2016-11-23
 */
class PerisianDatabaseMigrationController extends PerisianController
{
    
    /**
     * Provides data to display the current status of the system.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $data = $this->getParameter('data');
        $isFrontend = $this->getParameter('is_frontend');
        
        if(@$data->getFlag() == PerisianDatabaseMigrationException::FLAG_UPDATE_REQUIRED)
        {

            if($isFrontend)
            {

                $page = 'database_migration/hint_frontend';

            }
            else
            {

                $page = 'database_migration/hint';

            }

        }
        
        //
        
        $renderer = array(
            
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array();
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Performs a database migration to the latest version.
     * Before that, a dump of the current database is stores to database/dump.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionMigrate()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        $migrationUser = $this->getParameter('u');
        $migrationPassword = $this->getParameter('p');

        $result = array(

            'success' => 'true',
            'message' => PerisianLanguageVariable::getVariable('p5808e8dbb0741')

        );

        try
        {

            $migrationUser = CalsyUserBackend::getUserByCredentials($migrationUser, $migrationPassword);

            if(is_null($migrationUser))
            {

                // Invalid user data
                throw new PerisianException(PerisianLanguageVariable::getVariable('p5808ec44add6d'));

            }
            else if(!$migrationUser->isAdmin())
            {

                // That user is not an administrator
                throw new PerisianException(PerisianLanguageVariable::getVariable('p5808ec2d6d736'));

            }

            // Proceed with the migration...
            {

                PerisianDatabaseMigration::handleDatabaseMigration();

            }

        }
        catch(PerisianException $e)
        {

            $result['success'] = false;
            $result['message'] = $e->getMessage();

            if(PerisianSystemConfiguration::isDebugMode())
            {

                $result['debugInfo'] = $e->getDebugInfo();

            }

        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method / => 'The database has been updated'
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p5808ca8e088ef'));
            
            $exception = new PerisianException($message, '', PerisianException::CODE_ERROR_MIGRATION);
            
            throw $exception;
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}