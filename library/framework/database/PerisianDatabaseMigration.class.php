<?php

/**
 * This class has the ability to migrate an out-of-date database to the latest version.
 *
 * @author Peter Hamm
 * @date 2016-10-20
 */
class PerisianDatabaseMigration
{
    
    private static $folderDump = 'database/dump/';
    private static $folderMigration = 'database/migration/';
    
    /**
     * Checks if the database is up to date or needs to be patched.
     * 
     * @author Peter Hamm
     * @param $targetVersion Optional, specify a database revision to compare with. Leave it empty to compare it to the latest database revision. Default: Empty
     * @return bool
     */
    public static function isDatabaseUpToDate($targetVersion = '')
    {
        
        PerisianFrameworkToolbox::security($targetVersion);
                
        $currentRevision = static::getCurrentDatabaseRevision();
        $requiredRevision = strlen($targetVersion) > 0 ? $targetVersion : static::getLatestDatabaseRevision();
        
        if(abs((int)$currentRevision) < abs((int)$requiredRevision))
        {
            
            return false;
            
        }
        
        return true;
        
    }
    
    /**
     * Executes the database migration to the version specified (Or the version specified).
     * 
     * @author Peter Hamm
     * @param String $targetVersion Optional, specify a database revision to update to. Must be higher than the current database revision. Leave it empty to update to the latest database revision. Default: Empty
     * @return boolean
     */
    public static function handleDatabaseMigration($targetVersion = '')
    {
        
        PerisianFrameworkToolbox::security($targetVersion);
        
        if(PerisianDatabaseMigration::isDatabaseUpToDate($targetVersion))
        {
            
            // There is no need to update.
            
            return true;
            
        }
        
        static::dumpDatabase();
        
        return static::migrateDatabase($targetVersion);
        
    }
    
    /**
     * Retrieves the folder used for database dumps.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function getFolderDump()
    {
        
        $exportFolder = PerisianFrameworkToolbox::getConfig('basic/project/folder') . static::$folderDump;
        
        if(!file_exists($exportFolder))
        {
            
            @mkdir($exportFolder, 0777, true);
            
        }
        
        if(!file_exists($exportFolder))
        {
            
            // Missing rights
            
            $debugInfo = "Could not retrieve the dump folder.";
            
            $exception = new PerisianException(PerisianLanguageVariable::getVariable(10725), $debugInfo);
            
            throw $exception;
            
        }
        
        return $exportFolder;
        
    }
    
    /**
     * Dumps the whole database to the database dump folder.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    protected static function dumpDatabase()
    {
        
        $databaseInstanceName = 'main';
        $databaseConnection = PerisianMysql::getInstance($databaseInstanceName);
        $isExportPossible = PerisianMysql::isExportPossible();
        
        $exportFolder = static::getFolderDump();
        
        if($isExportPossible)
        {
            
            $filename = date("Y-m-d-H-i-s") . "-dump-complete.sql";
            
            $dumpData = $databaseConnection->dump($databaseInstanceName);
            @file_put_contents($exportFolder . $filename, $dumpData);
                        
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Migrates the current database to either the specified $targetVersion if set, 
     * or the currently default required version.
     * 
     * @author Peter Hamm
     * @param String $targetVersion Optional, specify a database revision to update to. Must be higher than the current database revision. Leave it empty to update to the latest database revision. Default: Empty
     * @return bool
     * @throws PerisianException
     */
    private static function migrateDatabase($targetVersion = '') 
    {
        
        PerisianFrameworkToolbox::security($targetVersion);
        
        $currentRevision = static::getCurrentDatabaseRevision();
        $requiredRevision = strlen($targetVersion) > 0 ? $targetVersion : static::getLatestDatabaseRevision();
        
        $returnValue = false;
        
        $queryList = static::getMigrationQueriesBetween($currentRevision, $requiredRevision);
        
        foreach($queryList as $revision => $query)
        {
            
            static::executeRevisionMigration($revision, $query);
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Tries to execute the specified query for the specified revision number.
     * 
     * @param String $revisionNumber
     * @param String $revisionQuery
     * @throws PerisianException
     */
    private static function executeRevisionMigration($revisionNumber, $revisionQuery)
    {
                
        $databaseConnection = PerisianMysql::getInstance('main');
        
        try
        {
            
            $ignoredErrorNumbers = array(
                
                1146 /* 'Table % doesn't exist */
                
            );
            
            $revisionQuery = $databaseConnection->stripComments($revisionQuery);
                    
            $databaseConnection->executeMultiQuery($revisionQuery, $ignoredErrorNumbers);
            
            // Everything went well, update the current revision in the database.
            PerisianSystemSetting::setSettingValue('database_revision', $revisionNumber);
            
        }
        catch(PerisianDatabaseException $e)
        {
            
            $errorMessage = str_replace('%1', $revisionNumber, PerisianLanguageVariable::getVariable('p580a2d08f1ff7'));
            
            $errorDetails = $e->getSqlError() . ' (error #' . $e->getSqlErrorNumber() . ', query #' . $e->getSqlErrorLine() . ')';
            $errorDetails .= ":\n" . $e->getSqlErrorQuery();
                        
            throw new PerisianException($errorMessage, $errorDetails);
            
        }
        
    }
        
    /**
     * Retrieves all of the SQL for migration between the two specified revisions.
     * The retrieved queries will start from $startRevision + 1 and end at $targetRevision.
     * 
     * @author Peter Hamm
     * @param String $startRevision
     * @param String $targetRevision
     * @return Array An associative array with the SQL data contained.
     * @throws PerisianException
     */
    private static function getMigrationQueriesBetween($startRevision, $targetRevision)
    {
        
        $startIndex = abs((int)$startRevision) + 1;
        $targetIndex = abs((int)$targetRevision);
        
        $returnValue = array();
        
        for($i = $startIndex; $i <= $targetIndex; ++$i)
        {
            
            $formattedIndex = static::formatRevisionString($i);
            
            $revisionFolder = static::getFolderPathMigration() . $formattedIndex . '/';
            $revisionFile = $revisionFolder . 'migration.sql';
            
            if(!file_exists($revisionFolder) || !file_exists($revisionFile))
            {
                
                $errorMessage = str_replace('%1', $formattedIndex, PerisianLanguageVariable::getVariable('p580a25c5d239e'));
                
                throw new PerisianException($errorMessage);
                
            }
            
            $returnValue[$formattedIndex] = file_get_contents($revisionFile);
                        
        }
        
        return $returnValue;
        
    }
    
    /**
     * Formats a revision number to the default revision format.
     * 
     * @author Peter Hamm
     * @param Strig $revision
     * @return String
     */
    private static function formatRevisionString($revision)
    {
        
        $formattedRevision = $revision;
        
        while(strlen($formattedRevision) < 5)
        {
            
            $formattedRevision = "0" . $formattedRevision;
            
        }
        
        return $formattedRevision;
        
    }
    
    /**
     * Retrieves the currently installed database revision of the system.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getCurrentDatabaseRevision()
    {
        
        global $settings;
        
        return static::formatRevisionString($settings->getSettingValue('database_revision'));
        
    }
    
    /**
     * Retrieves the version of the database that is required to run the system and specified as the latest version in the configuration file.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getLatestDatabaseRevision()
    {
        
        $configuration = PerisianSystemConfiguration::getConfiguration('database_migration');
                
        $requiredRevision = @$configuration['migration']['required_version'];
        
        return static::formatRevisionString($requiredRevision);
        
    }
    
    /**
     * Retrieves the folder path for the migration files.
     * 
     * @author Peter Hamm
     * @return string
     */
    private static function getFolderPathMigration()
    {
        
        $path = PerisianFrameworkToolbox::getConfig('basic/project/folder') . static::$folderMigration;
        
        return $path;
        
    }
    
}