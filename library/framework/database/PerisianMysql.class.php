<?php

require_once 'framework/abstract/PerisianDatabase.class.php';
require_once 'PerisianDatabaseInterface.interface.php';

/**
 * Standard singleton class to open connections to mysql databases
 *
 * @author Peter Hamm
 * @date 2010-10-24
 * @date 2017-05-24 
 */
class PerisianMysql extends PerisianDatabase implements PerisianDatabaseInterface
{

    static private $instances = Array();

    // Connection settings
    private $connectionOpen = false;
    private $connection = null;
    private $connectionId = null;

    // Caching
    private $lastQuery = '';
    private $lastResult = null;
    
    // Is caching results enabled?
    protected $enableCaching = false;
    
    /**
     * Creates a new instance of this class with the specified name,
     * or returns the existing instance of it if it already exists.
     *
     * @author Peter Hamm
     * @param String $instanceName
     * @return Object
     */
    static public function getInstance($instanceName)
    {

        PerisianFrameworkToolbox::security($instanceName);

        if(!isset(self::$instances[$instanceName]))
        {
            
            static::$instances[$instanceName] = new static($instanceName);
            
        }

        return static::$instances[$instanceName];

    }

    /**
     * Checks if an instance with the specified name already exists or not
     *
     * @author Peter Hamm
     * @param String $instanceName
     * @return boolean
     */
    static public function instanceExists($instanceName)
    {

        if(isset(self::$instances[$instanceName]))
        {
            
            return true;
            
        }

        return false;

    }

    /**
     * Opens a new database connection with the specified name
     * 
     * @param String $instanceName
     * @return void
     */
    public function __construct($instanceName)
    {

        PerisianFrameworkToolbox::security($instanceName);
        
        if(PerisianMysql::instanceExists($instanceName))
        {
            
            // This is a workaround for PHP versions < 5.3
            return;
            
        }

        $connectionData = $this->getConfiguration($instanceName);

        if(empty($connectionData['host']) || empty($connectionData['database']) || empty($connectionData['user']))
        {

            $message = 'Database connection could not be established';
            
            if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
            {
                
                $message .= ': Configuration for \'' . $instanceName . '\' was not found.';
                
            }

            throw new PerisianException($message);
			
        }
        
        $connectionData['port'] = isset($connectionData['port']) ? $connectionData['port'] : null;
        
        if($connectionData['host'] == 'db' && @strlen($_SERVER['HTTP_HOST']) > 0)
        {
            
            $connectionData['host'] = $_SERVER['HTTP_HOST'];
            
        }
        
        $connection = new mysqli($connectionData['host'], $connectionData['user'], $connectionData['password'], $connectionData['database'], $connectionData['port']);


        if(mysqli_connect_error())
        {

            $message = 'Database connection could not be established';
            
            if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
            {
                
                $message .= ': Configuration for \'' . $instanceName . '\' may have errors.';
                
                $message .= ' "' . mysqli_connect_error() . '"';
                
            }

            throw new PerisianException($message);

        }
        else
        {

            $this->connection = &$connection;
            $this->connectionOpen = true;
            
            // Set the proper UTF charset.
            $this->connection->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
            
            // Set the proper time zone.
            {
                
                $timeZone = PerisianTimeZone::getOffsetString(null, true);
                $this->connection->query("SET @@session.time_zone = '" . $timeZone . "';");
                $this->connection->query("SET @@global.time_zone = '" . $timeZone . "';");
                $this->connection->query("SET GLOBAL time_zone = '" . $timeZone . "';");
                
            }
            
        }

        return;
        
    }

    /**
     * Closes all database connections made with this class
     *
     * @author Peter Hamm
     * @return void
     */
    static public function closeConnections()
    {

        foreach($this->instances as $instance)
        {
            
            $instance->closeConnection();
            
        }

        return;

    }

    /**
     * Closes the database connection with the specified name
     *
     * @author Peter Hamm
     * @param String $instanceName
     * @return void
     */
    static public function closeConnection($instanceName)
    {

        PerisianFrameworkToolbox::security($instanceName);

        $instance = self::getInstance($instanceName);

        if($instance->connectionOpen)
        {
            
            $instance->connection->close();
            $instance->connectionOpen = false;
            
        }

        return;
        
    }
    
    
    /**
     * Strips comments from the specified SQL string.
     * 
     * @param String $query
     * @return String
     */
    public function stripComments($query)
    {
        
        $query = preg_replace('!/\*.*?\*/!s', '', $query);
        $query = preg_replace('/\n\s*\n/', "\n", $query);

        return $query;
        
    }

    /**
     * Executes multiple queries at once. Use with caution!
     *
     * @author Peter Hamm
     * @param String $query The query string
     * @param Array $errorNumbersToIgnore Specify mysql error numbes that should be ignored. Optional. Default: Will skip mysql error numbers 0 ('No error') and 1065 ('Query was empty')
     * @return bool
     */
    public function executeMultiQuery($query, $errorNumbersToIgnore = array())
    {
        
        $defaultErrorsToIgnore = array('0', '1065');
        $errorNumbersToIgnore = array_merge_recursive($defaultErrorsToIgnore, $errorNumbersToIgnore);
        
        $queryNumber = 0;
        
        if($this->enableCaching && $query == $this->lastQuery)
        {
            
            $query = $this->lastQuery;
            $result = $this->lastResult;

        }
        else
        {
            
            $start = microtime(true);
            
            $result = false;
            
            $explodedQuery = explode(";\n", $query);
            
            for($i = 0; $i < count($explodedQuery); ++$i)
            {
                
                ++$queryNumber;
                
                $queryPart = $explodedQuery[$i] . ";";
                
                $result = $this->connection->query($queryPart, $this->connectionId) . "\n";
                
                $errorNumber = $this->connection->errno;
                                                
                if(!in_array($errorNumber, $errorNumbersToIgnore) || $result == false)
                {
                    
                    $result = false;
                    
                    break;
                    
                }
                
            }
            
            $end = microtime(true);

            $time = ($end - $start) * 1000;

            $this->lastQuery = $query;
            $this->lastResult = $result;
            
        }

        if(PerisianFrameworkToolbox::getConfig('basic/project/log_queries'))
        {

            $logData['query'] = $this->lastQuery;
            $logData['time'] = round($time, 6) . ' ms';
            
            $this->queryLog[] = $logData;
            
        }
        
        if($result === false)
        {
            
            $exception = new PerisianDatabaseException('Database multiquery failed.', $query);
            $exception->setSqlError($this->connection->error);
            $exception->setSqlErrorNumber($this->connection->errno);
            
            if($queryNumber > 0)
            {
                
                $exception->setSqlErrorLine($queryNumber);
                $exception->setSqlErrorQuery($explodedQuery[$queryNumber - 1]);
                
            }
            
            throw $exception;
            
        }

        return $result;
        
    }

    /**
     * The main method to query the database,
     * it has a simple integrated cache.
     *
     * @author Peter Hamm
     * @param String $query The query string
     */
    public function executeQuery($query)
    {
        
        if($this->enableCaching && $query == $this->lastQuery)
        {
            
            $query = $this->lastQuery;
            $result = $this->lastResult;

        }
        else
        {
            
            $start = microtime(true);
            $result = $this->connection->query($query);
            $end = microtime(true);

            $time = ($end - $start) * 1000;

            $this->lastQuery = $query;
            $this->lastResult = $result;
            
        }

        if(PerisianFrameworkToolbox::getConfig('basic/project/log_queries'))
        {

            $logData['query'] = $this->lastQuery;
            $logData['time'] = round($time, 6) . ' ms';
            
            $this->queryLog[] = $logData;
            
        }
        
        if($result === false)
        {
            
            $backTrace = debug_backtrace();
            
            $exception = new PerisianDatabaseException('Database query failed.', (PerisianSystemConfiguration::isDebugMode() ? $query : ''));
            $exception->setSqlError($this->connection->error);
            
            throw $exception;
            
        }

        return $result;
        
    }
    
    /**
     * Prepares an SQL query statement. 
     * 
     * @author Peter Hamm
     * @param String $query
     * @param Array $parameters
     * @return Object A prepared statement object
     * @throws PerisianException
     */
    protected function prepareQuery($query, $parameters)
    {
                
        $types = "";
        $values = Array();
        
        foreach($parameters as $parameter)
        {
            
            $types .= $parameter['type'];
            array_push($values, $parameter['value']);
            
        }
                
        $preparedStatement = $this->connection->prepare($query);
        
        if($preparedStatement === false)
        {
            
            throw new PerisianException($this->connection->error);
            
        }
        
        $preparedStatement->bind_param($types, $values);
        
        return $preparedStatement;
        
    }
    
    /**
     * Uses mysqli to escape the specified string for safe statements.
     * 
     * @author Peter Hamm
     * @param String $string
     * @return String
     */
    public function escapeString($string)
    {
        
        $string = $this->connection->real_escape_string($string);
        
        return $string;
        
    }

    /**
     * Returns a set of database entries
     *
     * @author Peter Hamm
     * @param String $query
     * @param Array $parameters Used for prepared statements, optional
     * @return Array
     */
    public function select($query, $parameters = null)
    {

        if(stripos($query, 'SELECT ') === false)
        {
            
            throw new PerisianDatabaseException('Wrong database query type.', $query);

        }

        $returnArray = Array();
        
        if(!is_null($parameters) && count($parameters) > 0)
        {
            
            $preparedStatement = $this->prepareQuery($query, $parameters);
            
            $result = $preparedStatement->execute();
            
            $preparedStatement->close();
            
        }
        else
        {
            
            $result = $this->connection->query($query);
            
        }
        
        if($result !== false) 
        {

            while($row = $result->fetch_assoc()) 
            {
                
                $returnArray[] = $row;
                
            }

            $result->free();
            
        }

        return $returnArray;

    }

    /**
     * Use this method to delete entries.
     *
     * @author Peter Hamm
     * @param String $query
     * @return void
     */
    public function delete($query)
    {

        if(stripos($query, 'DELETE FROM') === false)
        {
            
            throw new PerisianDatabaseException('Wrong database query type.', $query);
            
        }

        $this->executeQuery($query);

    }

    /**
     * Executes an INSERT or UPDATE query and returns the primary key of the saved entry
     *
     * @author Peter hamm
     * @param String $query An INSERT or UPDATE query
     * @return integer Primary key ID of the saved entry, if INSERT was used
     */
    public function save($query)
    {
        
        if(stripos($query, "UPDATE") === false && stripos($query, "INSERT") === false)
        {
            
            throw new PerisianDatabaseException('This is not a correctly formed save query.', $query);
            
        }

        $this->executeQuery($query);
        $saveResult = $this->select('SELECT LAST_INSERT_ID()');

        return $saveResult[0]['LAST_INSERT_ID()'];

    }
    
    /**
     * Retrieves the configuration for the specified $instanceName
     * 
     * @author Peter Hamm
     * @param String $instanceName
     * @return Array
     */
    protected function getConfiguration($instanceName)
    {
        
        PerisianFrameworkToolbox::security($instanceName);
        
        $data = PerisianFrameworkToolbox::getConfig('database/mysql/' . $instanceName);
        
        return $data;
        
    }
    
    /**
     * Retrieves some general SQL server information
     * 
     * @author Peter Hamm
     * @return array
     */
    public function getInfo()
    {
        
        $returnData = array();
        
        $returnData['version_calsy'] = PerisianSystemSetting::getSettingValue('database_revision');
        $returnData['version_mysql'] = $this->connection->server_info;
        $returnData['status'] = explode('  ', $this->connection->stat());
            
        return $returnData;
            
    }
    
    /**
     * Returns whether a database dump is possible on this system or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isExportPossible()
    {
        
        $returnValue = function_exists('exec') && !in_array('exec', array_map('trim', explode(', ', ini_get('disable_functions')))) && strtolower(ini_get('safe_mode')) != 1;

        return $returnValue;
        
    }
    
    /**
     * Dumps the whole database to a String.
     * 
     * @author Peter Hamm
     * @param String $instanceName 
     * @return String The complete dump
     */
    public function dump($instanceName)
    {
        
        PerisianFrameworkToolbox::security($instanceName);
        
        $connectionData = $this->getConfiguration($instanceName);
                
        $returnLines = null;
        
        $dumpCommand = "mysqldump -u {$connectionData['user']} --password={$connectionData['password']} {$connectionData['database']}";// | gzip --best";   
        
        exec($dumpCommand, $returnLines);
        
        $returnValue = implode("\n", $returnLines);
        
        return $returnValue;
        
    }

}