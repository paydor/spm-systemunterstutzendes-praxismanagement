<?php

require_once 'database/PerisianDatabaseModel.class.php';

/**
 * This class handles missing language variables and 
 * their possible import from a remote source.
 *
 * @author Peter Hamm
 * @date 2016-06-23
 */
class PerisianLanguageVariableMissing extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'language_variable_missing';
    public $field_pk = 'language_variable_missing_id';

    // Fields
    public $field_variable_identifier = 'language_variable_missing_identifier';
    
    // Instance
    static private $instance = null;

    /**
     * This may get called internally by self::getInstance or externally when
     * editing a specific language variable
     *
     * @param mixed $id Optional: If specified, loads the data of the entry. Default: 0
     * @author Peter Hamm
     * @return void
     */
    public function __construct($id = 0)
    {

        parent::__construct('MySql', 'main');

        if($id > 0)
        {
            
            PerisianFrameworkToolbox::security($id);
            
            $query = "{$this->field_pk} = '{$id}'";
            
            $result = $this->getData($query, 'pk', 'DESC', 0, 1);

            if(count($result) == 1)
            {
                
                foreach($result[0] as $key => $value)
                {
                    
                    $this->{$key} = $value;
                    
                }

            }

        }

    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static public function getInstance()
    {

        if(!isset(self::$instance))
        {
            
            self::$instance = new self;
            
        }

        return self::$instance;

    }
    
    /**
     * Retrieves the count of entries
     * 
     * @author Peter Hamm
     * @return int
     */
    public static function getEntryCount()
    {
        
        $instance = static::getInstance();
        
        return $instance->getCount('');
        
    }
    
    /**
     * Retrieves if an entry for the specified variable $identifier exists.
     * 
     * @author Peter Hamm
     * @param String $identifier
     * @return boolean
     */
    public static function entryExistsForIdentifier($variableIdentifier)
    {
                
        $instance = self::getInstance();
        
        PerisianFrameworkToolbox::security($variableIdentifier);
        
        $query = "{$instance->field_variable_identifier} = '{$variableIdentifier}'";
        
        $count = $instance->getCount($query);
        
        return $count > 0;
        
    }
    
    /**
     * Gets triggered whenever there is a missing language variable with a unique identifier
     * on the system. 
     * 
     * @author Peter Hamm
     * @param String $variableIdentifier
     * @return void
     */
    public static function reportMissingVariable($variableIdentifier)
    {
        
        PerisianFrameworkToolbox::security($variableIdentifier);
        
        if(static::entryExistsForIdentifier($variableIdentifier))
        {
            
            return;
            
        }
        
        $newEntry = new static();
        
        $newEntry->{$newEntry->field_variable_identifier} = $variableIdentifier;
        
        $newEntry->save();
        
    }
    
    /**
     * Returns a properly formatted server address
     * 
     * @author Peter Hamm
     * @param String $serverAddress
     * @return String
     */
    protected static function getFormattedServerAddress($serverAddress)
    {
        
        if(!preg_match("~^(?:f|ht)tps?://~i", $serverAddress)) 
        {
            
            $serverAddress = "https://" . $serverAddress;
            
        }
        
        if(substr($serverAddress, 0, -1) != '/')
        {
            
            $serverAddress .= '/';
            
        }
        
        return $serverAddress;
        
    }
    
    /**
     * Retrieves the current list of field identifiers of missing language variables.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected static function getIdentifierList()
    {
        
        $instance = static::getInstance();
        
        $data = $instance->getData();
        
        $returnData = array();
        
        for($i = 0; $i < count($data); ++$i)
        {
            
            array_push($returnData, $data[$i][$instance->field_variable_identifier]);
            
        }
        
        return $returnData;
        
    }
    
    /**
     * Generates a query for a web request.
     * 
     * @author Peter Hamm
     * @param Array $identifierList
     * @return string
     */
    protected static function buildRequestFromIdentifierList($identifierList)
    {
                
        $data = array('v' => $identifierList);
        
        $returnValue = http_build_query($data);
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the list of missing language variables from the specfied server.
     * 
     * @author Peter Hamm
     * @param String $serverAddress The server (including port) to retrieve the variables from.
     * @return array An associative array containing the values 'success' (bool), 'message' (String) and 'messageExtended' (String(.
     */
    public static function getMissingVariablesFromServer($serverAddress)
    {
        
        $result = array();
        
        $serverAddress = static::getFormattedServerAddress($serverAddress) . 'admin/ajax/language_variable_missing/';
        
        $options = array(
            
            'http' => array(
                
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => static::buildRequestFromIdentifierList(static::getIdentifierList())
        
            )
            
        );

        $context = stream_context_create($options);
        $result = @file_get_contents($serverAddress, false, $context);
        
        if($result === false)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p576c833fcf2b4'));
            
        }
        
        $result = json_decode($result, true);
        
        return $result;
        
    }
    
    /**
     * Imports the specified variables into the system.
     * 
     * @author Peter Hamm
     * @param Array $variables
     * @return array
     */
    public static function importMissingVariables($variables)
    {
        
        $importedVariables = array();
        $variablesToDelete = array();
        
        $languageIdentifiers = array();
                
        foreach($variables as $missingVariableIdentifier => $variableData)
        {
            
            if(PerisianLanguageVariable::variableExists($missingVariableIdentifier))
            {
                
                // This was already imported.
                
                array_push($variablesToDelete, $missingVariableIdentifier);
                
                continue;
                
            }
                        
            foreach($variableData as $languageCode => $variableContent)
            {
                
                if(!isset($languageIdentifiers[$languageCode]))
                {
                    
                    $languageIdentifiers[$languageCode] = PerisianLanguage::getLanguageIdentifier($languageCode);
                    
                }
                
                $languageId = $languageIdentifiers[$languageCode];
                
                {
                    
                    $languageVariable = new PerisianLanguageVariable();
                    
                    $languageVariable->{$languageVariable->field_variable_unique_sub_id} = PerisianFrameworkToolbox::security($missingVariableIdentifier);
                    $languageVariable->{$languageVariable->field_variable_language_id} = $languageId;
                    $languageVariable->{$languageVariable->field_variable_content} = PerisianFrameworkToolbox::security($variableContent);
                    
                    $languageVariable->save();
                    
                }
                
            }
            
            array_push($importedVariables, $missingVariableIdentifier);
            array_push($variablesToDelete, $missingVariableIdentifier);
            
        }
        
        if(count($variablesToDelete) > 0)
        {
            
            $instance = static::getInstance();
            
            $instance->deleteImportedMissingVariables($variablesToDelete);
            
        }
        
        return $importedVariables;
        
    }
    
    /**
     * Deletes the list of specified missing variables 
     * 
     * @author Peter Hamm
     * @param Array $missingVariableIdentifierList An array of variable identifiers
     * @return void
     */
    protected function deleteImportedMissingVariables($missingVariableIdentifierList)
    {
        
        PerisianFrameworkToolbox::security($missingVariableIdentifierList);
        
        $variableList = implode("','", $missingVariableIdentifierList);
        
        $deleteQuery = "{$this->field_variable_identifier} IN('" . $variableList . "')";
        
        $this->delete($deleteQuery);
        
    }
    
}