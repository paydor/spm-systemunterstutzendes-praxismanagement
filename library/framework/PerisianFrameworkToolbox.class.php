<?php

class PerisianFrameworkToolbox
{
    
    /**
     * Forward to SSL if this is a non-SSL request
     * 
     * @author Peter Hamm
     * @param $isFrontend Optional, default: false
     * @return void
     */
    public static function forceSSL($isFrontend = false)
    {

        if(PerisianSystemConfiguration::ENFORCE_SSL || (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off'))
        {

            $sslAddress = str_replace('http://', 'https://', static::getServerAddress(!$isFrontend, true));
            
            $oldPort = substr($sslAddress, strrpos($sslAddress, ":"));
            $requestedPage = "";
            
            if(strpos($oldPort, "/") !== false)
            {
                
                $requestedPage = substr($oldPort, strpos($oldPort, "/") + 1);
                $oldPort = substr($oldPort, 0, strpos($oldPort, "/"));
                
            }
            
            $newPort = ':443';
            
            if(strlen($oldPort) > 0)
            {
                                
                $newPort = substr($oldPort, 0, -3) . '443';
                
            }
            
            $sslAddress = str_replace($oldPort, $newPort, $sslAddress);
            
            $newLocation = "Location: " . $sslAddress;
            
            header($newLocation);

            exit;

        }

    }
    
    /**
     * Handles different settings and configurations depending on the server setup (e.g. if the server is in debug mode)
     * 
     * @author Peter Hamm
     * @global $config
     * @return void
     */
    public static function handleServerSetup()
    {
        
        global $config;
        
        if($config['basic']['project']['debug_mode'])
        {
            
            error_reporting(E_ALL ^ E_STRICT ^ E_WARNING ^ E_NOTICE ^ E_DEPRECATED);
            ini_set('display_errors', '1');
            
            if(PHP_MAJOR_VERSION >= 7) 
            {

                set_error_handler(function ($errno, $errstr) {

                   return strpos($errstr, 'Declaration of') === 0;

                }, E_ALL ^ E_STRICT ^ E_WARNING ^ E_NOTICE ^ E_DEPRECATED);
                
            }
            
        }
        else
        {
            
            error_reporting(E_NONE);
            ini_set('display_errors', '0');
            
        }
        
    }
    
    /**
     * Escapes all of the request-variables, like $_GET and so on.
     * Should be called on the very beginning of a page.
     *
     * Warning: HTML and PHP tags are not being deleted by default,
     * only htmlspecialchars is being used.
     *
     * @author Peter Hamm
     * @global Array $_GET
     * @global Array $_POST
     * @global Array $_REQUEST
     * @return void
     */
    public static function escapeRequests()
    {

        global $_GET, $_POST, $_REQUEST;

        static::security($_GET, true);
        static::security($_POST, true);
        static::security($_REQUEST, true);
        static::security($_SERVER, true);

    }

    /**
     * Retrieves a somewhot unique identifier from the browser.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getUserIdentifier()
    {

        return md5(@$_SERVER['HTTP_COOKIE'] . @$_SERVER['HTTP_USER_AGENT'] . @$_SERVER['REMOTE_ADDR']);

    }

    /**
     * Sets values to $_GET or $_POST.
     * 
     * @author Peter Hamm 
     * @param String $paramName
     * @param Strin $value
     * @param String $type
     * @return mixed The value of the requested $paramName in $type
     */
    public static function setRequest($paramName, $value, $type = '')
    {

        global $_GET, $_POST, $_REQUEST;
        
        $type = strtolower($type);
        
        if(strlen($type) == 0)
        {
            
            $_REQUEST[$paramName] = $value;
            $_POST[$paramName] = $value;
            $_GET[$paramName] = $value;
            
        }
        else if($type == 'get')
        {
            
            $_GET[$paramName] = $value;
            
        }
        else if($type == 'post')
        {
            
            $_POST[$paramName] = $value;
            
        }

    }

    /**
     * Retrieves values from $_GET or $_POST,
     * stripping all characters that could be used for an XSS attack.
     * 
     * @author Peter Hamm 
     * @param String $paramName
     * @param String $type
     * @return mixed The value of the requested $paramName in $type
     */
    public static function getRequest($paramName, $type = '')
    {

        $data = static::getRequestXss($paramName, $type);

        return $data;

    }

    /**
     * Retrieves values from $_GET or $_POST
     *
     * @author Peter Hamm
     * @param String $paramName The name of the desired parameter
     * @param String $type Optional: 'get' or 'post', standard: 'get'
     * @return mixed The value of the requested $paramName in $type
     */
    private static function getRequestInsecure($paramName, $type = '')
    {

        global $_GET, $_POST, $_REQUEST;

        $type = strtolower($type);

        static::security($paramName);
        static::security($type);

        if($type == 'get' && isset($_GET[$paramName]))
        {
            
            return $_GET[$paramName];
            
        }
        else if($type == 'post' && isset($_POST[$paramName]))
        {
            
            return $_POST[$paramName];
            
        }
        else if(isset($_REQUEST[$paramName]))
        {
            
            return $_REQUEST[$paramName];
            
        }

        return null;

    }

    /**
     * Retrieves values from $_SERVER
     *
     * @author Peter Hamm
     * @param String $paramName The name of the desired parameter
     * @return String The value of the requested $paramName
     */
    public static function getServerVar($paramName)
    {

        global $_SERVER;

        static::security($paramName);
        $paramName = strtoupper($paramName);

        if(isset($_SERVER[$paramName]))
        {
            return $_SERVER[$paramName];
        }

        return '';

    }
    
    /**
     * Retrieves the absolute address of the current page in the system.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getPageAddress()
    {
                
        $relevantPart = substr($_SERVER['REDIRECT_URL'], strpos($_SERVER['REDIRECT_URL'], '/admin/') + 1);
        
        $address = PerisianFrameworkToolbox::getServerAddress(false) . $relevantPart; // . substr($_SERVER['REDIRECT_URL'], 1);
        
        return $address;
        
    }

    /**
     * Returns the web address of this server, also considers if HTTPS
     * is set on or not.
     *
     * @author Peter Hamm
     * @param bool $includeBackendFolder Optional, default: true
     * @return String
     */
    public static function getServerAddress($includeBackendFolder = true)
    {
        
        $protocol = PerisianSystemConfiguration::isSecureConnection() ? 'https://' : 'http://';
        
        if(strlen(static::getConfig('basic/project/url')) > 0)
        {
            
            $address = static::getConfig('basic/project/url');

            if(substr($address, -2) == '//')
            {

                $address = substr($address, 0, -1);

            }
            
            if($protocol == 'https://')
            {
                
                $address = str_replace('http://', 'https://', $address);
                
            }

        }
        else
        {
            
            $address = $protocol . static::getServerVar('HTTP_HOST');

        }

        if($includeBackendFolder)
        {

            $address .= 'admin/';

        }

        return $address;

    }

    /**
     * Retrieves values from $_GET or $_POST,
     * stripping all characters that could be used for an XSS attack.
     * 
     * @author Peter Hamm 
     * @param String $paramName
     * @param String $type
     * @return mixed The value of the requested $paramName in $type
     */
    public static function getRequestXss($paramName, $type = '')
    {

        $data = static::getRequestInsecure($paramName, $type);

        if(!is_null($data))
        {
        
            $data = static::secureXss($data);
            
        }
        
        return $data;

    }

    /**
     * Formats the specified data so that it is no longer harmful considering XSS attacks.
     * 
     * @author Peter Hamm
     * @param mixed $data
     * @return mixed
     */
    public static function secureXss($data)
    {

        if(is_array($data) && count($data) > 0)
        {

            foreach($data as $key => $value)
            {

                $data[$key] = static::secureXss($value);

            }

        }
        else 
        {

            $data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');

        }

        return $data;

    }

    /**
     * This public static function escapes $param
     *
     * @author Peter Hamm
     * @param mixed $param The parameter you want to be escaped
     * @param bool $allowHtml Optional: Allow HTML tags, default: false
     * @return mixed
     */
    public static function security(&$param, $allowHtml = false)
    {

        if(is_array($param))
        {

            foreach($param as &$paramValue)
            {
              
                static::security($paramValue);
                
            }

        }
        else
        {

            $param = @addslashes($param);

            if(!$allowHtml)
            {
                $allowHtml = strip_tags($allowHtml);
            }

            $param = htmlspecialchars($param, ENT_NOQUOTES);

        }

        return $param;

    }

    /**
     * This public static function unescapes $param, it is the opposite public static function of static::security()
     *
     * @author Peter Hamm
     * @param mixed $param The parameter you want to be unescaped
     * @param bool $allowHtml Optional: Allow HTML tags, default: false
     * @return mixed
     */
    public static function securityRevert(&$param)
    {

        if(is_array($param))
        {

            foreach($param as &$paramValue)
            {
                static::securityRevert($paramValue);
            }

        }
        else
        {

            $param = htmlspecialchars_decode($param, ENT_NOQUOTES);
            $param = stripslashes($param);

        }

        return $param;

    }

    /**
     * Adds additional folders, like the library, to the include path
     *
     * @author Peter Hamm
     * @return String The old include_path on success or false on failure.
     */
    public static function updateIncludePath()
    {

        global $config;

        $additionalFolders = Array();

        if(isset($config['basic']['project']['folder']))
        {

            $includeFolder = realpath($config['basic']['project']['folder'] . 'library') . "/";

            if(strlen($includeFolder) == 1)
            {

                $currentDir = dirname($_SERVER["PHP_SELF"]);
                $includeFolder = realpath($currentDir . "/../../library/") . "\\";

            }

            if(file_exists($includeFolder) !== false)
            {

                $additionalFolders[] = $includeFolder;
                $additionalFolders[] = $includeFolder . "external/";

            }

        }

        $newIncludePath = get_include_path();

        for($i = 0, $m = count($additionalFolders); $i<$m; ++$i)
        {

            // Depending on the OS of the server, the seperator can be different.
            $seperator = strtolower(substr(PHP_OS, 0, 3)) === 'win' ? ";" : ":";

            $newIncludePath .= $seperator . $additionalFolders[$i];

        }

        return set_include_path($newIncludePath);

    }
    
    /**
     * Retrieves whether the current script is run in a command line or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isCommandLine()
    {
        
        if(php_sapi_name() == 'cli')
        {
            
            return true;
            
        }
        
        return false;
       
    }

    /**
     * This public static function returns the current SQL timestamp.
     * Optionally, you may pass a PHP timestamp as parameter, 
     * then its SQL timestamp value is returned.
     * 
     * @author Peter Hamm
     * @param int $timestamp Optional: A PHP timestamp
     * @return int An SQL timestamp
     */
    public static function sqlTimestamp($timestamp = 0)
    {

        if($timestamp == 0)
        {
            
            $timestamp = static::isCommandLine() ? mktime() : gmmktime();
            
        }
        
        $dateFunction = static::isCommandLine() ? 'date' : 'gmdate';

        return $dateFunction('YmdHis', $timestamp);

    }

    /**
     * Transforms an SQL timestamp to a PHP typical timestamp
     *
     * @author Peter Hamm
     * @param int $timestamp An SQL timestamp
     * @return int A PHP timestamp
     */
    public static function sqlTimestampToPhp($timestamp)
    {

        if(strlen($timestamp) != 19)
        {
            
            return $timestamp;
            
        }

        return gmmktime(substr($timestamp, 11, 2),
                      substr($timestamp, 14, 2),
                      substr($timestamp, 17, 2),
                      substr($timestamp, 5, 2),
                      substr($timestamp, 8, 2),
                      substr($timestamp, 0, 4));

    }

    /**
     * Disables the drawing of the page footer and header
     *
     * @author Peter Hamm
     * @global boolean $dontShowHeader
     * @global boolean $dontShowFooter
     * @return void
     */
    public static function blankPage()
    {

        global $dontShowHeader, $dontShowFooter;

        $dontShowHeader = true;
        $dontShowFooter = true;

    }

    /**
     * Enables the drawing of the page footer and header
     *
     * @author Peter Hamm
     * @global boolean $dontShowHeader
     * @global boolean $dontShowFooter
     * @return void
     */
    public static function noBlankPage()
    {

        global $dontShowHeader, $dontShowFooter;

        $dontShowHeader = false;
        $dontShowFooter = false;

    }

    /**
     * Returns the database field names of a class as an associative array,
     * useful for static classes.
     *
     * @author Peter Hamm
     * @param String $className The name of a class
     * @return Array Associative array
     */
    public static function getFields($className)
    {

        static::security($className);

        $obj = new $className();
        $members = get_object_vars($obj);
        $result = Array();

        foreach($members as $key => $member)
        {

            if(strpos($key, 'field_') === 0)
            {
                $result[$key] = $member;
            }

        }

        return $result;

    }

    /**
     * Returns an associative array with names of database fields as keys
     * and with empty content. The primary key field is set to 0 (zero).
     *
     * @author Peter Hamm
     * @date 2010-12-15
     * @param String $className The name of a class
     * @return Array Associative array
     */
    public static function getEmptyFields($className)
    {

        $fields = static::getFields($className);
        $fieldPrimary = $fields['field_pk'];
        $fields = array_flip($fields);

        if(count($fields) > 0)
        {

            foreach($fields as $key => &$value)
            {

                if($key != $fieldPrimary)
                {
                    $value = null;
                }

            }

            $fields[$fieldPrimary] = 0;

        }

        return $fields;

    }

    public static function flipCount($flipNames, &$fieldArray, $unsetField = false)
    {

        $flipArray = Array();

        if(!PerisianValidation::checkArray($flipNames) || !PerisianValidation::checkArray($fieldArray))
        {
            return $flipArray;
        }

        foreach($flipNames as $flipName)
        {

            if(!is_array($fieldArray[$flipName]))
            {
                $fieldArray[$flipName] = Array($fieldArray[$flipName]);
            }

            for($j = 0, $n = count($fieldArray[$flipName]); $j < $n; ++$j)
            {
                $flipArray[$j][$flipName] = $fieldArray[$flipName][$j];
            }

            if($unsetField)
            {
                unset($fieldArray[$flipName]);
            }

        }

        return $flipArray;

    }

    /**
     * Gets the currently set time format (for an user)
     *
     * @author Peter Hamm
     * @param PerisianUser $user Optional: A user object
     */
    public static function getTimeFormat($user = null)
    {

        $settingObj = new PerisianSystemSetting();
        $timeFormat = $settingObj->getSetting(static::getConfig('system/setting_id/default_format_time'));

        if(is_object($user) && $user->{$user->field_status} == 'active')
        {

            $timeFormatUser = @$user->getSettingObject()->getSetting(0, static::getConfig('system/setting_id/user_format_time'));

            if(strlen($timeFormatUser) > 0)
            {
                $timeFormat = $timeFormatUser;
            }

        }

        return static::security($timeFormat);

    }

    /**
     * Gets the currently set time format (for an user)
     *
     * @author Peter Hamm
     * @param PerisianUser $user Optional: A user object
     * @return String
     */
    public static function getDateFormat($user = null)
    {

        $settingObj = new PerisianSystemSetting();
        $dateFormat = $settingObj->getSetting(static::getConfig('system/setting_id/default_format_date'));

        if(is_object($user) && $user->{$user->field_status} == 'active')
        {

            $dateFormatUser = @$user->getSettingObject()->getSetting(0, static::getConfig('system/setting_id/user_format_date'));

            if(strlen($dateFormatUser) > 0)
            {
                $dateFormat = $dateFormatUser;
            }

        }

        return static::security($dateFormat);

    }

    /**
     * Converts a timestamp to the human readable format set in the system
     * or user's configuration.
     *
     * @author Peter Hamm
     * @global PerisianUser $user The global user object
     * @param int $timestamp Optional: A timestamp, default: current time
     * @param String $timeFormat Optiona: Default: The current user's default setting
     * @return String
     */
    public static function timestampToTime($timestamp = 0, $timeFormat = null)
    {

        global $user;

        $timestamp = static::sqlTimestampToPhp($timestamp);

        if($timestamp == 0)
        {
            
            $timestamp = time();
            
        }
        
        if($timeFormat == null)
        {
            
            $timeFormat = static::getTimeFormat($user);
            
        }

        return $timestamp == 943916400 ? "-" : gmdate($timeFormat, $timestamp);

    }

    /**
     * Converts a timestamp to the human readable format set in the system
     * or user's configuration.
     *
     * @author Peter Hamm
     * @global PerisianUser $user The global user object
     * @param int $timestamp Optional: A timestamp, default: current time
     * @return String
     */
    public static function timestampToDate($timestamp = 0)
    {

        global $user;

        $timestampTransformed = static::sqlTimestampToPhp($timestamp);

        if($timestampTransformed == 0)
        {
            $timestampTransformed = time();
        }

        return ($timestamp === "0000-00-00 00:00:00" || $timestampTransformed == 943916400 || $timestampTransformed == 943938000) ? "-" : date(static::getDateFormat($user), $timestampTransformed);

    }

    /**
     * Converts a timestamp to the human readable format set in the system
     * or user's configuration.
     *
     * @author Peter Hamm
     * @global PerisianUser $user The global user object
     * @param int $timestamp Optional: A timestamp, default: current time
     * @return String
     */
    public static function timestampToDateTime($timestamp = 0)
    {

        $time = static::timestampToTime($timestamp);
        $date = static::timestampToDate($timestamp);

        return $date == "-" ? "-" : ($date . ', ' . $time);

    }

    /**
     * This public static functions returns values of the configuration array.
     *
     * Example usage:
     * static::getConfig('database/mysql/user') will return
     * the value of $config['database']['mysql']['user']
     *
     * @author Peter Hamm
     * @global $config
     * @param String $path Pseudo-path
     * @return mixed Your desired value
     */
    public static function getConfig($path)
    {

        global $config;

        static::security($path);
        $pathX = explode('/', $path);

        $value = $config;

        for($i=0, $m=count($pathX); $i<$m; ++$i)
        {
            if(isset($value[$pathX[$i]]))
            {
                $value = $value[$pathX[$i]];
            }
            else
            {
                throw new PerisianException("Configuration value does not exist");
            }
        }

        return $value;

    }
    
    /**
     * Retrieves the path to the temporary file folder of the system.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getPathFilesTemporary()
    {
        
        $folder = PerisianFrameworkToolbox::getConfig("basic/project/folder") . "files/temp/";
                
        return $folder;
        
    }

    /**
     * Takes a string or an array (recursively) and puts all contained entries into UTF8 encoding.
     * 
     * @param mixed $d Can be a string or an array
     * @return mixed
     */
    public static function utf8ize($d) 
    {

        if(is_array($d)) 
        {

            foreach ($d as $k => $v) 
            {

                $d[$k] = static::utf8ize($v);

            }

        } 
        else if (is_string ($d)) 
        {

            return utf8_encode($d);

        }

        return $d;

    }

    /**
     * Retrieves the localized name for the specified month index.
     * 
     * @param int $monthIndex The index of the desired month, starting at 0.
     * @return string
     */
    public static function getMonthName($monthIndex)
    {

        $months = array(10367, 10368, 10369, 10370, 10371, 10372, 10373, 10374, 10375, 10376, 10377, 10378);

        $result = PerisianLanguageVariable::getVariable($months[$monthIndex]);

        return $result;

    }

    /**
     * Highlights $highlight in $text:
     * encloses the text to highlight with <span class="highlight"> and </span>
     *
     * @param String $highlight String to highlight
     * @param String $text String containing text to highlight
     * @return String The text with the highlighted part
     */
    public static function highlightText($highlight, &$text)
    {

        $highlightLength = strlen($highlight);

        $start = stripos($text, $highlight);
        $end = $start + $highlightLength;

        if($start !== false)
        {
            $parts = Array();
            $parts[] = substr($text, 0, $start);
            $parts[] = '<span class="highlight">';
            $parts[] = substr($text, $start, $highlightLength);
            $parts[] = '</span>';
            $parts[] = substr($text, $end);

            $text = implode('', $parts);
        }

        return $text;

    }

    /**
     * Reverse public static function of highlightText
     * @param String $text
     * @return String
     */
    public static function stripHighlight($text)
    {

        $value = str_replace('<span class="highlight">', '', $text);
        $value = str_replace('</span>', '', $value);

        return $value;

    }
    
}
