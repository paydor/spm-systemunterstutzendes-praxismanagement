<?php

require "framework/abstract/PerisianModuleAbstract.class.php";

/**
 * Handles modules of the system
 *
 * @author Peter Hamm
 * @date 2016-04-08
 */
class PerisianModuleManager
{
    
    protected static $moduleFolder = 'library/modules/';
    
    protected static $moduleList;
    
    /**
     * Retrieves the absolute path to the module folder.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function getModuleFolder()
    {
        
        $moduleFolder = PerisianFrameworkToolbox::getConfig('basic/project/folder') . static::$moduleFolder;
        
        return $moduleFolder;
        
    }
    
    /**
     * Toggles the module with the specified identifier to the specified enabled status.
     * 
     * @author Peter Hamm
     * @param String $moduleIdentifier
     * @param bool $moduleEnabled
     * @return Array Updated module info
     */
    public static function toggleModuleEnabled($moduleIdentifier, $moduleEnabled)
    {
        
        $moduleInfo = PerisianModuleManager::getModuleInfoByProperty('identifier', $moduleIdentifier, false, true);
                
        if($moduleInfo && isset($moduleInfo['class']))
        {
            
            $moduleClass = static::getModuleInstance($moduleInfo['class']);
            
            $moduleClass->toggleEnabled($moduleEnabled);
            
        }
        
        $moduleInfoNew = PerisianModuleManager::getModuleInfoByProperty('identifier', $moduleIdentifier, true, true);
        
        return $moduleInfoNew;
        
    }
    
    /**
     * Toggles the module with the specified identifier to the specified blocked status.
     * 
     * @author Peter Hamm
     * @param String $moduleIdentifier
     * @param bool $moduleBlocked
     * @return Array Updated module info
     */
    public static function toggleModuleBlocked($moduleIdentifier, $moduleBlocked)
    {
        
        $moduleInfo = PerisianModuleManager::getModuleInfoByProperty('identifier', $moduleIdentifier, false, true);
                
        if($moduleInfo && isset($moduleInfo['class']))
        {
            
            $moduleClass = static::getModuleInstance($moduleInfo['class']);
            
            $moduleClass->toggleBlocked($moduleBlocked);
            
        }
        
        $moduleInfoNew = PerisianModuleManager::getModuleInfoByProperty('identifier', $moduleIdentifier, true, true);
        
        return $moduleInfoNew;
        
    }
    
    
    /**
     * Tries to instantiate a new module with the specified class name.
     * 
     * @author Peter Hamm
     * @param String $moduleClassName
     * @return Object
     */
    public static function getModuleInstance($moduleClassName)
    {
        
        $moduleClass = new $moduleClassName();
        
        return $moduleClass;
        
    }
    
    /**
     * Retrieves module information from a module class file.
     * 
     * @author Peter Hamm
     * @param String $file The path to a file.
     * @return Array An associative array with module information.
     */
    public static function getModuleInfoFromFile($file)
    {
        
        $moduleInfo = null;
        
        if(@is_file($file))
        {
                        
            require_once $file;
            
            $className = substr($file, strrpos($file, "/") + 1);
            $className = substr($className, 0, strpos($className, "."));
            
            if(is_subclass_of($className, "PerisianModuleAbstract"))
            {
                
                $instance = new $className();
                
                $moduleInfo = array(
                    
                    "file" => $file,
                    "class" => $className,
                    "name" => $instance::getModuleName(),
                    "identifier" => $instance::getModuleIdentifier(),
                    "isEnabled" => $instance->isModuleEnabled(),
                    "isBlocked" => $instance->isModuleBlocked(),
                    "icon" => $instance->getModuleIcon(),
                    "backendAddress" => $instance->getBackendAddress()
                   
                );
                
                if(method_exists($instance, 'getFrontendAddress'))
                {
                    
                    $moduleInfo['frontendAddress'] = $instance->getFrontendAddress();
                    
                }
                
                unset($instance);
                
            }
            
        }
        
        return $moduleInfo;
        
    }
    
    /**
     * Retrieves the list of installed modules.
     * 
     * @author Peter Hamm
     * @param bool $ignoreCache Optional, default: false
     * @param bool $showBlocked Optional, default: false
     * @return String
     */
    public static function getModuleList($ignoreCache = false, $showBlocked = false)
    {
        
        if(!$ignoreCache && isset(static::$moduleList))
        {
            
            return static::$moduleList;
            
        }
        
        $moduleList = array();
        
        $moduleFolder = static::getModuleFolder();
        $moduleFolderContents = scandir($moduleFolder);
        
        for($i = 0; $i < count($moduleFolderContents); ++ $i)
        {
            
            $currentFile = $moduleFolder . $moduleFolderContents[$i];
                        
            if(is_file($currentFile))
            {
                                
                if(substr($currentFile, -10) == ".class.php")
                {
                    
                    $moduleInfo = static::getModuleInfoFromFile($currentFile);
                                        
                    if($moduleInfo != null && (!$moduleInfo['isBlocked'] || $showBlocked))
                    {
                                            
                        array_push($moduleList, $moduleInfo);
                        
                    }
                    
                }
                
            }
                        
        }
        
        usort($moduleList, 'static::compareByName');
        
        static::$moduleList = $moduleList;
                
        return $moduleList;
        
    }
    
    /**
     * Tries to retrieve module information for the module that has the $value for the $property.
     * 
     * @author Peter Hamm
     * @param String $property
     * @param String $value
     * @param bool $ignoreCache Optional, default: false
     * @param bool $showBlocked Optional, default: false
     * @return Array
     */
    public static function getModuleInfoByProperty($property, $value, $ignoreCache = false, $showBlocked = false)
    {
        
        $moduleList = static::getModuleList($ignoreCache, $showBlocked);
                
        foreach($moduleList as $moduleInfo)
        {
                        
            if(isset($moduleInfo[$property]) && $moduleInfo[$property] == $value)
            {
                
                return $moduleInfo;
                
            }
            
        }
        
        return null;
        
    }
    
    /**
     * Compares two arrays by their "name" key.
     * 
     * @author Peter Hamm
     * @param Array $a
     * @param Array $b
     * @return type
     */
    public static function compareByName($a, $b) 
    {
        
        return strcasecmp($a['name'], $b['name']);

    }
    
}