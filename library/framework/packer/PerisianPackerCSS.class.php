<?php

require_once 'framework/packer/PerisianPackerAbstract.class.php';
require_once 'external/various/CSSMin.class.php';

class PerisianPackerCSS extends PerisianPackerAbstract
{
    
    /**
     * Retrieves the desired CSS file.
     * If packaging is enabled in the system settings, it is packaged.
     * 
     * @author Peter Hamm
     * @date 2016-03-08
     * @param string $filePath
     * @return string The CSS file's contents (packaged/unpackaged), or an empty String if non-existent.
     */
    public static function getFile($filePath)
    {
        
        if(@strpos($filePath, -3) != ".css")
        {
            
            $filePath .= ".css";
            
        }
        
        return parent::getFile($filePath);
        
    }
    
    /**
     * Compresses the specified $fileContent String.
     * 
     * @author Peter Hamm
     * @param String $fileContent
     * @return type
     */
    protected static function packContent($fileContent)
    {
        
        $minifier = new CSSMin();
        
        $packedContent = $minifier->run($fileContent);
        
        return $packedContent;
        
    }
        
    /**
     * Retrieves the system's CSS asset folder.
     * 
     * @author Peter Hamm
     * @global type $style
     * @return string
     */
    protected static function getAssetFolder()
    {
        
        return parent::getAssetFolder() . 'css/';
        
    }
    
    /**
     * Retrieves the system's CSS cache folder.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function getCacheFolder()
    {
        
        return parent::getCacheFolder() . 'css/';
        
    }
    
}
