<?php

require_once 'framework/packer/PerisianPackerAbstract.class.php';
require_once 'external/various/JavaScriptPacker.class.php';

class PerisianPackerJavaScript extends PerisianPackerAbstract
{
    
    /**
     * Retrieves the desired javascript file.
     * If packaging is enabled in the system settings, it is packaged.
     * 
     * @author Peter Hamm
     * @date 2016-03-08
     * @param string $filePath
     * @return string The JavaScript file's contents (packaged/unpackaged), or an empty String if non-existent.
     */
    public static function getFile($filePath)
    {
        
        if(@strpos($filePath, -3) != ".js")
        {
            
            $filePath .= ".js";
            
        }
        
        return parent::getFile($filePath);
        
    }
    
    /**
     * Compresses the specified $fileContent String.
     * 
     * @author Peter Hamm
     * @param String $fileContent
     * @return type
     */
    protected static function packContent($fileContent)
    {
        
        $packer = new JavaScriptPacker($fileContent, 'Numeric', true, false);
        
        $packedContent = $packer->pack($fileContent);
        
        return $packedContent;
        
    }
        
    /**
     * Retrieves the system's JavaScript asset folder.
     * 
     * @author Peter Hamm
     * @global type $style
     * @return string
     */
    protected static function getAssetFolder()
    {
        
        return parent::getAssetFolder() . 'js/';
        
    }
    
    /**
     * Retrieves the system's JavaScript cache folder.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function getCacheFolder()
    {
        
        return parent::getCacheFolder() . 'js/';
        
    }
    
}
