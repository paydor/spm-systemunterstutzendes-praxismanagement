<?php

abstract class PerisianPackerAbstract
{
    
    /**
     * Retrieves the desired javascript file.
     * If packaging is enabled in the system settings, it is packaged.
     * 
     * @author Peter Hamm
     * @date 2016-03-08
     * @param string $filePath
     * @return string The file's contents (packaged/unpackaged), or an empty String if non-existent.
     */
    public static function getFile($filePath)
    {
        
        $fullFilePath = static::getAssetFolder() . $filePath;
        
        $fileContents = "";
        
        if(file_exists($fullFilePath))
        {
            
            if(static::isPackagingEnabled())
            {
                
                try
                {
                    
                    $fileContents = static::getPackedFile($fullFilePath);
                    
                    return $fileContents;
                    
                }
                catch(Exception $e)
                {
                    
                    // Do nothing
                    
                }
                
            }
            
            try
            {

                $fileContents = file_get_contents($fullFilePath);

            }
            catch(Exception $e)
            {

                // Do nothing

            }
            
        }
        
        return $fileContents;
        
    }
    
    /**
     * Retrieves whether packaging is enabled for the system or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isPackagingEnabled()
    {
        
        $value = false;
        
        try
        {
            
            $setting = new PerisianSystemSetting('system_is_packaging_enabled');
            
            $value = $setting->getValue() == 1;
            
        }
        catch(Exception $e)
        {


        };
        
        return $value;
        
    }
    
    /**
     * Retrieves the contents of the specified JavaScript file
     * and returns it in a packaged form. 
     * 
     * @author Peter Hamm
     * @param String $fullFilePath
     * @return String
     */
    protected static function getPackedFile($fullFilePath)
    {

        $packedContent = "";
        
        if(static::cacheForFileExists($fullFilePath))
        {
            
            $packedContent = static::getCacheForFile($fullFilePath);
            
        }
        else
        {

            $fileContent = file_get_contents($fullFilePath);

            $packedContent = static::packContent($fileContent);
            
            static::storeCacheForFile($fullFilePath, $packedContent);
            
        }
        
        return $packedContent;
        
    }
    
    /**
     * Checks if a cached version of the specified file already exists.
     * Considers the file's meta data.
     * 
     * @author Peter Hamm
     * @param string $fullFilePath
     * @return bool
     */
    protected static function cacheForFileExists($fullFilePath)
    {
        
        $value = false;
        
        $fileInfo = static::getFileInfo($fullFilePath);
        
        if(file_exists(static::getCacheFolder() . $fileInfo["attributes"]["cacheName"]))
        {
            
            // The cached data for this file already exist.
            
            $value = true;
            
        }
        
        return $value;
        
    }
    
    /**
     * Retrieves the cached content for the specified file.
     * 
     * @author Peter Hamm
     * @param string $fullFilePath
     * @return string The cached data.
     */
    protected static function getCacheForFile($fullFilePath)
    {
        
        $value = "false";
        
        $fileInfo = static::getFileInfo($fullFilePath);
        
        $theoreticalCachedFilePath = static::getCacheFolder() . $fileInfo["attributes"]["cacheName"];
        
        if(file_exists($theoreticalCachedFilePath))
        {
            
            // The cached data for this file already exist, load it.
            $value = file_get_contents($theoreticalCachedFilePath);
            
        }
        
        return $value;
        
    }
    
    /**
     * Stores the specified data as the cache for the specified file.
     * 
     * @author Peter Hamm
     * @param string $fullFilePath
     * @param string $packedContent
     * @return void
     */
    protected static function storeCacheForFile($fullFilePath, $packedContent)
    {
        
        $cacheDirectoryContents = @scandir(static::getCacheFolder());
        
        $fileInfo = static::getFileInfo($fullFilePath);
        
        for($i = 0; $i < count($cacheDirectoryContents); ++$i)
        {
            
            if($cacheDirectoryContents)
            {
                
                // Delete the old cached version
                if(substr($cacheDirectoryContents[$i], 0, strlen($fileInfo["attributes"]["fileNameHash"])) == $fileInfo["attributes"]["fileNameHash"])
                {
                    
                    // And old file exists, delete it.
                    unlink(static::getCacheFolder() . $cacheDirectoryContents[$i]);
                    
                }
                
            }
            
        }
        
        if(!is_writable(static::getCacheFolder()))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10948) . ' "' . static::getCacheFolder() . '"');
            
        }
        
        file_put_contents(static::getCacheFolder() . $fileInfo["attributes"]["cacheName"], $packedContent);
        
    }
    
    /**
     * Retrieves basic information from the specified file path.
     * 
     * @author Peter Hamm
     * @param string $fullFilePath
     * @return array An associatiove array with different values.
     */
    protected static function getFileInfo($fullFilePath)
    {
        
        $actualFilePath = realpath($fullFilePath);
        $actualFolder = substr($actualFilePath, 0, strrpos($actualFilePath, "/") + 1);
        $actualFileName = substr($actualFilePath, strrpos($actualFilePath, "/") + 1);
        
        $timeModified = filemtime($actualFilePath);
        $fileSize = filesize($actualFilePath);
        
        $valueArray = array(
            
            "file" => $actualFileName,
            "folder" => $actualFolder,
            "path" => $actualFilePath,
            
            "attributes" => array(
                
                "modified" => $timeModified,
                "size" => $fileSize,
                "fileNameHash" => md5($actualFilePath),
                "cacheName" => md5($actualFilePath) . "-" . $timeModified . "-" . $fileSize
                
            )
            
        );
        
        return $valueArray;
        
    }
    
    /**
     * Compresses the specified $fileContent String.
     * 
     * @author Peter Hamm
     * @param String $fileContent
     * @return type
     */
    protected static function packContent($fileContent)
    {
        
        return "";
        
    }
    
    /**
     * Retrieves the system's asset folder.
     * 
     * @author Peter Hamm
     * @global type $style
     * @return string
     */
    protected static function getAssetFolder()
    {
        
        global $style;
        
        $assetFolder = PerisianFrameworkToolbox::getConfig("basic/project/folder") . "templates/{$style}/asset-data/";
        
        return $assetFolder;
        
    }
    
    /**
     * Retrieves the system's cache folder.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function getCacheFolder()
    {
                
        $cacheFolder = PerisianFrameworkToolbox::getConfig("basic/project/folder") . "files/cache/";
        
        if(!file_exists($cacheFolder))
        {
            
            @mkdir($cacheFolder);
            
        }
        
        return $cacheFolder;
        
    }
    
}

?>