<?php

class PerisianUploadFileManager
{
    
    protected static $allowedTypes = array('image');

    /**
     * Retrieves the upload file path for the specified $type.
     * 
     * @author Peter Hamm
     * @param String $type One of the allowed types, e.g. "image"
     * @return string
     * @throws PerisianException
     */
    public static function getPathForType($type)
    {
        
        $type = strtolower($type);
        
        if(!in_array($type, static::$allowedTypes))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10951));
            
        }
        
        $folder = PerisianFrameworkToolbox::getConfig("basic/project/folder") . "files/upload/" . $type . "/";
        
        return $folder;
        
    }
    
    /**
     * Retrieves external file url for the specified $type.
     * 
     * @author Peter Hamm
     * @param String $type One of the allowed types, e.g. "image"
     * @return string
     * @throws PerisianException
     */
    public static function getUrlForType($type)
    {
        
        $type = strtolower($type);
        
        if(!in_array($type, static::$allowedTypes))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10951));
            
        }
        
        $url = PerisianFrameworkToolbox::getServerAddress(false) . "u/" . $type . "/";
        
        return $url;
        
    }
    
    /**
     * Deletes the file with the specified $filename of the specified $type
     * 
     * @author Peter Hamm
     * @param String $type One of the allowed types, e.g. "image"
     * @param type $filename The file's name.
     * @return bool
     */
    public static function deleteFile($type, $filename)
    {
        
        $deletePath = static::getPathForType($type);
        
        if(strlen($filename) == 0)
        {
            
            return false;
            
        }
        
        @unlink($deletePath . $filename);
        
        return true;
        
    }
    
    /**
     * Tries to handle an uploaded file with the specified type.
     * 
     * @param String $type One of the allowed types, e.g. "image"
     * @param array $files
     * @param bool $preserveFileExtension Preserve the file's extension?
     * @return array
     */
    public static function handleUploadedFile($type, $files, $preserveFileExtension = true)
    {
        
        $targetPath = static::getPathForType($type);
        $targetFileExtension = $preserveFileExtension ? static::getFileExtension($files['file']["name"]) : "";
        
        $result = array(
            
            "message" => lg(10665),
            "success" => false
            
        );
        
        if(!empty($files))
        {
        
            $uploadedFile = $files['file']['tmp_name'];
            
            $filename = static::getRandomFilename($targetPath) . $targetFileExtension;

            $targetFile = $targetPath . $filename;

            if(!move_uploaded_file($uploadedFile, $targetFile))
            {
                
                $info = Array(
                    
                    'file_uploaded' => $uploadedFile,
                    'file_target' => $targetFile
                    
                );
                
                throw new PerisianException(lg(10954), $info);
                
            }
            
            $result["file"] = $filename;
            $result["path"] = $targetPath;
            $result["success"] = true;
            $result["message"] = lg(10666);
           
        }

        return $result;
        
    }
    
    /**
     * Retrieves the URL for the specified file.
     * 
     * @author Peter Hamm
     * @param String $type One of the allowed types, e.g. "image"
     * @param String $filename
     * @return type
     */
    public static function getUrlForFile($type, $filename)
    {
        
        $typePath = static::getPathForType($type);
        
        if(strlen($filename) == 0 || !file_exists($typePath . $filename))
        {
            
            return "";
            
        }
        
        $typeUrl = static::getUrlForType($type);
        
        return $typeUrl . $filename;
        
    }
    
    /**
     * Retrieves basic information for the specified file and the specified file type.
     * 
     * @author Peter Hamm
     * @param String $inputFileType
     * @param String $inputFileName
     * @return string
     */
    public static function getFileInfo($inputFileType, $inputFileName)
    {
        
        $fileFolder = static::getPathForType($inputFileType);
        $fileFolderUrl = static::getUrlForType($inputFileType);
        
        $filePath = realpath($fileFolder . $inputFileName);
        $fileUrl = ($fileFolderUrl . $inputFileName);
        
        if(strlen($filePath) == 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10952));
            
        }
        
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $fileType = finfo_file($finfo, $filePath);
        finfo_close($finfo);
        
        $fileInfo = array(

            "name" => $inputFileName,
            "filesize" => filesize($filePath),
            "type" => $fileType,
            "url" => $fileUrl,
            "path" => $filePath

        );
        
        return $fileInfo;
        
    }
    
    /**
     * Retrieves the contents of the specified file and file type.
     * 
     * @author Peter Hamm
     * @param String $inputFileType
     * @param String $inputFileName
     * @return String
     */
    public static function getFile($inputFileType, $inputFileName)
    {
        
        $fileFolder = static::getPathForType($inputFileType);
        $filePath = realpath($fileFolder . $inputFileName);
        
        return file_get_contents($filePath);
        
    }
    
    /**
     * Generates a random file name. Checks the specified folder if a file with that name already exists.
     * If that's the case, a new name is generated - this way you'll have a guaranteed unique random name.
     * 
     * @author Peter Hamm
     * @param String $folder
     * @return String
     */
    public static function getRandomFilename($folder)
    {
        
        $randomFileName = md5($folder . time() . rand(0, 100000));
        
        if(file_exists($folder . $randomFileName))
        {
            
            // Generate a new one
            return static::getRandomFilename($folder);
            
        }
        
        return $randomFileName;
        
    }
    
    /**
     * Retrieves the file extension for the specified file name.
     * 
     * @author Peter Hamm
     * @param String $fileName
     * @return String
     */
    public static function getFileExtension($fileName)
    {
        
        $extension = strrpos($fileName, ".") > 0 ? substr($fileName, strrpos($fileName, ".")): "";
        
        return $extension;
        
    }
    
}
