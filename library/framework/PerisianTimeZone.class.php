<?php

class PerisianTimeZone
{
    
    protected static $defaultTimeZoneName = "Europe/Berlin";
    
    /**
     * Retrieves the currently set time zone as a PHP native DateTimeZone object.
     * 
     * @author Peter Hamm
     * @return DateTimeZone
     */
    public static function getTimeZone()
    {
        
        $timeZone = new DateTimeZone(static::$defaultTimeZoneName);
        
        return $timeZone;
        
    }
    
    /**
     * Checks for possible time offsets between the user's browser and the specified timestamp.
     * Will return a corrected timestamp.
     *  
     * @param bool $browserHasDstOffset Is the user's browsers in daylight saving time?
     * @param time $timestamp The timestamp to check for and to modify
     * @return int
     */
    public static function addBrowserDstOffsetToTimestamp($browserHasDstOffset, $timestamp)
    {
                
        if($browserHasDstOffset)
        {
            
            // The browser is in DST, which means we need to 
            // alter the offset if the entry is not.

            if(!PerisianTimeZone::isTimestampInDST($timestamp))
            {
                
                $timestamp -= 3600;

            }

        }
        else
        {
            
            if(PerisianTimeZone::isTimestampInDST($timestamp))
            {
                
                $timestamp += 3600;

            }

        }
        
        return $timestamp;
        
    }
    
    /**
     * Retrieves the currently set time zone name.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getTimeZoneName()
    {
        
        return static::$defaultTimeZoneName;
        
    }
    
    /**
     * Retrieves whether the specified timezone has a daylight saving time or not.
     * 
     * @author Peter Hamm
     * @param String $timeZoneName
     * @return boolean
     */
    public static function timeZoneHasDst($timeZoneName)
    {
                
        $timeZone = new DateTimeZone($timeZoneName);
        
        $returnValue = count($timeZone->getTransitions(time())) > 0;
        
        return $returnValue;

    }
    
    /**
     * Retrieves information about the daylight saving time for ther specified time zone.
     * 
     * @author Peter Hamm
     * @param String $timeZoneName
     * @return Array
     */
    public static function getTimeZoneDstInfo($timeZoneName)
    {
        
        $returnValue = array(
            
            'timeZone' => $timeZoneName,
            'hasDst' => false,
            'begin' => null,
            'end' => null
            
        );
        
        if(!static::timeZoneHasDst($timeZoneName))
        {
            
            return $returnValue;
            
        }
        
        $returnValue['hasDst'] = true;
        
        $tz = new DateTimeZone($timeZoneName);
        $date = new DateTime("now", $tz);
        $trans = $tz->getTransitions();
        
        for($i = 0; $i < count($trans); ++ $i)
        {
            
            if($trans[$i]['ts'] > time())
            {
                
                if($trans[$i]['isdst'])
                {
                    
                    $returnValue['begin'] = $trans[$i];
                    
                }
                else
                {
                    
                    $returnValue['end'] = $trans[$i];
                    
                }
                
            }
            
            if($returnValue['begin'] != null && $returnValue['end'] != null)
            {
                
                break;
                
            }
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the current system's / specified timezone's offset in an interpretable string like "+1:00".
     * 
     * @author Peter Hamm
     * @param String $timeZoneName Optional, takes the system's current timezone if this is left empty. Default: null
     * @param bool $ignoreDaylightSavingTime Optional, ignores DST if set to true. Default: false
     * @return String
     */
    public static function getOffsetString($timeZoneName = null, $ignoreDaylightSavingTime = false)
    {
        
        if(is_null($timeZoneName))
        {
            
            $timeZoneName = static::$defaultTimeZoneName;
            
        }
        
        $timeOffsetInSeconds = static::getTimeZoneOffsetInSeconds($timeZoneName, $ignoreDaylightSavingTime);
        
        $defaultTimeZoneOffset = static::getFormattedTimeOffset($timeOffsetInSeconds);
        
        return $defaultTimeZoneOffset;
        
    }
    
    /**
     * Returns the timezone offset in seconds.
     * 
     * @author Peter Hamm
     * @param String $timeZoneName
     * @param bool $ignoreDaylightSavingTime Optional, ignores DST if set to true. Default: false
     * @return int
     */
    public static function getTimeZoneOffsetInSeconds($timeZoneName = null, $ignoreDaylightSavingTime = false)
    {
        
        if(is_null($timeZoneName))
        {
            
            $timeZoneName = static::$defaultTimeZoneName;
            
        }
        
        $timeOffsetInSeconds = timezone_offset_get(timezone_open($timeZoneName), new DateTime());
        
        if($ignoreDaylightSavingTime)
        {
            
            $timeOffsetInSeconds -= static::getDaylightSavingTimeOffsetInSeconds();
            
        }
        
        return $timeOffsetInSeconds;
        
    }
    
    /**
     * Checks if the specified timestamp is in the DST (daylight saving time).
     * Typically that means, in Summer: true, winter: false
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return bool
     */
    public static function isTimestampInDST($timestamp)
    {
                
        return date("I", $timestamp) == 1;
        
    }
    
    /**
     * Retrieves whether the day day of the specified timestamp is the day
     * where the clocks get switched from DST to winter time or net. (e.g. this should be true for the last Sunday of October in Germany).
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return bool
     */
    public static function isDaySwitchToWinterTime($timestamp)
    {
                
        $firstMinuteOfDay = static::getFirstMinuteOfDayAsTimestamp($timestamp);
        $noonOfDay = $firstMinuteOfDay + 12 * 3600;
        
        if(static::isTimestampInDST($firstMinuteOfDay) && !static::isTimestampInDST($noonOfDay))
        {
            
            return true;
            
        }
        
        return false;
        
    }
    /**
     * Retrieves whether the day day of the specified timestamp is the day
     * where the clocks get switched from winter to summer (/daylight saving) time or net. (e.g. this should be true for the last Sunday of March in Germany).
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return bool
     */
    public static function isDaySwitchToSummerTime($timestamp)
    {
                
        $firstMinuteOfDay = static::getFirstMinuteOfDayAsTimestamp($timestamp);
        $noonOfDay = $firstMinuteOfDay + 12 * 3600;
        
        if(!static::isTimestampInDST($firstMinuteOfDay) && static::isTimestampInDST($noonOfDay))
        {
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves the first minute (and first second) of the day of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired day
     * @return int The first minute of the day, as a timestamp
     */
    public static function getFirstMinuteOfDayAsTimestamp($timestamp)
    {
        
        $newTimestamp = gmmktime(0, 0, 0, date("n", $timestamp), date("d", $timestamp), date("Y", $timestamp));
                
        return $newTimestamp;
        
    } 
    
    /**
     * Retrieves the last second of the day of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired day
     * @return int The last second of the day, as a timestamp
     */
    public static function getLastSecondOfDayAsTimestamp($timestamp)
    {
        
        $newTimestamp = gmmktime(23, 59, 59, date("n", $timestamp), date("d", $timestamp), date("Y", $timestamp));
        
        return $newTimestamp;
        
    } 
    
    /**
     * Retrieves the first day of the week of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired week
     * @return int The first day of the week, as a timestamp
     */
    public static function getFirstDayOfWeekAsTimestamp($timestamp)
    {
        
        if(date("N", $timestamp) == 1)
        {
            
            // If the timestamp is a monday, add one week so that we get the right date with the strtotime() later.
            $timestamp += 86400 * 7;
            
        }
        
        $newTimestamp = strtotime("last monday " . date("Y-m-d", $timestamp));
        
        return $newTimestamp;
        
    }
    
    /**
     * Retrieves the last second of the week of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired week
     * @return int The last second of the week, as a timestamp
     */
    public static function getLastSecondOfWeekAsTimestamp($timestamp)
    {
        
        $timestamp = self::getFirstDayOfWeekAsTimestamp($timestamp) + 7 * 86400 - 1;
        
        return $timestamp;
        
    }
    
    /**
     * Retrieves the first day of the year of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired year
     * @return int The first day of the year, as a timestamp
     */
    public static function getFirstDayOfYearAsTimestamp($timestamp)
    {
        
        $newTimestamp = mktime(0, 0, 0, 1, 1, date("Y", $timestamp));
        
        return $newTimestamp;
        
    }
   
    /**
     * Retrieves the first day of the month of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired month
     * @return int The first day of the month, as a timestamp
     */
    public static function getFirstDayOfMonthAsTimestamp($timestamp)
    {
        
        $newTimestamp = gmmktime(0, 0, 0, date("n", $timestamp), 1, date("Y", $timestamp));
        
        return $newTimestamp;
        
    }
    
    /**
     * Retrieves the last second of the last day of the year of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired year
     * @return int The last second of the last day of the year, as a timestamp
     */
    public static function getLastSecondOfYearAsTimestamp($timestamp)
    {
        
        $newTimestamp = gmmktime(0, 0, 0, 1, 1, date("Y", $timestamp) + 1) - 1;
        
        return $newTimestamp;
        
    }
   
    /**
     * Retrieves the last second of the last day of the month of the specified timestamp
     * as a timestamp itself.
     * 
     * @author Peter Hamm
     * @param int $timestamp A timestamp in the desired month
     * @return int The last second of the last day of the month, as a timestamp
     */
    public static function getLastSecondOfMonthAsTimestamp($timestamp)
    {
        
        $newTimestamp = gmmktime(23, 59, 59, date("n", $timestamp), date("t", $timestamp), date("Y", $timestamp));
        
        return $newTimestamp;
        
    }
        
    /**
     * Returns the time offset in seconds if the daylight saving time is active.
     * Typicall that means: 3600 seconds in summer, 0 seconds in winter.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return int
     */
    public static function getDaylightSavingTimeOffsetInSeconds($timestamp = 0)
    {
        
        $timestamp = $timestamp == 0 ? time() : $timestamp;
        
        return static::isTimestampInDST($timestamp) ? 3600 : 0;
        
    }
    
    /**
     * Returns an interpretable time offset String.
     * E.g. if you input $offsetInSeconds = 3600, it will return "+1:00"
     * 
     * @param int $offsetInSeconds
     * @return String
     */
    protected static function getFormattedTimeOffset($offsetInSeconds)
    {
        
        $sign = $offsetInSeconds >= 0 ? "+" : "-";
        
        $formattedSeconds = static::secondsToTime($offsetInSeconds);
        
        $hoursAndMinutes = $formattedSeconds["h"] . ":" . ($formattedSeconds["m"] < 10 ? "0" : "") . $formattedSeconds["m"];
        
        $offsetString = $sign . $hoursAndMinutes;
        
        return $offsetString;
        
    }
    
    /** 
    * Converts the specified number of seconds into hours, minutes and seconds 
    * and returns an array containing those values 
    * 
    * @param int $inputSeconds The seconds
    * @return array 
    */ 
   public static function secondsToTime($inputSeconds)
   {

       $secondsInMinute = 60;
       $secondsInHour = 60 * $secondsInMinute;
       $secondsInDay = 24 * $secondsInHour;

       $days = floor($inputSeconds / $secondsInDay);

       $hourSeconds = $inputSeconds % $secondsInDay;
       $hours = floor($hourSeconds / $secondsInHour);

       $minuteSeconds = $hourSeconds % $secondsInHour;
       $minutes = floor($minuteSeconds / $secondsInMinute);

       $remainingSeconds = $minuteSeconds % $secondsInMinute;
       $seconds = ceil($remainingSeconds);

       $obj = array(
           
           'd' => (int) $days,
           'h' => (int) $hours,
           'm' => (int) $minutes,
           's' => (int) $seconds,
           
       );
       
       return $obj;
       
   }
    
}