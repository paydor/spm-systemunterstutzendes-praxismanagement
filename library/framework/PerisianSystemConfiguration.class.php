<?php

require_once 'database/PerisianDatabaseMigration.class.php';

class PerisianSystemConfiguration
{
    
    const ENFORCE_SSL = false;
    
    const SERVER_TYPE_DOCKER = 'docker';
    const SERVER_TYPE_LIVE = 'live';
    const SERVER_TYPE_TEST = 'test';
    const SERVER_TYPE_TEST_LOCAL = 'test_local';
    
    private static $configurations = array();
    
    /**
     * Retrieves the system's colors that can be set in the backend.
     * 
     * @author Peter Hamm
     * @param bool $getDefaultColors Optional, default: false
     * @return Array
     */
    public static function getSystemColors($getDefaultColors = false)
    {
        
        $returnValue = array(

            "main" => "#43b91c",
            "main_alternative" => "#b9eaa9",
            "main_alternative_two" => "#338c15",
            "secondary" => "#ffffff"

        );
        
        if(!$getDefaultColors)
        {
            
            $colorMain = PerisianSystemSetting::getSettingValue('backend_color_main');

            if(strlen($colorMain) > 0)
            {

                $returnValue["main"] = $colorMain;

            }

            $colorMainAlternative = PerisianSystemSetting::getSettingValue('backend_color_main_alternative');

            if(strlen($colorMainAlternative) > 0)
            {

                $returnValue["main_alternative"] = $colorMainAlternative;

            }

            $colorMainAlternativeTwo = PerisianSystemSetting::getSettingValue('backend_color_main_alternative_two');

            if(strlen($colorMainAlternativeTwo) > 0)
            {

                $returnValue["main_alternative_two"] = $colorMainAlternativeTwo;

            }

            $colorSecondary = PerisianSystemSetting::getSettingValue('backend_color_secondary');

            if(strlen($colorSecondary) > 0)
            {

                $returnValue["secondary"] = $colorSecondary;

            }
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Returns whether this is a test server or not.
     * 
     * @author Peter Hamm
     * @date 2015-09-01
     * @return bool
     */
    public static function isTestServer()
    {
        
        $serverType = self::getServerType();

        return ($serverType == static::SERVER_TYPE_TEST || $serverType == static::SERVER_TYPE_TEST_LOCAL);
        
    }
    
    /**
     * Retrieves the current page path, e.g. /index/
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getPagePath()
    {
        
        return $_SERVER["REDIRECT_URL"];
        
    }
    
    /**
     * Retrieves what kind of server this is.
     * 
     * @author Peter Hamm
     * @date 2015-07-20
     * @return string
     */
    public static function getServerType()
    {
        
        if(static::isInDockerContainer())
        {
            
            return static::SERVER_TYPE_DOCKER;
            
        }
        
        $serverType = static::SERVER_TYPE_LIVE;;
        
        $htmlRoot = "/var/www";
        
        if(strpos(strtolower($_SERVER["DOCUMENT_ROOT"]), $htmlRoot) === false)
        {
            
            $serverType = static::SERVER_TYPE_TEST_LOCAL;
            
        }
        
        return $serverType;
        
    }
        
    /**
     * Should be called as soon as possible,
     * will update some system variables to the environment the system is running on.
     * 
     * @author Peter Hamm
     * @return void
     */
    public static function updateRunningConfiguration()
    {
                
        static::setMySqlConnectionData();
        
        static::setServerEnvironment();
        
        static::updateGlobals();
        
    }
    
    /**
     * Handles the migration of the database, depending on what script file is being run.
     * 
     * @author Peter Hamm
     * @return void
     * @throws PerisianDatabaseMigrationException
     */
    public static function handleDatabaseMigration()
    {
        
        $excludedFiles = array(
            
            'ajax/css.php',
            'ajax/favicon.php',
            'ajax/javascript.php',
            'ajax/language_variable.php',
            'database_migration.php',
            'error.php'
            
        );
        
        $currentFile = @$_SERVER['SCRIPT_NAME'];
                
        foreach($excludedFiles as $excludedFile)
        {
            
            if(substr($currentFile, -strlen($excludedFile)) == $excludedFile)
            {
                
                // The currently called script file should not care about database migrations.
                // It's important for example for the JS and CSS loaders.
                
                return;
                
            }
            
        }
             
        if(!PerisianDatabaseMigration::isDatabaseUpToDate())
        {
            
            // A database update is required, throw an excepton to notify the following scripts.
            
            throw new PerisianDatabaseMigrationException(PerisianDatabaseMigrationException::FLAG_UPDATE_REQUIRED);
            
        }
        
    }
    
    /**
     * Initializes further global variables, e.g. the system wide settings being loaded from the database.
     * 
     * @author Peter Hamm
     * @global PerisianSystemSetting $settings
     * @global bool $debugModeEnabled
     * @global String $style
     * @global int $languageId
     * @return void
     */
    private static function updateGlobals()
    {
        
        global $user;
        global $settings;
        global $debugModeEnabled;
        global $style;
        global $languageId;
        
        $user = null;
        
        // System settings
        {
            
            try
            {
                
                $settings = new PerisianSystemSetting();
                
            }
            catch(PerisianException $e)
            {
				
				print_r($e);
				exit;
                                
                throw new PerisianSystemInitializationException($e->getMessage());
                
            }

            $debugModeEnabled = $settings->getSetting('is_debug_mode');
            $config['basic']['project']['debug_mode'] = $debugModeEnabled;
            
            $databaseRevision = $settings->getSetting('database_revision');
            $config['basic']['project']['database_revision'] = $databaseRevision;
            
        }
        
        // Style settings
        {

            if(PerisianSession::getSessionValue('style') == '')
            {

                // Set the standard style
                PerisianSession::setSessionValue('style', 'calsy');

            }

            if(PerisianFrameworkToolbox::getRequest('style') != '')
            {
                
                PerisianSession::setSessionValue('style', PerisianFrameworkToolbox::getRequest('style'));
                
            }

            $style = PerisianSession::getSessionValue('style');
            
        }

        $languageId = 1;
        
    }
    
    /**
     * Sets some various environment settings, e.g. if the script is currently running in the admin area or not.
     * 
     * @author Peter Hamm
     * @global type $config
     */
    protected static function setServerEnvironment()
    {
        
        global $config;
        
        $isAdminArea = strpos(@$_SERVER["PHP_SELF"], '/backend/') !== false || strpos(@$_SERVER["PHP_SELF"], '/admin/') !== false;
        
        $config['environment']['admin_area'] = $isAdminArea;
        
    }
    
    /**
     * Sets the language identifier.
     * 
     * @author Peter Hamm
     * @param int $languageId
     * @return void
     */
    public static function setLanguageId($languageId)
    {
        
        global $config;
        
        $config['basic']['language']['id'] = $languageId;
        
    }
    
    /**
     * Retrieves the currently set language identifier.
     * 
     * @author Peter Hamm
     * @return int
     */
    public static function getLanguageId()
    {
        
        global $config;
        
        return $config['basic']['language']['id'];
        
    }
    
    /**
     * Retrieves the configuration values from the specified $key .ini file.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return Array
     */
    public static function getConfiguration($key)
    {
        
        PerisianFrameworkToolbox::security($key);
        
        if(!isset(static::$configurations[$key]))
        {
            
            $configurationFolderPath = PerisianFrameworkToolbox::getConfig('basic/project/folder') . 'configuration/';
            $configurationPath = $configurationFolderPath . $key . '.ini';

            if(!file_exists($configurationPath))
            {

                throw new PerisianException("Invalid configuration: '" . $configurationPath . "'");

            }
            
            static::$configurations[$key] = parse_ini_file($configurationPath, true);
            
        }
        
        return static::$configurations[$key];
               
    }
    
    /**
     * Tries to find out if the application is running from within a Docker container.
     * 
     * It will check the file paths of additionally parsed PHP .ini files e.g. from modules like mysqli,
     * as well as the document root folder for any occurence of "docker".
     * 
     * @return boolean
     */
    public static function isInDockerContainer()
    {
                
        if(stripos($_SERVER["HTTP_HOST"], ".appuioapp.ch") !== false)
        {
                        
            return true;
            
        }
        
        $additionalParsedFiles = explode(',', php_ini_scanned_files());
        
        foreach($additionalParsedFiles as $additionalParsedFile)
        {
            
            if(stripos($additionalParsedFile, '/docker-php-ext-') !== false)
            {
                                
                return true;
                
            }
            
        }
        
        if(stripos(@$_SERVER['DOCUMENT_ROOT'], '/docker-') !== false)
        {
            
            return true;
            
        }
        
        return false;
        
    }
    
    /**
     * Tries to retrieve the configuration for the Docker instance.
     * Will only return valid data if the PHP application is run from within Docker.
     * 
     * If this is a freshly set-up Docker container, the user will be prompted
     * a basic installation screen to enter database connection credentials
     * that will then be tested and saved for future use.
     * 
     * @to do
     * 
     * @author Peter Hamm
     * @date 2019-04-06
     * @return Array
     */
    protected static function getDockerConfiguration()
    {
        
        if(static::isInDockerContainer())
        {
            
            // We're running in the docker container, the database configuration should be set through environment variables.
            
            $dockerDatabaseHost = getenv('CALSY_DB_HOST');
            $dockerDatabaseHostPort = @getenv('CALSY_DB_HOST_PORT');
            $dockerDatabaseDatabase = getenv('CALSY_DB_DB');
            $dockerDatabaseUser = getenv('CALSY_DB_USER');
            $dockerDatabasePassword = getenv('CALSY_DB_PASSWORD');
            
            if(strlen($dockerDatabaseHost) == 0 || strlen($dockerDatabaseDatabase) == 0)
            {
                
                $debugInfo = Array();
                
                if(strlen($dockerDatabaseHost) == 0)
                {
                    
                    array_push($debugInfo, "Database host not set: \"CALSY_DB_HOST\".");
                    
                }
                
                if(strlen($dockerDatabaseDatabase) == 0)
                {
                    
                    array_push($debugInfo, "Database name not set: \"CALSY_DB_DB\".");
                    
                }
                
                throw new PerisianException("The database connection is not configured. Please make sure to set this container's environment variables: \"DOCKER_DB_HOST\", \"DOCKER_DB_DB\", \"DOCKER_DB_USER\" and \"DOCKER_DB_PASSWORD\". Optionally also set the environment variable \"DOCKER_DB_HOST_PORT\".", $debugInfo);
                
            }
            
            $configuration = Array(
                
                'mysql' => Array(
                    
                    'main' => Array(

                        'host' => $dockerDatabaseHost,
                        'port' => $dockerDatabaseHostPort,
                        'database' => $dockerDatabaseDatabase,
                        'user' => $dockerDatabaseUser,
                        'password' => $dockerDatabasePassword
                        
                    )
                    
                )
                
            );
                        
            return $configuration;
                        
        }
        
        return Array();
                
    }
    
    /**
     * Sets the MySql connection data depending on what server the framework is running on.
     * 
     * @author Peter Hamm
     * @date 2015-07-20
     * @global type $config
     */
    protected static function setMySqlConnectionData()
    {
        
        global $config;
                
        $serverType = self::getServerType();
        
        if($serverType == static::SERVER_TYPE_DOCKER)
        {
            
            $databaseConfiguration = static::getDockerConfiguration();
            
            $config['database']['mysql']['main'] = $databaseConfiguration['mysql']['main'];
            
        }
        else
        {

            $databaseConfiguration = static::getConfiguration('database');
            
            if($serverType == static::SERVER_TYPE_LIVE)
            {

                $config['database']['mysql']['main'] = $databaseConfiguration['mysql']['main'];

            }
            else if($serverType == static::SERVER_TYPE_TEST_LOCAL)
            {

                $config['database']['mysql']['main'] = $databaseConfiguration['mysql']['development'];

            }
            
        }
                
    }
    
    /**
     * Retrieves the current system's fallback data, e.g. the address used in mailing, etc.
     * 
     * @author Peter Hamm
     * @return Array Associative array
     */
    protected static function getSystemFallbackData()
    {
        
        $systemFallbackData = array(
            
            'address' => PerisianSystemSetting::getSettingValue('system_fallback_server_address'),
            'project_folder' => PerisianSystemSetting::getSettingValue('system_fallback_server_project_folder'),
            'secure' => (PerisianSystemSetting::getSettingValue('system_fallback_server_secure') == 1 ? "on" : "off")
            
        );
        
        return $systemFallbackData;
        
    }
    
    /**
     * Retrieves whether the server is hosting a secure connection or not.
     * 
     * Can be forced by setting PerisianSystemConfiguration::ENFORCE_SSL true.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isSecureConnection()
    {
        
        $value = PerisianSystemConfiguration::ENFORCE_SSL || PerisianFrameworkToolbox::getServerVar('HTTPS') == 'on' || @$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https';
        
        return $value;
        
    }
    
    /**
     * Updates the server's fallback data, e.g. the address used in mailing, etc.
     * 
     * @author Peter Hamm
     * @date 2016-05-14
     * @return void
     */
    public static function updateServerFallbackData()
    {
        
        global $_SERVER;
        
        $systemFallbackData = static::getSystemFallbackData();
        
        if((@isset($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] != $systemFallbackData['secure']) || static::ENFORCE_SSL)
        {
            
            PerisianSystemSetting::setSettingValue('system_fallback_server_secure', (static::ENFORCE_SSL || $_SERVER['HTTPS'] == 'on') ? 1 : 0);
            
        }
                
        if(@isset($_SERVER["HTTP_HOST"]) && @$_SERVER["HTTP_HOST"] != $systemFallbackData['address'])
        {
            
            $host = $_SERVER["HTTP_HOST"];
            
            if(PerisianSystemSetting::getSettingValue('system_fallback_server_secure'))
            {
                                
                $host = str_replace('http://', 'https://', $host);
                
            }
            
            PerisianSystemSetting::setSettingValue('system_fallback_server_address', $host);
            
        }
        
        if(@isset($_SERVER["PROJECT_FOLDER_IN_ROOT"]) && @$_SERVER["PROJECT_FOLDER_IN_ROOT"] != $systemFallbackData['project_folder'])
        {
            
            PerisianSystemSetting::setSettingValue('system_fallback_server_project_folder', $_SERVER["PROJECT_FOLDER_IN_ROOT"]);
            
        }
                
    }
    
    /**
     * Use this whenever you don't have access to the $_SERVER variable, e.g. in CLI / cronjobs.
     * 
     * @author Peter Hamm
     * @global Array $config
     * @return void
     */
    public static function updateConfigWithServerFallbackData()
    {
        
        global $config, $_SERVER;
        
        $systemFallbackData = static::getSystemFallbackData();
        
        $_SERVER['HTTPS'] = $systemFallbackData['secure'];
        $_SERVER["HTTP_HOST"] = $systemFallbackData['address'];
        $_SERVER["PROJECT_FOLDER_IN_ROOT"] = $systemFallbackData['project_folder'];
                
        $config['basic']['connection']['secure'] = $systemFallbackData['secure'] == 'on';
        $config['basic']['project']['url'] = (static::ENFORCE_SSL || $config['basic']['connection']['secure'] ? 'https://' : 'http://') . $systemFallbackData['address'] . $systemFallbackData['project_folder'] . '/';
        
    }
    
    /**
     * Retrieves if the system is in debug mode or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isDebugMode()
    {
        
        global $config;
        
        return $config['basic']['project']['debug_mode'];
        
    }
    
    /**
     * Initializes he basic system configuration.
     * 
     * @author Peter Hamm
     * @global $config;
     * @return Array
     */
    public static function initSystemConfiguration()
    {
        
        global $config;
        
        $config = Array();

        // The default language ID of the system. (1 = "de", 2 = "en", ...)
        $config['basic']['language']['id'] = 1;

        $config['basic']['project']['name'] = "calsy";

        $config['basic']['connection']['secure'] = (@isset($_SERVER['HTTPS']) || @$_SERVER['HTTPS'] == 'on');

        $config['basic']['project']['charset'] = "Content-Type: text/html; charset=utf-8";

        // Debug mode active or not? Will be overriden by the database value after the connection was established.
        $config['basic']['project']['debug_mode'] = true;

        // Are all queries being logged in the database models?
        $config['basic']['project']['log_queries'] = false;

        // Basic project settings
        $config['basic']['project']['folder'] = @$_SERVER["DOCUMENT_ROOT"] . @$_SERVER["PROJECT_DOCUMENT_ROOT"] . (@$_SERVER["PROJECT_DOCUMENT_ROOT"] != '/' ? '/' : '');
        $config['basic']['project']['backend_folder'] = 'backend/';
        $config['basic']['project']['frontend_folder'] = 'frontend/';
        $config['basic']['project']['url'] = ($config['basic']['connection']['secure'] ? 'https://' : 'http://') . @$_SERVER["HTTP_HOST"] . @$_SERVER["PROJECT_FOLDER_IN_ROOT"] . '/';

        // Right settings
        {

            // Basic rights
            {

                // Which setting ID in the database resembles administrator rights? Default: 1
                $config['system']['setting_id']['admin_rights'] = 1;

            }

        }

        // Which setting ID in the database resembles if the page is in debug mode?
        $config['system']['setting_id']['debug_mode'] = 48;

        // Which setting ID in the database resembles the page's title? Default: 4
        $config['system']['setting_id']['page_title'] = 4;

        // Which setting ID in the database resembles a user's selected language? Default: 5
        $config['system']['setting_id']['user_language'] = 5;

        // Which setting ID in the database resembles default language? Default: 6
        $config['system']['setting_id']['default_language'] = 6;

        // Which setting ID in the database resembles blocked setting IDs? Default: 7
        $config['system']['setting_id']['blocked_settings'] = 7;

        // Customer and project settings
        {

            // The global hourly wage setting ID
            $config['system']['setting_id']['default_hourly_wage'] = 15;

            // The hourly wage setting ID for customers (overwrites global)
            $config['system']['setting_id']['customer_hourly_wage'] = 28;

            // The hourly wage setting ID for projects (overwirtes that of customers and global)
            $config['system']['setting_id']['project_hourly_wage'] = 29;

            // Setting ID that specifies if the customer is an 'A-customer'
            $config['system']['setting_id']['customer_special'] = 30;

        }

        // Default time and date settings
        {

            // Which setting ID in the database resembles default time format? Default: 24
            $config['system']['setting_id']['default_format_time'] = 24;

            // Which setting ID in the database resembles default date format? Default: 25
            $config['system']['setting_id']['default_format_date'] = 25;

            // Which setting ID in the database resembles a user's time format? Default: 26
            $config['system']['setting_id']['user_format_time'] = 26;

            // Which setting ID in the database resembles a user's time format? Default: 27
            $config['system']['setting_id']['user_format_date'] = 27;

            // The setting ID that stores the date (day) on which the monthly reports should be sent.
            $config['system']['setting_id']['monthly_report_date_day'] = 58;

            // The setting ID that stores the date when the monthly report was lat sent
            $config['system']['setting_id']['monthly_report_date_last'] = 61;

        }
        
        static::initSystemGlobals();
        
        return $config;
        
    }
    
    /**
     * Initializes a row of system-wide globals.
     * Will return all the values in an associative array as well.
     * However, those globals are subject to change in the system's initialization process
     * and may not represent their final values at this point.
     * 
     * @author Peter Hamm
     * @global int $executionStartTime The start time of the script's execution.
     * @global boolean $dontShowHeader
     * @global boolean $dontShowFooter
     * @global boolean $dontShowHeaderHtml
     * @global boolean $dontShowFooterHtml
     * @global boolean $isFrontend
     * @global boolean $showMenu
     * @global boolean $dontShowMenu
     * @global boolean $disableLoginScreen
     * @return Array
     */
    private static function initSystemGlobals()
    {
        
        global $executionStartTime;
        global $dontShowHeader;
        global $dontShowFooter;
        global $dontShowHeaderHtml;
        global $dontShowFooterHtml;
        global $isFrontend;
        global $showMenu;
        global $dontShowMenu;
        global $disableLoginScreen;
        
        $returnValue = array();
        
        if(!isset($executionStartTime))
        {
            
            $executionStartTime = microtime(true);
            
        }

        // Include header/footer phtml or not
        if(!isset($dontShowHeader))
        {

            $dontShowHeader = false;

        }

        if(!isset($dontShowFooter))
        {

            $dontShowFooter = false;

        }

        // Show header/footer html or not, will still include the phtml but skip the actual display.
        if(!isset($dontShowHeaderHtml))
        {

            $dontShowHeaderHtml = false;

        }

        if(!isset($dontShowFooterHtml))
        {

            $dontShowFooterHtml = false;

        }

        if(!isset($isFrontend))
        {

            $isFrontend = false;

        }

        // Initialization of some variables
        {

            $showMenu = false;
            $dontShowMenu = false;

        }

        if(!isset($disableLoginScreen))
        {

            $disableLoginScreen = false;

        }
        
        global $executionStartTime;
        global $dontShowHeader;
        global $dontShowFooter;
        global $dontShowHeaderHtml;
        global $dontShowFooterHtml;
        global $isFrontend;
        global $showMenu;
        global $dontShowMenu;
        global $disableLoginScreen;
        
        $returnValue = array(
            
            'executionStartTime' => $executionStartTime,
            'dontShowHeader' => $dontShowHeader,
            'dontShowFooter' => $dontShowFooter,
            'dontShowHeaderHtml' => $dontShowHeaderHtml,
            'dontShowFooterHtml' => $dontShowFooterHtml,
            'isFrontend' => $isFrontend,
            'showMenu' => $showMenu,
            'dontShowMenu' => $dontShowMenu,
            'disableLoginScreen' => $disableLoginScreen
            
        );
        
        return $returnValue;
        
    }
    
}
