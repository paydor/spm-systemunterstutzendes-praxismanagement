<?php

require_once 'framework/settings/PerisianDependentSetting.class.php';
require_once 'framework/PerisianLanguage.class.php';
require_once 'framework/mail/PerisianMail.class.php';
require_once 'framework/abstract/PerisianUserRegistrationAbstract.class.php';

/**
 * Standard user class
 * 
 * @author Peter Hamm
 * @date 2010-10-24
 */
class PerisianUser extends PerisianDatabaseModel
{

    const DEFAULT_USER_ID = '-1337';
    
    // Basic settings
    public $table = 'user';
    public $field_pk = 'user_id';

    // Table specific fields
    public $field_login_name = 'user_login_name';
    public $field_login_password = 'user_login_password';
    public $field_fullname = 'user_fullname';
    public $field_created = 'user_created';
    public $field_last_login = 'user_last_login';
    public $field_email = 'user_email';
    public $field_phone = 'user_phone';
    public $field_color = 'user_color';
    public $field_status = "user_status";
    public $field_image_profile = "user_image_profile";
    public $field_sorting_custom = 'user_sorting_custom';
    
    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_login_name', 'field_fullname', 'field_email', 'field_phone');

    // User specific members
    protected $loginSuccessful = false;
    protected $userSetting = null;
    
    protected $isCronjobUser = false;
    
    public $roleField = 'user_role';

    /**
     * Creates a new object and loads user data if desired
     *
     * @author Peter Hamm
     * @param integer $userId Optional: specify a user's ID to load his data, default: loads no data
     * @return void
     */
    public function __construct($userId = 0)
    {

        if(!PerisianValidation::checkId($userId) && $userId != static::DEFAULT_USER_ID)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p57ab3ac859102'));
            
        }
        
        parent::__construct('MySql', 'main');
        
        $this->{$this->field_last_login} = "00000000000000";

        if(!empty($userId) && $userId == static::DEFAULT_USER_ID)
        {
            
            $this->{$this->field_pk} = $userId;
            $this->{$this->field_fullname} = PerisianLanguageVariable::getVariable('p5b03118c5ff01');
            $this->{$this->field_color} = "#ff8000";
            
        }
        else if(!empty($userId))
        {
            
            $query = "{$this->field_pk} = '{$userId}'";
            $this->loadData($query);

        }
        
    }
    
    /**
     * Takes an array of user IDs and sorts them by the sorting key.
     * 
     * @author Peter Hamm
     * @param Array $userIdList
     * @return Array
     */
    public static function sortUserIdsBySortingKey($userIdList)
    {
        
        $keyList = Array();
        
        for($i = 0; $i < count($userIdList); ++$i)
        {
            
            try 
            {

                $userObject = new static($userIdList[$i]);

                $sortingKey = $userObject->{$userObject->field_sorting_custom};
                $sortingKey = strlen($sortingKey) > 0 ? $sortingKey : "000000000000000000000000000000000000000000" . $i;
                
                array_push($keyList, $sortingKey);
            
            } 
            catch(PerisianException $e) 
            {
                
                array_push($keyList, null);
                
            }
            
        }
        
        array_multisort($keyList, SORT_ASC, SORT_STRING, $userIdList);
                        
        return $userIdList;
        
    }
    
    /**
     * Retrieves the user's color
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getColor()
    {
        
        $color = $this->{$this->field_color};
        
        if(substr($color, 0, 1) != "#")
        {
            
            $color = "#" . $color;
            
        }
        
        return $color;
        
    }
    
    /**
     * Retreves the profile image address for the user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserImageProfile($userId)
    {
        
        $image = "";
        
        try
        {
            
            $userObj = new static($userId);
            
            $image = $userObj->getImageProfileAddress();

        }
        catch(PerisianException $e)
        {

        }
        
        return $image;
        
    }
    
    /**
     * Retrieves the user's profile image address, if one is set.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getImageProfileAddress()
    {
        
        $address = "";
        
        if($this->{$this->field_pk} == static::DEFAULT_USER_ID)
        {
            
            $address = PerisianFrameworkToolbox::getServerAddress() . 'assets/img/user_default.png';
            
            return $address;
            
        }
        
        $data = $this->getImageProfileData();
        
        if(strlen($data['image']['name']) > 0 && strlen($data['image']['url']) > 0)
        {
            
            $address = $data['image']['url'];
            
        }
        
        return $address;
        
    }
    
    /**
     * Retrieves the meta data of the profile image of this user (if applicable).
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getImageProfileData()
    {
        
        try
        {
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $this->{$this->field_image_profile});
            
        }
        catch(PerisianException $e)
        {
            
            $fileInfo = null;
            
        }
        
        $data = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => 'user_image_profile',
            "title" => PerisianLanguageVariable::getVariable('p591b287ccb77c'),
            "description" => PerisianLanguageVariable::getVariable('p591b28a1a5ca8'),

            "image" => $fileInfo

        );
            
        return $data;
        
    }
    
    /**
     * Sets the indicator whether this user is handled as a cronjob user or not.
     * Has special privileges.
     * 
     * @param bool $value Handle this user as a cronjob user or not?
     */
    public function setAsCronjobUser($value)
    {
        
        $this->isCronjobUser = $value;
        
    }
    
    /**
     * Retrieves if this user is trated as a cronjob user or not.
     * @return bool
     */
    public function isCronjobUser()
    {
     
        return $this->isCronjobUser;
        
    }
    
    /**
     * Retrieves a user by the specified credentials.
     * Will return null if the credentials were invalid.
     * 
     * @author Peter Hamm
     * @param String $userName
     * @param String $password
     * @return static
     */
    public static function getUserByCredentials($userName, $password)
    {
        
        PerisianFrameworkToolbox::security($userName);
        
        $password = static::encryptPassword($password);
        
        $returnValue = null;
        
        $userObject = new static();
        
        $isValidUser = $userObject->checkLogin($userName, $password);
        
        if($isValidUser)
        {
            
            $returnValue = $userObject;
            
        }
        
        return $returnValue;
        
    }

    /**
     * Checks if there is an entry for the specified username and -password
     * in the database.
     *
     * @author Peter Hamm
     * @param String $userName The user's login name
     * @param String $password The user's password
     * @return boolean Does this username / password combination exist or not?
     */
    public function checkLogin($userName, $password)
    {

        PerisianFrameworkToolbox::security($userName);
        PerisianFrameworkToolbox::security($password);

        if(strlen($userName) < 2 && strlen($password) < 5)
        {
            
            return false;
            
        }
       
        if($this->{$this->field_status} == 'inactive')
        {

            $this->loginSuccessful = false;
            
            return false;

        }
        
        $query = "{$this->field_login_name} = '{$userName}' AND {$this->field_login_password} = '{$password}'";
        
        try
        {
            
            $this->loginSuccessful = $this->loadData($query);
        
        }
        catch(PerisianException $e)
        {
            
            return false;
            
        }
        
        return $this->loginSuccessful;

    }

    /**
     * Use this to check if a user's access rights and if he is logged in or not
     * Returns an array containing an user object, plus two booleans indicating
     * his status
     *
     * @author Peter Hamm
     * @param String $action Optional. May be 'login' or 'logout'
     * @return PerisianUser An user object
     */
    static public function handleLogin($action = '')
    {

        PerisianFrameworkToolbox::security($action);

        if($action == 'login')
        {

            PerisianSession::setSessionValue('userName', PerisianFrameworkToolbox::getRequest('login_name'));
            PerisianSession::setSessionValue('userPassword', md5(PerisianFrameworkToolbox::getRequest('login_password')));
            
        }
        else if($action == 'logout')
        {

            PerisianSession::unsetSessionValue('userName');
            PerisianSession::unsetSessionValue('userPassword');

        }
                
        $user = new static();
                
        try
        {
                        
            $user->checkLogin(PerisianSession::getSessionValue('userName'), PerisianSession::getSessionValue('userPassword'));
            
        }
        catch(Exception $e)
        {
            
            return static::handleLogin("logout");
            
        }
        
        $languageId = $user->getLanguage();
        
        PerisianLanguageVariable::setLanguage($languageId);
        
        if($user->{$user->field_status} == 'inactive')
        {

            return $user;

        }

        if($action == 'login' && $user->isLoggedIn())
        {

            // If the user logged in successfully, he is being redirected
            // to the page he initially requested (or '/index/' by default).
            // Also, the login timestamp is being saved to the database

            $user->{$user->field_last_login} = PerisianFrameworkToolbox::sqlTimestamp();
            $user->save($user->field_last_login);

            $redirect = PerisianFrameworkToolbox::getRequest('redirect');

            if(strpos($redirect, 'do=logout') !== false)
            {
                
                $redirect = str_replace(Array('?do=logout', '&do=logout'), '', $redirect);
                
            }

            if(!isset($redirect) || strlen($redirect) == 0)
            {
                
                $redirect = PerisianFrameworkToolbox::getServerAddress() . 'index/';
                
            }

            header("Location: {$redirect}");

        }
        else if($user->isLoggedIn() && strpos(PerisianFrameworkToolbox::getServerVar('SCRIPT_NAME'), 'login.php') !== false)
        {

            // Direct requests of the login script when logged in
            // are being redirected to the indes file as well.
            
            $redirect = PerisianFrameworkToolbox::getServerAddress() . 'index/';
            
            header("Location: {$redirect}");

        }

        return $user;

    }
    
    /**
     * Retrieves a list of user data for the sending of monthly reports.
     * 
     * @return array
     */
    static public function getUsersForReports()
    {
     
        $object = new self();
        
        $reportFields = $object->getReportFields();
        
        $query = "";
        
        for($i = 0; $i < count($reportFields); ++$i)
        {
            
            if($i > 0)
            {
            
                $query .= " OR ";
            
            }
            
            $query .= $reportFields[$i]["field"] . ' = "1"';
            
        }
        
        $userList = $object->getData($query);
        
        return $userList;
        
    }

    /**
     * Outputs a list of users
     *
     * @author Peter Hamm
     * @param String $sortBy Optional: Which row to sort by, default: primary key (user ID)
     * @param String $sortOrder Optional: DESC or ASC, default: DESC
     * @param int $start Optional: Entry offset, default: 0
     * @param int $limit Optional: Entry limit, default: 50
     * @param String $search Optional: An individual search term, default: ''
     * @return Array A list of users
     */
    static public function getUserList($sortBy = 'pk', $sortOrder = 'DESC', $start = 0, $limit = 50, $search = '')
    {

        PerisianFrameworkToolbox::security($search);

        $object = new self();
        $query = $object->buildSearchString($search);
        
        $results = $object->getData($query, $sortBy, $sortOrder, $start, $limit, 'field_login_password');

        for($i = 0, $m = count($results); $i < $m; ++$i)
        {

            // If a search was performed, the search string is being highlighted
            if($search != '')
            {

                foreach($results[$i] as $key => &$field)
                {
                    
                    if(in_array($key, array($object->field_fullname, $object->field_login_name, $object->field_email)))
                    {
                        
                        PerisianFrameworkToolbox::highlightText($search, $field);
                        
                    }

                }

            }

            // Retrieve the settings for this user and set the user's role
            $settings = self::loadSettingsForUser($results[$i][$object->field_pk]);
            $results[$i][$object->roleField] = self::getRoleStringFromSetting($settings);
            
        }
        
        return $results;

    }

    /**
     * Returns the count of users
     *
     * @author Peter Hamm
     * @param String $search Optional: A search string, default: ''
     * @return int
     */
    static public function getUserCount($search = '')
    {

        PerisianFrameworkToolbox::security($search);

        $object = new self();
        $query = $object->buildSearchString($search);

        return $object->getCount($query);

    }
        
    /**
     * Tries to load the user's color for the specified user ID
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserColor($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        $color = '';
        
        if($userId > 0)
        {
            
            try
            {
                
                $userObject = new self($userId);
                
                $color = $userObject->{$userObject->field_color};
                
            }
            catch(PerisianException $e)
            {
             
                $color = "#ff0000";
                
            }
            
        }
        
        return $color;
        
    }
    
    /**
     * Tries to load the user's fullname for the specified user ID
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getUserFullname($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        $name = ""; 
                
        try
        {
            
            $userObj = new static($userId);
            
            $name = $userObj->{$userObj->field_fullname};

        }
        catch(PerisianException $e)
        {
            
            $name = PerisianLanguageVariable::getVariable('p5b03118c5ff01');

        }
        
        return $name;
        
    }
    
    /**
     * Retrieves a shortcut of the specified string.
     * E.g. the input "Wolfgang Testmann" would return the string "WT".
     * 
     * @param String $userFullname
     * @return String
     */
    public static function getUserFullnameShortcut($userFullname)
    {
        
        $split = explode(" ", $userFullname);
        
        $returnValue = array();
        
        for($i = 0; $i < count($split); ++$i)
        {
            
            $value = strtoupper(substr($split[$i], 0, 1));
            
            array_push($returnValue, $value);
            
        }
        
        $returnValue = implode("", $returnValue);
        
        return $returnValue;
        
    }
    
    /**
     * Tries to load a user by his e-mail address
     * 
     * @author Peter Hamm
     * @param String $emailAddress
     * @return void
     */
    public function getUserByEmail($emailAddress)
    {
        
        PerisianFrameworkToolbox::security($emailAddress);
        
        $query = $this->field_email . ' = "' . $emailAddress . '"';
        
        $this->loadData($query);
        
    }
    
    /**
     * Sends an e-mail out to the currently loaded user on how activate his newly registered account.
     * 
     * @author Peter Hamm
     * @return boolean Could the e-mail be sent or not?
     */
    public function sendEmailActivation()
    {
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        $result = PerisianUserActivation::addEntryForUser($this->{$this->field_pk});
        
        if(!$result["success"])
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the user
        {
            
            // The text content
            {
                
                $subject = PerisianLanguageVariable::getVariable(10645);
                
                $activationLink = PerisianFrameworkToolbox::getServerAddress() . "/registration/?do=r&c=" . $result["code"];

                $content = PerisianLanguageVariable::getVariable(10646) . "\n\n";
                $content = str_replace("%1", $this->{$this->field_fullname}, $content);
                $content = str_replace("%2", $activationLink, $content);
                $content = str_replace("%3", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
                
            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->{$this->field_email}, 'name' => $this->{$this->field_fullname})

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }
    
    /**
     * Sends an e-mail out to the currently loaded user on how to change his forgotten password
     * 
     * @author Peter Hamm
     * @param boolean $initial Is this actually a request for the user to set a custom password for the first login?
     * @return boolean Could the e-mail be sent or not?
     */
    public function sendEmailPasswordForgotten($initial = false)
    {
        
        if(strlen($this->{$this->field_email}) < 3)
        {
            
            return false;
            
        }
        
        $result = PerisianUserPasswordForgotten::addEntryForUser($this->{$this->field_pk});
        
        if(!$result["success"])
        {
            
            return false;
            
        }
        
        // Language handling
        {

            $previousLanguage = PerisianLanguageVariable::getLanguageId();

            $userLanguage = $this->getSetting('user_language');

            if($userLanguage > 0)
            {

                PerisianLanguageVariable::setLanguage($userLanguage);

            }

        }
        
        // Send the e-mail to the user
        {
            
            // The text content
            {
                
                $subject = PerisianLanguageVariable::getVariable($initial ? 10628 : 10618);
                
                $passwordLink = PerisianFrameworkToolbox::getServerAddress() . "/registration/?do=rp&c=" . $result["code"] . ($initial ? "&i=1" : "");

                $content = PerisianLanguageVariable::getVariable($initial ? 10627 : 10626) . "\n\n";
                $content = str_replace("%1", $this->{$this->field_fullname}, $content);
                $content = str_replace("%2", $passwordLink, $content);
                $content = str_replace("%3", PerisianFrameworkToolbox::timestampToDate(), $content);
                
                $content = nl2br($content);
                
                $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);

            }
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $this->{$this->field_email}, 'name' => $this->{$this->field_fullname})

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }

        // Change the language back to the default
        {

            PerisianLanguageVariable::setLanguage($previousLanguage);

        }
        
        return true;
        
    }

    /**
     * Loads all of a user's data and settings
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific user
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);
            
            $this->fillFields($data[0]);
            $this->loadSettings($data[0][$this->field_pk]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }

    /**
     * Loads a user's settings
     *
     * @author Peter Hamm
     * @param integer $userId A user's ID
     * @return void
     */
    protected function loadSettings($userId)
    {

        $this->userSetting = self::loadSettingsForUser($userId);

        return;
        
    }
    
    /**
     * Creates a settings object and loads the data.
     * 
     * @param String $userId
     * @return \PerisianUserSetting
     */
    protected static function loadSettingsForUser($userId)
    {

        PerisianFrameworkToolbox::security($userId);
        
        $settings = new PerisianUserSetting();
        
        if($userId > 0)
        {

            $settings->setDependency($userId);
            $settings->getSetting();
        
        }
        
        return $settings;
        
    }
    
    /**
     * Retrieves a string that describes the role of the user, e.g. "Admin" or "User".
     *
     * @author Peter Hamm
     * @return String
     */
    public function getRoleString()
    {
        
        $role = self::getRoleStringFromSetting($this->getUserSetting());
        
        return $role;
        
    }
    
    /**
     * Retrieves a string that describes the role of the user, e.g. "Admin" or "User".
     *
     * @author Peter Hamm
     * @return String
     */
    protected static function getRoleStringFromSetting($settingObject)
    {
        
        $role = PerisianLanguageVariable::getVariable(10025);
        
        if(self::isAdminSettingSet($settingObject))
        {
            
            // System administrator
            $role = PerisianLanguageVariable::getVariable(10000);
            
        }
        
        return $role;
        
    }

    /**
     * Returns true if the user is logged in, else: false
     *
     * @author Peter Hamm
     * @return boolean User logged in or not?
     */
    public function isLoggedIn()
    {
        
        return $this->{$this->field_status} == "inactive" ? false : $this->loginSuccessful;
        
    }
    
    /**
     * Checks whether this user has got administration rights or not
     *
     * @author Peter Hamm
    */
    public function isAdmin()
    {
        
        $setting = $this->getUserSetting();
        
        return self::isAdminSettingSet($this->getUserSetting());
        
    }

    /**
     * Checks whether this user has got administration rights or not
     *
     * @author Peter Hamm
     * @param PerisianUserSetting $settingsObject An optional object to read from
     * @return boolean
     */
    protected static function isAdminSettingSet($settingsObject)
    {

        if(!is_object(@$settingsObject))
        {
            
            return false;
            
        }
        
        return $settingsObject->getSetting(null, PerisianFrameworkToolbox::getConfig('system/setting_id/admin_rights')) == 1;

    }
    
    /**
     * Checks whether this user has got developer rights or not
     *
     * @author Peter Hamm
     * @return boolean
    */
    public function isDeveloper()
    {
        
        $setting = $this->getUserSetting();
        
        return self::isDeveloperSettingSet($this->getUserSetting());
        
    }

    /**
     * Checks whether this user has got developer rights or not
     *
     * @author Peter Hamm
     * @param PerisianUserSetting $settingsObject An optional object to read from
     * @return boolean
     */
    protected static function isDeveloperSettingSet($settingsObject)
    {

        if(!is_object(@$settingsObject))
        {
            
            return false;
            
        }
        
        return $settingsObject->getSetting(null, 'is_system_developer') == 1;

    }
    
    /**
     * Retrieves the user settings for this current object.
     * Loads it if it is not yet loaded.
     * 
     * @author Peter Hamm
     * @return PerisianUserSetting
     */
    protected function getUserSetting() 
    {
        
        if(!is_object($this->userSetting))
        {
                        
            $this->loadSettings($this->{$this->field_pk});
            
        }
        
        return $this->userSetting;
        
    }

    /**
     * Deletes an user by ID
     * 
     * @param int $userId Optional: An user's ID, default: ID of the object
     * @return void
     */
    public function deleteUser($userId = 0)
    {

        PerisianFrameworkToolbox::security($userId);

        if(empty($userId))
        {
            
            $userId = $this->{$this->field_pk};
            
        }
        
        $query = "{$this->field_pk} = {$userId}";
        
        return $this->delete($query, '', '', '', 1);

    }

    /**
     * Returns the setting object for this user
     *
     * @author Peter Hamm
     * @return PerisianUserSetting
     */
    public function getSettingObject()
    {
        
        return $this->getUserSetting();
        
    }
    
    /**
     * Retrieves the setting value for the specified setting ID or key.
     *
     * @author Peter Hamm
     * @param String $settingIdOrName The identifier or name of a setting.
     * @return mixed
     */
    public function getSetting($settingIdOrName)
    {
        
        $value = null;

        $settingObject = @$this->getSettingObject();

        if(is_object($settingObject))
        {

            $value = $settingObject->getSetting(null, $settingIdOrName);

        }
        
        return $value;

    }

    /**
     * Sets the setting value for the specified setting ID or key.
     *
     * @author Peter Hamm
     * @param String $settingIdOrName The identifier or name of a setting.
     * @param String $value Optional, the new value. Default: empty string
     * @return bool
     */
    public function setSetting($settingIdOrName, $value = '')
    {
        
        $setting = @$this->getSettingObject()->getSetting(null, $settingIdOrName);
                
        $setting->setValue($value);
        
        $setting->save();

    }

    /**
     * Checks if the setting value for the specified setting ID is '1' (true) or not.
     *
     * @author Peter Hamm
     * @param String $settingIdOrName Optional, the identifier or name of a setting. Default: empty
     * @return bool
     */
    public function hasSetting($settingIdOrName = '')
    {
        
        if($this->isCronjobUser)
        {
            
            return false;
            
        }

        if(@$this->getSettingObject()->getSetting(null, $settingIdOrName) == 1)
        {
            
            return true;
            
        }

        return false;

    }
    
    /**
     * Sets the language identifier for the specified user.
     * If the specified language identifier is invalid, the old one is kept.
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @param int $userId User identifier
     * @param int $languageId Language identifier
     * @return void
     */
    public function setLanguageForUserId($userId, $languageId)
    {
        
        if(!isset($userId) || strlen($userId) == 0)
        {
            
            return;
            
        }

        $languageExists = false;
        $languageObject = new PerisianLanguage();
        $languageList = PerisianLanguage::getLanguages();

        // Check if the specified language identifier is valid
        {

            if(isset($languageId) && strlen($languageId) > 0)
            {

                for($i = 0; $i < count($languageList); ++$i)
                {

                    if($languageList[$i][$languageObject->field_pk] == $languageId)
                    {

                        $languageExists = true;

                        break;

                    }

                }

            }

        }

        if($languageExists)
        {

            $setting = new PerisianUserSetting();
            
            $setting->{$setting->field_fk} = PerisianFrameworkToolbox::getConfig('system/setting_id/user_language');
            $setting->setDependency($userId);
            $setting->{$setting->field_setting_value} = $languageId;
            
            $setting->save();

        }

    }

    /**
     * Retrieves the language ID of this user
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return int Language ID
     */
    public function getLanguage()
    {

        $languageSettingId = PerisianFrameworkToolbox::getConfig('system/setting_id/default_language');
        $settingObj = new PerisianSystemSetting();
                
        $specifiedLanguage = $settingObj->getSetting($languageSettingId);
        
        $languageObject = new PerisianLanguage();
        $languageList = PerisianLanguage::getLanguages();
        
        // Check if the user wants to switch the language
        {
            
            $languageParam = PerisianFrameworkToolbox::getRequest('lg');
            
            if(isset($languageParam) && strlen($languageParam) > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i][$languageObject->field_pk] == $languageParam)
                    {
                        
                        // Save the language into a cookie.
                        PerisianSession::setSessionValue('userLanguage', $languageParam);
                        
                        return $languageParam;
                        
                    }
                    
                }
                
            }
            
        }

        if($this->isLoggedIn())
        {

            $userLanguage = $this->getSettingObject()->getSetting(0, PerisianFrameworkToolbox::getConfig('system/setting_id/user_language'));

            if($userLanguage > 0)
            {
                
                PerisianSession::unsetSessionValue('userLanguage');
                $specifiedLanguage = $userLanguage;
                
            }

        }
        else
        {
            
            // Check if there's a cookie set, use it if true.
            $sessionValue = PerisianSession::getSessionValue('userLanguage');
            
            if(isset($sessionValue) && strlen($sessionValue) > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i][$languageObject->field_pk] == $sessionValue)
                    {
                        
                        $specifiedLanguage = $sessionValue;
                        
                        break;
                        
                    }
                    
                }
                
            }
            
        }
        
        return $specifiedLanguage;

    }

    /**
     * Overwritten save method that checks several criteria,
     * e.g. if a user with that name already exists.
     *
     * @author Peter Hamm
     * @param Array $saveFields Optional: An array containing names of fields that shall be saved, default: save all fields
     * @return mixed The ID of the saved user or false, if an error occured
     */
    public function save($saveFields = Array())
    {

        // Check if a valid password was set

        if(strlen($this->{$this->field_login_password}) == 0)
        {
            
            return false;
            
        }
        
        if(strlen($this->{$this->field_color}) == 0)
        {
            
            // Generate a random color for this user.
            
            $randomColor = dechex(rand(0x000000, 0xFFFFFF));
            
            while(strlen($randomColor) < 6)
            {
                
                $randomColor .= "F";
                
            }
            
            $this->{$this->field_color} = '#' . strtoupper($randomColor);
            
        }

        // Check if a user with this name or email already exists
        
        $query = "{$this->field_login_name} = '". PerisianFrameworkToolbox::security($this->{$this->field_login_name}) . "'";
        $query .= " OR {$this->field_email} = '". PerisianFrameworkToolbox::security($this->{$this->field_email}) . "'";

        if($this->{$this->field_pk} > 0)
        {
            $query = "($query) AND {$this->field_pk} != '". PerisianFrameworkToolbox::security($this->{$this->field_pk}) . "'";
        }

        $existingCount = $this->getCount($query);

        if($existingCount > 0)
        {
            return false;
        }

        return parent::save($saveFields);

    }
    
    /**
     * Generates a random password string.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getRandomPassword()
    {
        
        $value = time() * rand(0, 10000) * rand(1, 10000);
        
        return substr(md5($value), 0, 10);
        
    }

    /**
     * Use this method the encrypt a user's password
     *
     * @author Peter Hamm
     * @param String $password A password
     * @return String The password, encrypted
     */
    public static function encryptPassword($password)
    {

        return md5($password);

    }
    
    /**
     * System setting - Behaviour when deleting user entries: 
     * Keep the related data, but anonymize it?
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isEnabledAnonymizationOnDelete()
    {
        
        $settingValue = PerisianSystemSetting::getSettingValue('is_enabled_store_data_anonymized_on_user_deletion');
        
        if($settingValue == 'anonymize')
        {
                        
            return true;
            
        }
        
        return false;
        
    }

}

/**
 * Handles settings for users
 *
 * @author Peter Hamm
 */
class PerisianUserSetting extends PerisianDependentSetting
{

    // Basic values for the specific settings
    public $table = 'user_setting';
    public $field_pk = 'user_setting_id';
    public $field_fk = 'user_setting_setting_id';
    public $field_setting_value = 'user_setting_value';
    public $field_setting_dependency = 'user_setting_user_id';

    public $setting_name = 'user';
    
}

/**
 * Handles forgotten passwords
 *
 * @author Peter Hamm
 */
class PerisianUserPasswordForgotten extends PerisianUserRegistrationAbstract
{

    public $table = 'user_password_forgotten';
    public $field_pk = 'user_password_forgotten_id';
    public $field_fk = 'user_password_forgotten_user_id';
    public $field_code = 'user_password_forgotten_code';
    public $field_time = 'user_password_forgotten_time';
        
}

/**
 * Handles activation codes for users, 
 * works similar as when a user forgot his password.
 * 
 * @author Peter Hamm
 */
class PerisianUserActivation extends PerisianUserPasswordForgotten
{
    
    public $table = 'user_activation';
    public $field_pk = 'user_activation_id';
    public $field_fk = 'user_activation_user_id';
    public $field_code = 'user_activation_code';
    public $field_time = 'user_activation_time';
        
}
