<?php

/**
 * Provides functionality for the command line interface.
 * 
 * @author Peter Hamm
 * @date 2016-05-10
 */
class PerisianCLI
{
    
    static protected $log = array();
    
    /**
     * Prints a clearly visible log line to the screen and also stores it to the log.
     * 
     * @author Peter Hamm
     * @param mixed $message
     */
    public static function printAndLog($message)
    {
        
        static::printMessage($message);
        static::log($message);
        
    }
    
    /**
     * Executes the specified command on the command line.
     * Works on Windows and Unix.
     * 
     * @author Peter Hamm
     * @param String $command
     * @return void
     */
    public static function executeCommand($command)
    {
        
        if(substr(php_uname(), 0, 7) == "Windows")
        {
            
            pclose(popen("start /B " . $command, "r")); 
            
        }
        else 
        {
            
            exec($command . " > /dev/null &");  
            
        }
        
    }
    
    /**
     * Prints a clearly visible log line to the screen.
     * 
     * @author Peter Hamm
     * @param mixed $message
     */
    public static function printMessage($message)
    {
        
        $time = microtime(true);
        $printMessage = "\n[{$time}][LOG] {$message} \n";
        
        echo $printMessage;
        
    }
    
    /**
     * Logs the specified message.
     * 
     * @author Peter Hamm
     * @param mixed $message
     */
    protected static function log($message)
    {
        
        $logMessageData = array(
            
            "message" => $message,
            "time" => microtime(true)
            
        );
        
        array_push(static::$log, $logMessageData);
        
    }
    
    /**
     * Retrieves all logged messages.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getLog()
    {
        
        return static::$log;
        
    }
    
}