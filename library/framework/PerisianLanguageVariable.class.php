<?php

require_once 'database/PerisianDatabaseModel.class.php';
require_once 'PerisianLanguageVariableMissing.class.php';

/**
 * This class handles language variables of the system
 *
 * @author Peter Hamm
 * @date 2010-10-25
 */
class PerisianLanguageVariable extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'language_variable';
    public $field_pk = 'language_variable_id';

    // Fields
    public $field_variable_language_id = 'language_variable_language_id';
    public $field_variable_unique_sub_id = 'language_variable_unique_sub_id';
    public $field_variable_content = 'language_variable_content';
    
    // Instance
    static private $instance = null;

    // Caching for language variables
    private $cache = Array();

    // The default language ID is automatically being set, do not change it here
    static private $languageId = 0;

    /**
     * This may get called internally by self::getInstance or externally when
     * editing a specific language variable
     *
     * @param mixed $uniqueIdentifier Optional: If specified together with $languageId, the data of that variable with this unique identifier is being loaded to the object
     * @param int $languageId Optional: A language ID, specifiy this together with $languageVariableSubId to retrieve your desired entry
     * @author Peter Hamm
     * @return void
     */
    public function __construct($uniqueIdentifier = 0, $languageId = 0)
    {

        parent::__construct('MySql', 'main');
        
        static::formatUniqueIdentifier($uniqueIdentifier);

        if((strlen($uniqueIdentifier) > 0 || $uniqueIdentifier > 0) && $languageId > 0)
        {

            // Variable sub ID and language ID were specified, now load the variable's data!
                             
            $query = "{$this->field_variable_unique_sub_id} = '{$uniqueIdentifier}'";
            $query .= " AND {$this->field_variable_language_id} = '{$languageId}'";
            
            $result = $this->getData($query, 'pk', 'DESC', 0,1);
            
            if(count($result) == 1)
            {
                
                foreach($result[0] as $key => $value)
                {
                    
                    $this->{$key} = $value;
                    
                }

            }
            else if(count($result) == 0)
            {
                
                static::checkMissing($uniqueIdentifier);
                        
            }

        }

    }
        
    /**
     * Formats an unique identifier, e.g. trims it.
     * 
     * @author Peter Hamm
     * @param String $uniqueIdentifier
     * @return String
     */
    protected static function formatUniqueIdentifier(&$uniqueIdentifier)
    {
        
        $uniqueIdentifier = trim($uniqueIdentifier);
    
        return $uniqueIdentifier;
        
    }
    
    /**
     * Checks if the specified variable exists or not.
     * 
     * @author Peter Hamm
     * @param mixed $uniqueIdentifier String or integer.
     * @return bool
     */
    public static function variableExists($uniqueIdentifier)
    {
        
        static::formatUniqueIdentifier($uniqueIdentifier);
        
        $instance = static::getInstance();
        
        $query = "{$instance->field_variable_unique_sub_id} = '{$uniqueIdentifier}'";
        
        $count = $instance->getCount($query);
        
        return $count > 0;
        
    }
    
    /**
     * Should be triggered whenever a variable could not be found on the system.
     * 
     * @author Peter Hamm
     * @param String $variableIdentifier
     * @return void
     */
    protected static function checkMissing($variableIdentifier)
    {
        
        // Variable not found on this system, add it to the list of missing variables
        PerisianLanguageVariableMissing::reportMissingVariable($variableIdentifier);
        
    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static public function getInstance()
    {

        if(!isset(self::$instance[self::$languageId]))
        {
            
            self::$instance[self::$languageId] = new self;
            
        }

        return self::$instance[self::$languageId];

    }
    
    /**
     * Retrieves the currently set language identifier.
     * 
     * @author Peter Hamm
     * @return int
     */
    static public function getLanguageId()
    {
        
        return static::$languageId;
        
    }

    /**
     * This method allows you to change the language of the whole page.
     *
     * @author peter Hamm
     * @param integer $languageId Optional: Language Id, default: 1
     * @return void
     */
    static public function setLanguage($languageId = 1)
    {

        PerisianFrameworkToolbox::security($languageId);
        $languageId = (int)$languageId;
        
        self::$languageId = $languageId;
        $instance = self::getInstance();
        
        PerisianSystemConfiguration::setLanguageId($languageId);
        
    }

    /**
     * Returns a language variable with the specified sub or unique ID in the
     * language you selected before by calling setLanguage($languageId).
     * 
     * Retrieved variables are being cached.
     *
     * @author Peter Hamm
     * @param mixed $uniqueIdentifier The unique identifier of a language variable
     * @param boolean $allLanguages Optional: Do you want the content for every language? Default: false
     * @return mixed It's a string if $allLanguages = false, an array if $allLanguages = true
     */
    static public function getVariable($uniqueIdentifier, $allLanguages = false)
    {
        
        static::formatUniqueIdentifier($uniqueIdentifier);

        PerisianFrameworkToolbox::security($uniqueIdentifier);
        $instance = self::getInstance();
        
        // Query building
        {
                        
            $query = "{$instance->field_variable_unique_sub_id} = '{$uniqueIdentifier}'";
                        
        }

        if($allLanguages)
        {
            
            $result = $instance->getData($query);
            
            $languages = PerisianLanguage::getLanguages();
            $languageFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');

            $variable = Array();

            for($i = 0, $m = count($result); $i < $m; ++$i)
            {

                $variable[$result[$i][$instance->field_variable_language_id]] = $result[$i][$instance->field_variable_content];

            }

            for($i = 0, $m = count($languages); $i < $m; ++$i)
            {

                if(!isset($variable[$languages[$i][$languageFields['field_pk']]]))
                {

                    $variable[$languages[$i][$languageFields['field_pk']]] = '';
                    
                }

            }

        }
        else
        {
            
            if(!isset($instance->cache[$uniqueIdentifier][self::$languageId]))
            {

                $query .= " AND {$instance->field_variable_language_id} = " . self::$languageId;
                $result = $instance->getData($query);
                $variable = '';

                if(isset($result[0]))
                {
                    
                    $variable = $result[0][$instance->field_variable_content];
                    $instance->cache[$uniqueIdentifier][self::$languageId] = $variable;
                    
                }
                else
                {
                    
                    static::checkMissing($uniqueIdentifier);
                    
                }

            }
            else
            {

                $variable = $instance->cache[$uniqueIdentifier][self::$languageId];

            }

        }

        return (PerisianFrameworkToolbox::securityRevert($variable));

    }

    /**
     * Returns the count of unique variable sub ids
     * 
     * @author Peter Hamm
     * @param Array $languageIds Optional: An array containing the language IDs you want to have, default: Only language 1
     * @param String $search Optional: A search string, default: ''
     * @return int
     */
    static public function getVariableCount($languageIds = Array(1), $search = '')
    {

        PerisianFrameworkToolbox::security($languageIds);
        PerisianFrameworkToolbox::security($search);
        
        $instance = self::getInstance();
        $query = '';

        if($search != '')
        {
            
            $query = "{$instance->field_variable_language_id} IN (" . implode(', ', $languageIds) . ") ";
            $query .= "AND {$instance->field_variable_content} LIKE '%{$search}%'";
            
        }
        
        return $instance->getCountDistinct($query, $instance->field_variable_unique_sub_id);
        
    }

    /**
     * Directly echoes a language variable with the specified sub or unique ID
     *
     * @author Peter Hamm
     * @param mixed $uniqueIdentifier A unique identifier of a language variable
     * @return void
     */
    static public function printVariable($uniqueIdentifier)
    {

        PerisianFrameworkToolbox::security($uniqueIdentifier);
        
        echo self::getVariable($uniqueIdentifier);
        
        return;

    }
    
    /**
     * Generates a unique identifier for a new language variable.
     * 
     * @author Peter Hamm
     * @return String
     */
    static public function generateUniqueId()
    {
        
        $uniqueId = 'p' . uniqid();
        
        // Check if the unique ID already exists
        {
            
            $instance = new static();
            
            $query = "{$instance->field_variable_unique_sub_id} = '{$uniqueId}'";
    
            $existingCount = $instance->getCount($query);
            
            if($existingCount > 0)
            {
                
                return static::generateUniqueId();
                
            }
            
        }
        
        return $uniqueId;
        
    }

    /**
     * Deletes the language variable with the specified unique identifer
     *
     * @author Peter Hamm
     * @param int $uniqueIdentifier The unique identifier of the language variable you want to delete
     * @return bool
     */
    static public function deleteUniqueId($uniqueIdentifier)
    {
        
        static::formatUniqueIdentifier($uniqueIdentifier);

        PerisianFrameworkToolbox::security($uniqueIdentifier);

        if(strlen($uniqueIdentifier) > 0)
        {

            $instance = self::getInstance();
            $query = "{$instance->field_variable_unique_sub_id} = '$uniqueIdentifier'";
            
            echo $query;
            
            $instance->delete($query);
            
            return true;

        }

        return false;

    }

    /**
     * Outputs a list of the latest language variables.
     *
     * @author Peter Hamm
     * @param Array $languageIds Optional: An array containing the language IDs you want to have, default: Only language 1
     * @param int $start Optional: Entry offset, default: 0
     * @param int $limit Optional: Entry limit, default: 50
     * @param String $search Optional: An individual search term, default: ''
     * @return Array Contains a list of the latest language variables in the specified languages (via $languageIds)
     */
    static public function getLatestVariables($languageIds = Array(1), $start = 0, $limit = 50, $search = '')
    {

        PerisianFrameworkToolbox::security($languageIds);

        if(!is_array($languageIds))
        {
            
            $languageIds = Array($languageIds);
            
        }

        $instance = self::getInstance();
        $result = Array();
        $query = '';
        $querySearch = Array();
        $order = $instance->field_variable_unique_sub_id;

        $query = "{$instance->field_variable_language_id} IN (" . implode(', ', $languageIds) . ") ";
        
        if($search != '')
        {

            $querySearch[] = "{$instance->field_variable_content} LIKE '%{$search}%'";
            $querySearch[] = "{$instance->field_variable_unique_sub_id} = '{$search}'";
            
            $query = implode(' AND ', Array($query, '(' . implode(' OR ', $querySearch) . ')'));

        }

        // Select the latest language variables
        $uniqueIdentifiers = $instance->getDistinct($instance->field_variable_unique_sub_id, $query, $order, 'DESC', $start, $limit);
        
        $uniqueIdentifierString = "'" . implode("','", $uniqueIdentifiers) . "'";
        
        // Select the entries for the specific languages in the array
        if(count($uniqueIdentifiers) == 0)
        {

            for($i = 0, $m = count($languageIds); $i < $m; ++$i)
            {
                
                $result[$languageIds[$i]] = Array();
                
            }

        }
        else
        {
            
            $query = "{$instance->field_variable_unique_sub_id} IN ({$uniqueIdentifierString})";
              
            $resultData = $instance->getData($query, $order);
            
            $result = array();
            
            foreach($resultData as $data)
            {
                
                $data[$instance->field_variable_content] = strip_tags($data[$instance->field_variable_content]);
            
                if(in_array($data[$instance->field_variable_language_id], $languageIds))
                {
                    
                    foreach($languageIds as $languageId)
                    {
                        
                        if(!is_array(@$result[$languageId]))
                        {

                            $result[$languageId] = array();

                        }
                        
                        if($languageId == $data[$instance->field_variable_language_id])
                        {
                            
                            // This is the proper entry
                           
                            $result[$languageId][$data[$instance->field_variable_unique_sub_id]] = $data;
                            
                        }
                        else
                        {
                            
                            // Create an empty entry for the other language, if it doesn't exist yet
                            
                            
                            if(!isset($result[$languageId][$data[$instance->field_variable_unique_sub_id]]))
                            {
                                
                                $emptyEntry = array();
                            
                                $emptyEntry[$instance->field_pk] = null;
                                $emptyEntry[$instance->field_variable_content] = '';
                                $emptyEntry[$instance->field_variable_language_id] = $languageId;
                                $emptyEntry[$instance->field_variable_unique_sub_id] = $data[$instance->field_variable_unique_sub_id];
                                
                                $result[$languageId][$data[$instance->field_variable_unique_sub_id]] = $emptyEntry;
                                
                            }
                            
                        
                        }
                        
                    }
                
                }
                
            }
                        
            // Clean up the result
            {
            
                $tidyResult = array();

                foreach($languageIds as $languageId)
                {

                    // Tidy up the indexes
                    $tidyResult[$languageId] = array();

                    foreach($result[$languageId] as $entry)
                    {

                        array_push($tidyResult[$languageId], $entry);

                    }

                }

                $result = $tidyResult;
                
            }

            // If a search was performed, the search string is being highlighted
            if($search != '')
            {

                foreach($languageIds as $languageId)
                {
                                        
                    for($i = 0; $i < count($result[$languageId]); ++$i)
                    {

                        PerisianFrameworkToolbox::highlightText($search, $result[$languageId][$i][$instance->field_variable_content]);
                        
                    }

                }

            }

        }
                
        PerisianFrameworkToolbox::securityRevert($result);
        
        if(count($languageIds) == 1)
        {
            
            $result = $result[$languageIds[0]];
            
        }
        
        return $result;
        
    }

    /**
     * Overwritten save method, additionally checks if the content of this
     * variable is empty or not. If so, it is not saved.
     * 
     * If it already exists, it is being deleted!
     *
     * @author Peter Hamm
     * @return integer The primary key ID of the saved database row - or 0 if query failed or variable has been deleted
     */
    public function save()
    {

        if(empty($this->{$this->field_variable_content}))
        {
            
            if(!empty($this->{$this->field_pk}))
            {
                
                $query = "{$this->field_pk} = {$this->{$this->field_pk}}";
                
                $this->delete($query);
                
            }

            return 0;

        }

        return parent::save();

    }

}

/**
 * Shortcut for PerisianLanguageVariable::getVariable();
 * 
 * @author Peter Hamm
 * @param mixed $subOrUniqueId The sub or unique identifier of a language variable
 * @param boolean $allLanguages Optional: Do you want the content for every language? Default: false
 * @param int $specificLanguageId Optional, the identifier of the language to load the variable for. Default: The system's currently set language.
 * @return mixed It's a string if $allLanguages = false, an array if $allLanguages = true
 */
function lg($subOrUniqueId, $allLanguages = false, $specificLanguageId = 0)
{
    
    return PerisianLanguageVariable::getVariable($subOrUniqueId, $allLanguages, $specificLanguageId);
    
}

/**
 * Shortcut for PerisianLanguageVariable::printVariable();
 * 
 * @author Peter Hamm
 * @param mixed $uniqueIdentifier A unique identifier of a language variable
 * @return void
 */
function plg($subOrUniqueId)
{
    
    PerisianLanguageVariable::printVariable($subOrUniqueId);
    
}