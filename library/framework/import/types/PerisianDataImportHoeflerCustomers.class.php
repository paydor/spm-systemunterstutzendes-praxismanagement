<?php

require_once 'framework/import/PerisianDataImport.class.php';
require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';

/**
 * Import class for customers of Hoefler.
 * 
 * @version 1.0.0
 * @author Peter Hamm
 * @date 2016-06-18
 */
class PerisianDataImportHoeflerCustomers extends PerisianDataImportAbstract
{
    
    /**
     * Checks if the import is allowed for e.g. the current user.
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @return boolean
     */
    public function isImportAllowed()
    {
        
        global $user;
        
        $returnValue = false;
        
        if(isset($user) && $user->isAdmin())
        {
            
            $returnValue = true;
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the value from the specified CSV file, row index and field name.
     * 
     * @author Peter Hamm
     * @param parseCSV $csvFile
     * @param int $row
     * @param String $fieldName
     * @return String
     */
    protected function getFieldValue($csvFile, $row, $fieldName)
    {
        
        $csvData = @$csvFile->data[$row][$fieldName];
        
        return PerisianFrameworkToolbox::security($csvData);
        
    }
    
    /**
     * Imports the specified file name.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @param type $importFileName
     * @return Array
     */
    public function import($importFileName)
    {
        
        $importFolder = $this->getFolderPath('upload');
        $importFilePath = $importFolder . $importFileName;

        $objectArray = Array();
        $timeStart = microtime(true);
        $savedIdentifiers = Array();        

        $countExisting = 0;
        $countNew = 0;
        
        try
        {

            // Read it...
            
            $csvFile = new parseCSV();
            $csvFile->delimiter = ";";
            $csvFile->parse($importFilePath);
            
            for($row = 0; $row < count($csvFile->data); ++$row)
            {
                
                // Formatting of some fields
                {
                    
                    $external_id = ($this->getFieldValue($csvFile, $row, "﻿KdNr"));
                                        
                    $gender_enumerator = self::getSalutationEnumerator($this->getFieldValue($csvFile, $row, "Anrede"));
                    
                    // Split street and house number...
                    /*
                    {

                        $input_street = $this->getFieldValue($csvFile, $row, "Adresse");

                        $result = SwisscomAssociationImportAbstract::splitStreetAndNumber($input_street);

                        $address_street = (count($result) > 1 ? $result[1] : $input_street);
                        $address_street_number = (@$result[2]);

                    }
                     */
                    
                    $email_address = $this->getFieldValue($csvFile, $row, "EMail");
                    
                    if(strlen($email_address) < 5)
                    {
                        
                        $email_address = null;
                        
                    }
                    
                }
                
                // Check if this account is already in the database.
                {
                    
                    if(strlen($external_id) == 0)
                    {
                        
                        // Update @ 2017-03-30: Entries without external ID are no longer permitted.
                        
                        throw new PerisianException(PerisianLanguageVariable::getVariable('p58c92a7a3f1d4') . ' Kundennummer!');
                        
                    }
                    
                    $entryObject = CalsyUserFrontend::getUserFrontendForExternalId($external_id);
                    
                    if(strlen($entryObject->{$entryObject->field_pk}) == 0)
                    {
                        
                        ++$countNew;
                        
                    }
                    else
                    {
                        
                        ++$countExisting;
                        
                    }
                                    
                }
                
                $entryObject->{$entryObject->field_external_id} = $external_id;
                $entryObject->{$entryObject->field_sex} = $gender_enumerator;
                $entryObject->{$entryObject->field_name_first} = $this->getFieldValue($csvFile, $row, "Vorname");
                $entryObject->{$entryObject->field_name_last} = $this->getFieldValue($csvFile, $row, "Nachname");
                $entryObject->{$entryObject->field_address_street} = $this->getFieldValue($csvFile, $row, "Strasse");
                $entryObject->{$entryObject->field_address_additional} = $this->getFieldValue($csvFile, $row, "Adresszusatz");
                $entryObject->{$entryObject->field_address_zip} = $this->getFieldValue($csvFile, $row, "PLZ");
                $entryObject->{$entryObject->field_address_city} = $this->getFieldValue($csvFile, $row, "Ort");
                $entryObject->{$entryObject->field_address_country} = $this->getFieldValue($csvFile, $row, "Land");
                $entryObject->{$entryObject->field_phone} = trim(implode(" ", array($this->getFieldValue($csvFile, $row, "Vorwahl1"), $this->getFieldValue($csvFile, $row, "Telefon1"))));
                $entryObject->{$entryObject->field_email} = $email_address;
                                
                // Gather additional data that have no dedicated field in the database.
                {
                    
                    $dataCollection = array();

                    $dataCollection["Weitere Telefonnummer oder Fax "] = trim(implode(" ", array($this->getFieldValue($csvFile, $row, "Vorwahl2"), $this->getFieldValue($csvFile, $row, "Telefon2_Fax"))));
                    $dataCollection["Mobiltelefon"] = $this->getFieldValue($csvFile, $row, "Mobil");
                    $dataCollection["Geburtstag"] = $this->getFieldValue($csvFile, $row, "Geburtstag");
                    $dataCollection["Anzahl Besuche"] = $this->getFieldValue($csvFile, $row, "Anzahl_Besuche");
                    $dataCollection["Erster Besuch"] = $this->getFieldValue($csvFile, $row, "Erster_Besuch");
                    $dataCollection["Letzter Besuch"] = $this->getFieldValue($csvFile, $row, "Letzter_Besuch");
                    $dataCollection["Stylist 1"] = $this->getFieldValue($csvFile, $row, "Stylist1");
                    $dataCollection["Stylist 2"] = $this->getFieldValue($csvFile, $row, "Stylist2");
                    $dataCollection["Stylist 3"] = $this->getFieldValue($csvFile, $row, "Stylist3");
                    $dataCollection["Stylist 4"] = $this->getFieldValue($csvFile, $row, "Stylist4");
                    $dataCollection["Geloescht"] = $this->getFieldValue($csvFile, $row, "gelöscht");
                    
                    $additionalData = array();
                    
                    foreach($dataCollection as $key => $value)
                    {
                        
                        if(strlen($value) > 0)
                        {
                            
                            $additionalData[$key] = $value;
                            
                        }
                        
                    }
                    
                    if(count($additionalData) > 0)
                    {
                        
                        $entryObject->{$entryObject->field_additional_data} = PerisianFrameworkToolbox::security(json_encode(PerisianFrameworkToolbox::securityRevert($additionalData)));
                    
                    }
                
                }
                
                array_push($objectArray, $entryObject);
                
            }
            
        } 
        catch(Exception $e) 
        {

            $error = PerisianLanguageVariable::getVariable(10507);
            $error = str_replace("%", $e->getMessage(), $error);

            if(isset($row))
            {

                $rowAddition = PerisianLanguageVariable::getVariable(10654);
                $rowAddition = str_replace("%", $row, $rowAddition);

                $error .= ', ' . $rowAddition;

            }

            throw new PerisianException($error);

        }

        // Import it into the database
        {
            
            for($i = 0; $i < count($objectArray); ++$i)
            {
                
                try
                {
                    
                    $savedId = $objectArray[$i]->save();

                    array_push($savedIdentifiers, $savedId);
                    
                }
                catch(PerisianException $e)
                {
                     
                }

            }

        }
        
        $timeEnd = microtime(true);
        $timeTotal = round(($timeEnd - $timeStart) * 1000, 2);

        $success = PerisianLanguageVariable::getVariable(10657);
        $success = str_replace("%1", count($objectArray), $success);
        $success = str_replace("%2", $timeTotal, $success);

        //

        $result = array(
            
            "count_existing" => $countExisting,
            "count_new" => $countNew,
            
            "message" => $success, 
            "success" => true
            
        );
        
        return $result;
        
    }
    
    /**
     * Retrieves an enumerator value for a specified salutation string.
     * 
     * @param String $inputValue
     * @return string
     */
    public static function getSalutationEnumerator($inputValue)
    {
        
        $enumerator = "none";
        
        $inputValue = strtolower($inputValue);
        
        if($inputValue == "herr")
        {
            
            $enumerator = "male";
            
        }
        else if($inputValue == "frau")
        {
            
            $enumerator = "female";
            
        }
        
        return $enumerator;
        
    }
    
}
