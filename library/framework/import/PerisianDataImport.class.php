<?php

require_once 'external/PHPExcel/PHPExcel.php';
require_once 'external/parseCSV/parsecsv.lib.php';
require_once 'framework/import/types/PerisianDataImportHoeflerCustomers.class.php';

/**
 * Main import class
 * 
 * @author Peter Hamm
 * @date 2016-06-17
 */
class PerisianDataImport extends PerisianDatabaseModel
{
    
    public $table = 'data_import';
    public $field_pk = 'data_import_id';
    public $field_identifier = 'data_import_identifier';
    public $field_version = 'data_import_version';
    public $field_is_enabled = 'data_import_is_enabled';
    
    private $folderImport = "files/import/";
    private $folderImportUpload = "upload/";
    
    protected static $instance;
    
    /**
     * Creates a new object and loads data if desired
     *
     * @author Peter Hamm
     * @param integer $importId Optional: specify an import's ID to load its data, default: loads no data
     * @param integer $importIdentifierString Optional: Specify this together with the $importVersionString to load the data. Default: loads no data
     * @param integer $importVersionString Optional: Specify this together with the $importIdentifierString to load the data. Default: loads no data
     * @return void
     */
    public function __construct($importId = 0, $importIdentifierString = '', $importVersionString = '')
    {

        if(!PerisianValidation::checkId($importId))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable());
            
        }
        
        parent::__construct('MySql', 'main');
        
        if(!empty($importId))
        {
            
            $query = "{$this->field_pk} = '{$importId}'";
            
            $this->loadData($query);

        }
        else if(strlen($importIdentifierString) > 0 && strlen($importVersionString) > 0)
        {
            
            PerisianFrameworkToolbox::security($importIdentifierString);
            PerisianFrameworkToolbox::security($importVersionString);
            
            $query = "{$this->field_identifier} = '{$importIdentifierString}'";
            $query .= " AND {$this->field_version} = '{$importVersionString}'";
            
            $this->loadData($query);
            
        }

        return;

    }
    
    /**
     * Retrieves a singleton instance of this class.
     * 
     * @author Peter Hamm
     * @return type
     */
    public static function getInstance()
    {
        
        if(!isset(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Retrieves if the specified import/version is enabled
     * 
     * @author Peter Hamm
     * @param String $identifierString
     * @param String $versionString
     * @return bool
     */
    public static function isImportEnabled($identifierString, $versionString)
    {
        
        $object = new static(0, $identifierString, $versionString);
        
        return @$object->{$object->field_is_enabled} == 1;
        
    }
    
    /**
     * Generates an import object for the specified $type and optional $version.
     * 
     * @param String $type
     * @param String $version Optional, default: will be ignored.
     * @return PerisianDataImportAbstract
     */
    public static function getImportObjectByTypeAndVersion($type, $version = '')
    {
        
        $importTypes = static::getAllPossibleImportTypes();
        
        for($i = 0; $i < count($importTypes); ++$i)
        {
            
            if($importTypes[$i]['identifier'] == strtolower($type) && (strlen($version) == 0 || $importTypes[$i]['version'] == $version))
            {
                
                return $importTypes[$i];
                
            }
            
        }
        
        return null;
        
    }
    
    /**
     * Loads all of the data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific user
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Retrieves a list of all possible different import types.
     * 
     * @author Peter Hamm
     * @param array $identifierFilter Optional, specify a list of identifiers that should be filtered. Default: No filter
     * @return Array
     * @date 2015-09-27
     */
    public static function getAllPossibleImportTypes($identifierFilter = array())
    {
        
        $importTypes = array();
        
        // Hoefler customer import 
        $importTypes[] = array(
            
            "name" => PerisianLanguageVariable::getVariable('p57640984b2547'),
            "class" => "PerisianDataImportHoeflerCustomers",
            "identifier" => "hoefler_customers",
            "version" => "1.0.0",
            "enabled" => false,
            "selected" => true,
            "file_types" => array("txt")
            
        );
        
        // Add the dynamic 'enabled' status.
        for($i = 0; $i < count($importTypes); ++ $i)
        {
            
            if(count($identifierFilter) > 0)
            {
                
                if(!in_array($importTypes[$i]['identifier'], $identifierFilter))
                {
                    
                    unset($importTypes[$i]);
                    
                    continue;
                    
                }
                
            }
            
            $importTypes[$i]['enabled'] = static::isImportEnabled($importTypes[$i]['identifier'], $importTypes[$i]['version']);
            
        }
                
        return $importTypes;
        
    }
    
    /**
     * Retrieves a comma-seperated list of all possible import type file types.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getAllPossibleImportTypeFileTypes()
    {
        
        $list = array();
        
        $types = self::getAllPossibleImportTypes();
        
        for($i = 0; $i < count($types); ++$i)
        {
            
            for($j = 0; $j < count($types[$i]["file_types"]); ++$j)
            {
                
                $extension = "." . $types[$i]["file_types"][$j];
                
                if(!in_array($extension, $list))
                {
                    
                    $list[] = $extension;
                    
                }
                
            }
            
        }
        
        return implode(",", $list);
        
    }
        
    /**
     * Removes an uploaded file from the system.
     * 
     * @author Peter Hamm
     * @param string $filename
     */
    public function deleteUploadedFile($filename)
    {
        
        $importFolder = $this->getFolderPath('upload');
        $importFilePath = $importFolder . $filename;
        
        @unlink($importFilePath);
        
    }
            
    /**
     * Imports the specified XLSX.
     * 
     * @author Peter Hamm
     * @param String $importFileName The name of the XLS file to initially import
     * @param int $importTypeKey One of the possible import type keys. Default: Standard import
     * @throws PerisianException
     */
    public function import($importFileName, $importTypeKey = -1)
    {
        
        $importTypeKey = (int)$importTypeKey;
        
        $importFolder = $this->getFolderPath('upload');
        $importFilePath = $importFolder . $importFileName;
        
        $multipleAssociations = strlen($this->{$this->field_pk}) > 0;
        
        if(!file_exists($importFilePath))
        {
            
            $error = PerisianLanguageVariable::getVariable(10506);
            $error = str_replace("%", $importFilePath, $error);
            
            throw new PerisianException($error);
            
        }
        
        // Check the import type
        if($importTypeKey >= 0 && strlen($importTypeKey) > 0)
        {

            $possibleImportTypes = $this->getPossibleImportTypes(true);
            
            if(isset($possibleImportTypes[$importTypeKey]))
            {
                
                $selectedImportType = $possibleImportTypes[$importTypeKey];

                if(!class_exists($selectedImportType["class"]) || !is_subclass_of($selectedImportType["class"], "PerisianDataImportAbstract"))
                {

                    // The import class does not exists or does not implement the required abstract.
                    throw new PerisianException(PerisianLanguageVariable::getVariable(10770));

                }

                // The class exists, try to import now.
                $importClass = new $selectedImportType["class"]();
                
            }
            
        }
        
        // Go on with the standard import if no, read the file
        {

            $importClass = isset($importClass) ? $importClass : new SwisscomAssociationImportDefault();

            $importClass->setAssociationObject($this);
            $importClass->setHasMultipleAssociations($multipleAssociations);
            
        }
        
        return $importClass->import($importFileName);
        
    }
        
    /**
     * Retrieves the folder for the uploaded files
     * 
     * @param String $type Either empty, "import or "upload"
     * @return String
     */
    public function getFolderPath($type = "")
    {
        
        $path = PerisianFrameworkToolbox::getConfig('basic/project/folder') . $this->folderImport;
        
        if($type == "initial")
        {
            
            $path .= $this->folderImportInitial;
            
        }
        elseif($type == "upload")
        {
            
            $path .= $this->folderImportUpload;
            
        }
        
        return $path;
        
    }
    
    /**
     * Tries to handle an uploaded XLSX file. 
     * 
     * @param array $files
     * @return array
     */
    public function handleUploadedFile($files)
    {
        
        $result = array(
            
            "message" => PerisianLanguageVariable::getVariable(10665),
            "success" => false
            
        );
        
        if(!empty($files))
        {
        
            $folder = $this->getFolderPath('upload');

            $uploadedFile = $files['file']['tmp_name'];
            
            $fileName = self::getRandomFileName();

            $targetFile = $folder . $fileName;

            move_uploaded_file($uploadedFile, $targetFile);
            
            $result["file"] = $fileName;
            $result["success"] = true;
            $result["message"] = PerisianLanguageVariable::getVariable(10666);
           
        }

        return $result;
        
    }
    
    /**
     * Retrieves a random file name
     * @return String
     */
    public static function getRandomFileName()
    {
        
        return md5(rand(0, 10000) * time());
        
    }

}

abstract class PerisianDataImportAbstract 
{
        
    /**
     * Imports the specified file name.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @param type $importFileName
     * @return Array with the keys "message" (String - default: empty) and "success" (boolean - default: false)
     */
    public function import($importFileName)
    {
        
        return Array("message" => PerisianLanguageVariable::getVariable(10770), "success" => false);
        
    }
    
    /**
     * Can be enhanced and should be checked before executing the import.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public function isImportAllowed()
    {
        
        return true;
        
    }
        
    /**
     * Retrieves the folder for the uploaded files
     * 
     * @param String $type Either empty, "import or "upload"
     * @return String
     */
    public function getFolderPath($type)
    {
        
        $import = new PerisianDataImport();
        
        return $import->getFolderPath($type);
        
    }
    
    /**
     * Takes a string and tries to split it into street name and street number.
     * @param String $inputValue
     * @return Array
     */
    public static function splitStreetAndNumber($inputValue)
    {
                       
        preg_match('/
            ^                         # start of string
             (.+?)\s                  # street or PO box name with separating whitespace
             (                        # open capture group 2
              (?:                     # open non-capturing group for alternation
               [\da-z]+               # match digits and letters of house number
               (?:\s*-\s*[\da-z]+)?   # optional street ranges i.e. Sunshine Blvd. 1 - 8
               |                      # or
               \d{1,2}(?:\s*\d{2})+   # match PO box numbers
              )                       # close alternation group
             )                        # close capture group 2
            $                         # end of string
            /ix', 
                
        $inputValue, $result);  
        
        return $result;
        
    }
    
    /**
     * Parses a specified Excel date string into an SQL timestamp.
     * 
     * @param type $dateString
     * @return type
     */
    public static function parseDate($dateString)
    {
        
        $value = "";
        
        if(strlen($dateString) > 0)
        {

            $excelFormattedString = PHPExcel_Shared_Date::ExcelToPHP($dateString);
            $value = PerisianFrameworkToolbox::sqlTimestamp($excelFormattedString);
            
        }
        
        if(strlen($dateString) == 0)
        {
            
            $value = "00000000000000";
            
        }
        
        return $value;
        
    }
    
}
