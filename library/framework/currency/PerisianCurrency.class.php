<?php

class PerisianCurrency
{
    
    /**
     * Retrieves the complete list of supported currencies.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getCurrencies()
    {
        
        $list = Array();
        
        $list["CAD"] = PerisianLanguageVariable::getVariable('p59e7831c1ec67');
        $list["CHF"] = PerisianLanguageVariable::getVariable('p59e782f448230');
        $list["EUR"] = PerisianLanguageVariable::getVariable('p59e782c810daa');
        $list["GBP"] = PerisianLanguageVariable::getVariable('p59e783393b574');
        $list["RUB"] = PerisianLanguageVariable::getVariable('p59e7837db11e5');
        $list["USD"] = PerisianLanguageVariable::getVariable('p59e783081eb8f');
                
        return $list;
        
    }
    
    /**
     * Retrieves the displayable HTML symbol for the specified currency code.
     * For example, this will return "&euro;" for "EUR".
     * 
     * @author Peter Hamm
     * @param String $currencyCode
     * @return String
     */
    public static function getSymbolForCurrency($currencyCode)
    {
        
        $list = Array();
        
        $list["CAD"] = "CA&#36;";
        $list["CHF"] = "Fr.";
        $list["EUR"] = "&euro;";
        $list["GBP"] = "&pound;";
        $list["RUB"] = "&#x20bd;";
        $list["USD"] = "&#36;";
        
        return @$list[$currencyCode];
        
    }
    
    /**
     * Retrieves a list of all possible VAT rates.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getRatesVat()
    {
        
        $list = Array();
        
        for($i = 0; $i < 600; ++$i)
        {
            
            $percentageValue = "" . ($i / 10);
            
            $list[$percentageValue] = $percentageValue . " " . PerisianLanguageVariable::getVariable('p59e786a4dc54d');
                        
        }
        
        return $list;
        
    }
    
}