<?php

class PerisianPayPalItem
{
    
    public $title = "";
    public $description = "";
    public $price = 0.0;
    public $taxRate = 0.0;
    public $currencyCode = "";
    public $customIdentifier = "";
    public $type = "";
    
    private $details = "";
            
    /**
     * Calculates the gross price of the tax for this item based on the price and tax rate.
     * 
     * @author Peter Hamm
     * @return float
     */
    public function getPriceGross()
    {
        
        $priceGross = round($this->price * $this->taxRate, 2);
        
        return $priceGross;
        
    }
    
    /**
     * Calculates the price of the tax for this item based on the price and tax rate.
     * 
     * @author Peter Hamm
     * @return float
     */
    public function getPriceTax()
    {
        
        $priceTax = $this->getPriceGross() - $this->price;
        
        return $priceTax;
        
    }
    
    /**
     * Retrieves the details for this item.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getDetails()
    {
        
        return json_decode($this->details, true);
        
    }
    
    /**
     * Sets the details for this item.
     * 
     * @author Peter Hamm
     * @param Array $details
     * @return void
     */
    public function setDetails($details)
    {
        
        $this->details = json_encode($details);
        
    }
    
}
