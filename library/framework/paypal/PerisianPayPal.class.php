<?php

require_once 'framework/currency/PerisianCurrency.class.php';
require_once 'IPerisianPayPal.class.php';
require_once 'PerisianPayPalItem.class.php';

class PerisianPayPal implements IPerisianPayPal
{
    
    protected $apiContext = null;
    protected $item = null;
    
    public function __construct()
    {
                
    }
    
    /**
     * Retrieves a list of all possible VAT rates.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getRatesVat()
    {
                
        return PerisianCurrency::getRatesVat();
        
    }
    
    /**
     * Retrieves the complete list of supported currencies.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getCurrencies()
    {
                        
        return PerisianCurrency::getCurrencies();
        
    }
    
    /**
     * Retrieves the default currency code set for this module.
     * 
     * @author Peter Hamm
     * @return String A currency code, e.g. "EUR" or "USD".
     */
    public static function getDefaultCurrency()
    {
        
        return PerisianSystemSetting::getSettingValue('module_setting_paypal_currency');
        
    }
    
    /**
     * Retrieves the configured default VAT rate for this module, e.g. "1.19".
     * 
     * @author Peter Hamm
     * @return float
     */
    public static function getDefaultRateVat()
    {
        
        $percentage = (int)PerisianSystemSetting::getSettingValue('module_setting_paypal_rate_vat');
        
        $rate = 1 + ($percentage > 0 ? $percentage / 100 : 0);
        
        return $rate;
        
    }
    
    /**
     * Sets the PayPal item for the transaction.
     * 
     * @author Peter Hamm
     * @param PerisianPayPalItem $item
     * @return void
     */
    public function setItem(PerisianPayPalItem $item)
    {
        
        $this->item = $item;
        
    }
    
    /**
     * Queries PayPal to retrieve the URL to send the payment with the specified 
     * PayPal payment identifier.
     * 
     * @author Peter Hamm
     * @param String $payPalPaymentId
     * @return String
     */
    public function getPaymentUrlForPaymentId($payPalPaymentId)
    {
                    
        $apiContext = $this->getApiContext();
        
        $payment = Paypal\Api\Payment::get($payPalPaymentId, $apiContext);
        
        return $payment->getApprovalLink();
        
    }
    
    /**
     * Creates a payment that the user will then have to confirm on the PayPal website.
     * An item for this object must be set, else the transaction cannot be created and
     * will throw a PerisianException.
     * 
     * @author Peter Hamm
     * @return Array Data about the transaction.
     * @throws PerisianException
     */
    public function createPayment()
    {
        
        $result = Array(
            
            "message" => "",
            "payment" => "",
            "url" => "",
            "excepton" => null,
            "success" => false
            
        );
        
        if(is_null($this->item))
        {
            
            // "Invalid transaction item."
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p59f3a4ae4200e'));
            
        }
        
        $apiContext = $this->getApiContext();
        
        $payer = new PayPal\Api\Payer();
        $payer->setPaymentMethod("paypal");
                
        // Add the item
        {
        
            $payPalItem = new PayPal\Api\Item();
            $payPalItem->setName($this->item->title);
            $payPalItem->setCurrency($this->item->currencyCode);
            $payPalItem->setQuantity(1);
            $payPalItem->setSku($this->item->customIdentifier);
            $payPalItem->setPrice($this->item->price);

            $itemList = new PayPal\Api\ItemList();
            $itemList->setItems(array($payPalItem));
                    
        }
        
        // Set the payment amounts
        {
            
            $details = new PayPal\Api\Details();
            $details->setShipping(0);
            $details->setTax($this->item->getPriceTax());
            $details->setSubtotal($this->item->price);
            
            $amount = new PayPal\Api\Amount();
            $amount->setCurrency($this->item->currencyCode);
            $amount->setTotal($this->item->getPriceGross());
            $amount->setDetails($details);
            
        }
        
        // Define the transaction, callback and intent
        {
            
            $transaction = new PayPal\Api\Transaction();
            $transaction->setAmount($amount);
            $transaction->setItemList($itemList);
            $transaction->setDescription($this->item->description);
            //$transaction->setInvoiceNumber('Rechnungsnummer ' . uniqid());
            
            $baseUrl = PerisianFrameworkToolbox::getServerAddress(false) . 'payment/';
            
            $redirectUrls = new PayPal\Api\RedirectUrls();
            
            $redirectUrls->setReturnUrl($baseUrl . "?do=confirmPayment&t=paypal&success=true")->setCancelUrl($baseUrl . "?do=confirmPayment&t=paypal&success=false");
            
            $payment = new PayPal\Api\Payment();
            
            $payment->setIntent("sale");
            $payment->setPayer($payer);
            $payment->setRedirectUrls($redirectUrls);
            $payment->setTransactions(array($transaction));
            
            $request = clone $payment;
            
            $result["payment"] = $request;
            
        }
        
        try 
        {
                        
            $payment->create($apiContext);
            
            $this->logPayment($payment, $this->item);
            
            // "The payment was successfully created with PayPal. Please proceed to PayPal to approve."
            
            $result["message"] = PerisianLanguageVariable::getVariable('p59f3a79e40888');
            $result["url"] = $payment->getApprovalLink();
            $result['payment_id'] = $payment->getId();
            $result["success"] = true;
            
        } 
        catch(Exception $e) 
        {
            
            // "The payment was already created. Please follow the link to PayPal now to approve the payment."
            
            $message = PerisianLanguageVariable::getVariable('p59f3a80bb677c');
            
            if(is_a($e, "PayPal\Exception\PayPalConnectionException"))
            {
                
                // We can get detailed information from this
                                
                $detailedData = json_decode($e->getData(), true);
                
                if(isset($detailedData['details']))
                {
                    
                    $detailedIssues = Array();
                    
                    for($i = 0; $i < count($detailedData['details']); ++$i)
                    {
                        
                        array_push($detailedIssues, $detailedData['details'][$i]['issue']);
                        
                    }
                    
                    $message = implode(", ", $detailedIssues);
                    
                }
                
            }
            
            $result["message"] = $message;
            $result["exception"] = $e;
                        
        }

        return $result;
        
    }
    
    /**
     * Logs or updates the status of a PayPal payment to the database.
     * 
     * @author Peter Hamm
     * @param Paypal\Api\Payment $payment
     * @param PerisianPayPalItem $item Optional, default: null. Should be set when creating a new transaction.
     * @return void
     */
    protected function logPayment(Paypal\Api\Payment $payment, PerisianPayPalItem $item = null)
    {
                
        $transaction = PerisianPayPalTransaction::getTransactionByPayPalId($payment->getId());
        
        // Set the status
        {
        
            $payPalStatus = $payment->getState();
        
            $newStatus = 'canceled';
            
            if($payPalStatus == 'created')
            {
                
                $newStatus = 'pending';
                
            }
            else if($payPalStatus == 'approved')
            {
                
                $newStatus = 'done';
                
            }
            else if($payPalStatus == 'failed')
            {
                
                $newStatus = 'failed';
                
            }
            
            $transaction->{$transaction->field_status} = $newStatus;
            
        }
        
        if(!is_null($item))
        {
            
            $percentageTax = ($item->taxRate - 1) * 100;
            $percentageTax = $percentageTax <= 0 ? 0 : $percentageTax;
            
            $transaction->{$transaction->field_percentage_tax} = $percentageTax;
            $transaction->{$transaction->field_amount} = $item->price;
            $transaction->{$transaction->field_currency} = $item->currencyCode;
            $transaction->{$transaction->field_type} = $item->type;
            $transaction->{$transaction->field_fk} = $item->customIdentifier;
            
            $transaction->setDetails($item->getDetails());
            
        }
                
        $transaction->save();
        
    }
    
    /**
     * Loads a PerisianPayPalItem by the specified PayPal payment ID through
     * an existing logged transaction. Used for handling payment results.
     * 
     * @author Peter Hamm 
     * @param String $paymentId
     * @return void
     */
    private function loadItemByPaymentId($paymentId)
    {
        
        $transaction = PerisianPayPalTransaction::getTransactionByPayPalId($paymentId);
        
        $item = new PerisianPayPalItem();
        
        $item->currencyCode = $transaction->{$transaction->field_currency};
        $item->price = $transaction->{$transaction->field_amount};
        $item->taxRate = 1 + ($transaction->{$transaction->field_percentage_tax} / 100);
        $item->type = $transaction->{$transaction->field_type};
        
        $this->item = $item;
        
    }
    
    /**
     * Handles the callback from PayPal and the data transmitted in that step.
     * 
     * @author Peter Hamm
     * @param String $payerId
     * @param String $paymentId
     * @param bool $isSuccess Was the transaction a success or not?
     * @return Array Result information data.
     */
    public function handlePaymentResult($payerId, $paymentId, $isSuccess)
    {
        
        $result = Array(
            
            "message" => "",
            "payment" => "",
            "excepton" => null,
            "success" => false
            
        );
                
        $apiContext = $this->getApiContext();
        
        if($isSuccess) 
        {
                        
            // Pulling up data from the existing transaction
            {
                
                $this->loadItemByPaymentId($paymentId);
            
            }
            
            $payment = Paypal\Api\Payment::get($paymentId, $apiContext);
            
            $execution = new Paypal\Api\PaymentExecution();
            $execution->setPayerId($payerId);
            
            $transaction = new Paypal\Api\Transaction();
            $amount = new Paypal\Api\Amount();
            $details = new Paypal\Api\Details();

            // Set the amount
            {
                
                $details->setShipping(0);
                $details->setTax($this->item->getPriceTax());
                $details->setSubtotal($this->item->price);

                $amount->setCurrency($this->item->currencyCode);
                $amount->setTotal($this->item->getPriceGross());
                $amount->setDetails($details);
                
                $transaction->setAmount($amount);
            
            }
            
            $execution->addTransaction($transaction);
            
            try 
            {
                
                $transactionResult = $payment->execute($execution, $apiContext);
                
                try 
                {
                    
                    // Retrieve the payment from PayPal
                    $payment = Paypal\Api\Payment::get($paymentId, $apiContext);
                    
                } 
                catch(Exception $e) 
                {
                    
                    $result["message"] = "The payment could not be retrieved, possibly the ID is incorrect: " . $payment->getId();
                    $result["exception"] = $e;
                    $result["payment"] = $payment;
                    
                    return $result;
                   
                }
                
            } 
            catch(Exception $e) 
            {
                                
                $this->logPayment($payment);
                
                $this->onTransactionCompleteSuccessful($paymentId);
                
                $result["message"] = "The payment was already executed: " . $payment->getId();
                $result["success"] = true;
                $result["payment"] = $payment;

                return $result;
                
                
            }
                
            $this->logPayment($payment);
            
            $this->onTransactionCompleteSuccessful($paymentId);
            
            $result["message"] = "The payment was successfully received and authorized: " . $payment->getId();
            $result["success"] = true;
            $result["payment"] = $payment;
            
        } 
        else 
        {    
            
            $result["message"] = "The payment was canceled by the user.";
            
        }
        
        return $result;
        
    }
    
    /**
     * Fired at the moment when a transaction is successfully completed and
     * confirmed by PayPal. This can, for example, automatically confirm a calendar entry.
     * 
     * @author Peter Hamm
     * @param String $paymentId
     */
    private function onTransactionCompleteSuccessful($paymentId)
    {
                
        $transaction = PerisianPayPalTransaction::getTransactionByPayPalId($paymentId);
                
        if($transaction->{$transaction->field_type} == PerisianPayPalTransaction::TYPE_CALENDAR_ENTRY)
        {
                        
            // Confirm the calendar entry for the associated frontend user.
            
            try
            {
                
                $details = $transaction->getDetails();;
                
                $userFrontendDummy = new CalsyUserFrontend();
                
                $calendarEntryId = $transaction->{$transaction->field_fk};
                $userFrontendId = $details[$userFrontendDummy->field_pk];
                $userFrontendObject = new CalsyUserFrontend($userFrontendId);
                
                if($calendarEntryId > 0 && $userFrontendId > 0)
                {
                                    
                    $calendarEntry = new CalsyCalendarEntry($calendarEntryId);
                    $calendarEntry->confirmForUserFrontendWithPayment($userFrontendId);
                
                }
                
            }
            catch(PerisianException $e)
            {
                                
            }
            
        }
                
    }
    
    /**
     * Creates a PayPal API context with the credentials specified in the backend.
     * 
     * @author Peter Hamm
     * @url https://developer.paypal.com/webapps/developer/applications/myapps
     * @return void
     */
    protected function getApiContext()
    {
        
        if(is_null($this->apiContext))
        {
                        
            $isSandbox = PerisianSystemSetting::getSettingValue('module_setting_paypal_is_sandbox_mode_active') == 1;
            
            $clientId = PerisianSystemSetting::getSettingValue('module_setting_paypal_client_id_sandbox');
            $clientSecret = PerisianSystemSetting::getSettingValue('module_setting_paypal_client_secret_sandbox');
            
            $configuration = Array();
                            
            if(!$isSandbox)
            {
                                                
                $clientId = PerisianSystemSetting::getSettingValue('module_setting_paypal_client_id');
                $clientSecret = PerisianSystemSetting::getSettingValue('module_setting_paypal_client_secret');
                
                $configuration = Array(
                    
                    'mode' => 'live'
                    
                );
                
            }
            
            $credentials = new \PayPal\Auth\OAuthTokenCredential($clientId, $clientSecret);

            $this->apiContext = new \PayPal\Rest\ApiContext($credentials);
            
            $this->apiContext->setConfig($configuration);
        
        }
        
        return $this->apiContext;
        
    }
    
}