<?php

require_once 'external/Psr/Log/LogLevel.php';
require_once 'external/Psr/Log/LoggerInterface.php';
require_once 'external/Psr/Log/AbstractLogger.php';

require_once 'external/PayPal/Rest/IResource.php';
require_once 'external/PayPal/Common/PayPalModel.php';
require_once 'external/PayPal/Common/PayPalResourceModel.php';
require_once 'external/PayPal/Common/PayPalUserAgent.php';
require_once 'external/PayPal/Common/ArrayUtil.php';
require_once 'external/PayPal/Common/ReflectionUtil.php';
require_once 'external/PayPal/Cache/AuthorizationCache.php';
require_once 'external/PayPal/Exception/PayPalConnectionException.php';
require_once 'external/PayPal/Api/Amount.php';
require_once 'external/PayPal/Api/Details.php';
require_once 'external/PayPal/Api/Item.php';
require_once 'external/PayPal/Api/ItemList.php';
require_once 'external/PayPal/Api/Links.php';
require_once 'external/PayPal/Api/Payee.php';
require_once 'external/PayPal/Api/Payer.php';
require_once 'external/PayPal/Api/PayerInfo.php';
require_once 'external/PayPal/Api/Currency.php';
require_once 'external/PayPal/Api/Payment.php';
require_once 'external/PayPal/Api/PaymentExecution.php';
require_once 'external/PayPal/Api/PaymentInstruction.php';
require_once 'external/PayPal/Api/RecipientBankingInstruction.php';
require_once 'external/PayPal/Api/RedirectUrls.php';
require_once 'external/PayPal/Api/RelatedResources.php';
require_once 'external/PayPal/Api/Sale.php';
require_once 'external/PayPal/Api/BaseAddress.php';
require_once 'external/PayPal/Api/Address.php';
require_once 'external/PayPal/Api/ShippingAddress.php';
require_once 'external/PayPal/Api/CartBase.php';
require_once 'external/PayPal/Api/TransactionBase.php';
require_once 'external/PayPal/Api/Transaction.php';
require_once 'external/PayPal/Auth/OAuthTokenCredential.php';
require_once 'external/PayPal/Security/Cipher.php';
require_once 'external/PayPal/Rest/ApiContext.php';
require_once 'external/PayPal/Transport/PayPalRestCall.php';
require_once 'external/PayPal/Core/PayPalConstants.php';
require_once 'external/PayPal/Core/PayPalHttpConnection.php';
require_once 'external/PayPal/Core/PayPalLoggingManager.php';
require_once 'external/PayPal/Core/PayPalConfigManager.php';
require_once 'external/PayPal/Core/PayPalHttpConfig.php';
require_once 'external/PayPal/Log/PayPalLogger.php';
require_once 'external/PayPal/Log/PayPalLogFactory.php';
require_once 'external/PayPal/Log/PayPalDefaultLogFactory.php';
require_once 'external/PayPal/Validation/NumericValidator.php';
require_once 'external/PayPal/Validation/UrlValidator.php';
require_once 'external/PayPal/Validation/ArgumentValidator.php';
require_once 'external/PayPal/Converter/FormatConverter.php';
require_once 'external/PayPal/Handler/IPayPalHandler.php';
require_once 'external/PayPal/Handler/RestHandler.php';
require_once 'external/PayPal/Handler/OauthHandler.php';

require_once "PerisianPayPalTransaction.class.php";

interface IPerisianPayPal
{
        
}
