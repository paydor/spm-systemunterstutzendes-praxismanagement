<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/PerisianPayPalModule.class.php';

/**
 * PayPal Controller
 *
 * @author Peter Hamm
 * @date 2017-10-17
 */
class PerisianPayPalController extends PerisianController
{
    
    /**
     * Provides an overview of transaction entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'paypal/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of transaction entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new PerisianPayPalTransaction();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'DESC');
        $sorting = $this->getParameter('sorting', $dummy->field_pk);
        $search = $this->getParameter('search');
        
        // Filter setup
        {
            
            $filterUserFrontendId = $this->getParameter('p') > 0 ? $this->getParameter('p') : 0;
            $filterUserFrontend = new CalsyUserFrontend($filterUserFrontendId);
            
            $filter = PerisianPayPalTransaction::createFilter($filterUserFrontendId);
            
        }
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'paypal/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => PerisianPayPalTransaction::getTransactionList($filter, $search, $sorting, $sortOrder, $offset, $limit),
            'count' => PerisianPayPalTransaction::getTransactionCount($filter, $search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search,
            
            'filter_user_frontend_id' => $filterUserFrontendId,
            'filter_user_frontend' => $filterUserFrontend
            
        );
        
        $this->setResultValue('result', $result);
                
    }
        
    /**
     * Retrieves JSON data for the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionGetJson()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $source = $this->getParameter('source');
        
        $data = array();
        
        if($source == "user_frontend")
        {
            
            $data = CalsyUserFrontend::getUserFrontendList();
            
        }
        
        $result = array(
            
            'data' => $data
            
        );
        
        $this->setResultValue('result', $result);
                
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}