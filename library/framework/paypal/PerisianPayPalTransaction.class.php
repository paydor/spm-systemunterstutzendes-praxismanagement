<?php

require_once 'framework/payment/IPerisianPayment.class.php';

class PerisianPayPalTransaction extends PerisianDatabaseModel implements IPerisianPayment
{
    
    const TYPE_CALENDAR_ENTRY = 'calendar_entry';
    
    // Main table settings
    public $table = 'paypal_tx';
    
    public $field_pk = 'paypal_tx_id';
    public $field_fk = 'paypal_tx_foreign_id';
    public $field_paypal_id = 'paypal_tx_paypal_id';
    public $field_type = 'paypal_tx_type';
    public $field_details = 'paypal_tx_details';
    public $field_status = 'paypal_tx_status';
    public $field_amount = 'paypal_tx_amount';
    public $field_percentage_tax = 'paypal_tx_percentage_tax';
    public $field_currency = 'paypal_tx_currency';
    public $field_date_created = 'paypal_tx_date_created';
    public $field_date_updated = 'paypal_tx_date_updated';
    
    protected static $instance = null;
    
    /**
     * Retrieves the name of the transaction type.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionName()
    {
        
        return 'paypal';
        
    }
    
    /**
     * Retrieves a nicely formatted name of the transaction type.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionNameFormatted()
    {
        
        return PerisianLanguageVariable::getVariable('p59dde99bc7d89');
        
    }
    
    /**
     * Retrieves the timestamp when the transaction wascreated
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionDateCreated()
    {
        
        return $this->{$this->field_date_updated};
        
    }
    
    /**
     * Retrieves the timestamp when the transaction was last updated
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionDateUpdated()
    {
        
        return $this->{$this->field_date_updated};
        
    }
    
    /**
     * Retrieves the name of the field for the transaction identifier.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getFieldTransactionId()
    {
        
        return $this->field_paypal_id;
        
    }
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId) && $entryId > 0)
        {
            
            $query = "{$this->field_pk} = '{$entryId}'";
            $this->loadData($query);

        }

        return;

    }
     
    /**
     * Retrieves whether the payment for this transaction was retrieved or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isPaymentReceived()
    {
        
        $paymentReceived = $this->{$this->field_status} == 'done';
        
        return $paymentReceived;
        
    }
     
    /**
     * Retrieves whether the payment for this transaction was pending or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isPaymentPending()
    {
        
        $paymentReceived = $this->{$this->field_status} == 'pending';
        
        return $paymentReceived;
        
    }
     
    /**
     * Retrieves whether the payment for this transaction was canceled or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isPaymentCanceled()
    {
        
        $paymentReceived = $this->{$this->field_status} == 'canceled';
        
        return $paymentReceived;
        
    }
    
     
    /**
     * Retrieves whether the payment for this transaction was invalid or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public function isPaymentInvalid()
    {
        
        $paymentReceived = $this->{$this->field_status} == 'invalid';
        
        return $paymentReceived;
        
    }
    
    /**
     * Checks whether a payment was received with the specified parameters or not.
     * 
     * @author Peter Hamm
     * @param String $transactionType
     * @param String $foreignKey
     * @param String $detailKey Optional, a key contained in the details field. Default: Empty
     * @param String $detailValue Optional, a value for the key contained in the details field. Default: Empty
     * @return bool
     */
    public static function isPaymentReceivedForTransaction($transactionType, $foreignKey, $detailKey = "", $detailValue = "")
    {
        
        $object = static::getInstance();
        
        $query = $object->field_fk . ' = "' . $foreignKey . '" AND ';
        $query .= $object->field_details . ' LIKE \'%"' . $detailKey . '":"' . $detailValue . '"%\' AND ';
        $query .= $object->field_status . ' = "done"';
                
        $count = $object->getCount($query);
        
        return $count > 0;
        
    }
    
    /**
     * If it exists, the transaction with the specified parameters is retrieved.
     * 
     * @author Peter Hamm
     * @param String $transactionType
     * @param String $foreignKey
     * @param String $detailKey Optional, a key contained in the details field. Default: Empty
     * @param String $detailValue Optional, a value for the key contained in the details field. Default: Empty
     * @return static
     */
    public static function getTransaction($transactionType, $foreignKey, $detailKey = "", $detailValue = "")
    {
        
        $object = static::getInstance();
        
        $query = $object->field_fk . ' = "' . $foreignKey . '" AND ';
        $query .= $object->field_details . ' LIKE \'%"' . $detailKey . '":"' . $detailValue . '"%\'';
                
        $data = $object->getData($query);
        
        if(count($data) > 0)
        {
            
            return new static($data[0][$object->field_pk]);
            
        }
        
        return null;
        
    }
    
    /**
     * Singleton instance retriever
     * 
     * @author Peter Hamm
     * @return static
     */
    protected function getInstance()
    {
        
        if(is_null(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
     
    }
    
    /**
     * Takes an encoded details String and decodes it into an associative Array.
     * 
     * @author Peter Hamm
     * @param String $details
     * @return Array
     */
    public static function decodeDetails($detailString)
    {
        
        $details = Array();
        
        $dummy = static::getInstance();
        
        if(isset($detailString) && strlen($detailString) > 2)
        {
            
            $details = json_decode($detailString, true);
            
        }
        
        return $details;
        
    }
    
    /**
     * Retrieves this entry's details as an associative Array.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getDetails()
    {
        
        $details = static::decodeDetails($this->{$this->field_details});
                
        return $details;
        
    }
    
    /**
     * Sets the details for this entry.
     * 
     * @author Peter Hamm
     * @param Array $details
     * @return void
     */
    public function setDetails($details)
    {
        
        if(!is_array($details))
        {
            
            $details = Array(
                
                'description' => $details
                
            );
            
        }
                
        $this->{$this->field_details} = json_encode($details);
        
    }
    
    /**
     * Retrieves a transaction entry by their unique PayPal identifier.
     * Will create a new one if it doesn't exist yet.
     * 
     * @author Peter Hamm
     * @param type $payPalId
     * @return \static
     */
    public static function getTransactionByPayPalId($payPalId)
    {
        
        $payPalId = PerisianFrameworkToolbox::security($payPalId);
        
        $object = new static();
        
        $object->{$object->field_paypal_id} = $payPalId;
        
        try 
        {
            
            $query = $object->field_paypal_id . '="' . $payPalId . '"';
            
            $object->loadData($query);
            
            $object->{$object->field_date_updated} = PerisianFrameworkToolbox::sqlTimestamp();
            
        } 
        catch(Exception $e) 
        {
             
            // No existing entry yet, just go on with the new one.
            
            $object->{$object->field_date_created} = PerisianFrameworkToolbox::sqlTimestamp();
            
        }
        
        return $object;
        
    }
    
    /**
     * Retrieves a nicely formatted string for the status specified.
     * 
     * @author Peter Hamm
     * @param String $status The status of a transaction entry, e.g. "pending", "done", "canceled" or "invalid".
     * @param bool $asHtml Optional, whether to include text formatting for HTML or not. Default: false.
     * @return String
     */
    public static function getStatusString($status, $asHtml = false)
    {
        
        $statusString = "";
        $statusClass = "none";
        
        if($status == 'pending')
        {
            
            $statusString = PerisianLanguageVariable::getVariable('11167');
            $statusClass = $status;
            
        }
        else if($status == 'done')
        {
            
            $statusString = PerisianLanguageVariable::getVariable('p59e66fa0b3db5');
            $statusClass = $status;
            
        }
        else if($status == 'canceled')
        {
            
            $statusString = PerisianLanguageVariable::getVariable('p59e66fb9185f4');
            $statusClass = $status;
            
        }
        else if($status == 'invalid')
        {
            
            $statusString = PerisianLanguageVariable::getVariable('p59e66faf1f97b');
            $statusClass = $status;
            
        }
        
        if($asHtml)
        {
            
            $statusString = '<span class="paypal-status-' . $statusClass . '">' . $statusString . "</span>";
            
        }
        
        return $statusString;
        
    }
    
    /**
     * Retrieves a link to the database entry with the specified foreign identifier and type.
     * 
     * @author Peter Hamm
     * @param String $type
     * @param int $foreignId
     * @return string
     */
    public static function getLinkForType($type, $foreignId)
    {
        
        $link = "#";
        
        if($type == static::TYPE_CALENDAR_ENTRY)
        {
            
            $link = PerisianFrameworkToolbox::getServerAddress() . 'event/overview/?se=' . $foreignId;
            
        }
        
        return $link;
        
    }
    
    /**
     * Retrieves a nicely formatted string for the type specified.
     * 
     * @author Peter Hamm
     * @param String $type One of the type constants of this class.
     * @return String
     */
    public static function getTypeString($type)
    {
        
        $typeString = "";
        
        if($type == static::TYPE_CALENDAR_ENTRY)
        {
            
            $typeString = PerisianLanguageVariable::getVariable('p59e671ca22b5a');
            
        }
        
        return $typeString;
        
    }
    
    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific entry
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {
        
        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }    
    
    
    /**
     * Generates an associative array out of the filter IDs specified.
     * Can later be used to filter lists by the parameters specified.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return array
     */
    public static function createFilter($userFrontendId = 0)
    {
        
        $filter = array();
        
        $filter['userFrontendId'] = PerisianFrameworkToolbox::security($userFrontendId);
        
        return $filter;
        
    }
    
    /**
     * Retrieves a list of all transactions for the specified filter
     * 
     * @author Peter Hamm
     * @param array $filter A filter array generated by self::createfilter();
     * @param String $search
     * @param int $orderBy
     * @param int $order
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public static function getTransactionList($filter = array(), $search = "", $orderBy = "pk", $order = "ASC", $offset = 0, $limit = 0)
    {
        
        $instance = new self();
                
        $query = $instance->buildSearchString($filter, $search);
        
        if($orderBy == "pk")
        {
            
            $orderBy = $instance->field_pk;
            
        }
                
        $results = $instance->getData($query, $orderBy, $order, $offset, $limit);
        
        return $results;
        
    }
    
    /**
     * Retrieves the count of all transactions for the specified filter
     * 
     * @author Peter Hamm
     * @param array $filter A filter array generated by self::createfilter();
     * @param String $search
     * @return array
     */
    public static function getTransactionCount($filter = array(), $search = "")
    {
                
        $instance = new self();
                
        $query = $instance->buildSearchString($filter, $search);
                
        $results = $instance->getCount($query, $instance->field_pk);
        
        return $results;
        
    }
    
    /**
     * Retrieves the VAT rate in the format as of e.g. "1.19".
     * 
     * @author Peter Hamm
     * @return float
     */
    public function getRateVat()
    {
         
        $rate = 1 + ($this->{$this->field_percentage_tax} > 0 ? ($this->{$this->field_percentage_tax} / 100) : 0);
        
        return $rate;
        
    }
    
    /**
     * Retrieves the formatted net price amount as a String.
     * 
     * @author Peter Hamm
     * @return float 
     */
    public function getPriceAmountFormattedNet()
    {
        
        return CalsyAreaPrice::formatPriceAmount($this->{$this->field_amount});
        
    }
    
    /**
     * Retrieves the formatted gross price amount as a String.
     * 
     * @author Peter Hamm
     * @return String 
     */
    public function getPriceAmountFormattedGross()
    {
        
        $price = $this->{$this->field_amount};
        
        $price *= $this->getRateVat();
        
        return CalsyAreaPrice::formatPriceAmount($price);
        
    }
    
    /**
     * Builds a search string with the specified parameters.
     * 
     * @author Peter Hamm
     * @param array $filter
     * @param String $search
     * @return string
     */
    public function buildSearchString($filter, $search)
    {

        PerisianFrameworkToolbox::security($search);
        
        $query = parent::buildSearchString($search);
        
        if(@$filter["userFrontendId"] > 0)
        {
            
            /*
            if(strlen($query) > 0)
            {

                $query = "(" . $query . ") AND ";

            }
            
            $query .= $this->field_fk . " = '" . @$filter["userFrontendId"] . "'";
            */
            
        }
                
        return $query;
        
    }
    
}
