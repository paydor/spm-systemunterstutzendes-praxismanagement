<?php

require_once 'database/PerisianDatabaseModel.class.php';

/**
 * This class handles the system's languages
 *
 * @author Peter Hamm
 * @date 2010-11-11
 */
class PerisianLanguage extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'language';
    public $field_pk = 'language_id';

    // Fields
    public $field_code = 'language_code';
    public $field_name = 'language_name';

    // Instance
    static private $instance = null;

    /**
     * This may get called internally by self::getInstance or externally when
     * editing a specific language
     *
     * @param int $languageId Optional: If specified, the data of that language is being loaded to the object
     * @author Peter Hamm
     * @return void
     */
    public function __construct($languageId = 0)
    {

        parent::__construct('MySql', 'main');

        if(!empty($languageId))
        {

            $query = "{$this->pk} = {$languageeId}";
            $result = $this->getData($languageId);

            // @todo: Save the data to this object

        }

    }
    
    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @return Object
     */
    static public function getInstance()
    {

        if(self::$instance === null)
        {
            self::$instance = new self;
        }

        return self::$instance;

    }

    /**
     * Returns a list of the system's languages
     *
     * @author Peter Hamm
     * @return Array The languages
     */
    public static function getLanguages()
    {

        $instance = self::getInstance();
        
        return $instance->getData('', 'pk', 'ASC');
        
    }
    
    /**
     * Returns a list of the system's languages that are allowed in the frontend
     *
     * @author Peter Hamm
     * @return Array The languages
     */
    public static function getFrontendLanguages()
    {

        $filteredLanguages = array();
        
        $languageDummy = new PerisianLanguage();
        $systemSetting = new PerisianSystemSetting('frontend_languages_available');
        
        $allLanguages = static::getLanguages();
        $selectedLanguages = $systemSetting->getValue();
        
        for($i = 0; $i < count($allLanguages); ++$i)
        {
            
            if(in_array($allLanguages[$i][$languageDummy->field_pk], $selectedLanguages))
            {
                
                array_push($filteredLanguages, $allLanguages[$i]);
                
            }
            
        }
        
        if(count($filteredLanguages) == 0)
        {
            
            array_push($filteredLanguages, $allLanguages[0]);
            
        }
        
        return $filteredLanguages;
        
    }

    /**
     * Retrieves the name for the specified language id
     *
     * @author Peter Hamm
     * @param integer $languageId A language id
     * @return String The name
     */
    public static function getLanguageName($languageId)
    {

        PerisianFrameworkToolbox::security($languageId);

        $instance = self::getInstance();
        $query = "{$instance->field_pk} = {$languageId}";
        $resultArray = $instance->getData($query);

        if(isset($resultArray[0][$instance->field_name]))
        {
            $result = $resultArray[0][$instance->field_name];
        }
        else
        {
            $result = '';
        }

        return $result;
        
    }
    
    /**
     * Retrieves the identifier for the specified language code
     *
     * @author Peter Hamm
     * @param integer $languageCode A language code, e.g. 'en' or 'de'
     * @return String The identiier
     */
    public static function getLanguageIdentifier($languageCode)
    {

        PerisianFrameworkToolbox::security($languageCode);
        
        $languageCode = strtolower($languageCode);

        $instance = self::getInstance();
        $query = "{$instance->field_code} = '{$languageCode}'";
        $resultArray = $instance->getData($query);

        if(isset($resultArray[0][$instance->field_pk]))
        {
            
            $result = $resultArray[0][$instance->field_pk];
            
        }
        else
        {
            
            $result = 0;
            
        }

        return $result;
        
    }

    /**
     * Retrieves the code for the specified language id
     *
     * @author Peter Hamm
     * @param integer $languageId A language id
     * @return String The code, e.g. "en" or "de"
     */
    public static function getLanguageCode($languageId)
    {

        PerisianFrameworkToolbox::security($languageId);
        
        if(!is_int($languageId))
        {
            
            $languageId = 2;
            
        }

        $instance = self::getInstance();
        $query = "{$instance->field_pk} = {$languageId}";
        $resultArray = $instance->getData($query);

        if(isset($resultArray[0][$instance->field_code]))
        {
            
            $result = $resultArray[0][$instance->field_code];
            
        }
        else
        {
            
            $result = '';
            
        }

        return $result;
        
    }

}
