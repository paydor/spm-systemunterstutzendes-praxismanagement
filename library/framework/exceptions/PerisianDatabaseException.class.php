<?php

/**
 * Enhanced exception class to handle database errors
 *
 * @author Peter Hamm
 * @date 2010-10-21
 * @todo
 */
class PerisianDatabaseException extends PerisianException
{
    
    protected $sqlError = null;
    protected $sqlErrorNumber = null;
    protected $sqlErrorLine = null;
    protected $sqlErrorQuery = null;

    /**
     * If debug mode is activated, the query will be printed
     * with the error message.
     *
     * @author Peter Hamm
     * @param String $message Error message
     * @param String $query Database query string
     * @return Object
     */
    public function __construct($message, $query)
    {

        if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
        {

            if(empty($query))
            {
                
                $query = 'No query specified!';
                
            }

            $message = implode(' ', Array($message, 'Debug-Query: ' . $query));
            
        }

        return parent::__construct($message);

    }
    
    public function getSqlError()
    {
        
        return $this->sqlError;
        
    }
    
    public function setSqlError($error)
    {
        
        $this->sqlError = $error;
        
    }
    
    public function getSqlErrorNumber()
    {
        
        return $this->sqlErrorNumber;
        
    }
    
    public function setSqlErrorNumber($errorNumber)
    {
        
        $this->sqlErrorNumber = $errorNumber;
        
    }
    
    public function getSqlErrorLine()
    {
        
        return $this->sqlErrorLine;
        
    }
    
    public function setSqlErrorLine($errorLine)
    {
        
        $this->sqlErrorLine = $errorLine;
        
    }
    
    public function getSqlErrorQuery()
    {
        
        return $this->sqlErrorQuery;
        
    }
    
    public function setSqlErrorQuery($errorQuery)
    {
        
        $this->sqlErrorQuery = $errorQuery;
        
    }

}
