<?php

/**
 * Enhanced exception class to handle database errors
 *
 * @author Peter Hamm
 * @date 2016-10-20
 */
class PerisianDatabaseMigrationException extends PerisianException
{
    
    const FLAG_UPDATE_REQUIRED = 'flag_update_required';
    
    protected $flag = '';

    /**
     * Exception for database migration problems
     *
     * @author Peter Hamm
     * @param String $message Error message
     * @param String $flag A flag to categorize this exception.
     * @return Object
     */
    public function __construct($flag)
    {
                
        $this->flag = $flag;
        
        $message = "";
        
        if($flag == static::FLAG_UPDATE_REQUIRED)
        {
                        
            $message = PerisianLanguageVariable::getVariable('p5808ca8e088ef');
                        
        }

        return parent::__construct($message);

    }
    
    /**
     * Retrieves the flag of this exception.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getFlag()
    {
        
        return $this->flag;
        
    }

}
