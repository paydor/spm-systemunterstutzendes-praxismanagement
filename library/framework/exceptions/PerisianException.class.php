<?php

require_once 'PerisianDatabaseException.class.php';
require_once 'PerisianDatabaseMigrationException.class.php';

class PerisianSystemInitializationException extends Exception
{
    
}

/**
 * Enhanced exception class for advanced error handling
 *
 * @author Peter Hamm
 * @date 2010-10-21
 * @todo
 */
class PerisianException extends Exception
{

    const CODE_ERROR_MIGRATION = 'error_migration';
    
    protected $debugInfo = '';
    protected $backTrace = null;
    protected $errorCode = '';

    /**
     * @author Peter Hamm
     * @param String $message Error message
     * @param String $debugInfo Optional: Additional debug information, only display in debug mode. Default: empty
     * @param String $errorCode Optional, specify a custom error code. Default: empty string
     * @return Object
     */
    public function __construct($message, $debugInfo = '', $errorCode = '')
    {

        PerisianFrameworkToolbox::security($debugInfo);
        
        $this->debugInfo = $debugInfo;
        $this->backTrace = debug_backtrace();
        $this->errorCode = $errorCode;

        return parent::__construct($message);
        
    }

    /**
     * Returns the filename and line where the error occured.
     *
     * @author Peter Hamm
     * @return String The filename where the error occured
     */
    public function getPosition()
    {

        $fileName = parent::getFile();
        $fileName = str_replace(PerisianFrameworkToolbox::getConfig('basic/project/folder'), '', $fileName);

        $line = $this->getLine();
        $position = $fileName . ', ' . PerisianLanguageVariable::getVariable(10114) . ' ' . $line;

        return $position;

    }
    
    /**
     * Retrieves the error code of this exception (it's optional, so it might not be set.).
     *
     * @author Peter Hamm
     * @return String
     */
    public function getErrorCode()
    {

        return $this->errorCode;

    }

    /**
     * Retrieves the call stack of this exception.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getStack()
    {

        $stack = $this->getTraceAsString();
        $stack = str_replace(PerisianFrameworkToolbox::getConfig('basic/project/folder'), '', $stack);
        $stack = str_replace('#', "\n#", $stack);

        return $stack;

    }

    /**
     * If the system is in debug mode, this method returns additional
     * debug information, if it was provided.
     *
     * @author Peter Hamm
     * @return String Debug info
     */
    public function getDebugInfo()
    {

        if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
        {
            
            return $this->debugInfo;
            
        }

        return '';

    }
    
    /**
     * If the system is in debug mode, this method returns backtrace information
     * to where the exception came from.
     *
     * @author Peter Hamm
     * @return Array
     */
    public function getBackTrace()
    {

        if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
        {
            
            return $this->backTrace;
            
        }

        return '';

    }

}
