<?php

require_once 'abstract/PerisianSettingAbstract.class.php';

/**
 * Enhanced setting class
 * @author Peter Hamm
 * @date 2010-10-25
 */
class PerisianDependentSetting extends PerisianSettingAbstract
{

    // Set this to the name of the field that contains the dependent IDs
    public $field_setting_dependency = '';

    // Set this if you have got a specific foreign ID via $this->setDependency()
    private $dependency_id = 0;

    /**
     * Optionally loads the setting data for the specified $settingId
     *
     * @author Peter Hamm
     * @parameter String $settingIdOrName Optional: The ID of a setting
     * @parameter int $dependencyId The Optional: ID of a foreign subject
     */
    public function __construct($settingIdOrName = 0, $dependencyId = 0)
    {

        parent::__construct();

        if(strlen($settingIdOrName) > 0 && $dependencyId > 0)
        {
            
            $settingSelector = static::getSettingSelector($settingIdOrName);

            $this->setDependency($dependencyId);
            $this->getSetting($dependencyId, $settingIdOrName);
            
            $this->{$this->field_fk} = $settingSelector['id'];

            if(isset($this->settings[$settingSelector['id']]))
            {
                
                $fieldData = $this->settings[$settingSelector['id']];

                foreach($fieldData as $key => $value)
                {
                    
                    $this->{$key} = $value;
                    
                }
                
            }

        }

        $this->handleLoadedValue();
        
    }
    
    /**
     * Retrieves the dependency for the specified setting/value combination.
     * 
     * @author Peter Hamm
     * @param mixed $settingIdOrName
     * @param mixed $settingValue
     * @return Array
     */
    public static function getDependencyForSettingValue($settingIdOrName, $settingValue)
    {
        
        PerisianFrameworkToolbox::security($settingIdOrName);
        PerisianFrameworkToolbox::security($settingValue);
        
        $resultData = Array();
            
        $setting = new PerisianSetting('', $settingIdOrName);
            
        $object = new static();
        
        $query = $object->field_fk . ' = "' . $setting->{$setting->field_pk} . '" AND ' . $object->field_setting_value . ' = "' . $settingValue . '"';
                        
        $result = $object->getData($query);
        
        for($i = 0; $i < count($result); ++$i)
        {
            
            array_push($resultData, $result[$i][$object->field_setting_dependency]);
            
        }
        
        return $resultData;
        
    }
    
    /**
     * Retrieves all the dependencies for where the specified setting, 
     * where the value is not the specified input. Caution: 
     * Will only return back dependencies that have this setting stored in the database.
     * 
     * @author Peter Hamm
     * @param mixed $settingIdOrName
     * @param mixed $settingValue
     * @return Array
     */
    public static function getDependencyForSettingValueIsNot($settingIdOrName, $settingValue)
    {
        
        PerisianFrameworkToolbox::security($settingIdOrName);
        PerisianFrameworkToolbox::security($settingValue);
        
        $resultData = Array();
            
        $setting = new PerisianSetting('', $settingIdOrName);
            
        $object = new static();
        
        $query = $object->field_fk . ' = "' . $setting->{$setting->field_pk} . '" AND ' . $object->field_setting_value . ' != "' . $settingValue . '"';
                        
        $result = $object->getData($query);
        
        for($i = 0; $i < count($result); ++$i)
        {
            
            array_push($resultData, $result[$i][$object->field_setting_dependency]);
            
        }
        
        return $resultData;
        
    }
    
    /**
     * Sets the value for this setting
     * 
     * @author Peter Hamm
     * @param String $value
     */
    public function setValue($value)
    {
        
        $this->{$this->field_setting_value} = PerisianFrameworkToolbox::security($value);
        
    }

    /**
     * Overwritten method to additionally handle the dependency
     *
     * @author Peter Hamm
     * @param integer $dependencyId Optional: The ID of a dependency, default: Use the class member value
     * @param String $settingIdOrName Optional: Identigier or name of a specific setting, default: empty
     * @return mixed If a setting's ID is specified: The setting's value as a string - else: An array of all settings with that dependency
     */
    public function getSetting($dependencyId = 0, $settingIdOrName = '')
    {

        $settingSelector = static::getSettingSelector($settingIdOrName);
        
        $dependencyId = (int)$dependencyId;

        if($settingSelector['id'] == 0)
        {
            
            $settingSelector['id'] = $this->dependency_id;
            
        }
        
        if($dependencyId == 0)
        {
            
            $dependencyId = @$this->{$this->field_setting_dependency};
            
        }

        if(empty($settingSelector['id']) or strlen($this->field_setting_dependency) == 0)
        {
            
            throw new PerisianException('Dependency definition deficient.');
            
        }

        $extendedQuery = "{$this->field_setting_dependency} = '{$dependencyId}'";
        
        return parent::getSetting($settingIdOrName, $extendedQuery);

    }
    
    /**
     * Gets the value of the specified setting.
     * 
     * @author Peter Hamm
     * @param int $dependencyId
     * @param String $settingIdOrName Either a setting identifier or a setting name
     * @param String $value
     * @return int
     */
    public static function getSettingValue($dependencyId, $settingIdOrName)
    {
        
        PerisianFrameworkToolbox::security($value);
         
        $setting = new static($settingIdOrName, $dependencyId);
                                
        return $setting->getValue();
        
    }
    
    /**
     * Sets the value for the specified setting.
     * 
     * @author Peter Hamm
     * @param int $dependencyId
     * @param String $settingIdOrName Either a setting identifier or a setting name
     * @param String $value
     * @return int
     */
    public static function setSettingValue($dependencyId, $settingIdOrName, $value)
    {
        
        PerisianFrameworkToolbox::security($value);
         
        $setting = new static($settingIdOrName, $dependencyId);
        
        $setting->setValue($value);
        
        return $setting->save();
        
    }

    /**
     * Returns an associative array of this item's settings.
     *
     * The array keys are setting IDs, values are values.
     *
     * @author Peter Hamm
     * @return Array
     */
    public function getAssociativeValues()
    {

        $data = $this->getSetting();
        $result = Array();

        for($i = 0, $m = count($data); $i < $m; ++$i)
        {
            $result[$data[$i][$this->foreign_field_pk]] = $data[$i][$this->field_setting_value];
        }

        return $result;

    }

    /**
     * Sets the current dependency to the specified ID.
     *
     * @author Peter Hamm
     * @param int $id A foreign ID
     * @return void
     */
    public function setDependency($id)
    {

        if(!PerisianValidation::checkId($id))
        {
            return;
        }

        $this->{$this->field_setting_dependency} = $id;
        $this->dependency_id = $id;

    }
    
    /**
     * Saves the setting.
     * 
     * @author Peter Hamm
     * @param Array $saveFields
     * @return mixed
     */
    public function save($saveFields = Array())
    {
        
        if(is_null($this->{$this->field_setting_value}) || $this->{$this->field_setting_value} === false)
        {
            
            $this->{$this->field_setting_value} = 0;
            
        }
        else if($this->{$this->field_setting_value} === true)
        {
            
            $this->{$this->field_setting_value} = 1;
            
        }
        
        return parent::save($saveFields);
        
    }

}
