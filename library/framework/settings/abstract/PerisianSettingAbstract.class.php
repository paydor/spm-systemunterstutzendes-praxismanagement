<?php

require_once 'framework/settings/PerisianSetting.class.php';

/**
 * Abstract class to handle setting changes.
 *
 * You may extend this class for further settings in other, different areas
 *
 * @author Peter Hamm
 * @uses PerisianSetting
 * @data 2010-10-25
 */
abstract class PerisianSettingAbstract extends PerisianDatabaseModel
{
    
    const CODE_EMPTY_STRING = '_=_empty_=_';

    // Basic values, to be set for the specific settings
    public $table = '';
    public $field_pk = '';
    public $field_fk = '';
    public $field_setting_value = '';

    // Main setting table
    public $foreign_table = 'setting';
    public $foreign_field_pk = 'setting_id';
    public $foreign_field_name = 'setting_name';
    public $foreign_field_title = 'setting_title';
    public $foreign_field_description = 'setting_description';
    public $foreign_field_type = 'setting_type';
    public $foreign_field_fieldtype = 'setting_fieldtype';

    // Set this to a specific name, e.g. 'system' or 'user' to define
    // which rights shall be loaded!
    public $setting_name = '';

    // Do not create additional members depending on
    // the specified '$field_X' variables?
    protected $create_members = false;

    protected $settings = Array();
    
    protected static $listIdentifiersByName = Array();
    
    /**
     * Optionally loads the setting data for the specified $settingId
     *
     * @author Peter Hamm
     * @parameter String $settingIdOrName Optional: Identifier or name of a setting
     * @return void
     */
    public function __construct($settingIdOrName = '')
    {

        parent::__construct();
        
        $settingSelector = static::getSettingSelector($settingIdOrName);
                
        if($settingSelector['id'] == 0 && strlen($settingSelector['name']) > 0)
        {
            
            return;
            
            //throw new PerisianException("Could not retrieve the identifier for the setting \"" . $settingSelector['name'] . "\"");
            
        }
                
        // Make sure it is loaded, either by cache or from the database
        if(strlen($settingIdOrName) > 0)
        {
                  
            $this->getSetting($settingIdOrName);
            
        }

        if($settingSelector['id'] > 0)
        {

            if(strlen($this->field_fk) > 0)
            {
                
                $this->{$this->foreign_field_pk} = $settingSelector['id'];
                $this->{$this->field_fk} = $settingSelector['id'];
                
                if(isset($this->settings[$settingSelector['id']]))
                {
                    
                    $fieldData = $this->settings[$settingSelector['id']];
                    
                    foreach($fieldData as $key => $value)
                    {
                        
                        $this->{$key} = $value;
                        
                    }

                }

            }

        }
        
        $this->handleLoadedValue();

    }
    
    /**
     * Handles the loaded values, e.g. decrypts passwords or encrypted texts.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function handleLoadedValue()
    {
        
        if(@$this->{$this->foreign_field_fieldtype} == 'text-encrypted' || @$this->{$this->foreign_field_fieldtype} == 'password')
        {
            if(strlen($this->{$this->field_setting_value}) > 0)
            {
                            
                $this->{$this->field_setting_value} = PerisianSetting::decryptString($this->{$this->field_setting_value});
                
            }
            
        }
        
    }
    
    /**
     * Retrieves a setting selector from the specified parameter.
     * 
     * @author Peter Hamm
     * @param String $settingIdOrName A setting identifier or name/key.
     * @return Array Has the keys 'id' and 'name
     */
    public static function getSettingSelector($settingIdOrName)
    {
        
        $requestedSettingName = '';
        $requestedSettingId = 0;
        
        try
        {
        
            if(is_null($settingIdOrName) || strlen($settingIdOrName) == 0 || $settingIdOrName == '0')
            {

                // Do nothing

            }
            else if(intval($settingIdOrName) > 0)
            {

                $requestedSettingId = $settingIdOrName;
                $requestedSettingName = PerisianSetting::getNameForId($requestedSettingId); 

            }
            else
            {
                
                $requestedSettingName = $settingIdOrName;                
                $requestedSettingId = PerisianSetting::getIdForName($settingIdOrName);
        
            }
            
        }
        catch(Exception $e)
        {
            
        }

        $result = array(
            
            'id' => $requestedSettingId,
            'name' => $requestedSettingName
            
        );
        
        return $result;
        
    }
    
    /**
     * Formats the value field for special field types, e.g. 'checkbox-list'
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getDecodedValue($encodedString)
    {
        
        $returnValue = $encodedString;
    
        if(is_string($encodedString) && strlen($encodedString) > 0)
        {
            
            $returnValue = json_decode(PerisianFrameworkToolbox::securityRevert($encodedString));
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Tries to load data form the specified foreign setting into this object.
     * 
     * @author Peter Hamm
     * @param mixed $foreignSettingIdOrName
     * @return bool True if successful, false if not.
     */
    protected function loadForeignSettingData($foreignSettingIdOrName)
    {
                
        try
        {
        
            $foreignSetting = new PerisianSetting('', $foreignSettingIdOrName);
            
            $this->{$this->foreign_field_fieldtype} = $foreignSetting->{$foreignSetting->field_fieldtype};
            $this->{$this->foreign_field_description} = $foreignSetting->{$foreignSetting->field_description};
            $this->{$this->foreign_field_name} = $foreignSetting->{$foreignSetting->field_name};
            $this->{$this->foreign_field_pk} = $foreignSetting->{$foreignSetting->field_pk};
            $this->{$this->foreign_field_type} = $foreignSetting->{$foreignSetting->field_type};
            $this->{$this->foreign_field_title} = $foreignSetting->{$foreignSetting->field_title};

        }
        catch(PerisianException $e)
        {
                        
            return false;
            
        }
        
        return true;
        
    }
    
    /**
     * Fired directly before the object is saved to the database.
     * Can be overriden.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
        
        // Load the setting identifier and type, if it is not yet set
        if(!isset($this->{$this->field_fk}) || strlen($this->{$this->field_fk}) == 0 && strlen($this->{$this->foreign_field_name}) > 0)
        {
            
            $this->{$this->field_fk} = PerisianSetting::getIdForName($this->{$this->foreign_field_name});
            
            
        }
        
        $this->loadForeignSettingData($this->{$this->field_fk});
                
        if(@$this->{$this->foreign_field_fieldtype} == 'checkbox')
        {
                                    
            if($this->{$this->field_setting_value} == "0" || $this->{$this->field_setting_value} === false || strlen($this->{$this->field_setting_value}) == 0)
            {
                                
                $this->{$this->field_setting_value} = "0";
                
            }
            else
            {
                
                $this->{$this->field_setting_value} = "1";
                
            }
            
        }
        else if(@$this->{$this->foreign_field_fieldtype} == 'checkbox-list')
        {
            
            if(!is_array(@json_decode(@$this->{$this->field_setting_value})))
            {
                
                $checkedValues = array();

                for($i = 0; $i < count($this->{$this->field_setting_value}); ++$i)
                {

                    if($this->{$this->field_setting_value}[$i]['checked'])
                    {

                        array_push($checkedValues, $this->{$this->field_setting_value}[$i]['value']);

                    }

                }

                $this->{$this->field_setting_value} = json_encode($checkedValues);
                
            }
            
        }
        else if(@$this->{$this->foreign_field_fieldtype} == 'timespan')
        {
            
            $this->{$this->field_setting_value} = json_encode($this->{$this->field_setting_value});
            
        }
        else if(@$this->{$this->foreign_field_fieldtype} == 'text')
        {
                        
            $this->{$this->field_setting_value} = $this->{$this->field_setting_value} == static::CODE_EMPTY_STRING ? '' : $this->{$this->field_setting_value};
            
        }
        else if(@$this->{$this->foreign_field_fieldtype} == 'text-encrypted' || @$this->{$this->foreign_field_fieldtype} == 'password')
        {
            
            $this->{$this->field_setting_value} = $this->{$this->field_setting_value} == static::CODE_EMPTY_STRING ? '' : $this->{$this->field_setting_value};
                        
            $this->{$this->field_setting_value} = PerisianSetting::encryptString($this->{$this->field_setting_value});
            
        }
       
        return true;
        
    }
    
    /**
     * Retrieves a list of settings by their name.
     * 
     * @author Peter Hamm
     * @param Array $nameList An array containing names of settings.
     * @return array
     */
    public static function getSettingsByNames($nameList = array())
    {
        
        $returnValue = array();
        
        if(count($nameList) > 0)
        {
         
            PerisianFrameworkToolbox::security($nameList);
            
            for($i = 0; $i < count($nameList); ++$i)
            {
                                
                $settingObject = new static($nameList[$i]);
                                
                if(count($settingObject->settings) > 1)
                {
                    
                    throw new PerisianException("Ambiguous setting: \"" . $nameList[$i] . "\"");
                    
                }
                
                foreach($settingObject->settings as $setting)
                {
                    
                    $currentSetting = $setting;
                    
                    $setting['disabled'] = false;
                    $setting['value'] = $setting[$settingObject->field_setting_value];
                    
                    array_push($returnValue, $setting);
                    
                }
                                
            }
            
        }
                        
        return $returnValue;
        
    }
    
    /**
     * Retrieves all settings from the &$settingList that have the specified prefix.
     * 
     * @author Peter Hamm
     * @param String $prefix The desired prefix, e.g. "admin_"
     * @param Array $settingList The list of settings, e.g. from $this->getPossibleSettings()
     * @param bool $deleteInSource Optional, default: false (Does nothing). Set this to true to update the found entries in the $settingList to null (by reference).
     * @return Array
     */
    public static function retrieveSettingsByPrefix($prefix, &$settingList, $deleteInSource = false)
    {
        
        $settingObj = new static();

        $filteredSettingList = array();

        foreach($settingList as $settingListKey => $settingListEntry)
        {

            if(substr($settingListEntry[$settingObj->foreign_field_name], 0, strlen($prefix)) == $prefix)
            {

                array_push($filteredSettingList, $settingListEntry);

                if($deleteInSource)
                {
                    
                    // "Remove" it from the initial list
                    $settingList[$settingListKey] = null;
                    
                }

            }

        }
        
        return $filteredSettingList;

    }
    
    /**
     * Retrieves the value for this setting (if there is one)
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getValue()
    {
        
        $value = @$this->{$this->field_setting_value};
        
        if(@$this->{$this->foreign_field_fieldtype} == 'checkbox-list')
        {
            
            return static::getDecodedValue($value);
            
        }
        else if(@$this->{$this->foreign_field_fieldtype} == 'text')
        {
            
            return htmlspecialchars_decode($value);
            
        }
        
        return $value;
        
    }

    /**
     * Returns an associative array of the settings.
     *
     * The array keys are setting IDs, values are their values.
     *
     * @author Peter Hamm
     * @return Array
     */
    public function getAssociativeValues()
    {

        $data = $this->getSetting();
        $result = Array();

        for($i = 0, $m = count($data); $i < $m; ++$i)
        {
                        
            $setting = new static($data[$i][$this->field_fk]);
                        
            $result[$data[$i][$this->field_fk]] = $setting->getValue();
            
        }

        return $result;

    }

    /**
     * Retrieves a list of settings of the specific class,
     * depending on $this->setting_name.
     *
     * By default only those settings without a suffix are being returned
     * 
     * Set $suffix to what you need, this will then output all entries
     * with the suffix '-$suffix'.
     *
     * @author Peter Hamm
     * @param String $suffix Optional: A suffix to select setting types, default: empty
     * @param Array $disabledList Optional: An array containing setting IDs that shall be disabled and cannot be changed, default: empty
     * @param Array $excludeList Otional: An array containing setting IDs that should be excluded, default: empty 
     * @return Array
     */
    public function getPossibleSettings($suffix = '', $disabledList = Array(), $excludeList = Array())
    {

        $settingObj = new PerisianSetting($this->setting_name);
        $result = $settingObj->getSettings($suffix, $disabledList, $excludeList);

        return $result;

    }
    
    /**
     * Retrieves the value for the specified setting.
     * 
     * @author Peter Hamm
     * @param String $settignNameOrId A settign name/key or identifier
     * @param String $requestedSettingName
     * @return String
     */
    public static function getSettingValue($settingNameOrId = '')
    {
        
        $setting = new static($settingNameOrId);
        
        return $setting->getValue();
        
    }
    
    /**
     * Sets the value for the specified setting.
     * 
     * @author Peter Hamm
     * @param String $settingNameOrId A setting name/key or identifier
     * @param String $value
     * @return int
     */
    public static function setSettingValue($settingNameOrId = '', $value)
    {
                
        PerisianFrameworkToolbox::security($value);
         
        $setting = new static($settingNameOrId);
        
        $setting->{$setting->field_setting_value} = $value;
                
        return $setting->save();
        
    }
    
    /**
     * Retrieves all of the settings cached in this object.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getSettings()
    {
        
        return $this->settings;
        
    }
    
    /**
     * Retrieves a setting's identifier by the specified setting name.
     * 
     * @author Peter Hamm
     * @param String $settingName
     * @return int
     */
    public static function getSettingIdentifierByName($settingName)
    {
        
        if(!isset(static::$listIdentifiersByName[$settingName]))
        {
            
            $setting = new static($settingName);
            
            static::$listIdentifiersByName[$settingName] = $setting->{$setting->foreign_field_pk};
            
        }
                
        return static::$listIdentifiersByName[$settingName];
        
    }
    
    /**
     * Retrieves a setting's title by the specified setting's identifier or name.
     * 
     * @author Peter Hamm
     * @param String $settingIdOrName
     * @return String
     */
    public static function getSettingTitle($settingIdOrName)
    {
        
        $setting = new static($settingIdOrName);
        
        return $setting->{$setting->foreign_field_title};
        
    }

    /**
     * Retrieves setting values for the system
     *
     * @author Peter Hamm
     * @param integer $requestedSettingId Optional: The ID of a setting, default: get all settings
     * @param String $requestedSettingName Optional: The name of a setting, default: get all settings
     * @param String $enhancedQuery Optional: You may enhance the database query with this
     * @return mixed If an ID or name is specified: The value as a string - else: An array of all setting values
     */
    public function getSetting($settingIdOrName = '', $enhancedQuery = '')
    {

        $requestedSettingId = 0;
        $requestedSettingName = '';
        
        $settingSelector = static::getSettingSelector($settingIdOrName);
        
        $queryCollection = array();
        $query = '';

        // Check if the setting is already in the cache...
        {
            
            if($settingSelector['id'] != 0 && isset($this->settings[$settingSelector['id']]))
            {

                // Use the cache of this object to retrieve this setting
                return $this->settings[$settingSelector['id']][$this->field_setting_value];

            }
            
        }

        if($settingSelector['id'] != 0)
        {
                        
            $queryCollection[] = "{$this->field_fk} = '" . $settingSelector['id'] . "'";
            
        }

        if(strlen($enhancedQuery) > 0)
        {
            
            $queryCollection[] = $enhancedQuery;
            
        }

        $query = implode(' AND ', $queryCollection);
                
        $result = $this->getData($query);
        
        foreach($result as &$settings)
        {

            $settings[$this->foreign_field_title] = PerisianLanguageVariable::getVariable($settings[$this->foreign_field_title]);
            $settings[$this->foreign_field_description] = PerisianLanguageVariable::getVariable($settings[$this->foreign_field_description]);
            
        }
                
        for($i = 0, $m = count($result); $i < $m; ++$i)
        {

            // Store the fetched data to this object as a cache.

            foreach($result[$i] as $key => $value)
            {
                
                $this->settings[$result[$i][$this->field_fk]][$key] = $value;
                
            }

        }

        if($settingSelector['id'] == 0)
        {
            
            return $result;
            
        }
        else if(isset($result[0][$this->field_setting_value]))
        {
            
            return $result[0][$this->field_setting_value];
            
        }

        return '';

    }
    
}
