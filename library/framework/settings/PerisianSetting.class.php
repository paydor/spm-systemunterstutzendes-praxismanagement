<?php

require_once 'phpseclib3/mcrypt.php';

/**
 * Access and show any settings
 *
 * @author Peter Hamm
 * @data 2010-11-30
 */
class PerisianSetting extends PerisianDatabaseModel
{

    public $table = 'setting';
    public $field_pk = 'setting_id';
    public $field_name = 'setting_name';
    public $field_title = 'setting_title';
    public $field_description = 'setting_description';
    public $field_type = 'setting_type';
    public $field_fieldtype = 'setting_fieldtype';
    public $field_value_array_name = 'setting_value_array_name';
    public $field_order = 'setting_order';

    private $type = '';
    
    private static $encryptionKey = 'Qj%7ODfA894@1337';

    /**
     * Sets the type of settings you want to use this class for
     * and calls the parent's constructor.
     *
     * @param String $type Optional: The type of settings you want to use this class for
     * @param Mixed $settingIdOrName Optional: The identifier or name/key of a setting
     * @return void
     */
    public function __construct($type = '', $settingIdOrName = '')
    {

        parent::__construct('MySql', 'main');
        
        $settingSelector = PerisianSettingAbstract::getSettingSelector($settingIdOrName);
        
        if(strlen($type) > 0)
        {
            
            $this->type = $type;
            
        }
        
        if(strlen($settingSelector['id']) > 0 && $settingSelector['id'] > 0)
        {
            
            PerisianFrameworkToolbox::security($settingSelector['id']);
            
            $this->loadData($this->field_pk . "='{$settingSelector['id']}'");
            
        }
        
        return;

    }
    
    /**
     * Checks whether the specified string is encrypted or not.
     * 
     * @author Peter Hamm
     * @param String $string
     * @return boolean
     */
    public static function isEncrypted($string) 
    {
        
        if(strlen($string) < 2)
        {
            
            return false;
            
        }
        
        try
        {
            
            $decoded = base64_decode($string, true);

            if(base64_encode($decoded) === $string) 
            {
                
                $decryptedString = static::decryptStringRaw($string);
                      
                if(static::encryptString($decryptedString) == $string)
                {
                    
                    return true;
                    
                }
                
            }
            
            return false;
            
        }
        catch(Exception $e)
        {
            
            return false;
            
        }

    }

    /**
     * Encrypts the specified string.
     * 
     * @author Peter Hamm
     * @param String $string
     * @return String
     */
    public static function encryptString($string)
    {
        
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, static::$encryptionKey, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
                
    }
    
    /**
     * Decrypts the specified string.
     * 
     * @author Peter Hamm
     * @param String $string
     * @return String
     */
    public static function decryptString($string)
    {
        
        if(!static::isEncrypted($string))
        {
                        
            return $string;
            
        }
        
        return static::decryptStringRaw($string);
        
    }
    
    /**
     * Does no checks and symply tries to decrypt the given string.
     * 
     * @author Peter Hamm
     * @param String $string
     * @return String
     */
    private static function decryptStringRaw($string)
    {
        
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
                
        $iv = mcrypt_create_iv($ivSize);
        
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, static::$encryptionKey, base64_decode($string), MCRYPT_MODE_ECB, $iv, MCRYPT_RAND);
        
        return trim($decrypted);
        
    }

    /**
     * Retrieves a list of settings of the type of this class.
     *
     * By default, only those settings with a type not having a
     * suffix like '-admin' are displayed.
     *
     * Set $suffix to what you need, this will then output all entries
     * with the suffix '-$suffix'.
     *
     * @param String $suffix Optional: A suffix to select setting types, default: empty
     * @param Array $disabledList Optional: An array containing setting IDs that shall be disabled and cannot be changed, default: empty
     * @param Array $excludeList Otional: An array containing setting IDs that should be excluded, default: empty
     * @return Array
     */
    public function getSettings($suffix = '', $disabledList = Array(), $excludeList = Array())
    {

        PerisianFrameworkToolbox::security($suffix);
        PerisianFrameworkToolbox::security($disabledList);

        $query = Array();
        $sortArray = Array();

        if(empty($suffix))
        {
            
            $query[] = "{$this->field_type} = '" . $this->type . "'";
            
        }
        else
        {
            
            $query[] = "{$this->field_type} = '" . $this->type . "-{$suffix}'";
            
        }

        $result = $this->getData(implode(' OR ', $query), $this->field_order, "ASC");
        
        $formattedResult = array();
        
        for($i = 0, $m = count($result); $i < $m; ++$i)
        {
            
            if(in_array($result[$i][$this->field_pk], $excludeList))
            {
                
                continue;
                
            }

            $sortArray[$i] = PerisianLanguageVariable::getVariable($result[$i][$this->field_title]);
            $result[$i][$this->field_title] = PerisianLanguageVariable::getVariable($result[$i][$this->field_title]);
            $result[$i][$this->field_description] = PerisianLanguageVariable::getVariable($result[$i][$this->field_description]);

            if(in_array($result[$i][$this->field_pk], $disabledList))
            {
                $result[$i]['disabled'] = true;
            }
            else
            {
                $result[$i]['disabled'] = false;
            }
            
            array_push($formattedResult, $result[$i]);

        }

        // The settings get sorted by their title
        //array_multisort($sortArray, $result);

        return $formattedResult;

    }


    /**
     * Loads the html template to show an input field for a setting
     * and directly prints it.
     *
     * @author Peter Hamm
     * @global String $style The current page style
     * @param mixed $settingData An object or associative array with setting data
     * @return void
     */
    static public function printSettingItem($settingData)
    {

        global $style;
        
        if(is_array($settingData) && isset($settingData[0]))
        {
            
            $settingData = $settingData[0];
            
        }
        
        if(is_object($settingData))
        {
            
            $settingData = (array)$settingData;
            
        }
        
        if($settingData == null)
        {
            
            return;
            
        }

        $fields = PerisianFrameworkToolbox::getFields('PerisianSetting');
        $disabled = false;
        
        if(!isset($settingData[$fields['field_pk']]))
        {
                        
            throw new PerisianException(PerisianLanguageVariable::getVariable(10121));
            
        }

        if($settingData['disabled'])
        {
            
            $disabled = true;
            
        }
        
        $fieldTitle = $settingData[$fields['field_title']];
        $fieldTitle = strlen($fieldTitle) > 0 ? $fieldTitle : "Unknown";
        $fieldDescription = $settingData[$fields['field_description']];
        $fieldNameDescriptive = $settingData[$fields['field_name']];

        if($settingData[$fields['field_fieldtype']] == 'checkbox')
        {

            $fieldChecked = false;
            $fieldName = 'setting_' . $settingData[$fields['field_pk']];

            if(isset($settingData['value']))
            {
                
                $fieldChecked = ($settingData['value'] == '1') ? true : false;
                
            }

            $file = 'setting_checkbox.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'icon')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
            }

            $file = 'setting_icon.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'text' || $settingData[$fields['field_fieldtype']] == 'text-encrypted' || $settingData[$fields['field_fieldtype']] == 'tags')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
                if($settingData[$fields['field_fieldtype']] == 'text-encrypted')
                {
                                        
                    $fieldValue = static::decryptString($fieldValue);
                     
                }
                                
            }

            $file = 'setting_text.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'number')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
            }

            $file = 'setting_number.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'password')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = static::decryptString($settingData['value']);
                                
            }

            $file = 'setting_password.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'textarea')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
            }

            $file = 'setting_textarea.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'wysiwyg')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
            }

            $file = 'setting_wysiwyg.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'dropdown')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';
            
            if(isset($settingData['value']))
            {
                
                $fieldValue = $settingData['value'];
                
            }

            // Try to get the entries for the dropdown field as a global!
            {
   
                $dropdownListName = stripcslashes($settingData[$fields['field_value_array_name']]);
                
                if(strpos($dropdownListName, "[") > 0)
                {
                    
                    // The desired value is in an array.
                    {
                        
                        $explosion = explode('[', $dropdownListName);

                        $globalVariable = $explosion[0];

                        global $$globalVariable;

                        $dropdownList = $$globalVariable;
                    
                    }
                    
                    for($i = 1; $i < count($explosion); ++$i)
                    {
                        
                        $key = substr($explosion[$i], 1, -2);
                        
                        $dropdownList = @$dropdownList[$key];
                        
                    }
                                                
                }
                else 
                {
                    
                    $dropdownList = $dropdownListName;

                    global $$dropdownList;

                    $dropdownList = &$$dropdownList;
                    
                }

            }
            
            if(!isset($dropdownList))
            {
                
                echo PerisianLanguageVariable::getVariable(10126) . $dropdownListName;
                
                return;
                
            }

            $file = 'setting_dropdown.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'checkbox-list')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = array();
            
            if(isset($settingData['value']))
            {
                
                $fieldValue = PerisianSettingAbstract::getDecodedValue($settingData['value']);
                
            }

            // Retrieve the values for the checkbox list
            {
                
                $checkboxListName = stripcslashes($settingData[$fields['field_value_array_name']]);
                
                if(strpos($checkboxListName, "[") > 0)
                {
                    
                    // The desired value is in an array.
                    {
                        
                        $explosion = explode('[', $checkboxListName);

                        $globalVariable = $explosion[0];

                        global $$globalVariable;

                        $checkboxList = $$globalVariable;
                    
                    }
                    
                    for($i = 1; $i < count($explosion); ++$i)
                    {
                        
                        $key = substr($explosion[$i], 1, -2);
                        
                        $checkboxList = @$checkboxList[$key];
                        
                    }
                                                
                }
                else 
                {
                    
                    $checkboxList = $checkboxListName;

                    global $$checkboxList;

                    $checkboxList = &$$checkboxList;
                    
                }
                
            }

            if(!isset($checkboxList))
            {
                
                echo PerisianLanguageVariable::getVariable(10126) . $checkboxListName;
                
                return;
                
            }

            $file = 'setting_checkbox_list.phtml';

        }
        else if($settingData[$fields['field_fieldtype']] == 'timespan')
        {

            $fieldName = 'setting_' . $settingData[$fields['field_pk']];
            $fieldValue = '';

            if(isset($settingData['value']))
            {
                
                $fieldValue = json_decode(PerisianFrameworkToolbox::securityRevert($settingData['value']), true);
                                
            }
            else
            {
                
                $fieldValue = array(
                    
                    'begin' => '00:00',
                    'end' => '24:00',
                    
                );
                
            }

            $file = 'setting_timespan.phtml';

        }

        if(isset($file))
        {

            try
            {

                $file = PerisianFrameworkToolbox::getConfig('basic/project/folder')."/templates/{$style}/includes/settings/{$file}";
                eval(' ?>' . htmlspecialchars_decode(file_get_contents($file)) . '<?php ');

            }
            catch(Exception $e)
            {

                echo $e->getMessage();

            }

        }

    }

    /**
     * Directly prints the template file for a list of settings.
     *
     * @author Peter Hamm
     * @global String $style The current page style
     * @param Array $settingData An associative array with setting data
     * @param int $title Language variable for the title
     * @param int $saveButtonSpace Optional: After how many entries the save button is repeatedly shown, default: 10
     * @return void
     */
    static public function printSettingList($settingList, $title, $saveButtonSpace = 10)
    {

        global $style;

        PerisianFrameworkToolbox::security($settingList);
        PerisianFrameworkToolbox::security($title);

        $settingsId = substr(md5($title), 0, 6);
        $title = PerisianLanguageVariable::getVariable($title);
        $countMaximum = count($settingList);

        try
        {
            
            $file = PerisianFrameworkToolbox::getConfig('basic/project/folder')."/templates/{$style}/includes/settings/setting_list.phtml";
            eval(' ?>' . htmlspecialchars_decode(file_get_contents($file)) . '<?php ');

        }
        catch(Exception $e)
        {

            echo $e->getMessage();

        }

    }

    /**
     * Checks if the save button needs to be displayed or not
     * Used in the settings list template
     *
     * @param int $countMaximum Defined in self::printSettingList()
     * @param int $saveButtonSpace Defined in self::printSettingList()
     * @param int $currentCount The current count within a loop
     * @return bool Whether to show or not to show the save button
     */
    static public function checkSaveButtonAppearance($currentCount, $countMaximum, $saveButtonSpace)
    {

        $saveButtonSpace = (int)$saveButtonSpace;
        $countMaximum = (int)$countMaximum;
        $currentCount = (int)$currentCount+1;
        $moduloMaximum = $countMaximum % $saveButtonSpace;

        if($countMaximum < $saveButtonSpace && $countMaximum == $currentCount)
        {
            return true;
        }

        if($currentCount % $saveButtonSpace == 0 && $currentCount + ceil($saveButtonSpace / 2) < $countMaximum)
        {
            return true;
        }

        if($currentCount == $countMaximum)
        {
            return true;
        }

        return false;
        
    }

    /**
     * Loads all of a setting's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific setting
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {
                
        if($this->getCount($query) == 1)
        {
            
            $data = $this->getData($query);
                        
            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            $exception = new PerisianException(PerisianLanguageVariable::getVariable(10223));
                        
            throw $exception;
            
        }
        
        return false;

    }
    
    /**
     * Retrieves the name for the specified setting id
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getNameForId($id)
    {
        
        PerisianFrameworkToolbox::security($id);
        
        $dummy = new static();
        
        $query = $dummy->field_pk . ' = "' . $id . '"';
        
        $result = $dummy->loadData($query);
        
        return $dummy->{$dummy->field_name};
        
    }
    
    /**
     * Retrieves the setting id for the specified setting name
     * 
     * @author Peter Hamm
     * @param String $name
     * @return int
     */
    public static function getIdForName($name)
    {
        
        PerisianFrameworkToolbox::security($name);
        
        $dummy = new static();
        
        $query = $dummy->field_name . ' = "' . $name . '" GROUP BY ' . $dummy->field_pk;
        
        $result = $dummy->loadData($query);
            
        return $dummy->{$dummy->field_pk};
                
    }

    /**
     * Retrieves the title for the specified setting id / name
     * 
     * @author Peter Hamm
     * @param int $idOrName
     * @return String
     */
    public static function getTitleForId($idOrName)
    {
     
        $setting = new static('', $idOrName);
        
        return $setting == null ? "" : PerisianLanguageVariable::getVariable($setting->{$setting->field_title});
        
    }

    /**
     * Retrieves the description for the specified setting id
     * 
     * @author Peter Hamm
     * @param int $id
     * @return String
     */
    public static function getDescriptionForId($id)
    {
        
        $setting = new static('', $id);
        return $setting == null ? "" : PerisianLanguageVariable::getVariable($setting->{$setting->field_description});
        
    }
    
    /**
     * Retrieves if the setting with the specified identifier is an administrator setting or not.
     * 
     * @auhtor Peter Hamm
     * @param int $id
     * @return boolean
     */
    public static function getIsAdminSettingForId($id)
    {
        
        $setting = new static('', $id);        
        return $setting->isAdminSetting();
        
    }
    
    
    /**
     * Retrieves if this setting is an administrator setting or not.
     * 
     * @auhtor Peter Hamm
     * @return boolean
     */
    public function isAdminSetting()
    {
                
        if(substr($this->{$this->field_type}, -6) == "-admin")
        {
            
            return true;
            
        }
        
        return false;
        
    }

}
