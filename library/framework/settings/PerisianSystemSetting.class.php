<?php

require_once 'abstract/PerisianSettingAbstract.class.php';
require_once 'framework/PerisianUploadFileManager.class.php';

/**
 * Handles system settings
 *
 * @author Peter Hamm
 * @data 2010-10-25
 */
class PerisianSystemSetting extends PerisianSettingAbstract
{

    // Basic values for the specific settings
    public $table = 'system_setting';
    public $field_pk = 'system_setting_id';
    public $field_fk = 'system_setting_setting_id';
    public $field_setting_value = 'system_setting_value';

    public $setting_name = 'system';
    
    /**
     * Retrieves if the specified setting's value is true or not.
     * 
     * @author Peter Hamm
     * @param mixed $settingIdOrName
     * @return bool
     */
    public static function isSettingTrue($settingIdOrName)
    {
        
        $settingValue = static::getSettingValue($settingIdOrName);
        
        $result = (int)$settingValue == 1 || $settingValue == 'true';
        
        return $result;
        
    }
    
    public static function getImageUploadData($settingIdOrName = '')
    {
                
        $frontendWelcomeImageSetting = new PerisianSetting(0, $settingIdOrName);
        $frontendWelcomeImageSystemSetting = new static($settingIdOrName);
        
        try
        {
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $frontendWelcomeImageSystemSetting->getValue());
            
        }
        catch(PerisianException $e)
        {
            
            $fileInfo = null;
            
        }
        
        $uploadField = array(

            "action" => $_SERVER["PHP_SELF"],
            "name" => $frontendWelcomeImageSetting->{$frontendWelcomeImageSetting->field_name},
            "title" => PerisianLanguageVariable::getVariable($frontendWelcomeImageSetting->{$frontendWelcomeImageSetting->field_title}),
            "description" => PerisianLanguageVariable::getVariable($frontendWelcomeImageSetting->{$frontendWelcomeImageSetting->field_description}),

            "image" => $fileInfo

        );
        
        return $uploadField;
            
    }
    
    /**
     * Retrieves the URL for the specified uploaded image.
     * 
     * @author Peter Hamm
     * @param String $settingIdOrName
     * @return Sring
     */
    public static function getImageUploadUrl($settingIdOrName = '')
    {
        
        $imageSystemSetting = new static($settingIdOrName);
        
        return PerisianUploadFileManager::getUrlForFile('image', $imageSystemSetting->getValue());
        
    }
    
}
