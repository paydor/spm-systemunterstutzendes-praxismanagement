<?php

/**
 * This class provides basic validation functions
 *
 * @author Peter Hamm
 * @date 2010-11-24
 */
class PerisianValidation
{
    
    const SECURITY_PASSWORD_POLICY_STRICT = 'strict';
    const SECURITY_PASSWORD_POLICY_LOOSE = 'loose';
    
    static $exceptions = Array();

    /**
     * Checks if an integer is a valid ID
     *
     * @author Peter Hamm
     * @param String $id
     * @return boolean
     */
    static public function checkId($id)
    {

        $id = (int)$id;

        if(!is_int($id))
        {
            self::$exceptions[] = PerisianLanguageVariable::getVariable(10109);
            return false;
        }
        if($id < 0)
        {
            self::$exceptions[] = PerisianLanguageVariable::getVariable(10110);
            return false;
        }

        return true;

    }

    /**
     * Checks if the passed parameter is a valid array
     *
     * @author Peter Hamm
     * @param Array $array
     * @return boolean
     */
    static public function checkArray($array)
    {

        if(isset($array) && is_array($array))
        {
            return true;
        }

        return false;

    }

    /**
     * Checks if the parameter is a valid email address
     *
     * @author Peter Hamm
     * @param String $address
     * @return boolean
     */
    static public function checkEmail($address)
    {

       $reg = '/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/';

       if(preg_match($reg, $address) == false) {

          self::$exceptions[] = PerisianLanguageVariable::getVariable(10094);
          
          return false;
          
       }

       return true;

    }

    /**
     * Assuming that the parameter is a required field,
     * this method checks it for validation
     *
     * @author Peter Hamm
     * @param String content
     * @return boolean
     */
    static public function checkRequired($content)
    {

        if(empty($content))
        {

            self::$exceptions[] = PerisianLanguageVariable::getVariable(10098);
            
            return false;

        }

        return true;

    }

    
    /**
     * Retrieves the currently set option for the system's password policy.
     * 
     * @author Peter Hamm
     * @return String Either "strict" or "loose".
     */
    public static function getPasswordPolicy()
    {
        
        $option = static::SECURITY_PASSWORD_POLICY_STRICT;
        
        $value = strtolower(PerisianSystemSetting::getSettingValue('system_security_password_policy'));
        
        if($value == static::SECURITY_PASSWORD_POLICY_LOOSE)
        {
            
            $option = $value;
            
        }
        
        return $option;
        
    }
    
    /**
     * Retrieves the possible options for the system's password policy.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getPasswordPolicyOptions()
    {
        
        $options = Array();
        
        $details = static::getPasswordPolicyDetails();
        
        foreach($details as $key => $policy)
        {
            
            $options[$key] = $policy['title'];
            
        }
        
        return $options;
        
    }
    
    /**
     * Retrieves details for the system's password policies.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getPasswordPolicyDetails()
    {
        
        $details = Array(
            
            static::SECURITY_PASSWORD_POLICY_STRICT => Array(
                
                'title' => PerisianLanguageVariable::getVariable('p5aa42926684af'),
                
                'requirements' => Array( 
                    
                    PerisianLanguageVariable::getVariable('10653'), // At least three characters
                    PerisianLanguageVariable::getVariable('10728'), // At least one number
                    PerisianLanguageVariable::getVariable('10729'), // At least one lower case character
                    PerisianLanguageVariable::getVariable('10730'), // At least one upper case character
                    PerisianLanguageVariable::getVariable('10731'), // At least one special character
                    PerisianLanguageVariable::getVariable('p5aa44fb6aa61a') // No whitespaces
                
                )
                
            ),
            
            static::SECURITY_PASSWORD_POLICY_LOOSE => Array(
                
                'title' => PerisianLanguageVariable::getVariable('p5aa4292f00e99'),
                
                'requirements' => Array( 
                    
                    PerisianLanguageVariable::getVariable('10653'), // At least three characters
                    PerisianLanguageVariable::getVariable('10728'), // At least one number
                    PerisianLanguageVariable::getVariable('10729'), // At least one lower case character
                    PerisianLanguageVariable::getVariable('10730'), // At least one upper case character
                    PerisianLanguageVariable::getVariable('p5aa44fb6aa61a') // No whitespaces
                
                )
                
            )
            
        );
        
        return $details;
        
    }

    /**
     * Checks the validation of a password.
     * 
     * You need to pass this method two password fields to make sure
     * the user did not make any mistake.
     * 
     * The validation considers the system's configured password policy
     *
     * @author Peter Hamm
     * @param String $passOne
     * @param String $passTwo
     * @return boolean
     */
    static public function checkPassword($passOne, $passTwo)
    {

        try
        {

            if(!$passOne || !$passTwo)
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10095));
                
            }

            if($passOne != $passTwo)
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10096));
                
            }

            if(strlen($passOne) < 3)
            {
                
                // The password should consist of at least three letters.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10097) . ' - ' . $passOne . ' - ' . $passTwo);
                
            }
            
            if(!preg_match("/\d/", $passOne)) 
            {
                
                // The password must contain at least one number.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10728'));
                
            }
            
            if(!preg_match("/[A-Z]/", $passOne)) 
            {
                
                // The password must contain at least one capital letter.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10730'));
                
            }
            
            if(!preg_match("/[a-z]/", $passOne)) 
            {
                
                // The password must contain at least one lower case letter.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10729'));
                
            }
            
            if(static::getPasswordPolicy() == static::SECURITY_PASSWORD_POLICY_STRICT)
            {
                
                if(!preg_match("/\W/", $passOne)) 
                {

                    // The password must contain at least one special character

                    throw new PerisianException(PerisianLanguageVariable::getVariable('10731'));

                }
            
            }
            
            if(preg_match("/\s/", $passOne))
            {
                
                // The password must not contain any whitespace.
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('p5aa44fb6aa61a'));
                
            }

            return true;

        }
        catch(Exception $e)
        {

            self::$exceptions[] = $e->getMessage();
            
            return false;

        }

    }
    
    /**
     * Retrieves the message from the latest exception thrown by this class.
     * 
     * @author Peter Hamm
     * @return String
     */
    static public function getLatestExceptionMessage()
    {
        
        $message = "";
        
        if(count(self::$exceptions) > 0)
        {
            
            $message = self::$exceptions[count(self::$exceptions) - 1];
            
        }
        
        return $message;
        
    }

    static public function throwLatestException()
    {

        if(count(self::$exceptions) > 0)
        {
            
            throw new PerisianException(self::$exceptions[count(self::$exceptions) - 1]);
            
        }

    }

}
