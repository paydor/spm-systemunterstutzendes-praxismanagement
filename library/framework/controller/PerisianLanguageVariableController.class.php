<?php

require_once 'framework/abstract/PerisianController.class.php';

/**
 * Controller to manage language variables
 *
 * @author Peter Hamm
 * @date 2016-12-30
 */
class PerisianLanguageVariableController extends PerisianController
{
    
    /**
     * Checks if the current user has access to this functionality.
     * Will throw an exception if not.
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    public function checkAuthorization()
    {

        global $user;

        if(!isset($user) || !$user->hasSetting('is_system_admin') || !$user->hasSetting('is_system_developer'))
        {

            // Missing permission
            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }
        
    }
        
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {

        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'system/language_variables/main'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries,
     * filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $languageList = PerisianLanguage::getLanguages();
                
        $languageOne = (int)$this->getParameter('languageOne');
        $languageTwo = (int)$this->getParameter('languageTwo');
                    
        // Selected languages by default & language validation check
        {
            
            $languageIds = Array();

            if($languageOne <= 0)
            {
                
                $languageOne = PerisianSystemConfiguration::getLanguageId();
                                
            }
            
            if($languageTwo <= 0)
            {
                
                if(count($languageList) > 1)
                {

                    for($i = 0, $m = count($languageList); $i < $m; ++$i)
                    {

                        if($languageList[$i]['language_id'] != $languageOne)
                        {

                            $languageTwo = $languageList[$i]['language_id'] ;

                            break;

                        }

                    }

                }
                
            }
            
            if((int)$languageOne > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i]['language_id'] == $languageOne)
                    {
                        
                        array_push($languageIds, $languageOne);
                        
                        break;
                        
                    }
                    
                }
                
            }
            
            if((int)$languageTwo > 0)
            {
                
                for($i = 0; $i < count($languageList); ++$i)
                {
                    
                    if($languageList[$i]['language_id'] == $languageTwo)
                    {
                        
                        array_push($languageIds, $languageTwo);
                        
                        break;
                        
                    }
                    
                }
                
            }
            
            if(!in_array($languageTwo, $languageIds))
            {
                
                unset($languageTwo);
                unset($languageTwo);
                
            }
            
        }

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $search = $this->getParameter('search');
        
        $languageVarFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
        
        $list = PerisianLanguageVariable::getLatestVariables($languageIds, $offset, $limit, $search);
        $count = PerisianLanguageVariable::getVariableCount($languageIds, $search);
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'system/language_variables/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list_languages' => $languageList,
            
            'list' => $list,
            'count' => $count,
            
            'offset' => $offset,
            'limit' => $limit,
            'search' => $search
            
        );
        
        if(isset($languageOne))
        {
            
            $result['language_one'] = '' . $languageOne;
            
        }
        
        if(isset($languageTwo))
        {
            
            $result['language_two'] = '' . $languageTwo;
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a modal form to quickly select language variables.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionListModal()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'system/language_variables/modal_list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array();
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides a result set of language variables
     * for the modal language variable search.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionListModalResults()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'system/language_variables/modal_list_result'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        // Parameter initialization
        {
            
            $offset = $this->getParameter('offset', 0);
            $limit = $this->getParameter('limit', 10);
            $search = $this->getParameter('search', '');
            
            $languageIds = Array(PerisianSystemConfiguration::getLanguageId());
            $languageVarFields = PerisianFrameworkToolbox::getFields('PerisianLanguage');
            
        }
        
        $variableList = array();
        $variableCount = 0;

        if(strlen($search) > 0)
        {
            
            $variableList = PerisianLanguageVariable::getLatestVariables($languageIds, $offset, $limit, $search);
            $variableCount = PerisianLanguageVariable::getVariableCount($languageIds, $search);
            
        }
        
        $result = array(
            
            'offset' => $offset,
            'limit' => $limit,
            'search' => $search,
            
            'list' => $variableList,
            'count' => $variableCount
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data for a form to create a new entry.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNew()
    {
        
        $this->setInternal('title_form', PerisianLanguageVariable::getVariable(10035));
        
        $this->actionEdit();
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionEdit()
    {
                
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'system/language_variables/edit'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $languageList = PerisianLanguage::getLanguages();
        $editId = $this->getParameter('variableSubId', null);
        $variableList = null;
        
        if(strlen($editId) > 0)
        {
            
            $variableList = PerisianLanguageVariable::getVariable($editId, true);
            
        }
        
        $result = array(
            
            'id' => $editId,
            'list_languages' => $languageList,
            'list_variables' => $variableList,
            
            'title_form' => strlen($this->getInternal('title_form')) > 0 ? $this->getInternal('title_form') : PerisianLanguageVariable::getVariable(10072)
            
        );
                
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Deletes an entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionDelete()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //

        $deleteId = $this->getParameter('deleteId');
        
        $result = array(

            'success' => false

        );

        if(strlen($deleteId) > 0)
        {
            
            PerisianLanguageVariable::deleteUniqueId($deleteId);

            $result = array(

                'success' => true

            );
            
        }

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Saves a language variable.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @return void
     */
    protected function actionSave()
    {
        
        $result = array();
        $renderer = array();
        
        // Parameter initialization
        {
            
            $step = $this->getParameter('step', 1);
            $sentLanguages = $this->getParameter('languages');
            $sentValues = $this->getParameter('values');
            $editId = $this->getParameter('editId');
                        
            $saveObj = null;
            
        }
        
        if($step == 1)
        {
            
            $renderer = array(

                'json' => true

            );
            
            $result = array(
                
                'id' => null,
                'success' => false
                
            );
            
            if(count($sentLanguages) != count($sentValues))
            {
                
                // 'Error before saving'
                throw new PerisianException(10724);
                
            }
            
            $results = Array();
            $saveSuccessful = false;
            
            $generatedUniqueId = PerisianLanguageVariable::generateUniqueId();

            for($i = 0, $m = count($sentLanguages); $i < $m; ++$i)
            {
                
                // Generate a language variable object for each transmitted language.

                $saveObj = new PerisianLanguageVariable($editId, $sentLanguages[$i]);
                    
                $uniqueId = (strlen($editId) > 0) ? $editId : $generatedUniqueId;
                
                if(!isset($saveObj->{$saveObj->field_variable_unique_sub_id}))
                {
                    
                    $saveObj->{$saveObj->field_variable_language_id} = $sentLanguages[$i];
                    
                }
                
                $saveObj->{$saveObj->field_variable_unique_sub_id} = $uniqueId;
                $saveObj->{$saveObj->field_variable_content} = $sentValues[$i];
                                
                $results[] = $saveObj->save();

            }

            for($i = 0, $m = count($results); $i < $m; ++$i)
            {

                if($results[$i] != 0)
                {
                    
                    $saveSuccessful = true;
                    
                    break;
                    
                }

            }

            if($saveSuccessful)
            {
                
                $result['success'] = true;
                $result['id'] = $uniqueId == '-1' ? $newSubId : $uniqueId;
                
            }
            
        }
        else if($step == 2)
        {
            
            $renderer = array(

                'blank_page' => true,
                'page' => 'system/language_variables/save'

            );
            
            if($editId == "error")
            {

                $title = PerisianLanguageVariable::getVariable(10066);
                $message = PerisianLanguageVariable::getVariable(10068);

            }
            else
            {

                $title = PerisianLanguageVariable::getVariable(10065);
                $message = PerisianLanguageVariable::getVariable(10067) . ' <span class="alert alert-success alert-success-language-variable">' . $editId . '</span>';

            }
            
            $result = array(
                
                'id' => $editId,
                'title' => $title,
                'message' => $message
                
            );
            
        }
        
        //        
        
        $this->setResultValue('renderer', $renderer);        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Retrieves the count of missing entries.
     * 
     * @author Peter Hamm
     * @return int
     */
    protected function getCountMissingEntries()
    {
        
        return PerisianLanguageVariableMissing::getEntryCount();
        
    }
    
    /**
     * Starts an import of language variables from a specified source.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionImport()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'success' => true,
            'message' => PerisianLanguageVariable::getVariable('p576c315213fed')
            
        );
        
        $importServer = $this->getParameter('server');
                
        PerisianSystemSetting::setSettingValue('system_language_variable_import_server', $importServer);
        
        try
        {
            
            $retrievedData = PerisianLanguageVariableMissing::getMissingVariablesFromServer($importServer);
            
            $impportedVariableIdentifiers = PerisianLanguageVariableMissing::importMissingVariables($retrievedData['variables']);
            
            if(count($impportedVariableIdentifiers) > 0)
            {
                
                $result['message'] = PerisianLanguageVariable::getVariable('p576c97fc08c72');
                $result['messageExtended'] = str_replace('%1', count($impportedVariableIdentifiers), PerisianLanguageVariable::getVariable('p576c9873be099'));
                
            }
            else
            {
                
                $result['success'] = false;
                $result['message'] = PerisianLanguageVariable::getVariable('p576c98ae9aa6e');
                
            }
            
        }
        catch(PerisianException $e)
        {
            
            $result['success'] = false;
            $result['message'] = PerisianLanguageVariable::getVariable('p576c315213fed');
            $result['messageExtended'] = $e->getMessage();
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data to display a form to select an import source for language variable imports,
     * e.g. a remote server.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSelectImport()
    {
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'language/import'
            
        );
        
        $this->setResultValue('renderer', $renderer);
                        
        //
                
        $result = array(
            
            'title_form' => PerisianLanguageVariable::getVariable('p576c1b8cbd8f0'),
            'server_import' => PerisianSystemSetting::getSettingValue('system_language_variable_import_server')
                        
        );
        
        $this->setResultValue('result', $result);
        
    }
        
    /**
     * Provides a list of missing language variables for the system.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionShowMissing()
    {
        
        $renderer = array(
            
            'page' => 'language/missing'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
                
        $result = array(
            
            'count_entries_missing' => $this->getCountMissingEntries()
                        
        );
        
        $this->setResultValue('result', $result);
        
    }
            
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}