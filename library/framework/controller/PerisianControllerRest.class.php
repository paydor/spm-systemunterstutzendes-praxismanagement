<?php

require_once 'framework/abstract/PerisianController.class.php';

abstract class PerisianControllerRest extends PerisianController
{
    
    protected $controllerInstance;
    protected $userApi;
    
    /**
     * Should return the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    abstract protected function getControllerClassName();
        
    /**
     * Retrieves if the API is generally activated or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    protected function isApiActive()
    {
        
        $isEnabled = PerisianSystemSetting::getSettingValue('is_enabled_api') == 1;
        
        // Special case: If the API user is a system internal user, 
        // the API is always active for that user's requests.
        if(isset($this->userApi) && $this->userApi->{$this->userApi->field_is_system} == 1)
        {
                        
            $isEnabled = true;
            
        }
        
        return $isEnabled;
        
    }
    
    /**
     * Tries to set the API user for this object by the specified access token.
     * 
     * @author Peter Hamm
     * @param String $token
     * @return boolean Returns false if any kind of error occured.
     */
    protected function setUserApiForToken($token)
    {

        $userApi = CalsyUserApi::getUserApiForToken($token);

        if(is_null($userApi) || !isset($userApi->{$userApi->field_pk}) || $userApi->{$userApi->field_pk} <= 0)
        {

            return false;

        }

        $this->userApi = $userApi;
        
        return true;
        
    }
    
    /**
     * Retrieves if the remote entity is allowed to retrieve data from this resource.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public function authorize()
    {
                
        // Check if the token is valid
        {

            $transmittedToken = trim(PerisianFrameworkToolbox::getRequest('token'));

            if(strlen($transmittedToken) == 0)
            {

                return false;

            }
            
            // Find the API user for this token
            $result = $this->setUserApiForToken($transmittedToken);
            
            if(!$result)
            {
                
                return false;
                
            }
            
        }
        
        if(!$this->isApiActive())
        {
            
            return false;

        }
        
        return true;
        
    }
    
    /**
     * Will require the current API user to be a system user to access the resource.
     * 
     * @author Peter Hamm
     * @throws RestException
     * @return void
     */
    protected function requireSystemUser()
    {
        
        if(!isset($this->userApi->{$this->userApi->field_is_system}) || $this->userApi->{$this->userApi->field_is_system} != 1)
        {
            
            throw new RestException(401);
            
        }
        
    }
    
    /**
     * Detailed access management, e.g. for read, write or custom access types.
     * Will throw an exception if the currently logged in API user does not have the required permission.
     * 
     * @author Peter Hamm
     * @throws RestException
     * @param String $type For example for the area module: 'areas_read', 'areas_write', or a custom implementation.
     * @return void
     */
    protected function requireAccess($type)
    {
        
        if(!$this->userApi->hasAccessRights($type))
        {
            
            throw new RestException(401);
            
        }
        
    }
    
    /**
     * Retrieves an instance of the content controller specified by $this->getControllerClassName()
     * 
     * @author Peter Hamm
     * @return PerisianController
     * @throws PerisianException
     */
    protected function getControllerInstance()
    {
        
        if(isset($this->controllerInstance))
        {
            
            return $this->controllerInstance;
            
        }
        
        $controllerName = $this->getControllerClassName();
        
        if(strlen($controllerName) == 0)
        {
            
            // Content controller not set.
            throw new PerisianException(PerisianLanguageVariable::getVariable('p582223125ba7b'));
            
        }
        
        if(!class_exists($controllerName))
        {
        
            throw new PerisianException(PerisianLanguageVariable::getVariable('p5a6ebf7561494') . " '" . $controllerName . "'");
            
        }
        
        $this->controllerInstance = new $controllerName();
        
        return $this->controllerInstance;
        
    }
        
    /**
     * Handles and executes the content.
     * 
     * @author Peter Hamm
     * @param PerisianController $contentController
     * @return void
     */
    public static function handleContent(PerisianController $contentController)
    {
                
        $instance = static::getInstance();
        
        $instance->setInternal('content_controller', $contentController);
                
        $instance->execute();
        
    }
    
    /**
     * Executes the request and retrieves data.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function finalize()
    {
        
        $this->execute();
        
        $result = $this->getResult();
        
        return $result['result'];
        
    }
    
    /**
     * Executes the specified controller and retrieves data for the HTML renderer.
     * 
     * @author Peter Hamm
     * @global String $page
     * @return void
     */
    public function execute()
    {
        
        try 
        {

            $controllerObject = $this->getControllerInstance();

            $controllerObject->setParametersFromRequest();
            $controllerObject->setAction($this->getAction());

            $controllerObject->execute();

            $this->result = $controllerObject->getResult();
            
            return;
            
        } 
        catch (Exception $e) 
        {
            
            $result = array(
                
                'success' => false
                
            );
            
            if(PerisianSystemConfiguration::isDebugMode())
            {
                
                $result['debug'] = array(
                    
                    'exception' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTraceAsString()
                    
                );
                
            }
            
            $this->setResultValue('result', $result);
            
        }
                
    }
    
}
