<?php

require_once 'framework/abstract/PerisianController.class.php';

class PerisianControllerWeb extends PerisianController
{
    
    protected static $instance = null;
    
    /**
     * Generates or retrieves the singleton instance of this controller.
     * 
     * @author Peter Hamm
     * @return static
     */
    protected static function getInstance()
    {
        
        if(is_null(static::$instance))
        {
            
            static::$instance = new static();
            
        }
        
        return static::$instance;
        
    }
    
    /**
     * Handles and executes the content.
     * 
     * @author Peter Hamm
     * @param PerisianController $contentController
     * @return void
     */
    public static function handleContent(PerisianController $contentController)
    {
                
        $instance = static::getInstance();
        
        $instance->setInternal('content_controller', $contentController);
                
        $instance->execute();
        
    }
    
    /**
     * Executes the specified controller and retrieves data for the HTML renderer.
     * 
     * @author Peter Hamm
     * @global String $page
     * @global bool $disableLoginScreen
     * @return void
     */
    public function execute()
    {
        
        global $page;
        global $disableLoginScreen;
                
        $contentController = $this->getInternal('content_controller');
        
        if(!isset($contentController) || is_null($contentController))
        {
            
            // Undefined content controller.
            throw new PerisianException(PerisianLanguageVariable::getVariable('p582223125ba7b'));
            
        }
        
        // Initialize the content controller with all required parameters.
        {
            
            $action = PerisianFrameworkToolbox::getRequest('do');

            if(strlen($action) > 0 && strlen($contentController->getAction()) == 0)
            {
                
                $contentController->setAction($action);
                
            }

            $contentController->setParametersFromRequest();

            $controllerData = $contentController->execute();

            $contentController->globalize();

        }

        // Render it to HTML
        {
                        
            if(@$controllerData['renderer']['blank_page'] || @$controllerData['renderer']['json'])
            {

                PerisianFrameworkToolbox::blankPage();

            }
            
            if(isset($controllerData['renderer']['disable_login_screen']))
            {
                
                $disableLoginScreen = $controllerData['renderer']['disable_login_screen'] == true;
                
            }

            $page = @$controllerData['renderer']['page'];

            if(@$controllerData['renderer']['json'])
            {
                
                echo json_encode(@$controllerData['result']);

            }

        }
        
    }
    
}
