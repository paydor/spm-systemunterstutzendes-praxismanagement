<?php

require_once "framework/controller/PerisianControllerRestObject.class.php";
require_once "framework/controller/PerisianLanguageVariableController.class.php";

class PerisianLanguageVariableControllerApi extends PerisianControllerRest
{
    
    /**
     * Handles authorization to this resource.
     * Only API system users may access this.
     * 
     * @author Peter Hamm
     * @throws RestException
     * @return bool
     */
    public function authorize()
    {
        
        $value = parent::authorize();
        
        $this->requireSystemUser();
        
        return $value;
                
    }
    
    /**
     * Returns the name of the controller class used for this object.
     * 
     * @author Peter Hamm
     * @return String
     */
    protected function getControllerClassName()
    {
        
        return "PerisianLanguageVariableController";
        
    }
        
    /**
     * Retrieves information about the system's languages.
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     */
    public function getLanguageInformation()
    {
                        
        return PerisianLanguage::getLanguages();
        
    }
        
    /**
     * Retrieves a list of language variables 
     * for the desired languages
     *
     * @author Peter Hamm
     * @url GET /variable
     * @return Array
     */
    public function getLanguageVariables()
    {
        
        $variableDummy = new PerisianLanguageVariable();
                
        $this->setAction('list');
        
        $result = $this->finalize();
        
        unset($result['list_languages']);
        
        if(isset($result['list']))
        {
            
            if(isset($result['list'][0][$variableDummy->field_pk]))
            {
              
                // Only one language
                
                for($i = 0; $i < count($result['list']); ++$i)
                {
                    
                    unset($result['list'][$i][$variableDummy->field_pk]);
                    
                }
                
            }
            else
            {
                
                // Multiple languages
                
                foreach($result['list'] as $key => $value)
                {
                    
                    for($i = 0; $i < count($result['list'][$key]); ++$i)
                    {
                        
                        unset($result['list'][$key][$i][$variableDummy->field_pk]);

                    }
                    
                }
                
            }
            
        }
        
        return $result;
        
    }
     
    /**
     * Retrieves a list of installed languages 
     * and further language information.
     *
     * @author Peter Hamm
     * @url GET /
     * @return Array
     *
    public function getLanguageInfo()
    {
                
        $this->setAction('languageInfo');
        
        return $this->finalize();
        
    }
    */
    
}
