<?php

require_once 'framework/controller/PerisianControllerRest.class.php';

abstract class PerisianControllerRestObject extends PerisianControllerRest
{
    
    /**
     * Returns the name of the standard object for this resource.
     * Should be overridden in the case you want to use it.
     * 
     * @author Peter Hamm
     * @return string
     */
    abstract protected function getObjectName();
    
    /**
     * Retrieves the last JSON error that occured, with detailed information.
     * 
     * @author Peter Hamm
     * @return mixed An array with details or null if no error occured.
     */
    public static function getLastJsonError()
    {
        
        $errorCode = json_last_error();
        
        switch($errorCode) 
        {
            
            case JSON_ERROR_NONE:
                
                return null;
                
            break;
        
            case JSON_ERROR_DEPTH:
                
                $errorMessage = 'Maximum stack depth exceeded';
                
            break;
        
            case JSON_ERROR_STATE_MISMATCH:
                
                $errorMessage = 'Underflow or the modes mismatch';
                
            break;
        
            case JSON_ERROR_CTRL_CHAR:
                
                $errorMessage = 'Unexpected control character found';
                
            break;
        
            case JSON_ERROR_SYNTAX:
               
                $errorMessage = 'Syntax error, malformed JSON';
                
            break;
        
            case JSON_ERROR_UTF8:
                
                $errorMessage = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                
            break;
        
            default:
                
                $errorMessage = 'Unknown error';
                
            break;
        
        }
        
        $data = Array(
            
            'code' => $errorCode,
            'error' => $errorMessage
            
        );
        
        return $data;
        
    }
    
    /**
     * Retrieves an object for this resource, as defined in $this->getObjectName().
     * 
     * @author Peter Hamm
     * @param mixed $identifier Optional, an identifier to load an object's data.
     * @return mixed
     * @throws PerisianException
     */
    protected function getDatabaseObject($identifier = null)
    {
        
        $objectName = $this->getObjectName();
        
        if(strlen($objectName) == 0)
        {
            
            throw new PerisianException(PerisianLanguageVariable::getLanguageVariable('10133'));
            
        }
        
        return new $objectName($identifier);;
    
    }
    
    /**
     * Deletes the object with the specified identifier from the database.
     * 
     * @author Peter Hamm
     * @param mixed $identifier
     * @return void
     */
    protected function deleteDatabaseObject($identifier = null)
    {
        
        $object = $this->getDatabaseObject($identifier);

        $result = $object->delete();
        
    }
    
    /**
     * Retrieves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @return Array
     */
    protected function getObject($identifier)
    {
                
        $object = $this->getDatabaseObject($identifier);
        
        $exposedFields = $object->getFieldValues();
        
        return $exposedFields;
        
    }
    
    /**
     * Saves a specific object by its identifier.
     *
     * @author Peter Hamm
     * @param mixed $identifier The identifier of the entry you want to patch.
     * @return Array
     */
    protected function updateObject($identifier)
    {
                
        $object = $this->getObjectWithFieldData($identifier);
        
        $returnValue = $this->saveObject($object);
        
        return $returnValue;
        
    }
    
    /**
     * Tries to save the given object.
     * 
     * @author Peter Hamm
     * @param PerisianDatabaseModel $object
     * @return Array An array with detailed info about the process.
     */
    protected function saveObject($object)
    {
        
        try
        {
            
            $saveId = $object->save();

            $returnValue['success'] = true;
            $returnValue['id'] = $saveId;
            
            if($saveId == false)
            {
                
                // 'Could not be saved.'
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10484'));
                
            }
            
        }
        catch(PerisianException $e)
        {
            
            unset($returnValue['id']);
            
            $returnValue['success'] = false;
            $returnValue['error'] = $e->getMessage();
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a specific object by its identifier (may also be zero for a dummy object)
     * and fills in the field values, then returns that object.
     * 
     * @author Peter Hamm
     * @param int $identifier
     * @return PerisianDatabaseModel
     */
    protected function getObjectWithFieldData($identifier)
    {
        
        $object = $this->getDatabaseObject($identifier);
        $exposedFields = $object->getFieldValues();
        
        $returnValue = array(
            
            'success' => false
            
        );
        
        foreach($exposedFields as $exposedFieldKey => $exposedFieldValue)
        {
                        
            $newValue = PerisianFrameworkToolbox::getRequest($exposedFieldKey);
            
            if(!is_null($newValue))
            {
                
                $object->{$exposedFieldKey} = $newValue;
                
            }
            
        }
        
        return $object;
        
    }
    
    /**
     * Deletes a specific object by its identifier.
     *
     * @author Peter Hamm
     * @return Array
     */
    protected function deleteObject($identifier)
    {
        
        $returnValue = array(
            
            'success' => false
            
        );
        
        try
        {
            
            $this->deleteDatabaseObject($identifier);
            
            $returnValue['success'] = true;
            
        }
        catch(PerisianException $e)
        {
            
            $returnValue['error'] = $e->getMessage();
            
        }
        
        return $returnValue;
        
    }
        
}