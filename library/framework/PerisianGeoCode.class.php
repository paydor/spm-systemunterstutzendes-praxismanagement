<?php

require_once 'database/PerisianDatabaseModel.class.php';

/**
 * Geo-coding for zip codes
 *
 * @author Peter Hamm
 * @date 2016-07-20
 */
class PerisianGeoCode extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'geocode_zip_coordinate';
    public $field_pk = 'geocode_zip_coordinate_zip';

    // Fields
    public $field_country_iso = 'geocode_zip_coordinate_country_iso';
    public $field_city = 'geocode_zip_coordinate_city';
    public $field_state = 'geocode_zip_coordinate_state';
    public $field_latitude = 'geocode_zip_coordinate_latitude';
    public $field_longitude = 'geocode_zip_coordinate_longitude';

    // Instance
    static private $instance = null;

    // Caching
    private $cache = Array();

    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct()
    {

        parent::__construct('MySql', 'main');

    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static private function getInstance()
    {

        if(!isset(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;

    }

    /**
     * Returns an enriched list.
     *
     * @author Peter Hamm
     * @param Array $list The list of zip codes.
     * @return Array The enriched list.
     */
    static public function getCoordinatesForZipCodes($list)
    {

        PerisianFrameworkToolbox::security($list);

        if(!is_array($list) || count($list) == 0)
        {
            
            return $list;
            
        }

        $instance = self::getInstance();
        
        $query = $instance->field_pk . " IN (";

        for($i = 0, $m = count($list); $i < $m; ++$i)
        {
            
            $zip = intval($list[$i]["zip"]);
            
            if(strlen($zip) == 0 || $zip == 0)
            {
                
                continue;
                
            }
        
            $query .= $zip;
            
            if($i < $m - 1)
            {
                
                $query .= ",";
                
            }

        }
        
        $query .= ')';

        $result = $instance->getData($query);

        return $result;

    }

    /**
     * Overwritten save method, does nothing.
     *
     * @author Peter Hamm
     * @return void
     */
    public function save()
    {

        return;

    }

}
