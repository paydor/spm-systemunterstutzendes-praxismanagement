<?php

/**
 * E-Mail distribution
 * 
 * @author Peter Hamm
 * @date 2015-08-11
 */
class PerisianMailDistribution extends PerisianDatabaseModel
{

    // Basic settings
    public $table = 'mail_distribution';
    public $field_pk = 'mail_distribution_id';

    // Table specific fields
    public $field_name = 'mail_distribution_name';
    public $field_email = 'mail_distribution_email';
    public $field_type = 'mail_distribution_type';

    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_name', 'field_email');

    /**
     * Creates a new object and loads data if desired
     *
     * @author Peter Hamm
     * @param integer $entityId Optional: specify an entry's ID to load its data, default: loads no data
     * @return void
     */
    public function __construct($entityId = 0)
    {

        if(!PerisianValidation::checkId($entityId))
        {
            throw new PerisianException(PerisianLanguageVariable::getVariable());;
        }
        
        parent::__construct('MySql', 'main');

        if(!empty($entityId))
        {
            
            $query = "{$this->field_pk} = '{$entityId}'";
            $this->loadData($query);

        }

        return;

    }

    /**
     * Outputs a list of entries
     *
     * @author Peter Hamm
     * @param String $typeFilter Optional: The type to filter by. 
     * @param String $sortBy Optional: Which row to sort by, default: primary key (identifier)
     * @param String $sortOrder Optional: DESC or ASC, default: DESC
     * @param int $start Optional: Entry offset, default: 0
     * @param int $limit Optional: Entry limit, default: 50
     * @return Array A list of entries
     */
    static public function getMailList($typeFilter = '', $sortBy = 'field_name', $sortOrder = 'ASC', $start = 0, $limit = 0)
    {

        PerisianFrameworkToolbox::security($typeFilter);

        $object = new self();
        
        $query = "";
        
        if(strlen($typeFilter) > 0)
        {
            
            $query = "{$object->field_type} = \"{$typeFilter}\"";
            
        }
        
        if($sortBy == "field_name")
        {
            
            $sortBy = $object->field_name;
            
        }
        
        $results = $object->getData($query, $sortBy, $sortOrder, $start, $limit);

        
        return $results;

    }

    /**
     * Returns the count of entries
     *
     * @author Peter Hamm
     * @param String $search Optional: A search string, default: ''
     * @return int
     */
    static public function getMailCount($search = '')
    {

        PerisianFrameworkToolbox::security($search);

        $object = new self();
        $query = $object->buildSearchString($search);

        return $object->getCount($query);

    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific user
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
        }
        
        return false;

    }

    /**
     * Deletes an entry by identifier
     * 
     * @param int $entityId Optional: An entry's ID, default: ID of the object
     * @return void
     */
    public function deleteRecipient($entityId = 0)
    {

        PerisianFrameworkToolbox::security($entityId);

        if(empty($entityId))
        {
            $entityId = $this->{$this->field_pk};
        }
        
        $query = "{$this->field_pk} = {$entityId}";
        return $this->delete($query, '', '', '', 1);

    }

}

/**
 * E-Mail distribution type
 * 
 * @author Peter Hamm
 * @date 2015-08-11
 */
class PerisianMailDistributionType extends PerisianDatabaseModel
{

    // Basic settings
    public $table = 'mail_distribution_type';
    public $field_pk = 'mail_distribution_type_id';

    // Table specific fields
    public $field_value = 'mail_distribution_type_value';
    public $field_language_variable = 'mail_distribution_type_language_variable';

    // Which fields to search by default
    protected $searchFields = Array('field_pk', 'field_value');

    /**
     * Creates a new object and loads data if desired
     *
     * @author Peter Hamm
     * @param integer $entityId Optional: specify an entry's ID to load its data, default: loads no data
     * @return void
     */
    public function __construct($entityId = 0)
    {

        if(!PerisianValidation::checkId($entityId))
        {
            throw new PerisianException(PerisianLanguageVariable::getVariable());;
        }
        
        parent::__construct('MySql', 'main');

        if(!empty($entityId))
        {
            
            $query = "{$this->field_pk} = '{$entityId}'";
            $this->loadData($query);

        }

        return;

    }

    /**
     * Outputs a list of entries
     *
     * @author Peter Hamm
     * @param String $sortBy Optional: Which row to sort by, default: primary key (identifier)
     * @param String $sortOrder Optional: DESC or ASC, default: DESC
     * @param int $start Optional: Entry offset, default: 0
     * @param int $limit Optional: Entry limit, default: 50
     * @param String $search Optional: An individual search term, default: ''
     * @return Array A list of entries
     */
    static public function getTypeList($sortBy = 'pk', $sortOrder = 'ASC', $start = 0, $limit = 0, $search = '')
    {

        PerisianFrameworkToolbox::security($search);

        $object = new self();
        $query = $object->buildSearchString($search);
        
        $results = $object->getData($query, $sortBy, $sortOrder, $start, $limit);

        // If a search was performed, the search string is being highlighted
        if($search != '')
        {

            for($i = 0, $m = count($results); $i < $m; ++$i)
            {

                foreach($results[$i] as &$field)
                {

                    PerisianFrameworkToolbox::highlightText($search, $field);

                }

            }

        }
        
        return $results;

    }

    /**
     * Returns the count of entries
     *
     * @author Peter Hamm
     * @param String $search Optional: A search string, default: ''
     * @return int
     */
    static public function getTypeCount($search = '')
    {

        PerisianFrameworkToolbox::security($search);

        $object = new self();
        $query = $object->buildSearchString($search);

        return $object->getCount($query);

    }

    /**
     * Loads all of an entry's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific user
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);

            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
        }
        
        return false;

    }

}
