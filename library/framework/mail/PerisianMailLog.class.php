<?php

require_once 'framework/database/PerisianDatabaseModel.class.php';
require_once 'PerisianMail.class.php';

/**
 * Log and display outgoing system mails.
 * 
 * @author Peter Hamm
 * @date 2016-05-07
 */
class PerisianMailLog
{  
    
    /**
     * Retrieves whether the mail log is enabled or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isEnabled()
    {
        
        return PerisianSystemSetting::getSettingValue('system_default_smtp_is_enabled_mail_log') == 1;
        
    }
    
    /**
     * Clears the whole mail log.
     * 
     * @author Peter Hamm
     * @return void
     */
    public static function emptyLog()
    {
        
        $entryDummy = new PerisianMailLogEntry();
        
        $entryDummy->delete('true');
        
    }
    
    public static function decodeString($string)
    {
        
        $string = str_replace("u00", "\u00", $string);
        $string = preg_replace('/\\\\u([0-9a-f]{4})/ie', 'mb_convert_encoding("&#x$1;", "UTF-8", "HTML-ENTITIES")', $string);

        return $string;
        
    }
    
    /**
     * Retrives a list of log entries for the specified parameters.
     * 
     * @author Peter Hamm
     * @param String $search
     * @param String $order Optional. The sort order, either "ASC" or "DESC", default: "DESC".
     * @param int $offset Optional, default: 0
     * @param int $limit Optional, default: 25
     */
    static public function getLog($search, $order = "DESC", $offset = 0, $limit = 25)
    {
        
        $entryDummy = new PerisianMailLogEntry();
        
        $order = strtoupper($order);
        $order = ($order != "ASC" && $order != "DESC") ? "DESC" : $order;
        
        $retrievedData = $entryDummy->getData($entryDummy->buildSearchString($search, $entryDummy->field_content), $entryDummy->field_date_created, $order, $offset, $limit);
        
        $formattedReturnData = array();
        
        for($i = 0; $i < count($retrievedData); ++$i)
        {
            
            $entryData = array(
                
                "id" => $retrievedData[$i][$entryDummy->field_pk],
                "status" => $retrievedData[$i][$entryDummy->field_status],
                "status_message" => $retrievedData[$i][$entryDummy->field_status_message],
                "date_created" => $retrievedData[$i][$entryDummy->field_date_created],
                "data" => PerisianMailLogEntry::unserializeContent($retrievedData[$i][$entryDummy->field_content])
                
            );
                        
            array_push($formattedReturnData, $entryData);
            
        }
        
        return $formattedReturnData;
        
    }

    /**
     * Retrives the count of log entries for the specified parameters.
     * 
     * @author Peter Hamm
     * @param String $search
     * @return int
     */
    static public function getLogCount($search)
    {
        
        $entryDummy = new PerisianMailLogEntry();
        $entryCount = $entryDummy->getCount($entryDummy->buildSearchString($search, $entryDummy->field_content));
        
        return $entryCount;
        
    }
    
    /**
     * Sends an email with the specified data to the specified recipient.
     * 
     * @author Peter Hamm
     * @param String $recipientEmail
     * @param String $subject
     * @param String $content
     * @return boolean
     * @throws PerisianException
     */
    static public function sendMail($recipientEmail, $subject, $content)
    {
        
        if(strlen($recipientEmail) < 6)
        {

            // Invalid email address
            throw new PerisianException(PerisianLanguageVariable::getVariable('10094'));

        }
        
        {
            
            $content = nl2br($content);

            $mailContent = PerisianMail::getDefaultEmailTemplateContent($subject, $content);
            
            $mailMessage = PerisianMail::getMailMessage(array(

                'html' => $mailContent,
                'subject' => $subject,
                'to' => array('mail' => $recipientEmail)

            ));
            
            PerisianMail::sendMail($mailMessage);
            
        }
        
        return true;
         
    }
    
}

/**
 * A single mail log entry
 *
 * @author Peter Hamm
 * @date 2016-05-07
 */
class PerisianMailLogEntry extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'mail_log';
    public $field_pk = 'mail_log_id';

    // Fields
    public $field_content = 'mail_log_content';
    public $field_status = 'mail_log_status';
    public $field_status_message = 'mail_log_status_message';
    public $field_date_created = 'mail_log_date_created';
    
    protected $possibleStates = array('pending', 'failed', 'sent');

    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct()
    {

        parent::__construct('MySql', 'main');

    }
    
    /**
     * Sets the specified data / object as the mail's content 
     * 
     * @author Peter Hamm
     * @param Object $content
     */
    public function setContent($content)
    {
        
        $this->{$this->field_content} = static::serializeContent($content);
        
    }
    
    /**
     * Serializes the specified $content so it can be savely stored to the database.
     * 
     * @author Peter Hamm
     * @param Array $content
     * @return String
     */
    public static function serializeContent($content)
    {
        
        return PerisianFrameworkToolbox::security(json_encode($content));
        
    }
    
    /**
     * Unserializes the specified $content String so it can be used as an associative array again.
     * 
     * @author Peter Hamm
     * @param String $content
     * @return Array
     */
    public static function unserializeContent($content)
    {
               
        return json_decode(PerisianFrameworkToolbox::securityRevert($content), true);
        
    }
    
    /**
     * Sets the specified data / object as the mail's content 
     * 
     * @author Peter Hamm
     * @param Object $content
     */
    public function getContent()
    {
        
        $content = static::unserializeContent($this->{$this->field_content});
        
    }
    
    /**
     * Fired directly before the object is saved to the database.
     * 
     * @author Peter Hamm
     * @return bool If set to true, the save will proceed. On false, saving is canceled.
     */
    protected function onSaveStarted()
    {
       
        if(!isset($this->{$this->field_date_created}))
        {
            
            $this->{$this->field_date_created} = PerisianFrameworkToolbox::sqlTimestamp();
            
        }
        
        return true;
        
    }
    
    /**
     * Retrieves a formatted string for the specified status enumerator.
     * 
     * @author Peter Hamm
     * @param String $status
     * @return String
     */
    public static function getFormattedStatus($status)
    {
        
        $statusLanguageVariable = 11167;
        
        if($status == 'failed')
        {
            
            $statusLanguageVariable = 10292;
            
        }
        else if($status == 'sent')
        {
            
            $statusLanguageVariable = 11168;
            
        }
        
        return PerisianLanguageVariable::getVariable($statusLanguageVariable);
        
    }
    
    /**
     * Retrieves a formatted string for the specified status enumerator.
     * 
     * @author Peter Hamm
     * @param String $status
     * @return String
     */
    public static function getFormattedStatusHtml($status)
    {
        
        $statusText = static::getFormattedStatus($status);
        
        $statusClass = 'info';
        
        if($status == 'failed')
        {
            
            $statusClass = 'danger';
            
        }
        else if($status == 'sent')
        {
            
            $statusClass = 'success';
            
        }
        
        $returnValue = '<span class="alert alert-block alert-' . $statusClass . '" style="width:100%">' . $statusText . '</span>';
        
        return $returnValue;
        
    }
    
}