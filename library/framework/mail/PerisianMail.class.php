<?php

require_once 'PerisianMailSendInBlue.class.php';
require_once 'PerisianMailLog.class.php';
require_once 'external/Zend/Loader/StandardAutoloader.php';

$loader = new Zend\Loader\StandardAutoloader(array('autoregister_zf' => true));
$loader->register();

use Zend\Mime;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class PerisianMail
{
    
    const MAIL_SERVICE_SMTP = 'smtp';
    const MAIL_SERVICE_SENDINBLUE = 'sendinblue';
    
    /**
     * Retrieves the list of available mail services.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getMailServices()
    {
                
        $services = Array();
                
        $services[static::MAIL_SERVICE_SMTP] = lg('p60279b575f3f1');
        $services[static::MAIL_SERVICE_SENDINBLUE] = lg('p60279b45cd3a5');
        
        return $services;
        
    }
    
    /**
     * Retrieves which mail service is currently enabled.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getConfiguredMailService()
    {
                
        if(PerisianSystemSetting::getSettingValue('system_default_smtp_service') == static::MAIL_SERVICE_SENDINBLUE)
        {
            
            return static::MAIL_SERVICE_SENDINBLUE;
            
        }
        
        return static::MAIL_SERVICE_SMTP;
        
    }
    
    /*
     * To setup Gmail, make sure to follow these steps:
     * 1) Login to your gmail account.
     * 2) Go to https://www.google.com/settings/security/lesssecureapps and Turn On this feature.
     * 3) Go to https://accounts.google.com/DisplayUnlockCaptcha and click Continue.
     */
    
    /**
     * Retrieves an associative array that can be logged from the specified Zend Mail Message
     * 
     * @author Peter Hamm
     * @date 2016-05-07
     * @param Zend\Mail\Message $message
     * @return array
     */
    public static function getLogDataFromZendMessage(Zend\Mail\Message $message)
    {
        
        $recipients = static::getAdressesFromZendAdressList($message->getTo());
        $recipientsCarbonCopy = static::getAdressesFromZendAdressList($message->getCc());
        $recipientsBlindCarbonCopy = static::getAdressesFromZendAdressList($message->getBcc());
                
        $returnValue = array(
            
            "recipients" => $recipients,
            "recipientsCarbonCopy" => $recipientsCarbonCopy,
            "recipientsBlindCarbonCopy" => $recipientsBlindCarbonCopy,
            "subject" => $message->getSubject()
            
        );
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a formatted list of mail addresses from a Zend Mail AddressList object.
     * 
     * @author Peter Hamm
     * @date 2016-05-07
     * @param Zend\Mail\AddressList $addressList
     * @return array
     */
    protected static function getAdressesFromZendAdressList(Zend\Mail\AddressList $addressList)
    {
        
        $returnValue = array();
        
        foreach($addressList as $address)
        {
            
            $newAddress = array(
                
                "name" => $address->getName(),
                "address" => $address->getEmail(),
                
            );
            
            array_push($returnValue, $newAddress);
            
        }
        
        
        return $returnValue;
        
    }
    
    /**
     * Tries to send an e-mail with the specified data.
     * 
     * @author Peter Hamm
     * @param Zend\Mail\Message $message
     * @return Object The result
     */
    public static function sendMail($message)
    {
        
        // Build the log entry
        if(PerisianMailLog::isEnabled())
        {
            
            $loggableData = static::getLogDataFromZendMessage($message);
        
            $mailLogEntry = new PerisianMailLogEntry();
            $mailLogEntry->setContent($loggableData);
                    
        }
        
        try
        {

            if(PerisianMailSendInBlue::isEnabled())
            {
                
                // Send via the SendInBlue API:
                
                PerisianMailSendInBlue::sendMail($message);
                
            }
            else
            {
                
                // Send via SMTP
                
                $mailTransport = self::getMailTransportSmtp();

                $mailTransport->send($message);

            }
            
            // Log it
            if(PerisianMailLog::isEnabled())
            {
                
                $mailLogEntry->{$mailLogEntry->field_status} = 'sent';
                
                $mailLogEntry->save();
                
            }
            
        }
        catch(Exception $e)
        {
                        
            // Log it
            if(PerisianMailLog::isEnabled())
            {
                
                $mailLogEntry->{$mailLogEntry->field_status_message} = print_r(addslashes($e->getMessage()), true);
                $mailLogEntry->{$mailLogEntry->field_status} = 'failed';
                
                $mailLogEntry->save();
                
            }
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10756) . " - " . $e->getMessage());
            
        }
                
        return true;
        
    }
    
    /**
     * Generates a Zend\Mail\Message with the specified message data.
     * 
     * @param array $mailData
     * @return \Zend\Mail\Message
     * @throws PerisianException
     */
    public static function getMailMessage($mailData)
    {
                
        if(!isset($mailData['subject']))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10754));
            
        }
        
        if(isset($mailData['to']) && is_array($mailData['to']) && isset($mailData['to']['mail']))
        {
            
            $mailData['to'] = array($mailData['to']);
            
        }
        
        if(!isset($mailData['to']) || !isset($mailData['to']['0']))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10755));
            
        }
        
        $mailBody = self::getMailBodyMimeHtml($mailData['html']);
        
        {
            
            $senderEmail = self::getMailSenderAddress();
            $senderName = self::getMailSenderName();
                        
        }

        {

            $message = new Zend\Mail\Message();
            
            $message->setSubject($mailData['subject']);
            $message->setBody($mailBody);
                        
            $message->setFrom($senderEmail, $senderName);
            
            $headers = $message->getHeaders();
            $headers->removeHeader('Content-Type');
            $headers->addHeaderLine('Content-Type', 'text/html; charset=UTF-8');
                                    
            for($i = 0; $i < count($mailData['to']); ++$i)
            {
                
                $recipientName = static::encodeMailString(@$mailData['to'][$i]['name']);
                
                $message->addTo($mailData['to'][$i]['mail'], $recipientName);
                
            }
            
        }
        
        return $message;
        
    }
    
    /**
     * Tries to retrieve MIME info for the specified file name
     * 
     * @author Peter Hamm
     * @param String $fileName
     * @return array
     */
    public static function getMimeForFile($fileName)
    {
        
        $extension = strtolower(str_replace(".", "", substr($fileName, strrpos($fileName, '.'))));
        
        $value = array (
            
            'type' => 'plain/text',
            'encoding' => Zend\Mime\Mime::TYPE_TEXT
            
        );
        
        switch($extension)
        {
            
            case 'ical':
            case 'ics':
                
                $value['type'] = "text/calendar";
                $value['encoding'] = Zend\Mime\Mime::ENCODING_BASE64;
                
                break;
            
            case 'xls':
            case 'xlsx':
                
                $value['type'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                $value['encoding'] = Zend\Mime\Mime::ENCODING_BASE64;
                
                break;
            
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'jpe':
            case 'bmp':
            case 'gif':
                
                $value['type'] = "image/" . $extension;
                $value['encoding'] = Zend\Mime\Mime::ENCODING_BASE64;
                
                break;
            
            case 'zip':
            case 'rar':
                
                $value['type'] = 'application/zip';
                $value['encoding'] = Zend\Mime\Mime::ENCODING_BASE64;
                
                break;
            
        }
        
        return $value;
        
    }
    
    /**
     * Adds the specified attachment to the specified message. 
     * 
     * @author Peter Hamm
     * @param \Zend\Mail\Message $message
     * @param array $attachment An array containing the key/value pairs "name" and "path"
     */
    public static function addAttachmentToMessage(&$message, $attachment)
    {
        
        if(!isset($attachment['path']))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10752));
            
        }
        
        if(!isset($attachment['name']))
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10753));
            
        }
        
        // Build the mime part
        {
            
            $mimeInfo = self::getMimeForFile($attachment['name']);
            $attachmentContent = fopen($attachment['path'], "r");
            
            $attachmentMimePart = new Mime\Part($attachmentContent);
            $attachmentMimePart->filename = $attachment['name'];
            $attachmentMimePart->type = $mimeInfo['type'];
            $attachmentMimePart->encoding = $mimeInfo['encoding'];
            $attachmentMimePart->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
           
        }
        
        // Put it into the message
        {
            
            $mimeMessage = $message->getBody();
            
            $parts = $mimeMessage->getParts();
            $parts[] = $attachmentMimePart;
            
            $newMimeMessage = new Mime\Message();
            $newMimeMessage->setParts($parts);
            
            $message->setBody($newMimeMessage);
            
        }
        
    }
    
    /**
     * Takes a plain text message and puts it into a Zend\Mime\Message.
     * @param type $mailContentPlain
     * @return \Zend\Mime\Message
     */
    public static function getMailBodyMimeHtml($mailContentPlain)
    {

        $mailHtml = new Zend\Mime\Part($mailContentPlain);
        $mailHtml->type = "text/html";
        
        $mailBody = new Zend\Mime\Message();
        $mailBody->setParts(array($mailHtml));
        
        return $mailBody;
        
    }

    /**
     * Retrieves a fully set-up Zend_Mail_Transport_Smtp with data from the configured server.
     * 
     * @return Zend\Mail\Transport\Smtp
     */
    protected static function getMailTransportSmtp()
    {
            
        $server = PerisianSystemSetting::getSettingValue('system_default_smtp_server');
        $port = PerisianSystemSetting::getSettingValue('system_default_smtp_server_port');
        $userName = PerisianSystemSetting::getSettingValue('system_default_smtp_user');
        $password = PerisianSystemSetting::getSettingValue('system_default_smtp_password');
                
        $mailConfig = Array(
            
            //'name' => $_SERVER['SERVER_NAME'],
            'host' => $server,
            'port' => $port,
            'connection_class' => 'login',
            'connection_config' => array(
                
                'username' => $userName,
                'password' => $password
                
            )
            
        );
        
        if(PerisianSystemSetting::getSettingValue('system_default_smtp_server_ssl_active') == 1)
        {
                                    
            if(strtolower($mailConfig['host']) == 'smtp.gmail.com' && (int)$mailConfig['port'] == 587)
            {
                
                $mailConfig['connection_config']['ssl'] = 'tls';
                
            }
            else if(strtolower($mailConfig['host']) == 'mail.gmx.net')
            {
                
                $mailConfig['connection_config']['ssl'] = 'tls';
                
            }
            else if(strtolower($mailConfig['host']) == 'smtp.ionos.de' || strtolower($mailConfig['host']) == 'smtp.ionos.com')
            {
                
                $mailConfig['connection_config']['ssl'] = 'tls';
                
            }
            else
            {
                
                $mailConfig['connection_config']['ssl'] = 'ssl';
            
            }
                        
        }
        
        $mailTransportOptions = new Zend\Mail\Transport\SmtpOptions($mailConfig);
        
        $mailTransport = new Zend\Mail\Transport\Smtp();
        $mailTransport->setOptions($mailTransportOptions);
        
        return $mailTransport;
    
    }
    
    /**
     * Retrieves an e-mail sender name that is set in the system configuration.
     * 
     * @author Peter Hamm
     * @param bool $encode Optional: Whether to encode the string or not. Default: true.
     * @return string
     */
    public static function getMailSenderName($encode = true)
    {
        
        $title = PerisianSystemSetting::getSettingValue('title_website');
        $override = PerisianSystemSetting::getSettingValue('system_default_smtp_reply_to_name');
        
        if(strlen($override) > 0 && $override != "0")
        {
            
            $title = $override;
            
        }
                
        $senderName = $encode ? static::encodeMailString($title) : $title;
        
        return $senderName;
        
    }
    
    /**
     * Retrieves the sender e-mail address that is set in the system configuration.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getMailSenderAddress()
    {
        
        $address = PerisianSystemSetting::getSettingValue('system_default_smtp_user');
        $override = PerisianSystemSetting::getSettingValue('system_default_smtp_reply_to_address');
        
        if(strlen($override) > 0 && $override != "0")
        {
            
            $address = $override;
            
        }
        
        return $address;
        
    }
    
    /**
     * Encodes a given string to a proper format so it can be set as a recipient or sender with Zend::Mail.
     * This is required in order to prevent Zend's built-in CRLF injection detection to cause problems.
     * 
     * @author Peter Hamm
     * @param String $senderName
     * @return String
     */
    public static function encodeMailString($senderName)
    {
        
        $maxLength = 255;
        
        $senderName = strip_tags($senderName);
        $senderName = strlen($senderName) > $maxLength ? (substr($senderName, 0, $maxLength - 3) . '...') : $senderName;
        $senderName = Zend\Mime\Mime::encodeQuotedPrintableHeader($senderName, 'UTF-8', $maxLength);
        
        return $senderName;
        
    }
    
    /**
     * Retrieves the default e-mail template from the system
     * and puts the specified subject and the content inside.
     * 
     * @author Peter Hamm
     * @param String $subject
     * @param String $content
     * @return type
     */
    public static function getDefaultEmailTemplateContent($subject, $content)
    {
        
        global $style;
     
        $template = file_get_contents(PerisianFrameworkToolbox::getConfig('basic/project/folder') . "templates/{$style}/calsy/email/email_600.phtml");
        
        $imageHeaderUrl = PerisianFrameworkToolbox::getServerAddress(false) . 'assets/img/email-logo-header-dynamic.png';
        
        $template = str_replace("{{imageHeader}}", $imageHeaderUrl, $template);
        $template = str_replace("{{subject}}", $subject, $template);
        $template = str_replace("{{content}}", $content, $template);
        
        // Styling
        {
            
            $systemColors = PerisianSystemConfiguration::getSystemColors();
        
            $template = str_replace("{{color-header}}", $systemColors["main_alternative_two"], $template);
            
        }
        
        return $template;
        
    }

}
