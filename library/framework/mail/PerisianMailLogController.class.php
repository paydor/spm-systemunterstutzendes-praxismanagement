<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'PerisianMailLog.class.php';

/**
 * Email log controller
 *
 * @author Peter Hamm
 * @date 2017-05-23
 */
class PerisianMailLogController extends PerisianController
{
    
    /**
     * Provides an overview of entries.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $this->actionList();
        
        //
        
        $renderer = array(
            
            'page' => 'mail/log/overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
    }
    
    /**
     * Provides a list of entries, filtered by the specified parameters.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionList()
    {
        
        $dummy = new CalsyArea();

        $offset = $this->getParameter('offset', 0);
        $limit = $this->getParameter('limit', 25);
        $sortOrder = $this->getParameter('order', 'ASC');
        $sorting = $this->getParameter('sorting', $dummy->field_title);
        $search = $this->getParameter('search', '');
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'mail/log/list'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'list' => PerisianMailLog::getLog($search, 'DESC', $offset, $limit),
            'count' => PerisianMailLog::getLogCount($search),
            
            'offset' => $offset,
            'limit' => $limit,
            'order' => $sortOrder,
            'sorting' => $sorting,
            'search' => $search
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionNewMail()
    {
        
        $dummyUser = new CalsyUserBackend();
        
        $renderer = array(
            
            'blank_page' => true,
            'page' => 'mail/log/form_mail'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'title_form' => PerisianLanguageVariable::getVariable('p5924588aaf696')
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Sends an email
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSendMail()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array('success' => false);
        
        // Parameter setup
        {
            
            $recipient = $this->getParameter('recipient');
            $subject = $this->getParameter('subject');
            $content = $this->getParameter('content');
            
        }
        
        try
        {
            
            if(strlen($recipient) < 6)
            {
                
                // Invalid email address
                throw new PerisianException(PerisianLanguageVariable::getVariable('10094'));
                
            }
            
            $resultSuccessful = PerisianMailLog::sendMail($recipient, $subject, $content);
            
            $result = array('success' => $resultSuccessful);
            
        }
        catch(Exception $e)
        {
            
            $result = array(

                'success' => false,
                'message' => PerisianLanguageVariable::getVariable('10756') . ' "' . $e->getMessage() . '"'

            );
            
        }
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Empties the complete email log
     * 
     * @author Peter Hamm
     * @global PerisianUser $user
     * @throws PerisianException
     * @return void
     */
    protected function actionEmptyLog()
    {
        
        global $user;
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        if(!$user->isAdmin())
        {

            throw new PerisianException(PerisianLanguageVariable::getVariable(10130));

        }

        PerisianMailLog::emptyLog();

        $result = array(

            'success' => true

        );

        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}