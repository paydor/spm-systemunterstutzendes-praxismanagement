<?php

if(!function_exists('curl_reset'))
{
    
    function curl_reset(&$ch)
    {
        
        $ch = curl_init();
        
    }
    
}

require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/PromiseInterface.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/Promise.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/functions_include.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/ClientInterface.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Client.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/RequestOptions.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Utils.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/HandlerStack.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Exception/GuzzleException.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Exception/TransferException.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Exception/RequestException.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Exception/BadResponseException.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Exception/ClientException.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/Proxy.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/CurlMultiHandler.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/EasyHandle.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/CurlFactoryInterface.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/CurlFactory.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/CurlHandler.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Handler/StreamHandler.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/TaskQueueInterface.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/TaskQueue.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Promise/FulfilledPromise.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Middleware.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/RedirectMiddleware.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/PrepareBodyMiddleware.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/functions_include.php';

require_once 'external/guzzle-6.5.5/Psr/Http/Message/MessageInterface.php';
require_once 'external/guzzle-6.5.5/Psr/Http/Message/RequestInterface.php';
require_once 'external/guzzle-6.5.5/Psr/Http/Message/ResponseInterface.php';
require_once 'external/guzzle-6.5.5/Psr/Http/Message/UriInterface.php';
require_once 'external/guzzle-6.5.5/Psr/Http/Message/StreamInterface.php';

require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/MessageTrait.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/Uri.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/Response.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/Request.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/Stream.php';
require_once 'external/guzzle-6.5.5/GuzzleHttp/Psr7/functions_include.php';

require_once 'external/sendinblue-api-v3-7.1.0/lib/Configuration.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/ApiException.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/Api/TransactionalEmailsApi.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/Model/ModelInterface.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/Model/ErrorModel.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/Model/SendSmtpEmail.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/Model/CreateSmtpEmail.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/HeaderSelector.php';
require_once 'external/sendinblue-api-v3-7.1.0/lib/ObjectSerializer.php';

/**
 * SendInBlue email service connector class.
 * API: https://developers.sendinblue.com/reference
 * 
 * @author Peter Hamm
 * @date 2020-10-23
 */
class PerisianMailSendInBlue
{
    
    /**
     * Retrieves whether the SendInBlue service is activated to send the system's emails or not.
     * 
     * @author Peter Hamm
     * @return boolean
     */
    public static function isEnabled()
    {
        
        return PerisianSystemSetting::getSettingValue('system_default_smtp_service') == PerisianMail::MAIL_SERVICE_SENDINBLUE;
        
    }
    
    /**
     * Returns the API key for the SendInBlue service.
     * 
     * @author Peter Hamm
     * @return String
     */
    private static function getApiKey()
    {
                  
        return PerisianSystemSetting::getSettingValue('system_default_smtp_service_sendinblue_api_key');
        
    }
    
    /**
     * Retrieves the name of the sender of the emails sent out via the SendInBlue email service.
     * 
     * @author Peter Hamm
     * @return String
     */
    private static function getSenderName()
    {
        
        $senderName = PerisianSystemSetting::getSettingValue(PerisianFrameworkToolbox::getConfig('system/setting_id/page_title'));
        
        $overwrittenSenderName = PerisianSystemSetting::getSettingValue('system_default_smtp_reply_to_name');
        
        if(strlen($overwrittenSenderName) > 0 && $overwrittenSenderName != "0")
        {
            
            $senderName = $overwrittenSenderName;
            
        }
        
        return $senderName;
        
    }
    
    /**
     * Retrieves the email address of the sender of the emails sent out via the SendInBlue email service.
     * 
     * @author Peter Hamm
     * @return String
     */
    private static function getSenderEmailAddress()
    {
        
        $defaultSenderAddress = "info@calsy.eu";
        
        $overwrittenAddress = PerisianSystemSetting::getSettingValue('system_default_smtp_reply_to_address');
        
        if(strlen($overwrittenAddress) > 0)
        {
            
            $defaultSenderAddress = $overwrittenAddress;
            
        }
        
        return $defaultSenderAddress;
        
    }
    
    /**
     * Sends the specified email via the SendInBlue service.
     * 
     * @author Peter Hamm
     * @throws PerisianException
     * @param Zend\Mail\Message $message
     * @return boolean Indicates whether the mail was successfully sent or not.
     */
    public static function sendMail(Zend\Mail\Message $message)
    {
        
        $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', static::getApiKey());

        $apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
                
            new GuzzleHttp\Client(),
            $config
                
        );
        
        $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
        
        $sendSmtpEmail['subject'] = $message->getSubject();
        
        {
            
            $sendSmtpEmail['htmlContent'] = '';

            $zendBody = $message->getBody();
            
            foreach($zendBody->getParts() as $zendPart)
            {
                
                if($zendPart->getType() =='text/html')
                {

                    $sendSmtpEmail['htmlContent'] .= $zendPart->getContent();

                }

            }
                        
        }
        
        $sendSmtpEmail['sender'] = array(
            
            'email' => static::getSenderEmailAddress(),
            'name' => static::getSenderName()
            
        );
        
        // Recipients
        {
            
            $listTo = static::getRecipients($message, 'to');
            $listCc = static::getRecipients($message, 'cc');
            $listBcc = static::getRecipients($message, 'bcc');
                        
            if(count($listTo) > 0)
            {
                
                $sendSmtpEmail['to'] = $listTo;
                
            }   
            
            if(count($listCc) > 0)
            {
                
                $sendSmtpEmail['cc'] = $listCc;
                
            }     
            
            if(count($listBcc) > 0)
            {
                
                $sendSmtpEmail['bcc'] = $listBcc;
                
            }
                        
        }
        
        $sendSmtpEmail['replyTo'] = array(
            
            'email' => static::getSenderEmailAddress(),
            'name' => static::getSenderName()
            
        );
        
        //$sendSmtpEmail['headers'] = array('Some-Custom-Name' => 'unique-id-1234');
        //$sendSmtpEmail['params'] = array('parameter' => 'My param value', 'subject' => 'New Subject');

        try 
        {
            
            $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
            
            return true;
            
        }
        catch(Exception $e) 
        {
            
            $exception = new PerisianException('Exception when calling TransactionalEmailsApi->sendTransacEmail: ' . $e->getMessage());
                        
            throw $exception;
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves the recipients from the specified Zend message as an array.
     * 
     * @author Peter Hamm
     * @param Zend\Mail\Message $zendMessage
     * @param String $recipientType Optional, can be "to", "cc" or "bcc". Default: "to".
     * @return Array
     */
    private static function getRecipients(Zend\Mail\Message $zendMessage, $recipientType = 'to')
    {
        
        if(!in_array($recipientType, Array('to', 'cc', 'bcc')))
        {
            
            return Array();
            
        }
        
        $zendRecipientList = Array();
        
        if($recipientType == 'to')
        {
            
            $zendRecipientList = $zendMessage->getTo();
            
        }
        else if($recipientType == 'cc')
        {
            
            $zendRecipientList = $zendMessage->getCc();
            
        }
        else if($recipientType == 'to')
        {
            
            $zendRecipientList = $zendMessage->getBcc();
            
        }

        $recipientList = Array();
        $recipientIndex = 0;

        foreach($zendRecipientList as $zendRecipient)
        {

            $recipientEmail = $zendRecipient->getEmail();
            $recipientName = $zendRecipient->getName();

            if($recipientName == '=?UTF-8?Q??=')
            {

                $recipientName = substr($recipientEmail, 0, strpos($recipientEmail, "@"));

            }

            $recipientEntry = Array(

                'email' => $recipientEmail, 
                'name' => $recipientName

            );


            //$sendSmtpEmail['to'][] = $recipientEntry;

            array_push($recipientList, $recipientEntry);

            ++$recipientIndex;

        }
        
        return $recipientList;
        
    }
    
}