<?php

/**
 * Use this singleton class to handle sessions.
 * 
 * @author Peter Hamm
 * @date 2010-10-24
 */
class PerisianSession
{

    // The first instance of this class is stored here
    static private $instance = null;

    /**
     * Starts a new instance or gets the existing one
     *
     * @author Peter Hamm
     * @return Object
     */
    static private function getInstance()
    {

        if(self::$instance === null)
        {
            
            self::$instance = new self;
            
        }

        return self::$instance;

    }

    /**
     * Handler to get session variables
     *
     * @author Peter Hamm
     * @param String $name The name of the variable
     */
    static public function getSessionValue($name)
    {
        
        global $_SESSION;

        PerisianFrameworkToolbox::security($name);
        
        $prefix = PerisianFrameworkToolbox::getConfig('basic/project/name') . '-';
        $name = $prefix . $name;

        $instance = self::getInstance();

        if(isset($_SESSION[$name]))
        {
            
            return $_SESSION[$name];
            
        }

        return '';

    }

    /**
     * Handler to set session variables
     *
     * @author Peter Hamm
     * @param String $name The name of the variable
     * @param mixed $value The value of the variable
     * @return void
     */
    static public function setSessionValue($name, $value)
    {
        
        global $_SESSION;

        PerisianFrameworkToolbox::security($name);
        PerisianFrameworkToolbox::security($value);
        
        $prefix = PerisianFrameworkToolbox::getConfig('basic/project/name') . '-';
        $name = $prefix . $name;

        $instance = self::getInstance();
        $_SESSION[$name] = $value;

        return;

    }

    /**
     * Handler to unset session variables
     *
     * @author Peter Hamm
     * @param String $name The name of the variable
     * @return void
     */
    static public function unsetSessionValue($name)
    {

        global $_SESSION;

        PerisianFrameworkToolbox::security($name);
        
        $prefix = PerisianFrameworkToolbox::getConfig('basic/project/name') . '-';
        $name = $prefix . $name;

        $instance = self::getInstance();
        unset($_SESSION[$name]);

        return;

    }

    /**
     * Overwritten constructor
     * 
     * @author Peter Hamm
     * @return void
     */
    private function __construct() 
    {
        
        global $_SESSION;

        session_start();
        PerisianFrameworkToolbox::security($_SESSION);

        return;

    }

    /**
     * Overwritten clone method
     *
     * @author Peter Hamm
     * @return void
     */
    private function __clone()
    {

        throw new PerisianException('This class must not be cloned.');

    }

}
