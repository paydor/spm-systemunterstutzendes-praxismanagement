<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'modules/PerisianPayPalModule.class.php';

/**
 * Payment Controller
 *
 * @author Peter Hamm
 * @date 2017-10-28
 */
class PerisianPaymentController extends PerisianController
{
    
    /**
     * Provides data for a form to edit an existing entry
     * 
     * @author Peter Hamm
     * @return void
     *
     */
    protected function actionForwardToPayment()
    {
                
        $renderer = array(
            
            'blank_page' => 'true'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $forwardUrl = PerisianFrameworkToolbox::getServerAddress(false);
        
        // Parameter setup
        {
            
            $paymentType = $this->getParameter('t');
            $paymentId = $this->getParameter('p');
            
        }
        
        if($paymentType == 'paypal')
        {
            
            $payPal = new PerisianPayPal();
            
            $forwardUrl = $payPal->getPaymentUrlForPaymentId($paymentId);
                        
        }
        
        header("Location: " . $forwardUrl);
        
    }
    
    /**
     * Landing return page for the payment service, e.g. after fullfilling or also cancelling a payment on PayPal.
     * 
     * @author Peter Hamm
     * @return void
     *
     */
    protected function actionConfirmPayment()
    {
                        
        $renderer = array(
            
            'disable_login_screen' => true,
            'page' => 'frontend/payment/confirm'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = Array(
            
            "success" => false
            
        );
                
        // Parameter setup
        {
            
            $paymentType = $this->getParameter('t');
            $paymentSuccess = $this->getParameter('success');
            
        }
        
        if($paymentType == 'paypal')
        {            
            
            $payPal = new PerisianPayPal();
            
            $payerId = PerisianFrameworkToolbox::getRequest('PayerID');
            $paymentId = PerisianFrameworkToolbox::getRequest('paymentId');
            $isSuccess = PerisianFrameworkToolbox::getRequest('success') == 'true';
            
            $transactionResult = $payPal->handlePaymentResult($payerId, $paymentId, $isSuccess);
            
            if($transactionResult["success"])
            {
                
                // PayPal confirmed the transaction as successful.
                
                $result["success"] = true;
                $result["output"] = PerisianLanguageVariable::getVariable('p59f6781fd31e3');
                                
            }
            else
            {
                
                $result["output"] = $transactionResult["message"];
                $result["exception"] = @$transactionResult["exception"];
                
            }
            
                        
        }
        
        {

            $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('frontend_welcome_image');
            $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";

            $result['url_image_welcome'] = $customLogoUrl;
        
        }
        
        $this->setResultValue('result', $result);        
        
    }
        
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'forwardToPayment';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}