<?php

interface IPerisianPayment
{
    
    /**
     * Retrieves the name of the transaction type.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionName();
    
    /**
     * Retrieves a nicely formatted name of the transaction type.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionNameFormatted();
    
    /**
     * Retrieves the name of the field for the transaction identifier.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getFieldTransactionId();
    
    /**
     * Retrieves whether the payment for this transaction was retrieved or not.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function isPaymentReceived();
    
    /**
     * Retrieves whether the payment for this transaction is pending or not.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function isPaymentPending();
    
    /**
     * Retrieves whether the payment for this transaction is canceled or not.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function isPaymentCanceled();
    
    /**
     * Retrieves whether the payment for this transaction is invalid or not.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function isPaymentInvalid();
    
    /**
     * Retrieves the timestamp when the transaction was last updated
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionDateUpdated();
    
    /**
     * Retrieves the timestamp when the transaction was created
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getTransactionDateCreated();
    
}