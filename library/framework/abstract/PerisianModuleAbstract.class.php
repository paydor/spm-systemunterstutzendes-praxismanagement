<?php

if(!class_exists('PerisianModuleAbstract'))
{

    /**
     * Abstract class for system modules
     * 
     * @author Peter Hamm
     * @date 2016-02-08
     */
    abstract class PerisianModuleAbstract extends PerisianDatabaseModel
    {

        protected static $languageVariables = array();

        // The list of required settings for this module to be enabled.
        protected $requiredSettingNames = array();

        /**
         * Retrieves the name of this module.
         * 
         * @author Peter Hamm
         * @return string
         */
        public static function getModuleName() 
        {

            return PerisianLanguageVariable::getVariable(11072);

        }

        /**
         * Retrieves the icon of this module.
         * 
         * @author Peter Hamm
         * @return string
         */
        public static function getModuleIcon() 
        {

            return 'md-view-module';

        }

        /**
         * Retrieves the unique identifier of this module.
         * 
         * @author Peter Hamm
         * @return string
         */
        public static function getModuleIdentifier() 
        {

            return "undefined";

        }

        /**
         * Retrieves the name of the setting that determines whether this module is blocked or not.
         * 
         * @author Peter Hamm
         * @return String
         */
        private static function getSettingNameBlocked()
        {

            $returnValue = 'is_module_blocked_' . static::getModuleIdentifier();

            return $returnValue;

        }

        /**
         * Retrieves an instance of this moduke
         * 
         * @author Peter Hamm
         * @return PerisianModule
         */
        protected static function getInstance()
        {

            $instance = new static();

            return $instance;

        }

        /**
         * Retrieves if this module is blocked or not.
         * 
         * @author Peter Hamm
         * @return boolean
         */
        public function isModuleBlocked()
        {

            $isBlocked = false;

            if(PerisianSystemSetting::getSettingValue($this->getSettingNameBlocked()) == 1)
            {

                $isBlocked = true;

            }

            return $isBlocked;

        }

        /**
         * Retrieves if this module is enabled or not.
         * 
         * @author Peter Hamm
         * @return boolean
         */
        public function isModuleEnabled()
        {

            $isEnabled = true;

            for($i = 0; $i < count($this->requiredSettingNames); ++$i)
            {

                $setting = new PerisianSystemSetting($this->requiredSettingNames[$i]);

                if($setting->getValue() != 1)
                {

                    $isEnabled = false;

                    break;

                }

            }

            return $isEnabled;

        }

        /**
         * Toggles the 'enabled' status of this module.
         * 
         * @author Peter Hamm
         * @param bool $enabled
         * @return void
         */
        public function toggleEnabled($enabled)
        {

            for($i = 0; $i < count($this->requiredSettingNames); ++$i)
            {

                PerisianSystemSetting::setSettingValue($this->requiredSettingNames[$i], $enabled == true ? '1' : '0');

            }

        }

        /**
         * Toggles the 'blocked' status of this module.
         * 
         * @author Peter Hamm
         * @param bool $blocked
         * @return void
         */
        public function toggleBlocked($blocked)
        {

            PerisianSystemSetting::setSettingValue(static::getSettingNameBlocked(), $blocked == true ? '1' : '0');

        }

        /**
         * Retrieves whether this module is blocked or not.
         * If it is blocked, it should not be displayed anywhere in the system, also it will count as being disabled.
         * 
         * @author Peter Hamm
         * @return bool
         */
        public static function isBlocked()
        {

            $instance = static::getInstance();

            $returnValue = $instance->isModuleBlocked();

            return $returnValue;

        }

        /**
         * Retrieves if this module is enabled or not.
         * It will also be disabled when it is blocked.
         * 
         * @author Peter Hamm
         * @return bool
         */
        public static function isEnabled()
        {

            $instance = static::getInstance();

            $returnValue = $instance->isModuleEnabled() && !$instance->isModuleBlocked();

            return $returnValue;

        }

        /**
         * Retrieves the backend address of this module
         * 
         * @author Peter Hamm
         * @return string
         */
        public static function getBackendAddress() 
        {

            $backendAddress = '/' . static::getModuleIdentifier() . '/overview/';

            return $backendAddress;

        }

        /**
         * Retrieves the language variables associated with this module.
         * 
         * @author Peter Hamm
         * @return array
         */
        public static function getLanguageVariables()
        {

            $formattedVariables = array();

            for($i = 0; $i < count(static::$languageVariables); ++$i)
            {

                $formattedVariables[static::$languageVariables[$i]] = PerisianLanguageVariable::getVariable(static::$languageVariables[$i]);

            }

            asort($formattedVariables);

            return $formattedVariables;

        }

    }

}