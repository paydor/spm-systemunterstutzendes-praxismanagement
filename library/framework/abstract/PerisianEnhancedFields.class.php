<?php

/**
 * Class to handle additional fields
 *
 * @author Peter Hamm
 * @date 2016-04-11
 */
abstract class PerisianEnhancedFields extends PerisianDatabaseModel
{
    
    // Instance
    private static $instance = null;
    
    // Main table settings
    public $table = '';
    public $field_pk = '';
    public $field_index = '';
    public $field_identifier = '';
    public $field_page = '';
    public $field_title = '';
    public $field_type = '';
    public $field_data = '';
    
    protected $data_decoded = 'data_decoded';
    
    protected $prefix = '';
    
    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @param int $entryId Optional
     * @param String $identifier Optional
     * @return void
     */
    public function __construct($entryId = 0)
    {
        
        parent::__construct('MySql', 'main');
        
        if(!empty($entryId))
        {
            
            $this->{$this->field_pk} = $entryId;
            
            $query = "{$this->field_pk} = '{$entryId}'";
            
            $this->loadData($query);

        }

        return;

    }
    
    /**
     * Loads all of a field's data
     *
     * @author Peter Hamm
     * @param String $query A filter to select a specific field
     * @return boolean Could the data be retrieved or not?
     */
    private function loadData($query)
    {

        if($this->getCount($query) == 1)
        {

            $data = $this->getData($query);
            
            $this->fillFields($data[0]);

            return true;

        }
        else
        {
            
            throw new PerisianException(PerisianLanguageVariable::getVariable(10223));
            
        }
        
        return false;

    }
    
    /**
     * Sets this class' 'field_X' member variables with data.
     *
     * @author Peter Hamm
     * @param Array $data An associative array with field names and values
     * @return boolean Operation performed successfully or not?
     */
    protected function fillFields($data)
    {

        PerisianFrameworkToolbox::security($data);
        
        $prefix = strlen($this->prefix) > 0 ? $this->prefix : 'field_';
        
        if(!is_array($data))
        {
            
            return false;
            
        }

        foreach($data as $key => $value)
        {
            
            if(strpos($key, $prefix) == 0)
            {
                                
                $this->{$key} = PerisianFrameworkToolbox::securityRevert($value);
                
            }
            
        }

        return true;

    }
    
    /**
     * Retrieves data from the concrete class' specified $this->table.
     * 
     * @param String $where Optional: WHERE clause, without the keyword 'WHERE', default: empty
     * @param String $sortyBy Optional: The key to sort by, default: $field_index
     * @param String $order Optional: Sort results DESC or ASC, default: ASC
     * @param integer $limit Optional: Limits the output to $limit entries, default: no limit (0)
     * @param Array $fieldExcludes Optional: An array containing names of fields that shall not be retrieved, default: empty
     * @param Array $fieldList Optional, the opposite of $fieldExludes - The result will contain only the specified fields, default: empty
     * @return Array An associative array with the resulting entries
     */
    public function getData($where = '', $sortBy = 'field_index', $order = 'ASC', $start = 0, $limit = 0, $fieldExcludes = Array(), $fieldList = Array())
    {
        
        if($sortBy == 'field_index')
        {
            
            $sortBy = $this->field_index;
            
        }
        
        $results = parent::getData($where, $sortBy, $order, $start, $limit, $fieldExcludes, $fieldList);
                
        return $this->enrichResults($results);
        
    }
    
    /**
     * Retrieves data from the concrete class' specified $this->table.
     * 
     * @param Array
     * @return Array An associative array with the resulting entries
     */
    public function enrichResults($results)
    {
        
        for($i = 0; $i < count(@$results); ++$i)
        {
            
            $results[$i][$this->data_decoded] = static::decodeData($results[$i][$this->field_data]);
            
        }
        
        return $results;
        
    }
    
    /**
     * Retrieves the decoded data of this field.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public function getDecodedData()
    {
                
        return static::decodeData($this->{$this->field_data});
        
    }
    
    /**
     * Decodes the data.
     * 
     * @author Peter Hamm
     * @param type $data
     * @return type
     */
    protected static function decodeData($data)
    {
        
        return json_decode(PerisianFrameworkToolbox::securityRevert($data), true);
        
    }
    
    /**
     * Encodes the specified data for this field and stores it encoded.
     * 
     * @author Peter Hamm
     * @param Array
     * @return void
     */
    public function setDataAndEncode($data)
    {
        
        $this->{$this->field_data} = PerisianFrameworkToolbox::security(json_encode($data));
        
        return;
        
    }
    
    /**
     * Retrieves the singleton instance of this object
     * 
     * @author Peter Hamm
     * @date 2016-04-11
     * @return CalsyAppointmentField
     */
    protected static function getInstance()
    {
        
        if(!isset(self::$instance))
        {
            
            self::$instance = new static();
            
        }
        
        return self::$instance;
        
    }
    
    /**
     * Retrieves a list of all contained language variables in the specified $list
     * generated from static::getFormattedList. Can be used to preload those language variables
     * to speed up loading a lot.
     * 
     * @author Peter Hamm
     * @param Array $list
     * @return Array
     */
    static public function getLanguageVariablesFromList($list)
    {
        
        $returnValue = array();
        
        $languageVariables = array();
        $systemVariables = array(10796, 11084, 11080, 11082, 11081, 11086, 11088, 11083, 11093, 11105, 11215, 11108, 11106, 11107, 11089);
        
        for($i = 0; $i < count($list['pages']); ++$i)
        {
            
            foreach($list['pages'][$i] as $field)
            {
                
                array_push($languageVariables, $field['label']);
                
                if(@$field['data']['values'])
                {
                    
                    foreach($field['data']['values'] as $key => $value)
                    {

                        array_push($languageVariables, $value);

                    }
                    
                }
            }
            
        }
        
        $languageVariables = array_merge($languageVariables, $systemVariables);
        
        for($i = 0; $i < count($languageVariables); ++$i)
        {
            
            $variableWithValue = array(
                
                'id' => $languageVariables[$i],
                'value' => PerisianLanguageVariable::getVariable($languageVariables[$i])
                
            );
            
            array_push($returnValue, $variableWithValue);
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a formatted list of displayable fields.
     * 
     * @author Peter Hamm
     * @return Array
     */
    static public function getFormattedList()
    {
        
        $instance = static::getInstance();
        
        $list = $instance->getData();
        
        $formattedList = array(
            
            'pages' => array()
            
        );
        
        for($i = 0; $i < count($list); ++$i)
        {
            
            $currentPage = $list[$i][$instance->field_page];
            
            if(!isset($formattedList['pages'][$currentPage]))
            {
                
                $formattedList['pages'][$currentPage] = array();
                
            }
            
            $formattedField = static::getFormattedField($list[$i]);
                        
            array_push($formattedList['pages'][$currentPage], $formattedField);
            
        }
        
        return $formattedList;
        
    }
    
    /**
     * Builds a conditional string that can be executed in JavaScript.
     * 
     * @author Peter Hamm
     * @param Array $conditionArray
     * @return string
     */
    static public function buildConditionJavascript($conditionArray)
    {
        
        if(!isset($conditionArray) || count($conditionArray) == 0)
        {
            
            return "true";
            
        }
        
        $conditionStringArray = array();
        
        for($i = 0; $i < count($conditionArray); ++$i)
        {
            
            $operator = "==";
            $operator = $conditionArray[$i]['operator'] == 'is_not' ? "!=" : $operator;
            
            $conditionString = "AppointmentAssistant.getFieldValue('" . $conditionArray[$i]['identifier'] . "') " . $operator . " '" . html_entity_decode($conditionArray[$i]['value']) . "'";
            
            array_push($conditionStringArray, $conditionString);
            
        }
        
        return implode(" && ", $conditionStringArray);
        
    }
    
    /**
     * Updates the fields with the specified data.
     * 
     * @author Peter Hamm
     * @param Array $fieldData
     * @param Array $allowedPages Optional, an array containing a list of associative arrays like this: array('index' => 0, 'title' => 'Your title').
     */
    static public function updateFields($fieldData, $allowedPages = array())
    {
        
        $instance = static::getInstance();
        
        // Delete everything from the database
        $instance->delete('true');
        
        // Insert all the fields.
        foreach($fieldData['pages'] as $pageIndex => $pageContent)
        {
            
            foreach($pageContent as $fieldIndex => $fieldContent)
            {
                
                $fieldObject = new static();
                
                $fieldObject->{$fieldObject->field_page} = $pageIndex;
                $fieldObject->{$fieldObject->field_index} = $fieldIndex;
                $fieldObject->{$fieldObject->field_identifier} = strlen(@$fieldContent['identifier']) > 0 ? $fieldContent['identifier'] : 'random_' . uniqid();
                $fieldObject->{$fieldObject->field_title} = @$fieldContent['label'];
                $fieldObject->{$fieldObject->field_type} = $fieldContent['type'];
                $fieldObject->setDataAndEncode($fieldContent['data']);
                
                $fieldObject->save();
                
            }
            
        }
        
    }
    
    /**
     * Generates formatted field data for the specified raw field data.
     * 
     * @author Peter Hamm
     * @param Array $fieldData
     * @return Array
     */
    static protected function getFormattedField($fieldData)
    {
        
        $instance = static::getInstance();
        
        $formattedFieldData = array(
            
            "identifier" => $fieldData[$instance->field_identifier],
            "label" => $fieldData[$instance->field_title],
            "type" => $fieldData[$instance->field_type],
            "data" => $fieldData[$instance->data_decoded]
            
        );
        
        return $formattedFieldData;
        
    }
    
    /**
     * Takes an associative array, with identifiers as keys and returns an easily readable String, 
     * complete with formatted field names and values.
     * 
     * @author Peter Hamm
     * @param Array $associativeValueList
     * @return String
     */
    static public function getFieldsAsFormattedString($associativeValueList)
    {
        
        $returnValue = "";
        $formattedFields = array();
        
        if(isset($associativeValueList) && count($associativeValueList) > 0)
        {
            
            foreach($associativeValueList as $identifier => $value)
            {
                
                $fieldData = static::getFieldByIdentifier($identifier);
                
                if($fieldData->{$fieldData->field_pk} > 0)
                {

                    $fieldString = PerisianLanguageVariable::getVariable($fieldData->{$fieldData->field_title}) . ": ";

                    if($fieldData->{$fieldData->field_type} == 'radio' || $fieldData->{$fieldData->field_type} == 'select')
                    {

                        $possibleValues = $fieldData->getPossibleValues();

                        foreach($possibleValues as $possibleKey => $possibleValue)
                        {

                            if($possibleKey == $value)
                            {

                                $value = PerisianLanguageVariable::getVariable($possibleValue);

                                break;

                            }

                        }

                    }

                    $fieldString .= '"' . $value . '"';

                    array_push($formattedFields, $fieldString);
                    
                }
                                    
            }
            
            $returnValue = implode("\n", $formattedFields);
                        
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves a field's data by the specified identifier.
     * 
     * @param String $identifier
     * @return \static
     */
    static public function getFieldByIdentifier($identifier)
    {
        
        $instance = static::getInstance();
        
        PerisianFrameworkToolbox::security($identifier);
        
        $result = $instance->getData($instance->field_identifier . ' = "' . $identifier . '"');
        
        if(count($result) == 1)
        {
            
            return new static($result[0][$instance->field_pk]);
            
        }
        
        return new static();
        
    }
    
    /**
     * Retrieves the possible values for this field.
     * 
     * @author Peter Hamm
     * @return mixed Either a String or an array, depending on the field type.
     */
    public function getPossibleValues()
    {
        
        $returnValue = "";
        
        if($this->{$this->field_type} == 'radio' || $this->{$this->field_type} == 'select')
        {
            
            $data = $this->getDecodedData();
            
            $returnValue = $data['values'];
            
        }
        
        return $returnValue;
        
    }
    
}