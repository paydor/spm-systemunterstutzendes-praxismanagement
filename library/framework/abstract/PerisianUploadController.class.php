<?php

require_once "PerisianSettingController.class.php";

abstract class PerisianUploadController extends PerisianSettingController
{
    
    /**
     * Retrieves a list of allowed upload setting names for this controller.
     * 
     * @author Peter Hamm
     * @return Array
     */
    abstract protected function getAllowedUploadSettings();
    
    /**
     * Resets uploaded files
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionResetUpload()
    {
        
        $this->setInternal('reset', true);
        
        $this->actionUpload();
        
    }
    
    /**
     * Handles file uploads
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionUpload()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $postedSetting = $this->getParameter('setting');
        
        $uploadResult = array(
            
            'success' => false
            
        );
        
        try
        {
            
            $allowedUploadSettingNames = $this->getAllowedUploadSettings();
        
            if(in_array($postedSetting, $allowedUploadSettingNames))
            {
                
                $resetFile = false;

                if($this->getInternal('reset') === true)
                {
                    
                    $uploadResult = array(

                        "success" => true,
                        "message" => PerisianLanguageVariable::getVariable(11132)

                    );
                    
                    $resetFile = true;
                    
                }
                else
                {
                    
                    $uploadResult = PerisianUploadFileManager::handleUploadedFile('image', $_FILES);
                    
                }
                
                $settingId = PerisianSetting::getIdForName($postedSetting);
                
                // Delete the old file and update the setting with the new file.
                $imageSetting = new PerisianSystemSetting($settingId);
                
                PerisianUploadFileManager::deleteFile('image', $imageSetting->getValue());
                
                $imageSetting->{$imageSetting->field_setting_value} = ($resetFile ? "" : $uploadResult["file"]);
                $imageSetting->save();

            }
            else
            {
                
                throw new PerisianException(PerisianLanguageVariable::getVariable(10665));
                
            }
        
        }
        catch(Exception $e)
        {
            
            $uploadResult = array(
                
                "success" => false,
                "message" => $e->getMessage()
                
            );
                        
        }
        
        $this->setResultValue('result', $uploadResult);
        
    }
    
}