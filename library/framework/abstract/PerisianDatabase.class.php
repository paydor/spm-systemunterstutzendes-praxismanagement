<?php

require_once 'framework/PerisianTimeZone.class.php';

/**
 * Abstract class for database functionalities
 */
abstract class PerisianDatabase
{

    // Instance holder
    private static $instances = Array();

    // Query logging, only active config value 'basic/project/log_queries' is true
    protected $queryLog = Array();

    /**
     * Overwritten clone method
     *
     * @author Peter Hamm
     * @return void
     */
    final public function __clone()
    {

        throw new PerisianException('This class must not be cloned.');

    }

    /**
     * Returns the log of all queries made,
     * with optional additional info
     *
     * @author Peter Hamm
     * @return Array
     */
    final public function getQueryLog()
    {
        
        return $this->queryLog;
        
    }
    
}
