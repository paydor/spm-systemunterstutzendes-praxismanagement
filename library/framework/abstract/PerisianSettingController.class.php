<?php

require_once "PerisianController.class.php";

abstract class PerisianSettingController extends PerisianController
{
    
    protected $listSettingsSystem = null;
    
    /**
     * Saves the specific settings of this controller.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
                
        $possibleSettings = Array();
        $settingObj = new PerisianSetting();
        $systemSettingObj = new PerisianSystemSetting();

        $settingList = $this->getListSettingsSpecific();
        
        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }

        foreach($possibleSettings as $possibleSettingId)
        {
                        
            if(isset($this->parameters['s_' . $possibleSettingId]))
            {
                                                
                $settingValue = $this->getParameter('s_' . $possibleSettingId);

                $newValue = ($settingValue === false || $settingValue == null) ? "0" : $settingValue;
                
                $newObj = new PerisianSystemSetting($possibleSettingId);
                $newObj->{$newObj->field_setting_value} = $newValue;
                $newObj->save();
                
            }

        }
        
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Retrieves all the relevant settings of this controller.
     * Should be overwritten to filter the settings.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getListSettingsSpecific()
    {
        
        if(is_null($this->listSettingsSystem))
        {
        
            $this->listSettingsSystem = $this->getListSettingsSystem();
        
        }
                
        return $this->listSettingsSystem;
        
    }
    
    /**
     * Retrieves the complete list of system settings.
     * 
     * @author Peter Hamm
     * @return Array
     */
    protected function getListSettingsSystem()
    {
        
        $settingObj = new PerisianSystemSetting();

        $systemSettingList = $settingObj->getPossibleSettings('admin');
        $systemSettings = $settingObj->getAssociativeValues();

        // Combine the possible rights with the values of this user to generate
        // a list that can easily processed in the template
        {

            for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
            {

                if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                {

                    $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];

                }

            }

        }

        return $systemSettingList;
        
    }
    
}