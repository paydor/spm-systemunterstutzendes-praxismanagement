<?php

/**
 * Handles forgotten passwords
 *
 * @author Peter Hamm
 */
abstract class PerisianUserRegistrationAbstract extends PerisianDatabaseModel
{

    // Basic values for the specific settings
    public $table = '';
    public $field_pk = '';
    public $field_fk = '';
    public $field_code = '';
    public $field_time = '';
    
    public function __construct()
    {
        
        parent::__construct();
        
    }
    
    /**
     * Adds an entry for the specified user identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return array
     */
    public static function addEntryForUser($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
            
        if(static::entryAlreadyExistsForUser($userId))
        {
            
            return array("success" => false);
            
        }
        
        $entry = new static();
        
        // Delete any existing entries for this user
        {
            
            $entry->deleteByUserId($userId);
            
        }
        
        $code = static::generateCode();
        
        $entry->{$entry->field_fk} = $userId;
        $entry->{$entry->field_code} = $code;
        $entry->{$entry->field_time} = PerisianFrameworkToolbox::sqlTimestamp();
        
        $entry->save();
        
        return array("success" => true, "code" => $code);
        
    }
    
    /**
     * Retrieves the entry by the specified code
     * 
     * @author Peter Hamm
     * @param string $code
     * @return Object
     */
    public static function getByCode($code)
    {
        
        PerisianFrameworkToolbox::security($code);
        
        $object = new static();
        $query = $object->field_code . ' = "' . $code . '"';
        
        $results = $object->getData($query);
        
        return count($results) > 0 ? $results[0] : null;
        
    }
    
    /**
     * Retrieves a code that can be used to identify a user's request call.
     * 
     * @author Peter Hamm
     * @return string
     */
    protected static function generateCode()
    {
        
        $code = md5(time() * rand(0, 10000) * rand(1, 10000));
        
        return $code;
        
    }
    
    /**
     * Retrieves if an entry for a specified user id already exists or not.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return bool
     */
    public static function entryAlreadyExistsForUser($userId)
    {
        
        $object = new static();
        
        return ($object->getEntryCountForUserId($userId) > 0);
    
    }
    
    /**
     * Deletes all entries for the specifies code.
     * @param string $code
     * @return bool
     */
    public function deleteByCode($code)
    {
        
        PerisianFrameworkToolbox::security($code);
        
        if(strlen($code) == 0)
        {
            
            return;
            
        }

        $query = "{$this->field_code} = {$code}";
        
        return $this->delete($query);
        
    }
    
    /**
     * Deletes all entries for the specifies user identifier.
     * @param int $userId
     * @return bool
     */
    public function deleteByUserId($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        if(strlen($userId) == 0)
        {
            
            return;
            
        }

        $query = "{$this->field_fk} = {$userId}";
        
        return $this->delete($query);
        
    }
    
    /**
     * Retrieves the count of already existing entries for a specified user id.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return int
     */
    public function getEntryCountForUserId($userId)
    {
        
        PerisianFrameworkToolbox::security($userId);
        
        // Only check for entries younger than 5 minutes;
        $timestamp = PerisianFrameworkToolbox::sqlTimestamp(time() - 30);
       
        $query = $this->field_fk . ' = "' . $userId . '"';
        $query .= " AND " . $this->field_time . " > " . $timestamp;

        return $this->getCount($query);
        
    }
    
}
