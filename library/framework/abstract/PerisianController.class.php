<?php

/**
 * Basic controller class
 *
 * @author Peter Hamm
 * @date 2016-11-08
 */
abstract class PerisianController
{
    
    protected $action = "";
    protected $internals = array();
    protected $parameters = array();
    protected $result = array();
    
    /**
     * Executes what the controller should do, depending on the specified parameters.
     * It should write key-value-pairs to $this->result which can then be retrieved by
     * $this->getResult.
     * 
     * @return void.
     */
    public abstract function execute();
    
    /**
     * Sets the main action of this controller.
     * 
     * @author Peter Hamm
     * @param String $action
     * @return void
     */
    public function setAction($action = "")
    {
        
        $this->action = $action;
        
    }
    
    /**
     * Retrieves the main action of this controller.
     * 
     * @author Peter Hamm
     * @return String
     */
    public function getAction()
    {
        
        return $this->action;
        
    }
    
    /**
     * Sets the parameters to this controller to those
     * specified in a HTTP request.
     * 
     * @author Peter Hamm
     * @global Array $_REQUEST
     * @return void
     */
    public function setParametersFromRequest()
    {

        global $_REQUEST;
        
        foreach(@$_REQUEST as $key => $value)
        {
            
            $this->setParameter($key, $value);
            
        }
        
    }
        
    /**
     * Sets the parameter with the specified key to the specified value.
     * 
     * @author Peter Hamm
     * @param String $key
     * @param mixed $value
     * @return void
     */
    public function setParameter($key, $value)
    {
        
        $this->parameters[$key] = $value;
        
    }
    
    /**
     * Retrieves parameter value with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @param mixed $defaultValue Optional. Sets the parameter to this default value if it was not specified.
     * @return mixed 
     */
    public function getParameter($key, $defaultValue = null)
    {
        
        $value = $defaultValue;
        
        if(@$this->parameters[$key])
        {
            
            $value = @$this->parameters[$key];
            
        }
        
        return $value;
        
    }
    
    /**
     * Retrieves all parameters.
     * 
     * @author Peter Hamm
     * @return mixed 
     */
    public function getParameters()
    {
        
        return $this->parameters;
        
    }
    
    /**
     * Sets the internal data with the specified key to the specified value.
     * 
     * @author Peter Hamm
     * @param String $key
     * @param mixed $value
     * @return void
     */
    protected function setInternal($key, $value)
    {
        
        $this->internals[$key] = $value;
        
    }
    
    /**
     * Retrieves the internal value with the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return mixed 
     */
    protected function getInternal($key)
    {
        
        return @$this->internals[$key];
        
    }
    
    /**
     * Sets the result value for the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @param String $value
     * @return void
     */
    protected function setResultValue($key, $value)
    {
        
        $this->result[$key] = $value;
        
    }
    
    /**
     * Tries to retrieve the result value for the specified key.
     * 
     * @author Peter Hamm
     * @param String $key
     * @return void
     */
    protected function getResultValue($key)
    {
        
        $returnValue = null;
        
        if(@$this->result[$key])
        {
            
            $returnValue = $this->result[$key];
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the result 
     * @return type
     */
    public function getResult()
    {
        
        return $this->result;
        
    }
    
    /**
     * Will write the keys from the result set into global variables.
     * Use with caution as it may overwrite data.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function resultToGlobals()
    {
        
        foreach($this->result as $key => $value)
        {
            
            global $$key;
            
            $$key = $value;
            
        }
                
    }
    
    /**
     * Will write the Parameters into global variables.
     * Use with caution as it may overwrite data.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function parametersToGlobals()
    {
        
        foreach($this->parameters as $key => $value)
        {
            
            global $$key;
            
            $$key = $value;
            
        }
                
    }
    
    /**
     * Puts the key / value pairs of the parameters and result to
     * global variables.
     * 
     * @author Peter Hamm
     * @return void
     */
    public function globalize()
    {
        
        $this->parametersToGlobals();
        $this->resultToGlobals();
        
    }
    
}