<?php

require_once 'database/PerisianDatabaseModel.class.php';

/**
 * This class handles countries
 *
 * @author Peter Hamm
 * @date 2010-12-13
 */
class PerisianCountry extends PerisianDatabaseModel
{

    // Main table settings
    public $table = 'country';
    public $field_pk = 'country_id';

    // Fields
    public $field_language_id = 'country_language_id';
    public $field_code = 'country_code';
    public $field_name = 'country_name';
    public $field_capital = 'country_capital';

    // Instance
    static private $instance = null;

    // Caching
    private $cache = Array();

    /**
     * Overwritten constructor
     *
     * @author Peter Hamm
     * @return void
     */
    public function __construct()
    {

        parent::__construct('MySql', 'main');

    }

    /**
     * Returns an instance of this object
     *
     * @author Peter Hamm
     * @uses PerisianSystemSetting
     * @return Object
     */
    static private function getInstance()
    {

        if(!isset(self::$instance))
        {
            self::$instance = new self;
        }

        return self::$instance;

    }

    /**
     * Outputs a list of countries in the languages with their IDs specified in $languageIds
     *
     * @author Peter Hamm
     * @param Array $languageIds Optional: An array containing the language IDs you want to have, default: Only language 1
     * @return Array A list of countries
     */
    static public function getCountryList($languageIds = Array(1))
    {

        PerisianFrameworkToolbox::security($languageIds);

        if(!is_array($languageIds))
        {
            $languageIds = Array($languageIds);
        }

        $instance = self::getInstance();
        $result = Array();
        $query = '';

        for($i = 0, $m = count($languageIds); $i < $m; ++$i)
        {

            if(isset($instance->cache[$languageIds[$i]]))
            {

                $result[$languageIds[$i]] = $instance->cache[$languageIds[$i]];
                
            }
            else
            {

                $query = "{$instance->field_language_id} = {$languageIds[$i]}";
                $result[$languageIds[$i]] = $instance->getData($query);
                $instance->cache[$languageIds[$i]] = $result[$languageIds[$i]];

            }

        }

        PerisianFrameworkToolbox::securityRevert($result);

        if(count($languageIds) == 1)
        {

            $resultNew = Array();

            foreach($result[$languageIds[0]] as $country)
            {

                $resultNew[$country[$instance->field_code]] = $country[$instance->field_name];

            }

            $result = $resultNew;
            asort($result);

        }

        return $result;

    }

    /**
     * Overwritten save method, does nothing.
     *
     * @author Peter Hamm
     * @return void
     */
    public function save()
    {

        return;

    }


}
