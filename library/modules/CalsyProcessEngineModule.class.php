<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/process_engine/CalsyProcessEngineLog.class.php';
require_once 'calsy/process_engine/CalsyProcessEngineConnection.class.php';

/**
 * Process engine module
 * 
 * @author Peter Hamm
 * @date 2017-02-11
 */
class CalsyProcessEngineModule extends PerisianModuleAbstract
{
    
    const FLAG_PROCESS_TO_START = 'process_engine_process_to_start';
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_process_engine");
    
    protected static $languageVariables = array(
        
        'p589f32abb25e2', 'p589f44cf69f89', 'p58a0880733b5e', 'p58a0881f0ecde', 'p58a088314493e', 'p58a088607b349',
        'p58a0887d43d05', 'p58a0889acf3b1', 'p58a20ce06f12a', 'p58a20cd0e6d7e', 'p58a1d78024827', 'p58a1d7579e233',
        'p58a22d853dbf4', 'p58a22b627dfa6', 'p58a5d260b12ea', 'p58a5c589b9346', 'p58a5ead79b720', 'p58a7351644513',
        'p58a73e57ccf1e'
        
    );
    
    protected static $colorDefault = '';
    
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p589f32abb25e2') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_process_engine";
        
    }
    
    /**
     * Retrieves if the specified backend user has the rights to manage processes
     * @param CalsyUserBackend $userBackend
     * @return bool
     */
    public static function userCanManageProcesses($userBackend)
    {
        
        return $userBackend->hasSetting('process_engine_user_backend_can_plan_process_starts');
        
    }
    
    /**
     * Retrieves a connection to the ProcessEngine server.
     * 
     * @author Peter Hamm
     * @return \CalsyProcessEngineConnection
     */
    private static function getConnection()
    {
        
        $connectionServer = PerisianSystemSetting::getSettingValue('process_engine_server_address');
        $connectionUser = PerisianSystemSetting::getSettingValue('process_engine_server_user');
        $connectionPassword = PerisianSystemSetting::getSettingValue('process_engine_server_password');
                
        $connection = new CalsyProcessEngineConnection($connectionServer, $connectionUser, $connectionPassword);
                
        return $connection;
        
    }
    
    /**
     * Tries to retrieve the processes from the ProcessEngine server.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getProcessList()
    {
        
        $connection = static::getConnection();
                        
        $processes = $connection->getProcessList();
        
        return $processes;
        
    }
    
    /**
     * Tries to start the specified processes on the ProcessEngine server, if the module is activated.
     * Should the module not be activated, an error is thrown.
     * 
     * @author Peter Hamm
     * @param String $processIdentifier
     * @param String $processTitle Optional, default: empty.
     * @return String
     */
    public static function startProcess($processIdentifier, $processTitle = '')
    {
        
        $logEntry = new CalsyProcessEngineLogEntry();

        $additionalData = Array(

            'processId' => $processIdentifier

        );
        
        $processName = static::getProcessNameForIdentifier($processIdentifier);
        
        $connection = static::getConnection();
                                
        try
        {
                    
            $processInstanceId = $connection->startProcess($processIdentifier, $processTitle);
            
            $logEntry->{$logEntry->field_status} = 'started';
            $logEntry->{$logEntry->field_status_message} = $processName;
            
            $additionalData['instanceId'] = $processInstanceId;
            
            $logEntry->setContent($additionalData);
            
            $logEntry->save();
            
        }
        catch(PerisianException $e)
        {
                        
            $logEntry->{$logEntry->field_status} = 'error';
            $logEntry->{$logEntry->field_status_message} = $processName;
            
            $logEntry->setContent($additionalData);
            
            $logEntry->save();
            
            throw $e;
            
        }
        
        return $processInstanceId;
        
    }
    
    /**
     * Retrieves the ProcessEngine calendar entries within the two specified timestamps.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $timestampEnd
     * @return array
     */
    public static function getProcessEngineCalendarEntriesBetween($timestampBegin, $timestampEnd)
    {
                
        // Build the filter object
        {
            
            $filter = new CalsyCalendarFilter();
            
            $filter->addFlag(static::FLAG_PROCESS_TO_START, null, '!=');
            
        }
        
        $entries = CalsyCalendarEntry::getEntriesBetween($timestampBegin, $timestampEnd, $filter);
                
        $entryDummy = new CalsyCalendarEntry();
        
        $returnEntries = array();
        
        for($i = 0; $i < count($entries); ++$i)
        {
            
            $calendarEntry = new CalsyCalendarEntry($entries[$i][$entryDummy->field_pk]);
            
            array_push($returnEntries, $calendarEntry);
                        
        }
        
        return $returnEntries;
        
    }
    
    /**
     * Tries to retrieve the name of the process with the specified identifier.
     * 
     * @author Peter Hamm
     * @param String $processIdentifier
     * @return String
     */
    public static function getProcessNameForIdentifier($processIdentifier)
    {
        
        $processList = static::getProcessList();
        
        for($i = 0; $i < count($processList); ++$i)
        {
            
            if($processList[$i]['id'] == $processIdentifier)
            {
                
               return $processList[$i]['name']; 
                
            }
            
        }
        
        return '?';
        
    }
    
    /**
     * Retrieves whether the default color for process engine appointments
     * should be the creator's color user or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function useColorFromCreator() 
    {

        return PerisianSystemSetting::isSettingTrue('process_engine_color_use_creator');
        
    }
    /**
     * Retrieves default color for process engine appointments.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getColorDefault() 
    {
        
        if(strlen(static::$colorDefault) == 0)
        {
            
            static::$colorDefault = PerisianSystemSetting::getSettingValue('process_engine_color_default');

            if(strlen(static::$colorDefault) == 0)
            {

                static::$colorDefault = "#338C15";

            }
            
        }
        
        return static::$colorDefault;
        
    }
    
}

