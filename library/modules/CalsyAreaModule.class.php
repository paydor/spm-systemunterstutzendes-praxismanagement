<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/area/CalsyArea.class.php';

/**
 * Area module
 * 
 * @author Peter Hamm
 * @date 2016-04-28
 */
class CalsyAreaModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_area");
    
    protected static $languageVariables = array(
        
        11059, 11047, 11046, 11020, 11019, 11017, 11016, 11015, 
        'p575ecd585ad92', 'p575eea3b50829', 'p577e5b01378dd', 
        'p575edf80ea145', 'p596a5b13b4c08', 'p5d94d86ccc54f'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable(11015) . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_area";
        
    }
    
}
