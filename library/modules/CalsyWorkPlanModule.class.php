<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/work_plan/CalsyWorkPlan.class.php';

/**
 * Work plan module
 * 
 * @author Peter Hamm
 * @date 2017-12-15
 */
class CalsyWorkPlanModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_work_plan");
    
    protected static $languageVariables = array(
        
        'p5a340dbeaf5af', 'p5a343269d271f', 'p5a343b35217c3', 'p5a343bfd2b07d',
        'p5a3540eb1ab0f', 'p5a356795c7647', 'p5a3567b555a6f', 'p5a3567e91221d'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p5a340dbeaf5af') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_work_plan";
        
    }
    
}

