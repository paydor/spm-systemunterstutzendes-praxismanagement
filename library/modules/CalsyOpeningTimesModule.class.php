<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';

/**
 * Opening times
 * 
 * @author Peter Hamm
 * @date 2017-11-09
 */
class CalsyOpeningTimesModule extends PerisianModuleAbstract
{
    
    public static $settingNameOpeningTimes = 'module_setting_calsy_opening_times_hours';
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_opening_times");
    
    protected static $languageVariables = array(
        
        'p5a046893d3461'
        
    );
    
    private static $openingTimesRaw;
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p5a046893d3461') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_opening_times";
        
    }
        
    /**
     * Retrieves the list of opening times as a displayable string.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getOpeningTimesAsString()
    {
        
        $bookableTimes = static::getOpeningTimes();
        
        $days = array();
        
        for($i = 0; $i < count($bookableTimes); ++$i)
        {
            
            if(!isset($days[$bookableTimes[$i]["day"]]))
            {
                
                // Put it into an array resembling the weekdays.
                $days[$bookableTimes[$i]["day"]] = array();
                
            }
            
            array_push($days[$bookableTimes[$i]["day"]], $bookableTimes[$i]);
            
        }
        
        $bookableTimeString = "";
        
        foreach($days as $dayIndex => $values)
        {
            
            // Sort the entries by time
            usort($days[$dayIndex], function($a, $b) {

                return $a['from'] - $b['from'];

            });
            
            $bookableTimeString .= CalsyCalendar::getDayName($dayIndex) . ":";
            
            for($i = 0; $i < count($days[$dayIndex]); ++$i)
            {
                
                if($i > 0)
                {
                    
                    $bookableTimeString .= ",";
                    
                }
                
                $fromString = str_pad(floor($days[$dayIndex][$i]["from"] / 60), 2, '0', STR_PAD_LEFT). ":" . str_pad(($days[$dayIndex][$i]["from"] % 60), 2, '0', STR_PAD_LEFT);
                $toString = str_pad(floor($days[$dayIndex][$i]["to"] / 60), 2, '0', STR_PAD_LEFT). ":" . str_pad(($days[$dayIndex][$i]["to"] % 60), 2, '0', STR_PAD_LEFT);
                
                $bookableTimeString .= " " . $fromString . " - " . $toString . " " . PerisianLanguageVariable::getVariable(10903);
                
            }
            
            $bookableTimeString .= "\n";
              
        }
        
        return $bookableTimeString;
        
    }

    
    /**
     * Retrieves the opening times as an Array.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getOpeningTimes()
    {
        
        $openingTimesRaw = static::getOpeningTimesRaw();
        
        $openingTimes = json_decode($openingTimesRaw, true);
        
        return $openingTimes;
        
    }
    
    /**
     * Checks whether the specified timestamp's day and hour are within
     * the defined opening times or not.
     * 
     * @author Peter Hamm
     * @param int $timestamp
     * @return bool
     */
    public static function isDayAndTimeInOpeningTimes($timestamp)
    {
        
        $openingTimes = static::getOpeningTimes();
                
        $checkDay = date("N", $timestamp) - 1;
        $checkHour = ($timestamp - PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestamp));
        
        for($i = 0; $i < count($openingTimes); ++$i)
        {
            
            if($openingTimes[$i]['day'] == $checkDay)
            {
                
                if($openingTimes[$i]['from'] * 60 <= $checkHour && $openingTimes[$i]['to'] * 60 >= $checkHour)
                {
                    
                    return true;
                    
                }
                
            }
            
        }
        
        return false;
        
    }
    
    /**
     * Retrieves the raw opening times as a JSON string.
     * 
     * @author Peter Hamm
     * @return String
     */
    public static function getOpeningTimesRaw()
    {
        
        if(static::$openingTimesRaw == null)
        {
            
            static::$openingTimesRaw = PerisianFrameworkToolbox::securityRevert(PerisianSystemSetting::getSettingValue(CalsyOpeningTimesModule::$settingNameOpeningTimes));
            static::$openingTimesRaw = strlen(static::$openingTimesRaw) > 1 ? static::$openingTimesRaw : "[]";
            
        }
        
        return static::$openingTimesRaw;
        
    }
        
}
