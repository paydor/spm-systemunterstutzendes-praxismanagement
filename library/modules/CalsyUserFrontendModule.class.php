<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';

/**
 * Frontend user module
 * 
 * @author Peter Hamm
 * @date 2016-05-02
 */
class CalsyUserFrontendModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array(
        
        "is_module_enabled_calsy_user_frontend"
        
    );
    
    protected static $languageVariables = array(
        
        11097, 11076, 11071, 11061, 11054, 11045, 11013,
        10998, 10976, 10975, 10974, 10962, 10959, 10958, 
        10957, 10931, 10929, 10927, 10885, 10876, 10872,
        10868, 10841, 10836, 10835, 10834, 10829, 10828,
        10827, 10822, 10821, 10806, 10789, 10784, 10794,
        'p58a9d697b1a39', 'p58ac6bf57553d', 'p58bbb5fcd8c84', 
        'p58bbb690c713c', 'p596a5b13b4c08'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable(10794) . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_user_frontend";
        
    }
    
    /**
     * Retrieves if the calendar history is enabled when creating a new calendar entry.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isEnabledHistory()
    {
        
        return PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_display_history') == 1;
        
    }
    
    /**
     * Retrieves if the data merging functionality is enabled in the backend.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isEnabledDataMerging()
    {
        
        return PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_merging_enabled') == 1;
        
    }
    
}
