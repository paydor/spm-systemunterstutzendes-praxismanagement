<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/user_backend_group/CalsyUserBackendGroup.class.php';

/**
 * Module to enable user groups for backend users
 * 
 * @author Peter Hamm
 * @date 2017-04-10
 */
class CalsyUserBackendGroupModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_user_backend_group");
    
    protected static $languageVariables = array(
        
        'p58eb6bb85159c', 'p58eb75b7de6fa', 'p58eb75d80f0f7', 'p58eb7d6d8f7ab', 'p58eb7da0af762',
        'p58eb84229f63d', 'p58eb8445e9d34', 'p58eb877a506fc'
        
    );
            
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p58eb6bb85159c') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_user_backend_group";
        
    }
    
}
