<?php

require_once 'modules/CalsyCheckInModule.class.php';
require_once 'framework/abstract/PerisianModuleAbstract.class.php';

/**
 * Check in module
 * 
 * @author Peter Hamm
 * @date 2020-06-15
 */
class CalsyCheckInModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_checkin");
    
    protected static $languageVariables = array(
        
        'p5ee8de444dbed', 'p5ee8deaa787b5', 'p5ee8debbf305f', 'p5ee8dec63ff49', 'p5ee8e29ee694b',
        'p5ee8e2ac3226c', 'p5ee8e73cb0fcd', 'p5ee8e7636e3da', 'p5ee8e77f7f1be', 'p5ee8e7d286fc9',
        'p5ee8e7e89a615', 'p5ee8e8d261199', 'p5ee8ed70312e1', 'p5ee9033fcf8f5'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . lg('p5ee8de444dbed') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_checkin";
        
    }
    
}
