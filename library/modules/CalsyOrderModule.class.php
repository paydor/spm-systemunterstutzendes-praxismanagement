<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'modules/CalsySupplierModule.class.php';
require_once 'calsy/order/CalsyOrder.class.php';

/**
 * Order module
 * 
 * @author Peter Hamm
 * @date 2016-04-28
 */
class CalsyOrderModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_order");
    
    protected static $languageVariables = array(
        
        10839, 10838, 10837, 10805, 10802, 10801,
        10800, 10799, 10797, 10793, 10782
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable(10793) . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_order";
        
    }
    
}
