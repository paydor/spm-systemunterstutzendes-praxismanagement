<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';

/**
 * Module for a calendar with bookable times
 * 
 * @author Peter Hamm
 * @date 2016-04-28
 */
class CalsyBookableTimesModule extends PerisianModuleAbstract
{
    
    const FLAG_BOOKABLE_TIME = 'is_bookable_time';
    const FLAG_BOOKABLE_TIME_AREA_ID = 'bookable_time_area_id';
    const FLAG_BOOKABLE_TIME_NUMBER_BOOKABLE = 'bookable_time_number_bookable';
    const FLAG_BOOKABLE_TIME_PUBLIC_ANNOTATIONS_ENABLED = 'bookable_time_public_annotations_enabled';
    const FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE = 'bookable_time_annotation_image';
        
    const MAXIMUM_NUMBER_OF_BOOKINGS = 250;
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_bookable_times");
    
    protected static $languageVariables = array(
        
        'p57a8e9ed626d3', 'p57a9e58004bfb', 'p57ab3eb8b0edc', 'p57ab3ef12a0ea', 'p57ab4b06056ee',
        'p57ab60f0b4632', 'p57b6dc21c3f04'
        
    );
    
    /**
     * Returns a filter that can be applied on CalsyCalendarEntry::getCalendarEntriesForFilter() 
     * to find the entries of bookable times in the calendar.
     * 
     * @author Peter Hamm
     * @param mixed $users Optional, specify a single user ID or an array of user IDs to load only their entries.
     * @return CalsyCalendarFilter
     */
    public static function getEntryFilter($users = array())
    {
          
        {

            $filter = new CalsyCalendarFilter();

            $filter->setUserBackendIdentifiers($users);
            $filter->setIgnoreConfirmationStatus(true);
            
            $filter->addFlag(static::FLAG_BOOKABLE_TIME, '1');

        }
                
        return $filter;
        
    }
    
    /**
     * Finds all the bookable times with the specified filter and timespan.
     * 
     * @author Peter Hamm
     * @param int $timestampFrom
     * @param int $timestampTo
     * @param CalsyCalendarFilter $filter Optional, default: empty
     * @return Array
     */
    public static function getBookableTimes($timestampFrom, $timestampTo, $filter = null, $sortByDate = false, $limit = 0, $offset = 0)
    {
                
        $bookableTimesFilter = $filter == null ? new CalsyCalendarFilter() : clone $filter;

        if(!$bookableTimesFilter->hasFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME, '1'))
        {
            
            $bookableTimesFilter->addFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME, '1');
            
        }

        // User filter manipulation to get those bookable times without users as well.
        if(!is_null($filter))
        {

            $userFilter = $filter->getUserBackendIdentifiers(true);

            if(!in_array('NULL', $userFilter))
            {

                array_push($userFilter, 'NULL');
                $filter->setUserBackendIdentifiers($userFilter);

            }
            
            $filter->setUserFrontendIdentifiers(null);

        }
                
        $bookableTimesFilter->setIgnoreConfirmationStatus(true);
        $bookableTimesFilter->setIgnoreTimespanEdges(false);
        
        $query = "SELECT * " . CalsyCalendarEntry::getQueryForFilter($bookableTimesFilter, $timestampFrom, $timestampTo, $sortByDate, $limit, $offset);
                                
        $entry = CalsyCalendarEntry::getInstance();
        $database = $entry->getDatabaseConnection();
        
        $bookableTimes = $database->select($query);
        $bookableTimes = $entry->enrichResults($bookableTimes);
        
        return $bookableTimes;

    }
    /**
     * Takes the result of static::getBookableTimes() as an argument and 
     * formats it in a way so it's easy to pull out the most important information.
     * 
     * @author Peter Hamm
     * @param Array $bookableTimes
     * @return Array
     */
    public static function getBookableTimesInfo($bookableTimes)
    {
        
        $dummy = CalsyCalendarEntry::getInstance();
            
        $bookableTimesInfo = Array();
        
        for($i = 0; $i < count($bookableTimes); ++$i)
        {
            
            $numberFreeBookings = CalsyCalendarEntry::getMaximumAllowedNumberOfEntriesForBookableTime($bookableTimes[$i][$dummy->field_pk]);
            
            $entry = Array(
                
                'title' => $bookableTimes[$i][$dummy->field_title],
                'count_free_bookings' => $numberFreeBookings,
                'fields' => Array(),
                
                'user_backend_id' => $bookableTimes[$i][$dummy->field_user_id],
                'area_id' => null
                
            );
            
            // The fields
            {
                
                $timezoneOffsetBegin = 3600 + PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($bookableTimes[$i][$dummy->timestamp_begin]);
                $timezoneOffsetEnd = 3600 + PerisianTimeZone::getDaylightSavingTimeOffsetInSeconds($bookableTimes[$i][$dummy->timestamp_end]);
                
                array_push($entry['fields'], Array(
                    
                    'title' => PerisianLanguageVariable::getVariable('p5a108f36d95a3'),
                    'value' => PerisianFrameworkToolbox::timestampToDateTime($bookableTimes[$i][$dummy->timestamp_begin] + $timezoneOffsetBegin)
                    
                ));
                
                array_push($entry['fields'], Array(
                    
                    'title' => PerisianLanguageVariable::getVariable('p5a108f4a7b398'),
                    'value' => PerisianFrameworkToolbox::timestampToDateTime($bookableTimes[$i][$dummy->timestamp_end] + $timezoneOffsetEnd)
                    
                ));

                array_push($entry['fields'], Array(

                    'title' => PerisianLanguageVariable::getVariable('10896'),
                    'value' => (strlen($entry['user_backend_id']) == 0 || $entry['user_backend_id'] <= 0) ? PerisianLanguageVariable::getVariable('11039') : CalsyUserBackend::getUserFullname($entry['user_backend_id'])

                ));
                
                if(CalsyAreaModule::isEnabled())
                {
                    
                    $entry['area_id'] = CalsyCalendarEntryFlag::getFlagValueForEntry($bookableTimes[$i][$dummy->field_pk], CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID);
                    $selectedArea = new CalsyArea($entry['area_id']);
                                        
                    array_push($entry['fields'], Array(

                        'title' => PerisianLanguageVariable::getVariable('11059'),
                        'value' => strlen($entry['area_id']) > 0 ? $selectedArea->{$selectedArea->field_title} : PerisianLanguageVariable::getVariable('p575edf80ea145')

                    ));

                }
                
                {
                               
                    array_push($entry['fields'], Array(

                        'title' => PerisianLanguageVariable::getVariable('p5a298723740b7'),
                        'value' => ($numberFreeBookings == -1 || $numberFreeBookings >= PHP_INT_MAX - 100000) ? PerisianLanguageVariable::getVariable('p57f381721cab9') : $numberFreeBookings

                    ));
                
                }
                
            }
            
            array_push($bookableTimesInfo, $entry);
            
        }
        
        return $bookableTimesInfo;
        
    }
    
    
    /**
     * Takes a list of bookable time entries and merges overlapping ones to single ones.
     * 
     * @author Peter Hamm
     * @param Array $bookableTimes
     * @return Array The merged bookable times.
     */
    public static function mergeBookableTimes($bookableTimes)
    {
        
        $dummy = CalsyCalendarEntry::getInstance();
                
        for($i = 0; $i < count($bookableTimes); ++$i)
        {
                                    
            for($j = 0; $j < count($bookableTimes); ++$j)
            {
                
                // Check if it overlaps with any other bookable time than itself
                
                if($i == $j)
                {
                    
                    continue;
                    
                }
                
                $beginsHere = false;
                $endsHere = false;
                
                if($bookableTimes[$j][$dummy->timestamp_begin] > $bookableTimes[$i][$dummy->timestamp_begin] && $bookableTimes[$j][$dummy->timestamp_begin] < $bookableTimes[$i][$dummy->timestamp_end])
                {
                    
                    $beginsHere = true;
                    
                }
                
                if($bookableTimes[$j][$dummy->timestamp_end] > $bookableTimes[$i][$dummy->timestamp_begin] && $bookableTimes[$j][$dummy->timestamp_end] < $bookableTimes[$i][$dummy->timestamp_end])
                {
                    
                    $endsHere = true;
                    
                }
                                            
                if($beginsHere)
                {
                                        
                    // Shift the end of the bookable time
                    
                    $bookableTimes[$i][$dummy->timestamp_end] = $bookableTimes[$j][$dummy->timestamp_end];
                    
                }
                else if($endsHere)
                {
                    
                    // Shift the beginning of the bookable time
                    $bookableTimes[$i][$dummy->timestamp_begin] = $bookableTimes[$j][$dummy->timestamp_begin];
                    
                }
                
                if($beginsHere || $endsHere)
                {
                    
                    $bookableTimes[$i][$dummy->field_title] = PerisianLanguageVariable::getVariable('p57ab295ddf9ec');
                    
                    unset($bookableTimes[$j]);
                   
                    return static::mergeBookableTimes($bookableTimes);
                    
                }
                
            }
            
        }
        
        return $bookableTimes;
        
    }
    
    /**
     * Takes a list of bookable time entries and normal entries and splits the bookable times
     * with the entries. Returned is basically a new list of split bookable times with the entry times cut out.
     * 
     * @author Peter Hamm
     * @param Array $bookableTimes
     * @param Array $entries
     * @return Array The split bookable times.
     */
    public static function splitBookableTimesWithEntries($bookableTimes, $entries)
    {
        
        $dummy = CalsyCalendarEntry::getInstance();
        
        for($i = 0; $i < count($bookableTimes); ++$i)
        {
            
            for($j = 0; $j < count($entries); ++$j)
            {
                
                $beginsHere = false;
                $endsHere = false;
                
                if($entries[$j][$dummy->timestamp_begin] > $bookableTimes[$i][$dummy->timestamp_begin] && $entries[$j][$dummy->timestamp_begin] < $bookableTimes[$i][$dummy->timestamp_end])
                {
                    
                    $beginsHere = true;
                    
                }
                
                if($entries[$j][$dummy->timestamp_end] > $bookableTimes[$i][$dummy->timestamp_begin] && $entries[$j][$dummy->timestamp_end] < $bookableTimes[$i][$dummy->timestamp_end])
                {
                    
                    $endsHere = true;
                    
                }
                
                if($beginsHere && $endsHere)
                {
                    
                    // Split the bookable time
                    
                    $bookableTimeCopy = $bookableTimes[$i];
                    
                    $bookableTimes[$i][$dummy->timestamp_end] = $entries[$j][$dummy->timestamp_begin];
                    
                    $bookableTimeCopy[$dummy->timestamp_begin] = $entries[$j][$dummy->timestamp_end];
                                        
                    array_push($bookableTimes, $bookableTimeCopy);
                                  
                }
                else if($beginsHere)
                {
                                        
                    // Shift the end of the bookable time
                    
                    $bookableTimes[$i][$dummy->timestamp_end] = $entries[$j][$dummy->timestamp_begin];
                    
                }
                else if($endsHere)
                {
                    
                    // Shift the beginning of the bookable time
                    $bookableTimes[$i][$dummy->timestamp_begin] = $entries[$j][$dummy->timestamp_end];
                    
                }
                
            }
            
        }
        
        return $bookableTimes;
        
    }
    
    
    /**
     * Takes the specified matrix day and enriches it with entries of bookable times.
     * 
     * @author Peter Hamm
     * @param Array $matrixDay Recursive
     * @param bool $splitBookableTimes Optional, default: false
     * @param bool $mergeBookableTimes Optional, default: false
     * @return void
     */
    public static function enrichCalendarMatrixDayWithBookableTimes(&$matrixDay, $splitBookableTimes = false, $mergeBookableTimes = false)
    {
        
        $timeBegin = $matrixDay['time'];
        $timeEnd = $timeBegin + 86400;
        
        $bookableTimesForDay = CalsyBookableTimesModule::getBookableTimes($timeBegin, $timeEnd);
        $bookableTimesForDay = array_reverse($bookableTimesForDay);
        
        for($i = 0; $i < count($bookableTimesForDay); ++$i)
        {
            
            $bookableTimeEntry = &$bookableTimesForDay[$i];
            
            $bookableTimeEntry['startsToday'] = true;
            $bookableTimeEntry['endsToday'] = true;
            $bookableTimeEntry['overspansToday'] = false;
            $bookableTimeEntry['isBookableTime'] = true;
            $bookableTimeEntry['displayColor'] = '#3d7fec';
                 
        }
        
        if($mergeBookableTimes)
        {
            
            $bookableTimesForDay = CalsyBookableTimesModule::mergeBookableTimes($bookableTimesForDay);
            
        }
        
        if($splitBookableTimes)
        {
            
            $bookableTimesForDay = CalsyBookableTimesModule::splitBookableTimesWithEntries($bookableTimesForDay, $matrixDay['entries']);
            
        }
        
        for($i = 0; $i < count($bookableTimesForDay); ++$i)
        {
            
            array_unshift($matrixDay['entries'], $bookableTimesForDay[$i]);
            
        }
        
        CalsyCalendarEntry::sortEntriesByDate($matrixDay['entries']);
                
    }
    
    /**
     * Tries to find an amount of free, bookable times.
     * Takes in consideration the bookable times specified in the backend.
     * 
     * @author Peter Hamm
     * @param int $timestampBegin
     * @param int $duration In seconds: The duration of the desired appointment
     * @param int $distance In seconds:  How far apart should the appointment proposals be? E.g. '300' for 5 minutes.
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @param int $limit Optional, default: 5
     * @param bool $useBookableTimesModule Optional, default: false
     * @return Array
     */
    public static function findBookableTimes($timestampBegin, $duration, $distance, $filter, $limit = 5, $useBookableTimesModule = false)
    {
        
        global $userFrontend;
                                
        $limit = abs($limit);
        $freeBookableTimes = array();
        $timestampBeginOriginal = $timestampBegin;
        
        // Create the different filters
        {

            // Build the holiday filter
            {

                $holidayFilter = new CalsyCalendarFilter();

                $holidayFilter->setShowUnconfirmed(true);
                $holidayFilter->setShowHolidays(true);
                $holidayFilter->setIgnoreConfirmationStatus(true);
                $holidayFilter->setIgnoreTimespanEdges(true);

            }
            
            // Build the frontend user filter
            {

                $accountFilter = new CalsyCalendarFilter();

                $userFrontendIdentifiers = $filter->getUserFrontendIdentifiers(true);
                
                $accountFilter->setUserFrontendIdentifiers($userFrontendIdentifiers);
                
                $accountFilter->setShowUnconfirmed(true);
                $accountFilter->setIgnoreConfirmationStatus(true);
                $accountFilter->setIgnoreTimespanEdges(true);
                
                if(count($userFrontendIdentifiers) == 1 && $userFrontendIdentifiers[0] == 0)
                {
                    
                    $accountFilter->setUserFrontendIdentifiers(null);
                    
                }
                
            }
                        
            $userBackendFilter = $filter->getUserBackendIdentifiers(true);
            $areaFilter = $filter->getAreaIdentifiers(true);
                     
        }
                
        $iterationIndex = 0;
        $iterationLimit = 500;
        
        $debugInfo = Array();
        
        while(count($freeBookableTimes) < $limit)
        {
            
            ++$iterationIndex;
                    
            if($iterationIndex >= $iterationLimit)
            {
                
                // The iteration limit has been reached, skip out at this point.
                            
                if(count($freeBookableTimes) == 0)
                {
                    
                    throw new PerisianException(str_replace('%', $iterationLimit, PerisianLanguageVariable::getVariable(CalsyBookableTimesModule::isEnabled() ? 'p57ab4b06056ee' : 'p577e91c6c0071')), $debugInfo);
                    
                }
                
                break;
                
            }
                                    
            // Check which (and if) dates are available at all.
            $currentFreeBookableTimes = static::findBookableTimeOnDayBetween($timestampBegin, ($timestampBegin + $duration), $duration, $distance, $filter, $useBookableTimesModule);
                        
            array_push($debugInfo, Array(
                
                'message' => 'Looking for free bookable times between ' . PerisianFrameworkToolbox::timestampToDateTime($timestampBegin) . ' and ' . PerisianFrameworkToolbox::timestampToDateTime($timestampBegin + $duration),
                'timestamp_begin' => $timestampBegin,
                'timestamp_end' => ($timestampBegin + $duration),
                'duration' => $duration,
                'distance' => $distance,
                'filter' => $filter,
                'use_bookable_times_module' => $useBookableTimesModule ? 'true' : false
                 
            ));
                            
            for($i = 0; $i < count($currentFreeBookableTimes); ++$i)
            {
                
                if(count($freeBookableTimes) == $limit)
                {
                    
                    // We have enough results already
                    
                    break;
                    
                }
                
                if($currentFreeBookableTimes[$i]["begin"] < $timestampBeginOriginal)
                {
                    
                    // The beginning is too early
                              
                    continue;
                    
                }
                
                $timestampBegin = $currentFreeBookableTimes[$i]["begin"];
                $timestampEnd = $currentFreeBookableTimes[$i]["end"];
                
                $addEntry = true;
                
                // Check if there are free times to allow the booking.
                if($useBookableTimesModule)
                {
                    
                    // This search looks through the bookable times.
                    // The bookable times module allows for multiple booking of a timespan.
                    // Check what the related flag for the current entry says about how many bookings 
                    // are allowed at the same time and compare it with other existing entries.
                    
                    // Retrieve the maximum of allowed entries for the desired timespan.
                    //$maximumAllowedEntries = CalsyCalendarEntry::getMaximumAllowedNumberOfEntriesInTimespan($filter, $timestampBegin, $timestampEnd);
                                                            
                    if(false)//$maximumAllowedEntries <= 0)
                    {
                        
                        // The maximum count has been reached
                        
                        $addEntry = false;
                        
                    }
                    
                    if($currentFreeBookableTimes[$i]["duration"] < $duration)
                    {
                        
                        // The duration of the proposal is too short
                        
                        $addEntry = false;
                        
                    }
                    
                }
                else
                {
                    
                    // Find out if opening times are enabled and defined for this day
                    if(CalsyOpeningTimesModule::isEnabled())
                    {
                        
                        $currentBegin = $currentFreeBookableTimes[$i]['begin'];
                        $currentEnd = $currentFreeBookableTimes[$i]['end'];
                                      
                        if(!CalsyOpeningTimesModule::isDayAndTimeInOpeningTimes($currentBegin) || !CalsyOpeningTimesModule::isDayAndTimeInOpeningTimes($currentEnd))
                        {
                            
                            // Either the beginning or the end time of this time are not within the opening times
                                     
                            $addEntry = false;
                        
                        }
                        
                    }
                    
                }
                
                if($addEntry && count($filter->getUserFrontendIdentifiers(true)) > 0 && isset($userFrontend) && $userFrontend->{$userFrontend->field_pk} > 0)
                {
                    
                    // Check if the frontend account already has an appointment here
                    $entryCount = CalsyCalendarEntry::getCountCalendarEntriesForFilter($accountFilter, $timestampBegin, $timestampEnd);
                                        
                    if($entryCount > 0)
                    {
                        
                        // The account already has an appointment here.
                        $addEntry = false;
                        
                    }
                                        
                }
                
                if($addEntry)
                {
                    
                    if(!$useBookableTimesModule)
                    {
                       
                        // Find out if this might be on a holiday.            
                        {

                            $numberOverlappingEntriesHolidays = CalsyCalendarEntry::getCountCalendarEntriesForFilter($holidayFilter, $timestampBegin, $timestampEnd);

                            if($numberOverlappingEntriesHolidays > 0)
                            {

                                // Can't book on a holiday
                                $addEntry = false;

                            }

                        }
                        
                        // Find out if the area is bookable on this day
                        if($addEntry && count($areaFilter) > 0)
                        {
                            
                            $isAreaOkay = false;
                            
                            for($j = 0; $j < count($areaFilter); ++$j)
                            {
                                
                                if(CalsyArea::isBookableForTimestamp($areaFilter[$j], $currentFreeBookableTimes[$i]['begin']) && CalsyArea::isBookableForTimestamp($areaFilter[$j], $currentFreeBookableTimes[$i]['end']))
                                {

                                    // Both the beginning or the end time of this time are within the area's enabled days.

                                    $isAreaOkay = true;

                                }
                                
                            }

                            $addEntry = $isAreaOkay;
                            
                        }

                        // Find out if the backend users are bookable on this day
                        if($addEntry)
                        {
                            
                            $isBookableForUserBackend = false;
                            
                            for($j = 0; $j < count($userBackendFilter); ++$j)
                            {

                                $currentBegin = $currentFreeBookableTimes[$i]['begin'];
                                $currentEnd = $currentFreeBookableTimes[$i]['end'];

                                if(CalsyUserBackend::isBookableOnDayForTimestamp($userBackendFilter[$j], $currentBegin) && CalsyUserBackend::isBookableOnDayForTimestamp($userBackendFilter[$j], $currentEnd))
                                {

                                    // Either the beginning or the end of this time 
                                    // are not within this current backend users's enabled days.

                                    $isBookableForUserBackend = true;
                                    
                                    break;

                                }

                            }
                            
                            if(!$isBookableForUserBackend)
                            {
                               
                                $addEntry = false;
                                
                            }
                        
                        }
                    
                    }
                    
                }
                
                if(count($freeBookableTimes) > 0)
                {
                    
                    $previousEntry = $freeBookableTimes[count($freeBookableTimes) - 1];
                    
                    if($previousEntry['begin'] == $currentFreeBookableTimes[$i]['begin'])
                    {
                        
                        if($previousEntry['end'] == $currentFreeBookableTimes[$i]['end'])
                        {
                            
                            // An identical entry already exists.
                            
                            $addEntry = false;
                            
                        }
                        
                    }
                    
                }
                
                if($addEntry)
                {
                                        
                    array_push($freeBookableTimes, $currentFreeBookableTimes[$i]);

                }
                
            }
            
            // In case it loops, go to the next step.
            $timestampBegin = $timestampBegin + $duration;
            
        }
        
        // Sort it
        function sortByBegin($a, $b) 
        {
            
            return $a['begin'] - $b['begin'];
            
        }
        
        usort($freeBookableTimes, 'sortByBegin');
                        
        return $freeBookableTimes;
        
    }
        
    /**
     * Finds bookable times for timestamps on a specified day. 
     * Note that $timestampFrom and $timestampTo must be on the same day.
     * 
     * @author Peter Hamm
     * @param int $timestampFrom
     * @param int $timestampTo
     * @param int $duration In seconds: The duration of the desired appointment
     * @param int $distance In seconds: How far apart should the appointment proposals be? E.g. '300' for 5 minutes.
     * @param CalsyCalendarFilter $filter The criteria to filter by.
     * @param bool $useBookableTimesModule Optional, default: false
     * @return array
     * @throws PerisianException
     */
    protected static function findBookableTimeOnDayBetween($timestampFrom, $timestampTo, $duration, $distance, $filter, $useBookableTimesModule = false)
    {
        
        $distance = abs($distance);
        $duration = abs($duration);
                
        $firstMinuteOfDay = PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestampFrom);
        
        if($firstMinuteOfDay != PerisianTimeZone::getFirstMinuteOfDayAsTimestamp($timestampTo))
        {
            
            // The $timestampTo cannot be later than the last second of the day.
            $timestampTo = PerisianTimeZone::getLastSecondOfDayAsTimestamp($timestampFrom);
            
        }
                
        // Find out what the allowed times on this day are
        {
                
            $instance = CalsyCalendarEntry::getInstance();
            
            $allowedTimesOnDay = array();
                        
            if($useBookableTimesModule)
            {
                              
                // Use the times that were defined in the 'bookable times' module.
                
                // User filter manipulation to get those bookable times without users as well.
                {

                    $userFilter = $filter->getUserBackendIdentifiers(true);
                    
                    if(!in_array('NULL', $userFilter))
                    {
                        
                        array_push($userFilter, 'NULL');
                        $filter->setUserBackendIdentifiers($userFilter);
                        
                    }
                    
                }
                
                $timestampTo = PerisianTimeZone::getLastSecondOfDayAsTimestamp($timestampFrom);
                
                $bookableTimesFilter = CalsyBookableTimesModule::getEntryFilter($userFilter);
                $bookableTimesFilter->addFlag(CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID, $filter->getAreaIdentifiers());
                
                $allowedTimes = CalsyCalendarEntry::getCalendarEntriesForFilter($bookableTimesFilter, $timestampFrom, $timestampTo);
                
                // Log the query (if enabled), for debugging
                {
                    
                    $details = Array(
                        
                        'from' => PerisianFrameworkToolbox::timestampToDateTime($timestampFrom),
                        'to' => PerisianFrameworkToolbox::timestampToDateTime($timestampTo),
                        'count_results' => count($allowedTimes),
                        'use_bookable_times_module' => $useBookableTimesModule,
                        'times_allowed' => $allowedTimes
                            
                    );
                    
                    CalsyAppointmentAssistantLog::addQuery('bookable_times_between', CalsyCalendarEntry::getLastQuery(), $details);
                    
                }
                
                $countor = 0;
                                
                foreach($allowedTimes as $allowedTime)
                {
                    
                    $beginTime = $allowedTime[$instance->timestamp_begin];
                    $endTime = $allowedTime[$instance->timestamp_end];
                    
                    while($beginTime + $duration <= $endTime)
                    {
                        
                        $timeEntry = array(
                            
                            'from' => $beginTime,
                            'to' => ($beginTime + $duration),
                            
                            'time_begin' => $beginTime,
                            'time_end' => $endTime,
                            
                            'flags' => CalsyCalendarEntryFlag::getFlagsForEntry($allowedTime[$instance->field_pk])
                            
                        );
                        
                        if(CalsyCalendarEntryFlag::getFlagValueForEntry($allowedTime[$instance->field_pk], CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_PUBLIC_ANNOTATIONS_ENABLED) != "1")
                        {
                                                        
                            unset($timeEntry['flags'][CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_ANNOTATIONS_IMAGE]);
                            
                        }
                        else
                        {
                            
                            $timeEntry['flags']['bookable_time_id'] = $allowedTime[$instance->field_pk];
                            
                        }
                        
                        array_push($allowedTimesOnDay, $timeEntry);
                        
                        $beginTime += $distance;
                    
                        ++$countor;

                    }
                    
                }

            }
            else
            {
                                
                $beginTime = $timestampFrom;
                $endTime = PerisianTimeZone::getLastSecondOfDayAsTimestamp($timestampFrom);
                
                while($beginTime + $duration <= $endTime)
                {

                    array_push($allowedTimesOnDay, array(

                        'from' => $beginTime,
                        'to' => ($beginTime + $duration)

                    ));

                    $beginTime += $distance;

                }
                
            }

            if(count($allowedTimesOnDay) == 0)
            {

                return array();

            }
        
        }
                
        $existingEntries = array();
        
        if(count($filter->getUserBackendIdentifiers()) == 0)
        {
                        
            // Find blocked times on the day for the specified user(s)
            
            for($i = 0; $i < count($allowedTimesOnDay); ++$i)
            {
                
                $currentTimestampFrom = $allowedTimesOnDay[$i]["from"];
                $currentTimestampTo = $allowedTimesOnDay[$i]["to"];
                                
                $filter->setIgnoreTimespanEdges(true);
                
                $newExistingEntries = CalsyCalendarEntry::getCalendarEntriesForFilter($filter, $currentTimestampFrom, $currentTimestampTo + 3600);
                
                // Log the query (if enabled), for debugging
                {
                    
                    $details = Array(
                        
                        'from' => PerisianFrameworkToolbox::timestampToDateTime($currentTimestampFrom),
                        'to' => PerisianFrameworkToolbox::timestampToDateTime($currentTimestampTo + 3600),
                        'count_results' => count($newExistingEntries)
                            
                    );
                    
                    CalsyAppointmentAssistantLog::addQuery('blocked_times_for_users_specified', CalsyCalendarEntry::getLastQuery(), $details);
                    
                }
                
                if(count($newExistingEntries) > 0)
                {
                    
                    for($j = 0; $j < count($newExistingEntries); ++$j)
                    {
                        
                        array_push($existingEntries, $newExistingEntries[$j]);

                    }

                }

            }
                        
        }
        
        $filterFlags = $filter->getFlags();
        
        if(count($filterFlags) > 0)
        {
                        
            // Respect the flag filter
            
            for($i = 0; $i < count($allowedTimesOnDay); ++$i)
            {
                
                if(isset($allowedTimesOnDay[$i]['flags']))
                {
                    
                    foreach($filterFlags as $filterFlag)
                    {
                        
                        $flagKey = $filterFlag['key'];
                        $flagValue = $filterFlag['value'];
                        
                        if(!is_null($flagValue))
                        {
                            
                            // Check if the entry has the desired flag set to the requested value.
                            
                            if(!isset($allowedTimesOnDay[$i]['flags'][$flagKey]) || $allowedTimesOnDay[$i]['flags'][$flagKey] != $flagValue)
                            {
                                
                                if($flagKey == CalsyBookableTimesModule::FLAG_BOOKABLE_TIME_AREA_ID && @$allowedTimesOnDay[$i]['flags'][$flagKey] == 0)
                                {
                                    
                                    // Ignore this, "0" as an area means: All areas are valid.
                                    
                                    continue;
                                    
                                }
                                
                                $allowedTimesOnDay[$i] = null;
                                
                            }
                            
                        }
                       
                    }
                    
                }
                
            }
            
        }
        
        $freeTimes = CalsyCalendarEntry::combineAllowedTimesWithBlockedTimes($allowedTimesOnDay, $existingEntries);
              
        $freeTimesWithDuration = CalsyCalendarEntry::getFreeAppointments($freeTimes, $duration, $distance);
              
        return $freeTimesWithDuration;
        
    }
    
    /**
     * Checks if the entry with the specified $entryId is flagged as a bookable time
     * 
     * @author Peter Hamm
     * @param int $entryId
     * @return bool
     */
    public static function isEntryBookableTime($entryId)
    {
        
        return CalsyCalendarEntryFlag::getFlagValueForEntry($entryId, static::FLAG_BOOKABLE_TIME) == 1;
        
    }
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p57a8e9ed626d3') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_bookable_times";
        
    }
    
}
