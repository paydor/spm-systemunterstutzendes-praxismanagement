<?php

require_once 'modules/controller/CalsyModuleController.class.php';

class CalsyTableReservationModuleController extends CalsyModuleController
{
    
    protected $moduleSettingListCustom = Array(
        
        'module_setting_calsy_table_reservation_frontend_enabled',
        'module_setting_calsy_table_reservation_frontend_quota'
        
    );
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_table_reservation';
        $this->nameModuleShort = 'table_reservation';
        $this->classModule = 'CalsyTableReservationModule';
        
    }
    
    /**
     * Provides data for an overview of settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
    
        $returnValue = parent::actionOverview();
    
        $result = $this->getResultValue('result');
        
        $result['booking_quota'] = Array();
        
        for($i = 0; $i <= 100; ++$i)
        {
            
            $result['booking_quota'][$i] = str_replace("%1", $i, lg('p5f6ceb78e1a11'));
            
        }
        
        $this->setResultValue('result', $result);
        
        return $returnValue;
        
    }
    
}