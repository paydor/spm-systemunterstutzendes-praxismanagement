<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyUserFrontendRegistrationAndCalendarModule.class.php';

class CalsyUserFrontendRegistrationAndCalendarModuleController extends CalsyModuleController
{
    
    /**
     * Retrieves a list of allowed pages for the appointment assistant.
     * 
     * @author Peter Hamm
     * @return array
     */
    protected function getAllowedPages()
    {
        
        $allowedPages = array(

            array(

                'index' => 0,
                'title' => PerisianLanguageVariable::getVariable(11026)

            ),

            array(

                'index' => 1,
                'title' => PerisianLanguageVariable::getVariable(11027)

            )

        );
        
        return $allowedPages;
        
    }
    
    /**
     * Provides data to display an overview for settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $renderer = array(
            
            'page' => 'module/' . $this->pageCustom
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $userId = $this->getParameter('u');
        $showAdminFunctions = false;
        
        $moduleObject = new $this->classModule();
        
        $allowedPages = $this->getAllowedPages();

        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
                        
            {
                        
                // Add the module settings to a seperate list
                {
                
                    $moduleSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_enabled_calsy_user_frontend_registration_and_calendar', $systemSettingList, true);

                }
                                
                // Different module specific settings
                {
                    
                    $moduleUserFrontendRegistrationSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_calsy_user_frontend_registration_and_calendar_', $systemSettingList, true);
                    
                }
                
                // Different module specific settings
                {
                    
                    $moduleUserFrontendRegistrationAppointmentAssistantSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_calsy_user_frontend_registration_and_calendar_is_appointment_assistant', $moduleUserFrontendRegistrationSettingList, true);
                    
                }
                
                // The display statuses for the apointment assistant in the frontend
                {

                    $appointmentAssistantStatusList = CalsyUserFrontendRegistrationAndCalendarModule::getStatusesForAppointmentAssistant();

                }
                
            }

        }
        
        // Custom fields
        {
            
            $customFields = CalsyAppointmentField::getFormattedList();
            $customFieldLanguageVariables = CalsyAppointmentField::getLanguageVariablesFromList($customFields);
                        
        }
        
        $moduleLanguageVariables = $moduleObject::getLanguageVariables();
        
        $result = array(
            
            'object_module' => $moduleObject,
            
            'custom_fields' => $customFields,
            'custom_field_language_variables' => $customFieldLanguageVariables,
            
            'list_appointment_assistant_status' => $appointmentAssistantStatusList,
            'list_settings_module' => $moduleSettingList,
            'list_language_variables_module' => $moduleLanguageVariables,
            'list_settings_user_frontend_registration_module' => $moduleUserFrontendRegistrationSettingList,
            'list_settings_appointment_assistant_frontend_registration_module' => $moduleUserFrontendRegistrationAppointmentAssistantSettingList,
            
            'module_name' => $this->nameModule,
            'module_name_short' => $this->nameModuleShort,
            
            'hide_link_overview' => $this->hideLinkOverview,
            
            'pages_allowed' => $allowedPages
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Saves the fields of the appointment assistant.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveFields()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $allowedPages = $this->getAllowedPages();
        
        $result = array(
            
            "message" => PerisianLanguageVariable::getVariable(11095),
            "success" => true
            
        );
        
        try
        {
            
            CalsyAppointmentField::updateFields($this->getParameter('fields'), $allowedPages);
            
        }
        catch(Exception $e)
        {
            
            $result["success"] = false;
            $result["message"] = PerisianLanguageVariable::getVariable(11096) . "\n\"" . $e->getMessage() . "\"";
            
        }
        
        
        $this->setResultValue('result', $result);
        
    }
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_user_frontend_registration_and_calendar';
        $this->nameModuleShort = 'user_frontend_registration_and_calendar';
        $this->classModule = 'CalsyUserFrontendRegistrationAndCalendarModule';
        
        $this->pageCustom = 'calsy_user_frontend_registration_and_calendar';
        
    }
    
}