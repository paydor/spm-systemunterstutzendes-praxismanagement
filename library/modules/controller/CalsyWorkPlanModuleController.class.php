<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyWorkPlanModule.class.php';

class CalsyWorkPlanModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_work_plan';
        $this->nameModuleShort = 'work_plan';
        $this->classModule = 'CalsyWorkPlanModule';
        
    }
    
}