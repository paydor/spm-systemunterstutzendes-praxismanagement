<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyOrderModule.class.php';

class CalsyOrderModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_order';
        $this->nameModuleShort = 'order';
        $this->classModule = 'CalsyOrderModule';
        
    }
    
}