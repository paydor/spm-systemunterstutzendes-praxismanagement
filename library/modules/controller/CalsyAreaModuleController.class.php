<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyAreaModule.class.php';

class CalsyAreaModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_area';
        $this->nameModuleShort = 'area';
        $this->classModule = 'CalsyAreaModule';
        
    }
    
    protected $moduleSettingListCustom = Array(
        
        "module_setting_calsy_area_consider_gender"
        
    );
    
}