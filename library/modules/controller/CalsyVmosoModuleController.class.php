<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyVmosoModule.class.php';

class CalsyVmosoModuleController extends CalsyModuleController
{
    
    protected $hideLinkOverview = true;
    
    protected $moduleSettingListCustom = Array(
        
        "module_setting_calsy_vmoso_connection_server",
        "module_setting_calsy_vmoso_connection_cid",
        "module_setting_calsy_vmoso_connection_user",
        "module_setting_calsy_vmoso_connection_password",
        "module_setting_calsy_vmoso_chat_recipients",
        "module_setting_calsy_vmoso_chat_layout_selection_for_accounts_enabled",
        "module_setting_calsy_vmoso_chat_layout_default"
        
    );
    
    protected function actionOverview() 
    {
        
        parent::actionOverview();
        
        $result = $this->getResultValue('result');
        
        $result['list_layouts_chat'] = CalsyVmosoModule::getListChatLayouts();
        
        // Kick out some backend settings
        {

            for($i = 0; $i < count($result['list_settings_module']); ++$i)
            {

                if(substr($result['list_settings_module'][$i]['setting_name'], -8) == '_backend')
                {
                    
                    unset($result['list_settings_module'][$i]);
                    
                    continue;
                    
                }

            }
            
            for($i = 0; $i < count($result['list_settings_icon']); ++$i)
            {

                if(substr($result['list_settings_icon'][$i]['setting_name'], -8) == '_backend')
                {
                    
                    unset($result['list_settings_icon'][$i]);
                    
                    continue;
                    
                }

            }
        
        }
                
        $this->setResultValue('result', $result);
        
    }
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_vmoso';
        $this->nameModuleShort = 'vmoso';
        $this->classModule = 'CalsyVmosoModule';
        
    }
    
}