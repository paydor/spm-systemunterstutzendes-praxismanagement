<?php

require_once 'framework/abstract/PerisianController.class.php';

/**
 * Module controller
 *
 * @author Peter Hamm
 * @date 2016-11-20
 */
abstract class CalsyModuleController extends PerisianController
{
    
    protected $pageCustom;
    protected $nameModule;
    protected $nameModuleShort;
    protected $classModule;
    protected $moduleSettingListCustom;
    
    protected $hideLinkOverview = false;
    
    /**
     * Retrieves an instance of the module of this controller.
     * 
     * @author Peter Hamm
     * @return Object
     */
    protected function getModuleInstance()
    {
        
        $module = new $this->classModule();
        
        return $module;
        
    }
    
    /**
     * Provides data for an overview of settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $page = 'module/calsy_module';
        
        if(isset($this->pageCustom) && strlen($this->pageCustom) > 0)
        {
            
            $page = 'module/' . $this->pageCustom;
            
        }
        
        $renderer = array(
            
            'page' => $page
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $userId = $this->getParameter('u');
        $showAdminFunctions = false;
        
        $moduleObject = $this->getModuleInstance();

        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();
            
            $moduleSettingListCustom = array(); 

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
                  
            // Seperate the settings
            {
                       
                // Add the module settings to a seperate list
                {
                                    
                    $moduleSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_enabled_' . $moduleObject->getModuleIdentifier(), $systemSettingList, true);

                }   
                
                // Add the module icon setting to a seperate list
                {
                
                    $iconSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('module_icon_' . $moduleObject->getModuleIdentifier(), $systemSettingList, true);

                }
                
                if(isset($this->moduleSettingListCustom) && count($this->moduleSettingListCustom) > 0)
                {
                    
                    for($i = 0; $i < count($this->moduleSettingListCustom); ++$i)
                    {
                        
                        $setting = PerisianSystemSetting::retrieveSettingsByPrefix($this->moduleSettingListCustom[$i], $systemSettingList, true);
                        
                        array_push($moduleSettingListCustom, $setting);
                        
                    }

                }
                       
            }

        }
        
        $moduleLanguageVariables = $moduleObject::getLanguageVariables();
        
        $result = array(
            
            'object_module' => $moduleObject,
            
            'list_settings_module' => $moduleSettingList,
            'list_settings_module_custom' => $moduleSettingListCustom,
            'list_settings_icon' => $iconSettingList,
            'list_language_variables_module' => $moduleLanguageVariables,
            
            'module_name' => $this->nameModule,
            'module_name_short' => $this->nameModuleShort,
            
            'hide_link_overview' => $this->hideLinkOverview
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Saves the settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSave()
    {
        
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $possibleSettings = Array();
        $settingObj = new PerisianSetting();
        $systemSettingObj = new PerisianSystemSetting();

        $settingList = $systemSettingObj->getPossibleSettings('admin');

        foreach($settingList as $setting)
        {
            
            $possibleSettings[] = $setting[$settingObj->field_pk];
            
        }
        
        foreach($possibleSettings as $possibleSettingId)
        {

            if(isset($_REQUEST['s_' . $possibleSettingId]))
            {
                                
                $settingValue = $this->getParameter('s_' . $possibleSettingId);
                
                $newObj = new PerisianSystemSetting($possibleSettingId);
                $newObj->{$newObj->field_setting_value} = $settingValue;
                $newObj->save();

            }

        }
        
        $result = array(
            
            'success' => true
            
        );
        
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Should set up the controller for the specific module.
     * 
     * @author Peter Hamm
     * @return void
     */
    abstract protected function setup();
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
        
        $this->setup();
        
        if($this->getModuleInstance()->isBlocked())
        {
            
            // This module was blocked.
            
            throw new PerisianException(PerisianLanguageVariable::getVariable('p58af5e4083901'));
            
        }
        
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}