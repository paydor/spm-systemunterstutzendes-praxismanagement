<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyCheckInModule.class.php';
require_once 'calsy/checkin/CalsyCheckInLogSynch.class.php';

class CalsyQuickLogModuleController extends CalsyModuleController
{
    
    protected $moduleSettingListCustom = Array(
        
        "module_setting_calsy_quick_log_synch_vmoso_enabled",
        "module_setting_calsy_quick_log_synch_vmoso_connection_server",
        "module_setting_calsy_quick_log_synch_vmoso_connection_cid",
        "module_setting_calsy_quick_log_synch_vmoso_connection_user",
        "module_setting_calsy_quick_log_synch_vmoso_connection_password",
        "module_setting_calsy_quick_log_synch_vmoso_connection_chat_id"
        
    );
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_quick_log';
        $this->nameModuleShort = 'quick_log';
        $this->classModule = 'CalsyQuickLogModule';
        
    }
    
}