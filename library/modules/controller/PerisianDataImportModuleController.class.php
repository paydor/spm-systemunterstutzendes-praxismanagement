<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyBookableTimesModule.class.php';

class PerisianDataImportModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'data_import';
        $this->nameModuleShort = 'data_import';
        $this->classModule = 'PerisianDataImportModule';
        
        $this->hideLinkOverview = true;
        
    }
    
}