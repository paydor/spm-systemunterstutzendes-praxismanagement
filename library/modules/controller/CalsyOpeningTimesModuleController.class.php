<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyOpeningTimesModule.class.php';

class CalsyOpeningTimesModuleController extends CalsyModuleController
{
        
    protected function setup()
    {
        
        $this->nameModule = 'calsy_opening_times';
        $this->nameModuleShort = 'opening_times';
        $this->classModule = 'CalsyOpeningTimesModule';
        
        $this->pageCustom = 'calsy_opening_times';
        
    }
    
    /**
     * Provides data to display an overview for settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
     
        parent::actionOverview();
           
        $overallResult = $this->getResult();
        $result = $overallResult['result'];
        
        // Custom data
        {
                                                                        
            $result['list_opening_times'] = CalsyOpeningTimesModule::getOpeningTimesRaw();
                        
            $dayList = CalsyCalendar::getDayNames();

            $result['list_days'] = $dayList;
                        
        }
                        
        $this->setResultValue('result', $result);
    
    }
    
    /**
     * Saves the opening times for the calendar.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveOpeningTimes()
    {
                
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $times = PerisianFrameworkToolbox::getRequest('times');
                
        try
        {
            
            if(count($times) == 0)
            {
                
                // "Missing parameter(s)"
                
                throw new PerisianException(PerisianLanguageVariable::getVariable('10823'));
                
            }
            
            // Kick out empty values
            for($i = 0; $i < count($times); ++$i)
            {

                if(strlen($times[$i]["from"]) == 0 || strtolower($times[$i]["from"]) == "nan" || strlen($times[$i]["to"]) == 0 || strtolower($times[$i]["to"]) == "nan")
                {

                    unset($times[$i]);

                }

            }

            // Sort the times by day and start time
            {

                $timeSort = Array();

                foreach($times as $timeKey => $timeValue) 
                {

                    $timeSort['day'][$timeKey] = $timeValue['day'];
                    $timeSort['from'][$timeKey] = $timeValue['from'];

                }

                array_multisort($timeSort['day'], SORT_ASC, $timeSort['from'], SORT_ASC, $times);

            }

            $times = json_encode($times);

            $timeSetting = new PerisianSystemSetting(CalsyOpeningTimesModule::$settingNameOpeningTimes);

            $timeSetting->{$timeSetting->field_setting_value} = $times;
            
            $timeSetting->save();
            
            $result = array(
                
                "success" => true, 
                "message" => PerisianLanguageVariable::getVariable(10669)
                    
            );
            
        }
        catch(Exception $e)
        {
        
            $result = array(
                
                "success" => false, 
                "message" => $e->getMessage()
                    
            );    
            
        }
        
        $this->setResultValue('result', $result);
        
    }

}