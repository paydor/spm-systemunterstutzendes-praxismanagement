<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyBigBlueButtonModule.class.php';

class CalsyBigBlueButtonModuleController extends CalsyModuleController
{
    
    protected $hideLinkOverview = true;
    
    protected $moduleSettingListCustom = array(
        
        'big_blue_button_server_address', 'big_blue_button_server_secret'
        
    );
    
    protected function setup()
    {
        
        $this->nameModule = 'big_blue_button';
        $this->nameModuleShort = $this->nameModule;
        $this->classModule = 'CalsyBigBlueButtonModule';
                
    }
        
}
