<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyCheckInModule.class.php';
require_once 'calsy/checkin/CalsyCheckInLogSynch.class.php';

class CalsyCheckInModuleController extends CalsyModuleController
{
    
    protected $moduleSettingListCustom = Array(
        
        "module_setting_calsy_checkin_synch_vmoso_enabled",
        "module_setting_calsy_checkin_synch_vmoso_connection_server",
        "module_setting_calsy_checkin_synch_vmoso_connection_cid",
        "module_setting_calsy_checkin_synch_vmoso_connection_user",
        "module_setting_calsy_checkin_synch_vmoso_connection_password",
        "module_setting_calsy_checkin_synch_vmoso_connection_chat_id",
        "module_setting_calsy_checkin_synch_vmoso_connection_time"
        
    );
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_checkin';
        $this->nameModuleShort = 'checkin';
        $this->classModule = 'CalsyCheckInModule';
        
    }
    
    protected function actionOverview() 
    {
        
        parent::actionOverview();
        
        $result = $this->getResultValue('result');
        
        $result['synch_vmoso_times'] = CalsyCheckInLogSynch::getSynchTimesVmoso();
                        
        $this->setResultValue('result', $result);
        
    }
    
}