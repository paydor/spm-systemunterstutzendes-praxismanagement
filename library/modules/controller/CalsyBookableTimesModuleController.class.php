<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyBookableTimesModule.class.php';

class CalsyBookableTimesModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_bookable_times';
        $this->nameModuleShort = 'bookable_times';
        $this->classModule = 'CalsyBookableTimesModule';
        
    }
    
}