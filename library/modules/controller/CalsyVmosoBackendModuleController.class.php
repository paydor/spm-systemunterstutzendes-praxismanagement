<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyVmosoBackendModule.class.php';

class CalsyVmosoBackendModuleController extends CalsyModuleController
{
    
    protected $hideLinkOverview = true;
    
    protected $moduleSettingListCustom = Array(
        
        "module_setting_calsy_vmoso_connection_server",
        "module_setting_calsy_vmoso_connection_cid"
        
    );
    
    protected function actionOverview() 
    {
        
        parent::actionOverview();
        
        $result = $this->getResultValue('result');
        
        $result['list_layouts_chat'] = CalsyVmosoModule::getListChatLayouts();
        
        $this->setResultValue('result', $result);
        
    }
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_vmoso_backend';
        $this->nameModuleShort = 'vmoso_backend';
        $this->classModule = 'CalsyVmosoBackendModule';
        
    }
    
}
