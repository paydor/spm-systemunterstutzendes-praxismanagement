<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyProcessEngineModule.class.php';

class CalsyProcessEngineModuleController extends CalsyModuleController
{
    
    protected $hideLinkOverview = true;
    protected $pageCustom = 'process_engine';
    
    protected $moduleSettingListCustom = array(
        
        'process_engine_server_address', 'process_engine_server_user', 'process_engine_server_password'
        
    );
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_process_engine';
        $this->nameModuleShort = 'process_engine';
        $this->classModule = 'CalsyProcessEngineModule';
                
    }
        
}
