<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyOrderModule.class.php';

class CalsySupplierModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_supplier';
        $this->nameModuleShort = 'supplier';
        $this->classModule = 'CalsySupplierModule';
        
    }
    
}