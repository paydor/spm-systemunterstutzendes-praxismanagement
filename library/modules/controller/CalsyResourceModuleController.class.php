<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyResourceModule.class.php';

class CalsyResourceModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_resource';
        $this->nameModuleShort = 'resource';
        $this->classModule = 'CalsyResourceModule';
        
    }
    
}