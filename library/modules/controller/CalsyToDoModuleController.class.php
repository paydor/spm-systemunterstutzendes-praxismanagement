<?php

require_once 'modules/controller/CalsyModuleController.class.php';

class CalsyToDoModuleController extends CalsyModuleController
{
    
    protected $moduleSettingListCustom = Array();
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_todo';
        $this->nameModuleShort = 'todo';
        $this->classModule = 'CalsyToDoModule';
        
    }
    
}