<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyUserBackendGroupModule.class.php';

class CalsyUserBackendGroupModuleController extends CalsyModuleController
{
        
    protected $moduleSettingListCustom = Array();
        
    protected function setup()
    {
        
        $this->nameModule = 'calsy_user_backend_group';
        $this->nameModuleShort = 'user_backend_group';
        $this->classModule = 'CalsyUserBackendGroupModule';
        
    }
    
}
