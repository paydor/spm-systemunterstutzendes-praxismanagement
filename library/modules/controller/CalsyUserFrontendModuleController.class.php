<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyUserFrontendModule.class.php';

class CalsyUserFrontendModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_user_frontend';
        $this->nameModuleShort = 'user_frontend';
        $this->classModule = 'CalsyUserFrontendModule';
        
        $this->pageCustom = 'calsy_user_frontend';
        
    }
    
    /**
     * Stores the default custom fields for frontend users.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveCustomFields()
    {
                
        $renderer = array(

            'json' => true

        );

        $this->setResultValue('renderer', $renderer);
        
        //
        
        $result = array(
            
            'success' => false,
            'message' => lg('10484')
            
        );
        
        if($this->updateCustomFields($this->getParameter('custom_fields')))
        {
            
            $result = array(

                'success' => true,
                'message' => lg('10483')

            );
            
        }
              
        $this->setResultValue('result', $result);
        
    }
    
    /**
     * Updates this user's custom field data.
     * 
     * @author Peter Hamm
     * @param Array $customFieldData
     * @return boolean
     */
    public function updateCustomFields($customFieldData)
    {
        
        $validFields = Array();
        
        foreach($customFieldData as $customField)
        {

            $fieldId = strlen(@$customField['id']) > 0 ? PerisianFrameworkToolbox::security(@$customField['id']) : '';

            if(strlen($fieldId) == 0)
            {
                
                continue;
                
            }
            
            array_push($validFields, $customField);
            
        }
        
        $encodedFields = json_encode($validFields);
        
        PerisianSystemSetting::setSettingValue(CalsyUserFrontend::SETTING_VALUE_SYSTEM_CUSTOM_FIELDS, $encodedFields);
        
        return true;
        
    }
    
    /**
     * Provides data to display an overview for settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
     
        parent::actionOverview();
        
        $overallResult = $this->getResult();
        $result = $overallResult['result'];
        
        // Load custom module settings
        {
            
            $customSettingNames = array(
                
                'module_setting_calsy_user_frontend_display_history',
                'module_setting_calsy_user_frontend_merging_enabled'
                
            );
            
            $settingsCustom = PerisianSystemSetting::getSettingsByNames($customSettingNames);
                                    
            $result['list_settings_custom'] = $settingsCustom;
                        
        }
        
        for($i = 0; $i < count($result['list_settings_module']); ++$i)
        {
            
            if($result['list_settings_module'][$i]['setting_name'] == 'is_module_enabled_calsy_user_frontend_registration_and_calendar')
            {
                
                unset($result['list_settings_module'][$i]);
                
            }
            
        }
                
        sort($result['list_settings_module']);
        
        $result['user_frontend_object'] = new CalsyUserFrontend();
                
        $this->setResultValue('result', $result);
    
    }
    
}