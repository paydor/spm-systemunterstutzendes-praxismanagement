<?php

require_once 'framework/abstract/PerisianController.class.php';
require_once 'framework/PerisianLanguage.class.php';
require_once 'framework/PerisianModuleManager.class.php';

/**
 * Module overview controller
 *
 * @author Peter Hamm
 * @date 2016-12-30
 */
class CalsyModuleOverviewController extends PerisianController
{
        
    /**
     * Provides data for an overview of settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $renderer = array(
        
            'page' => 'module/module_overview'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $moduleList = PerisianModuleManager::getModuleList();
        
        //
        
        $result = array(
            
            'list_modules' => $moduleList
            
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Executes the controller, depending on the action and parameters given.
     * 
     * @throws PerisianException
     * @return void
     */
    public function execute()
    {
                
        $action = $this->getAction();
        
        if(strlen($action) == 0)
        {
            
            $action = 'overview';
            
        }
        
        $actionMethod = 'action' . ucfirst($action);
        
        if(!method_exists($this, $actionMethod))
        {
            
            // Undefined method.
            
            $message = str_replace("%", $action, PerisianLanguageVariable::getVariable('p58220c9c7460d'));
            
            throw new PerisianException($message);
            
        }
        
        $this->setResultValue('action', $this->getAction());
        
        $this->$actionMethod();
        
        return $this->getResult();
        
    }
    
}