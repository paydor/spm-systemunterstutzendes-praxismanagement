<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/CalsyAreaModule.class.php';

class CalsyMasterDataModuleController extends CalsyModuleController
{
    
    protected function setup()
    {
        
        $this->nameModule = 'calsy_master_data';
        $this->nameModuleShort = 'master_data';
        $this->classModule = 'CalsyMasterDataModule';
        
    }
    
}