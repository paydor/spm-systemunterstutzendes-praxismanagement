<?php

require_once 'modules/controller/CalsyModuleController.class.php';
require_once 'modules/PerisianPayPalModule.class.php';

class PerisianPayPalModuleController extends CalsyModuleController
{
    
    
    /**
     * Provides data to display an overview for settings for the module.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionOverview()
    {
        
        $renderer = array(
            
            'page' => 'module/perisian_paypal'
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $userId = $this->getParameter('u');
        $showAdminFunctions = false;
        
        $moduleObject = new $this->classModule();
        
        // Load the settings
        {
            
            $settingObjGeneral = new PerisianSetting();
            $settingObj = new PerisianSystemSetting();
            
            $systemSettingList = $settingObj->getPossibleSettings('admin');
            $systemSettings = $settingObj->getAssociativeValues();

            // Combine the possible rights with the values of this user to generate
            // a list that can easily processed in the template
            {

                for($i = 0, $m = count($systemSettingList); $i < $m; ++$i)
                {

                    if(isset($systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]]))
                    {
                        
                        $systemSettingList[$i]['value'] = $systemSettings[$systemSettingList[$i][$settingObj->foreign_field_pk]];
                        
                    }

                }
                
            }
                        
            {
                        
                // Add the module settings to a seperate list
                {
                
                    $moduleSettingList = PerisianSystemSetting::retrieveSettingsByPrefix('is_module_enabled_paypal', $systemSettingList, true);

                }
                                
                // Different module specific settings
                {
                    
                    $moduleSettingCustomList = PerisianSystemSetting::retrieveSettingsByPrefix('module_setting_paypal_', $systemSettingList, true);
                    
                }
                                
            }

        }
                
        $moduleLanguageVariables = $moduleObject::getLanguageVariables();
        
        $result = array(
            
            'object_module' => $moduleObject,
            
            'list_currencies' => PerisianPayPal::getCurrencies(),
            'list_rates_vat' => PerisianPayPal::getRatesVat(),
            
            'list_settings_module' => $moduleSettingList,
            'list_settings_module_custom' => $moduleSettingCustomList,
            
            'module_name' => $this->nameModule,
            'module_name_short' => $this->nameModuleShort,
            
            'hide_link_overview' => $this->hideLinkOverview,
                        
        );
        
        $this->setResultValue('result', $result);
                
    }
    
    /**
     * Saves the fields of the appointment assistant.
     * 
     * @author Peter Hamm
     * @return void
     */
    protected function actionSaveFields()
    {
                
        $renderer = array(
            
            'json' => true
            
        );
        
        $this->setResultValue('renderer', $renderer);
        
        //
        
        $allowedPages = $this->getAllowedPages();
        
        $result = array(
            
            "message" => PerisianLanguageVariable::getVariable(11095),
            "success" => true
            
        );
        
        try
        {
            
            // Save it...
            
        }
        catch(Exception $e)
        {
            
            $result["success"] = false;
            $result["message"] = PerisianLanguageVariable::getVariable(11096) . "\n\"" . $e->getMessage() . "\"";
            
        }
        
        
        $this->setResultValue('result', $result);
        
    }
    
    protected function setup()
    {
        
        $this->nameModule = 'paypal';
        $this->nameModuleShort = 'paypal';
        $this->classModule = 'PerisianPayPalModule';
        
    }
    
}