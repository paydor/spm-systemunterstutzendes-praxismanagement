<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';

/**
 * Data import module
 * 
 * @author Peter Hamm
 * @date 2016-06-18
 */
class PerisianDataImportModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_data_import");
    
    protected static $languageVariables = array(
        
        'p57640984b2547', 'p57655b72a1219', 'p57656e6a5df71', 'p57656ec89cf96',
        'p5765754dbf3bf', 'p576575c7bd67d', 'p576596e616ee5'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p57655b72a1219') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . static::getModuleIdentifier() . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "data_import";
        
    }
    
}
