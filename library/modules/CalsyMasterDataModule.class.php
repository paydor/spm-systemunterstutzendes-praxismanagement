<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/master_data/CalsyMasterData.class.php';

/**
 * Frontend user module
 * 
 * @author Peter Hamm
 * @date 2016-05-02
 */
class CalsyMasterDataModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array(
        
        "is_module_enabled_calsy_master_data"
        
    );
    
    protected static $languageVariables = array(
        
        'p5e615d660ac5d', 'p5e619e2b40ad1', 'p5e619aabbe428', 'p5e619a9e6c331',
        'p5e619a805892d', 'p5e6196bdf0bb4', 'p5e6195d835329', 'p5e618eb631602',
        'p5e618ce4174cd', 'p5e618c6b95f3e', 'p5e61887628d17', 'p5e618288a7011',
        'p5e617eff8831e', 'p5e617daca8fc8', 'p5e61773087e16', 'p5e61744c605b2',
        'p5e6173a9037ba', 'p5e616e696f590', 'p5e616d42da7f2', 'p5e6169b7ee6ce',
        'p5e6167614d39d', 'p5e616623caef6', 'p5e616611e37fe', 'p5e6165be6ed34'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p5e615d660ac5d') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_master_data";
        
    }
    
}
