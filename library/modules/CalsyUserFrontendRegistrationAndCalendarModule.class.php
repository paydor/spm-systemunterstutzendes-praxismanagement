<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/calendar/CalsyCalendar.class.php';
require_once 'calsy/enhanced_fields/CalsyAppointmentField.class.php';

/**
 * Frontend user registration module
 * 
 * @author Peter Hamm
 * @date 2016-02-13
 */
class CalsyUserFrontendRegistrationAndCalendarModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_user_frontend_registration_and_calendar");
    
    protected static $appointmentAssistantDisplayStatus = null;
        
    /**
     * Retrieves the possible statuses for the appointment assistant in the frontend.
     * 
     * @author Peter Hamm
     * @return array
     */
    public static function getStatusesForAppointmentAssistant()
    {
        
        $statuses = array(
            
            "big" => PerisianLanguageVariable::getVariable(11000),
            "small" => PerisianLanguageVariable::getVariable(11001),
            "none" => PerisianLanguageVariable::getVariable(11002)
            
        );
        
        return $statuses;
        
    }
    
    /**
     * Retrieves whether the appointment assistant should display a list
     * of upcoming appointment proposals as a tile list or not.
     * 
     * @author Peter Hamm
     * @return Boolean
     */
    public static function isEnabledTileProposals()
    {
        
        $settingValue = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_enabled_tile_view_propsals');
        
        return $settingValue == "1";
        
    }
    
    /**
     * Retrieves whether multiple bookings are allowed or not.
     * 
     * @author Peter Hamm
     * @return Boolean
     */
    public static function isEnabledMultipleBookings()
    {
        
        $settingValue = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_multiple_booking_enabled');
        
        // @todo: Remove this
        {
            
            global $userFrontend;

            if(!isset($userFrontend) || !$userFrontend->isLoggedIn() || $userFrontend->{$userFrontend->field_pk} != 2827)
            {

                //throw new PerisianException("Invalid parameter.");

            }
            
        }
        
        return $settingValue == "1";
        
    }
    
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable(11071) . '"';
        
        return $title;
        
    }
        
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_user_frontend_registration_and_calendar";
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        return '';
        
    }
    
    /**
     * Retrieves whether the appointments created by frontend users are automatically confirmed or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function autoConfirmFrontendAppointments()
    {
        
        $value = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_automatically_confirm_entries') == "1";
        
        return $value;
        
    }
    
    /**
     * Retrieves whether the frontend users are allowed to cancel an appointment or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function userFrontendMayCancelAppointment()
    {
        
        $value = PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_enable_appointment_cancellation') == "1";
        
        return $value;
        
    }
    
    /**
     * Retrieves the display status for the appointment assistant.
     * It's either "none", "small" or "big"
     * 
     * @author Peter Hamm
     * @return type
     */
    public static function getAppointmentAssistantDisplayStatus()
    {
        
        if(static::$appointmentAssistantDisplayStatus == null)
        {
            
            $setting = new PerisianSystemSetting('module_setting_calsy_user_frontend_registration_and_calendar_is_appointment_assistant_enabled');
            static::$appointmentAssistantDisplayStatus = $setting->getValue();
            
        }
        
        if(strlen(static::$appointmentAssistantDisplayStatus) == 0)
        {
            
            static::$appointmentAssistantDisplayStatus = "none";
            
        }

        return static::$appointmentAssistantDisplayStatus;
        
    }
    
    /**
     * Retrieves whether frontend users are allowed to pick the user or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isUserSelectionEnabled()
    {
        
        $setting = new PerisianSystemSetting('module_setting_calsy_user_frontend_registration_and_calendar_is_user_selection_enabled');
        
        return $setting->getValue() == 1;
        
    }
    
    /**
     * Generates a simple array list of available areas.
     * The keys are the IDs, the values the titles of the area.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @return array
     */
    public static function getAreas()
    {
        
        global $userFrontend;
        
        $dummy = new CalsyArea();
        
        $areaList = CalsyArea::getAreaList("",  $dummy->field_title);
        
        $filterByGender = PerisianSystemSetting::getSettingValue('calendar_on_select_account_filter_gender') == 1;
                
        if(isset($userFrontend) && is_object($userFrontend))
        {

            $areaListFiltered = Array();

            // Show only areas for the gender of the frontend user
            
            for($i = 0; $i < count($areaList); ++$i)
            {
                                
                if($areaList[$i][$dummy->field_disabled_frontend] == "1")
                {
                    
                    continue;
                    
                }
                
                $genderIsOkay = ($areaList[$i][$dummy->field_gender] == 'none' || $areaList[$i][$dummy->field_gender] == $userFrontend->{$userFrontend->field_sex} || $userFrontend->{$userFrontend->field_sex} == 'none');

                if(!$filterByGender || !$userFrontend->isLoggedIn() || ($filterByGender && $genderIsOkay))
                {

                    array_push($areaListFiltered, $areaList[$i]);

                }

            }

            $areaList = $areaListFiltered;

            unset($areaListFiltered);

        }
        
        
        $formattedAreaList = array();
        
        for($i = 0; $i < count($areaList); ++$i)
        {
            
            $formattedAreaList[$areaList[$i][$dummy->field_pk]] = $areaList[$i][$dummy->field_title];
            
        }
        
        return $formattedAreaList;
        
    }
    
    /**
     * Combines the specified form field data with the specified prefill data.
     * 
     * @author Peter Hamm
     * @param int $targetTabIndex The targetted tab index. Only fields on tabs with an index smaller or equal than this number will be filled out.
     * @param Array $pageFormData Data retrieves from static::getAppointmentAssistantFormFields()
     * @param Array $prefillData For example: Data from a request.
     * @return Array The combination
     */
    public static function combineFormFieldsWithPrefillData($targetTabIndex, $pageFormData, $prefillData)
    {
        
        for($tabIndex = 0; $tabIndex <= $targetTabIndex; ++$tabIndex)
        {
            
            if(!isset($pageFormData[$tabIndex]))
            {
                
                continue;
                
            }
                        
            for($fieldIndex = 0; $fieldIndex < count($pageFormData[$tabIndex]['fields']); ++$fieldIndex)
            {

                if(isset($pageFormData[$tabIndex]['fields'][$fieldIndex]['type']) && $pageFormData[$tabIndex]['fields'][$fieldIndex]['type'] == "date_selection")
                {
                    
                    if(isset($prefillData['aa_date_selection_earliest_possible_date']))
                    {
                        
                        $pageFormData[$tabIndex]['fields'][$fieldIndex]['begin_date'] = $prefillData['aa_date_selection_earliest_possible_date'];
                        
                    }
                    
                    if(isset($prefillData['aa_date_selection_earliest_possible_time']))
                    {
                        
                        $pageFormData[$tabIndex]['fields'][$fieldIndex]['begin_time'] = $prefillData['aa_date_selection_earliest_possible_time'];
                        
                    }
                    
                    if(isset($prefillData['aa_date_selection_earliest_possible_time']))
                    {
                        
                        $pageFormData[$tabIndex]['fields'][$fieldIndex]['begin_timestamp_earliest'] = $prefillData['aa_date_selection_earliest_possible_timestamp'];
                        
                    }
                                       
                }
                
                if(!@$pageFormData[$tabIndex]['fields'][$fieldIndex]['identifier'])
                {
                    
                    continue;
                    
                }
                
                $prefillFieldValue = static::getPrefillValueForFieldIdentifier($prefillData, $pageFormData[$tabIndex]['fields'][$fieldIndex]['identifier']);
                
                if(!is_null($prefillFieldValue))
                {
                                        
                    if($pageFormData[$tabIndex]['fields'][$fieldIndex]['identifier'] == 'user_backend')
                    {
                        
                        // If applicable: Load the possible backend user for the specified area
                        
                        $prefilledAreaId = static::getPrefillValueForFieldIdentifier($prefillData, 'area');
                        
                        if(!is_null($prefilledAreaId) && $prefilledAreaId > 0)
                        {
                            
                            $formattedUserList = array();
                            $userDummy = new CalsyUserBackend();

                            {

                                $userList = CalsyAreaUserBackend::getUsersForArea($prefilledAreaId);

                                for($i = 0; $i < count($userList); ++$i)
                                {

                                    $formattedUserList[$userList[$i]->{$userDummy->field_pk}] = $userList[$i]->{$userDummy->field_fullname};

                                }

                            }

                            asort($formattedUserList);
                            
                            $pageFormData[$tabIndex]['fields'][$fieldIndex]['values'] = $formattedUserList;
                            
                        }
                        
                        $prefilledBackendUserId = $prefillData;
                                                
                    }
                    
                    $pageFormData[$tabIndex]['fields'][$fieldIndex]['prefillValue'] = $prefillFieldValue;
                    
                }
                
            }
                        
        }
                
        return $pageFormData;
        
    }
    
    /**
     * Retrieves the value for the field with the specified $fieldIdentifier from the specified $prefillData.
     * 
     * @author Peter Hamm
     * @param Array $prefillData
     * @param String $fieldIdentifier
     * @return mixed Either "null" if the field was not found in the $prefillData, or the value that was retrieved from it.
     */
    protected static function getPrefillValueForFieldIdentifier($prefillData, $fieldIdentifier)
    {
        
        $returnValue = null;
        
        foreach($prefillData as $fieldKey => $fieldValue)
        {
            
                        
            if(is_array($fieldValue))
            {
                
                $arrayValue = static::getPrefillValueForFieldIdentifier($fieldValue, $fieldIdentifier);
                
                if(!is_null($arrayValue))
                {
                    
                    return $arrayValue;
                    
                }
                
            }
            
            if($fieldKey == $fieldIdentifier)
            {
                
                return $fieldValue;
                
            }
            
        }
        
        return $returnValue;
        
    }
    
    /**
     * Retrieves the list of form fields for the appointment assistant.
     * 
     * @author Peter Hamm
     * @global CalsyUserFrontend $userFrontend
     * @param Array $customEnhancedFields Optional, a list of fields from CalsyAppointmentField::getFormattedList(). Default: Ignored.
     * @return array
     */
    public static function getAppointmentAssistantFormFields($customEnhancedFields = array())
    {
        
        global $userFrontend;
        
        $formFields = array();
        
        // Tab 1
        {
            
            {
                
                $tabOne = array(

                    "title" => PerisianLanguageVariable::getVariable(11026),
                    "fields" => array()

                );
                                
                array_push($formFields, $tabOne);
                
            }
            
        }
        
        // Tab 2
        {
            
            $tabData = array(

                "title" => PerisianLanguageVariable::getVariable(11027),
                "fields" => array()

            );
            
            if(static::isUserSelectionEnabled())
            {
                
                // Backend User selection
                $formattedUserList = array();
                
                $userList = CalsyAreaUserBackend::getUsersForArea(0, false, false);
                
                for($i = 0; $i < count($userList); ++$i)
                {

                    $formattedUserList[$userList[$i]->{$userList[$i]->field_pk}] = $userList[$i]->{$userList[$i]->field_fullname};

                }
                
                /*
                $userDummy = new CalsyUserBackend();
                
                if(!CalsyAreaModule::isEnabled())
                {
                    
                    $userList = CalsyUserBackend::getUserList();
                    
                    for($i = 0; $i < count($userList); ++$i)
                    {
                        
                        $formattedUserList[$userList[$i][$userDummy->field_pk]] = $userList[$i][$userDummy->field_fullname];
                        
                    }
                                        
                }
                
                asort($formattedUserList);
                
                */
                
                array_push($tabData['fields'], 

                    array(

                        "title" => PerisianLanguageVariable::getVariable(10896),
                        "identifier" => "user_backend",
                        "value" => "",
                        "type" => "select",
                        "required" => false,
                        "values" => $formattedUserList

                    )

                );
                
            }
            
            if(static::isEnabledMultipleBookings())
            {
                
                array_push($tabData['fields'], 

                    array(

                        "title" => PerisianLanguageVariable::getVariable('p5e390d8ee252f'),
                        "identifier" => "multiple-booking",
                        "value" => "",
                        "type" => "multiple-booking",
                        "required" => false

                    )

                );
                
            }
            
            array_push($tabData['fields'], 

                array(

                    "title" => PerisianLanguageVariable::getVariable(11030),
                    "identifier" => "annotations",
                    "value" => "",
                    "type" => "textarea",
                    "required" => false

                )
                    
            );
            
            array_push($formFields, $tabData);
            
        }
        
        // Tab 3
        {
            
            $dateBegin = date("d.m.Y", PerisianTimeZone::getFirstMinuteOfDayAsTimestamp(time() + (PerisianSystemSetting::getSettingValue('module_setting_calsy_user_frontend_registration_and_calendar_is_enabled_booking_same_day') == '1' ? 0 : 86400)));
            $timeBegin = gmdate("H:i", CalsyCalendar::getDefaultEventStartTime());
                        
            array_push($formFields, array(

                "title" => PerisianLanguageVariable::getVariable('p5a063d6f8d62a'),
                "fields" => array(

                    array(

                        "type" => "date_selection",
                        "begin_date" => $dateBegin,
                        "begin_time" => $timeBegin,
                        "begin_timestamp_earliest" => -1

                    )

                )

            ));
            
        }
        
        // Tab 4
        if(!isset($userFrontend) || !$userFrontend->isLoggedIn())
        {
            
            array_push($formFields, array(

                "title" => PerisianLanguageVariable::getVariable(11013),
                "fields" => array(

                    array(

                        "type" => "registration_login"

                    )

                )

            ));
        
        }
        
        // Tab 5
        {
            
            array_push($formFields, array(

                "title" => PerisianLanguageVariable::getVariable(11028),
                "fields" => array(

                    array(

                        "type" => "approval"

                    )

                )

            ));
            
        }
        
        if(count($customEnhancedFields) > 0)
        {
            
            $formFields = static::enhanceFormFieldsWithCustomFields($formFields, $customEnhancedFields);
            
        }
        
        return $formFields;
        
    }
    
    /**
     * Enhances the efault form fields with custom fields specified in the backend.
     * 
     * @author Peter Hamm
     * @param Array $formFields
     * @param Array $customEnhancedFields
     * @return Array
     */
    public static function enhanceFormFieldsWithCustomFields($formFields, $customEnhancedFields)
    {
                        
        $areaModule = new CalsyAreaModule();
                
        foreach($customEnhancedFields['pages'] as $pageIndex => $pageContent)
        {
            
            if(!isset($formFields[$pageIndex]))
            {
                
                continue;
                
            }
            
            $oldFields = $formFields[$pageIndex]['fields'];
            
            $formFields[$pageIndex]['fields'] = array();
            
            foreach($pageContent as $newField)
            {
                
                $newFormattedField = array(
                    
                    'is_additional' => true,
                    'identifier' => $newField['identifier'],
                    'type' => $newField['type'],
                    'title' => PerisianLanguageVariable::getVariable($newField['label']),
                    'required' => (isset($newField['data']) && isset ($newField['data']['required']) ? $newField['data']['required'] : false),
                    'condition' => CalsyAppointmentField::buildConditionJavascript(@$newField['data']['conditions'])
                    
                );
                
                if($newField['type'] == 'radio' || $newField['type'] == 'select')
                {
                    
                    $newFormattedField['values'] = @$newField['data']['values'];
                    
                    foreach($newFormattedField['values'] as &$valueContent)
                    {
                        
                        $valueContent = PerisianLanguageVariable::getVariable($valueContent);
                        
                    }
                    
                }
                else if($newField['type'] == 'area-filter' || $newField['type'] == 'area-group-filter')
                {
                    
                    $newFormattedField['value'] = $newField['data']['value'];
                    
                }
                else if($newField['type'] == 'area-selector')
                {
                    
                    if(!$areaModule->isModuleEnabled())
                    {
                        
                        continue;
                        
                    }
                    
                    $newFormattedField['title'] = PerisianLanguageVariable::getVariable(11018);
                    $newFormattedField['identifier'] = "area";
                    $newFormattedField['value'] = "";
                    $newFormattedField['type'] = "radio";
                    $newFormattedField['required'] = true;
                    $newFormattedField['values'] = static::getAreas(true);
                                        
                }
                
                array_push($formFields[$pageIndex]['fields'], $newFormattedField);
                   
            }
            
            $formFields[$pageIndex]['fields'] = array_merge_recursive($formFields[$pageIndex]['fields'], $oldFields);
            
        }
        
        return $formFields;
        
    }
        
}
