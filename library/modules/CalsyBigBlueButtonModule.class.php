<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/big_blue_button/CalsyBigBlueButtonApi.class.php';

/**
 * BigBlueButton module
 * 
 * @author Peter Hamm
 * @date 2018-05-11
 */
class CalsyBigBlueButtonModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_big_blue_button");
    
    protected static $languageVariables = array(
        
        'p5af5f9f3dcb26'
        
    );
    
    private static $apiConnection = null;
    
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p5af5f9f3dcb26') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "big_blue_button";
        
    }
    
    /**
     * Retrieves a connection to the BigBlueButton server's API.
     * 
     * @author Peter Hamm
     * @return CalsyBigBlueButtonApi
     */
    public static function getApiConnection()
    {
        
        if(is_null(static::$apiConnection))
        {
            
            $server = PerisianSystemSetting::getSettingValue('big_blue_button_server_address');
            $secret = PerisianSystemSetting::getSettingValue('big_blue_button_server_secret');
            
            static::$apiConnection = new CalsyBigBlueButtonApi($server, $secret);
            
        }
        
        return static::$apiConnection;
        
    }
        
}

