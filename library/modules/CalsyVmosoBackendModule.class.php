<?php

require_once 'CalsyVmosoModule.class.php';

/**
 * Vmoso module
 * 
 * @author Peter Hamm
 * @date 2017-01-05
 */
class CalsyVmosoBackendModule extends CalsyVmosoModule
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_vmoso_backend");
    
    protected static $languageVariables = array(
        
        'p592c40ed01fc6'
        
    );
    
    public static function getCalsyVmosoUserList()
    {
        
        $dummy = new CalsyUserBackend();
        
        $listUsers = CalsyUserBackend::getListUsersWhereSettingHasValueNotEmpty('user_vmoso_account_key');
        $listUsersFormatted = Array();
        
        if(count($listUsers) > 0)
        {
            
            for($i = 0; $i < count($listUsers); ++$i)
            {
                
                $userData = Array(
                    
                    $dummy->field_pk => $listUsers[$i][$dummy->field_pk],
                    $dummy->field_fullname => $listUsers[$i][$dummy->field_fullname],
                    $dummy->field_color => $listUsers[$i][$dummy->field_color],
                    'vmoso_key' => PerisianUserSetting::getSettingValue($listUsers[$i][$dummy->field_pk], 'user_vmoso_account_key')
                    
                );
                
                array_push($listUsersFormatted, $userData);
                
            }
            
        }
        
        return $listUsersFormatted;
        
    }
    
    /**
     * Retrieves the currently active chat layout for the backend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userId
     * @return String
     */
    public static function getChatLayoutForUser($userId)
    {
        
        $selectedLayout = '';
        
        try
        {
            
            $selectedLayout = PerisianUserSetting::getSettingValue($userId, 'user_vmoso_chat_layout');
            
        }
        catch(PerisianException $e)
        {

        }
        
        if(strlen($selectedLayout) == 0)
        {
            
            // There is no default system layout, just define it here
            
            $selectedLayout = 'technical';
            
        }
        
        return $selectedLayout;
        
    }
            
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p592c40ed01fc6') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
                
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/chat/';
                
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_vmoso_backend";
        
    }
    
}
