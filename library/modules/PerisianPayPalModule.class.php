<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'framework/paypal/PerisianPayPal.class.php';

/**
 * PayPal module
 * 
 * @author Peter Hamm
 * @date 2017-10-10
 */
class PerisianPayPalModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_paypal");
    
    protected static $languageVariables = array(
        
        'p59dde99bc7d89', 'p59ddeac848460', 'p59ddead429c3b', 'p59ddeadeccd83',
        'p59ddeaed2ff2c', 'p59ddeb1f1edab'
        
    );
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p59dde99bc7d89') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
                
        return 'fa-paypal';
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . static::getModuleIdentifier() . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return 'paypal';
        
    }
    
}

