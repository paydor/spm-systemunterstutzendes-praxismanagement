<?php

require_once 'framework/abstract/PerisianModuleAbstract.class.php';
require_once 'calsy/vmoso/CalsyVmosoChatBackend.class.php';

/**
 * Vmoso module
 * 
 * @author Peter Hamm
 * @date 2017-01-05
 */
class CalsyVmosoModule extends PerisianModuleAbstract
{
    
    // The list of required settings for this module to be enabled.
    protected $requiredSettingNames = array("is_module_enabled_calsy_vmoso");
    
    protected static $languageVariables = array(
        
        'p586e81756f0c8', 'p586e81bc9f14c', 'p586e8eb9176cc', 
        'p586eae0617158', 'p586eba3abad01', 'p58cbd16d3f05e'
        
    );
    
    /**
     * Retrieves the frontend address of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public function getFrontendAddress()
    {
        
        $address = '/vmoso/chat/';
        
        return $address;
        
    }
    
    /**
     * Retrieves whether frontend users may pick their own choice for the chat layout or not.
     * 
     * @author Peter Hamm
     * @return bool
     */
    public static function isCustomChatLayoutForUserFrontendEnabled()
    {
        
        $isCustomLayoutEnabled = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_chat_layout_selection_for_accounts_enabled') == '1';
        
        return $isCustomLayoutEnabled;
        
    }
    
    /**
     * Retrieves the currently active chat layout for the frontend user with the specified identifier.
     * 
     * @author Peter Hamm
     * @param int $userFrontendId
     * @return String
     */
    public static function getChatLayoutForUserFrontend($userFrontendId)
    {
        
        $selectedLayout = '';
        
        if(static::isCustomChatLayoutForUserFrontendEnabled())
        {
            
            // Try to load the frontend user's setting
            
            try
            {
                
                $userFrontend = new CalsyUserFrontend($userFrontendId);
                
                $selectedLayout = $userFrontend->getSettingValue('account_vmoso_chat_layout');
            
            }
            catch(PerisianException $e)
            {
                
            }
            
        }
        
        if(strlen($selectedLayout) == 0)
        {
            
            // Just use the system default
            
            $selectedLayout = PerisianSystemSetting::getSettingValue('module_setting_calsy_vmoso_chat_layout_default');
            
        }
        
        if(strlen($selectedLayout) == 0)
        {
            
            // There is no default system layout, just define it here
            
            $selectedLayout = 'technical';
            
        }
        
        return $selectedLayout;
        
    }
    
    /**
     * Retrieves a list of available layouts for the chat as an associative Array.
     * 
     * @author Peter Hamm
     * @return Array
     */
    public static function getListChatLayouts()
    {
        
        $list = array(
            
            'technical' => PerisianLanguageVariable::getVariable('p58e23497364d5'),
            'modern' => PerisianLanguageVariable::getVariable('p58e2349f161ca')
            
        );
        
        return $list;
        
    }
        
    /**
     * Retrieves the name of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleName() 
    {
        
        $title = PerisianLanguageVariable::getVariable(11116) . ' "' . PerisianLanguageVariable::getVariable('p586e81bc9f14c') . '"';
        
        return $title;
        
    }
    
    /**
     * Retrieves the icon of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIcon() 
    {
        
        $settingName = 'module_icon_' . static::getModuleIdentifier();
        
        return PerisianSystemSetting::getSettingValue($settingName);
        
    }
    
    /**
     * Retrieves the backend address of this module
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getBackendAddress() 
    {
        
        $backendAddress = '/' . str_replace("calsy_", "", static::getModuleIdentifier()) . '/overview/';
        
        return $backendAddress;
        
    }
    
    /**
     * Retrieves the unique identifier of this module.
     * 
     * @author Peter Hamm
     * @return string
     */
    public static function getModuleIdentifier() 
    {
        
        return "calsy_vmoso";
        
    }
    
}
