<?php

try
{
    
    $disableLoginScreen = true;

    require_once 'includes/header.inc.php';
    require_once 'framework/database/PerisianDatabaseMigrationController.class.php';
        
    $contentController = new PerisianDatabaseMigrationController();
    
    $contentController->setParameter('data', @$e);
    $contentController->setParameter('is_frontend', @$isFrontend);
    
    PerisianControllerWeb::handleContent($contentController);
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
