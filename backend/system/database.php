<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'external/FineDiff/FineDiff.class.php';

    $do = PerisianFrameworkToolbox::getRequest('do');
    
    $databaseInstanceName = 'main';
    $databaseConnection = PerisianMysql::getInstance($databaseInstanceName);
    $databaseFileUploadTarget = PerisianFrameworkToolbox::getConfig('basic/project/folder') . 'files/upload/database/db-upload.sql';
    $isExportPossible = PerisianMysql::isExportPossible();
    
    if(empty($do))
    {
                
        $page = 'system/system_database';
        
        $databaseInfo = $databaseConnection->getInfo();

    }
    else if($do == 'dump')
    {
        
        PerisianFrameworkToolbox::blankPage();
        
        if($isExportPossible)
        {
            
            $filename = date("Y-m-d") . "-dump-complete.sql";
            $mime = "application/sql";

            $dumpData = $databaseConnection->dump($databaseInstanceName);
            
            if(strlen($dumpData) > 0 && $dumpData != "127")
            {
                
                header("Content-Type: " . $mime);
                header('Content-Disposition: attachment; filename="' . $filename . '"');
                                
                echo $dumpData;
                
            }
            else
            {
                
                echo PerisianLanguageVariable::getVariable('p57b4479a6de58');
                
            }

            exit;
            
        }
        else
        {
            
            // Data export impossible on this system
            echo PerisianLanguageVariable::getVariable('p57b4479a6de58');
            
        }

        exit;
        
    }
    else if($do == 'upload_database')
    {
        
        PerisianFrameworkToolbox::blankPage();
        
        $formTitle = PerisianLanguageVariable::getVariable('p57b587a0c56ce');
        $page = 'system/modal/system_database_upload';
        
    }
    else if($do == 'upload_database_file')
    {
        
        PerisianFrameworkToolbox::blankPage();
        
        $result = "false";
        
        if(!empty($_FILES))
        {
            
            $uploadedFile = $_FILES['file']['tmp_name'];
            move_uploaded_file($uploadedFile, $databaseFileUploadTarget);
            
            $result = "true";
            
        }        
        
        echo json_encode(array("success" => $result));
        
    }
    else if($do == 'diff')
    {
        
        PerisianFrameworkToolbox::blankPage();
                
        $databaseStringOld = $databaseConnection->dump($databaseInstanceName);
        $databaseStringNew = file_get_contents($databaseFileUploadTarget);

        $databaseDiff = new FineDiff($databaseStringOld, $databaseStringNew, FineDiff::$paragraphGranularity);

        $diffResult = $databaseDiff->renderDiffToHTML();
        
        @unlink($databaseFileUploadTarget);
        
        echo nl2br($diffResult);
        
    }
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
