<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/system/CalsySystemSettingController.class.php';
        
    $contentController = new CalsySystemSettingController();
    
    if(!isset($do) || strlen($do) == 0)
    {
        
        $contentController->setAction('status');
                
    }
    
    PerisianControllerWeb::handleContent($contentController);
        
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
