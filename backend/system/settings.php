<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/system/CalsySystemSettingController.class.php';
        
    $contentController = new CalsySystemSettingController();
    
    $do = PerisianFrameworkToolbox::getRequest('do');
    
    if(!isset($do) || strlen($do) == 0)
    {
        
        $do = 'settings';
                
    }
    
    $contentController->setAction($do);
    
    PerisianControllerWeb::handleContent($contentController);
    
    $menu->setActiveMenuPoint(10);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}