<?php

// Debug file for various tests

exit;

$url = "https://baden.liga.nu/cgi-bin/WebObjects/nuLigaTENDE.woa/wa/teamPortrait?team=2081511&championship=B3+W+17%2F18";
$target = "Bezirk 3 Winter 2017/2018";

{
    
    $dataFile = substr($url, strrpos($url, "?") + 1);
    $dataFile = __DIR__ . '/' . mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $dataFile) . '.dat';
        
    $readExistingFile = false;
    
    if(file_exists($dataFile))
    {
        
        if(filemtime($dataFile) >= time() - 3600)
        {
            
            $readExistingFile = true;
            
        }
        
    }
    
    if($readExistingFile)
    {
        
        $data = file_get_contents($dataFile);
        
    }
    else
    {
        
        $data = file_get_contents($url);
        
        @unlink($dataFile);
        file_put_contents($dataFile, $data);
            
    }

    {
        
        //$data = utf8_decode($data);
        
        $data = substr($data, strpos($data, $target));
        $data = substr($data, strpos($data, "<table"));
        $data = substr($data, 0, strpos($data, "</table>") + 8);
        
        $data = str_replace('<a href="', '<a href="https://baden.liga.nu', $data);
        
    }
    
    echo $data;
    
}
?>
<style type="text/css">
    
    .result-set
    {
        
        border: 1px solid #ccc;
        
    }
    
    .result-set th,
    .result-set td
    {
        
        text-align: left;
        padding: 4px;
        border: 1px solid #ccc;
        
    }
    
    .result-set tr td:first-child
    {
        
        border-right: 0;
        
    }
    
    .result-set tr td:nth-child(2)
    {
        
        border-left: 0;
        
    }
    
</style>
