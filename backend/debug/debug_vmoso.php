<?php

exit;

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/vmoso/CalsyVmosoChatUserFrontend.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    error_reporting(E_ALL);
    
    $connection = new CalsyVmosoChatUserFrontend(2827);

    $result = $connection->test();
    
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo "API fatal error: " . $e->getMessage();
    
    echo"<br>\nDebug info: ";
    
    print_r($e->getDebugInfo());
    
    exit;
    
}