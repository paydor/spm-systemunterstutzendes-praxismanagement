<?php


try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    set_time_limit(10);
    
    // Send an e-mail
    try
    {
        
        // Setup
        {
        
            $recipient = "peter.hamm@cons-sys.com"; //$user->{$user->field_email};
            $recipientName = "Peter"; //$user->{$user->field_fullname};
            
            $mailSubject = "Test E-Mail";
            $mailContentText = "Test E-Mail...";
            
            $mailContent = PerisianMail::getDefaultEmailTemplateContent($mailSubject, $mailContentText);
            
        }
        
        echo"Sending a test e-mail to \"" . $recipient . "\".<br>";
        
        {
            
            require_once 'external/Pear/Mail/mail.php';
            require_once 'external/Pear/Mail/mail.php';
            
            
            
        }
        
        echo"... mail sent!";

    }
    catch(Exception $e)
    {
                
        throw new PerisianException($e->getMessage());
        
    }
       
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
    exit;
    
}

