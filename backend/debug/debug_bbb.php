<?php

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'modules/CalsyBigBlueButtonModule.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    set_time_limit(10);
    
    try
    {
        
        $connection = CalsyBigBlueButtonModule::getApiConnection();
        
        $result = $connection->create('test123', 'pwAttendee123', 'pwModerator456', Array('name' => 'Test-Meeting-xyz'));
        print_r($result);
        
        //$result = $connection->getMeetings();
        //$result = $connection->isMeetingRunning('test123');
        
        $joinUrl = $connection->getJoinURL('Peter', 'test123', 'pwModerator456');
        
        echo"<a href=\"" . $joinUrl . "\">Hier klicken</a>, um beizutretren.";
        
    }
    catch(Exception $e)
    {
                
        throw new PerisianException($e->getMessage());
        
    }
    
    //echo json_encode($result);
       
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
    exit;
    
}
