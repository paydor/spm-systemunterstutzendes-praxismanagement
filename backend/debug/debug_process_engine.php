<?php

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/process_engine/CalsyProcessEngineConnection.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    echo"Loading the process list...<br>\n";
    
    $processList = CalsyProcessEngineModule::getProcessList();
    
    print_r($processList);
    
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
        
    echo $e->getMessage();
    
    exit;
    
}
