<?php

try
{
        
    require_once __DIR__ . '/../includes/header.inc.php';
    require_once __DIR__ . '/../includes/menu.inc.php';
    
    require_once 'modules/PerisianPayPalModule.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    require_once __DIR__ . '/../includes/menu.inc.php';
    
    set_time_limit(20);
    
    try
    {
    
        $page = 'debug/default';
        
        $action = PerisianFrameworkToolbox::getRequest('do');
        
        $result = Array(
            
            "output" => "",
            "exception" => null
            
        );
        
        if(strlen($action) == 0)
        {
            
            $payPal = new PerisianPayPal();
            
            // The item we want to sell
            {
                                
                $item = new PerisianPayPalItem();
                
                $item->title = 'Buchung';
                $item->description = 'Beschreibung dieser Zahlung...';
                $item->customIdentifier = '1425';
                $item->price = 120.00;
                $item->currencyCode = 'CAD';
                $item->taxRate = 1.10;
                $item->type = PerisianPayPalTransaction::TYPE_CALENDAR_ENTRY;
                
                $payPal->setItem($item);
                
            }
                        
            $transactionResult = $payPal->createPayment();
            
            if($transactionResult["success"])
            {
                
                $result["output"] = 'Danke, Ihre Buchung haben wir empfangen.<br/>Jetzt <a href="' . $transactionResult["url"] . '">Zahlung durchführen</a>';
                
            }
            else
            {
                
                $result["output"] = $transactionResult["message"];
                $result["exception"] = $transactionResult["exception"];
                
            }
            
        }
        else if($action == 'confirm')
        {
            
            $payPal = new PerisianPayPal();
            
            $transactionResult = $payPal->handlePaymentResult();
            
            if($transactionResult["success"])
            {
                
                $result["output"] = "Vielen Dank für Ihre Bezahlung.";
                
            }
            else
            {
                
                $result["output"] = $transactionResult["message"];
                $result["exception"] = @$transactionResult["exception"];
                
            }
            
        }
        
    }
    catch(Exception $e)
    {
        
        throw new PerisianException($e->getMessage());
        
    }
    
    require_once '../includes/footer.inc.php';
       
}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
    exit;
    
}
