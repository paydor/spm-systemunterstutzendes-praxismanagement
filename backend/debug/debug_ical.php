<?php

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/calendar/CalsyCalendarExport.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    require_once '../includes/menu.inc.php';
    
    set_time_limit(20);
    
    // Generate a calendar file.
    try
    {
        
        $export = new CalsyCalendarExport(1354);
        
        //$export->sendFileToBrowser();
        
        echo $export->getExportRaw();
        
        //echo $export->getExportFilePath();
        //$export->deleteExportFile();
        
    }
    catch(Exception $e)
    {
        
        throw new PerisianException($e->getMessage());
        
    }
       
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
    exit;
    
}
