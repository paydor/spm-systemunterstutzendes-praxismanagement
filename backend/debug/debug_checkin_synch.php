<?php

try
{
    
    // https://termine.triathlonkids.de/admin/debug/debug_checkin_synch/
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/checkin/CalsyCheckInLogSynch.class.php';
    
    if(!PerisianFrameworkToolbox::getConfig('basic/project/debug_mode'))
    {
        
        // The system is not in debug mode.
        throw new PerisianException(PerisianLanguageVariable::getVariable(10723));
        
    }
    
    error_reporting(E_ALL);
    
    CalsyCheckInLogSynch::synchToVmoso();
    
    $result = "";
    
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo "Fatal error: " . $e->getMessage();
    
    echo"<br>\nDebug info: ";
    
    print_r($e->getDebugInfo());
    
    exit;
    
}