<?php

exit;

try
{
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    try
    {
        
        $userFrontendOrderObject = new CalsyCalendarEntryUserFrontendOrder();
        
        $result = $userFrontendOrderObject->deleteUserFrontendEntriesWithoutCalendarEntry();
        
        echo $result;

    }
    catch(Exception $e)
    {
        
        throw new PerisianException($e->getMessage());
        
    }
       
    PerisianFrameworkToolbox::blankPage();
    
}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
    exit;
    
}
