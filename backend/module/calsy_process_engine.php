<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'modules/controller/CalsyProcessEngineModuleController.class.php';
            
    $contentController = new CalsyProcessEngineModuleController();    
    PerisianControllerWeb::handleContent($contentController);
            
    if(isset($result['object_module']))
    {    
        
        $menu->setActiveMenuPointForModule($result['object_module']->getModuleIdentifier());
    
    }
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
