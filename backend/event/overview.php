<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/calendar/controller/CalsyCalendarController.class.php';
    
    {
        
        $isMenuPointOnUnconfirmed = PerisianFrameworkToolbox::getRequest('ul', 'get');
        $isMenuPointOnCanceled = PerisianFrameworkToolbox::getRequest('c', 'get');

        $menu->setActiveMenuPoint($isMenuPointOnCanceled ? 53 : ($isMenuPointOnUnconfirmed == 1 ? 42 : 35));
 
    }
        
    $contentController = new CalsyCalendarController();
        
    if(strlen(PerisianFrameworkToolbox::getRequest('do')) == 0)
    {
        
        $contentController->setAction('overview');
        
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}