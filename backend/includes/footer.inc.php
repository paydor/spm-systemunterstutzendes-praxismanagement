<?php

if(!$dontShowHeader)
{
        
    $customHeaderImage = PerisianSystemSetting::getImageUploadUrl('backend_header_image');
    $customHeaderImage = strlen($customHeaderImage) > 0 ? $customHeaderImage : "assets/img/custom/logo_header_long.png";
        
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . "templates/{$style}/includes/header.phtml";
    
}

if(isset($page) && isset($style))
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . "templates/{$style}/{$page}.phtml";
    
}

if(!$dontShowFooter)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . "templates/{$style}/includes/footer.phtml";
    
}

exit;