<?php

/**
 * This is the main header file of the page.
 * 
 * It includes various classes, opens a connection to the database,
 * loads system settings and handles sessions.
 * 
 * @author Peter Hamm
 * 
 * @date 2016-10-17 v2.0
 * @date 2010-10-21 v1.0
 */
    
error_reporting(E_ALL);
ini_set('display_errors', '1');
    
ini_set('memory_limit', '-1');
gc_enable();
    
require realpath(dirname(__FILE__)) . '/../../library/framework/PerisianSystemConfiguration.class.php';

PerisianSystemConfiguration::initSystemConfiguration();

require realpath(dirname(__FILE__)) . '/../../library/framework/PerisianFrameworkToolbox.class.php';

PerisianFrameworkToolbox::handleServerSetup();
PerisianFrameworkToolbox::updateIncludePath();
PerisianFrameworkToolbox::escapeRequests();

require_once 'framework/PerisianTimeZone.class.php';

date_default_timezone_set(PerisianTimeZone::getTimeZoneName());

require_once 'framework/exceptions/PerisianException.class.php';
require_once 'framework/PerisianValidation.class.php';
require_once 'framework/database/PerisianMysql.class.php';
require_once 'framework/PerisianLanguageVariable.class.php';
require_once 'calsy/user_backend/CalsyUserBackend.class.php';
require_once 'framework/PerisianSession.class.php';
require_once 'framework/settings/PerisianSystemSetting.class.php';
require_once 'framework/PerisianMenu.class.php';
require_once 'framework/controller/PerisianControllerWeb.class.php';

try
{

    PerisianSystemConfiguration::updateRunningConfiguration();

}
catch(Exception $e)
{

    print_r($e);

    exit;

}

if(isset($isCommandLine) && !$isCommandLine)
{
    
    PerisianFrameworkToolbox::forceSSL($isFrontend);
    
}
    
if(false)//PerisianSystemConfiguration::getServerType() == PerisianSystemConfiguration::SERVER_TYPE_DOCKER)
{
    
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    echo"calSy @ openShift<br>\n";
    
    try
    {
        
        //$link = mysqli_connect("viproso.ch:19336", "spm", "ll9A1PLKFDC2", "spm");
        $link = mysqli_connect(getenv('CALSY_DB_HOST'), getenv('CALSY_DB_USER'), getenv('CALSY_DB_PASSWORD'), getenv('CALSY_DB_DB'));

        if(!$link) 
        {
            
            echo "Fehler: konnte nicht mit MySQL verbinden." . PHP_EOL;
            echo "Debug-Fehlernummer: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debug-Fehlermeldung: " . mysqli_connect_error() . PHP_EOL;
            
            echo "Server:<br>\n";
            
            print_r($_SERVER);
            
            exit;
            
        }

        echo "MySQL verbunden!" . PHP_EOL;
        echo "Host-Informationen: " . mysqli_get_host_info($link) . PHP_EOL;
        
        echo "<br>\n" . lg("10000");

        mysqli_close($link);
        
    }
    catch(Exception $e)
    {
        
        echo"<br>\nDB connection failed: ";
        
        print_r($e);
        
    }
    
    exit;
    
}

// Login, language and charset handling (admin area)
if(PerisianFrameworkToolbox::getConfig("environment/admin_area"))
{
    
    try
    {

        $user = CalsyUserBackend::handleLogin(PerisianFrameworkToolbox::getRequest('do'));
                             
    }
    catch(Exception $e)
    {

        // Login was not successful
        $user = new CalsyUserBackend();
        
    }
    
    // The user class is dynamic and can be extended
    $userClass = get_class($user);

    header(PerisianFrameworkToolbox::getConfig('basic/project/charset')); 

    PerisianSystemConfiguration::updateServerFallbackData();
        
    if(!@$isFrontend)
    {

        PerisianSystemConfiguration::handleDatabaseMigration();

    }
    
    if(!$user->isLoggedIn() && !$disableLoginScreen)
    {
        
        // Force the login
        
        require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . '/login.php';
        
        exit;
        
    }

}
