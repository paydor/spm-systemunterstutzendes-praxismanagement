<?php

try
{
	
    require_once 'includes/header.inc.php';
    require_once 'includes/menu.inc.php';
    
    require_once 'calsy/calendar/controller/CalsyCalendarController.class.php';

    if(!is_null($menu))
    {
        
        $menu->setActiveMenuPoint(4);
        
    }
    
    $contentController = new CalsyCalendarController();
    $contentController->setAction('overviewCalendarGlobal');
    PerisianControllerWeb::handleContent($contentController);
    
    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
        
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}