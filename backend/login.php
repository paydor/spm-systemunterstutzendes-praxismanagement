<?php

try
{
        
    require_once 'includes/header.inc.php';
    require_once 'framework/PerisianLanguage.class.php';

    if(!isset($page))
    {
        
        $page = 'registration/login';
        
    }

    $redirectAfterLogin = PerisianFrameworkToolbox::getServerVar('REQUEST_URI');
    $redirectAfterLogin = substr($redirectAfterLogin, 0, strpos($redirectAfterLogin, '?'));
    
    $dontShowHeaderHtml = true;
    $dontShowFooterHtml = true;

    if(strpos($redirectAfterLogin, 'error/') === true)
    {
        
        require('error.php');
        
        exit;
        
    }
    
    $languageList = PerisianLanguage::getLanguages();
    
    $isFailedLoginAttempt = @strlen(PerisianFrameworkToolbox::getRequest('login_password')) > 0;
    
    $customLogoUrl = PerisianSystemSetting::getImageUploadUrl('backend_welcome_image');
    $customLogoUrl = strlen($customLogoUrl) > 0 ? $customLogoUrl : "assets/img/custom/logo_login.png";
    
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
    {
        
        $dontShowHeader = true;
        $dontShowFooter = true;
        
        // This is an ajax call. Return a special page.
        $page = 'registration/login_hint_ajax';
        
    }

    require 'includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
