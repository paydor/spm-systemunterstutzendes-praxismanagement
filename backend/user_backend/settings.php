<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/user_backend/controller/CalsyUserBackendController.class.php';
        
    $contentController = new CalsyUserBackendController();
    
    if(strlen(PerisianFrameworkToolbox::getRequest('do')) == 0)
    {
        
        $contentController->setAction('settings');
                
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    $menu->setActiveMenuPoint((empty($result['id']) || !$user->isAdmin()) ? 24 : 5);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
