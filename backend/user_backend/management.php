<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/user_backend/controller/CalsyUserBackendController.class.php';

    $request = PerisianFrameworkToolbox::getRequest('do');
    
    if(!in_array($request, Array('editOwnPassword', 'saveOwnPassword')))
    {
        
        $menu->setActiveMenuPoint(5);
        
    }
        
    $contentController = new CalsyUserBackendController();    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
