<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'framework/controller/PerisianLanguageVariableController.class.php';
        
    $contentController = new PerisianLanguageVariableController();
    
    $do = PerisianFrameworkToolbox::getRequest('do');
    
    if(!isset($do) || strlen($do) == 0)
    {
        
        $contentController->setAction('showMissing');
                
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    $contentController->checkAuthorization();
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}