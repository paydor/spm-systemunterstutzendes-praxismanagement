<?php

try
{

    $disableLoginScreen = true;
    
    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';

    require_once 'framework/PerisianLanguage.class.php';
    
    PerisianFrameworkToolbox::blankPage();

    $requestedVariableIdentifiers = PerisianFrameworkToolbox::getRequest('v', 'post');
    
    $languageVariables = array();
    
    $languageCodes = array();
        
    foreach($requestedVariableIdentifiers as $variableIdentifier)
    {
        
        if(!PerisianLanguageVariable::variableExists($variableIdentifier))
        {
            
            continue;
            
        }
                
        $entry = PerisianLanguageVariable::getVariable($variableIdentifier, true);
        
        $formattedEntry = array();
                
        foreach($entry as $variableLanguageId => $value)
        {
                        
            if(!isset($languageCodes[$variableLanguageId]))
            {
                
                $languageCodes[$variableLanguageId] = PerisianLanguage::getLanguageCode($variableLanguageId);
                
            }
            
            $languageCode = $languageCodes[$variableLanguageId];
            
            $formattedEntry[$languageCode] = $value;
                        
        }
        
        $languageVariables[$variableIdentifier] = $formattedEntry;
        
    }

    $result = array(
        
        'variables' => $languageVariables
        
    );
    
    echo json_encode($result);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
