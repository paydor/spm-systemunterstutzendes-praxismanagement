<?php

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';

    require_once 'framework/PerisianLanguage.class.php';

    $languageId = PerisianFrameworkToolbox::getRequest('languageId', 'get');
    $variableId = PerisianFrameworkToolbox::getRequest('variableId', 'get');

    PerisianLanguageVariable::setLanguage($languageId);
    PerisianLanguageVariable::printVariable($variableId);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
