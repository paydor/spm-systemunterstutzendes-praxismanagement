<?php

// Loads the favorite icon depending on the specified area (backend / frontend)

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';

    $backendArea = PerisianFrameworkToolbox::getRequest('b', 'get');
    $requestedFile = PerisianSystemSetting::getSettingValue($backendArea == 1 ? "backend_favorite_icon" : "frontend_favorite_icon");
        
    if(strlen($requestedFile) > 0)
    {
        
        try
        {
            
            $fileInfo = PerisianUploadFileManager::getFileInfo('image', $requestedFile);
            $fileContents = PerisianUploadFileManager::getFile('image', $requestedFile);
            
        }
        catch(Exception $e)
        {
            
            // Do nothing, just go to the fallback
            
        }
        
    }
    
    if(!isset($fileContents))
    {
        
        $fileInfo = array(

            "type" => "image/png"

        );
                
        $defaultIconFilePath = PerisianFrameworkToolbox::getConfig("basic/project/folder") . 'templates/' . $style . '/asset-data/img/favicon_default.png';
        
        $fileContents = file_get_contents($defaultIconFilePath);
        
    }

    $headerString = "Content-type: " . $fileInfo["type"] . ";";
    header($headerString);

    echo $fileContents;

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
