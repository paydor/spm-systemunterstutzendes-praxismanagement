<?php

// Tunnel for uploaded files

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    
    require_once 'framework/PerisianUploadFileManager.class.php';
    
    $requestedFile = PerisianFrameworkToolbox::getRequest('f', 'get');
    $requestedType = PerisianFrameworkToolbox::getRequest('t', 'get');
    
    try
    {
        
        $fileInfo = PerisianUploadFileManager::getFileInfo($requestedType, $requestedFile);
        $fileContents = PerisianUploadFileManager::getFile($requestedType, $requestedFile);

        $headerString = "Content-type: " . $fileInfo["type"] . ";";
        header($headerString);
        
        echo $fileContents;

    }
    catch(Exception $e)
    {
        
        echo $e->getMessage();
        
    }
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}

