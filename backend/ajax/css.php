<?php

// Compresses and caches CSS files if the related system setting is set on.

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'framework/packer/PerisianPackerCSS.class.php';

    $requestedFile = PerisianFrameworkToolbox::getRequest('f', 'get');
    
    header("Content-type: text/css; charset: UTF-8");
    
    $cssString = PerisianPackerCSS::getFile($requestedFile);
    
    $systemColors = PerisianSystemConfiguration::getSystemColors();
    
    $additionalCss = array();
        
    if($requestedFile == 'global')
    {
        
        array_push($additionalCss, ".btn-primary,.btn-primary:hover,.btn-primary-bright,.btn-primary-bright:hover,.btn-primary:active,.btn-primary.active,.btn-primary:hover,.btn-primary:focus,.open .dropdown-toggle.btn-primary{background-color:" . $systemColors["main"] . " !important;border-color:" . $systemColors["main"] . " !important;color:" . $systemColors["secondary"] . " !important;}");
        array_push($additionalCss, ".card-head.style-primary{background-color:" . $systemColors["main_alternative_two"] . ";border-color:" . $systemColors["main_alternative_two"] . ";color:" . $systemColors["secondary"] . ";}");
        array_push($additionalCss, ".dropzone{background-color:" . $systemColors["main_alternative"] . " !important;}");
        array_push($additionalCss, ".form-group .form-control ~ label:after,.form-group .form-control ~ .form-control-line:after{background-color:" . $systemColors["main_alternative_two"] . " !important;}");
        array_push($additionalCss, ".datepicker-days .active.day, .datepicker-months .month.active, .datepicker-years .year.active{background-color:" . $systemColors["main_alternative_two"] . " !important;}");
        array_push($additionalCss, ".datepicker table tr td.today, .datepicker table tr td.today:hover, .datepicker table tr td.today:hover:hover, .datepicker table tr td.today.disabled:hover, .datepicker table tr td.today.disabled:hover:hover, .datepicker table tr td.today:focus, .datepicker table tr td.today:hover:focus, .datepicker table tr td.today.disabled:focus, .datepicker table tr td.today.disabled:hover:focus, .open .dropdown-toggle.datepicker table tr td.today, .open .dropdown-toggle.datepicker table tr td.today:hover, .open .dropdown-toggle.datepicker table tr td.today.disabled, .open .dropdown-toggle.datepicker table tr td.today.disabled:hover{background-color:" . $systemColors["main_alternative"] . " !important;}");
        array_push($additionalCss, ".form-wizard.form-wizard-horizontal .nav li.active .step, .form-wizard.form-wizard-horizontal .nav li:hover .step, .form-wizard.form-wizard-horizontal .nav li.done .step{border-color:" . $systemColors["main"] . " !important;}");
        array_push($additionalCss, ".form-wizard.form-wizard-horizontal .nav li.done .step, .progress-bar-primary{background-color:" . $systemColors["main"] . " !important;}");
        array_push($additionalCss, "a.bg-primary,a.bg-primary:hover{background-color:" . $systemColors["main_alternative_two"] . " !important;}");
        array_push($additionalCss, ".form-control:focus{border-bottom-color:" . $systemColors["main_alternative_two"] . " !important;}");
        array_push($additionalCss, ".btn-primary-bright:active, .btn-primary-bright.active{background-color:" . $systemColors["main"] . " !important};");
                
    }
    else if($requestedFile == 'calsy/vmoso/vmoso')
    {
                
        array_push($additionalCss, ".chat-log-entry-active {background-color:" . $systemColors["main_alternative"] . " !important};");
        
    }
    
    if(count($additionalCss) > 0)
    {
        
        $cssString .= "\n" . implode("\n", $additionalCss);
        
    }
    
    echo $cssString;

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}