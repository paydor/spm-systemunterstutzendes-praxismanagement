<?php

// Compresses and caches JavaScript files if the related system setting is set on.

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'framework/packer/PerisianPackerJavaScript.class.php';

    $requestedFile = PerisianFrameworkToolbox::getRequest('f', 'get');
    
    header("Content-type: text/javascript; charset: UTF-8");
    
    echo PerisianPackerJavaScript::getFile($requestedFile);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    echo $e->getMessage();
    
}
