<?php

// Handles uploaded data.

try
{

    $dontShowHeader = true;
    $dontShowFooter = true;
    $showMenu = false;
    $disableLoginScreen = true;

    require_once '../includes/header.inc.php';
    require_once 'modules/PerisianDataImportModule.class.php';
    require_once 'framework/import/PerisianDataImport.class.php';
    
    $requestedImportType = PerisianFrameworkToolbox::getRequest('t');
    $requestedImportVersion = PerisianFrameworkToolbox::getRequest('v');
    $uploadedFile = PerisianFrameworkToolbox::getRequest("f");
    $do = PerisianFrameworkToolbox::getRequest('do');
    
    if(!PerisianDataImportModule::isEnabled())
    {
        
        // The import module is currently not activated.
        throw new PerisianException(PerisianLanguageVariable::getVariable('p57656ec89cf96'));
        
    }

    $generalImportObject = new PerisianDataImport();
    
    $result = array(
        
        'success' => true,
        'message' => PerisianLanguageVariable::getVariable('p57656e6a5df71')
        
    );
    
    if($do == "upload")
    {

        $result = $generalImportObject->handleUploadedFile($_FILES);
        
        echo json_encode($result);

        exit;

    }
    else if($do == "import")
    {

        $requestedImportData = PerisianDataImport::getImportObjectByTypeAndVersion($requestedImportType, $requestedImportVersion);
        
        if(strlen($requestedImportType) == 0 || $requestedImportData == null)
        {

            // This specified import does not exist.
            throw new PerisianException(PerisianLanguageVariable::getVariable('p5765754dbf3bf'));

        }    

        if($requestedImportData['enabled'] != 1)
        {

            // This specified import is not enabled.
            throw new PerisianException(PerisianLanguageVariable::getVariable('p576575c7bd67d'));

        }
        
        $specificImportObject = new $requestedImportData['class']();

        if(!$specificImportObject->isImportAllowed())
        {
            
            // The necessary requirements to allow the import have not been met.
            throw new PerisianException(PerisianLanguageVariable::getVariable('10130'));

        }

        {
                        
            $result = $specificImportObject->import($uploadedFile);

            $generalImportObject->deleteUploadedFile($uploadedFile);
            
        }
        
    }

    echo json_encode($result);

    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    $generalImportObject->deleteUploadedFile($uploadedFile);
    
    $result = array(
        
        'success' => false,
        'message' => $e->getMessage()
        
    );
    
    echo json_encode($result);
    
}
