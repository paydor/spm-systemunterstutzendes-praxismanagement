<?php

$disableLoginScreen = true;

require_once 'includes/header.inc.php';

$page = 'error';
            
try
{
    
    $dontShowHeaderHtml = true;
    $dontShowMenu = true;
    
    if(isset($e) && is_object($e))
    {
        
        if(is_a($e, 'PerisianDatabaseMigrationException'))
        {
            
            // This exception is related to the database migration.
            
            require 'database_migration.php';
            
            exit;
            
        }
        
        // This is a normal error page.
        {
            
            $error = array();

            $error['type'] = '';
            $error['title'] = PerisianLanguageVariable::getVariable(10113);
            $error['message'] = $e->getMessage();
            $error['code'] = $e->getErrorCode();

            if(PerisianFrameworkToolbox::getConfig('basic/project/debug_mode') && $error['code'] != PerisianException::CODE_ERROR_MIGRATION)
            {

                $error['details'] = PerisianLanguageVariable::getVariable(10115) . '<br/>' . $e->getPosition();
                $error['details'] .= '<br/><br/>' . PerisianLanguageVariable::getVariable(10116) . '<br/>' . nl2br($e->getStack());

                if($e->getDebugInfo() != '')
                {
                    
                    $debugInfo = $e->getDebugInfo();
                    
                    if(is_array($debugInfo))
                    {
                        
                        $debugInfo = print_r($debugInfo, true);
                        
                    }

                    $error['details'] = PerisianLanguageVariable::getVariable(10131) . $debugInfo . '<br/><br/>' . @$error['details'];
                    
                }
                
                $error['backtrace'] = $e->getBackTrace();

            }
            
        }

    }
    else
    {
        
        // This is a server-level error

        $error['type'] = PerisianFrameworkToolbox::getRequest('e');

        switch($error['type'])
        {

            case '400':

                $error['type'] = PerisianLanguageVariable::getVariable(10148);

                $showMenu = false;

                break;

            case '401':

                $showMenu = false;

            case '403':

                $error['type'] = PerisianLanguageVariable::getVariable(10149);

                $showMenu = false;

                break;

            case '404':

                $error['type'] = PerisianLanguageVariable::getVariable(10147);

                $showMenu = false;

                break;

            case '404':

                $error['type'] = PerisianLanguageVariable::getVariable(10150);

                $showMenu = false;

                break;

        }

        $error['title'] = PerisianLanguageVariable::getVariable(10023);
        $error['message'] = PerisianLanguageVariable::getVariable(10024) . ' ' . $error['type'];
        $error['details'] = nl2br(str_replace(' ', '&nbsp;', print_r($_SERVER, true)));

    }
    
}
catch(Exception $additionalException)
{
    
    // There must be something seriously wrong
    
    echo "[System error] " . $e->getMessage();
    echo "\n\r[Trace] " . print_r($e->getBackTrace());
    
    exit;
    
}

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower(@$_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
    
    // This was an AJAX call, simply return JSON
    
    // Modify the response a little
    {
   
        $error['success'] = false;
        $error['error'] = true;
        
        if(!$debugModeEnabled)
        {
            
            unset($error['details']);
            unset($error['backtrace']);
            
        }
    
    }
    
    unset($page);
    
    $dontShowHeader = true;
    $dontShowHeaderHtml = true;
    $dontShowFooter = true;
    $dontShowFooterHtml = true;
    
    echo json_encode($error);
        
}

require_once 'includes/footer.inc.php';