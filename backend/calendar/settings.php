<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/calendar/controller/CalsyCalendarController.class.php';
        
    $contentController = new CalsyCalendarController();
    
    $do = PerisianFrameworkToolbox::getRequest('do');
    
    if(!isset($do) || strlen($do) == 0)
    {
        
        $contentController->setAction('settings');
                
    }
    
    PerisianControllerWeb::handleContent($contentController);
    
    $menu->setActiveMenuPoint(46);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
