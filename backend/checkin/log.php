<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    require_once 'calsy/checkin/controller/CalsyCheckInLogController.class.php';

    $menu->setActiveMenuPoint(69);
        
    $contentController = new CalsyCheckInLogController();    
    PerisianControllerWeb::handleContent($contentController);
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
