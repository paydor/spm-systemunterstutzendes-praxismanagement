<?php

try
{

    require_once '../includes/header.inc.php';
    require_once '../includes/menu.inc.php';
    
    $menu->setActiveMenuPoint(56);
    $do = PerisianFrameworkToolbox::getRequest('do');
    
    $entriesPerPage = 25;
    $search = PerisianFrameworkToolbox::getRequest('search', 'get');
    
    if(empty($do))
    {

        $page = 'calsy/process_engine/log/overview';
        
        $offset = 0;
        $limit = $entriesPerPage;
                
        $entryList = CalsyProcessEngineLog::getLog($search, 'DESC', $offset, $limit);
        $entryCount = CalsyProcessEngineLog::getLogCount($search);
                        
    }
    else if($do == 'list')
    {
        
        PerisianFrameworkToolbox::blankPage();
        
        $page = 'calsy/process_engine/log/list';
        
        $offset = PerisianFrameworkToolbox::getRequest('offset', 'get');
        $limit = PerisianFrameworkToolbox::getRequest('limit', 'get');
        $sortOrder = PerisianFrameworkToolbox::getRequest('order', 'get');
        $sortBy = PerisianFrameworkToolbox::getRequest('sorting', 'get');

        $entryList = CalsyProcessEngineLog::getLog($search, $sortOrder, $offset, $limit);
        $entryCount = CalsyProcessEngineLog::getLogCount($search);

    }
    else if($do == 'emptyLog')
    {
        
        PerisianFrameworkToolbox::blankPage();
        
        CalsyProcessEngineLog::emptyLog();

        $result = array('success' => true);
 
        echo json_encode($result);
        
        exit;
        
    }
    
    require '../includes/footer.inc.php';

}
catch(PerisianException $e)
{
    
    require PerisianFrameworkToolbox::getConfig('basic/project/folder') . PerisianFrameworkToolbox::getConfig('basic/project/backend_folder') . 'error.php';
    
}
