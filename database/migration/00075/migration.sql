/* = */

ALTER TABLE `spm`.`calsy_user_frontend` 
ADD COLUMN `calsy_user_frontend_image_profile` TEXT NULL AFTER `calsy_user_frontend_address_country`;

ALTER TABLE `spm`.`calsy_user_frontend` 
CHANGE COLUMN `calsy_user_frontend_image_profile` `calsy_user_frontend_image_profile` VARCHAR(255) NULL DEFAULT NULL ;
