/* = */

ALTER TABLE `spm`.`calsy_area` 
ADD COLUMN `calsy_area_gender` ENUM('none', 'male', 'female') NOT NULL DEFAULT 'none' AFTER `calsy_area_bookable_times`;

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p596a53926c54d', 'For a specific gender:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p596a53926c54d', 'For a specific gender:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p596a53926c54d', 'For a specific gender:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p596a53926c54d', 'Für spezifisches Geschlecht:');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p596a5b13b4c08', 'Show only the areas that match the selected customer\'s gender.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p596a5b13b4c08', 'Show only the areas that match the selected customer\'s gender.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p596a5b13b4c08', 'Show only the areas that match the selected customer\'s gender.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p596a5b13b4c08', 'Nur Leistungen anzeigen, die für das Geschlecht des Kunden zutreffen.');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('calendar_on_select_account_filter_gender', ' p596a5b13b4c08', 'system-admin', 'checkbox', '1000');
