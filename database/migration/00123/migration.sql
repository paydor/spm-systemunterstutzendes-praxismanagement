/* 2020-09-21 - v123 - Table reservation module & to do list basics */

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5f6912e86d187', 'Hidden setting');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5f6912e86d187', 'Hidden setting');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5f6912e86d187', 'Hidden setting');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5f6912e86d187', 'Versteckte Einstellung');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5f69064970ff0', 'Table reservation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5f69064970ff0', 'Table reservation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5f69064970ff0', 'Table reservation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5f69064970ff0', 'Tischreservierung');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5f68fc00548f9', 'To-do list');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5f68fc00548f9', 'To-do list');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5f68fc00548f9', 'To-do list');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5f68fc00548f9', 'To-Do-Liste');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`) VALUES ('module_icon_calsy_todo', '11200', '11199', 'system-admin', 'icon');
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('is_module_enabled_calsy_todo', '10869', '10870', 'system-admin');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`) VALUES ('module_icon_calsy_table_reservation', '11200', '11199', 'system-admin', 'icon');
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('is_module_enabled_calsy_table_reservation', '10869', '10870', 'system-admin');

INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES ('1', 'p5f69064970ff0', 'is_module_enabled_calsy_table_reservation', '/table_reservation/overview/', '158', '_module_backend');
INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES ('36', 'p5f69064970ff0', 'is_module_enabled_calsy_table_reservation', '/table_reservation/overview/', '181', '_module_backend');

INSERT INTO `setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('300', 'module_setting_calsy_user_frontend_registration_and_calendar_is_appointment_assistant_enabled', '10998', 'p5f6912e86d187', 'system-admin');
INSERT INTO `system_setting` (`system_setting_setting_id`, `system_setting_value`) VALUES ('300', '1');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('module_setting_calsy_table_reservation_frontend_enabled', 'p5f6ce9b516541', 'p5f6ce9d1e03a5', 'system-admin');
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_table_reservation_frontend_quota', 'p5f6cea0d06447', 'p5f6cea35bbbee', 'system-admin', 'dropdown', 'result[\'booking_quota\']', 2);

CREATE TABLE `calsy_table_reservation_location` (
	`calsy_table_reservation_location_id` INT NOT NULL AUTO_INCREMENT,
	`calsy_table_reservation_location_title` VARCHAR(255) NOT NULL,
	`calsy_table_reservation_location_description` TEXT NULL DEFAULT '',
	`calsy_table_reservation_location_address` TEXT NULL DEFAULT '',
	`calsy_table_reservation_location_image` TEXT NULL DEFAULT '',
	PRIMARY KEY (`calsy_table_reservation_location_id`)
)