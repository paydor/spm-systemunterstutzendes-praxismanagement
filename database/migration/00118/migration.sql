/* = */

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e17b4cc23775', 'Tile view for appointment proposals activated');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e17b4cc23775', 'Tile view for appointment proposals activated');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e17b4cc23775', 'Tile view for appointment proposals activated');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e17b4cc23775', 'Kachel-Ansicht für Terminvorschläge aktiviert');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_type`) VALUES ('module_setting_calsy_user_frontend_registration_and_calendar_is_enabled_tile_view_propsals', 'p5e17b4cc23775', 'system-admin');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e17e80727afb', 'Find appointment');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e17e80727afb', 'Find appointment');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e17e80727afb', 'Find appointment');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e17e80727afb', 'Termin finden');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e17d13ca559f', 'Current appointments');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e17d13ca559f', 'Current appointments');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e17d13ca559f', 'Current appointments');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e17d13ca559f', 'Aktuelle Termine');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e17d1184806a', 'Sorry, there are currently no appointments available.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e17d1184806a', 'Sorry, there are currently no appointments available.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e17d1184806a', 'Sorry, there are currently no appointments available.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e17d1184806a', 'Im Moment gibt es leider keine Termine.');
