/* = */
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efb4ea4985b6', 'Log time');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efb4ea4985b6', 'Log time');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efb4ea4985b6', 'Log time');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efb4ea4985b6', 'Log-Zeitpunkt');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efb4a809fb89', 'Checkin log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efb4a809fb89', 'Checkin log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efb4a809fb89', 'Checkin log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efb4a809fb89', 'Check-In Log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efb490e82a26', 'Log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efb490e82a26', 'Log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efb490e82a26', 'Log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efb490e82a26', 'Log');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee28af7473a', 'To the quick checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee28af7473a', 'To the quick checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee28af7473a', 'To the quick checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee28af7473a', 'Zum Quick-Check-In');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee0b3d72c23', 'Check out now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee0b3d72c23', 'Check out now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee0b3d72c23', 'Check out now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee0b3d72c23', 'Jetzt auschecken');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee0b20c9428', 'Check in now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee0b20c9428', 'Check in now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee0b20c9428', 'Check in now');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee0b20c9428', 'Jetzt einchecken');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee0a1d87a03', 'Checked in');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee0a1d87a03', 'Checked in');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee0a1d87a03', 'Checked in');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee0a1d87a03', 'Eingecheckt');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee07342ae93', 'Checked out');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee07342ae93', 'Checked out');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee07342ae93', 'Checked out');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee07342ae93', 'Ausgecheckt');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee06b87dfdd', 'My current status: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee06b87dfdd', 'My current status: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee06b87dfdd', 'My current status: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee06b87dfdd', 'Mein aktueller Status: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee0609d86e7', 'My checkin overview');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee0609d86e7', 'My checkin overview');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee0609d86e7', 'My checkin overview');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee0609d86e7', 'Meine Check-In-Übersicht');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee053848403', 'Reload status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee053848403', 'Reload status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee053848403', 'Reload status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee053848403', 'Status neu laden');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee04fd3375d', 'Do you really want to log out of the system?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee04fd3375d', 'Do you really want to log out of the system?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee04fd3375d', 'Do you really want to log out of the system?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee04fd3375d', 'Willst Du dich wirklich vom System abmelden?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee005c18ec8', 'I understand!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee005c18ec8', 'I understand!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee005c18ec8', 'I understand!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee005c18ec8', 'Verstanden!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eee001bdc119', '&lt;b&gt;Please note:&lt;/b&gt; We have sent you an email that contains the instructions on how to &lt;b&gt;activate&lt;/b&gt; your account. This step is mandatory before you can proceed using this service.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eee001bdc119', '&lt;b&gt;Please note:&lt;/b&gt; We have sent you an email that contains the instructions on how to &lt;b&gt;activate&lt;/b&gt; your account. This step is mandatory before you can proceed using this service.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eee001bdc119', '&lt;b&gt;Please note:&lt;/b&gt; We have sent you an email that contains the instructions on how to &lt;b&gt;activate&lt;/b&gt; your account. This step is mandatory before you can proceed using this service.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eee001bdc119', '&lt;b&gt;Achtung:&lt;/b&gt; Wir haben dir eine E-Mail gesendet, über welche du dein Benutzerkonto zuerst noch &lt;b&gt;aktivieren&lt;/b&gt; musst, bevor du es verwenden kannst.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5eedffa8ab887', 'Your account was successfully created.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5eedffa8ab887', 'Your account was successfully created.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5eedffa8ab887', 'Your account was successfully created.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5eedffa8ab887', 'Dein Benutzerkonto wurde erfolgreich angelegt.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee9ac46cadc0', "'You\'re currently not logged in.\nThe checkin requires an account.");
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee9ac46cadc0', "You\'re currently not logged in.\nThe checkin requires an account.");
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee9ac46cadc0', "You\'re currently not logged in.\nThe checkin requires an account.");
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee9ac46cadc0', 'Du bist momentan nicht angemeldet.\nFür den Check-In benötigst du ein Benutzerkonto.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee9aa6ee4207', 'Verwende unseren Check-In, um die Nutzung des Schwimmbads für alle zu erleichtern.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee9aa6ee4207', 'Verwende unseren Check-In, um die Nutzung des Schwimmbads für alle zu erleichtern.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee9aa6ee4207', 'Verwende unseren Check-In, um die Nutzung des Schwimmbads für alle zu erleichtern.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee9aa6ee4207', 'Verwende unseren Check-In, um die Nutzung des Schwimmbads für alle zu erleichtern.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee90e3114b01', 'List of my checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee90e3114b01', 'List of my checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee90e3114b01', 'List of my checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee90e3114b01', 'Liste meiner Check-Ins');


INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee9033fcf8f5', 'Dates');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee9033fcf8f5', 'Dates');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee9033fcf8f5', 'Dates');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee9033fcf8f5', 'Daten');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8ed70312e1', 'Delete checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8ed70312e1', 'Delete checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8ed70312e1', 'Delete checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8ed70312e1', 'Check-In löschen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e8d261199', 'List of checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e8d261199', 'List of checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e8d261199', 'List of checkins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e8d261199', 'Liste der Check-Ins');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e7e89a615', 'Finished');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e7e89a615', 'Finished');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e7e89a615', 'Finished');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e7e89a615', 'Abgeschlossen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e7d286fc9', 'Open');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e7d286fc9', 'Open');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e7d286fc9', 'Open');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e7d286fc9', 'Offen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e77f7f1be', 'Status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e77f7f1be', 'Status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e77f7f1be', 'Status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e77f7f1be', 'Status');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e7636e3da', 'Label');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e7636e3da', 'Label');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e7636e3da', 'Label');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e7636e3da', 'Bezeichnung');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e73cb0fcd', 'Swimming pool');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e73cb0fcd', 'Swimming pool');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e73cb0fcd', 'Swimming pool');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e73cb0fcd', 'Schwimmbad');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e2ac3226c', 'Edit checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e2ac3226c', 'Edit checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e2ac3226c', 'Edit checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e2ac3226c', 'Check-In bearbeiten');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8e29ee694b', 'New checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8e29ee694b', 'New checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8e29ee694b', 'New checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8e29ee694b', 'Neuer Check-In');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8dec63ff49', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8dec63ff49', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8dec63ff49', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8dec63ff49', 'Auschecken');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8debbf305f', 'Checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8debbf305f', 'Checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8debbf305f', 'Checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8debbf305f', 'Einchecken');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8deaa787b5', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8deaa787b5', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8deaa787b5', 'Checkout');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8deaa787b5', 'Check-Out');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5ee8de444dbed', 'Check-In');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5ee8de444dbed', 'Check-In');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5ee8de444dbed', 'Checkin');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5ee8de444dbed', 'Check-In');



INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('is_module_enabled_calsy_checkin', '10869', '10870', 'system-admin');
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`) VALUES ('module_icon_calsy_checkin', '11200', '11199', 'system-admin', 'icon');

INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (1, 'p57a8e9ed626d3', 'is_module_enabled_calsy_checkin', NULL, '/checkin/overview/', 225, '_module_backend');
INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (1, 'p5efb4a809fb89', 'is_module_enabled_calsy_checkin', NULL, '/checkin/log/', 156, 'fa-history');

CREATE TABLE IF NOT EXISTS `calsy_checkin` (
  `calsy_checkin_id` int(11) NOT NULL AUTO_INCREMENT,
  `calsy_checkin_user_frontend_id` int(11) NOT NULL,
  `calsy_checkin_time_in` int(11) NOT NULL,
  `calsy_checkin_time_out` int(11) NOT NULL,
  `calsy_checkin_status` enum('open','closed') NOT NULL DEFAULT 'open',
  PRIMARY KEY (`calsy_checkin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE DATABASE IF NOT EXISTS `spm` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `spm`;

CREATE TABLE IF NOT EXISTS `calsy_checkin_log` (
  `calsy_checkin_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `calsy_checkin_log_user_frontend_id` int(11) NOT NULL,
  `calsy_checkin_log_checkin_id` int(11) NOT NULL,
  `calsy_checkin_log_time` int(11) NOT NULL,
  `calsy_checkin_log_status` enum('open','closed') NOT NULL,
  `calsy_checkin_log_description` text,
  `calsy_checkin_log_meta` text,
  PRIMARY KEY (`calsy_checkin_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
