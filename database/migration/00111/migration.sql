/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5af875cbd2d90', 'Display: Default color for process appointments in the calendar');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5af875cbd2d90', 'Display: Default color for process appointments in the calendar');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5af875cbd2d90', 'Display: Default color for process appointments in the calendar');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5af875cbd2d90', 'Darstellung: Standard-Farbe für Prozesse im Kalender');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5af875ae86058', 'Display: Use the color of the creator for process appointments');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5af875ae86058', 'Display: Use the color of the creator for process appointments');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5af875ae86058', 'Display: Use the color of the creator for process appointments');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5af875ae86058', 'Darstellung: Farbe des Erstellers für Prozess-Termine verwenden');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('process_engine_color_use_creator', 'p5af875ae86058', 'system-admin', 'checkbox', '400');
INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('process_engine_color_default', 'p5af875cbd2d90', 'system-admin', 'text', '500');
