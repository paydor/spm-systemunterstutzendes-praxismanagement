/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p596bce6340c9a', 'Propose only those users who are available at the time of the appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p596bce6340c9a', 'Propose only those users who are available at the time of the appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p596bce6340c9a', 'Propose only those users who are available at the time of the appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p596bce6340c9a', 'Nur Benutzer vorschlagen, die zur Zeit des Termins verfügbar sind');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p596bde41e46cb', 'Global, all users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p596bde41e46cb', 'Global, all users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p596bde41e46cb', 'Global, all users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p596bde41e46cb', 'Global, alle Benutzer');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p596be5c898779', 'Sort users by a custom key instead of their names');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p596be5c898779', 'Sort users by a custom key instead of their names');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p596be5c898779', 'Sort users by a custom key instead of their names');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p596be5c898779', 'Benutzer nach eigenen Schlüsseln sortieren anstatt nach Namen');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('calendar_on_select_user_filter_free', 'p596bce6340c9a', 'system-admin', 'checkbox', '1100');
INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('system_show_backend_users_sorted_by_key', 'p596be5c898779', 'system-admin', 'checkbox', '300');

ALTER TABLE `spm`.`user` 
ADD COLUMN `user_sorting_custom` VARCHAR(255) NULL AFTER `user_created`;
