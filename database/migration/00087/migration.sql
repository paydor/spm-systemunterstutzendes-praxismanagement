/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c15784440f1', 'All groups');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c15784440f1', 'All groups');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c15784440f1', 'All groups');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c15784440f1', 'Alle Gruppen');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c1588d7664b', 'Filter by group');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c1588d7664b', 'Filter by group');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c1588d7664b', 'Filter by group');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c1588d7664b', 'Nach Gruppe filtern');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c2fc4a05006', 'Data merging activated');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c2fc4a05006', 'Data merging activated');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c2fc4a05006', 'Data merging activated');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c2fc4a05006', 'Datensatz-Merging aktiviert');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('module_setting_calsy_user_frontend_merging_enabled', 'p59c2fc4a05006', 'system-admin', 'checkbox', '200');
UPDATE `spm`.`setting` SET `setting_order`='100' WHERE `setting_id`='138';

INSERT INTO `spm`.`system_setting` (`system_setting_setting_id`, `system_setting_value`) VALUES ('193', '1');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c4047f8c9b9', 'Pick two data sets from the list which you want to merge into one single data set.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c4047f8c9b9', 'Pick two data sets from the list which you want to merge into one single data set.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c4047f8c9b9', 'Pick two data sets from the list which you want to merge into one single data set.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c4047f8c9b9', 'Wählen Sie zwei Datensätze in der Liste, die Sie zu einem einzelnen Datensatz zusammenführen möchten.');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c4044767c46', 'Data merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c4044767c46', 'Data merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c4044767c46', 'Data merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c4044767c46', 'Datensatz Merging');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c405dee0252', 'Data set');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c405dee0252', 'Data set');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c405dee0252', 'Data set');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c405dee0252', 'Datensatz');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c4079f9942f', 'Merge');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c4079f9942f', 'Merge');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c4079f9942f', 'Merge');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c4079f9942f', 'Zusammenführen');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c407cd1691b', 'Validate');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c407cd1691b', 'Validate');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c407cd1691b', 'Validate');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c407cd1691b', 'Überprüfen');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c4090242879', 'Pick for merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c4090242879', 'Pick for merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c4090242879', 'Pick for merging');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c4090242879', 'Für Merging auswählen');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c50429707fc', 'Calendar entries');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c50429707fc', 'Calendar entries');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c50429707fc', 'Calendar entries');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c50429707fc', 'Kalender-Einträge');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c50e1c6a933', 'New version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c50e1c6a933', 'New version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c50e1c6a933', 'New version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c50e1c6a933', 'Neue Version');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c50dd55cd6c', 'Version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c50dd55cd6c', 'Version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c50dd55cd6c', 'Version');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c50dd55cd6c', 'Version');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c5196f52257', 'Take over from version %1');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c5196f52257', 'Take over from version %1');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c5196f52257', 'Take over from version %1');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c5196f52257', 'Von Version %1 übernehmen');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c524c7eb796', 'The merge was not successful, an error occured.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c524c7eb796', 'The merge was not successful, an error occured.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c524c7eb796', 'The merge was not successful, an error occured.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c524c7eb796', 'Die Zusammenführung warf einen Fehler auf.');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p59c524a39d6ba', 'Merge successful.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p59c524a39d6ba', 'Merge successful.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p59c524a39d6ba', 'Merge successful.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p59c524a39d6ba', 'Zusammenführung erfolgreich.');
