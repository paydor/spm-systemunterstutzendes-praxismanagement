/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5a072fe142f65', 'Display of days');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5a072fe142f65', 'Display of days');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5a072fe142f65', 'Display of days');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5a072fe142f65', 'Tagesanzeige');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`) VALUES ('220', 'calendar_displayed_days', 'p5a072fe142f65', 'system', 'text');

INSERT INTO `spm`.`system_setting` (`system_setting_id`, `system_setting_setting_id`, `system_setting_value`) VALUES (NULL, '220', '[{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"},{\"year\":\"1\",\"month\":\"1\",\"week\":\"1\",\"day\":\"1\"}]');
