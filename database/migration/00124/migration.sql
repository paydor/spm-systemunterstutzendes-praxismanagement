/* 2021-02-11 - Password changing for backend users */

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p60253bc4d6b48', 'Your password has been successfully updated.\nPlease login in again.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p60253bc4d6b48', 'Your password has been successfully updated.\nPlease login in again.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p60253bc4d6b48', 'Your password has been successfully updated.\nPlease login in again.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p60253bc4d6b48', 'Ihr Passwort wurde erfolgreich aktualisiert.\nBitte melden Sie sich erneut an.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p602539c8105f4', 'Please store your password safely.\nShould your password get lost, an administrator or developer can reset it.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p602539c8105f4', 'Please store your password safely.\nShould your password get lost, an administrator or developer can reset it.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p602539c8105f4', 'Please store your password safely.\nShould your password get lost, an administrator or developer can reset it.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p602539c8105f4', 'Bitte bewahren Sie Ihr Passwort sicher auf.\nSollte Ihr Passwort verloren gehen, kann ein Administrator oder Entwickler Ihr Passwort zurücksetzen.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p602536684a580', 'Change my password');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p602536684a580', 'Change my password');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p602536684a580', 'Change my password');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p602536684a580', 'Mein Passwort ändern');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p60279b575f3f1', 'SMTP server');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p60279b575f3f1', 'SMTP server');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p60279b575f3f1', 'SMTP server');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p60279b575f3f1', 'SMTP-Server');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p60279b45cd3a5', 'SendInBlue');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p60279b45cd3a5', 'SendInBlue');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p60279b45cd3a5', 'SendInBlue');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p60279b45cd3a5', 'SendInBlue');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p60279b1c56dce', 'Choose between several e-mail services.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p60279b1c56dce', 'Choose between several e-mail services.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p60279b1c56dce', 'Choose between several e-mail services.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p60279b1c56dce', 'Wählen Sie zwischen verschiedenen E-Mail-Diensten.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p60279afff2a69', 'Mail service');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p60279afff2a69', 'Mail service');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p60279afff2a69', 'Mail service');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p60279afff2a69', 'Mail-Dienst');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p6027a0837fcfc', 'The key to log in via the API.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p6027a0837fcfc', 'The key to log in via the API.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p6027a0837fcfc', 'The key to log in via the API.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p6027a0837fcfc', 'Der Schlüssel zur Anmeldung an der API.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p6027a070bde8e', 'SendInBlue API key');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p6027a070bde8e', 'SendInBlue API key');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p6027a070bde8e', 'SendInBlue API key');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p6027a070bde8e', 'SendInBlue API-Schlüssel');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('system_default_smtp_service', 'p60279afff2a69', 'p60279b1c56dce', 'system-admin', 'dropdown', 'result[\'list_mail_services\']', '0');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('system_default_smtp_service_sendinblue_api_key', 'p6027a070bde8e', 'p6027a0837fcfc', 'system-admin', 'password', '9');
