/* = */
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efe026933da0', 'beginning');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efe026933da0', 'beginning');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efe026933da0', 'beginning');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efe026933da0', 'Beginn an');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efe01b40535d', 'Status from %1 until %2:');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efe01b40535d', 'Status from %1 until %2:');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efe01b40535d', 'Status from %1 until %2:');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efe01b40535d', 'Status von %1 bis %2:');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdfc38df2be', 'At % h');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdfc38df2be', 'At % h');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdfc38df2be', 'At % h');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdfc38df2be', 'Um % Uhr');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdfae34c762', 'When will the synchronisation with Vmoso take place?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdfae34c762', 'When will the synchronisation with Vmoso take place?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdfae34c762', 'When will the synchronisation with Vmoso take place?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdfae34c762', 'Wann wird mit Vmoso synchronisiert?');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdfab6d39ad', 'Time of synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdfab6d39ad', 'Time of synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdfab6d39ad', 'Time of synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdfab6d39ad', 'Synchronisationszeitpunkt');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdd9daa150a', 'Can be copied from the address bar in vmoso, e.g. "349685_4".');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdd9daa150a', 'Can be copied from the address bar in vmoso, e.g. "349685_4".');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdd9daa150a', 'Can be copied from the address bar in vmoso, e.g. "349685_4".');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdd9daa150a', 'Kann aus der Adresszeile in Vmoso kopiert werden, z.B. "349685_4".');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdd9b7628e1', 'Vmoso chat ID for posts');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdd9b7628e1', 'Vmoso chat ID for posts');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdd9b7628e1', 'Vmoso chat ID for posts');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdd9b7628e1', 'Vmoso Chat-ID für Posts');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdd72de7b15', 'Defines whether the logs should be transferred to Vmoso.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdd72de7b15', 'Defines whether the logs should be transferred to Vmoso.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdd72de7b15', 'Defines whether the logs should be transferred to Vmoso.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdd72de7b15', 'Legt fest, ob die Logs an Vmoso übertragen werden sollen.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5efdd5ec87ac2', 'Activate Vmoso synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5efdd5ec87ac2', 'Activate Vmoso synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5efdd5ec87ac2', 'Activate Vmoso synchronisation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5efdd5ec87ac2', 'Vmoso-Synchronisation aktiviert');

CREATE TABLE IF NOT EXISTS `calsy_checkin_log_synch` (
  `calsy_checkin_log_synch_id` int(11) NOT NULL AUTO_INCREMENT,
  `calsy_checkin_log_synch_type` enum('vmoso') NOT NULL DEFAULT 'vmoso',
  `calsy_checkin_log_synch_time` int(11) NOT NULL,
  `calsy_checkin_log_synch_status` enum('success','failed') NOT NULL,
  `calsy_checkin_log_synch_meta` text NOT NULL,
  PRIMARY KEY (`calsy_checkin_log_synch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `calsy_checkin_log_synch` (`calsy_checkin_log_synch_id`, `calsy_checkin_log_synch_type`, `calsy_checkin_log_synch_time`, `calsy_checkin_log_synch_status`, `calsy_checkin_log_synch_meta`) VALUES (1, 'vmoso', 1593528570, 'success', '0');

CREATE TABLE IF NOT EXISTS `calsy_quick_log` (
  `calsy_quick_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `calsy_quick_log_user_frontend_id` int(11) DEFAULT NULL,
  `calsy_quick_log_type` enum('water_examination') NOT NULL DEFAULT 'water_examination',
  `calsy_quick_log_data` text NOT NULL,
  `calsy_quick_log_time` int(11) NOT NULL,
  PRIMARY KEY (`calsy_quick_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8085, 4, 'p5f0324ae3d6d4', 'Quick log');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8084, 3, 'p5f0324ae3d6d4', 'Quick log');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8083, 2, 'p5f0324ae3d6d4', 'Quick log');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8082, 1, 'p5f0324ae3d6d4', 'Quick-Log');

INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8093, 4, 'p5f0322b5df1a7', 'Water quality examiner');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8092, 3, 'p5f0322b5df1a7', 'Water quality examiner');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8091, 2, 'p5f0322b5df1a7', 'Water quality examiner');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8090, 4, 'p5f0322dff3a78', 'This person may use the quick log to post water examination data.	');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8089, 3, 'p5f0322dff3a78', 'This person may use the quick log to post water examination data.	');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8088, 2, 'p5f0322dff3a78', 'This person may use the quick log to post water examination data.	');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8087, 1, 'p5f0322dff3a78', 'Diese Person darf das Quick-Log verwenden, um aktuelle Wasserprüfdaten einzutragen.');
INSERT INTO `language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (8086, 1, 'p5f0322b5df1a7', 'Wasserprüfer');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`) VALUES ('module_icon_calsy_quick_log', '11200', '11199', 'system-admin', 'icon');

INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (1, 'p5f0324ae3d6d4', 'is_module_enabled_calsy_quick_log', NULL, '/quick_log/overview/', 157, '_module_backend');
INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (36, 'p5f0324ae3d6d4', 'is_module_enabled_calsy_quick_log', NULL, '/quick_log/overview/', 180, '_module_backend');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('is_module_enabled_calsy_quick_log', '10869', '10870', 'system-admin', 'checkbox', '', NULL);

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_enabled', 'p5efdd5ec87ac2', '', 'system-admin', 'checkbox', '', 100);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_chat_id', 'p5efdd9b7628e1', 'p5efdd9daa150a', 'system-admin', 'text', '', 450);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_cid', 'p58ca6c890a998', 'p58ca6ca1dd348', 'system-admin', 'text-encrypted', '', 200);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_password', 'p58ca6c53a979e', 'p58ca6c6c57497', 'system-admin', 'password', '', 400);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_server', 'p58ca6c034cc2a', 'p58ca6c1da21bf', 'system-admin', 'text-encrypted', '', 150);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_token_data', 'p58c83f05bf338', 'p58c83f349d464', 'system-admin', 'text-encrypted', '', NULL);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_quick_log_synch_vmoso_connection_user', 'p58ca6c2aa30c4', 'p58ca6c4362d2e', 'system-admin', 'text-encrypted', '', 300);

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_chat_id', 'p5efdd9b7628e1', 'p5efdd9daa150a', 'system-admin', 'text', '', 450);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_cid', 'p58ca6c890a998', 'p58ca6ca1dd348', 'system-admin', 'text-encrypted', '', 200);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_password', 'p58ca6c53a979e', 'p58ca6c6c57497', 'system-admin', 'password', '', 400);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_server', 'p58ca6c034cc2a', 'p58ca6c1da21bf', 'system-admin', 'text-encrypted', '', 150);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_time', 'p5efdfab6d39ad', 'p5efdfae34c762', 'system-admin', 'dropdown', 'result[\'synch_vmoso_times\']', 500);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_token_data', 'p58c83f05bf338', 'p58c83f349d464', 'system-admin', 'text-encrypted', '', NULL);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_connection_user', 'p58ca6c2aa30c4', 'p58ca6c4362d2e', 'system-admin', 'text-encrypted', '', 300);
INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_checkin_synch_vmoso_enabled', 'p5efdd5ec87ac2', '', 'system-admin', 'checkbox', '', 100);
