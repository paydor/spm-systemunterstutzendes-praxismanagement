/* = */
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e619e2b40ad1', 'Invalid name.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e619e2b40ad1', 'Invalid name.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e619e2b40ad1', 'Invalid name.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e619e2b40ad1', 'Ungültiger Name.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e619aabbe428', 'Edit again');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e619aabbe428', 'Edit again');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e619aabbe428', 'Edit again');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e619aabbe428', 'Noch einmal bearbeiten');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e619a9e6c331', 'Thank you very much, your master data has been updated.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e619a9e6c331', 'Thank you very much, your master data has been updated.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e619a9e6c331', 'Thank you very much, your master data has been updated.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e619a9e6c331', 'Vielen Dank, Ihre Stammdaten wurden aktualisiert.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e619a805892d', 'Your master data could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e619a805892d', 'Your master data could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e619a805892d', 'Your master data could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e619a805892d', 'Ihre Stammdaten konnten nicht gespeichert werden.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6196bdf0bb4', 'Further fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6196bdf0bb4', 'Further fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6196bdf0bb4', 'Further fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6196bdf0bb4', 'Weitere Felder');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6195d835329', 'Personal data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6195d835329', 'Personal data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6195d835329', 'Personal data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6195d835329', 'Persönliche Daten');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e618eb631602', 'Please fill out this form so we can have your up-to-date information in our system.\nThis helps us do our work a lot. Thank you very much!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e618eb631602', 'Please fill out this form so we can have your up-to-date information in our system.\nThis helps us do our work a lot. Thank you very much!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e618eb631602', 'Please fill out this form so we can have your up-to-date information in our system.\nThis helps us do our work a lot. Thank you very much!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e618eb631602', 'Bitte füllen Sie das Formular aus, damit wir Ihre aktuellen Stammdaten im System haben.\nDies erleichtert uns die Arbeit enorm. Vielen Dank!');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e618ce4174cd', 'The link you were following is invalid or expired.\nPlease contact the administrator if you have any questions.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e618ce4174cd', 'The link you were following is invalid or expired.\nPlease contact the administrator if you have any questions.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e618ce4174cd', 'The link you were following is invalid or expired.\nPlease contact the administrator if you have any questions.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e618ce4174cd', 'Der von Ihnen aufgerufene Link ist nicht (mehr) gültig.\nBei Rückfragen kontaktieren Sie bitte den Administrator.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e618c6b95f3e', 'Update master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e618c6b95f3e', 'Update master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e618c6b95f3e', 'Update master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e618c6b95f3e', 'Stammdaten aktualisieren');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e61887628d17', 'Open master data form');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e61887628d17', 'Open master data form');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e61887628d17', 'Open master data form');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e61887628d17', 'Stammdatenformular aufrufen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e618288a7011', 'The invitations have been successfully sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e618288a7011', 'The invitations have been successfully sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e618288a7011', 'The invitations have been successfully sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e618288a7011', 'Die Einladungen wurden erfolgreich versendet.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e617eff8831e', 'The invitations could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e617eff8831e', 'The invitations could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e617eff8831e', 'The invitations could not be sent.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e617eff8831e', 'Die Einladungen konnten nicht versendet werden.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e617daca8fc8', 'Send invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e617daca8fc8', 'Send invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e617daca8fc8', 'Send invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e617daca8fc8', 'Einladungen senden');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e61773087e16', 'Invitation active! Valid until: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e61773087e16', 'Invitation active! Valid until: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e61773087e16', 'Invitation active! Valid until: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e61773087e16', 'Einladung aktiv! Gültig bis: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e61744c605b2', 'Send an invitation to your customers, asking them to fill out their master data.\nAfter the expiry date, the generated links become invalid and the users can no longer access their master data form.\n\nShould you send a user with an active invitation a new invitation, the existing link will remain the same and the expiry date will be adjusted according to your input.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e61744c605b2', 'Send an invitation to your customers, asking them to fill out their master data.\nAfter the expiry date, the generated links become invalid and the users can no longer access their master data form.\n\nShould you send a user with an active invitation a new invitation, the existing link will remain the same and the expiry date will be adjusted according to your input.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e61744c605b2', 'Send an invitation to your customers, asking them to fill out their master data.\nAfter the expiry date, the generated links become invalid and the users can no longer access their master data form.\n\nShould you send a user with an active invitation a new invitation, the existing link will remain the same and the expiry date will be adjusted according to your input.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e61744c605b2', 'Senden Sie Einladungen an Ihre Kunden, damit diese Ihre Stammdatensätze ausfüllen.\nNach dem Überschreiten des Ablaufdatums verfallen die für diesen Prozess erzeugten Links und ein Zugriff ist nicht mehr möglich.\n\nWenn sie einem Benutzer mit aktiver Einladung erneut eine Einladung senden, bleibt der bestehende Link gültig und das Ablaufdatum wird auf den eingegebenen Wert gesetzt. \nDer Benutzer erhält eine neue E-Mail.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6173a9037ba', 'Invitation details');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6173a9037ba', 'Invitation details');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6173a9037ba', 'Invitation details');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6173a9037ba', 'Angaben zur Einladung');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e616e696f590', 'Liebe(r) %1, \n \nyou have been invited to enter your master data.\n&lt;a href="%2" target="_blank"&gt;Please follow this link to the form.&lt;/a&gt;\n\nThe link is valid until: %3\n \n %4');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e616e696f590', 'Liebe(r) %1, \n \nyou have been invited to enter your master data.\n&lt;a href="%2" target="_blank"&gt;Please follow this link to the form.&lt;/a&gt;\n\nThe link is valid until: %3\n \n %4');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e616e696f590', 'Liebe(r) %1, \n \nyou have been invited to enter your master data.\n&lt;a href="%2" target="_blank"&gt;Please follow this link to the form.&lt;/a&gt;\n\nThe link is valid until: %3\n \n %4');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e616e696f590', 'Liebe(r) %1, \n \ndu wurdest dazu eingeladen, deine Stammdaten einzutragen. \n&lt;a href="%2" target="_blank"&gt;Bitte folge diesem Link zum entsprechenden Formular.&lt;/a&gt;\n\nDer Link is gültig bis: %3\n \n %4');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e616d42da7f2', 'Invitation to fill out your master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e616d42da7f2', 'Invitation to fill out your master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e616d42da7f2', 'Invitation to fill out your master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e616d42da7f2', 'Einladung zum Ausfüllen Ihrer Stammdaten');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6169b7ee6ce', 'List of current invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6169b7ee6ce', 'List of current invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6169b7ee6ce', 'List of current invitations');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6169b7ee6ce', 'List aktueller Einladungen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6167614d39d', 'Delete invitation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6167614d39d', 'Delete invitation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6167614d39d', 'Delete invitation');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6167614d39d', 'Einladung löschen');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e616623caef6', 'Expiration date');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e616623caef6', 'Expiration date');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e616623caef6', 'Expiration date');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e616623caef6', 'Ablaufdatum');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e616611e37fe', 'User');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e616611e37fe', 'User');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e616611e37fe', 'User');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e616611e37fe', 'Benutzer');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e6165be6ed34', 'Send invitation(s)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e6165be6ed34', 'Send invitation(s)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e6165be6ed34', 'Send invitation(s)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e6165be6ed34', 'Einladung(en) versenden');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e615d660ac5d', 'Master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e615d660ac5d', 'Master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e615d660ac5d', 'Master data');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e615d660ac5d', 'Stammdaten');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67e0ba8344a', 'The following annotations were attached: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67e0ba8344a', 'The following annotations were attached: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67e0ba8344a', 'The following annotations were attached: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67e0ba8344a', 'Folgende Anmerkungen wurden hinterlegt: ');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67dfd928142', 'Optional: Annotations (will be sent by mail)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67dfd928142', 'Optional: Annotations (will be sent by mail)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67dfd928142', 'Optional: Annotations (will be sent by mail)');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67dfd928142', 'Optional: Anmerkungen (wird ebenfalls per Mail verschickt)');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67f67dd6529', 'Done');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67f67dd6529', 'Done');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67f67dd6529', 'Done');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67f67dd6529', 'Fertig');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67eaf619dad', 'Custom field');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67eaf619dad', 'Custom field');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67eaf619dad', 'Custom field');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67eaf619dad', 'Benutzerdefiniertes Feld');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67e80db5bb8', 'List of fields the user is supposed to fill out.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67e80db5bb8', 'List of fields the user is supposed to fill out.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67e80db5bb8', 'List of fields the user is supposed to fill out.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67e80db5bb8', 'Liste der Felder, die der Benutzer ausfüllen soll.');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e67e766340b7', 'Requested fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e67e766340b7', 'Requested fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e67e766340b7', 'Requested fields');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e67e766340b7', 'Abzufragende Felder');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5e7278d605a1d', 'Mobile phone number');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5e7278d605a1d', 'Mobile phone number');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5e7278d605a1d', 'Mobile phone number');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5e7278d605a1d', 'Handy-Nummer');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`) VALUES ('is_module_enabled_calsy_master_data', '10869', '10870', 'system-admin');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`) VALUES ('module_icon_calsy_master_data', '11200', '11199', 'system-admin', 'icon');

INSERT INTO `system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (1, 'p5e615d660ac5d', 'is_module_enabled_calsy_master_data', NULL, '/master_data/overview/', 220, '_module_backend');


CREATE TABLE `calsy_master_data` (
	`calsy_master_data_id` INT NOT NULL AUTO_INCREMENT,
	`calsy_master_data_user_id` INT NOT NULL,
	`calsy_master_data_code` VARCHAR(255) NOT NULL DEFAULT '',
	`calsy_master_data_timestamp_expiration` INT NOT NULL,
	PRIMARY KEY (`calsy_master_data_id`)
)
COLLATE='latin1_swedish_ci';

ALTER TABLE `calsy_master_data` ADD COLUMN `calsy_master_data_text_custom` TEXT NULL DEFAULT NULL AFTER `calsy_master_data_code`;
ALTER TABLE `calsy_master_data` ADD COLUMN `calsy_master_data_requested_fields` TEXT NOT NULL AFTER `calsy_master_data_text_custom`;

ALTER TABLE `calsy_user_frontend` ADD COLUMN `calsy_user_frontend_phone_mobile` VARCHAR(255) NULL DEFAULT NULL AFTER `calsy_user_frontend_phone`;