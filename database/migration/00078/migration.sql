/* = */

ALTER TABLE `spm`.`setting` 
CHANGE COLUMN `setting_fieldtype` `setting_fieldtype` ENUM('checkbox', 'checkbox-list', 'textarea', 'number', 'text', 'text-encrypted', 'password', 'dropdown', 'wysiwyg', 'image', 'icon', 'tags', 'timespan', 'setting') NOT NULL DEFAULT 'checkbox' ;

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p594be84853b32', 'Vmoso User Key');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p594be84853b32', 'Vmoso User Key');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p594be84853b32', 'Vmoso User Key');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p594be84853b32', 'Vmoso User Key');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p594fd75727a86', '%1 participants:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p594fd75727a86', '%1 participants:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p594fd75727a86', '%1 participants:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p594fd75727a86', '%1 Teilnehmer:');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`) VALUES ('user_vmoso_account_key', 'p594be84853b32', 'user', 'setting');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES ('', 'user_vmoso_chat_layout', 'p58e233b6dc52b', 'p58e233cccabbc', 'user', 'dropdown', 'result[\'list_layouts_chat\']');
