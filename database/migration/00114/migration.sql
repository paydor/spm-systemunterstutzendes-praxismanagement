/* = */

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5cd0158901793', 'Show the link generator for this user in the appointment assistant');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5cd0158901793', 'Show the link generator for this user in the appointment assistant');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5cd0158901793', 'Show the link generator for this user in the appointment assistant');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5cd0158901793', 'Linkgenerierung für diese Benutzer im TA anzeigen');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_type`) VALUES ('', 'is_enabled_show_link_generator_appointment_assistant', 'p5cd0158901793', 'user-admin');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5cd01b7459cbd', 'Message to the client');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5cd01b7459cbd', 'Message to the client');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5cd01b7459cbd', 'Message to the client');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5cd01b7459cbd', 'Nachricht an Klient');

ALTER TABLE `calsy_calendar_entry_user_frontend_order` ADD COLUMN `calsy_calendar_entry_user_frontend_order_message_to_client` TEXT NULL AFTER `calsy_calendar_entry_user_frontend_order_info_additional`;