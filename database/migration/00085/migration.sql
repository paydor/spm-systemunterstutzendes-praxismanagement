/* = */

ALTER TABLE `spm`.`calsy_area` 
ADD COLUMN `calsy_area_disabled_frontend` INT(1) NULL DEFAULT 0 AFTER `calsy_area_gender`;

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5991a9411b1f7', 'Don\'t show and disable selection in the frontend');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5991a9411b1f7', 'Don\'t show and disable selection in the frontend');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5991a9411b1f7', 'Don\'t show and disable selection in the frontend');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5991a9411b1f7', 'Nicht im Frontend zur Auswahl anzeigen');
