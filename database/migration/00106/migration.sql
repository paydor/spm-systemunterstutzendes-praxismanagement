/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa42245a6700', 'Security');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa42245a6700', 'Security');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa42245a6700', 'Security');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa42245a6700', 'Sicherheit');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa423af308f4', 'Layout &amp; Design');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa423af308f4', 'Layout &amp; Design');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa423af308f4', 'Layout &amp; Design');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa423af308f4', 'Layout &amp; Design');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa428c7eecd8', 'Password policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa428c7eecd8', 'Password policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa428c7eecd8', 'Password policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa428c7eecd8', 'Passwort-Richtlinie');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa4292f00e99', 'Loose');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa4292f00e99', 'Loose');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa4292f00e99', 'Loose');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa4292f00e99', 'Locker');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa42926684af', 'Strict');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa42926684af', 'Strict');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa42926684af', 'Strict');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa42926684af', 'Strikt');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa42eb96fa6e', 'Policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa42eb96fa6e', 'Policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa42eb96fa6e', 'Policy');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa42eb96fa6e', 'Richtlinie');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa42dd8b8dcb', 'Requirements of the password policies:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa42dd8b8dcb', 'Requirements of the password policies:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa42dd8b8dcb', 'Requirements of the password policies:');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa42dd8b8dcb', 'Anforderungen der Passwort-Richtlinien:');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5aa44fb6aa61a', 'The password must not contain any whitespace.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5aa44fb6aa61a', 'The password must not contain any whitespace.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5aa44fb6aa61a', 'The password must not contain any whitespace.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5aa44fb6aa61a', 'Das Passwort darf keine Leerzeichen enthalten.');

INSERT INTO `spm`.`system_menu` (`system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`) VALUES ('18', 'p5aa423af308f4', '51', '/system/layout/', '1075');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('system_security_password_policy', 'p5aa428c7eecd8', 'system-admin', 'dropdown', 'result[\'list_security_password_options\']', '100');
