/* = */

CREATE TABLE `calsy_calendar_entry_image` (
	`calsy_calendar_entry_image_id` INT(255) NOT NULL AUTO_INCREMENT,
	`calsy_calendar_entry_image_calendar_entry_id` INT(11) NOT NULL DEFAULT '0',
	`calsy_calendar_entry_image_file` TEXT NOT NULL,
	`calsy_calendar_entry_image_filename` TEXT NOT NULL,
	`calsy_calendar_entry_image_date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`calsy_calendar_entry_image_id`),
	UNIQUE INDEX `caly_calendar_entry_image_id` (`calsy_calendar_entry_image_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3;

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5d94d86ccc54f', 'Activate the selector for a specific gender');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5d94d86ccc54f', 'Activate the selector for a specific gender');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5d94d86ccc54f', 'Activate the selector for a specific gender');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5d94d86ccc54f', 'Auswahl des Geschlechts aktivieren');

INSERT INTO `setting` (`setting_name`, `setting_type`, `setting_title`) VALUES ('module_setting_calsy_area_consider_gender', 'system-admin', 'p5d94d86ccc54f');

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5d94dac737da3', 'Enable image uploads for calendar entries');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5d94dac737da3', 'Enable image uploads for calendar entries');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5d94dac737da3', 'Enable image uploads for calendar entries');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5d94dac737da3', 'Bilder-Upload für Termine aktiviert');

INSERT INTO `setting` (`setting_name`, `setting_type`, `setting_order`, `setting_title`) VALUES ('calendar_is_enabled_image_upload', 'system-admin', '1250', 'p5d94dac737da3');