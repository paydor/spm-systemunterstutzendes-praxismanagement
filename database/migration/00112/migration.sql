/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5b00619f7b24a', 'Keep the data, anonymized');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5b00619f7b24a', 'Keep the data, anonymized');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5b00619f7b24a', 'Keep the data, anonymized');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5b00619f7b24a', 'Daten anonymisiert behalten');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5b00617154513', 'Irreversibly delete all related data');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5b00617154513', 'Irreversibly delete all related data');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5b00617154513', 'Irreversibly delete all related data');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5b00617154513', 'Alle zugehörigen Daten irreversibel löschen');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5b00614c9075d', 'Behaviour when deleting users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5b00614c9075d', 'Behaviour when deleting users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5b00614c9075d', 'Behaviour when deleting users');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5b00614c9075d', 'Verhalten beim Löschen von Benutzern');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5b03118c5ff01', 'Deleted user');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5b03118c5ff01', 'Deleted user');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5b03118c5ff01', 'Deleted user');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5b03118c5ff01', 'Gelöschter Benutzer');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('250', 'is_enabled_store_data_anonymized_on_user_deletion', 'p5b00614c9075d', 'system-admin', 'dropdown', 'result[\'list_behaviour_user_deletion\']', '1000');
