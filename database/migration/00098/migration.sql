/* = */

ALTER TABLE `spm`.`calsy_area` 
ADD COLUMN `calsy_area_requires_bookable_time` INT(1) NULL DEFAULT '0' AFTER `calsy_area_disabled_frontend`;

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5a29e22e2fa12', 'Requires bookable time');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5a29e22e2fa12', 'Requires bookable time');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5a29e22e2fa12', 'Requires bookable time');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5a29e22e2fa12', 'Erfordert buchbare Zeit');
