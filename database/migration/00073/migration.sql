/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p592c40ed01fc6', 'Backend chat');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p592c40ed01fc6', 'Backend chat');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p592c40ed01fc6', 'Backend chat');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p592c40ed01fc6', 'Backend-Chat');

INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('user_vmoso_account', 'p58ca6c2aa30c4', 'user', 'text-encrypted', '100');
INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('user_vmoso_account_password', 'p58ca6c53a979e', 'user', 'password', '200');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES ('', 'is_module_enabled_calsy_vmoso_backend', '10869', '10870', 'system-admin', 'checkbox', '');
INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES ('', 'module_icon_calsy_vmoso_backend', '11200', '11199', 'system-admin', 'icon', '');

INSERT INTO `spm`.`system_setting` (`system_setting_setting_id`, `system_setting_value`) VALUES ('180', 'fa-commenting');

INSERT INTO `spm`.`system_menu` (`system_menu_id`, `system_menu_parent_id`, `system_menu_language_variable_id`, `system_menu_system_settings_required`, `system_menu_user_settings_required`, `system_menu_link`, `system_menu_order`, `system_menu_icon`) VALUES (NULL, '1', 'p592c40ed01fc6', '179', '', '/vmoso_chat/overview/', '275', '_module_backend');
