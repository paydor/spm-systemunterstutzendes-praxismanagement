/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5a047feccbc4f', 'The %1 is activated causing this module to be ignored.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5a047feccbc4f', 'The %1 is activated causing this module to be ignored.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5a047feccbc4f', 'The %1 is activated causing this module to be ignored.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5a047feccbc4f', 'Das %1 ist aktiviert, dadurch wird dieses Modul ignoriert.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5a046893d3461', 'Opening times');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5a046893d3461', 'Opening times');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5a046893d3461', 'Opening times');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5a046893d3461', 'Öffnungszeiten');

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5a063d6f8d62a', 'Appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5a063d6f8d62a', 'Appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5a063d6f8d62a', 'Appointment');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5a063d6f8d62a', 'Termin');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'is_module_blocked_calsy_opening_times', 'p58af5e4083901', 'p58af5e5f84fc9', 'system-admin', 'checkbox', '');
INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'is_module_enabled_calsy_opening_times', '10869', '10870', 'system-admin', 'checkbox', '');
INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES ('', 'module_icon_calsy_opening_times', '11200', '11199', 'system-admin', 'icon', '');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES ('', 'module_setting_calsy_opening_times_hours', 'p5a046893d3461', '10885', 'system-admin', 'text', '');
