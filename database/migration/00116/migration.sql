/* = */

INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (4, 'p5d4175d5ae4d7', 'Allow same-day bookings');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (3, 'p5d4175d5ae4d7', 'Allow same-day bookings');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (2, 'p5d4175d5ae4d7', 'Allow same-day bookings');
INSERT INTO `language_variable` (`language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (1, 'p5d4175d5ae4d7', 'Buchungen am heutigen Tag zulassen');

INSERT INTO `setting` (`setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`, `setting_order`) VALUES ('module_setting_calsy_user_frontend_registration_and_calendar_is_enabled_booking_same_day', 'p5d4175d5ae4d7', '', 'system-admin', 'checkbox', '', NULL);
