/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p592da23a9c1c2', 'Start a new chat or open an existing one to begin communicating.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p592da23a9c1c2', 'Start a new chat or open an existing one to begin communicating.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p592da23a9c1c2', 'Start a new chat or open an existing one to begin communicating.');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p592da23a9c1c2', 'Starten Sie einen neuen oder öffnen Sie einen bestehenden Chat, um zu kommunizieren.');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'user_backend_vmoso_chat_id', '', '', 'user', 'text-encrypted', '');
