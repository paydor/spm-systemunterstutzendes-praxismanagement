/* = */

INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5af5fb5d41706', 'Server secret');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5af5fb5d41706', 'Server secret');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5af5fb5d41706', 'Server secret');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5af5fb5d41706', 'Server-Geheimnis (Secret)');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5af5fb4c6658c', 'Server address');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5af5fb4c6658c', 'Server address');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5af5fb4c6658c', 'Server address');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5af5fb4c6658c', 'Server-Adresse');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '4', 'p5af5f9f3dcb26', 'BigBlueButton');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '3', 'p5af5f9f3dcb26', 'BigBlueButton');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '2', 'p5af5f9f3dcb26', 'BigBlueButton');
INSERT INTO `spm`.`language_variable` (`language_variable_id`, `language_variable_language_id`, `language_variable_unique_sub_id`, `language_variable_content`) VALUES (NULL, '1', 'p5af5f9f3dcb26', 'BigBlueButton');

INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'is_module_blocked_big_blue_button', 'p58af5e4083901', 'p58af5e5f84fc9', 'system-admin', 'checkbox', '');
INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'is_module_enabled_big_blue_button', '10869', '10870', 'system-admin', 'checkbox', '');
INSERT INTO `spm`.`setting` (`setting_id`, `setting_name`, `setting_title`, `setting_description`, `setting_type`, `setting_fieldtype`, `setting_value_array_name`) VALUES (NULL, 'module_icon_big_blue_button', '11200', '11199', 'system-admin', 'icon', '');
INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('big_blue_button_server_address', 'p5af5fb4c6658c', 'system-admin', 'text', '100');
INSERT INTO `spm`.`setting` (`setting_name`, `setting_title`, `setting_type`, `setting_fieldtype`, `setting_order`) VALUES ('big_blue_button_server_secret', 'p5af5fb5d41706', 'system-admin', 'text-encrypted', '200');

INSERT INTO `spm`.`system_setting` (`system_setting_id`, `system_setting_setting_id`, `system_setting_value`) VALUES (NULL, '232', 'md-radio-button-on');
