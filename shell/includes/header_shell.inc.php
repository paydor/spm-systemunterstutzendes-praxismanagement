<?php

$disableLoginScreen = true;
$isCommandLine = true;

$currentPathInfo = pathinfo($_SERVER['SCRIPT_FILENAME']);
$currentPath = $currentPathInfo['dirname'];

$_SERVER['DOCUMENT_ROOT'] = realpath($currentPath . '/../');

require_once $currentPath . '/../backend/includes/header.inc.php';
require_once 'framework/PerisianCLI.class.php';

PerisianSystemConfiguration::updateConfigWithServerFallbackData();

PerisianLanguageVariable::setLanguage($languageId);
