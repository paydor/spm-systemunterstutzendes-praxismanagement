<?php

require_once 'includes/header_shell.inc.php';

$disableLoginScreen = true;
$dontShowHeaderHtml = true;
$dontShowMenu = true;

$do = PerisianFrameworkToolbox::getRequest('do');
$do = strlen($do) == 0 ? getopt('d:') : $do;

if(is_array($do))
{
    
    $do = $do['d'];
    
}

if($do == 'migrate')
{
    
    PerisianFrameworkToolbox::blankPage();
            
    $result = array(
        
        'success' => 'true',
        'message' => PerisianLanguageVariable::getVariable('p5808e8dbb0741')
        
    );
    
    if(PerisianDatabaseMigration::isDatabaseUpToDate())
    {
     
        $result['message'] = PerisianLanguageVariable::getVariable('p5821eca7b3cdb');
        
    }
    else
    {
        
        try
        {

            PerisianDatabaseMigration::handleDatabaseMigration();

        }
        catch(PerisianException $e)
        {

            $result['success'] = false;
            $result['message'] = $e->getMessage();

            if($debugModeEnabled)
            {

                $result['debugInfo'] = $e->getDebugInfo();

            }

        }
        
    }
    
    echo json_encode($result);
    
}

exit;