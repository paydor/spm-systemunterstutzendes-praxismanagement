<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

exit;

ini_set('memory_limit', '-1');
gc_enable();
    
require realpath(dirname(__FILE__)) . '/library/framework/PerisianSystemConfiguration.class.php';

PerisianSystemConfiguration::initSystemConfiguration();

require realpath(dirname(__FILE__)) . '/library/framework/PerisianFrameworkToolbox.class.php';

PerisianFrameworkToolbox::handleServerSetup();
PerisianFrameworkToolbox::updateIncludePath();
PerisianFrameworkToolbox::escapeRequests();

require_once 'framework/PerisianTimeZone.class.php';

date_default_timezone_set(PerisianTimeZone::getTimeZoneName());

require_once 'framework/exceptions/PerisianException.class.php';
require_once 'framework/PerisianValidation.class.php';
require_once 'framework/database/PerisianMysql.class.php';
require_once 'framework/PerisianLanguageVariable.class.php';
require_once 'calsy/user_backend/CalsyUserBackend.class.php';
require_once 'calsy/user_frontend/CalsyUserFrontend.class.php';
require_once 'framework/PerisianSession.class.php';
require_once 'framework/settings/PerisianSystemSetting.class.php';
require_once 'framework/PerisianMenu.class.php';
require_once 'framework/controller/PerisianControllerWeb.class.php';

try
{

    PerisianSystemConfiguration::updateRunningConfiguration();

}
catch(Exception $e)
{

    print_r($e);

    exit;

}

if(isset($isCommandLine) && !$isCommandLine)
{
    
    PerisianFrameworkToolbox::forceSSL($isFrontend);
    
}

$users = CalsyUserFrontend::getUserFrontendListFormattedWithCustomFields();

$result = '';

$dummy = new CalsyUserFrontend();

foreach($users as $userEntry)
{
    
    $result .= "Account '" . utf8_decode($userEntry[$dummy->field_name_first]) . " " . utf8_decode($userEntry[$dummy->field_name_last]) . "':\n";
    
    foreach($userEntry['fields_custom'] as $customData)
    {
        
        $result .= utf8_decode($customData['label']) . ': ' . utf8_decode($customData['value']) . "\n";
        
    }
    
    $result .= "\n";
    
}

echo nl2br($result);

exit;